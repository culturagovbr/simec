<?php
/**
 * Arquivo principal de cria��o de um novo pedido de recurso financeiro.
 * $Id: novasolicitacao.inc 91268 2014-12-01 13:16:14Z maykelbraz $
 */

/**
 * Fun��es de apoio �s solicita��es recurso.
 * @see _funcoessolicitacoesrecursos.php
 */
require(APPRAIZ . 'www/progfin/_funcoessolicitacoesrecursos.php');

/**
 * Helper de exibi��o de alertas entre requisi��es.
 * @see Simec_Helper_FlashMessage
 */
require APPRAIZ . "includes/library/simec/Helper/FlashMessage.php";
$fm = new Simec_Helper_FlashMessage('progfin/novasolicitacao');