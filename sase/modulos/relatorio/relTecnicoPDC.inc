<?php
/**
 * Created by PhpStorm.
 * User: victormachado
 * Date: 06/10/2015
 * Time: 08:41
 */

include APPRAIZ . 'includes/cabecalho.inc';
ob_clean ();
global $db;
include_once APPRAIZ . 'sase/classes/AvaliadorEducacional.class.inc';
include_once APPRAIZ . 'sase/classes/RelatorioAvaliadorRedeae.class.inc';
include_once APPRAIZ . "includes/classes/dateTime.inc";
$data = new Data();

$ravid = $_REQUEST['ravid'];
$rel = new RelatorioAvaliadorRedeae();
$rel->carregarPorId($ravid);
$ave = new AvaliadorEducacional();
$ave->carregarPorId($rel->aveid);

$estado = $rel->pegaEstadoAvaliador();
$avaliador = $ave->avenome;
$sql = "select mundescricao from territorios.municipio where muncod = '".$ave->muncod."'";
$cidade = $db->pegaUm($sql, 'mundescricao');
$uf = $rel->pegaEstadoAvaliador('estuf');
$dataA = date('d/m/Y');
$diaIni = $data->formataData($rel->ratdata1periodo, 'DD');
$diaFim = $data->formataData($rel->ratdata2periodo, 'DD');
$mes = $data->formataData($rel->ratdata2periodo, 'mesTextual');
$ano = $data->formataData($rel->ratdata2periodo, 'YYYY');
$d = $cidade.'/'.$uf.', '.$dataA;


$atividades = $rel->getListaAtividadesDesenvolvidas(false, false, PFLCOD_SASE_TECNICO_DIVAPE, true, null, 2);
$resultados = $rel->getResultadosConsolidadosPlanoCarreira(false, false);
$quadro3 = $rel->getMunicipiosAlteracaoPerioroTecRedeae();
$quadro4 = $rel->getComprometimento(false, false);
$quadro5 = $rel->getEstruturaPlanoCarreiraRemuneracao(false, false);

$html = <<<HTML
<style>
    @media print {
        .page {
            page-break-after: always;

            min-height: 20px;
            /*background-color: #00516e;*/
        }

        .content{
            min-height: 40px;
            margin-bottom: 20px;
            /*background-color: #0079a1;*/
        }
        .content p {
            text-align: justify;
            text-justify: inter-word;
        }
        .content h4{margin-bottom: 5px;}
        .maiusculo{text-transform: uppercase}
        .minusculo{text-transform: lowercase}
        .italico{font-style: italic}

        table,table th, table tr, table td{border: 1px solid black; padding-left: 15px; padding-right: 15px;}
        table {border-collapse:collapse;}
        nav.navbar-listagem{display:none!important}
        .modal{display:none}
        .text-center{text-align: center;}
    }
    .page {
        page-break-after: always;

        min-height: 20px;
        /*background-color: #00516e;*/
    }

    .content{
        min-height: 40px;
        margin-bottom: 20px;
        /*background-color: #0079a1;*/
    }
    .content p {
        text-align: justify;
        text-justify: inter-word;
    }
    .content h4{margin-bottom: 5px;}
    .maiusculo{text-transform: uppercase}
    .minusculo{text-transform: lowercase}
    .italico{font-style: italic}

    table,table th, table tr, table td{border: 1px solid black; padding-left: 15px; padding-right: 15px;}
    table {border-collapse:collapse}
    nav.navbar-listagem{display:none!important}
    .modal{display:none}
    .text-center{text-align: center;}
</style>



<link rel='StyleSheet' href="/sase/css/estilo.css" type="text/css" media='screen'/>
<link rel='StyleSheet' href="/includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" media='screen'/>
<link rel='StyleSheet' href="../library/bootstrap-3.0.0/css/bootstrap.min-simec.css" rel="stylesheet" media="screen">
<script src="/includes/funcoes.js"></script>
<script src="/includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>

<div class="row">
    <div class="page col-lg-12">

        <div class="content maiusculo col-lg-offset-2 col-lg-8" style="margin-bottom: 380px;">
            <div class="text-center col-lg-12">Minist�rio da educa��o</div>
            <div class="text-center col-lg-12">Secretaria de articula��o com os sistemas de ensino</div>
            <div class="text-center col-lg-12">Diretoria de valoriza��o dos profissionais de educa��o</div>
        </div>

        <div class="content maiusculo col-lg-offset-2 col-lg-8" style="margin-bottom: 380px;>
            <p>Estado {$estado}</p>
        </div>

        <div class="content maiusculo col-lg-offset-2 col-lg-8">
            <p>Relat�rio dos resultados de elabora��o/adequa��o dos planos de carreira e remunera��o no estado de {$estado} no per�odo de {$diaIni} � {$diaFim} de {$mes} de {$ano}</p>
        </div>

        <div class="content italico col-lg-offset-2 col-lg-8">
            <div>{$avaliador}</div>
            <div>{$d}</div>
        </div>

    </div>

    <div class="page col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h4>APRESENTA��O</h4>
            <p>Este documento traz o relat�rio quinzenal das atividades de assist�ncia t�cnica realizadas em munic�pios que desenvolveram/desenvolvem a��es de adequa�ao e/ou elabora��o de planos de carreira e remunera��o, considerando a legisla��o vigente.</p>
        </div>

        <div class="content col-lg-offset-2 col-lg-8">
            <h4>JUSTIFICATIVA</h4>
            <p>A valoriza��o dos profissionais da educa��o escolar b�sica, pela sua relev�ncia, � um princ�pio estabelecido pela Constitui��o Federal do Brasil de 1988, especificamente, em seu artigo 206, incisos V e VIII. Esse princ�pio foi refor�ado por outras legisla��es constando, por exemplo, no artigo n� 67 da Lei 9394/1996, que trata das Diretrizes e Bases da Educa��o Brasileira (LDB).</p>
            <p>Em 16 de julho de 2008 foi sancionada a Lei n� 11.738, que instituiu o piso salarial profissional nacional para os profissionais do magist�rio p�blico da educa��o b�sica (PSPN), regulamentando o que se encontra disposto na Constitui��o e na LDB.</p>
            <p>Com aprova��o do Plano Nacional de Educa��o (PNE), Lei n� 13.005, de 25 de junho de 2014, ficou assegurado, atrav�s da Meta n� 18, a obrigatoriedade da Uni�o, estados e munic�pios garantirem planos de carreira para os profissionais da educa��o b�sica e superior p�blica, ampliando a abrang�ncia que a Lei 11.738/2008 havia estabelecido para os profissionais que, necessariamente, precisam ter suas carreiras regulamentadas, tendo como refer�ncia de vencimento inicial o Piso Salarial Profissional Nacional (PSPN).</p>
            <p>Considerando a necessidade dos entes federativos se adequarem ao que estabelece a Legisla��o, o Minist�rio da Educa��o (MEC), o Conselho Nacional de Secret�rios de Educa��o (CONSED) e a Uni�o Nacional dos Dirigentes Municipais de Educa��o (UNDIME) construiriam uma Rede de Assist�ncia T�cnica. A principal atribui��o da Rede ser� o desenvolvimento de atividades de forma��o, junto aos gestores das redes p�blicas de ensino e as equipes respons�veis por processos de constru��o, revis�o e implanta��o de planos de carreira e remunera��o. O presente relat�rio apresenta as a��es e resultados obtidos, nos munic�pios assistidos pelos profissionais que comp�em a mencionada Rede.</p>
        </div>

        <div class="content col-lg-offset-2 col-lg-8">
            <h4>OBJETIVO GERAL</h4>
            <p>Apresentar relat�rio quinzenal das atividades de assist�ncia t�cnica voltadas para a adequa��o e/ou elabora��o de planos de carreira e remunera��o, desenvolvida nos munic�pios assistidos, considerando: a valoriza��o profissional e a viabilidade financeira do PCR, o cumprimento da Lei 11.738/2008 e a efetiva��o da Meta 18 do Plano Nacional de Educa��o, Lei 13.005/2014.</p>
            <h4>OBJETIVOS ESPEC�FICO</h4>
            <ul>
                <li><p>Informar as a��es de assist�ncia t�cnica para a adequa��o e elabora��o de planos de carreira e remunera��o, desenvolvidas nos munic�pios assistidos no per�odo de {$diaIni} a {$diaFim} de {$mes} de {$ano}.</p></li>
                <li><p>Consolidar os resultados consolidados, decorrentes do processo de assist�ncia t�cnica para adequa��o e elabora��o de planos de carreira, desenvolvido junto aos munic�pios.</p></li>
                <li><p>Informar os resultados consolidados, decorrentes do processo de assist�ncia t�cnica para adequa��o e elabora��o de planos de carreira, desenvolvido junto aos munic�pios.</p></li>
            </ul>
        </div>
    </div>

    <div class="page col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h4>ATIVIDADES DESENVOLVIDAS</h4>
            <h5>Quadro 01. Atividades executadas</h5>
            <div>{$atividades}</div>
        </div>
    </div>

    <div class="page col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h4>RESULTADOS CONSOLIDADOS</h4>
            <h5>Quadro 02. Situa��o atual dos munic�pios assistidos com rela��o � exist�ncia de planos de carreira e remunera��o, o cumprimento da Lei 11.738/2008 e a adequa��o dos planos de carreira ao que estabelece a Meta 18 do PNE.</h5>
            <div>{$resultados}</div>
        </div>
    </div>

    <div class="page col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h5>Quadro 03. Munis�pios assistidos que apresentaram altera��o quanto � exist�ncia de planos de carreira e remunera��o.</h5>
            <div>{$quadro3}</div>
        </div>
    </div>

    <div class="page col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h5>Quadro 04. Comprometimento das receitas com gastos com pessoal.</h5>
            <div>{$quadro4}</div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="content col-lg-offset-2 col-lg-8">
            <h5>Quadro 05. Estrutura do plano de carreira e remunera��o.</h5>
            <div>{$quadro5}</div>
        </div>
    </div>
</div>
HTML;

$content = http_build_query(array(
    'conteudoHtml' => utf8_encode($html)
));

$context = stream_context_create(array(
    'http' => array (
        'method' => 'POST',
        'content' => $content
    )
));

$contents = file_get_contents ( 'http://ws.mec.gov.br/ws-server/htmlParaPdf', null, $context );
header ( 'Content-Type: application/pdf' );
header ( 'Content-Disposition: attachment; filename=Relatorio_AE_T�cnico_PDC_'.$avaliador);
echo $contents;
//echo $html;
?>
