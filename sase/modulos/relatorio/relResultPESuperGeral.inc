<?php
/**
 * Created by PhpStorm.
 * User: VictorMachado
 * Date: 28/04/2015
 * Time: 15:56
 */

// Depend�ncias
ob_start();
global $db;

$ravid = $_GET['ravid'];

$title = "Relat�rio AE Supervisor Geral";
$meses = array(
    '01' => 'Janeiro',
    '02' => 'Fevereiro',
    '03' => 'Mar�o',
    '04' => 'Abril',
    '05' => 'Maio',
    '06' => 'Junho',
    '07' => 'Julho',
    '08' => 'Agosto',
    '09' => 'Setembro',
    '10' => 'Outubro',
    '11' => 'Novembro',
    '12' => 'Dezembro'
);

include_once '../../sase/classes/AvaliadorEducacional.class.inc';
include_once '../../sase/classes/RelatorioAvaliadorRedeae.class.inc';
include_once APPRAIZ . "includes/classes/fileSimec.class.inc";
include_once APPRAIZ . "includes/classes/dateTime.inc";
// Fim Depend�ncias

$avaliadoreducacional = new AvaliadorEducacional();
$rel = new RelatorioAvaliadorRedeae();

$usucpf = $_SESSION['usucpf'];
$rel->arAtributos['usucpf'] = $usucpf;
$data = new Data();
$pflcod = PFLCOD_SASE_SUPERVISOR_GERAL;
$estuf = $rel->pegaEstadoAvaliador('estuf');
$mes = date('n');

/**
 * Carrega as informa��es do relat�rio.
 * Dentro da vari�vel '$rel'
 */
if ($ravid != ''){
    $rel->carregarPorId($ravid);
    //ver($rel, d);
    $perCampos = $rel->retornaDisCampos($_SESSION['usucpf']);

    $dados = $rel->getDados();
    $usucpf = $rel->arAtributos['usucpf'];
    $ratperiodo = $rel->arAtributos['ratperiodo'];
    $ratdata1periodo = $data->formataData($rel->arAtributos['ratdata1periodo']);
    $ratdata2periodo = $data->formataData($rel->arAtributos['ratdata2periodo']);
    $mr = explode("/", $ratdata2periodo);
    $mes = $mr[1];
    $pflcod = $rel->arAtributos['pflcod'];
    $usucpf = $rel->arAtributos['usucpf'];
    $estuf = $rel->pegaEstadoAvaliador('estuf');

}

if ($_POST['acao']){
    $retorno = array(
        "return" => null,
        "msg"    => null,
        "ravid"  => null
    );
    switch ($_POST['acao']){
        case 'teste':
            ver(simec_utf8_decode_recursive($_POST), d);
            exit;

        case 'carregar':
            /**
             * C�digo encarregado de criar o relat�rio.
             */
            if ($rel->validaRelatorio()){
                $arAtr = array();
                $arAtr['pflcod']          = $_POST['pflcod'];
                $arAtr['ratdata1periodo'] = $_POST['ratdata1periodo'];
                $arAtr['ratdata2periodo'] = $_POST['ratdata2periodo'];
                $arAtr['ratperiodo']      = $_POST['ratperiodo'];
                $arAtr['usucpf']          = $_POST['usucpf'];
                $rel->popularDadosObjeto($arAtr);

                //ver($rel->arAtributos, d);

                $ravid = $rel->salvarRelatorio();

                if ($ravid) {
                    $retorno['return'] = "true";
                    $retorno['msg']    = "Relat�rio criado com sucesso.";
                    $retorno['ravid']  = $ravid;
                } else {
                    $retorno['return'] = "false";
                    $retorno['msg']    = $rel->msg;
                }

            } else {
                $retorno['return'] = "false";
                $retorno['msg']    = $rel->msg;
            }
            echo implode("|",$retorno);
            exit;

        case 'salvar':
            $_POST    = simec_utf8_decode_recursive($_POST);
            $_REQUEST = simec_utf8_decode_recursive($_REQUEST);
            /**
             * C�digo encarregado de salvar as informa��es do relat�rio.
             */

            /**
             * Salva o quadro de Atividades Execitadas no Per�odo
             */
            if (!$rel->salvaAtividades()){
                if ($rel->msg != ''){
                    $retorno['return'] = "false";
                    $retorno['msg']    = $rel->msg;
                    echo implode("|",$retorno);
                    exit;
                } else {
                    $retorno['return'] = "false";
                    $retorno['msg']    = "Erro ao salvar as Atividades Executadas no per�odo.";
                    echo implode("|",$retorno);
                    exit;
                }
            }

            /**
             *  Salva o quadro de Informa��es a respeito do Plano Estadual de Educa��o
             */
            if (!$rel->salvaInformacoesPEESupervisorGeral()){
                if ($rel->msg != ''){
                    $retorno['return'] = "false";
                    $retorno['msg']    = $rel->msg;
                    echo implode("|",$retorno);
                    exit;
                } else {
                    $retorno['return'] = "false";
                    $retorno['msg']    = "Erro ao salvar o quadro de Informa��es a respeito do Plano Estadual de Educa��o";
                    echo implode("|",$retorno);
                    exit;
                }
            }

            /**
             *  Salva o quadro de A��es propostas pelos AE Eupervisores para os municipios sem informa��o, sem comiss�o institu�da ou sem altera��o de etapa de trabalho
             */
            if (!$rel->salvaAcoesSuperGeral()){
                if ($rel->msg != ''){
                    $retorno['return'] = "false";
                    $retorno['msg']    = $rel->msg;
                    echo implode("|",$retorno);
                    exit;
                } else {
                    $retorno['return'] = "false";
                    $retorno['msg']    = "Erro ao salvar o quadro de A��es propostas pelos AE Supervisores para os municipios sem informa��o, sem comissao institu�da e sem altera��o de etapa de trabalho";
                    echo implode("|",$retorno);
                    exit;
                }
            }

            /**
             *  Salva as informa��es complementares
             */
            if (!$rel->salvaInformacoesComplementares()){
                if ($rel->msg != ''){
                    $retorno['return'] = "false";
                    $retorno['msg']    = $rel->msg;
                    echo implode("|",$retorno);
                    exit;
                } else {
                    $retorno['return'] = "false";
                    $retorno['msg']    = "Erro ao salvar as Informa��es Complementares";
                    echo implode("|",$retorno);
                    exit;
                }
            }

            $retorno['return'] = true;
            $retorno['msg']    = "Dados do relat�rio salvos com sucesso.";
            $retorno['ravid']  = $ravid;
            echo implode("|",$retorno);

            exit;

        case 'imprimir':
            $_POST    = simec_utf8_decode_recursive($_POST);
            $_REQUEST = simec_utf8_decode_recursive($_REQUEST);
            ob_clean ();

            $html = $rel->geraRelatorioSuperGeral();
            $nome = $rel->pegaAvaliador();

            $content = http_build_query(array(
                'conteudoHtml' => utf8_encode($html)
            ));

            $context = stream_context_create(array(
                'http' => array (
                    'method' => 'POST',
                    'content' => $content
                )
            ));

            $contents = file_get_contents ( 'http://ws.mec.gov.br/ws-server/htmlParaPdf', null, $context );
            header ( 'Content-Type: application/pdf' );
            header ( 'Content-Disposition: attachment; filename=Relatorio_AE_'.$nome.'_'.$_REQUEST['ravid']);
            echo $contents;
            exit();
            break;
    }
}

include APPRAIZ . 'includes/cabecalho.inc';
$prsano = trim($_SESSION['exercicio']);
//$perCampos = true;
?>

<link rel='StyleSheet' href="/sase/css/estilo.css" type="text/css" media='screen'/>
<link rel='StyleSheet' href="/includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" media='screen'/>
<link rel='StyleSheet' href="../library/bootstrap-3.0.0/css/bootstrap.min-simec.css" rel="stylesheet" media="screen">

<script src="/includes/funcoes.js"></script>
<script src="/includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>


<script>
    function salvar(acao, ajax){
        ajax = typeof ajax !== 'undefined' ? ajax : true;
        jQuery('#acao').val(acao);
        //console.log(jQuery('#form-save').serialize());
        if (acao !== 'undefined'){
            if (ajax) {
                jQuery.ajax({
                    url: '',
                    type: 'POST',
                    data: jQuery('#form-save').serialize(),
                    //contentType: 'application/x-www-form-urlencoded; charset=ISO-8859-1',
                    //contentType: 'Content-type: text/plain; charset=iso-8859-1',
                    success: function (e) {
                        if (acao != 'teste') {
                            var a = e;
                            var r = a.split("|");
                            alert(r[1]);
                            console.log(r);
                            if (r[2] != '' && r[2] !== 'undefined') {
                                window.location.href = 'sase.php?modulo=relatorio/relResultPESuperGeral&acao=A&ravid=' + r[2];
                            }
                        } else {
                            alert(e);
                        }
                    }
                });
            } else {
                jQuery('[name=acao]').val(acao);
                jQuery('[name=form-save]').submit();
            }
        } else {
            alert('A��o n�o informada.');
        }
    }

    function montaPeriodo(){
        jQuery('#ratdata1periodo').val('01/'+jQuery('#mes').val()+'/<?= $prsano ?>');
        jQuery('#ratdata2periodo').val((new Date(2015, jQuery('#mes').val(), 0)).getDate()+'/'+jQuery('#mes').val()+'/<?= $prsano ?>');
        jQuery('#hidmes').val(jQuery('#mes').val());
        console.log(jQuery('#ratdata1periodo').val() + ' a ' + jQuery('#ratdata2periodo').val());
    }
</script>

<div id='cabecalhoSistema'>
    <center><h3><?=$titulo?></h3></center>
</div>

<div id="container">
    <div class="row">
        <div class="row col-md-12">
            <form id="form-save" method="post" name="form-save" role="form" class="form-horizontal">
                <input type="hidden" id="acao" name="acao" value=""/>
                <input type="hidden" id="ravid" name="ravid" value="<?= $ravid ?>"/>
                <input type="hidden" id="usucpf" name="usucpf" value="<?= $usucpf ?>"/>
                <input type="hidden" id="ratperiodo" name="ratperiodo" value="3"/>
                <input type="hidden" id="pflcod" name="pflcod" value="<?= PFLCOD_SASE_SUPERVISOR_GERAL ?>"/>
                <input type="hidden" id="ratdata1periodo" name="ratdata1periodo" value="<?= $ratdata1periodo ?>"/>
                <input type="hidden" id="ratdata2periodo" name="ratdata2periodo" value="<?= $ratdata2periodo ?>"/>
                <input type="hidden" id="hidmes" name="hidmes" value="<?= $mes ?>"/>

                <!-- --------------------------------------------------------------------------------------------- -->
                <!--                Informa��es gerais do relat�rio                                                -->
                <!-- --------------------------------------------------------------------------------------------- -->
                <div class="row col-md-11">
                    <div class="well">
                        <fieldset>
                            <legend>1. Informa�oes gerais do relat�rio</legend>
                            <div class="form-group">
                                <label for="pflcod" class="col-lg-4 col-md-4 control-label obrigatorio">Supervisor Geral</label>
                                <div class="col-lg-8 col-md-8">
                                    <label style="font-weight: normal; margin-top: 8px;">
                                        <?php
                                        echo $rel->pegaAvaliador();
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pflcod" class="col-lg-4 col-md-4 control-label obrigatorio">Estado</label>
                                <div class="col-lg-8 col-md-8">
                                    <label style="font-weight: normal; margin-top: 8px;">
                                        <?php
                                        echo $rel->pegaEstadoAvaliador();
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <?php if ($ratdata1periodo != '' && $ratdata2periodo != ''){ ?>
                                <div class="form-group">
                                    <label class="col-lg-4 col-md-4 control-label">Per�odo em Data</label>
                                    <div class="col-lg-8 col-md-8">
                                        <label style="font-weight: normal; margin-top: 8px;">
                                            <?php
                                            echo $ratdata1periodo . ' � ' . $ratdata2periodo;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="dmdprazo" class="col-lg-4 col-md-4 control-label obrigatorio">M�s:</label>
                                <div class="col-lg-8 col-md-8 ">
                                    <select class="form-control chosen" id="mes" name="mes" onchange="montaPeriodo()">
                                        <?php
                                        foreach ($meses as $c => $v) {
                                            $s = $mes == $c ? 'selected="true"' : '';
                                            echo <<<HTML
                                                    <option {$s} value="{$c}">$v</option>
HTML;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="text-right">
                                <?php if (empty($ravid)) { ?>
                                    <button title="Salvar" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('carregar')">Carregar
                                    </button>
                                <?php } else { ?>
                                    <button title="Salvar" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('salvar')">Salvar
                                    </button>
                                <?php } ?>
                                <?php if (!empty($ravid)) { ?>
                                    <button title="Gerar Relatorio" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('imprimir', false)">Gera Relat�rio
                                    </button>
                                <?php } ?>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <!-- --------------------------------------------------------------------------------------------- -->
                <!--            Fim Informa��es gerais do relat�rio                                                -->
                <!-- --------------------------------------------------------------------------------------------- -->

                <?php if ($ravid != ''){ ?>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                A�oes de workflow                                                              -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="col-md-1">
                        <?php
                        if( !$rel->docid && $_GET['ravid'] ){
                            //$docid = wf_cadastrarDocumento( TPDID_SASE_AVALIADOREDUCACIONAL, 'Documento Avaliador Educacional' );
                            $docid = cadastraDocumentoAE($pflcod, 'Documento Avaliador Educacional');
                            $rel->docid = $docid;
                            // ver($Assessoramento,d);
                            $rel->alterar();
                            $rel->commit();
                        }

                        if($_GET['ravid'] && $rel->docid) wf_desenhaBarraNavegacao( $rel->docid, array('docid'=>$rel->docid,'ravid'=>$rel->ravid, 'usucpf'=>$rel->arAtributos['usucpf'], 'ratdata1periodo'=>$data->formataData($rel->arAtributos['ratdata1periodo']), 'ratdata2periodo'=>$data->formataData($rel->arAtributos['ratdata2periodo'])) ); ?>

                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--            Fim A�oes de workflow                                                              -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Atividades exutadas no per�odo                                                 -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>2. Atividades Executadas no Per�odo</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                       <?php echo $rel->getListaAtividadesDesenvolvidas($perCampos, true, $pflcod); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--            Fim Atividades exutadas no per�odo                                                 -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Informa��es a respeito do Plano Estadual de Educa��o                           -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>3. Informa��es a respeito do Plano Estadual de Educa��o</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getListaInformacoesPEESupervisorGeral($perCampos, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--        Fim Informa��es a respeito do Plano Estadual de Educa��o                               -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Situa�ao atual das etapas de trabalho propostas ao PNE                         -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>4. Situa��o atual com rela��o �s etapas de trabalho propostas para a elabora��o ou adequa��o dos Planos Municipais de Educa��o ao PNE.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getContadorSituacaoAtualEtapasPNE($estuf, $mes, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--        Fim Situa��o atual das etapas de trabalho propostas ao PNE                             -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Municipios que apresentaram altera�ao de etapa no per�odo                      -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>5. Munic�pios do Estado que apresentaram altera��o na sua etapa de trabalho no per�odo.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getListaMunComAlteracaoPEESupervisorGeral($estuf, $mes, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--        Fim Municipio que apresentam altera��o de etapa no per�odo                             -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Munic�pios que permaneceram namesma etapa do per�odo anterior                  -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>6. Munic�pios que permaneceram na mesma etapa do per�odo anterior.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getListaMunSemAlteracaoEtapa($estuf, $mes, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--            Fim Munic�pios que permaneceram namesma etapa do per�odo anterior                  -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Municipios ainda sem informa��o ou sem trabalho iniciado.                      -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>7. Munic�pios ainda sem informa��o ou sem trabalho iniciado.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getListaMunSemInfo($estuf, $mes, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--            Fim Munic�pios ainda sem informa��o ou sem trabalho iniciado                       -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--     A�oes propostas pelos AE T�cnicos e Supervisores para os munic�pios.                      -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>8. A��es propostas pelos AE Supervisores para os munic�pios sem informa��o, sem comissao institu�da e sem altera��o de etapa de trabalho.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <?php echo $rel->getAcoesSuperGeral($estuf, $mes, $perCampos, true); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!-- Fim A�oes propostas pelos AE T�cnicos e Supervisores para os munic�pios.                      -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                          Informa��es complementares                                           -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <legend>9. Informa��es complementares.</legend>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12">
                                        <textarea class="form-control" maxlength="500" id="ravinfcomplementar" <?= $perCampos ? '' : 'disabled="disabled""' ?> name="ravinfcomplementar" rows="5"><?= $rel->arAtributos['ravinfcomplementar'] ?></textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                      Fim Informa��es complementares                                           -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--                Atividades exutadas no per�odo                                                 -->
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <div class="row col-md-11">
                        <div class="well">
                            <fieldset>
                                <div class="col-lg-12 col-md-12">
                                    <div class="text-right">
                                        <?php if (empty($ravid)) { ?>
                                            <button title="Salvar" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('carregar')">Carregar
                                            </button>
                                        <?php } else { ?>
                                            <button title="Salvar" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('salvar')">Salvar
                                            </button>
                                        <?php } ?>
                                        <?php if (!empty($ravid)) { ?>
                                            <button title="Gerar Relatorio" class="btn btn-success" type="button" id="btnPesquisar" onclick="salvar('imprimir', false)">Gera Relat�rio
                                            </button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- --------------------------------------------------------------------------------------------- -->
                    <!--            Fim Atividades exutadas no per�odo                                                 -->
                    <!-- --------------------------------------------------------------------------------------------- -->

                <?php } ?>

            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.campoCpf').mask("999.999.999-99");
        $('.campoData').mask('99/99/9999');
        $('.campoData').datepicker();
        // Inicia o per�odo com o valor marcado na combo 'M�s'
        montaPeriodo();
    });
</script>