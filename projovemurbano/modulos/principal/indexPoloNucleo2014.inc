<?php 
if($_SESSION['projovemurbano']['ppuid'] != '3'){
	echo "<script>
			alert('Falha na verifica��o do ano de exercicio.');
			window.location.href = 'projovemurbano.php?modulo=inicio&acao=C';
		  </script>";
	die();
}

if(!$_SESSION['projovemurbano']['pjuid'])die("<script>alert('Problemas de navega��o. Inicie novamente.');window.location='projovemurbano.php?modulo=inicio&acao=C';</script>");

include_once APPRAIZ."/www/projovemurbano/_funcoes_planoImplementacao2014.php";

if($_REQUEST['requisicao']) {
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}

$docid = $db->pegaUm("SELECT docid FROM projovemurbano.projovemurbano WHERE pjuid='".$_SESSION['projovemurbano']['pjuid']."'");

if(!$docid) {
	$docid = criaDocumento();
	$db->executar("UPDATE projovemurbano.projovemurbano SET docid='".$docid."' WHERE pjuid='".$_SESSION['projovemurbano']['pjuid']."'");
	$db->commit();
}

$esdid = $db->pegaUm("SELECT esdid FROM workflow.documento WHERE docid='".$docid."'");

include_once APPRAIZ . 'includes/cabecalho.inc';
echo '<br>';

monta_titulo('Projovem Urbano', montaTituloEstMun());

echo montarAbasArray(montaMenuProJovemUrbano(), "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A");


$us = pegarUsuarioProJovem();
$rsMetas = recuperaMetasPorUfMuncod($us);

switch( $_GET['aba'] )
{
	case 'Publico_Juventude_Viva':
		$abaAtiva = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=Publico_Juventude_Viva";
		$_SESSION['projovemurbano']['tprid'] = 1;
		$arrOrientacoes = Array('','',
				'<p>A seguir apresenta-se a meta final do termo de ades�o para o exerc�cio de 
				2014, bem como o per�odo de matr�cula e a data do in�cio de aula.</p>',
				'<p>O Polo s� pode ser constitu�do na localidade onde a Secretaria de 
				Educa��o for organizada em coordenadorias ou regionais de ensino. 
				Os N�cleos, localizados nos munic�pios da �rea de abrang�ncia de cada 
				regional de ensino, independentemente de seu n�mero, formam um Polo. 
				O Polo �, portanto, uma inst�ncia de gest�o do Projovem Urbano, que 
				funciona no espa�o f�sico da pr�pria regional de ensino.</p>
				<p>Um n�cleo � composto por 5 (cinco) turmas. Se o n�cleo for igual a 1 
				(um), o n� de alunos deve ser necessariamente 200. Se o n� de n�cleo for 
				maior que 1 (um), o n� de alunos no n�cleo poder� variar entre 150 e 200.
				A turma pode ter entre 30 e 40 alunos cada uma.</p>',
				'<p>A equipe � composta por:</p>
				<p> 1 coordenador geral.</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> 1 diretor de polo (onde houver regional de ensino).</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> At� 2 profissionais por n�cleo de apoio para a etapa de matr�cula.</p>
				<p> 5 educadores de ensino fundamental (Ci�ncias Humanas, L�ngua Portuguesa, Ingl�s, Matem�tica e Ci�ncias da Natureza).</p>
				<p> 1 educador de qualifica��o profissional.</p>
				<p> 1 educador de participa��o cidad�.</p>
				<p> At� 2 educadores para as salas de acolhimento das crian�as de 0 a 8 anos dos jovens atendidos pelo programa.</p>
				<p> 1 tradutor e int�rprete de libras.</p>
				<p> 1 educador para atendimento educacional especializado.</p>
				<p>A seguir ser� discriminado o pagamento dos educadores, dos assistentes, 
				da coordena��o geral e do diretor de polo, quando houver, e dos 
				profissionais para a etapa da matr�cula, de acordo com percentual m�ximo 
				permitido.</p>
				<p>Ao final do lan�amento dever� ser informado o instrumento legal que 
				rege as remunera��es dos profissionais.</p>
				<p>Para maiores esclarecimentos, consulte a Resolu��o n� 54 de 21 de 
				novembro de 2012.</p>',
				'<p>A seguir ser� discriminado o custeio da forma��o continuada de 
				educadores de ensino fundamental, qualifica��o profissional e 
				participa��o cidad�, bem como pagamento de aux�lio financeiro aos 
				educadores durante a primeira etapa de forma��o, conforme os 
				percentuais determinados na Resolu��o CD/FNDE n� 54/2012.</p>
				<p>ATEN��O: somente os educadores N�O contratados at� o per�odo da 
				forma��o poder�o receber o aux�lio financeiro.</p>
				<p>Carga hor�ria da forma��o de educadores:</p>
				<p>Forma��o Continuada - Primeira etapa (realizada antes do in�cio das 
				aulas) - 96 horas presenciais e 64 horas de atividades n�o presenciais</p>
				<p>Forma��o Continuada - Demais etapas (realizadas em formas de 
				encontros durante os 18 meses de curso) - 216 horas presenciais</p>
				<p>Os munic�pios com menos de 20 educadores poder�o designar do quadro 
				da rede de ensino local e/ou contratar at� dois formadores para garantir 
				a continuidade das a��es da forma��o at� o final do curso, observando o 
				percentual permitido para o custeio dessa a��o.</p>
				<p>As demais localidades dever�o designar e/ou contratar um formador para 
				cada grupo de 20 a 30 educadores tamb�m observando o percentual 
				permitido para o custeio dessa a��o.</p>',
				'<p>Aquisi��o de g�neros aliment�cios destinados para fornecimento de 
				lanche ou refei��o aos jovens matriculados e frequentes, no �mbito 
				do Programa, bem como para filhos de benefici�rios do Programa que 
				tenham at� oito anos e sejam atendidos nas salas de acolhimento, at� que 
				o ente executor passe a receber os recursos procedentes do Programa 
				Nacional de Alimenta��o Escolar (PNAE).</p>',
				'<p>A Qualifica��o Profissional ser� ofertada por meio da Forma��o T�cnica 
				Geral - FTG e da Forma��o T�cnica Espec�fica FTE e do Projeto de 
				Orienta��o Profissional - POP. A Forma��o T�cnica Espec�fica, por sua 
				vez, poder� ser desenvolvida por meio de programas nacionais de 
				educa��o t�cnica em forma��o inicial e continuada ou por meio de 
				arcos ocupacionais ou por meio das duas op��es (Arcos Ocupacionais e 
				PRONATEC).</p>
				<p>Para a oferta da FTG, por meio dos arcos ocupacionais, apresenta-se, a 
				seguir, as despesas poss�veis para esta execu��o: loca��o de espa�os e 
				equipamentos, aquisi��o de material de consumo, bem como pagamento 
				de monitores para as atividades pr�ticas da qualifica��o profissional 
				quando for necess�rio, conforme definido na Resolu��o CD/FNDE n� 54/
				2012.</p>',
				'<p>Para os Estados e DF: discrimina-se a seguir at� 1,5% do montante 
				repassado para pagamento do transporte do material did�tico-pedag�gico 
				entregue pelo Governo Federal, da capital at� os Munic�pios.</p>',
				'<p>Caso o EEx n�o atinja os percentuais m�ximos previstos na Resolu��o 
				CD/FNDE n� 54/2012 nas a��es descritas nas abas anteriores, poder� 
				empregar o restante dos recursos transferidos para custear as a��es 
				discriminadas abaixo, de acordo com percentuais m�ximo permitidos 
				definidos na mesma Resolu��o.</p>',
				'<p>Este Munic�pio/Estado esclarece que tem ci�ncia de que o Plano de 
				Implementa��o apresentado � um instrumento de planejamento de 
				a��es e controle de gastos e que, para efeito de repasse da 1� parcela de 
				recursos, � considerado o n�mero da meta de estudantes.</p>
				<p>Da mesma forma, este Munic�pio/Estado tem ci�ncia de que as demais 
				parcelas ser�o efetuadas com base na f�rmula descrita no Anexo VI da 
				Resolu��o CD/FNDE N� 54/2012. Na mesma resolu��o determina-se 
				que esses repasses ter�o como base de c�lculo o n�mero de estudantes 
				efetivamente matriculados e frequentes, de acordo com o Sistema de 
				Matr�cula, Acompanhamento de Frequ�ncia e Certifica��o do Projovem 
				Urbano.</p>
				<p>Por fim, este Munic�pio/Estado compromete-se a atualizar estas 
				informa��es, constantemente, conforme prazos e por meio dos 
				instrumentos estabelecidos pela DPEJUV/SECADI/MEC.</p>',
				'<p>Esta planilha demonstra os gastos a serem realizados por este Munic�pio. 
				� importante ressaltar que os repasses a serem efetuados s�o calculados 
				com base no n�mero de alunos (per capita) e na f�rmula descrita no 
				Anexo VI � Resolu��o do FNDE acerca do ProJovem Urbano, para o 
				exerc�cio de 2014.</p>'
				);
		break;
	case 'Publico_Unidades_Prisionais':
		$abaAtiva = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=Publico_Unidades_Prisionais";
		$_SESSION['projovemurbano']['tprid'] = 2;
		$arrOrientacoes = Array('','',
				'<p>A seguir apresenta-se a meta final do termo de ades�o para o exerc�cio de
				2014, bem como o per�odo de matr�cula e a data do in�cio de aula.</p>',
				'<p>O Polo s� pode ser constitu�do na localidade onde a Secretaria de
				Educa��o for organizada em coordenadorias ou regionais de ensino.
				Os N�cleos, localizados nos munic�pios da �rea de abrang�ncia de cada
				regional de ensino, independentemente de seu n�mero, formam um Polo.
				O Polo �, portanto, uma inst�ncia de gest�o do Projovem Urbano, que
				funciona no espa�o f�sico da pr�pria regional de ensino.</p>
				<p>O n�cleo da Unidade Prisional � composto de no m�nimo 60 estudantes e no m�ximo 150.</p>',
				'<p>A equipe � composta por:</p>
				<p> 1 coordenador geral.</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> 1 diretor de polo (onde houver regional de ensino).</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> At� 2 profissionais por n�cleo de apoio para a etapa de matr�cula.</p>
				<p> 5 educadores de ensino fundamental (Ci�ncias Humanas, L�ngua Portuguesa, Ingl�s, Matem�tica e Ci�ncias da Natureza).</p>
				<p> 1 educador de qualifica��o profissional.</p>
				<p> 1 educador de participa��o cidad�.</p>
				<p> At� 2 educadores para as salas de acolhimento das crian�as de 0 a 8 anos dos jovens atendidos pelo programa.</p>
				<p> 1 tradutor e int�rprete de libras.</p>
				<p> 1 educador para atendimento educacional especializado.</p>
				<p>A seguir ser� discriminado o pagamento dos educadores, dos assistentes,
				da coordena��o geral e do diretor de polo, quando houver, e dos
				profissionais para a etapa da matr�cula, de acordo com percentual m�ximo
				permitido.</p>
				<p>Ao final do lan�amento dever� ser informado o instrumento legal que
				rege as remunera��es dos profissionais.</p>
				<p>Para maiores esclarecimentos, consulte a Resolu��o n� 54 de 21 de
				novembro de 2012.</p>',
				'<p>A seguir ser� discriminado o custeio da forma��o continuada de
				educadores de ensino fundamental, qualifica��o profissional e
				participa��o cidad�, bem como pagamento de aux�lio financeiro aos
				educadores durante a primeira etapa de forma��o, conforme os
				percentuais determinados na Resolu��o CD/FNDE n� 54/2012.</p>
				<p>ATEN��O: somente os educadores N�O contratados at� o per�odo da
				forma��o poder�o receber o aux�lio financeiro.</p>
				<p>Carga hor�ria da forma��o de educadores:</p>
				<p>Forma��o Continuada - Primeira etapa (realizada antes do in�cio das
				aulas) - 96 horas presenciais e 64 horas de atividades n�o presenciais</p>
				<p>Forma��o Continuada - Demais etapas (realizadas em formas de
				encontros durante os 18 meses de curso) - 216 horas presenciais</p>
				<p>Os munic�pios com menos de 20 educadores poder�o designar do quadro
				da rede de ensino local e/ou contratar at� dois formadores para garantir
				a continuidade das a��es da forma��o at� o final do curso, observando o
				percentual permitido para o custeio dessa a��o.</p>
				<p>As demais localidades dever�o designar e/ou contratar um formador para
				cada grupo de 20 a 30 educadores tamb�m observando o percentual
				permitido para o custeio dessa a��o.</p>',
				'<p>Aquisi��o de g�neros aliment�cios destinados para fornecimento de
				lanche ou refei��o aos jovens matriculados e frequentes, no �mbito
				do Programa, bem como para filhos de benefici�rios do Programa que
				tenham at� oito anos e sejam atendidos nas salas de acolhimento, at� que
				o ente executor passe a receber os recursos procedentes do Programa
				Nacional de Alimenta��o Escolar (PNAE).</p>',
				'<p>A Qualifica��o Profissional ser� ofertada por meio da Forma��o T�cnica
				Geral - FTG e da Forma��o T�cnica Espec�fica FTE e do Projeto de
				Orienta��o Profissional - POP. A Forma��o T�cnica Espec�fica, por sua
				vez, poder� ser desenvolvida por meio de programas nacionais de
				educa��o t�cnica em forma��o inicial e continuada ou por meio de
				arcos ocupacionais ou por meio das duas op��es (Arcos Ocupacionais e
				PRONATEC).</p>
				<p>Para a oferta da FTG, por meio dos arcos ocupacionais, apresenta-se, a
				seguir, as despesas poss�veis para esta execu��o: loca��o de espa�os e
				equipamentos, aquisi��o de material de consumo, bem como pagamento
				de monitores para as atividades pr�ticas da qualifica��o profissional
				quando for necess�rio, conforme definido na Resolu��o CD/FNDE n� 54/
				2012.</p>',
				'<p>Para os Estados e DF: discrimina-se a seguir at� 1,5% do montante
				repassado para pagamento do transporte do material did�tico-pedag�gico
				entregue pelo Governo Federal, da capital at� os Munic�pios.</p>',
				'<p>Caso o EEx n�o atinja os percentuais m�ximos previstos na Resolu��o
				CD/FNDE n� 54/2012 nas a��es descritas nas abas anteriores, poder�
				empregar o restante dos recursos transferidos para custear as a��es
				discriminadas abaixo, de acordo com percentuais m�ximo permitidos
				definidos na mesma Resolu��o.</p>',
				'<p>Este Munic�pio/Estado esclarece que tem ci�ncia de que o Plano de
				Implementa��o apresentado � um instrumento de planejamento de
				a��es e controle de gastos e que, para efeito de repasse da 1� parcela de
				recursos, � considerado o n�mero da meta de estudantes.</p>
				<p>Da mesma forma, este Munic�pio/Estado tem ci�ncia de que as demais
				parcelas ser�o efetuadas com base na f�rmula descrita no Anexo VI da
				Resolu��o CD/FNDE N� 54/2012. Na mesma resolu��o determina-se
				que esses repasses ter�o como base de c�lculo o n�mero de estudantes
				efetivamente matriculados e frequentes, de acordo com o Sistema de
				Matr�cula, Acompanhamento de Frequ�ncia e Certifica��o do Projovem
				Urbano.</p>
				<p>Por fim, este Munic�pio/Estado compromete-se a atualizar estas
				informa��es, constantemente, conforme prazos e por meio dos
				instrumentos estabelecidos pela DPEJUV/SECADI/MEC.</p>',
				'<p>Esta planilha demonstra os gastos a serem realizados por este Munic�pio.
				� importante ressaltar que os repasses a serem efetuados s�o calculados
				com base no n�mero de alunos (per capita) e na f�rmula descrita no
				Anexo VI � Resolu��o do FNDE acerca do ProJovem Urbano, para o
				exerc�cio de 2014.</p>'
		);
		break;
	case 'Publico_Geral':
		$abaAtiva = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=Publico_Geral";
		$_SESSION['projovemurbano']['tprid'] = 3;
		$arrOrientacoes = Array('','',
				'<p>A seguir apresenta-se a meta final do termo de ades�o para o exerc�cio de
				2014, bem como o per�odo de matr�cula e a data do in�cio de aula.</p>',
				'<p>O Polo s� pode ser constitu�do na localidade onde a Secretaria de
				Educa��o for organizada em coordenadorias ou regionais de ensino.
				Os N�cleos, localizados nos munic�pios da �rea de abrang�ncia de cada
				regional de ensino, independentemente de seu n�mero, formam um Polo.
				O Polo �, portanto, uma inst�ncia de gest�o do Projovem Urbano, que
				funciona no espa�o f�sico da pr�pria regional de ensino.</p>
				<p>Um n�cleo � composto por 5 (cinco) turmas. Se o n�cleo for igual a 1
				(um), o n� de alunos deve ser necessariamente 200. Se o n� de n�cleo for
				maior que 1 (um), o n� de alunos no n�cleo poder� variar entre 150 e 200.
				A turma pode ter entre 30 e 40 alunos cada uma.</p>',
				'<p>A equipe � composta por:</p>
				<p> 1 coordenador geral.</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> 1 diretor de polo (onde houver regional de ensino).</p>
				<p> 1 assistente pedag�gico.</p>
				<p> 1 assistente administrativo.</p>
				<p> At� 2 profissionais por n�cleo de apoio para a etapa de matr�cula.</p>
				<p> 5 educadores de ensino fundamental (Ci�ncias Humanas, L�ngua Portuguesa, Ingl�s, Matem�tica e Ci�ncias da Natureza).</p>
				<p> 1 educador de qualifica��o profissional.</p>
				<p> 1 educador de participa��o cidad�.</p>
				<p> At� 2 educadores para as salas de acolhimento das crian�as de 0 a 8 anos dos jovens atendidos pelo programa.</p>
				<p> 1 tradutor e int�rprete de libras.</p>
				<p> 1 educador para atendimento educacional especializado.</p>
				<p>A seguir ser� discriminado o pagamento dos educadores, dos assistentes,
				da coordena��o geral e do diretor de polo, quando houver, e dos
				profissionais para a etapa da matr�cula, de acordo com percentual m�ximo
				permitido.</p>
				<p>Ao final do lan�amento dever� ser informado o instrumento legal que
				rege as remunera��es dos profissionais.</p>
				<p>Para maiores esclarecimentos, consulte a Resolu��o n� 54 de 21 de
				novembro de 2012.</p>',
				'<p>A seguir ser� discriminado o custeio da forma��o continuada de
				educadores de ensino fundamental, qualifica��o profissional e
				participa��o cidad�, bem como pagamento de aux�lio financeiro aos
				educadores durante a primeira etapa de forma��o, conforme os
				percentuais determinados na Resolu��o CD/FNDE n� 54/2012.</p>
				<p>ATEN��O: somente os educadores N�O contratados at� o per�odo da
				forma��o poder�o receber o aux�lio financeiro.</p>
				<p>Carga hor�ria da forma��o de educadores:</p>
				<p>Forma��o Continuada - Primeira etapa (realizada antes do in�cio das
				aulas) - 96 horas presenciais e 64 horas de atividades n�o presenciais</p>
				<p>Forma��o Continuada - Demais etapas (realizadas em formas de
				encontros durante os 18 meses de curso) - 216 horas presenciais</p>
				<p>Os munic�pios com menos de 20 educadores poder�o designar do quadro
				da rede de ensino local e/ou contratar at� dois formadores para garantir
				a continuidade das a��es da forma��o at� o final do curso, observando o
				percentual permitido para o custeio dessa a��o.</p>
				<p>As demais localidades dever�o designar e/ou contratar um formador para
				cada grupo de 20 a 30 educadores tamb�m observando o percentual
				permitido para o custeio dessa a��o.</p>',
				'<p>Aquisi��o de g�neros aliment�cios destinados para fornecimento de
				lanche ou refei��o aos jovens matriculados e frequentes, no �mbito
				do Programa, bem como para filhos de benefici�rios do Programa que
				tenham at� oito anos e sejam atendidos nas salas de acolhimento, at� que
				o ente executor passe a receber os recursos procedentes do Programa
				Nacional de Alimenta��o Escolar (PNAE).</p>',
				'<p>A Qualifica��o Profissional ser� ofertada por meio da Forma��o T�cnica
				Geral - FTG e da Forma��o T�cnica Espec�fica FTE e do Projeto de
				Orienta��o Profissional - POP. A Forma��o T�cnica Espec�fica, por sua
				vez, poder� ser desenvolvida por meio de programas nacionais de
				educa��o t�cnica em forma��o inicial e continuada ou por meio de
				arcos ocupacionais ou por meio das duas op��es (Arcos Ocupacionais e
				PRONATEC).</p>
				<p>Para a oferta da FTG, por meio dos arcos ocupacionais, apresenta-se, a
				seguir, as despesas poss�veis para esta execu��o: loca��o de espa�os e
				equipamentos, aquisi��o de material de consumo, bem como pagamento
				de monitores para as atividades pr�ticas da qualifica��o profissional
				quando for necess�rio, conforme definido na Resolu��o CD/FNDE n� 54/
				2012.</p>',
				'<p>Para os Estados e DF: discrimina-se a seguir at� 1,5% do montante
				repassado para pagamento do transporte do material did�tico-pedag�gico
				entregue pelo Governo Federal, da capital at� os Munic�pios.</p>',
				'<p>Caso o EEx n�o atinja os percentuais m�ximos previstos na Resolu��o
				CD/FNDE n� 54/2012 nas a��es descritas nas abas anteriores, poder�
				empregar o restante dos recursos transferidos para custear as a��es
				discriminadas abaixo, de acordo com percentuais m�ximo permitidos
				definidos na mesma Resolu��o.</p>',
				'<p>Este Munic�pio/Estado esclarece que tem ci�ncia de que o Plano de
				Implementa��o apresentado � um instrumento de planejamento de
				a��es e controle de gastos e que, para efeito de repasse da 1� parcela de
				recursos, � considerado o n�mero da meta de estudantes.</p>
				<p>Da mesma forma, este Munic�pio/Estado tem ci�ncia de que as demais
				parcelas ser�o efetuadas com base na f�rmula descrita no Anexo VI da
				Resolu��o CD/FNDE N� 54/2012. Na mesma resolu��o determina-se
				que esses repasses ter�o como base de c�lculo o n�mero de estudantes
				efetivamente matriculados e frequentes, de acordo com o Sistema de
				Matr�cula, Acompanhamento de Frequ�ncia e Certifica��o do Projovem
				Urbano.</p>
				<p>Por fim, este Munic�pio/Estado compromete-se a atualizar estas
				informa��es, constantemente, conforme prazos e por meio dos
				instrumentos estabelecidos pela DPEJUV/SECADI/MEC.</p>',
				'<p>Esta planilha demonstra os gastos a serem realizados por este Munic�pio.
				� importante ressaltar que os repasses a serem efetuados s�o calculados
				com base no n�mero de alunos (per capita) e na f�rmula descrita no
				Anexo VI � Resolu��o do FNDE acerca do ProJovem Urbano, para o
				exerc�cio de 2014.</p>'
		);
		break;
	default:
		$abaAtiva = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=coordenadorResponsavel";
		$pagAtiva = "coordenadorResponsavel";
		unset($_SESSION['projovemurbano']['tprid']);
		break;
}

$menu[] = array("id" => 1,
		"descricao" => "Coordenador Respons�vel",
		"link" => "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=coordenadorResponsavel");

$sql = "SELECT * FROM projovemurbano.coordenadorresponsavel WHERE pjuid='".$_SESSION['projovemurbano']['pjuid']."'";
$coordenadorresponsavel = $db->pegaLinha($sql);

if ($coordenadorresponsavel) {
	
	if ($_SESSION ['projovemurbano'] ['muncod']) {
		$cmecodibge = $_SESSION ['projovemurbano'] ['muncod'];
	} else {
		$cmecodibge = $db->pegaUm ( "select estcod from territorios.estado where estuf = '{$_SESSION['projovemurbano']['estuf']}'" );
	}
	
	if ($_SESSION ['projovemurbano'] ['muncod']) {
		
		$sql = "SELECT cam.* FROM projovemurbano.cargameta cam
		LEFT JOIN territorios.municipio mun ON (cam.cmecodibge = mun.muncod::INT)
		LEFT JOIN projovemurbano.projovemurbano ppu ON cam.cmecodibge = ppu.muncod::INT
		where cam.cmecodibge = '{$cmecodibge}' and ppu.ppuid = {$_SESSION['projovemurbano']['ppuid']} AND pjuid = {$_SESSION['projovemurbano']['pjuid']}";
	} else {
		
		$sql = "SELECT cam.* FROM projovemurbano.cargameta cam
		LEFT JOIN territorios.estado est ON (cam.cmecodibge = est.estcod::INT)
		LEFT JOIN projovemurbano.projovemurbano ppu ON cam.cmecodibge = est.estcod::INT
		where cam.cmecodibge = '{$cmecodibge}' and cam.ppuid = {$_SESSION['projovemurbano']['ppuid']} AND pjuid = {$_SESSION['projovemurbano']['pjuid']}";
	}
	
	$rsCargaMeta = $db->pegaLinha ( $sql );
	
	$sql = "select
		t.tpmid,
		mtpvalor,
		tpmdescricao,
		pjuid
		from
		projovemurbano.tipometadoprograma t
		left join
		projovemurbano.metasdoprograma m on m.tpmid = t.tpmid
		and cmeid = {$rsCargaMeta['cmeid']}
		and ppuid = {$_SESSION['projovemurbano']['ppuid']}
				where
				t.tpmid in (7,10,13)";
	
	$rsMetas = $db->carregar ( $sql );
	
	if ($_SESSION ['projovemurbano'] ['ppuid'] == 3) {
		
		$sql = '';
		foreach ( $rsMetas as $value ) {
			if (! $value ['pjuid']) {
				if ($value ['mtpvalor'] != '') {
					$sql .= "insert into projovemurbano.metasdoprograma(tpmid, ppuid, suaid, cmeid, mtpvalor, pjuid)
					values ({$value['tpmid']}, {$_SESSION['projovemurbano']['ppuid']}, null, {$rsCargaMeta['cmeid']}, '{$value['mtpvalor']}', '{$_SESSION['projovemurbano']['pjuid']}');";
				}
			}
		}
		
		if (! empty ( $sql )) {
			$sqlDelete = 'delete from projovemurbano.metasdoprograma where cmeid = ' . $rsCargaMeta ['cmeid'] . ' and tpmid in (7,10,13);';
			$db->executar ( $sqlDelete );
			$db->executar ( $sql );
			$db->commit ();
		}
	}
	
// 	if ($_SESSION ['projovemurbano'] ['muncod']) {
		$sql = "SELECT adesaotermoajustado FROM projovemurbano.projovemurbano WHERE pjuid = {$_SESSION['projovemurbano']['pjuid']} and ppuid = 3";
		$ajustado = $db->pegaUm ( $sql );
// 	}
	if ($ajustado == 't') {
		$not = ",7,10,13";
	} else {
		$not = ",9,12,15";
	}
	
	$sql = "SELECT DISTINCT
		removeacento(replace(tprdesc,' ','_')) as aba,
		tprdesc as descricao, mtpvalor
		FROM
		projovemurbano.tipoprograma tpr
		INNER JOIN projovemurbano.tipoprograma_programaprojovemurbano nn ON nn.tprid = tpr.tprid
		INNER JOIN projovemurbano.tipometadoprograma tmp ON tmp.tprid = nn.tprid
		INNER JOIN projovemurbano.metasdoprograma mtp ON mtp.tpmid = tmp.tpmid
		WHERE
		mtp.ppuid = 3
		AND mtp.pjuid = {$_SESSION['projovemurbano']['pjuid']}
		AND mtpvalor > 0
				AND tmp.tpmid NOT IN  (2,5,8,11,14$not)";
	$programas = $db->carregar ( $sql );
	if ($programas) {
		
		foreach ( $programas as $k => $programa ) {
			$menu [] = array (
					"id" => $k + 2,
					"descricao" => $programa ['descricao'],
					"link" => "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba=" . $programa ['aba'] 
			);
		}
		
// 		if (! $_GET ['aba']) {
// 			foreach ( $programas as $k => $programa ) {
// 				if ($programa ['aba'] != '') {
// 					$_GET ['aba'] = $programa ['aba'];
					
// 					break;
// 				}
// 			}
// 		}
	}
}
	
echo montarAbasArray($menu, $abaAtiva);
// monta_titulo(' ', '');
if( $_GET['aba'] != '' && $_GET['aba'] != 'coordenadorResponsavel'){
	switch( $_REQUEST['aba2'] )
	{
		case 'poloNucleo':
			$abaAtiva2 = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba={$_GET['aba']}&aba2=poloNucleo2014";
			$pagAtiva = "poloNucleo";
			break;
		default:
			$abaAtiva2 = "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba={$_GET['aba']}&aba2=poloNucleo2014";
			$pagAtiva = "poloNucleo";
			break;
	}

// 	echo "<br>";
	
	$menu2[] = array("id" => 1,"descricao" => "P�lo/N�cleo","link" => "projovemurbano.php?modulo=principal/indexPoloNucleo2014&acao=A&aba={$_GET['aba']}&aba2=poloNucleo2014");
// 	ver($menu2, $abaAtiva2,d);
// echo montarAbasArray($menu2, $abaAtiva2);
// 	monta_titulo(' ', '');

}
	include 'planoImplementacao'.$_SESSION['projovemurbano']['ppuano'].'/'.$pagAtiva.$_SESSION['projovemurbano']['ppuano'].'.inc';
?>
