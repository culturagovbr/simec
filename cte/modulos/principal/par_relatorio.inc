<?php


verifica_sessao();

$ptostatus = isset( $_REQUEST['ptostatus'] ) ? $_REQUEST['ptostatus'] : 'A';

// ----- CARREGA DADOS DO BANCO SOBRE INSTRUMENTOUNIDADE - INU -----------------
	$sqlinu = "select * from cte.instrumentounidade where inuid = " . 
			$_SESSION["inuid"];
	$dadosinu = $db->recuperar( $sqlinu );
	if ( is_array( $dadosinu ) )
	{
		$inueqploc = $dadosinu['inueqploc'];
		$inuddsdmgr = $dadosinu['inuddsdmgr'];
		$inucadsec = $dadosinu['inucadsec'];
		$inucadcmt = $dadosinu['inucadcmt'];
	}
	
	$inueqploc = $inueqploc == "null" ? "" : $inueqploc;
	$inuddsdmgr = $inuddsdmgr == "null" ? "" : $inuddsdmgr;
	$inucadsec = $inucadsec == "null" ? "" : $inucadsec;
	$inucadcmt = $inucadcmt == "null" ? "" : $inucadcmt;
// ----- FIM CARREGA DADOS DO BANCO SOBRE INSTRUMENTOUNIDADE - INU -------------

// ----- CARREGA SINTESE POR INDICADOR -----------------------------------------
	$sqlsi = sprintf( "
		select distinct
			d.dimcod
			,d.dimdsc
			,ad.ardcod
			,ad.arddsc
			,i.indcod
			,c.ctrpontuacao
			,p.ptojustificativa
			,p.ptodemandamunicipal
			,p.ptodemandaestadual
		from 
			cte.instrumento ins
			inner join cte.instrumentounidade iu on iu.itrid = ins.itrid
			inner join cte.dimensao d on d.itrid = ins.itrid
			inner join cte.areadimensao ad on d.dimid = ad.dimid
			inner join cte.indicador i on i.ardid = ad.ardid
			inner join cte.criterio c on c.indid = i.indid
			inner join cte.pontuacao p on p.crtid = c.crtid and p.inuid = %d
		where 
			d.dimstatus = 'A'
			and ad.ardstatus = 'A'  
			and i.indstatus = 'A'
			and d.itrid = %d
			and iu.estuf = '%s'
			and p.ptostatus = '%s'
		" , 
			$_SESSION['inuid'],
			$_SESSION['itrid'],
			$_SESSION['estuf'],
			$ptostatus
	);
	$dadossi = $db->carregar($sqlsi);
// ----- FIM CARREGA SINTESE POR INDICADOR -------------------------------------

// ----- CARREGA SINTESE POR DIMENS�O ------------------------------------------
	$sqlSD = sprintf( "
		select distinct
			ins.itrid
			,d.dimid
			,d.dimcod || '. ' ||d.dimdsc as dimdsc
			,c.ctrpontuacao 
			,count ( c.ctrpontuacao ) as qtpontos
			
		from cte.instrumento ins
			inner join cte.instrumentounidade iu on iu.itrid = ins.itrid
			inner join cte.dimensao d on d.itrid = ins.itrid and d.itrid = %d
			inner join cte.areadimensao ad on d.dimid = ad.dimid
			inner join cte.indicador i on i.ardid = ad.ardid
			inner join cte.criterio c on c.indid = i.indid
			left join cte.pontuacao pt on pt.crtid = c.crtid and pt.inuid = %d
		where
			d.dimstatus = 'A'
			and ad.ardstatus = 'A'  
			and i.indstatus = 'A' 
			and iu.estuf = '%s'
			and pt.inuid = %d
			and pt.ptostatus = '%s'
		group by
			ins.itrid,
			d.dimcod,
			d.dimdsc,
			c.ctrpontuacao,
			d.dimid
	", 
			$_SESSION['itrid']				
			,$_SESSION['inuid']				
			,$_SESSION['estuf']
			,$_SESSION['inuid']	,
			$ptostatus			
	);

	if( $dadosSD = $db->carregar($sqlSD) )
	{
		//percorrendo o resultado e criando um array por dimensao 
		foreach($dadosSD as $keySD => $valSD )
		{
			$corSD = $icone ? '#959595' : '#133368';
			$relatorioSD[$valSD["dimid"]]["dimdsc"] = $valSD["dimdsc"];
			switch($valSD["ctrpontuacao"])
			{
				case "0":
					$relatorioSD[$valSD["dimid"]]["0"] = $valSD["qtpontos"];
					break;
				case "1":
					$relatorioSD[$valSD["dimid"]]["1"] = $valSD["qtpontos"];
					break;
				case "2":
					$relatorioSD[$valSD["dimid"]]["2"] = $valSD["qtpontos"];
					break;
				case "3":
					$relatorioSD[$valSD["dimid"]]["3"] = $valSD["qtpontos"];
					break;
				case "4":
					$relatorioSD[$valSD["dimid"]]["4"] = $valSD["qtpontos"];
					break;
			}
		}
	}
	
	$totalSD["t0"] = 0;
	$totalSD["t1"] = 0;
	$totalSD["t2"] = 0;
	$totalSD["t3"] = 0;
	$totalSD["t4"] = 0;
	$corSD = '#e7e7e7';

// ----- FIM CARREGA SINTESE POR DIMENS�O --------------------------------------

// ----- RECUPERA NOME DO ESTADO -----------------------------------------------
	$sqliuf = "select * from territorios.estado where estuf = '" 
				. $_SESSION["estuf"] . "'";
	$dadosuf = $db->recuperar( $sqliuf );
	if ( is_array( $dadosuf ) )
	{
		$estdescricao = $dadosuf['estdescricao'];
	}
	$estdescricao = $estdescricao == "null" ? "" : $estdescricao;
// ----- FIM RECUPERA NOME DO ESTADO -------------------------------------------

?>

<LINK rel="stylesheet" type="text/css" media="print, handheld" href="../includes/print.css">
<style type="text/css">
<!--
	body
	{
		font-family: sans-serif;
	}
	#geral
	{
		margin: 10px 10px 10px 10px;		
	}
	#geral #capa 
	{
		font: bold;
		text-align: center;
		vertical-align: middle;
	}
	#geral #capa .topo 
	{
		height: 9cm;
		font-size: 18px;
	}
	#geral #capa .meio
	{
		height: 14cm;
		font-size: 20px;
	}
	#geral #capa .rodape
	{
		height: 1cm;
		font-size: 14px;
	}
	#geral #conteudo{}
	#geral #conteudo h1
	{
		font-size: 18px;
		margin-left: 15px;
		list-style: decimal;
		margin-bottom: 30px;
	}
	#geral #conteudo h2
	{
		font-size: 16px;
		margin-left: 30px;
	}
	#geral #conteudo .texto
	{
		font-size: 12px;
		text-align: justify;
	}
	#geral #conteudo .texto ul
	{
		line-height: 30px;
		list-style: disc;
		margin-left: 40px;
		font-size: 12px;
	}
	#geral #conteudo p
	{
		font-size: 12px;
		text-indent: 60px;
		white-space: normal;
		line-height: 30px;
	}
	#geral #conteudo .textoTabela
	{
		font-size: 12px;
	}
	#geral #conteudo .tabela
	{
		font-size: 12px;
		width: 100%;
	}
	#geral #conteudo p
	{
		margin: 10px 10px 10px 10px;
	}
	#relatorioPar tr td
	{
		font-size: 10px;
	}
-->
</style>
<div id="geral">
	<div id="capa">
		<div style="display: none;"></div>
		<div class="topo">
			<p>GOVERNO DO ESTADO<br /><br />DE<br /><br /><a href="?modulo=principal/estrutura_avaliacao&acao=A"><?php echo $estdescricao; ?></a></p>
		</div>
		<div class="meio">
			<p>PLANO DE METAS COMPROMISSO TODOS PELA EDUCA��O
			<br /><br />PLANO DE A��ES ARTICULADAS - PAR</p>
		</div>
		<div class="rodape">
			<p>2007</p>
		</div>
	</div>
	<div id="conteudo" style="page-break-before: always;">
		<ul>
		<center><h1>Equipe envolvida na elabora&ccedil;&atilde;o</h1></center>
		<div class="texto" style="page-break-after: always;">
			<p><?php echo $inueqploc ?></p> 
		</div>
		<center><h1 style="margin-top: 150px;">Apresenta&ccedil;�o</h1></center>
		<div class="texto" style="page-break-after: always;">
			<p>
			No momento da assinatura do Termo de Ades&atilde;o ao &nbsp;&nbsp; <i>Plano de Metas 
			Compromisso	Todos pela Educa&ccedil;&atilde;o</i>, assumimos o compromisso de 
			melhorar nossos indicadores educacionais a partir do desenvolvimento de 
			a&ccedil;&otilde;es que possibilitem o cumprimento das diretrizes 
			estabelecidas no referido Termo de Ades&atilde;o e tamb&eacute;m o alcance 
			das metas estabelecidas para o IDEB.
			</p>
			<p>
			Visando promover a melhoria da qualidade da Educa&ccedil;&atilde;o 
			B&aacute;sica oferecida neste Estado nos propomos a cumprir integralmente as 
			a&ccedil;&otilde;es propostas no presente Plano de A&ccedil;&otilde;es 
			Articuladas - PAR e, com a mesma responsabilidade estabeleceremos, em 
			parceria com o MEC, sistemas de acompanhamento e avalia&ccedil;&atilde;o das 
			a&ccedil;&otilde;es a serem desenvolvidas, al&eacute;m disso, &eacute; nosso 
			compromisso	divulgar a evolu&ccedil;&atilde;o dos dados educacionais 
			&agrave; popula&ccedil;&atilde;o local e estimula-la a participar e promover 
			o controle social de todas as a&ccedil;&otilde;es propostas neste documento.
			</p>
			<p>
			Nossa sugest&atilde;o de PAR concentra-se na melhoria gradativa dos 
			resultados educacionais e tem o aluno como o centro de todas as 
			decis&otilde;es. Assim, o seu principal objetivo &eacute; contribuir para o 
			desenvolvimento de aprendizagens, habilidades e compet&ecirc;ncias, atitudes 
			e valores necess&aacute;rios para a sua forma&ccedil;&atilde;o integral.
			</p>
			<p>
			A seguir apresentamos o PAR elaborado a partir do diagn&oacute;stico 
			realizado pela equipe da Secretaria de Estado de Educa&ccedil;&atilde;o em 
			conjunto com especialistas do MEC nos dias ____________ do m�s de 
			____________________ de ______________.
			</p>
			<p align="right">
			<br /><br /><br /><br />
			      _______________________________________
			<br />Secret&aacute;rio (a) de Estado de Educa&ccedil;&atilde;o
			</p>
		</div>
		<h1><li>Dados B�sicos de identifica��o</li></h1>
		<h2>1.1 Do Estado</h2>
		<div class="texto">
			<p><?php echo $inuddsdmgr ?></p>
		</div>
		<h2>1.2 Do Governo Estadual</h2>
		<div class="texto">
			<p><?php echo $inucadsec ?></p>
		</div>
		<h2>1.3 Comit� Local "Plano de Metas Compromisso Todos pela Educa��o</h2>
		<div class="texto" style="page-break-after: always;">
			<p><?php echo $inucadcmt ?></p>
		</div>
		<h1><li>Resultados do Diagn�stico <i>in loco</i></li></h1>
		<div class="texto">
			<p>Neste item agregam-se as planilhas resultantes dos quadros de&nbsp;&nbsp; 
			 <strong>Sistematiza��o dos Crit�rios de Pontua��o</strong> e <strong>Sistematiza��o 
			Geral por Dimens�o</strong> da Parte III do Instrumento do Campo, 
			conforme segue:</p>
		</div>
		<div class="tabela">
			<?php if($dadossi):?>
				<table border="0" class="textoTabela">
					<tr bgcolor="grey"> 
						<th colspan="6" align="left" style="color: white;">Quadro 1. S�ntese dos conceitos gerados por dimens�o e respectivos indicadores</th>
					</tr>
					<?php foreach( $dadossi as $key => $val ): ?>
					<?php if($key == 0 or $val["dimcod"] != $dadossi[$key - 1]["dimcod"]):?>
					<tr bgcolor="grey"> 
						<th colspan="6" align="left"><?php echo $val["dimcod"] . '. ' . $val["dimdsc"];?></th>
					</tr>
					<?php endif;?>
					<?php if($key == 0 or $val["ardcod"] != $dadossi[$key - 1]["ardcod"]):?>
					<tr bgcolor="#cccccc"> 
						<td></td>
						<th colspan="5" align="left"><?php echo $val["ardcod"] . '. ' . $val["arddsc"];?></th>
					</tr>
					<tr bgcolor="#cccccc"> 
						<td></td>
						<td></td>
						<th width="25">indicador</th>
						<th width="25">pontua&ccedil;&atilde;o</th>
						<th>justificativa</th>
						<th>demanda potencial</th>
					</tr>
					<?php $corSD = '#dfdfdf'; ?>
					<?php endif;?>
					<tr bgcolor="<?php echo $corSD; ?>" onmouseover="this.bgColor='#ffffcc';" onmouseout="this.bgColor='<?=$corSD?>';"> 
						<td></td>
						<td></td>
						<td align="center"><?php echo $val["indcod"];?>&nbsp;</td>
						<td align="center"><?php echo $val["ctrpontuacao"];?>&nbsp;</td>
						<td><?php echo $val["ptojustificativa"];?>&nbsp;</td>
						<td>
								<?php print(trim($val["ptodemandaestadual"]) == "")?"":"<p><strong>Estadual:</strong> ".trim($val["ptodemandaestadual"]) . "</p>" ;?>
								<?php print(trim($val["ptodemandamunicipal"]) == "")?"":"<p><strong>Municipal:</strong> ".trim($val["ptodemandamunicipal"]) . "</p>";?>
						</td>
					</tr>
					<?php if($corSD == '#dfdfdf') $corSD = '#ffffff'; else $corSD = '#dfdfdf'; ?>
					<?php endforeach; ?>
				</table>
			<?php endif; ?>
		</div>
		<div class="texto">
			<p>A partir da sistematiza��o acima, a totaliza��o da pontua��o por
			dimens�o ficou distribu�da conforme Quadro 2.</p>
		</div>
		<div class="tabela" style="page-break-after: always;">
			<?php if( isset($relatorioSD)): ?>
				<table border="0" class="textoTabela" width="95%">
					<thead>
					<tr bgcolor="grey">
						<th colspan="6" align="left" style="color: white;">Quadro 2. Total da pontua��o gerada por dimens�o</th>
					</tr>
					<tr bgcolor="grey">
						<th rowspan="2">Dimens�o</th>
						<th colspan="5">Pontua��o</th>
					</tr>
					<tr bgcolor="grey">
						<th>4</th>
						<th>3</th>
						<th>2</th>
						<th>1</th>
						<th>n/a</th>
					</tr>
					</thead>
					<?php foreach($relatorioSD as $keyr => $valr ): ?>
						<tr bgcolor="<?=$corSD?>" onmouseover="this.bgColor='#ffffcc';" onmouseout="this.bgColor='<?=$corSD?>';">
							<td><?php echo $valr["dimdsc"]; ?></td>
							<td align="right"><?php echo (int)$valr["4"]; ?>&nbsp;</td>
							<td align="right"><?php echo (int)$valr["3"]; ?>&nbsp;</td>
							<td align="right"><?php echo (int)$valr["2"]; ?>&nbsp;</td>
							<td align="right"><?php echo (int)$valr["1"]; ?>&nbsp;</td>
							<td align="right"><?php echo (int)$valr["0"]; ?>&nbsp;</td>
						</tr>
						<?php 
							$totalSD["t0"] += (int)$valr["0"];
							$totalSD["t1"] += (int)$valr["1"];
							$totalSD["t2"] += (int)$valr["2"];
							$totalSD["t3"] += (int)$valr["3"];
							$totalSD["t4"] += (int)$valr["4"];
							if($corSD == '#e7e7e7') $corSD = '#ffffff'; else $corSD = '#e7e7e7';
						?>
					<?php endforeach; ?>
						<tr onmouseover="this.bgColor='#ffffcc';" onmouseout="this.bgColor='#FFFFFF';">
							<td align="right"><strong>Total:</strong></td>
							<td align="right"><strong><?php echo $totalSD["t4"]; ?>&nbsp;</strong></td>
							<td align="right"><strong><?php echo $totalSD["t3"]; ?>&nbsp;</strong></td>
							<td align="right"><strong><?php echo $totalSD["t2"]; ?>&nbsp;</strong></td>
							<td align="right"><strong><?php echo $totalSD["t1"]; ?>&nbsp;</strong></td>
							<td align="right"><strong><?php echo $totalSD["t0"]; ?>&nbsp;</strong></td>
						</tr>
						<tfoot>
						<tr >
								<td colspan="6" align="right">*n/a :  N�o se Aplica.</td>
						</tr>
						<tfoot>
				</table>
			<?php endif; ?>
		</div>
		<h1><li>Defini��o das a��es que far�o parte do Plano de A��es Articuladas - PAR</li></h1>
		<h2>3.1 Detalhamento das a��es</h2>
		<div class="texto" style="page-break-after: always;">
			<p>
			Com base nas informa��es acima, � feito o detalhamento das a��es do 
			PAR para o per�odo de quatro anos e o cronograma financeiro para o 
			primeiro ano. O detalhamento compreende:
			</p>
			<p>
			<ul>
				<li>
					especifica��o da a��o e suba��o o que corresponde a 
					identifica��o dos diferentes e principais insumos 
					necess�rios � sua execu��o;
				</li>
				<li>
					estrat�gia de implementa��o, ou seja, como a a��o/suba��o 
					ser� executada;
				</li>
				<li>
					estimativa do per�odo de realiza��o, com as datas de seu 
					in�cio e t�rmino;
				</li>
				<li>
					nomea��o do respons�vel, na SEE, pela execu��o da a��o;
				</li>
				<li>
					resultados esperados;
				</li>
				<li>
					unidade de medida a ser usada como refer�ncia b�sica para o
					dimensionamento f�sico e financeiro;
				</li>
				<li>
					quantidade anual, prevista para cada ano do per�odo de
					planejamento de 4 anos;
				</li>
				<li>
					custo unit�rio estimado e custos anuais;
				</li>
				<li>
					origem dos recursos - assinalar a forma e identificar o 
					programa do MEC que poder� apoiar t�cnica ou financeiramente 
					a a��o.
				</li>
			</ul>
			</p>
			<p>
			Os resultados do detalhamento das a��es e seus respectivos 
			cronogramas devem ser registrados nos quadros a seguir:
			</p>
			<p style="text-indent: 0px;">
			<strong>Quadro 3.</strong> propostas de desenvolvimento de a��es para a 
			Dimens�o 1. Gest�o Educacional, para o per�odo de quatro anos;<br />
			<strong>Quadro 4.</strong> propostas de desenvolvimento de a��es para a 
			Dimens�o 2. Forma��o de Professores e Profissionais de Apoio e 
			Servi�o Escolar, para o per�odo de quatro anos;<br />
			<strong>Quadro 5.</strong> propostas de desenvolvimento de a��es para a 
			Dimens�o 3. Pr�ticas Pedag�gicas e Avalia��o, para o per�odo de 
			quatro anos;<br />
			<strong>Quadro 6.</strong> propostas de desenvolvimento de a��es para a 
			Dimens�o 4. Infra-estrutura f�sica e Recursos Pedag�gicos, para o 
			per�odo de quatro anos;
			</p>
		</div>
		<div class="tabela" style="page-break-after: always;">
			<?php $cabecalhoImprecao = false; ?>
			<?php require_once('impressao.inc'); ?>
		</div>
		<h1><li>Acompanhamento e Avalia��o</li></h1>
		<div class="texto">
			<p>
			Durante a execu��o do PAR, o acompanhamento sistem�tico das a��es � 
			fundamental e contar�, quando necess�rio, com o apoio da equipe 
			t�cnica do MEC.
			</p>
			<p>
			A avalia��o do processo de implementa��o do plano ser� realizada 
			continuamente e dever� captar em que medida as estrat�gias e op��es 
			metodol�gicas utilizadas no desenvolvimento das a��es s�o adequadas 
			para concretizar os objetivos propostos.
			</p>
			<p>
			Assim, periodicamente ser�o enviados ao MEC relat�rios com a 
			explicita��o, estruturada, de todos os procedimentos de 
			acompanhamento e avalia��o sistem�tica da execu��o do plano e dos 
			resultados alcan�ados.
			</p>
		</div>
		<!-- GAMB PRA MANTER A NUMERA��O DO TOPICO CONSIDERA��ES COMO 6 -->
		<li style="color: white;"></li>
		<!-- FIM GAMB -->
		<h1><li>Considera��es Finais</li></h1>
		<div class="texto">
			<p>
			A constru��o deste Plano de A��es Articuladas significa um grande 
			avan�o, pois representa um plano do Estado para a melhoria da 
			Educa��o B�sica.
			</p>
			<p>
			A implanta��o deste plano depende n�o somente da mobiliza��o e 
			vontade pol�tica, mas tamb�m de mecanismos e instrumentos de 
			acompanhamento e avalia��o das diversas a��es a serem desenvolvidas.�
			Neste sentido, a Secretaria de Estado de Educa��o ser� a respons�vel 
			pela coordena��o do processo de implanta��o e consolida��o do PAR.  
			Al�m dela, desempenhar� tamb�m um papel essencial nessas fun��es o 
			Comit� Local do Compromisso Todos pela Educa��o e a sociedade civil 
			organizada.
			</p>
			<p>
			Diante do exposto solicitamos an�lise da Comiss�o T�cnica do 
			Minist�rio da Educa��o e devidas provid�ncias para o desenvolvimento 
			das a��es propostas.
			</p>
			<p align="right">
			<br /><br /><br /><br /><br /><br />
			____________________________________<br />
			Secret�rio (a) de Estado de Educa��o 
			</p>
		</div>
		</ul>
	</div>
</div>