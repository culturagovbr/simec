<?php
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';
$db->cria_aba( $abacod_tela, $url, '' );
cte_montaTitulo( $titulo_modulo, '&nbsp;');

?>
<style>
 ul{
 	list-style-type:decimal-leading-zero;
 }
 
 li{
 	font-size: 12px;
 	font-family: arial;
 	font-style: normal;
 	
 }
 
 table.tabela2{
 	background-color: white;
 }
</style>
<table align="center" border="0" class="tabela tabela2" cellpadding="3" cellspacing="1">
	<tr>
		<th>Lista de munic�pios</th>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<th>Lista de Estados</th>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<th>Tela de Pontua��o</th>
	</tr>
	<tr>
		<td>
			<ul>
				<li>Se for estado ele pode ou n�o seguir o quia. O munic�pio e obrigado a sequir o que est� no guia do PAR.</li>
				<li>Se a pontua��o for 3 ou 4 abre apenas para o Administrador o bot�o de proposta de suba��es onde ele pode inserir a��es e suba��oes propostas no quia.</li>
			</ul>
		</td>
	</tr>
	<tr>
		<th>Tela de Suba��oes</th>
	</tr>
	<tr>
		<td>
			<ul>
				<li>Se a suba��o / ano estiver conveniada ent�o ela fica travada apenas no ano que foi conv�niada e o bot�o de inserir adtivos abre.</li>
				<li>Se o ano for menor que o ano corrente ela trava - So fica liberada para Administradores e Super Usu�rio.</li>
				<li>Se a suba��o / ano estiver com o status do parecer lan�ado e o munic�pio estiver na fase de elabora��o a sub��o fica travado.</li>
				<li>Se a suba��o for de assist�ncia financeira e n�o tiver nada preenchido ent�o n�o h� pend�ncia em nenhuma fase.</li>
				<li>Se a suba��o for de assist�ncia t�cnica Pelo menos em um ano t�m que ter dados lan�ados se n�o da erro de pendencia em qualquer fase. </li>
			</ul>
		</td>
	</tr>
	<tr>
		<th>Regras para gera��o de termo de coopera��o e pareceres</th>
	</tr>
	<tr>
		<td>
			<ul>
				<li>Apenas administradores podem gerar o termo e o parecer.</li>
			</ul>
		</td>
	</tr>
</table>
