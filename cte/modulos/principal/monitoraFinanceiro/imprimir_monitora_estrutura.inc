<?
/************************* INCLUDES **********************************/
include_once(APPRAIZ.'www/cte/monitoraFinanceiro/includes/constantes.php');
include_once(APPRAIZ.'includes/classes/dateTime.inc');
include_once(APPRAIZ.'cte/modulos/principal/monitoraFinanceiro/funcoes.inc');
include_once(APPRAIZ .'cte/classes/HistoricoMonitoramentoDadosBancarios.class.inc');
include_once(APPRAIZ .'cte/classes/HistoricoMonitoramentoConvenio.class.inc');
include_once(APPRAIZ .'cte/classes/HistoricoMonitoramentoConvSubac.class.inc');
include_once(APPRAIZ .'cte/classes/HistoricoConvItemComposicao.class.inc');

/************************* FUN��ES AJAX **********************************/

/************************* DECLARA��O DE VARIAVEIS *************************/
global  $db;
$dataFormatada 	= new Data();
$inuid			= $_SESSION['inuid'];
$prsid			= $_REQUEST['prsid'];
$hmcid			= $_REQUEST['hmcid'];
$hmdid			= $_REQUEST['hmdid'];
$obconvenio		= new HistoricoMonitoramentoConvenio();

if(!$hmcid){ // Se iniciou o monitoramento carrega o id dele.
	$sql = "SELECT hmcid
			FROM cte.historicomonitoramentoconvenio hc
			INNER JOIN cte.historicomonitoramentodadosbancarios hm ON hm.hmdid = hc.hmdid
			WHERE hc.prsid =".$prsid." and hm.hmdid = ".$hmdid;
	$hmcid = $db->pegaUm($sql);
	$_REQUEST['hmcid'] = $hmcid;
}

/************************* CARREGA MONITORAMENTO DE ACORDO COM O M�S. *************************/
$sql = "SELECT 	hm.hmdmes, 
				hm.hmdano,
				hm.hmdid, 
				hc.hmcstatus,
				ps.prsnumconvsape,
				CASE WHEN hc.hmcstatus = 'I' THEN 'Em andamento.' ELSE 'Finalizado.' END AS hmcstatusdescricao
		FROM cte.historicomonitoramentoconvenio hc 
		INNER JOIN cte.historicomonitoramentodadosbancarios hm ON hm.hmdid = hc.hmdid
		INNER JOIN cte.projetosape ps ON ps.prsid = hc.prsid
		WHERE hc.hmcid = ".$hmcid." AND hc.prsid = ".$prsid;
$monitoraMes 	= $db->pegaLinha($sql);
$mes 		 	= $monitoraMes['hmdmes'];
$ano 		 	= $monitoraMes['hmdano'];
$hmdid 		 	= $monitoraMes['hmdid'];
$status 	 	= $monitoraMes['hmcstatus'];
$numConvenio 	= $monitoraMes['prsnumconvsape'];
$descStatus  	= $monitoraMes['hmcstatusdescricao'];
$display 	 	= '';
$tudoMonitorado = false;

if($mes != 10 && $mes != 11 && $mes != 12){
	$mes = '0'.$mes;
}

if($status == 'I'){ // Caso iniciado o monitoramento carrega o Btn de Finalizar o monitoramento. 
	$style = '';
}else if($status == 'F'){
	$display = 'style="display:none;"';
}

$mesTexto = $dataFormatada->mesTextual($mes);

/************************* CABE�ALHO E T�TULO ******************************/
$url = "?modulo=principal/monitoraFinanceiro/estrutura_mes&acao=A&anoreferencia=".$_SESSION['ano'];
cteMontaTituloFinanceiro( 'Monitoramento Financeiro do PAR', '&nbsp;', $url );

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../../cte/monitoraFinanceiro/includes/monitora_financeiro.css"/>
	<link rel="stylesheet" type="text/css" href="../../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../../includes/listagem.css"/>
	<script type="text/javascript" src="../../includes/funcoes.js"></script>
	<script type="text/javascript" src="../../includes/prototype.js"></script>
</head>
<body>
		<!-- CARREGANDO DADOS AJAX -->
		<div id="loader-container" style="display:none;">
	    	<div id="loader">
	    		<img src="../../imagens/wait.gif" border="0" align="middle">
	    		<span>Aguarde! Carregando Dados...</span>
	    	</div>
	    </div>
	    <!-- CARREGA DADOS DO MONITORAMENTO -->
	    <table align="center" border="0" class="tabela tabelaMonitoramento" cellpadding="3" cellspacing="1" width="95%">
			<tr>
				<td  class="tdTopo" style="text-align: left;" >Monitoramento do m�s: <?=$mesTexto; ?> / <?=$ano; ?> do conv�nio n�: <?=$numConvenio;?>  </td>
			</tr>
			<tr>
				<td width="170px;" class="tdTopo" style="text-align: left;" >Status deste monitoramento: <?=$descStatus;?></td>
			</tr>
		</table>
		<br>
		<!-- LISTA DE HIST�RICO DE MONITORAMENTO -->
		<table align="center" border="0" class="tabela" cellpadding="3" cellspacing="1">
			<tr>
				<th>Hist�rico de Monitoramentos</th>
			</tr>
		</table>
		<table align="center" width="100%" >
			<tr>
				<td id="listaMonitoramentos">
				<?php 	
					$obHistorico = new HistoricoMonitoramentoConvenio();
					$obHistorico->listaHistoricoMonitoramento($inuid,$_SESSION['ano'], $prsid, true);  
				?>
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					<input type="button" value="imprimir" onClick="javascript:window.print()" id="botao" />
				</td>
			</tr>			
		</table>
		<script type="text/javascript">

		/**
		 * function erro(codigo);
		 * descscricao 	: Mostra erros para o usu�rio (Regras de Neg�cio).
		 * author 		: Thiago Tasca Barbosa
		 * parametros 	: codigo 
		 */
		function erro(codigo){
			var ERRONAOINICIADA = 1;
			var ERRONAOEXECUTADAOUEMEXECUCAO = 2;
			var MONITORAMENTONAOINICIADA = 3;
			
			if(codigo == ERRONAOINICIADA){ // Monitoramento n�o iniciado.
				alert("O monitoramento desta suba��o n�o foi iniciada. Cadastre o status da suba��o.");
			}
			if(codigo == ERRONAOEXECUTADAOUEMEXECUCAO){ // Suba��o n�o aberta para monitoramento.
				alert("Item j� monitorado.");
			}
			if(codigo == MONITORAMENTONAOINICIADA){ // Suba��o n�o aberta para monitoramento.
				alert("Inicie o monitoramento para poder monitorar as sub-a��es e os itens de composi��o.");
			}
			if(codigo == 4){ // Suba��o n�o aberta para monitoramento.
				alert("A suba��o est� com status de Pendente. \n Monitore a suba��o para poder iniciar o monitoramento dos itens da pr�pria suba��o.");
			}
			if(codigo == 5){ // monitoramento do mes encerrado.
				alert("Monitorado deste m�s j� foi finalizado. \n n�o � possivel editar a suba��o.");
			}
			if(codigo == 6){ // monitoramento do mes encerrado.
				alert("Monitorado deste m�s j� foi finalizado. \n n�o � possivel editar o item de composi��o.");
			}
			
		}
		
		/**
		 * function inicialytebox(url, title );
		 * descscricao 	: Fun��o que faz o lytebox rodar com o ajax. carrega as telas de formulario.
		 * author 		: Thiago Tasca Barbosa
		 * parametros 	: url (caminho)
		 * parametros 	: title (titulo do campo)
		 */
		function inicialytebox(url,title,width,height  ) {
		      var anchor = this.document.createElement('a'); // cria um elemento de link
		      anchor.setAttribute('rev', 'width: '+width+'px; height: '+height+'px; scrolling: auto;');
		      anchor.setAttribute('title', '');
		      anchor.setAttribute('href', url);
		      anchor.setAttribute('rel', 'lyteframe');
		      // paramentros para do lytebox //
		      myLytebox.maxOpacity = 50;
		      myLytebox.filter = 10;
		      myLytebox.outerBorder = true;
		      myLytebox.doAnimations = false;
		      myLytebox.start(anchor, false, true);
		      document.getElementById("lbIframe").frameBorder='0';
		      //tooltipWindow
		      return false;
		}
			/**
			 * function voltarListaMeses();
			 * descscricao 	: Volta para tela de meses do conv�nio. 
			 * author 		: Thiago Tasca Barbosa
			 */
			function voltarListaMeses(){
				var url 	= '?modulo=principal/monitoraFinanceiro/estrutura_mes&acao=A&anoreferencia='+<?=$_SESSION['ano']; ?>;
				window.location = url;
			}
			
			/**
			 * function carregaSubacoes(convenio, hmcid);
			 * descscricao 	: Carrega lista de suba��es quando clicado no btn + do conv�nio. 
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: convenio (id do conv�nio)
			 * parametros  	: ssuid (id do status da suba��o)
			 */
			function carregaSubacoes(convenio, hmcid){
				$('loader-container').show();
				return new Ajax.Request(window.location.href,{	
					method: 'post',
					parameters: '&requisicao=carregasubacao&prsid='+convenio+'&hmcid='+hmcid,
					asynchronous: false,
					onComplete: function(resposta)
					{	
						$('img_convenios').innerHTML = '<img id="mais" onclick="enableSubacoes();" name="mais" src="../../imagens/mais.gif" style="display: none;"/><img id="menos" onclick="disableSubacoes();" name="menos" style="" src="../../imagens/menos.gif"/>'
						$('listaAcoes').style.display = '';
						$('linha').style.display = '';
						$('listaAcoes').innerHTML = resposta.responseText;
						$('menos').style.display = '';
						$('mais').style.display = 'none';
						$('loader-container').hide();	
					}
				});
			}
			
			/**
			 * function enableSubacoes();
			 * descscricao 	: Habilita suba��es. Fun��o que troca a fun��o do bot�o + por - e mostra a lista de suba��es. 
			 *				  Fun��o utilizado para que os dados j� carregado n�o seja carregado novamente evitanto outra requisi��o no banco, 
			 *				  apenas habilitar dados existentes com css.
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: convenio (id do conv�nio)
			 */
			function enableSubacoes(){
				$('menos').style.display = '';
				$('linha').style.display = '';
				$('mais').style.display = 'none';
				$('listaAcoes').style.display = '';
			}
			
			/**
			 * function disableSubacoes();
			 * descscricao 	: Desabilita suba��es. Fun��o que troca o o bot�o - por + e esconde a lista de suba��es.
			 *				  Fun��o utilizado para que os dados j� carregado n�o seja carregado novamente evitanto outra requisi��o no banco, 
			 *				  apenas Desabilitar dados existentes com css.
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: convenio (id do conv�nio)
			 */
			function disableSubacoes(){
				$('menos').style.display = 'none';
				$('linha').style.display = 'none';
				$('mais').style.display = '';
				$('listaAcoes').style.display = 'none';
				
			}
			
			/**
			 * function carregaTelaStatusSubAcao();
			 * descscricao 	: Carrega tela de Suba��o
			 * author 		: Thiago Tasca Barbosa
			 */
			function carregaTelaStatusSubAcao(sbaid, ssuid, prsid,hmsid, ano, hmcid){
				inicialytebox('cte.php?modulo=principal/monitoraFinanceiro/monitora_SubAcao&acao=A&requisicao=carregar&sbaid='+sbaid+'&ssuid='+ssuid+'&prsid='+prsid+'&hmsid='+hmsid+'&ano='+ano+'&hmcid='+hmcid,'Monitora Status Suba��o','800','250');
				return true;
			}
			
			/**
			 * function closeLyteboxSubAcao();
			 * descscricao 	: Fecha tela de suba��o
			 * author 		: Thiago Tasca Barbosa
			 */
			function closeLyteboxSubAcao(sbaidPrsid,sbaid, ssuid,prsid, hmsid,sucesso, descStatusSub, ano,hmcid){
				if(sucesso == "ok"){
					$('subStatus_'+sbaidPrsid).innerHTML ='<img title="Sub-a��o monitorada" id="imgsubStatus_'+sbaidPrsid+'" align="absmiddle"  src="../../imagens/check_p.gif" width="18" height="18">';
					$('subStatusDesc_'+sbaidPrsid).innerHTML = descStatusSub;
					$('img_subacoes_'+sbaidPrsid).innerHTML = '<img id="mais_'+sbaidPrsid+'" onclick="carregaItensComposicao('+sbaid+', '+<?=$_SESSION['ano'] ?>+','+ssuid+','+prsid+','+hmsid+','+hmcid+' );" name="mais_'+sbaidPrsid+'" src="../../imagens/mais.gif"/>';
					atualizaQtdItensPendentes(sbaid, ano, hmcid);
					carregaItensComposicao(sbaid, ano, ssuid, prsid, hmsid, hmcid);
					if(window.addEventListener){ // Mozilla, Netscape, Firefox
						$('editarSub_'+sbaidPrsid).setAttribute( "onclick", 'carregaTelaStatusSubAcao('+sbaid+','+ssuid+','+prsid+','+hmsid+','+ano+','+hmcid+')' );
					} else { // IE
						$('editarSub_'+sbaidPrsid).attachEvent( "onclick", 'carregaTelaStatusSubAcao('+sbaid+','+ssuid+','+prsid+','+hmsid+','+ano+','+hmcid+')' );
					}
				}
				myLytebox.end(); 
				return true;
			}
			
			function atualizaQtdItensPendentes(sbaid, ano, hmcid){
				return new Ajax.Request(window.location.href,{	
					method: 'post',
					parameters: '&requisicao=atualizaqtditenspendentes&sbaid='+sbaid+'&ano='+ano+'&hmcid='+hmcid,
					onComplete: function(resposta)
					{	
						$('itensPendentes_'+sbaid+ano).innerHTML = resposta.responseText;				
					}
				});
			}
			
			/**
			 * function carregaItensComposicao(sbaid, ano, ssuidmonitora, prsid, hmsid);
			 * descscricao 	: Carrega lista de itens de composi��o por suba��o.
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: sbaid (id da suba��o)
			 * parametros 	: ano (ano referencia)
			 * parametros 	: ssuidmonitora (id do status da suba��o )
			 * parametros 	: prsid (id do conv�nio )
			 * parametros 	: hmsid (id do monitoramento da Suba��o )
			 * parametros 	: ssuidmonitora (id do status da suba��o )
			 */
			function carregaItensComposicao(sbaid, ano, ssuidmonitora, prsid, hmsid, hmcid, indicePai){
				sbaidPrsid = sbaid.toString()+prsid.toString()+ano.toString();
				$('loader-container').show();
				return new Ajax.Request(window.location.href,{	
					method: 'post',
					parameters: '&requisicao=carregaitens&sbaid='+sbaid+'&ssuid='+ssuidmonitora+'&prsid='+prsid+'&hmsid='+hmsid+'&ano='+ano+'&hmcid='+hmcid+'&indicePai='+indicePai,
					onComplete: function(resposta)
					{	
						$('img_subacoes_'+sbaidPrsid).innerHTML = '<img id="mais_'+sbaidPrsid+'" onclick="enableItensComposicao('+sbaidPrsid+');" name="mais_'+sbaidPrsid+'" src="../../imagens/mais.gif" style="display: none;"/><img id="menos_'+sbaidPrsid+'" onclick="disableItensComposicao('+sbaidPrsid+');" name="menos_'+sbaidPrsid+'" style="" src="../../imagens/menos.gif"/>'; // troca os onclick dos bot�es + e -.
						$('listaItensComposicao_'+sbaidPrsid).style.display = ''; 	// mostra a lista de itens de composi��o
						$('linha_'+sbaidPrsid).style.display = ''; 				    // mostra a lista de suba��es
						$('listaItensComposicao_'+sbaidPrsid).innerHTML = resposta.responseText;
						$('menos_'+sbaidPrsid).style.display = ''; 				    // mostra o bot�o -
						$('mais_'+sbaidPrsid).style.display = 'none'; 				// oculta o bot�o +
						$('loader-container').hide();	
						
					}
				});
			}
			
			/**
			 * function disableItensComposicao(sbaid);
			 * descscricao 	: habilita itens composi��o. Fun��o que troca o o bot�o - por + e esconde a lista de itens com css. 
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: sbaid (id da suba��o)
			 */
			function disableItensComposicao(sbaidPrsid){
				if($('menos_'+sbaidPrsid)){
					if($('menos_'+sbaidPrsid).style.display != 'none'){
						$('menos_'+sbaidPrsid).style.display = 'none';
					}
				}
					if($('linha_'+sbaidPrsid).style.display != 'none'){
						$('linha_'+sbaidPrsid).style.display = 'none';
					}
					if($('mais_'+sbaidPrsid).style.display != ''){
						$('mais_'+sbaidPrsid).style.display = '';
					}
					if($('listaItensComposicao_'+sbaidPrsid).style.display != 'none'){
						$('listaItensComposicao_'+sbaidPrsid).style.display = 'none';
					}
			}
			
			/**
			 * function enableItensComposicao(sbaid);
			 * descscricao 	: habilita itens composi��o. Fun��o que troca o o bot�o + por - e mostra a lista de itens com css. 
			 * author 		: Thiago Tasca Barbosa
			 * parametros 	: sbaid (id da suba��o)
			 */
			function enableItensComposicao(sbaidPrsid){
				$('menos_'+sbaidPrsid).style.display = '';
				$('linha_'+sbaidPrsid).style.display = '';
				$('mais_'+sbaidPrsid).style.display = 'none';
				$('listaItensComposicao_'+sbaidPrsid).style.display = '';
			}
			
			/**
			 * function closeLytebox(codigo);
			 * descscricao 	: Fecha a tela de edi��o do item de composi��o.
			 * author 		: Thiago Tasca Barbosa
			 */
			function closeItens(item, sbaid, hmsid, hciid, quant, valor,quantPaga ,valorUnitario, valorTotalPago, ano, hmcid, statusItem, arrTotalExecutado){
				// Atualiza os valores das colunas que referem-se ao grupo "Executados Acumulados"
				atualizaExecutadoAcumulado(item, arrTotalExecutado);

				// formatar valores
//				valorUnitario = valorUnitario+'00'; // Para poder mostrar o valor com ,00 quando fechar a tela de itens
//				valorTotalPago = valorTotalPago+'00';
				valorUnitario  = valorUnitario.toFixed(2); 
				valorTotalPago = valorTotalPago.toFixed(2);				
				valorUnitario  = mascaraglobal( "[###.]###,##", valorUnitario );
				valorTotalPago = mascaraglobal( "[###.]###,##", valorTotalPago );
				
				$('td_'+item).innerHTML = '<img title="Item monitorado" id="'+item+'" align="absmiddle"  src="../../imagens/check_p.gif" width="18" height="18">';
				$('tdPago1_'+item).innerHTML = quantPaga;
				$('tdPago2_'+item).innerHTML = 'R$ '+valorUnitario;
				$('tdPago3_'+item).innerHTML = 'R$ '+valorTotalPago;
				$('td_status_item_'+item).innerHTML = statusItem;
				if(window.addEventListener){ // Mozilla, Netscape, Firefox
					$('Editar_'+item).setAttribute( "onclick", 'inicialytebox(\'cte.php?modulo=principal/monitoraFinanceiro/monitora_itensComp&acao=A&cosid='+item+'&sbaid='+sbaid+'&hmsid='+hmsid+'&hciid='+hciid+'&valor='+valor+'&quant='+quant+'\',\'Monitora Itens Composi��o\',\'800\',\'550\')' );
				} else { // IE
					$('Editar_'+item).attachEvent( "onclick", 'inicialytebox(\'cte.php?modulo=principal/monitoraFinanceiro/monitora_itensComp&acao=A&cosid='+item+'&sbaid='+sbaid+'&hmsid='+hmsid+'&hciid='+hciid+'&valor='+valor+'&quant='+quant+'\',\'Monitora Itens Composi��o\',\'800\',\'550\')' );
				}
				if(sbaid && ano && hmcid){
					atualizaQtdItensPendentes(sbaid, ano, hmcid);
				}
				myLytebox.end(); 
				return true;
			}
			
			function atualizaExecutadoAcumulado(item, arrTotalExecutado){
				var vlrTot, 
					qtdTot,
					vlrNovo,
					qtdNovo,
					vlrAtual,
					qtdAtual;
				var d = document;
				var qtd = d.getElementById('td_qtd_executado_acumulado_' + item);			
				var vlr = d.getElementById('td_valor_executado_acumulado_' + item);	
				/**** 
				 *
				 * IN�CIO = Prepara valores vindos da p�gina pai
				 *	
				 ****/
				vlrNovo  = arrTotalExecutado.valorExecutadoNovo;
				vlrNovo  = vlrNovo != '' ? parseFloat( vlrNovo ) : 0;
				
				qtdNovo  = arrTotalExecutado.qtdExecutadoNovo;
				qtdNovo  = qtdNovo != '' ? parseFloat( qtdNovo ) : 0;
				
				vlrAtual = arrTotalExecutado.valorExecutadoAtual;
				vlrAtual = vlrAtual != '' ? parseFloat( vlrAtual ) : 0;
				
				qtdAtual = arrTotalExecutado.qtdExecutadoAtual;
				qtdAtual = qtdAtual != '' ? parseFloat( qtdAtual ) : 0;
				/**** 
				 *
				 * FIM = Prepara valores vindos da p�gina pai
				 *	
				 ****/
						
				// Prepara o valor total
				vlrTot = replaceAll( replaceAll( replaceAll( vlr.innerHTML, '.', ''), ',', '.'), ' ', '');
				vlrTot = vlrTot != '' ? parseFloat( vlrTot ) : 0;
				// Calcula o valor total
				vlrTot = parseFloat( (vlrTot - vlrAtual) + vlrNovo ).toFixed(2);
				
				// Prepara a quantidade total
				qtdTot = qtd.innerHTML;
				qtdTot = qtdTot != '' ? parseFloat( qtdTot ) : 0;
				// Calcula o quantidade total
				qtdTot = (qtdTot - qtdAtual) + qtdNovo; 
				
				vlr.innerHTML = mascaraglobal( '[###.]###,##', vlrTot );
				qtd.innerHTML = qtdTot;								
			}
			
			/**
			 * function finalizarmonitoramento(fimConvenio);
			 * parametros	: id do conv�nio.
			 * desccri��o 	: finaliza o conv�nnio. 
			 * author 		: Thiago Tasca Barbosa
			*/
			function finalizarmonitoramento(fimConvenio, hmcid){
				var cpf = <?=$_SESSION["usucpf"];?>; 
				var nome = "<?=$_SESSION["usunome"];?>"; 
				//var ano = "<?=$_SESSION['ano'];?>";
				var ano = "<?=$monitoraMes['hmdano'];?>";				
				var mes = "<?=$mesTexto;?>";
				$('loader-container').show();
				return new Ajax.Request(window.location.href,{	
						method: 'post',
						parameters: '&requisicao=podefinalizarmonitoramento&hmcid='+hmcid,
						onComplete: function(resposta)
						{	
							$('loader-container').hide();
							if(resposta.responseText == 1){
								if(confirm('O monitoramento do m�s de '+mes+' de '+ano+' ser� finalizado agora. Deseja realmente prosseguir?')){
									if(confirm('Eu, '+nome+', CPF '+cpf+', desejo finalizar o monitoramento do m�s de '+mes+' de '+ano+'? \n Estou ciente de que, ap�s a finaliza��o, os dados n�o podem ser alterados e de que as  informa��es inseridas no sistema s�o de inteira responsabilidade do convenente.  Certifico a veracidade das informa��es inseridas.')){
										$('loader-container').show();
										return new Ajax.Request(window.location.href,{	
											method: 'post',
											parameters: '&requisicao=finalizarmonitoramento&hmcid='+hmcid,
											onComplete: function(resposta)
											{	
												alert(resposta.responseText);
												if( resposta.responseText == "Monitoramento encerrado com sucesso."){
													$('finalizarMonitora_'+fimConvenio).style.display = 'none';
													$('tdStatusMonitoramento').innerHTML = 'Finalizado.'; 
													carregaSubacoes(fimConvenio, hmcid);
													atualizaListaHistoricoMonitoramento(fimConvenio);
												}
												$('loader-container').hide();	
											}
										});
									}
								}
							}else{
								alert(resposta.responseText);
							}
						}
					});
			}
			
			/**
			 * function atualizaListaHistoricoMonitoramento(prsid);
			 * parametros	: id do conv�nio.
			 * desccri��o 	: Atualiza a lista de Monitoramento quando um monitoramento e iniciado e finalizado. 
			 * author 		: Thiago Tasca Barbosa
			*/
			function atualizaListaHistoricoMonitoramento(prsid){
				$('loader-container').show();
				return new Ajax.Request(window.location.href,{	
					method: 'post',
					parameters: '&requisicao=atualizaListaHistoricoMonitoramento&prsid='+prsid,
					onComplete: function(resposta)
					{				
						$('listaMonitoramentos').innerHTML = resposta.responseText;
						$('loader-container').hide();	
					}
				});
			}

			 function visualizaRelatorioHistoricoMonitoramento(prsid,hmcid){
					return windowOpen('cte.php?modulo=principal/monitoraFinanceiro/relatorio_historico_convenio&acao=A&prsid='+prsid+'&hmcid='+hmcid,
	                          'Hist�rico de monitoramento de conv�nio',
	                          'height=700,width=800,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes');
				}
				
			/* Fun��o para subustituir todos 
			 * descscricao 	: 
			 * author 		: Alexandre Dourado
			 * parametros 	: 
			 */
			function replaceAll(str, de, para){
				if ( str == ''){ return str; }
			    var pos = str.indexOf(de);
			    while (pos > -1){
					str = str.replace(de, para);
					pos = str.indexOf(de);
				}
			    return (str);
			}		

			function janelaImpressao(prsid, hmcid, hmdid) {
				return window.open('cte.php?modulo=principal/monitoraFinanceiro/imprimir_monitora_estrutura&acao=A&prsid='+prsid+'&hmcid='+hmcid+'&hmdid='+hmdid,'monitoraConvenio','height=500,width=800,status=no,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes');
			}
		</script>
	</body>
</html>
