<?php
//verifica_sessao();

if ( isset( $_REQUEST['buscar'] ) )
{
	include "geral_municipio_resultado.inc";
	exit();
}


$agrupadorHtml =
<<<EOF
	<table>
		<tr valign="middle">
			<td>
				<select id="{NOME_ORIGEM}" name="{NOME_ORIGEM}[]" multiple="multiple" size="4" style="width: 160px; height: 120px;" onDblClick="moveSelectedOptions( document.getElementById( '{NOME_ORIGEM}' ), document.getElementById( '{NOME_DESTINO}' ), true, '' );" class="combo campoEstilo"></select>
			</td>
			<td>
				<img src="../imagens/rarrow_one.gif" style="padding: 5px" onClick="moveSelectedOptions( document.getElementById( '{NOME_ORIGEM}' ), document.getElementById( '{NOME_DESTINO}' ), true, '' );"/><br/>
				<!--
				<img src="../imagens/rarrow_all.gif" style="padding: 5px" onClick="moveAllOptions( document.getElementById( '{NOME_ORIGEM}' ), document.getElementById( '{NOME_DESTINO}' ), true, '' );"/><br/>
				<img src="../imagens/larrow_all.gif" style="padding: 5px" onClick="moveAllOptions( document.getElementById( '{NOME_DESTINO}' ), document.getElementById( '{NOME_ORIGEM}' ), true, ''); sortSelect( document.getElementById( '{NOME_ORIGEM}' ) );"/><br/>
				-->
				<img src="../imagens/larrow_one.gif" style="padding: 5px" onClick="moveSelectedOptions( document.getElementById( '{NOME_DESTINO}' ), document.getElementById( '{NOME_ORIGEM}' ), true, '' ); sortSelect( document.getElementById( '{NOME_ORIGEM}' ) );"/><br/>
			</td>
			<td>
				<select id="{NOME_DESTINO}" name="{NOME_DESTINO}[]" multiple="multiple" size="4" style="width: 160px; height: 120px;" onDblClick="moveSelectedOptions( document.getElementById( '{NOME_DESTINO}' ), document.getElementById( '{NOME_ORIGEM}' ), true, '' ); sortSelect( document.getElementById( '{NOME_ORIGEM}' ) );" class="combo campoEstilo"></select>
			</td>
			<td>
				<img src="../imagens/uarrow.gif" style="padding: 5px" onClick="subir( document.getElementById( '{NOME_DESTINO}' ) );"/><br/>
				<img src="../imagens/darrow.gif" style="padding: 5px" onClick="descer( document.getElementById( '{NOME_DESTINO}' ) );"/><br/>
			</td>
		</tr>
	</table>
	<script type="text/javascript" language="javascript">
		limitarQuantidade( document.getElementById( '{NOME_DESTINO}' ), {QUANTIDADE_DESTINO} );
		limitarQuantidade( document.getElementById( '{NOME_ORIGEM}' ), {QUANTIDADE_ORIGEM} );
		{POVOAR_ORIGEM}
		{POVOAR_DESTINO}
		sortSelect( document.getElementById( '{NOME_ORIGEM}' ) );
	</script>
EOF;

include APPRAIZ . 'includes/Agrupador.php';
include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';
$db->cria_aba( $abacod_tela, $url, '' );
monta_titulo( "Relat�rio Geral", "" );
//monta_titulo_cte( $titulo_modulo );

?>
<script type="text/javascript">

	function exibirRelatorio()
	{
		var formulario = document.filtro;
		
		// verifica se fisico e/ou financeiro esta selecionado
		var fisico = document.getElementById( 'fisico' ).checked;
		var financeiro = document.getElementById( 'financeiro' ).checked;
		if ( !fisico && !financeiro )
		{
			alert( 'Escolha ao menos um dos tipos "F�sico" e "Financeiro".' );
			return;
		}
		
		// verifica se solicitado e/ou atendido esta selecionado
		var solicitado = document.getElementById( 'solicitado' ).checked;
		var atendido = document.getElementById( 'atendido' ).checked;
		if ( !solicitado && !atendido )
		{
			alert( 'Escolha ao menos um dos tipos "Solicitado" e "Atendido".' );
			return;
		}
		
		// verifica se tem algum agrupador selecionado
		agrupador = document.getElementById( 'agrupador' );
		if ( !agrupador.options.length )
		{
			alert( 'Escolha ao menos um item para agrupar o resultado.' );
			return;
		}
		
		selectAllOptions( agrupador );
		selectAllOptions( document.getElementById( 'ideb' ) );
		selectAllOptions( document.getElementById( 'regcod' ) );
		selectAllOptions( document.getElementById( 'estuf' ) );
		selectAllOptions( document.getElementById( 'muncod' ) );
		selectAllOptions( document.getElementById( 'dimid' ) );
		selectAllOptions( document.getElementById( 'ardid' ) );
		selectAllOptions( document.getElementById( 'indid' ) );
		selectAllOptions( document.getElementById( 'undid' ) );
		selectAllOptions( document.getElementById( 'plicod' ) );
		selectAllOptions( document.getElementById( 'prgid' ) );
//		selectAllOptions( document.getElementById( 'aciparecer' ) );
//		selectAllOptions( document.getElementById( 'psuid' ) );
//		selectAllOptions( document.getElementById( 'gpsid' ) );
		selectAllOptions( document.getElementById( 'ssuid' ) );
		selectAllOptions( document.getElementById( 'frmid' ) );
		selectAllOptions( document.getElementById( 'foaid' ) );
		
		// submete formulario
		//alert( 'blz!' ); return;
		formulario.target = 'resultadoCteGeral';
		var janela = window.open( '', 'resultadoCteGeral', 'width=780,height=465,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
		formulario.submit();
		janela.focus();
	}

</script>
<form action="" method="post" name="filtro">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">
		
		<!-- VALOR --------------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Valor
			</td>
			<td>
				<input type="checkbox" name="fisico" id="fisico" value="1"/>
				<label for="fisico">F�sico</label>
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" checked="checked" name="financeiro" id="financeiro" value="1"/>
				<label for="financeiro">Financeiro</label>
				<br/>
				<input type="checkbox" checked="checked" name="solicitado" id="solicitado" value="1"/>
				<label for="solicitado">Original</label>
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="atendido" id="atendido" value="1"/>
				<label for="atendido">Atual</label>
			</td>
		</tr>
		
		<!-- GRANDES CIDADE ------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Filtro por Tipo
			</td>
			<td>
				<input type="checkbox" name="grandescidades" id="grandescidades" value="1"/>
				<label for="grandescidades">Grandes Cidades</label>
			</td>
		</tr>
		
		<!-- AGRUPADOR ----------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top" width="20%">
				Agrupadores
			</td>
			<td width="80%">
				<?php
					
					// inicia agrupador
					$agrupador = new Agrupador( 'filtro', $agrupadorHtml );
					$destino = array();
					$origem = array(
						'dimensao' => array(
							'codigo'    => 'dimensao',
							'descricao' => 'Dimens�o'
						),
						'area' => array(
							'codigo'    => 'area',
							'descricao' => 'Area'
						),
						'indicador' => array(
							'codigo'    => 'indicador',
							'descricao' => 'Indicador'
						),
						'estado' => array(
							'codigo'    => 'estado',
							'descricao' => 'Estado'
						),
						'pais' => array(
							'codigo'    => 'pais',
							'descricao' => 'Pa�s'
						),
						'acao' => array(
							'codigo'    => 'acao',
							'descricao' => 'A��o'
						),
						'subacao' => array(
							'codigo'    => 'subacao',
							'descricao' => 'Sub-a��o'
						),
						'unidade' => array(
							'codigo'    => 'unidade',
							'descricao' => 'Unidade de Medida'
						),
						'execucao' => array(
							'codigo'    => 'execucao',
							'descricao' => 'Forma de Execu��o'
						),
						'planointerno' => array(
							'codigo'    => 'planointerno',
							'descricao' => 'Plano Interno'
						),
						'programa' => array(
							'codigo'    => 'programa',
							'descricao' => 'Programa'
						),
						'regiao' => array(
							'codigo'    => 'regiao',
							'descricao' => 'Regi�o'
						),
						'tipoparecersubacao' => array(
							'codigo'    => 'tipoparecersubacao',
							'descricao' => 'Tipo An�lise Suba��o'
						),
						'grupoparecersubacao' => array(
							'codigo'    => 'grupoparecersubacao',
							'descricao' => 'Grupo An�lise Suba��o'
						),
						'statussubacao' => array(
							'codigo'    => 'statussubacao',
							'descricao' => 'Status Suba��o'
						),
						'municipio' => array(
							'codigo'    => 'municipio',
							'descricao' => 'Munic�pio'
						),
						'ibge' => array(
							'codigo'    => 'ibge',
							'descricao' => 'C�digo do IBGE'
						),
						'sbastgmpl' => array(
							'codigo'    => 'sbastgmpl',
							'descricao' => 'Estrat�gia'
						)
					);
					
					// exibe agrupador
					$agrupador->setOrigem( 'naoAgrupador', null, $origem );
					$agrupador->setDestino( 'agrupador', null, $destino );
					$agrupador->exibir();
					
				?>
			</td>
		</tr>
		<!-- ANO DE REFERENCIA ----------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Ano:
			</td>
			<td>
				<select name="anoreferencia" id="anoreferencia">
						<option value="">selecione</option>
		  				<option value="2008">2008</option>
		  				<option value="2009">2009</option>
		  				<option value="2010">2010</option>
		  				<option value="2011">2011</option>
					</select>
			</td>
		</tr>
		
		<!-- CALSSIFICACAO IDEB -------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Classifica��o IDEB
			</td>
			<td>
				<?php
				$sqlComboIDEB = "
					select
						tpmid as codigo,
						tpmdsc as descricao
					from territorios.tipomunicipio
					where
						gtmid = ( select gtmid from territorios.grupotipomunicipio where gtmdsc = 'Classifica��o IDEB' ) and
						tpmstatus = 'A'
				";
				combo_popup( "ideb", $sqlComboIDEB, "Classifica��o IDEB", "215x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- REGIAO -------------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Regi�es
			</td>
			<td>
				<?php
				$sqlComboRegiao = "
					select
						regcod as codigo,
						regdescricao as descricao
					from territorios.regiao
					order by
						regdescricao
				";
				combo_popup( "regcod", $sqlComboRegiao, "Regi�es", "192x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- ESTADO -------------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Unidades da Federa��o
			</td>
			<td>
				<?php
				$sqlComboEstado = "
					select
						estuf as codigo,
						estdescricao as descricao
					from territorios.estado
					order by
						estdescricao
				";
				combo_popup( "estuf", $sqlComboEstado, "Estado", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- MUNICIPIO -------------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Munic�pios
			</td>
			<td>
				<?php
				//verificar se possui acesso a munic�pio restrito
				if(!cte_possuiPerfilSemVinculo()) {	
					$codMunicipios = cte_pegarMunicipiosPermitidos();	
					if(count($codMunicipios) > 0) {
						$codMunicipios = implode(',', $codMunicipios);	
						$wh = " and m.muncod in ('"	. $codMunicipios . "') ";		
					}
				}
				else {
					$wh = '';
				}
			
				$sqlComboMunicipio = "
					select
						m.muncod as codigo,
						m.estuf || ' - ' || m.mundescricao as descricao
					from territorios.municipio m
						inner join cte.instrumentounidade i on
							i.muncod = m.muncod and
							i.mun_estuf = m.estuf
						inner join cte.pontuacao p on
							p.inuid = i.inuid
						inner join cte.acaoindicador a on
							a.ptoid = p.ptoid
						inner join cte.subacaoindicador s on
							s.aciid = a.aciid
					where
						i.itrid = " . INSTRUMENTO_DIAGNOSTICO_MUNICIPAL . "
						$wh 
					group by
						m.muncod,
						m.estuf,
						m.mundescricao
					order by
						m.estuf,
						m.mundescricao
				";
				//dbg($sqlComboMunicipio, 1);					
				combo_popup( "muncod", $sqlComboMunicipio, "Munic�pio", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- DIMENSAO ------------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Dimens�es
			</td>
			<td>
				<?php
				$sqlComboDimencao = "
					select
						dimid as codigo,
						dimcod || ' - ' || dimdsc as descricao
					from cte.dimensao
					where
						dimstatus = 'A' and
						itrid = '" . INSTRUMENTO_DIAGNOSTICO_MUNICIPAL . "'
					order by
						dimcod
				";
				combo_popup( "dimid", $sqlComboDimencao, "Dimens�es", "175x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- AREA ---------------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				�reas
			</td>
			<td>
				<?php
				$sqlComboArea = "
					select
						a.ardid as codigo,
						d.dimcod || '.' || a.ardcod || ' - ' || substr( a.arddsc, 0, 95 ) || '...' as descricao
					from cte.areadimensao a
						inner join cte.dimensao d on d.dimid = a.dimid
					where
						a.ardstatus = 'A' and
						d.itrid = '" . INSTRUMENTO_DIAGNOSTICO_MUNICIPAL . "'
					order by
						d.dimcod,
						a.ardcod
				";
				combo_popup( "ardid", $sqlComboArea, "�reas", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- INDICADOR ----------------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Indicadores
			</td>
			<td>
				<?php
				$sqlComboIndicador = "
					select
						i.indid as codigo,
						d.dimcod || '.' || a.ardcod || '.' || i.indcod || ' - ' || substr( i.inddsc, 0, 95 ) || '...' as descricao
					from cte.indicador i
						inner join cte.areadimensao a on a.ardid = i.ardid
						inner join cte.dimensao d on d.dimid = a.dimid
					where
						i.indstatus = 'A' and
						d.itrid = '" . INSTRUMENTO_DIAGNOSTICO_MUNICIPAL . "'
					order by
						d.dimcod,
						a.ardcod,
						i.indcod
				";
				combo_popup( "indid", $sqlComboIndicador, "Indicadores", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- UNIDADE DE MEDIDA --------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Unidades de Medida
			</td>
			<td>
				<?php
				$sqlComboUnidade = "
					select
						undid as codigo,
						unddsc as descricao
					from cte.unidademedida
					order by
						unddsc
				";
				combo_popup( "undid", $sqlComboUnidade, "Unidades de Medida", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- PLANO INTERNO ------------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Plano Interno
			</td>
			<td>
				<?php
				$sqlComboPlanoInterno = "
					select
						plicod as codigo,
						plicod || ' - ' ||plidsc as descricao
					from cte.programa p
						inner join financeiro.planointerno pi on pi.plicod = p.prgplanointerno
					order by prgplanointerno
				";
				combo_popup( "plicod", $sqlComboPlanoInterno, "Plano Interno", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- PROGRAMA ------------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Programas
			</td>
			<td>
				<?php
				$sqlComboPrograma = "
					select
						prgid as codigo,
						prgdsc as descricao
					from cte.programa					
					order by
						prgdsc
				";
				combo_popup( "prgid", $sqlComboPrograma, "Programas", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- PARECER ACAO -------------------------------------------------- -->
		<!--<tr>
			<td class="SubTituloDireita" valign="top">
				An�lise A��o
			</td>
			<td>
				<?php
				$sqlComboParecerAcao = "
					select
						aciparecer as codigo,
						substr( aciparecer, 0, 95 ) as descricao
					from cte.acaoindicador
					order by
						aciparecer
				";
				combo_popup( "aciparecer", $sqlComboParecerAcao, "Parecer A��o", "400x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		--><!-- PARECER SUBACAO ----------------------------------------------- -->
		<!--<tr>
			<td class="SubTituloDireita" valign="top">
				Tipo An�lise Suba��o
			</td>
			<td>
				<?php
				$sqlComboTipoParecerSubacao = "
					SELECT
						psuid as codigo,
						psudescricao as descricao
					FROM cte.parecersubacao
					ORDER BY
						psudescricao
				";
				combo_popup( "psuid", $sqlComboTipoParecerSubacao, "Tipo Parecer Suba��o", "335x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		--><!-- GRUPO PARECER SUBACAO ----------------------------------------- -->
		<!--<tr>
			<td class="SubTituloDireita" valign="top">
				Grupo An�lise Suba��o
			</td>
			<td>
				<?php
				$sqlComboGrupoParecerSubacao = "
					select
						gpsid as codigo,
						gpsdescricao as descricao
					from cte.grupoparecersubacao
					order by
						gpsdescricao
				";
				combo_popup( "gpsid", $sqlComboGrupoParecerSubacao, "Grupo Parecer Suba��o", "143x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		--><!-- STATUS SUBACAO ------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Status Suba��o
			</td>
			<td>
				<?php
				$sqlComboStatusSubacao = "
					select
						ssuid as codigo,
						ssudescricao as descricao
					from cte.statussubacao
					order by
						ssudescricao
				";
				combo_popup( "ssuid", $sqlComboStatusSubacao, "Status Suba��o", "143x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- FORMA EXECUCAO ------------------------------------------------ -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Forma de Execu��o
			</td>
			<td>
				<?php
				$sqlComboFormaExecucao = "
					select
						frmid as codigo,
						frmdsc as descricao
					from cte.formaexecucao
					where
						frmbrasilpro = false and
						frmtipo		 = 'M'					
					order by
						frmdsc
				";
				combo_popup( "frmid", $sqlComboFormaExecucao, "Forma de Execu��o", "210x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<!-- FORMA ATENDIMENTO --------------------------------------------- -->
		<tr>
			<td class="SubTituloDireita" valign="top">
				Forma de Atendimento
			</td>
			<td>
				<?php
				$sqlComboFormaAtendimento = "
					select
						foaid as codigo,
						foadsc as descricao
					from cte.formaatendimento
					order by
						foadsc
				";
				combo_popup( "foaid", $sqlComboFormaAtendimento, "Forma de Atendimento", "160x400", 0, array(), "", "S", false, false, 5, 400 );
				?>
			</td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" valign="top">
				&nbsp;
			</td>
			<td class="SubTituloDireita" style="text-align:left;">
				<input
					type="button"
					name="filtrar"
					value="Visualizar"
					onclick="exibirRelatorio();"
				/>
			</td>
		</tr>
		
	</table>
</form>