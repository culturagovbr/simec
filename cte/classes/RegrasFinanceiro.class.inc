<?php
/*
class Regrasfinanceiro extends Modelo{
	protected $ano 		= '';
	protected $mes 		= '';
	protected $status 	= '';
	
	public function existeDadosBancariosCadastrado(){
		
	}
	
	public function podeIniciarMonitoramento(){
		
	}
///////////////// VERIFICAR ////////////////
	public function validaSePodeInserirMonitoramento($prsid, $mesTela, $anoTela = NULL, $return = NULL){
		$obdata = new Data();
		$obHistMonitoraConvenio = new HistoricoMonitoramentoConvenio();
		
		$sql = "SELECT hmcid
				FROM cte.historicomonitoramentoconvenio 
				WHERE prsid = '".$prsid."' ORDER BY hmcid DESC";
		$hmcidC = $obHistMonitoraConvenio->pegaUm($sql);
	
		if($hmcidC){
			if(!valorTotalConvenio($hmcidC, $_SESSION['ano'])){
				if($return){
					return false;
				}
				return "O monitoramento deste m�s n�o pode ser criado, \n pois o monitoramento do m�s anterior j� chegou ao teto m�ximo de valor do conv�nio.";
			}
		}
		
		// REGRA 1
		//dbg($mesTela);
		//if($mesTela !=10 || $mesTela !=11 || $mesTela !=12){
		//	$mesTela = '0'.$mesTela;
		//}
	
		$sql = "SELECT hmcstatus
				FROM cte.historicomonitoramentoconvenio 
				WHERE prsid = ".$prsid." AND to_char(hmcdatamonitoramento, 'MM') = '".$mesTela."' 
				ORDER BY hmcdatamonitoramento DESC LIMIT 1"; // recupera o mes do ultimo monitoramento feito.
		
		$status = $obHistMonitoraConvenio->pegaUm($sql);
		
		if($status == "F"){
			if($return){
					return false;
			}
			return "O monitoramento deste m�s j� foi finalizado.";
		}
		
		$sql = "SELECT hmcstatus
				FROM cte.historicomonitoramentoconvenio 
				WHERE prsid = ".$prsid."
				ORDER BY hmcdatamonitoramento DESC LIMIT 1"; // recupera o mes do ultimo monitoramento feito.
		
		$status2 = $obHistMonitoraConvenio->pegaUm($sql);
		//dbg($status2,1);
		if($status2 == "I"){
			if($return){
					return false;
			}
			return "O monitoramento do m�s anterior n�o foi finalizado.";
		}
		
		
		
		// REGRA 2
		if(!existeDadosFinanceirosCadastrados($prsid)){
			if($return){
				return false;
			}
			return "O monitoramento n�o pode ser iniciado. \n N�o existe dados Financeiros do Conv�nio cadastrado para o m�s atual. ";
		}
		
		
		// REGRA 3
		$sql = "select  to_char(prsdata, 'YYYY') AS anoconvenio, 
						to_char(prsdata, 'MM')   AS mesconvenio,
						to_char(prsdata, 'YYYY-MM-DD') as data
				from cte.projetosape 
				where prsid = ".$prsid." limit 1";
		$dataConvenio = $obHistMonitoraConvenio->pegaLinha($sql);
		$podeIniciar = podeIniciarConvenio($dataConvenio);
		if($podeIniciar == false){
			if($return){
				return false;
			}
			return "O monitoramento n�o pode ser iniciado antes do dia 15 do pr�ximo m�s do conv�nio.";
		}
		
		// REGRA 4
		if(!monitoramentoAnteriorEncerrado($prsid) && !$mesTela ){
			if($return){
				return false;
			}
			return "O monitoramento n�o pode ser iniciado. \n Finalize o monitoramento do m�s anterior. ";
		}
		
		// REGRA 5
		$sql = "SELECT to_char(hmcdatamonitoramento, 'MM') AS mes 
				FROM cte.historicomonitoramentoconvenio 
				WHERE prsid = ".$prsid." ORDER BY hmcdatamonitoramento DESC LIMIT 1"; // recupera o mes do ultimo monitoramento feito.
		$mes = $obHistMonitoraConvenio->pegaUm($sql);
		
		// PEGA MES
		$sql = "select  to_char(hmcdatamonitoramento, 'MM') AS hmdmes , to_char(hmcdatamonitoramento, 'YYYY') AS hmdano
			from cte.historicomonitoramentoconvenio 
			where prsid = ".$prsid." AND hmcstatus = 'F' ORDER BY hmcdatamonitoramento desc limit 1";
		$mesDB = $obHistMonitoraConvenio->pegaLinha($sql);
		//dbg($sql,1);
		if($mes){ // se j� existe monitoramneto.
			$mes 	= $mes != 12 ? $mes += 01 : 01;
			$data 	= date('Y-').$mes.date('-d');
			if(date('d') < 15 && $mes == date('m') ){
				if($return){
					return false;
				}
				return "O monitoramento s� pode ser iniciado ap�s o dia 15 do m�s.";
			}
			// REGRA 6
			/*
			if($mesTela != $mes ){
				if($return){
					return false;
				}
				return "� obrigat�rio monitorar os meses consecultivamente,\n desde a data de in�cio do conv�nio firmado.";
			}
			*/
			
			$proximoMes 	= $mesDB['hmdmes'] != 12 ? $mesDB['hmdmes'] += 01 : '01';
			//$proximoMes = $mesDB['hmdmes'] + 1;
			
			if($mesTela != $proximoMes){
				$msnMes = $obdata->mesTextual($proximoMes);
				if($return){
					return false;
				}
				return "Este m�s n�o pode ser monitorado. \n Inicie o monitoramento do m�s ".$msnMes.".";
			}else{
				return true;
			}
			if($anoTela != NULL){
				if($mesDB['hmdano'] != $anoTela){
					$msnMes = $obdata->mesTextual($proximoMes);
					if($return){
						return false;
					}
					return "Este m�s n�o pode ser monitorado. \n Inicie o monitoramento do m�s ".$msnMes.".";
				}
			}
			
			
		}else{ // SE N�O EXISTE MONITORAMENTO CRIADO.
			// PEGA MES
			$sql = "select  hmdmes, hmdano
				from cte.historicomonitoramentodadosbancarios 
				where prsid = ".$prsid." AND hmdano >= ".$_SESSION['ano']." ORDER BY hmdano, hmdmes ASC limit 1";
			$mesDB2 = $obHistMonitoraConvenio->pegaLinha($sql);
			
			// REGRA 7
			$data 			= $dataConvenio['data']; 
			$mesConvenio 	= $mesTela;
			$mesVerificacao = $mesTela; //$mesConvenio != 12 ? $mesConvenio += 01 : 01;
			$anoConvenio 	= $dataConvenio['anoconvenio']; 
			$data 			= date('Y-').$mesTela.date('-d');
			if(date('m') <= $mesConvenio && $anoConvenio == date('Y')  ){
				if($return){
					return false;
				}
				return "O monitoramento s� pode ser iniciado no m�s seguinte ao conv�nio.";
			}else if(date('d') < 15 && $mesVerificacao == date('m') ){
				if($return){
					return false;
				}
				return "O monitoramento s� pode ser iniciado ap�s o dia 15 do m�s.";
			}
			
			//REGRA 8
			if($mesTela != $mesDB2['hmdmes']){
				$msnMes = $obdata->mesTextual($mesDB2['hmdmes']);
				if($return){
					return false;
				}
				return "Este m�s n�o pode ser monitorado. \n Inicie o monitoramento do m�s ".$msnMes.".";
			}
			
			if($anoTela != NULL){
				if($mesDB2['hmdano'] != $anoTela){
					$msnMes = $obdata->mesTextual($mesDB2['hmdmes']);
					if($return){
						return false;
					}
					return "Este m�s n�o pode ser monitorado. \n Inicie o monitoramento do m�s ".$msnMes.".";
				}
			}
	
			
			if($anoTela != $mesDB2['hmdano'] && $mesTela == $mesDB2['hmdmes']){
				$msnMes = $obdata->mesTextual($mesDB2['hmdmes']);
				if($return){
					return true;
				}
				return "Este m�s n�o pode ser monitorado. \n Inicie o monitoramento do m�s2 ".$msnMes.".";
			}
		}
		return true;
	}
	
	/**
	 * function carregaMonitoramentosParaSession($prsid, $hmcid);
	 * @desc   : Carrega os monitoramentos com status de iniciados para session. 
	 * @author : Thiago Tasca Barbosa
	 * @param  : numeric $prsid (id do conv�nio)
	 * @param  : numeric $hmcid (id do monitoramento)
	 * @return : boleano true
	 * @since 01/04/2009
	 */
	public function carregaMonitoramentosParaSession($prsid,$hmcid){
			$_SESSION['hmc'][$prsid] = $hmcid;
		return true;
	}
	
	
	
}
*/
?>