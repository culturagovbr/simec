<?php
	global $servidor_bd, $porta_bd, $nome_bd, $usuario_db, $senha_bd, $email_sistema,$servidor_bd_siafi, $porta_bd_siafi, $nome_bd_siafi, $usuario_db_siafi, $senha_bd_siafi;
	
	/**
	 * Configura��o PHP  
	 */
	session_start();
	ini_set("short_open_tag","on");
    ini_set('display_errors', 'on');
	error_reporting(E_ALL ^E_NOTICE);
	date_default_timezone_set('America/Sao_Paulo');
	header('content-type: text/html; charset=ISO-8859-1');
	
	defined('AUTHSSD') || define('AUTHSSD', false);
	defined('APPRAIZ') || define('APPRAIZ', '/var/www/simec/simec_dev/simec/');
	defined('DIRFILES') || define('DIRFILES', '/var/www/simec/simec_dev/simec/arquivos/');
	defined('SISRAIZ') || define('SISRAIZ', APPRAIZ . $_SESSION['sisdiretorio'] . '/');
	defined('MAXONLINETIME') || define('MAXONLINETIME', 3600);
	defined('IS_PRODUCAO') || define('IS_PRODUCAO', false);

	$_SESSION['ambiente'] = IS_PRODUCAO ? 'Ambiente de Produ��o' : 'Ambiente de Desenvolvimento';
	$_SESSION['sisbaselogin'] = $_REQUEST['baselogin'] && $_REQUEST['baselogin'] != "teste" ? $_REQUEST['baselogin'] : $_SESSION['sisbaselogin'];
	$_SESSION['baselogin'] = $_REQUEST['baselogin'] ? $_REQUEST['baselogin'] : $_SESSION['baselogin'];
	
	if ($_SESSION['baselogin'] == "simec_espelho_producao") {
		$nome_bd     = 'simec_espelho_producao';
        $servidor_bd = '192.168.222.45';
		$porta_bd    = '5432';
		$usuario_db  = 'simec';
		$senha_bd    = 'phpsimecao';
	} else {
		$nome_bd = 'simec_desenvolvimento';
		$servidor_bd = '192.168.222.45';
		$porta_bd = '5433';
		$usuario_db = 'simec';
		$senha_bd = 'phpsimecao';
	}
		
	$servidor_bd_siafi = '192.168.222.21';
	$porta_bd_siafi = '5432';
	$nome_bd_siafi = 'dbsimecfinanceiro';
	$usuario_db_siafi = 'seguranca';
	$senha_bd_siafi = 'phpsegurancasimec';	
			
	# Sistema PDDEInterativo - ESPELHO DE PRODU��O
	$configDbPddeinterativo = new stdClass();
	$configDbPddeinterativo->host = '192.168.222.45';
	$configDbPddeinterativo->port = 5433;
	$configDbPddeinterativo->dbname = 'dbpdeinterativo_hmg';
	$configDbPddeinterativo->user = 'simec';
	$configDbPddeinterativo->password = 'phpsimecao';

	# Sistema SIGFOR
	$configDbSigfor = new stdClass();
	$configDbSigfor->host = '10.1.3.168';
	$configDbSigfor->port = 5432;
	$configDbSigfor->dbname = 'dbsigfor_hmg';
	$configDbSigfor->user = 'simec';
	$configDbSigfor->password = 'phpsimecao';

	$email_sistema = 'cristiano.cabral@mec.gov.br; adonias.malosso@mec.gov.br';
	
	preg_match( '/\/([a-zA-Z]*)\//', $_SERVER['REQUEST_URI'], $sisdiretorio );
	$sisdiretorio = $sisdiretorio[1];
	
	preg_match( '/\/([a-zA-Z]*)\.php/', $_SERVER['REQUEST_URI'], $sisarquivo );
	$sisarquivo = $sisarquivo[1];
	
	include_once APPRAIZ . 'includes/simec_funcoes.inc';
	include_once APPRAIZ . 'includes/backtrace/BackTrace.php';
	include_once APPRAIZ . 'includes/backtrace/BackTraceExplain.php';
	include_once APPRAIZ . 'includes/failure/ErrorHandler.php';
	include_once APPRAIZ . 'includes/failure/ExceptionHandler.php';
	include_once APPRAIZ . 'includes/library/simec/SoapClient.php';
	include_once APPRAIZ . "includes/connection/adapter-connection.php";
	
	function fechar_conexoes()
	{	
		while(  pg_ping())
		{	
			pg_close();
		}
	}
	
	register_shutdown_function( 'fechar_conexoes' );
	ErrorHandler::start();
	ExceptionHandler::start();
	ob_start();
?>