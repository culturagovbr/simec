<?php

class Model_PlanoAcao extends Abstract_Model
{
    
    /**
     * Nome da tabela
     * @var string
     */
    protected $_name = 'pesplanoacao';
    
    /**
     * Nome da chave primaria
     * @var string / array
     */
    protected $_primary = 'placodigo';
}
