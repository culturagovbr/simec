<?php

class Model_Municipio extends Abstract_Model
{
    /**
     * Nome da tabela
     * @var string
     */
    protected $_name = 'pesmunicipio';
    
    /**
     * Nome da chave primaria
     * @var string
     */
    protected $_primary = 'muncodigo';
    
}
