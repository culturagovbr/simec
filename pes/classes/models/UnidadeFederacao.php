<?php

class Model_UnidadeFederacao extends Abstract_Model
{
    /**
     * Nome da tabela
     * @var string
     */
    protected $_name = 'pesunidadefederacao';
    
    /**
     * Nome da chave primaria
     * @var string
     */
    protected $_primary = 'ufecodigo';
    
}