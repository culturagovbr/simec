<? 
 /*
   Sistema Simec
   Setor respons�vel: SPO-MEC
   Desenvolvedor: Equipe Simec
   Analista: Alexandre Soares Diniz
   Programador: Alexandre Soares Diniz
   M�dulo:sistemas.inc
   Finalidade: permitir o cadastro dos sistemas
   */

### function, Copia diret�rio ###
function copyDir ($source, $dest) {
	$arq = (array) scandir($source);
	
	// Caso n�o exista o diret�rio de destino cria o mesmo
	if (!is_dir($dest))
		mkdir($dest); 
		
	// LOOP nos arquivos do diret�rio que ser� copiado	
	foreach ($arq as $arq):
	
		## Caso seja um diret�rio aplica recursividade ##
		if (strpos($arq, ".") === false && is_dir($source."/".$arq)):
			copyDir($source."/".$arq, $dest."/".$arq);
			continue;
		## Caso seja um arquivo ".php ou .inc" copia para o novo diret�rio
		elseif (strrpos($arq, ".php") || strrpos($arq, ".inc")):
			copy($source."/".$arq, $dest."/".$arq);
			continue;
		endif;
	endforeach;
	return true;
}

########### A��ES Cadastro/Atualiza��o de sistema ###########
/* 
 * Ser� feita uma C�PIA do sistema RAIZ => sisid = 999 
 * Criado com a finalidade de ser o sistema padr�o 
*/
if ( ($_REQUEST['act'] == 'inserir') && (! is_array($msgerro)) && !$_POST['pesquisa'] ):
	
    $sql= "SELECT
  	  	   sidid as sidid, 
  	  	   siddescricao as siddescricao
  		  FROM
  		   demandas.sistemadetalhe 
  		  WHERE
  		   siddescricao = '".$_REQUEST['dsc']."' OR
  		   sidabrev = '".$_REQUEST['abrev']."';";
   $usu = $db->pegaUm($sql);
   
   
   unset($sql);
	
   if ($usu) {
	   // existe perfil identico, logo, tem que bloquear
	   ?>
	      <script>
	         alert ('O Sistema: <?=$_REQUEST['dsc']?> j� se encontra cadastrado no sistema.');
	         history.back();
	      </script>
	   <?
	     exit();
	}

//////// Verifica ESQUEMA /////////
/*
 * Se h� esquema == diret�rio novo, interrompe cadastro
 */	
	 $sql = sprintf("SELECT
				     sidid 
				    FROM
				     demandas.sistemadetalhe 
				    WHERE
				     sidabrev = '%s'",
			$_REQUEST['abrev']);
	if ($db->pegaUm($sql) == "t" ){
		die('<script>
	         	alert(\'J� existe a Abrevia��o "'.$_REQUEST['abrev'].'" no Banco de Dados.\nOpera��o n�o realizada!\');
	         	history.back();
	      	 </script>');
	}	
	
	if(!$_REQUEST['qtdeusuarios']) $_REQUEST['qtdeusuarios'] = "0";

///////// Cadastro do sistema //////////	   
    $sql = "INSERT INTO demandas.sistemadetalhe (
    			siddescricao,
    			sidurl,
    			sidabrev,
    			sidfinalidade,
    			sidrelacionado,
    			sidpublico,
    			sidstatus,
    			sidemail,
			    sipid, 
			    sitid, 
			    sidoutrastecno,
			    ssiid, 
			    sidmotivo, 
			    sidimplantacao,
			    sidqtdeusuarios, 
			    sidpublicar,
			    unaid 			
    			) 
    			VALUES (".
		   "'".$_REQUEST['dsc']."',".
		   "'".$_REQUEST['url']."',".
		   "'".strtoupper($_REQUEST['abrev'])."',".   
		   "'".$_REQUEST['finalidade']."',".
		   "'".$_REQUEST['relacionado']."',".
		   "'".$_REQUEST['publico']."',".
		   "'A',".
		   "'".$_REQUEST['email']."',".
		   "'".$_REQUEST['plataforma']."',".
		   "'".$_REQUEST['tecnologia']."',".
       	   "'".$_REQUEST['outras']."',".
    	   "'".$_REQUEST['situacao']."',".
    	   "'".$_REQUEST['motivo']."',".
    	   "'".$_REQUEST['implantacao']."',".
    	   "'".$_REQUEST['qtdeusuarios']."',".    
    	   "'".$_REQUEST['publicar']."',".
    	   "'".$_REQUEST['orgao']."') RETURNING sidid";
   $sisidNew = $db->pegaUm($sql);
   $db->commit();
   unset($sql);
   
    ?>
    	<script>
            alert('Opera��o realizada com sucesso!');
		  	location.href='demandas.php?modulo=sistema/apoio/sistemas&acao=A';
        </script>
    <?
	exit();
endif;

if ( ($_REQUEST['act']=='alterar') && (! is_array($msgerro)) && !$_POST['pesquisa'] ):

	if(!$_REQUEST['qtdeusuarios']) $_REQUEST['qtdeusuarios'] = "0";
	
   // fazer altera��o de perfil na base de dados.
  	$sql = "UPDATE 
   			 demandas.sistemadetalhe 
   			SET 
		   	 siddescricao 		= '".$_REQUEST['dsc']."',
		  	 sidurl 			= '".$_REQUEST['url']."',
		  	 sidabrev 			= '".strtoupper($_REQUEST['abrev'])."',
		  	 sidfinalidade 		= '".$_REQUEST['finalidade']."',
		  	 sidrelacionado 	= '".$_REQUEST['relacionado']."',
		  	 sidpublico 		= '".$_REQUEST['publico']."',
		  	 sidemail 			= '".$_REQUEST['email']."',
 		  	 sipid 				= '".$_REQUEST['plataforma']."',
		  	 sitid 				= '".$_REQUEST['tecnologia']."',
		  	 sidoutrastecno 	= '".$_REQUEST['outras']."',
		  	 ssiid 				= '".$_REQUEST['situacao']."',
		  	 sidmotivo 			= '".$_REQUEST['motivo']."',
		  	 sidimplantacao		= '".$_REQUEST['implantacao']."',
		  	 sidqtdeusuarios 	= '".$_REQUEST['qtdeusuarios']."',		  	 
		  	 sidpublicar 		= '".$_REQUEST['publicar']."',		  	 		  	 		  	 		  	  		  	 
		  	 unaid 		    	= '".$_REQUEST['orgao']."'		  	 
		  	WHERE
		  	 sidid = ".$_POST['sisidEdit'];
  	
    $saida = $db->executar($sql);
	$db->commit();
	?>
    	<script>
            alert('Opera��o realizada com sucesso!');
		   	location.href='demandas.php?modulo=sistema/apoio/sistemas&acao=A';
        </script>
    <?
	exit();
endif;

if ( ($_REQUEST['act']=='excluir') && (! is_array($msgerro)) && !$_POST['pesquisa']):
   // fazer exclus�o de perfil na base de dados.
   	
	$sql1 = "SELECT sc.sidid 
			 FROM demandas.celula AS c
			 INNER JOIN demandas.sistemacelula AS sc ON sc.celid = c.celid 
			 WHERE c.celstatus = 'A' 
			 AND sc.sidid = ". $_REQUEST['sidid'] . "
			 LIMIT 1";
	 
	$verifica = $db->pegaUm($sql1);
	
	if($verifica)
	{
		?>
		<script >
			alert('Opera��o n�o pode ser realizada. \nDevido a dados vinculados com alguma c�lula de projeto');
			location.href = 'demandas.php?modulo=sistema/apoio/sistemas&acao=A';
		</script>
		<? 
	}else{
	     /*
	    $sql  = "DELETE FROM demandas.sistemadetalhe WHERE sidid = ".$_REQUEST['sidid'];
	    */
		$sql  = "UPDATE demandas.sistemadetalhe	SET sidstatus = 'I' WHERE sidid = ".$_REQUEST['sidid'];
	    $saida = $db->executar( $sql );
	    $db->commit();
		?>
	    	<script>
	            alert('Opera��o realizada com sucesso!');
			   	location.href='demandas.php?modulo=sistema/apoio/sistemas&acao=A';
	        </script>
	    <?
	}
	exit();
endif;

include APPRAIZ."includes/cabecalho.inc";
?>
<script language="JavaScript" src="../includes/calendario.js"></script>
<br>
<?
$db->cria_aba($abacod_tela,$url,$parametros);
$titulo_modulo='Cadastro de Sistema';
monta_titulo($titulo_modulo,'<img src="../imagens/obrig.gif" border="0"> Indica Campo Obrigat�rio.');
?>
<div align="center">
<center>
<?
//atribui valores as variaveis
$modulo=$_REQUEST['modulo'] ;//
//$sidid = md5_decrypt($_REQUEST['sisidEdit'],'');
$sidid = $_REQUEST['sidid'];

if ($sidid):
	$sql= "SELECT
			 sidid, 
			 siddescricao AS dsc, 
			 sidurl AS url, 
			 sidabrev AS abrev, 
			 sidfinalidade AS finalidade, 
		     sidrelacionado AS relacionado, 
		     sidpublico AS publico, 
		     sidstatus AS status,
		     sidemail AS email,
		     sipid AS plataforma, 
 		     sitid AS tecnologia,
 		     sidoutrastecno AS outras, 
		     ssiid AS situacao, 
		     sidmotivo AS motivo,
   		     sidqtdeusuarios AS qtdeusuarios, 
		     sidimplantacao AS implantacao, 
		     unaid AS orgao,  
             sidpublicar AS publicar 	     
		   FROM
		     demandas.sistemadetalhe 
		   WHERE 
		     sidid = ".$sidid."
		   ORDER BY
		     siddescricao";
	$dados = $db->carregar($sql);
	
	
	#### Transforma $dados[0] em Vari�veis ####
	extract($dados[0]);
	
else:
		
	$sidid 		   = "";
	$url		   = "";
	$abev		   = "";
	$finalidade    = "";
	$relacionado   = "";
	$publico       = "";
	$status        = "";
	$email         = "";
	$plataforma    = "";
	$tecnologia    = "";
	$outras		   = "";
	$situacao      = "";
	$motivo        = "";
	$qtdeusuarios  = "";	
	$implantacao   = "";
	$orgao         = "";
	$publicar      = "";
	
endif;
	$act = '';
	
?>

<form method="POST"  name="formulario">

	<input type='hidden' name="modulo" value="<?=$modulo?>">
	<input type='hidden' name="sisidEdit" value="<?=$sidid?>">
	<input type='hidden' name="act" value=<?=$act?>>
	<input type="hidden" name="pesquisa" value="">

    <center>
    <table  class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
    <!-- 
      <tr>
        <td align='right' class="SubTituloDireita">C�digo:</td>
	    <td><?//=campo_texto('sisid','S',($sisid != ""?'N':'S'),'',5,5,'','');?></td>
      </tr>
      -->
      <tr>
        <td align='right' class="SubTituloDireita">Sistema:</td>
	     <td><?=campo_texto('dsc','S','S','',50,100,'','');?></td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">Abrevia��o:</td>
	    <td><?=campo_texto('abrev','S','S','',30,100,'','');?></td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">Finalidade:</td>
	    <td><?=campo_textarea('finalidade','S','S','',75,3,'');?></td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">Relacionado:</td>
	    <td><?=campo_textarea('relacionado','S','S','',75,3,'');?></td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">P�blico:</td>
	    <td><?=campo_textarea('publico','S','S','',75,3,'');?></td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">Plataforma:</td>
	    <td><?php
  	
	    	
	    	$sql = "select 	sipid as codigo, 
               				sipdsc as descricao
						from demandas.sistemaplataforma
						order by descricao";	
	    	                             	    
			$db->monta_combo('plataforma', $sql, 'S', "Selecione...", 'filtraPlataforma', '', '', '150', 'S', 'plataforma',false,null,'Plataforma');?></td>
      </tr>
      <!-- tr>
			<td align='right' class="SubTituloDireita">Plataforma:</td>
			<td>
				<table width="20%">
				<?/* $sql = "SELECT
							'<input type=\'radio\' name=\'sipid\' value=\'' || sipid || '\' onclick=\'filtraPlataforma('|| sipid ||');\'/>' || sipdsc || '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'AS campo 
						   FROM
						    demandas.sistemaplataforma
						   WHERE
						    sipstatus = 'A'
							ORDER BY 
							 sipdsc";
					$radio = (array) $db->carregar($sql);
					
					foreach ($radio as $radio){
						//$a++;
						//echo $a == 1 || $a == 7 || $a == 13 ? '<TR>' : '';
						//echo  '<td>'.$radio['campo'].'</td>';
						//echo $a == 3 || $a == 9 || $a == 15 ? '</TR>' : '';
						echo ($a%3) == 0 ? '<TR>' : '';
						echo  '<td>'.$radio['campo'].'</td>';
						echo ($a+1%3) == 0 ? '</TR>' : '';
						$a++;
					}*/	
			    ?>
			    </table>
			</td>
	  </tr-->	
		<?php $mostraUrl='none';
		 if ($plataforma == '1') $mostraUrl='';
		 ?>
      <tr id=tr_url style="display:<?=$mostraUrl?>;">
        <td align='right' class="SubTituloDireita">Url:</td>
	    <td id=td_url>
	     <?php
	     if (!$url) $url =  '';
	     ?>
	    <?=campo_texto('url','S',$sidid ? 'S' : 'S','',50,100,'','','','','','','',$url);?>
	    </td>
      </tr>

      <tr>
        <td align='right' class="SubTituloDireita">Tecnologia:</td>
	    <td><?php

	    	$sql = "select 	sitid as codigo, 
               				sitdsc as descricao
						from demandas.sistematecnologia
						where sitid  not in (6)
						order by 2";	
			$arraysql = $db->carregar( $sql );	    	                             	    

	    	$sql = "select 	sitid as codigo, 
               				sitdsc as descricao
						from demandas.sistematecnologia
						where sitid  in (6)
						";	
			$arraysql2 = $db->pegalinha( $sql );	    	                             	    
			
			array_push($arraysql,$arraysql2);
			
			$db->monta_combo('tecnologia', $arraysql, 'S', "Selecione...", 'filtraTecnologia', '', '', '100', 'S', 'tecnologia',false,null,'Tecnologia');?></td>
      </tr>

		<?php $mostraOutras='none';
		 if ($tecnologia == '6') $mostraOutras='';
		 ?>
      <tr id=tr_outras style="display:<?=$mostraOutras?>;">
        <td align='right' class="SubTituloDireita">Especificar (Outras):</td>
	    <td id=td_outras>
	     <?php
	     if (!$outras) $outras =  '';
	     ?>
	    <?=campo_texto('outras','S',$sidid ? 'S' : 'S','',50,100,'','','','','','','',$outras);?>
	    </td>
      </tr>

      <tr>
        <td align='right' class="SubTituloDireita">Situa��o:</td>
	    <td><?php

	    	$sql = "select 	ssiid as codigo, 
               				ssidsc as descricao
						from demandas.sistemasituacao
						order by 1";	
	    	                             	    
			$db->monta_combo('situacao', $sql, 'S', "Selecione...", 'filtraSituacao', '', '', '150', 'S', 'situacao',false,null,'Situacao');?></td>
      </tr>
      	<?php $mostraMotivo='none';
		 if ($situacao == '4') $mostraMotivo='';
		 ?>
      
      <tr id=tr_motivo style="display:<?=$mostraMotivo?>;">
        <td align='right' class="SubTituloDireita">Justificativa Situa��o:</td>
	    <td id=td_motivo>     
	    <?php 
	    if (!$motivo) $motivo =  '';
	    ?>
			<?=campo_textarea('motivo','S','S','',75,3,'');?>	    
	    </td>
      </tr>
      	<?php $mostraQtde='none';
   	
		 if ($situacao == '2') {
		 	$mostraQtde='';
         		 }
         else  	$qtdeusuarios='0';		 
	 		 
		 ?>

      <tr id=tr_qtde style="display:<?=$mostraQtde?>;">
        <td align='right' class="SubTituloDireita">Quantidade de Usu�rios:</td>
	    <td id=td_qtde><?=campo_texto('qtdeusuarios','S','S','',15,15,'#########','');?></td>
      </tr>

      <tr>
        <td align='right' class="SubTituloDireita">Data Implanta��o:</td>
	    <td><?=campo_texto('implantacao','S','S','',8,15,'##/####','');?> (mm/aaaa)</td>
      </tr>

      <tr>
        <td align='right' class="SubTituloDireita">�rg�o Respons�vel:</td>
	    <td><?php

	    	$sql = "select 	unaid as codigo, 	
                			unasigla || ' - ' || unadescricao as descricao
						from demandas.unidadeatendimento
						order by unasigla";
	    	                             	    
			$db->monta_combo('orgao', $sql, 'S', "Selecione...", '', '', '', '500', 'S', 'orgao',false,null,'orgao');?></td>
      </tr>
      

            
      <tr>
        <td align='right' class="SubTituloDireita">E-mail:</td>
	     <td><?=campo_texto('email', 'N', 'S' , '', 50, 100, '', '', 'left', '', 0, 'id="email"'); ?></td>
      </tr>

      <tr>
        <td align='right' class="SubTituloDireita">Publicar em Relat�rio:</td>
	       <td>
            <input type="radio" name="publicar" value="S" <?=($publicar=='S'?"CHECKED":"")?>> Sim
            <input type="radio" name="publicar" value="N" <?=($publicar=='N'?"CHECKED":"")?>> N�o
        </td>
      </tr>
      
      <!--
      <tr>
        <td align='right' class="SubTituloDireita">Status:</td>
	       <td>
            <input type="radio" name="status" value="A" <?=($status=='A'?"CHECKED":"")?>> Ativo
            <input type="radio" name="status" value="I" <?=($status=='I'?"CHECKED":"")?>> Inativo
        </td>
      </tr>
      <tr>
        <td align='right' class="SubTituloDireita">Tabelas de seguran�a:</td>
	       <td>
            <input type="radio" name="tblseg" <?=$sisid ? 'disabled="disabled"' : '';?> value="A" <?=($tblseg=='A'?"CHECKED":"")?>> Ativo
            <input type="radio" name="tblseg" <?=$sisid ? 'disabled="disabled"' : '';?> value="I" <?=($tblseg=='I'?"CHECKED":"")?>> Inativo
        </td>
      </tr>
      -->
<? if ($sidid) { ?>
	<tr bgcolor="#CCCCCC">
   		<td></td>
   		<td>
   			<input type="button" name="btalterar" value="Alterar" onclick="validar_cadastro('A')" class="botao">
   			&nbsp;
   			<input type="button" name="btcancela" value="Cancelar" onclick="inicio();" class="botao">
   		</td>
 	</tr>
<? } else { ?>
	<tr bgcolor="#CCCCCC">
   		<td></td>
   		<td>
   			<input type="button" name="btinserir" value="Incluir" onclick="validar_cadastro('I')" class="botao">
   			&nbsp;
   			<input type="button" name="btcancela" value="Cancelar" onclick="inicio();" class="botao">
   			&nbsp;
	    	<input type="button" name="Pesquisar" value="Pesquisar" onclick="pesq();">
   		</td>
	</tr>
<? } ?>
      
    </table>

  
</form>


<? 


//filtro
	if($_REQUEST['pesquisa'] || $dsc || $abrev ){
		
		$_POST['dsc'] = str_replace("'", "", $_POST['dsc']);
		$dsc = str_replace("'", "", $dsc);
		if($_POST['dsc']) {$and .= " AND upper(siddescricao) ilike '%".strtoupper($_POST['dsc'])."%' ";}
		else if($dsc) {$and .= " AND upper(siddescricao) ilike '%".strtoupper($dsc)."%' ";}

		$_POST['abrev'] = str_replace("'", "", $_POST['abrev']);
		$abrev = str_replace("'", "", $abrev);
		if($_POST['abrev']) {$and .= " AND upper(sidabrev) ilike '%".strtoupper($_POST['abrev'])."%' ";}
		else if($abrev) {$and .= " AND upper(sidabrev) ilike '%".strtoupper($abrev)."%' ";}
		
	}
	
	//if(!$_REQUEST['status']) $_REQUEST['status'] = 'A';
	
//exibe listagem dos sistemas cadastrados
$sql= "select 
		'<div align=center><a href=\"demandas.php?modulo=sistema/apoio/sistemas&acao=A&sidid=' || sd.sidid || '&op=update\"><img border=0 src=\"../imagens/alterar.gif\" /></a> <a style=\"cursor:pointer\"  onclick=\"return exclusao(\'demandas.php?modulo=sistema/apoio/sistemas&acao=A&sidid=' || sd.sidid || '&act=excluir\');\" ><img border=0 src=\"../imagens/excluir.gif\" /></a>' AS op,
		sd.sidid as codigo2, 
		sd.siddescricao as descricao2, 
		sd.sidabrev as abreviacao2, 
		ua.unasigla || ' - ' || ua.unadescricao as orgao
 		from demandas.sistemadetalhe sd
 		left join demandas.unidadeatendimento ua on ua.unaid = sd.unaid
 		WHERE sidstatus = 'A'
 		$and
 		order by siddescricao ";
//dbg($sql,1);
$cabecalho = array("A��o","C�d","Sistema","Abrevia��o","�rg�o");
$db->monta_lista( $sql, $cabecalho, 50, 10, 'N', '', '');

?>


<script>

var d=document;

function strSpecial(string){
	var search = '����������������������������������!@#$%�&*()-+][}{~^�`?:;,"';

	for (i = 0; i < search.length; i++){
		if ( string.indexOf(search.substr(i,1)) >= 0 ){
			alert('Esse caractere � inv�lido para o campo: " '+search.substr(i,1)+' "');
			return false;
		}		
	}
	return true;
}



function altera_cadastro(cod) {
    document.formulario.sisidEdit.value = cod;
    document.formulario.act.value = 'alterar';
    document.formulario.submit();
}



function exclusao(url) {
	var questao = confirm("Deseja realmente excluir este item?")
	if (questao){
		window.location = url;
	}
}

function inicio() {
 	document.formulario.act.value = '';
 	location.href='demandas.php?modulo=sistema/apoio/sistemas&acao=A';
}
  
function validar_cadastro(cod) {

	/*
	if (document.formulario.sisid.value == "") 
	{
		alert("Sistema sem c�digo.");
		document.formulario.sisid.focus();
		return;
	}
	*/
	
	if (document.formulario.dsc.value == "") 
	{
		alert("Favor informar descri��o do Sistema.");
		document.formulario.dsc.focus();
		return;
	}
	
	if (document.formulario.abrev.value == "") 
	{
		alert("Favor informar abrevia��o do Sistema.");
		document.formulario.abrev.focus();
		return;
	}
	
	if (document.formulario.finalidade.value == "") 
	{
		alert("Favor informar finalidade do Sistema.");
		document.formulario.finalidade.focus();
		return;
	}
	
	if (document.formulario.relacionado.value == "") 
	{
		alert("Favor informar relacionamento do Sistema.");
		document.formulario.relacionado.focus();
		return;
	}
	
	if (document.formulario.publico.value == "") 
	{
		alert("Favor informar p�blico alvo do Sistema.");
		document.formulario.publico.focus();
		return;
	}
	
//	if (document.formulario.diret.value == "") 
//	{
//		alert("Sistema sem diretorio.");
//		document.formulario.diret.focus();
//		return;
//	}
//	
//	if (!strSpecial(document.formulario.diret.value)){
//		document.formulario.diret.focus();
//		return;
//	}

	if (document.formulario.plataforma.value == "")
	{
		alert("Favor informar plataforma do Sistema.");
		document.formulario.plataforma.focus();
		return;		
	}
//	alert(document.formulario.url.value.substr(0,7));
//    return;

	
	
	if (document.formulario.plataforma.value == 1){	 
		if (document.formulario.url.value == "http:\\\\" || document.formulario.url.value == "")  {
			alert("Plataforma WEB � obrigat�rio informar URL.");
			document.formulario.url.focus();
			return;
		}
	}

	if (document.formulario.tecnologia.value == "")
	{
		alert("Favor informar tecnologia do Sistema.");
		document.formulario.tecnologia.focus();
		return;		
	}

	if (document.formulario.tecnologia.value == 6){	 
		if (document.formulario.outras.value == "")  {
			alert("Tecnologia 'Outras' � obrigat�rio informar especifica��o.");
			document.formulario.outras.focus();
			return;
		}
	}

	
	if (document.formulario.situacao.value == "")
	{
		alert("Favor informar situa��o do Sistema.");
		document.formulario.situacao.focus();
		return;		
	}

	if (document.formulario.situacao.value == 4) {
		if (document.formulario.motivo.value == "") 
	{
			alert("Situa��o 'Suspenso' � obrigat�rio informar Motivo.");
			document.formulario.motivo.focus();
			return;
		}
	}

	if (document.formulario.situacao.value == 2) {
		if (document.formulario.qtdeusuarios.value == 0)
		{
			alert("Situa��o 'Em Produ��o' � obrigat�rio informar a quantidade de usu�rios.");
			document.formulario.qtdeusuarios.focus();
			return;		
		}
	}
	
	if (document.formulario.implantacao.value == "")
	{
		alert("Favor informar data implanta��o do Sistema.");
		document.formulario.implantacao.focus();
		return;		
	}
	var mes = document.formulario.implantacao.value.substring(0,2);
	var ano = document.formulario.implantacao.value.substring(3,7);
	
	if (mes < 1 || mes > 12){
		alert("Favor informar data de implanta��o com 'm�s' v�lido.");
		document.formulario.implantacao.focus();
		return;		
	}		

	if (ano > 2030 || ano < 1980){
		alert("Favor informar data de implanta��o com 'ano' v�lido.");
		document.formulario.implantacao.focus();
		return;		
	}		
	
	if (document.formulario.orgao.value == "")
	{
		alert("Favor informar �rg�o respons�vel pelo Sistema.");
		document.formulario.orgao.focus();
		return;		
	}
	
	if (document.formulario.email.value == "")
	{
		alert("Favor informar e-mail.");
		document.formulario.email.focus();
		return;
	}

	if ((document.formulario.publicar[0].checked == 0) && (document.formulario.publicar[1].checked == 0)) 
	{
		alert("Favor informar op��o de publicar em relat�rio.");
		document.formulario.publicar.focus();
		return;
	}

	
//	if (document.formulario.paginainicial.value == "") 
//	{
//		alert("Sistema sem p�gina inicial.");
//		document.formulario.paginainicial.focus();
//		return;
//	}
	/*
	if(document.formulario.tel.value == "")
	{
		alert("Sistema sem telefone.");
		document.formulario.tel.focus();
		return;
	}
	
	if(document.formulario.fax.value == "")
	{
		alert("Sistema sem fax.");
		document.formulario.fax.focus();
		return;
	}
	*/
//	if ((document.formulario.exerc[0].checked == 0) && (document.formulario.exerc[1].checked == 0)) 
//	{
//		alert("Sistema sem exerc�cio.");
//		document.formulario.exerc.focus();
//		return;
//	}

	/*
	if ((document.formulario.status[0].checked == 0) && (document.formulario.status[1].checked == 0)) 
	{
		alert("Favor informar status do cadastro.");
		document.formulario.status.focus();
		return;
	}
	*/
	
	if (cod == 'I') document.formulario.act.value = 'inserir'; else document.formulario.act.value = 'alterar';
    document.formulario.submit();

}

/*
* Faz requisi��o via ajax
* Filtra o tipo de damanda, atrav�z do parametro passado 'ordid'
*/
function filtraPlataforma(sipid) {
	
	trUrl  = d.getElementById('tr_url');
	tdUrl  = d.getElementById('td_url');

	trUrl.style.display = 'none';
	tdUrl.style.display = 'none';

	if(sipid == 1){ //Web
		trUrl.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
		tdUrl.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
	}
}

function filtraSituacao(ssiid) {
	
	trMotivo  = d.getElementById('tr_motivo');
	tdMotivo  = d.getElementById('td_motivo');
	trQtde	  = d.getElementById('tr_qtde');
	tdQtde 	  = d.getElementById('td_qtde');
	

	trMotivo.style.display = 'none';
	tdMotivo.style.display = 'none';
	trQtde.style.display = 'none';
	tdQtde.style.display = 'none';
	

	if(ssiid == 2){ //Em Produ��o
		trQtde.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
		tdQtde.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
	}

	if(ssiid == 4){ //Suspenso
		trMotivo.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
		tdMotivo.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
	}
	
}

function filtraTecnologia(sitid) {
	
	trOutras  = d.getElementById('tr_outras');
	tdOutras  = d.getElementById('td_outras');

	trOutras.style.display = 'none';
	tdOutras.style.display = 'none';

	if(sitid == 6){ //Outras
		trOutras.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
		tdOutras.style.display = navigator.appName == 'Netscape' ? 'table-row' : 'block';
	}
}


function pesq(){
	if(document.formulario.dsc.value == '' && document.formulario.abrev.value == ''){
		alert ('Informe o Sistema ou a Abrevia��o.');
		document.formulario.dsc.focus();
		return;
	}
	document.formulario.pesquisa.value="1";
    document.formulario.submit();
}


</script>
