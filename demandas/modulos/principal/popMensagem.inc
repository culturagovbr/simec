<html>
<head>
		<title>Mensagem de Aviso</title>
		<style type="text/css">
			
			@media print {.notprint { display: none }}

	
		</style>

		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
		
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
		<tr bgcolor="#ffffff">
			<td width="4%" style="border-bottom: solid 0px black;">
				<img src="../imagens/brasao.gif" width="45" height="45" border="0">
			</td>
			<td style="border-bottom: solid 0px black;">
				SIMEC- Sistema Integrado do Minist�rio da Educa��o<br/>
				Mensagem de Aviso<br/>
			</td>

			<td align="right" style="border-bottom: solid 0px black;">
				Usu�rio: <? echo $_SESSION['usunome']; ?><br/>
				Data: 04/06/2009<?// echo date("d/m/Y  G:i:s"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<b><font style="font-size:14px;">AVISO</font></b>
			</td>
		</tr>
	</table> 
	


<table width="100%" align="center" border="0"  cellpadding="3" cellspacing="0">
	<tr>
		<td style="padding:15px; background-color:#fafafa; color:#404040; border-top: 1px solid black;" >
			<li>
				Por motivos de seguran�a, informamos que solicita��es destinadas � rede MEC (cria��o de conta de novo usu�rio, acesso a diret�rios em servidores, etc..) devem partir do respons�vel pelo setor, repassando as seguintes informa��es:
			</li>
			
			<br><br>
			
			<div style="padding-left:15px;">
				<b>Cria��o de conta de usu�rio:</b>
				<br>
				<li>Nome completo e localiza��o (sala e ramal) do usu�rio;</li>
				<li>Per�odo de utiliza��o da conta;</li>
				<br>
				Ex.: Solicito cria��o de conta de e-mail e login para a funcion�ria: Carla da Silva Mota (SESU � 3 � andar � Sala 00 � ramal: 5555), a mesma � estagi�ria e seu contrato expira em 06 meses.
			</div>
			
			<br>

			<div style="padding-left:15px;">
				<b>Cria��o de e-mail institucional:</b>
				<br>
				<li>Qual o nome do e-mail a ser criado;</li>
				<li>Definir qual a utiliza��o do mesmo, ou seja, o motivador;</li>
				<li>Nome completo e localiza��o (sala e ramal) dos usu�rios que dever�o ter acesso ao e-mail;</li>
				<li>Per�odo de utiliza��o do e-mail;</li>
				<br>
				Ex.: Solicito a cria��o do e-mail institucional educa��o@mec.gov.br, devido ao Programa de Educa��o, que ser� utilizado at� o dia 31/12/2009. Segue a lista dos usu�rios que dever�o ter acesso ao mesmo: Carla da Silva Mota (SESU � 3 � andar � Sala 00 � ramal: 5555) e Evandro Silva Torres (SESU � 3 � andar � Sala 00 � ramal: 5555).
			</div>
			
			<br>

			<div style="padding-left:15px;">
				<b>Acesso a diret�rios:</b>
				<br>
				<li>Nome completo e localiza��o (sala e ramal) do usu�rio;</li>
				<li>Caminho completo do diret�rio;</li>
				<li>O tipo de acesso de cada usu�rio (somente leitura ou acesso total);</li>
				<br>
				Ex.: Solicito acesso de leitura � pasta: \\mecsrv02\dti\programas\testes para a usu�ria Carla da Silva Mota (SESU � 3 � andar � Sala 00 � ramal: 5555).
				<br><br>
				Obs.: Os solicitantes de cada demanda ser�o respons�veis por todos os servi�os e acessos de rede solicitados.
				<br>
				<li>Informamos que as demandas que n�o seguirem essa padroniza��o ser�o desconsideradas, por�m, o solicitante ser� informado por telefone ou e-mail.</li>
				<li>Esse tipo de iniciativa, informando todos os dados solicitados pela Rede, n�o s� agiliza o atendimento de cada chamado, mas ajuda a manter um controle m�nimo com rela��o � Seguran�a de Rede.</li>
			</div>
		</td>
	</tr>
	<tr>
		<td style="background-color:#fafafa; color:#404040; font-weight:bold; font-size: 10px; border-bottom: 1px solid black;" >
			&nbsp;
		</td>
	</tr>
</table>
		
</body>
</html>		

