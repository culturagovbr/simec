<?php

/**
 * Controle responsavel pelas entidades.
 *
 * @author Equipe simec - Consultores OEI
 * @since  17/10/2013
 *
 * @name       Board
 * @package    classes
 * @subpackage controllers
 * @version    $Id
 */
class Controller_diagnostico extends Abstract_Controller
{
    public function indexAction()
    {
        $this->render(__CLASS__, __FUNCTION__);
    }

    public function exibirAction()
    {
        $this->render(__CLASS__, __FUNCTION__);
    }

    public function formularioAction()
    {
        $this->render(__CLASS__, __FUNCTION__);
    }


}