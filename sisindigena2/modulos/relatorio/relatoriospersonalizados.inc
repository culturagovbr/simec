<?php

include_once "_funcoes.php";

$perfis = pegaPerfilGeral();

if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - SISInd�gena", "" );

$relatorios = array(//0=>array('codigo'=>'relatpersonal_erroscadastrossgb.inc','descricao'=>'Relat�rio de erros no cadastro de bolsistas no SGB'),
					//1=>array('codigo'=>'relatpersonal_aceitacaotermocompromisso.inc','descricao'=>'Relat�rio de ades�o ao termo de compromisso'),
					//2=>array('codigo'=>'relatpersonal_errospagamentossgb.inc','descricao'=>'Relat�rio de erros no envio de pagamentos no SGB'),
					//3=>array('codigo'=>'relatpersonal_bolsistasparados.inc','descricao'=>'Relat�rio de Bolsistas sem tramita��o do pagamento'),
					0=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do SISInd�gena'),
					1=>array('codigo'=>'relatpersonal_relatorioabrangencia.inc','descricao'=>'Relat�rio de abrang�ncia nos Munic�pios'),
					2=>array('codigo'=>'relatpersonal_relatorioabrangencia2.inc','descricao'=>'Relat�rio de abrang�ncia geral'),
					//8=>array('codigo'=>'relatpersonal_professorespacto.inc','descricao'=>'Professores participando do Pacto'),
					//9=>array('codigo'=>'relatpersonal_aprendizagem.inc','descricao'=>'Informa��es sobre aprendizagem'),
					//10=>array('codigo'=>'relatpersonal_turmasformadores.inc','descricao'=>'Turmas de formadores'),
					//11=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Quantitativos do SISPACTO'),
					//2=>array('codigo'=>'relatpersonal_dataplanoatv.inc','descricao'=>'Relat�rio das datas dos planos de atividades'),
					//13=>array('codigo'=>'relatpersonal_gestaomobilizacao.inc','descricao'=>'Relat�rio personalizados Gest�o e Mobiliza��o'),
					//14=>array('codigo'=>'relatpersonal_evasao.inc','descricao'=>'Relat�rio de evas�o do perfis'),
					//15=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
					//16=>array('codigo'=>'relatpersonal_acompanhamentogeral.inc','descricao'=>'Relat�rio de Acompanhamento Geral'),
					//17=>array('codigo'=>'relatpersonal_relatoriofinal.inc','descricao'=>'Situa��o dos Relat�rios Finais')
		
					
					);
	


?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadossisindigena';
		var janela = window.open( '', 'relatoriopersonlizadossisindigena', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '400', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>