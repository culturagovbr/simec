<?
if(!$_SESSION['sisindigena2'][$sis]['uncid']) {
	$al = array("alert"=>"Usu�rio n�o vinculado com nenhuma universidade","location"=>"sisindigena2.php?modulo=inicio&acao=C");
	alertlocation($al);
}
?>
<script>
function wf_exibirHistorico( docid )
{
	var url = 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/geral/workflow/historico.php' +
		'?modulo=principal/tramitacao' +
		'&acao=C' +
		'&docid=' + docid;
	window.open(
		url,
		'alterarEstado',
		'width=675,height=500,scrollbars=yes,scrolling=no,resizebled=no'
	);
}

function abrirDetalhes(id) {
	if(document.getElementById('img_'+id).title=='mais') {
		document.getElementById('tr_'+id).style.display='';
		document.getElementById('img_'+id).title='menos';
		document.getElementById('img_'+id).src='../imagens/menos.gif'
	} else {
		document.getElementById('tr_'+id).style.display='none';
		document.getElementById('img_'+id).title='mais';
		document.getElementById('img_'+id).src='../imagens/mais.gif'

	}

}

</script>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="10" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2">Acompanhamento de avalia��es e bolsas</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao("/sisindigena2/sisindigena2.php?modulo=principal/{$sis}/".(($sis=='universidade')?'universidadeexecucao':(($sis=='coordenadorlocal')?'coordenadorlocalexecucao':$sis))."&acao=A&aba=acompanhamentoavaliacaobolsas"); ?></td>
</tr>
<tr>
	<td colspan="2">
	<?
		
	$sql = "SELECT DISTINCT f.fpbid as codigo, 
				   'Ref. ' || m.mesdsc || ' / ' || fpbanoreferencia as descricao,
				   COALESCE((
				   	SELECT e.esddsc || ' ( ' || to_char(h.htddata,'dd/mm/YYYY HH24:MI') || ' )' as s FROM sisindigena2.pagamentobolsista p 
					INNER JOIN workflow.documento d ON d.docid = p.docid 
					INNER JOIN workflow.estadodocumento e ON e.esdid = d.esdid 
					LEFT JOIN workflow.historicodocumento h ON h.hstid = d.hstid 
					WHERE p.iusd='".$_SESSION['sisindigena2'][$sis]['iusd']."' AND p.fpbid=f.fpbid
					),'') as statuspagamento,
				    (fpbanoreferencia::text||'-'||lpad(fpbmesreferencia::text, 2, '0')||'-01')::date as peri 
			FROM sisindigena2.folhapagamento f 
			INNER JOIN sisindigena2.folhapagamentouniversidade rf ON rf.fpbid = f.fpbid 
			INNER JOIN public.meses m ON m.mescod::integer = f.fpbmesreferencia
			WHERE rf.uncid='".$_SESSION['sisindigena2'][$sis]['uncid']."' ".(($_SESSION['sisindigena2'][$sis]['picid'])?" AND rf.picid='".$_SESSION['sisindigena2'][$sis]['picid']."'":"")." AND to_char(NOW(),'YYYYmmdd')>=to_char((fpbanoreferencia::text||lpad(fpbmesreferencia::text, 2, '0')||'01')::date,'YYYYmm') ORDER BY peri";
	
	$folhapagamento = $db->carregar($sql);
	
	if($folhapagamento[0]) {
		
		echo '<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="8" align="center">';
		echo '<tr><td class="SubTituloEsquerda" colspan="2">Extrato de pagamento/avalia��es</td></tr>';
		echo '<tr><td class="SubTituloCentro" width="50%">Parcela</td><td class="SubTituloCentro" width="50%">Situa��o pagamento(Data de atualiza��o)</td></tr>';
		
		foreach($folhapagamento as $fl) {
			
			echo '<tr><td class="SubTituloEsquerda">'.$fl['descricao'].'</td>
					  <td><font size=3><b>'.$fl['statuspagamento'].'</b></font></td></tr>';
			echo '<tr><td colspan="2"><img src="../imagens/mais.gif" style="cursor:pointer;" title="mais" id="img_ava_'.$fl['codigo'].'" onclick="abrirDetalhes(\'ava_'.$fl['codigo'].'\');"> Detalhes da avalia��o</td></td></tr>';
			echo '<tr style="display:none;" id="tr_ava_'.$fl['codigo'].'"><td colspan="2">';
			echo '<p align="center"><b>INFORMA��ES SOBRE AVALIA��ES</b></p>';
			
			$sql = "SELECT * FROM sisindigena2.mensario WHERE fpbid='".$fl['codigo']."' AND iusd='".$_SESSION['sisindigena2'][$sis]['iusd']."'";
			$mensario = $db->pegaLinha($sql);
			
			if($mensario['menid']) consultarDetalhesAvaliacoes(array('menid'=>$mensario['menid']));
			else echo '<p align=center style=color:red;>N�o existem avalia��es nesse per�odo de refer�ncia</p>';
			
			echo '</td></tr>';
			echo '<tr><td colspan="2"><img src="../imagens/mais.gif" style="cursor:pointer;" title="mais" id="img_pag_'.$fl['codigo'].'" onclick="abrirDetalhes(\'pag_'.$fl['codigo'].'\');"> Detalhes do pagamento</td></td></tr>';
			echo '<tr style="display:none;" id="tr_pag_'.$fl['codigo'].'"><td colspan="2">';
			echo '<p align="center"><b>INFORMA��ES SOBRE PAGAMENTO</b></p>';
			
			$sql = "SELECT pboid FROM sisindigena2.pagamentobolsista WHERE fpbid='".$fl['codigo']."' AND iusd='".$_SESSION['sisindigena2'][$sis]['iusd']."'";
			$pboid = $db->pegaUm($sql);
			
			if($pboid) {
				consultarDetalhesPagamento(array('pboid'=>$pboid));
			} else {
				echo "<p align=center style=color:red;>N�o existem pagamentos nesse per�odo de refer�ncia</p>";	
				
				$restricao = pegarRestricaoPagamento(array('iusd' => $_SESSION['sisindigena2'][$sis]['iusd'], 'fpbid' => $fl['codigo']));
				
				echo "<table class=\"listagem\" bgcolor=\"#f5f5f5\" cellSpacing=\"5\" cellPadding=\"10\" align=\"center\">";
				echo "<tr>";
				echo "<td class=\"SubTituloDireita\"><b>Poss�vel restri��o:</b></td>";
				echo "<td><b>".$restricao."</b></td>";
				echo "</tr>";
				echo "</table>";
			} 
			
			
			echo '</td></tr>';
			
		}
		
		//echo '<tr><td class="SubTituloCentro" colspan="2"><input type=button value=Voltar onclick="window.location=\'sisindigena2_consulta_pagamento.php\';"></td></tr>';
		
		echo '</table>';
		echo '<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="8" align="center">';
		echo '<tr><td colspan="2"><p>Prezado bolsista, ap�s ser APROVADO no fluxo de avalia��o, o pagamento das bolsas no �mbito do Pacto Nacional pela Alfabetiza��o na Idade Certa obedece ao seguinte fluxo:</p></td></tr>';
		echo '<tr><td class="SubTituloCentro">Status de pagamento</td><td class="SubTituloCentro">Descri��o</td></tr>';
		echo '<tr><td>Aguardando autoriza��o IES</td><td>O bolsista foi avaliado e considerado apto a receber a bolsa. A libera��o do pagamento est� aguardando autoriza��o final pela Universidade respons�vel pela forma��o.</td></tr>';
		echo '<tr><td>Autorizado IES</td><td>O pagamento da bolsa foi autorizado pela Universidade e est� sendo processado pelos sistemas do MEC.</td></tr>';
		echo '<tr><td>Aguardando autoriza��o SGB</td><td>O pagamento da bolsa est� no Sistema de Gest�o de Bolsas, aguardando autoriza��o do MEC.</td></tr>';
		echo '<tr><td>Aguardando pagamento</td><td>O pagamento da bolsa foi autorizado pelo SGB e est� em processamento.</td></tr>';
		echo '<tr><td>Enviado ao Banco</td><td>A ordem banc�ria referente ao pagamento da bolsa foi emitida. O pagamento estar� dispon�vel para saque em at� 02 dias �teis, em fun��o do processamento da O.B. pelo banco</td></tr>';
		echo '<tr><td>Pagamento efetivado</td><td>O pagamento foi creditado em conta e confirmado pelo banco.</td></tr>';
		echo '<tr><td>Pagamento n�o autorizado FNDE</td><td>O pagamento da bolsa n�o foi autorizado pelo FNDE, pois o bolsista recebe bolsa de outro programa do MEC.</td></tr>';
		echo '<tr><td>Pagamento recusado</td><td>Pagamento recusado em fun��o de algum erro de registro. Ser� reencaminhado a IES respons�vel pela forma��o.</td></tr>';
		echo '<tr><td colspan="2"><p>Observa��o: Caso o seu status no fluxo de pagamento esteja em BRANCO, significa que o m�s de referencia ainda n�o teve o seu fluxo de avalia��o conclu�do. Voc� deve procurar a coordena��o local do PACTO ou a IES respons�vel pela forma��o do seu munic�pio.</p></td></tr>';
		echo '</table>';
		
	} else {
		$al = array("alert"=>"A universidade do Usu�rio n�o possui per�odo de refer�ncia atribu�do","location"=>"sisindigena2.php?modulo=inicio&acao=C");
		alertlocation($al);
	}

	?>
	</td>
</tr>

<tr>
	<td class="SubTituloDireita" width="20%">&nbsp;</td>
	<td>
	<? if($goto_ant) : ?>
	<input type="button" value="Anterior" onclick="divCarregando();window.location='<?=$goto_ant ?>';">
	<? endif; ?>
	<? if($goto_pro) : ?>
	<input type="button" value="Pr�ximo" onclick="divCarregando();window.location='<?=$goto_pro ?>';">
	<? endif; ?>
	</td>
</tr>
</table>