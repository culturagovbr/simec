<?
monta_titulo( "Orienta��es - Gest�o", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10"	align="center">
<tr>
	<td width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue">
	<p>A melhoria da gest�o escolar � o foco desta metodologia de planejamento e, apesar de envolver todos os aspectos mencionados neste diagn�stico, h� uma grande expectativa de que a equipe gestora promova mudan�as qualitativas na condu��o da escola.</p> 
	<p>O(a) diretor(a) � a lideran�a formal, mas pode ser muito mais do que isso. Gestores din�micos e comprometidos tendem a produzir bons resultados, mas estes depender�o tamb�m, em grande medida, da capacidade dele(a) de aperfei�oar processos, compartilhar responsabilidades, gerir recursos e motivar pessoas. Mas ele(a) precisa ser apoiado nesta tarefa pelos demais membros da equipe, notadamente aqueles que participam diretamente da gest�o da escola.</p>
	<p>Um bom diagn�stico precisa identificar se a equipe gestora est� bem preparada para realizar suas atividades. � o que vamos fazer nesta tela, inserindo informa��es relacionadas ao perfil da equipe gestora e dos colaboradores que lhe d�o suporte.</p>
	<p>Preencha o quadro abaixo com todas as informa��es solicitadas. Observe que, excetuando o perfil "Diretor(a)", poder�o ser inseridos tantos nomes quantas forem as pessoas que desempenham as demais fun��es (vice-diretor(a), secret�rio(a) da escola e equipe pedag�gica). Caso n�o exista algum dos perfis indicados, assinale a op��o �N�o existe�. Para inserir nomes, clique no bot�o "Incluir". Caso deseje modificar os dados inseridos, clique em "Editar". Se desejar excluir algum nome, clique em "Excluir".</p>
	<p>Depois de preencher o quadro, responda �s perguntas. Tamb�m neste caso, quando as respostas �sempre� e �na maioria das vezes� somarem mais de 50% em rela��o ao total de perguntas, � necess�rio que a escola apresente, no final da tela, justificativas ou evid�ncias sobre esses aspectos, �s luz dos resultados apresentados nas dimens�es 1 e 2. Ou seja, se os desafios da gest�o foram superados, porque os resultados da escola n�o s�o melhores?</p>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="anterior" value="Anterior" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_3_sintesedimensao3';">
	<input type="button" name="proximo" value="Pr�ximo" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_4_gestao&aba1=diagnostico_4_1_direcao';"> 
	</td>
</tr>
</table>