<?
monta_titulo( "Orienta��es - PDE Escola", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
<tr>
	<td class="blue" width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue" >
	<p>Agora que o Grupo de Trabalho j� concluiu o Diagn�stico e conhece bem a situa��o da escola, seus principais problemas e poss�veis causas, chegou a hora de dar continuidade ao Plano de Desenvolvimento da Escola definindo os Objetivos, Metas, Estrat�gias e A��es. Ou seja, o caminho que dever� ser perseguido para superar os desafios e alcan�ar os melhores resultados.</p>
	<p>Antes de avan�ar, � fundamental entender o que significa cada informa��o:</p>
	<p>a) Os Objetivos s�o as situa��es que a escola deseja alcan�ar num dado espa�o de tempo (2 anos, neste caso), devem refletir os compromissos essenciais da escola e onde ela concentrar� os seus esfor�os. Deve responder � quest�o "O que desejamos?";</p>
	<p>b) As Estrat�gias surgem a partir dos Objetivos e definem os caminhos para a escola alcan��-los. Deve responder, de forma ampla, � pergunta "Como fazer para alcan�ar este Objetivo?";</p>
	<p>c) As Metas definem os resultados quantitativos que devem ser atingidos naquele per�odo (2 anos) para que os Objetivos sejam alcan�ados. Para tanto, � importante conhecer o p�blico que ser� afetado pela meta e responder � quest�o "Qual o patamar/ n�vel/ indicador desejado?" e ser expresso em n�meros;</p>
	<p>d) As A��es refletem o que a escola far�, efetivamente, para alcan�ar as metas e objetivos. Elas podem ser feitas com ou sem recursos financeiros, mas precisam estar bem descritas para que a equipe envolvida na execu��o do plano possa cumpri-lo de forma satisfat�ria.</p>
	<p>Neste etapa, o Grupo de Trabalho dever� definir, � luz dos problemas identificados e dos objetivos previamente definidos pelo MEC para cada m�dulo, quais as metas que a escola perseguir� para melhorar os seus resultados, as estrat�gias que ser�o adotadas e a��es a serem realizadas. Vamos adiante!</p>
	<p>O Plano de Desenvolvimento da Escola deve englobar a��es financi�veis e n�o financi�veis. Entretanto, para elaborar um plano coerente � necess�rio que a escola informe a previs�o de recursos dispon�veis para os pr�ximos exerc�cios.</p>
	<p>Se a escola foi priorizada pelo PDE Escola, esses valores j� aparecem automaticamente e os bens e servi�os a serem adquiridos com cada parcela n�o podem superar esse limite. Tampouco podem ser superiores aos valores de capital e custeio.</p>
	<p>Caso a escola n�o tenha sido priorizada pelo PDE Escola, � necess�rio inserir a estimativa de recursos para este e o pr�ximo exerc�cio. Neste caso, preencha os campos ao lado e clique em "Salvar". Lembre-se que este ser� o limite de valor das a��es financi�veis em cada categoria de despesa (capital e custeio). Para alterar o valor depois de salvar a tela, clique no s�mbolo  , atualiza a informa��o e clique novamente em "Salvar".</p>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="anterior" value="Anterior" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_7_sintese';">
	<input type="button" name="proximo" value="Pr�ximo" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_1_priorizacao_problemas';">
	</td>
</tr>
</table>