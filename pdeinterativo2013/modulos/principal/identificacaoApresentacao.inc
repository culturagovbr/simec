<?php
monta_titulo( "Apresenta��o", '&nbsp' );
?>
<link rel="stylesheet" type="text/css" href="../pdeinterativo2013/css/pdeinterativo2013.css"/>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
	<tr>
		<td>
			<img border="0" src="/imagens/bussola.png" />
		</td>
		<td>
		<font style="color:#1719fd;">
		Seja bem vindo(a) ao PDE Interativo!
		<BR>
		O PDE Interativo � uma ferramenta de apoio � gest�o escolar desenvolvida pelo Minist�rio da Educa��o, em parceria com as Secretarias de Educa��o. Ele foi concebido a partir da metodologia de planejamento estrat�gico utilizada pelo PDE Escola e sua formula��o teve como principal objetivo universalizar a metodologia, tornando-a acess�vel a todas as escolas e secretarias interessadas.
		<BR><br>
		Uma inova��o importante desta ferramenta � o seu car�ter auto-instrucional, o que significa que n�o � necess�rio realizar uma forma��o espec�fica para conhecer a metodologia. Basta ler as orieta��es dispon�veis em cada tela e refletir coletivamente sobre os problemas e desafios identificados. O PDE Interativo tamb�m possibilita que as Secretarias e o Minist�rio da Educa��o conhe�am melhor as escolas e, a partir da�, proponham pol�ticas p�blicas cada vez mais aderentes �s necessidades delas.
		<BR><BR>
		Antes de iniciar a navega��o e elabora��o do plano, � importante entender como o sistema est� organizado:
		<BR><BR>
		1) A metodologia do PDE Interativo envolve quatro grandes etapas: Identifica��o, Primeiros Passos, Diagn�stico e Plano Geral.
		<BR>
		2) O Plano Geral est� dividido em planos espec�ficos e este ano a escola poder� visualizar o Plano de Forma��o (dispon�vel para todas as escolas) e o PDE Escola (dispon�vel apenas para as escolas priorizada por esse programa).
		<BR>
		3) Esses dois planos (PDE Escola e Plano de Forma��o) possuem fluxos de an�lise independentes. Ou seja, n�o � obrigat�rio concluir o PDE Escola para enviar o Plano de Forma��o e vice-versa.
		<BR><BR>
		Buscar orienta��es junto � secretaria estadual ou municipal de educa��o por meio do comit� estrat�gico. Os nomes e contatos dos integrantes desse comit� s�o exibidos no "passo 3" da aba "Primeiros passos".
		<BR>
		Em caso de d�vida, ligue para 0800 616161 ou envie um e-mail para pdeinterativo@mec.gov.br.
		</font>
		</td>
	</tr>
</table>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="2" cellPadding="5" align="center" style="border-top:1px solid black;">
	<tr>
		<td align="right" class="SubTituloDireita" width="350px">
			O que deseja fazer agora?
		</td>
		<td>
			<input type="button" name="btn_anterior" value="Anterior" onclick="window.location='pdeinterativo2013.php?modulo=principal/principal&acao=A'" >
			<input type="button" name="btn_proximo" value="Pr�ximo" onclick="window.location='pdeinterativo2013.php?modulo=principal/identificacao&acao=A&aba=Diretor'" >
		</td>
	</tr>
</table>