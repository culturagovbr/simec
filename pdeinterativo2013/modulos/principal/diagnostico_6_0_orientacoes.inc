<?
monta_titulo( "Orienta��es - Infraestrutura", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10"	align="center">
<tr>
	<td width="20%" ><img src="../imagens/bussola.png"></td>
	<td class="blue" >
	<p>Chegamos � sexta e �ltima dimens�o do Diagn�stico, no qual vamos identificar aspectos relacionados �s condi��es das instala��es e dos equipamentos existentes na escola. Importante destacar que a infraestrutura costuma ser um aspecto muito valorizado pela comunidade escolar e, n�o raro, suas defici�ncias s�o apontadas como principal causa para o eventual insucesso dos alunos, o que nem sempre corresponde � realidade.</p>
	<p>� claro que todos desejam e merecem conviver em espa�os que possuam instala��es adequadas e confort�veis, com condi��es apropriadas para realizar atividades estimulantes e promover um clima escolar agrad�vel. Mas sabemos que o processo de ensino e aprendizagem exige bem mais do que isso e associar a melhoria da infraestrutura � melhoria dos resultados da escola pode ser um equ�voco grave.</p>
	<p>Al�m disso, considere que o planejamento estrat�gico focaliza aspectos que est�o sob a governabilidade de quem planeja, o que geralmente n�o acontece com a infraestrutrua. Dito de outra forma: a melhoria das instala��es da escola � compet�ncia da Secretaria de Educa��o, o que n�o impede a escola de identificar os problemas mais graves.</p>
	<p>Preencha os quadros conforme os comandos e, em seguida, assinale em que medida o Grupo de Trabalho concorda com cada pergunta.</p>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td width="20%" class="SubTituloDireita">O que deseja fazer agora?</td>
	<td>
		<input type="button" name="btn_anterior" value="Anterior" onclick="window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_5_comunidadeescolar&aba1=diagnostico_5_5_sintesedimensao5'" >
		<input type="button" name="btn_proximo" value="Pr�ximo" onclick="window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_6_infraestrutura&aba1=diagnostico_6_1_instalacoes'" >
	</td>
</tr>
</table>