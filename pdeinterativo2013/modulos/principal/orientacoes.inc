<?php

if( !$_SESSION['pdeinterativo2013_vars']['usucpfdiretor'] )
{
	echo '<script>
			alert("Voc� deve ter perfil de Diretor(a) para acessar esta p�gina.");
			window.location.href = "pdeinterativo2013.php?modulo=principal/principal&acao=A";
		  </script>';
	exit;
}

$arrDados = recuperaDadosEscolaPorCPFDiretor($_SESSION['pdeinterativo2013_vars']['usucpfdiretor']);

cabecalhoPDEInterativo2013($arrDados);
echo monta_titulo("Primeiros Passos", "Orienta��es");

?>

<script type="text/javascript">

function abreJanela()
{
	var janela = window.open('pdeinterativo2013.php?modulo=principal/popOrientacoes&acao=A', 'popOrientacoes', 'width=500,height=280,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1');
	janela.focus();
}

</script>

<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
	<tr>
		<td>
			<img border="0" src="/imagens/bussola.png" />
		</td>
		<td>
		<font style="color:#1719fd;">
		<p>Agora que j� identificamos a escola e o(a) gestor(a) da escola, vamos executar os Primeiros Passos para o planejamento. Para tanto, � necess�rio que a escola e a Secretaria de Educa��o adotem algumas provid�ncias essenciais para que a elabora��o do plano seja efetivamente democr�tica e participativa.</p> 
		<p>Esta etapa � muito importante e o seu cumprimento � obrigat�rio, pois ela organiza o ambiente institucional para a constru��o do planejamento. Para saber quais s�o os primeiros passos <a href="#" onclick="abreJanela();">clique aqui</a>..</p>
		</font>
		</td>
	</tr>
</table>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="2" cellPadding="5" align="center" style="border-top:1px solid black;">
	<tr>
		<td align="right" class="SubTituloDireita" width="350px">
			O que deseja fazer agora?
		</td>
		<td>
			<input type="button" name="btn_anterior" value="Anterior" onclick="window.location='pdeinterativo2013.php?modulo=principal/identificacao&acao=A&aba=Galeria'" >
			<input type="button" name="btn_proximo" value="Pr�ximo" onclick="window.location='pdeinterativo2013.php?modulo=principal/primeirospassos&acao=A&aba=primeiropasso'" >
		</td>
	</tr>
</table>