<script>



function diagnostico_1_3_provabrasil() {


	if(document.getElementById('rpbinicialport_S')) {
		var checked = false;
	
		for(var i=0;i<document.getElementsByName('rpbinicialport').length;i++) {
			if(document.getElementsByName('rpbinicialport')[i].checked) {
				checked = true;
			}
		}
		
		if(checked == false) {
			alert('Marque a op��o: Os resultados de L�ngua Portuguesa na Prova Brasil nos anos iniciais do Ensino Fundamental demonstram evolu��o nos �ltimos anos?');
			return false;		
		}
		
		var checked = false;
		
		for(var i=0;i<document.getElementsByName('rpbinicialmat').length;i++) {
			if(document.getElementsByName('rpbinicialmat')[i].checked) {
				checked = true;
			}
		}
		
		if(checked == false) {
			alert('Marque a op��o: Os resultados de Matem�tica na Prova Brasil nos anos iniciais do Ensino Fundamental demonstram evolu��o nos �ltimos anos?');
			return false;		
		}
		
	}
	
	if(document.getElementById('rpbfinalport_S')) {
		var checked = false;
	
		for(var i=0;i<document.getElementsByName('rpbfinalport').length;i++) {
			if(document.getElementsByName('rpbfinalport')[i].checked) {
				checked = true;
			}
		}
		
		if(checked == false) {
			alert('Marque a op��o: Os resultados de L�ngua Portuguesa na Prova Brasil nos anos finais do Ensino Fundamental demonstram evolu��o nos �ltimos anos?');
			return false;		
		}
		
		var checked = false;
		
		for(var i=0;i<document.getElementsByName('rpbfinalmat').length;i++) {
			if(document.getElementsByName('rpbfinalmat')[i].checked) {
				checked = true;
			}
		}
		
		if(checked == false) {
			alert('Marque a op��o: Os resultados de Matem�tica na Prova Brasil nos anos finais do Ensino Fundamental demonstram evolu��o nos �ltimos anos?');
			return false;		
		}
		
	}

	divCarregando();
	document.getElementById('formulario').submit();
}

</script>
<?
monta_titulo( "Prova Brasil - Indicadores e Taxas", "&nbsp;");

$sql = "SELECT * FROM pdeinterativo2013.respostaprovabrasil WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND rbdstatus='A'";
$dadospb = $db->pegaLinha($sql);

if($dadospb) {
	extract($dadospb);
	$style_display = '';
} else {
	$style_display = 'style="display:none"';
}

?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
<tr>
	<td class="SubTituloDireita" width="10%"><font class="blue">Orienta��es</font></td>
	<td class="blue">
	<p>Agora vamos analisar o �ltimo indicador desta Dimens�o. A Prova Brasil e o Sistema Nacional de Avalia��o da Educa��o B�sica (Saeb) s�o avalia��es para diagn�stico e avalia��o da qualidade do ensino oferecido pelo sistema educacional brasileiro. Nos testes aplicados na 4� (quarta) e 8� (oitava) s�ries (quinto e nono anos) do Ensino Fundamental e na 3� (terceira) s�rie do Ensino M�dio, os estudantes respondem a quest�es de L�ngua Portuguesa (com foco em leitura) e Matem�tica (com foco na resolu��o de problemas). No question�rio socioecon�mico, os estudantes fornecem informa��es sobre fatores de contexto que podem estar associados ao desempenho.</p>
	<p>As m�dias de desempenho nessas avalia��es ajudam a definir a��es de aprimoramento da qualidade da educa��o e tamb�m subsidiam o c�lculo do �ndice de Desenvolvimento da Educa��o B�sica (IDEB), ao lado das taxas de aprova��o nessas esferas.</p>
	<p>Clique no bot�o abaixo para visualizar a(s) tabela(s) e o(s) gr�fico(s) que mostram a s�rie hist�rica da Prova Brasil no Brasil, Estado, Munic�pio e os resultados da pr�pria escola, em cada etapa oferecida no ano do Censo.</p>
	<p>Analise os gr�ficos e, em seguida, responda �s perguntas. Caso a escola n�o possua resultados em pelo menos dois anos, assinale a op��o �N�o se aplica�, mas n�o deixe de comentar os resultados do seu munic�pio, estado e do Brasil.</p>
	
	<? if($style_display) : ?>
	<p>Clique no bot�o abaixo e veja os resultados do Brasil, Estado, Munic�pio e da sua escola na Prova Brasil, nas tr�s �ltimas medi��es. Analise a evolu��o desse indicador e, em seguida, responda �s perguntas.</p>
	<p><input type="button" name="verideb" value="CLIQUE AQUI para visualizar os dados da Prova Brasil" onclick="document.getElementById('div_provabrasil').style.display='';"></p>
	<? endif; ?> 
	</td>
</tr>
</table>
<div id="div_provabrasil" <?=$style_display ?> >
<form method="post" id="formulario">
<input type="hidden" name="togo" id="togo" value="/pdeinterativo2013/pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_4_sintesedimensao1">
<input type="hidden" name="requisicao" value="diagnostico_1_3_provabrasil">
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
<?
$possui_ai = possuiEnsino($ensino='I', $submodulo='P'); 
$possui_af = possuiEnsino($ensino='F', $submodulo='P');
?>
<? if($possui_ai) : ?>
<tr>
	<td class="SubTituloDireita" width="10%">Anos iniciais do Ensino Fundamental</td>
	<td width="30%" align="center">
	<? $dados_ai_p = montaTabelaProvaBrasil($ensino='I',$materia='P'); echo $dados_ai_p['html']; ?>
	<? $dados_ai_m = montaTabelaProvaBrasil($ensino='I',$materia='M'); echo $dados_ai_m['html']; ?>
	</td>
	<td width="30%" align="center">
	<img src="pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&requisicao=montaGraficoProvaBrasil&ensino=I&materia=P">
	<img src="pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&requisicao=montaGraficoProvaBrasil&ensino=I&materia=M">
	</td>
	<td width="30%" valign="top">
	<table width="100%">
		<tr>
		<td class="SubTituloCentro">Perguntas</td>
		<td class="SubTituloCentro">Sim</td>
		<td class="SubTituloCentro">N�o</td>
		<td class="SubTituloCentro">N�o se aplica</td>
		</tr>
		<tr>
		<td class="SubTituloDireita">Os resultados de L�ngua Portuguesa na Prova Brasil nos anos iniciais do Ensino Fundamental demonstram evolu��o nas duas �ltimas medi��es?</td>
		<td align="center"><input type="radio" name="rpbinicialport" id="rpbinicialport_S" value="S" <? echo (($rpbinicialport=="S")?"checked":""); echo $dados_ai_p['onclickperg']['S']; ?> ></td>
		<td align="center"><input type="radio" name="rpbinicialport" id="rpbinicialport_N" value="N" <? echo (($rpbinicialport=="N")?"checked":""); echo $dados_ai_p['onclickperg']['N']; ?> ></td>
		<td align="center"><input type="radio" name="rpbinicialport" id="rpbinicialport_A" value="A" <? echo (($rpbinicialport=="A")?"checked":""); echo $dados_ai_p['onclickperg']['A']; ?> ></td>
		</tr>
		<tr>
		<td class="SubTituloDireita">Os resultados de Matem�tica na Prova Brasil nos anos iniciais do Ensino Fundamental demonstram evolu��o nas duas �ltimas medi��es? </td>
		<td align="center"><input type="radio" name="rpbinicialmat" value="S" <? echo (($rpbinicialmat=="S")?"checked":""); echo $dados_ai_m['onclickperg']['S']; ?> ></td>
		<td align="center"><input type="radio" name="rpbinicialmat" value="N" <? echo (($rpbinicialmat=="N")?"checked":""); echo $dados_ai_m['onclickperg']['N']; ?> ></td>
		<td align="center"><input type="radio" name="rpbinicialmat" value="A" <? echo (($rpbinicialmat=="A")?"checked":""); echo $dados_ai_m['onclickperg']['A']; ?> ></td>
		</tr>

	</table>
	</td>
</tr>
<? endif; ?>
<? if($possui_af) : ?>
<tr>
	<td class="SubTituloDireita" width="10%">Anos finais do Ensino Fundamental</td>
	<td width="30%" align="center">
	<? $dados_af_p = montaTabelaProvaBrasil($ensino='F',$materia='P'); echo $dados_af_p['html']; ?>
	<? $dados_af_m = montaTabelaProvaBrasil($ensino='F',$materia='M'); echo $dados_af_m['html']; ?>
	</td>
	<td width="30%" align="center">
	<img src="pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&requisicao=montaGraficoProvaBrasil&ensino=F&materia=P">
	<img src="pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&requisicao=montaGraficoProvaBrasil&ensino=F&materia=M">
	</td>
	<td width="30%" valign="top">
	<table width="100%">
		<tr>
		<td class="SubTituloCentro">Perguntas</td>
		<td class="SubTituloCentro">Sim</td>
		<td class="SubTituloCentro">N�o</td>
		<td class="SubTituloCentro">N�o se aplica</td>
		</tr>
		<tr>
		<td class="SubTituloDireita">Os resultados de L�ngua Portuguesa na Prova Brasil nos anos finais do Ensino Fundamental demonstram evolu��o nas duas �ltimas medi��es?</td>
		<td align="center"><input type="radio" name="rpbfinalport" value="S" <? echo (($rpbfinalport=="S")?"checked":""); echo $dados_af_p['onclickperg']['S']; ?> ></td>
		<td align="center"><input type="radio" name="rpbfinalport" value="N" <? echo (($rpbfinalport=="N")?"checked":""); echo $dados_af_p['onclickperg']['N']; ?> ></td>
		<td align="center"><input type="radio" name="rpbfinalport" value="A" <? echo (($rpbfinalport=="A")?"checked":""); echo $dados_af_p['onclickperg']['A']; ?> ></td>
		</tr>
		<tr>
		<td class="SubTituloDireita">Os resultados de Matem�tica na Prova Brasil nos anos finais do Ensino Fundamental demonstram evolu��o nas duas �ltimas medi��es?</td>
		<td align="center"><input type="radio" name="rpbfinalmat" value="S" <? echo (($rpbfinalmat=="S")?"checked":""); echo $dados_af_m['onclickperg']['S']; ?> ></td>
		<td align="center"><input type="radio" name="rpbfinalmat" value="N" <? echo (($rpbfinalmat=="N")?"checked":""); echo $dados_af_m['onclickperg']['N']; ?> ></td>
		<td align="center"><input type="radio" name="rpbfinalmat" value="A" <? echo (($rpbfinalmat=="A")?"checked":""); echo $dados_af_m['onclickperg']['A']; ?> ></td>
		</tr>

	</table>
	</td>
</tr>
<? endif; ?>

<? if(!$possui_ai && !$possui_af) : ?>
<tr>
	<td class="SubTituloCentro" colspan="4">N�o existem informa��es da Prova Brasil referente a sua escola</td>
</tr>
<? endif; ?>

</table>

<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="btn_anterior" value="Anterior" onclick="divCarregando();window.location='/pdeinterativo2013/pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_2_taxasderendimento';" >
	<input type="button" name="btn_continuar" value="Salvar" onclick="document.getElementById('togo').value='/pdeinterativo2013/pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_3_provabrasil';diagnostico_1_3_provabrasil();">
	<input type="button" name="btn_continuar" value="Salvar e Continuar" onclick="document.getElementById('togo').value='/pdeinterativo2013/pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_4_sintesedimensao1';diagnostico_1_3_provabrasil();">
	<input type="button" name="btn_proximo" value="Pr�ximo" onclick="divCarregando();window.location='/pdeinterativo2013/pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_4_sintesedimensao1';" >
	</td>
</tr>
</table>

</form>
</div>