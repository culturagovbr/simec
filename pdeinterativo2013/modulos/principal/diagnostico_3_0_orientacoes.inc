<?
monta_titulo( "Orienta��es - Ensino e aprendizagem", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10"	align="center">
<tr>
	<td width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue">
	<p>Chegamos � Dimens�o 3 do diagn�stico. Nela, vamos identificar poss�veis dificuldades relacionadas a dois aspectos: planejamento pedag�gico e tempo de aprendizagem.</p>
	<p>Esta � uma das dimens�es mais importantes do processo de reflex�o sobre os principais desafios que a escola precisa enfrentar para melhorar os seus resultados, pois esta ligada diretamente � capacidade de interven��o da equipe gestora. O Grupo de Trabalho deve envolver as pessoas que estejam mais diretamente ligadas aos t�picos de an�lise, discutindo-os sem pressa. Cada senten�a deve ser lida cuidadosamente e as respostas assinaladas devem estar baseadas em fatos e dados, n�o em suposi��es.</p>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="anterior" value="Anterior" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_2_distorcaoeaproveitamento&aba1=diagnostico_2_5_sintesedimensao2';">
	<input type="button" name="proximo" value="Pr�ximo" onclick="divCarregando();window.location='pdeinterativo2013.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_1_planejamentopedagogico';"> 
	</td>
</tr>
</table>