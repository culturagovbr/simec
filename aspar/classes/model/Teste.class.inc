<?php
/**
 * Classe de mapeamento da entidade aspar.posicionamento.
 *
 * $Id$
 */

/**
 * Mapeamento da entidade aspar.posicionamento.
 *
 * @see Modelo
 */
class Aspar_Model_Teste extends Modelo
{
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = 'aspar.posicionamento';

    /**
     * Chave primaria.
     * @var array
     * @access protected
     */
    protected $arChavePrimaria = array(
    );

    /**
     * Chaves estrangeiras.
     * @var array
     */
    protected $arChaveEstrangeira = array(
    );

    /**
     * Atributos
     * @var array
     * @access protected
     */
    protected $arAtributos = array(
        'posid' => null,
        'posdsc' => null,
    );



    /**
     * M�todo que n�o faz nada
     * Atributos
     * @var array
     * @access public
     */
    public function meuMetodoTop(){
        return true;
    }


    /**
     * M�todo que n�o faz nada mesmo
     * Atributos
     * @var array
     * @access public
     */
    public function metodoMachado(){
        return true;
    }



    /**
     * M�todo qalskhdah sjkfhasdjk hfjksdh jk
     * Atributos
     * @var array
     * @access public
     */
    public function metodoDudu(){
        return true;
    }

    /**
     * M�todo que acochambra
     * Atributos
     * @var array
     * @access public
     */
    public function metodoJunio(){
        return true;
    }



    /**
     * M�todo do daniel
     * Atributos
     * @var array
     * @access public
     */
    public function metodoTasca(){
        return true;
    }
}