<?php
require_once APPRAIZ . "/par/classes/controle/PreObraControle.class.inc";
require_once APPRAIZ . "/par/classes/controle/SubacaoControle.class.inc";
require_once APPRAIZ . "/par/classes/modelo/PreTipoDocumento.class.inc";
require_once APPRAIZ . "/par/classes/modelo/PreObra.class.inc";
require_once APPRAIZ . "/www/par/_constantes.php";
//require_once APPRAIZ . "/www/par/_funcoes.php";

$empid = $_SESSION['obras2']['empid'];
$obrid = $_SESSION['obras2']['obrid'];

$empreendimento = new Empreendimento( $empid );
$obPreObra 	= new PreObraControle();

//if($_REQUEST['requisicao']=='telaSubirArquivo') {
//	$dadosarquivo = $db->pegaLinha("SELECT arqnome||'.'||arqextensao as nomearquivo, arqtamanho FROM public.arquivo WHERe arqid='".$_REQUEST['arqid']."'");
//	die("<script>function limpaUpload(arqid){document.getElementById('arquivo_' + arqid).value = \"\";}</script><form method=post enctype=multipart/form-data id=formulario_ name=formulario><input type=hidden name=requisicao value=subirarquivo><input type=hidden name=_sisdiretorio value=obras><table class=tabela><tr><td class=SubTituloDireita>Nome do arquivo:</td><td>".$dadosarquivo['nomearquivo']."</td></tr><tr><td class=SubTituloDireita>Tamanho (bytes):</td><td>".$dadosarquivo['arqtamanho']."</td></tr><tr><td class=SubTituloDireita>Selecione novo arquivo:</td><td><input type=file name=arquivo[".$_REQUEST['arqid']."] id=arquivo_".$_REQUEST['arqid']." > <img onclick=limpaUpload('".$_REQUEST['arqid']."') src=../imagens/excluir.gif /></td></tr><tr><td colspan=2 class=SubTituloCentro><input type=button value=Enviar onclick=document.getElementById('formulario_').submit();> <input type=button name=fechar value=Fechar onclick=closeMessage();></td></tr></table></form>");
//}
//
//if($_REQUEST['requisicao']=='subirarquivo') {
//	
//	if($_FILES['arquivo']) {
//		
//		include APPRAIZ ."includes/funcoes_public_arquivo.php";
//		
//		$resp = atualizarPublicArquivo($arrValidacao = array(''));
//		
//		if($resp['TRUE']) $msg = 'Arquivo atualizado com sucesso';
//		else {
//			if($resp['FALSE']) {
//				$msg .= 'Problemas encontrados:'.'\n';
//				foreach($resp['FALSE'] as $k => $v) {
//					$msg .= $v .'\n';
//				}
//			}
//		}
//		
//		die("<script>
//				alert('".$msg."');
//				window.location = window.location;
//			 </script>");
//	}
//	
//}
//
//if($_REQUEST['requisicao']=='validararquivo') {
//	ob_clean();
//	$db->executar("UPDATE public.arquivo_recuperado SET arqvalidacao=true WHERE arqid='".$_REQUEST['arqid']."'");
//	$db->commit();
//	die("TRUE");
//}

switch ( $_REQUEST['requisicao'] ){
	case 'download': 
		require_once APPRAIZ . "includes/classes/fileSimec.class.inc";
		$obPreObra->documentoDownloadAnexo($_GET['arqid']);
		die("<script>
				location.href = '?modulo=principal/listaDocumentoPreObra&acao=V';
			</script>");
}


//$escrita = verificaPermiss�oEscritaUsuarioPreObra($_SESSION['usucpf'], $_REQUEST['preid']);

//$obPreObra 	= new PreObraControle();
//$db 		= new cls_banco();

//if($_REQUEST['download'] == 's'){	
//	
//	$obPreObra->documentoDownloadAnexo($_GET['arqid']);
//	die();
//}


if(!$empreendimento->preid){
    die("<script>
            alert('N�o existe uma Pr�-Obra para esta Obra!');
            history.back(-1);
         </script>");
}

$preid = $empreendimento->preid;//($_SESSION['par']['preid']) ? $_SESSION['par']['preid'] : $_REQUEST['preid'];
$docid = prePegarDocid($preid);

$esdid = prePegarEstadoAtual($docid);

$respSim = $respSim ? $respSim : array();

$arTipoObraDocumentos = $obPreObra->recuperarTiposObraDocumentosProInfancia($preid);
$arTipoObraDocumentos = $arTipoObraDocumentos ? $arTipoObraDocumentos : array();

$boDocDominialidade = $obPreObra->verificaDocumentoDominialidade($preid);

$boAtivo = 'N';
$stAtivo = 'disabled="disabled"';
if( $esdid ){
	
	$obSubacaoControle2 = new SubacaoControle();
	$obPreObra2 = new PreObra();
	
	if($preid){
		$arDados = $obSubacaoControle2->recuperarPreObra($preid);
	}
	
//	// Regra passada pelo Daniel - 9/6/11
//	if(possuiPerfil(array(PAR_PERFIL_COORDENADOR_GERAL)) && $esdid == WF_TIPO_OBRA_APROVADA && $arDados['ptoprojetofnde'] == 'f') {
//		$boAtivo = 'S';
//		$stAtivo = '';
//	} else {
//		$arrPerfil = array(PAR_PERFIL_EQUIPE_MUNICIPAL, PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO,PAR_PERFIL_EQUIPE_ESTADUAL,PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO,PAR_PERFIL_PREFEITO,PAR_PERFIL_SUPER_USUARIO);
//		$arrSituacao = array(WF_TIPO_EM_CADASTRAMENTO, WF_TIPO_EM_CORRECAO, WF_TIPO_EM_REFORMULACAO);
//		
//		if( in_array($esdid, $arrSituacao) && possuiPerfil($arrPerfil) ){
//			$boAtivo = 'S';
//			$stAtivo = '';
//		}
//	}
}

	# C�digo refeito em 22/10/2012. Regra para libera��o da tela para (cadastramento e atera��o) dos perfil abaixos listados nas seguintes situa��es tamb�m listadas abaixo. 
	# Foi inserido os perfis Estaduais e a situa��o em Dilig�ncia.
	# Foi tamb�m inserido o os perfis. (n�o havia perfil, era verificado apenas o estado).
//	$perfil = pegaArrayPerfil($_SESSION['usucpf']);	
//	if(	(	WF_TIPO_EM_CORRECAO == $esdid || WF_TIPO_EM_CADASTRAMENTO == $esdid || WF_TIPO_EM_REFORMULACAO == $esdid || WF_TIPO_EM_ANALISE_DILIGENCIA == $esdid	) &&
//		(
//			in_array(PAR_PERFIL_EQUIPE_MUNICIPAL, $perfil) ||
//			in_array(PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO, $perfil) ||
//			in_array(PAR_PAR_PERFIL_PREFEITO, $perfil) ||
//			in_array(PAR_PERFIL_EQUIPE_ESTADUAL, $perfil) ||
//			in_array(PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO, $perfil) ||
//			in_array(PAR_PERFIL_EQUIPE_ESTADUAL_SECRETARIO, $perfil) ||
//			in_array(PAR_PERFIL_ENGENHEIRO_FNDE, $perfil) ||
//			in_array(PAR_PERFIL_COORDENADOR_GERAL, $perfil)
//		)
//	){
//		$boAtivo = 'S';
//		$stAtivo = '';
//	}

//$percexec = $db->pegaUm( "select COALESCE(oi.obrpercexec, 0)from obras.obrainfraestrutura oi where oi.preid = $preid" );
//// nova situa��o, se tiver mais de 0% de execu��o da obra... desabilitar
//if((float)$percexec > 0 && $esdid != WF_TIPO_EM_REFORMULACAO) {
//	$boAtivo = 'N';
//	$stAtivo = 'disabled="disabled"';
//}

/*
 * REGRA TEMPORARIA 02/05/2012
 * SOLICITADO PELO DANIEL AREAS
 * LIBERA EDI��O PARA
 * MUNICIPIO COM OBRA
 * EM REFORMULA��O
 * 
 * MUNICIPIOS: SORRISO/MT
 */
//if(in_array($_SESSION['par']['muncod'], array(5107925)) && in_array($esdid, array(WF_TIPO_EM_REFORMULACAO)) ){
//	$boAtivo = 'S';
//	$stAtivo = '';
//}

//if($_POST['poasituacao'] && $boAtivo == 'S'){
//	
//	$sql = "UPDATE obras.preobraanexo SET poasituacao = 'A' WHERE preid = {$preid}";
//	$db->executar($sql);
//	
//	$arDados = $_POST['poasituacao'];
//	foreach($arDados as $podid){
//		$sql = "UPDATE obras.preobraanexo SET poasituacao = 'B' WHERE preid = {$preid} AND podid = {$podid}";
//		$db->executar($sql);
//	}
//	$db->commit();
//}
//
//if($_GET['arqidDel'] && $boAtivo == 'S' ){
//	$obPreObra->excluirDocumentosPreObra($_GET['arqidDel'],$preid);
//}


//if($esdid == WF_TIPO_EM_ANALISE_FNDE){
//	$boAnalise = true;
//}
?>

<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>

<script language="javascript" type="text/javascript">

	var boAtivo = '<?=$boAtivo?>';

	jQuery(document).ready(function(){

		jQuery(".modelo").click(function(){

			var arDados = this.id.split("_");
			
			if(arDados[0] == 8 || arDados[0] == 9){
				return window.open('/par/documentos/modeloDados.php?modelo='+arDados[0]+'&preid='+arDados[1], 
						   'modelo', 
						   "height=600,width=950,scrollbars=yes,top=50,left=200" );
			}else{
				return window.open('/par/documentos/modelo.php?modelo='+arDados[0]+'&preid='+arDados[1], 
						   'modelo', 
						   "height=600,width=950,scrollbars=yes,top=50,left=200" );
			}
		});		

		jQuery(".anexo").click(function(){
			document.location.href = '?modulo=principal/listaDocumentoPreObra&acao=V&requisicao=download&arqid=' + this.id;
		});
	});
	 
//	function excluirAnexo( arqid ){
//		var preid = jQuery('#preid').val();
//		if( boAtivo == 'S' ){
//	 		if ( confirm( 'Deseja excluir o Documento?' ) ) {
//	 			location.href= window.location+'&arqidDel='+arqid+'&preid='+preid;
//	 		}
//		}
// 	}

// 	function popup_arquivo(podid, preid, sisid)
// 	{
// 	 	if(podid == 'disabled'){
//			alert("Documento j� analisado e bloqueado pelo FNDE.");
//			return false;
// 	 	}
// 	 	
// 		return window.open('geral/upload_arquivo.php?podid='+podid+'&preid='+preid+'&sisid='+sisid, 
//				   'anexo', 
//				   "height=300,width=450,scrollbars=yes,top=50,left=200" );
// 	}

 	function popup_ajuda(link)
 	{
 		return window.open('geral/popup_ajuda.php?link='+link, 
				   'ajuda', 
				   "height=300,width=450,scrollbars=yes,top=50,left=200" );
 	}
 	
// 	function validarFoto(arqid){
//		if(confirm('Deseja realmente validar esta foto ?')){
//			jQuery.ajax({
//		   		type: "POST",
//		   		url: window.location.href,
//		   		data: "requisicao=validararquivo&arqid="+arqid,
//		   		success: function(msg){
//		   			if(msg=="TRUE") {
//		   				alert("Arquivo validado com sucesso");
//		   				window.location=window.location;
//		   			} else {
//		   				alert("Arquivo n�o validado com sucesso");
//		   				window.location=window.location;
//		   			}
//		   		}
//		 		});
//			
//		}
//	}
 
</script>

<?php 

$db->cria_aba($abacod_tela,$url,$parametros);
//$habilitado = false;
//$habilita = 'N';

monta_titulo( 'Documentos Pr�-Obras', $obraDescricao  );
?>

<script type="text/javascript" src="../includes/ModalDialogBox/modal-message.js"></script>
<script type="text/javascript" src="../includes/ModalDialogBox/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="../includes/ModalDialogBox/ajax.js"></script>
<link rel="stylesheet" href="/includes/ModalDialogBox/modal-message.css" type="text/css" media="screen" />
<script type="text/javascript">
				messageObj = new DHTML_modalMessage();	// We only create one object of this class
				messageObj.setShadowOffset(5);	// Large shadow
				
				function displayMessage(url) {
					messageObj.setSource(url);
					messageObj.setCssClassMessageBox(false);
					messageObj.setSize(690,400);
					messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
					messageObj.display();
				}
				function displayStaticMessage(messageContent,cssClass) {
					messageObj.setHtmlContent(messageContent);
					messageObj.setSize(600,150);
					messageObj.setCssClassMessageBox(cssClass);
					messageObj.setSource(false);	// no html source since we want to use a static message here.
					messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
					messageObj.display();
				}
				function closeMessage() {
					messageObj.close();	
				}
</script>

<form action="" method="post">
	<table width="95%" align="center" border="0" cellspacing="0" cellpadding="2" class="listagem">
		<table width="95%" align="center" border="0" cellspacing="2" cellpadding="2" class="listagem">
			<thead>
				<tr>
					<td valign="top" width="40" align="center" class="title" style="border-right: 1px solid #c0c0c0; border-bottom: 1px solid #c0c0c0; border-left: 1px solid #ffffff;" onmouseover="this.bgColor='#c0c0c0';" onmouseout="this.bgColor='';"><b>Item</b></td>
					<td valign="top" align="center" class="title" style="border-right: 1px solid #c0c0c0; border-bottom: 1px solid #c0c0c0; border-left: 1px solid #ffffff;" onmouseover="this.bgColor='#c0c0c0';" onmouseout="this.bgColor='';"><b>Descri��o</b></td>
					<td valign="top" align="center" class="title" style="border-right: 1px solid #c0c0c0; border-bottom: 1px solid #c0c0c0; border-left: 1px solid #ffffff;" onmouseover="this.bgColor='#c0c0c0';" onmouseout="this.bgColor='';"><b>Anexo(s)</b></td>
					<?php if($boAnalise): ?>
						<td valign="top" align="center" class="title" style="border-right: 1px solid #c0c0c0; border-bottom: 1px solid #c0c0c0; border-left: 1px solid #ffffff;" onmouseover="this.bgColor='#c0c0c0';" onmouseout="this.bgColor='';"><b>Bloquear</b></td>
					<?php endif; ?>				
				</tr>
			</thead>
			<tbody>
				<?php $x = 0; ?>
				<?php foreach($arTipoObraDocumentos as $tipos): ?>
					<?php
					$cor = ($x % 2) ? "#F7F7F7" : "white"; 
					?>
					<tr bgcolor="<?php echo $cor ?>" onmouseover="this.bgColor='#ffffcc';" onmouseout="this.bgColor='<?php echo $cor ?>';">
						<td align="center"><?php echo $x+1 //$tipos['codigo'] ?></td>			
						<td><?php echo $tipos['descricao'] ?></td>
						<td align="right" width="350px">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<?php
										$trav = true;

										if( ($esdid == WF_TIPO_EM_CADASTRAMENTO) && possuiPerfil($arrPerfil) ){
											$trav = false;
										}
										if(possuiPerfil(array(PAR_PERFIL_COORDENADOR_GERAL)) && $esdid == WF_TIPO_OBRA_APROVADA && $arDados['ptoprojetofnde'] == 'f') {
											$trav = false;
										}
										if( ($esdid == WF_TIPO_EM_CORRECAO) && possuiPerfil($arrPerfil) ){
											switch ($tipos['codigo']) {
											    case "3":
											        $trav = in_array(QUESTAO_ESTUDO_DEM,$respSim);
											        $txtAjuda = "Anexar o Estudo de Demanda elaborado, devidamente assinado, justificando e apresentando a necessidade de implanta��o da obra solicitada, focado na regi�o de abrang�ncia, informando o n�mero de crian�as a serem atendidas na faixa et�ria pleiteada e apresentando as fontes de pesquisa (ex. levantamento da Secretaria de Sa�de sobre vacina��o infantil).";
											        break;
											    case "1":
											        $trav = in_array(QUESTAO_PLANILHA_LOCALIZACAO,$respSim);
											        $txtAjuda = "Anexar arquivo de imagem da Planta de Localiza��o, destacando o terreno selecionado na malha urbana, indicando o endere�o, apresentando seu entorno, os acessos ao lote, as refer�ncias naturais e constru�das pr�ximas, localizando o terreno proposto dentro do munic�pio. ";
											        break;
											    case "4":
											        $trav = in_array(QUESTAO_PLANILHA_SIT,$respSim);
											        $txtAjuda = "Anexar arquivo de imagem da Planta de Situa��o, indicando as dimens�es do terreno e apresentando os lotes e vias lim�trofes.";
											        break;
											    case "6":
											        $trav = in_array(QUESTAO_PLANILHA_LOCACAO,$respSim);
											        $txtAjuda = "Anexar arquivo de imagem da Planta de Loca��o da obra solicitada no terreno proposto. � necess�rio: 1. Apresentar a Planta Baixa da obra solicitada (disponibilizada no site do FNDE), indicando as dimens�es do terreno e as dimens�es gerais da edifica��o, bem como os afastamentos entre esta e os limites do terreno. Cabe ressaltar que o afastamento das edifica��es em rela��o aos limites do terreno dever� obedecer �s determina��es do Plano Diretor local e demais leis vigentes; 2. Locar a entrada das redes de abastecimento de energia, �gua e esgoto, sempre respeitando as orienta��es t�cnicas definidas pelas concession�rias locais. Em caso de utiliza��o de fossa s�ptica, sumidouro, cisterna ou outro elemento que tenha como fun��o suprir defici�ncia em redes existentes, tais instrumentos dever�o ser tamb�m locados; 3. Defini��o das cotas de n�vel de cada edifica��o existente, bem como dos acessos e �reas verdes em rela��o � via de acesso ou �s cal�adas lim�trofes; 4. Apresentar norte magn�tico e ventos dominantes; 5. Representar em planta o escoamento de �guas pluviais, muros de conten��o e qualquer outro tipo de obra que n�o conste em projeto-padr�o, mas que seja imprescind�vel ao funcionamento adequado da escola.";
											        break;
											    case "5":
											        $trav = in_array(QUESTAO_PLANIALTIMETRICO,$respSim);
											        $txtAjuda = "Encaminhar o Levantamento Planialtim�trico do terreno proposto, apresentando as curvas de n�vel cotadas a cada metro de desn�vel.";
											        break;
											    case "9":
											        $trav = in_array(QUESTAO_DOMINIALLIDADE,$respSim);
											        $txtAjuda = "Gerar a declara��o padr�o de dominialidade, disponibilizada no sistema, preenchendo os dados solicitados; imprimir o documento gerado que deve ser assinado pelo prefeito ou represente legal do munic�pio e anexar em campo pr�prio o arquivo de imagem do documento ou, caso j� tenha-se dispon�vel, da Certid�o de Matr�cula do Im�vel.";
											        break;
											    case "19":
											        $trav = in_array(QUESTAO_DOMINIALLIDADE,$respSim);
											        $txtAjuda = "Gerar a declara��o padr�o de dominialidade, disponibilizada no sistema, preenchendo os dados solicitados; imprimir o documento gerado que deve ser assinado pelo prefeito ou represente legal do munic�pio e anexar em campo pr�prio o arquivo de imagem do documento ou, caso j� tenha-se dispon�vel, da Certid�o de Matr�cula do Im�vel.";
											        break;
											    case "7":
											        $trav = in_array(QUESTAO_FORNECIMENTO_INFRA,$respSim);
											        $txtAjuda = "Gerar a declara��o padr�o de responsabilidade pelo fornecimento e manuten��o dos servi�os de abastecimento de �gua, energia el�trica, esgotamento sanit�rio e pela coleta de lixo para o terreno proposto para edifica��o do objeto pleiteado, al�m da execu��o dos servi�os de terraplanagem, caso sejam necess�rios. Preencher os dados solicitados; imprimir o documento gerado que deve ser assinado pelo prefeito ou represente legal do munic�pio e anexar em campo pr�prio o arquivo de imagem do documento.";
											        break;
											    case "8":
											        $trav = in_array(QUESTAO_COMPATIBILIDADE_FUNDACAO,$respSim);
											        $txtAjuda = "Gerar a declara��o padr�o de adequa��o da funda��o, disponibilizada no sistema, preenchendo os dados solicitados; imprimir o documento gerado que deve ser assinado por profissional competente e anexar em campo pr�prio o arquivo de imagem do documento. No caso de verificar-se inadequa��o do solo ao tipo de funda��o proposta, o munic�pio dever� apresentar novo projeto executivo de funda��o contendo, inclusive, a Anota��o de Responsabilidade T�cnica - ART.";
											        break;
											    case "10":
											        $trav = in_array(QUESTAO_PLANILHA,$respSim);
											        $txtAjuda = "Gerar a Planilha or�ament�ria padr�o preenchida";
											        break;
											}
										}
										
										if( ($esdid == WF_TIPO_EM_REFORMULACAO) && possuiPerfil($arrPerfil) ){
											$trav = false;
										}
										/*
										if($_SESSION['par']['muncod']){
											if($esdid == WF_TIPO_EM_CORRECAO && $obPreObra->verificaGrupoMunicipioTipoObra_A($_SESSION['par']['muncod'])){
												$trav = false;
											}
										}*/
										// Se a obra estiver em dilig�ncia ent�o liberam todos os anexos. ( Victor Benzi - 18/06/2012 )
										if( $esdid == WF_TIPO_EM_CORRECAO ){
											$trav = false;
										}
										$arAnexos = $obPreObra->recuperarDocumentosAnexoPorPodid($tipos['codigo'], $preid);
										$arAnexos = $arAnexos ? $arAnexos : array();
										$Anexar   = true;
										echo '<table width=100% border="0" cellspacing="0" cellpadding="0">';
										foreach($arAnexos as $anexo){
											$excluir = '';
											$subtituir = true;
											$restricao = '';
											if($tipos['situacao'] == 'B' || $boAtivo == 'N' || $trav){
//												echo '<img src="../imagens/excluir_01.gif" align="absmiddle" title="Excluir documento" style="padding-right:5px;padding-bottom:5px;" />';
											}else{
												if( $esdid == WF_TIPO_EM_CORRECAO ){
													$subtituir = true;
												}
												$excluir = '<img class="excluir" id="'.$anexo['codigo'].'" src="../imagens/excluir.gif" align="absmiddle" title="Excluir documento" style="cursor:pointer;padding-right:5px;padding-bottom:5px;" />';
											}
											$arqvalidacao = $db->pegaUm("SELECT arqvalidacao FROM public.arquivo_recuperado WHERE arqid='".$anexo['codigo']."'");
											if(!is_file(APPRAIZ."arquivos/obras/".floor($anexo['codigo']/1000)."/".$anexo['codigo'])) {
												if( $esdid == WF_TIPO_EM_CADASTRAMENTO ){
													$Anexar   = false;
												}
//												if( $subtituir ){
//													$restricao = "<img src=../imagens/restricao.png align=absmiddle border=0> <input type=button name=b ".$stAtivo." value=Substituir onclick=\"displayMessage(window.location.href+'&requisicao=telaSubirArquivo&arqid=".$anexo['codigo']."',false);\">";
//												}else{
//													$restricao = "";
//												}
//												$alerta = 'background-color: red;';
												$restricao = "&nbsp;";
												$alerta = '';
											}elseif($arqvalidacao=="f") {
//												if( possuiPerfil(Array(PAR_PERFIL_COORDENADOR_GERAL,PAR_PERFIL_SUPER_USUARIO)) ){
//													$restricao = "<br /> <img src=../imagens/restricao.png align=absmiddle border=0> <input type=button name=b value=Validar onclick=\"validarFoto('".$anexo['codigo']."');\">";
//												}
//												$restricao .= "<br /><img src=../imagens/restricao.png align=absmiddle border=0> <input type=button name=b ".$stAtivo." value=Substituir onclick=\"displayMessage(window.location.href+'&requisicao=telaSubirArquivo&arqid=".$anexo['codigo']."',false);\">";
												$restricao = "&nbsp;";
											} else {
												$restricao = "&nbsp;";
												$alerta = '';
											}
											
											echo '<tr><td width=60%>'.$excluir.' <a title="Baixar arquivo" href="javascript:void(0)" id="'.$anexo['codigo'].'" class="anexo" style="'.$alerta.'">'.delimitador($anexo['descricao'], 20).'.'.$anexo['extensao'].'</a></td><td>'.$restricao.'</td></tr>';
										}
										echo '</table>';
										?>
									</td>
									<td width="70px;" align="center">
									<?php if( $Anexar ){?>
									<!--
										<input type="button" value="Anexar" onclick="popup_arquivo('<?php echo ($tipos['situacao'] == 'B') ? "disabled" : $tipos['codigo'] ?>', '<?php echo $preid ?>', '<?php echo SIS_OBRAS ?>')" <?php echo $stAtivo ?> <?= $trav ? 'disabled="disabled"' : ''; ?>/>
									-->
									<?php }?>
									</td>
								</tr>						
							</table>															
						</td>
						<?php if($boAnalise): ?>
							<td width="80px" align="center">
								<input type="checkbox" name="poasituacao[]" value="<?php echo $tipos['codigo'] ?>" <?php echo ($tipos['situacao'] == 'B') ? "checked" : "" ?>>
							</td>
						<?php endif; ?>
					</tr>
					<?php $x++ ?>
				<?php endforeach; ?>
			</tbody>
	</table>
	
	<?php
//		$isReformulacao = $db->pegaUm("SELECT preidpai FROM obras.preobra WHERE preid='".$preid."'");
//		if(!$isReformulacao) {
//			if( possuiPerfil( Array(PAR_PERFIL_SUPER_USUARIO,PAR_PERFIL_CONSULTA) ) ){
//				$hab = 'S';
//			}elseif( possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL) || possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO) || possuiPerfil(PAR_PERFIL_PREFEITO) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO) ){
//				if(  in_array( $esdid, Array(WF_TIPO_EM_REFORMULACAO, WF_TIPO_EM_CADASTRAMENTO, WF_TIPO_EM_CORRECAO, WF_TIPO_OBRA_ARQUIVADA) ) ){
//					$hab = 'S';
//				}
//			}elseif( possuiPerfil(PAR_PERFIL_ENGENHEIRO_FNDE) ){
//				if(  !in_array( $esdid, Array(WF_TIPO_OBRA_INDEFERIDA, WF_TIPO_OBRA_DEFERIDA, WF_TIPO_OBRA_CONDICIONADA, WF_TIPO_OBRA_INDEFERIDA_PRAZO, WF_TIPO_OBRA_APROVADA, WF_TIPO_OBRA_ARQUIVADA) ) ){
//					$hab = 'S';
//				}
//			}elseif( possuiPerfil(PAR_PERFIL_COORDENADOR_GERAL) ){
//				if(  !in_array( $esdid, Array(WF_TIPO_OBRA_INDEFERIDA, WF_TIPO_OBRA_DEFERIDA, WF_TIPO_OBRA_CONDICIONADA, WF_TIPO_OBRA_INDEFERIDA_PRAZO, WF_TIPO_OBRA_APROVADA, WF_TIPO_OBRA_ARQUIVADA) ) ){
//					$hab = 'S';
//				}
//			}
//		}elseif( possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL) || possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO) || possuiPerfil(PAR_PERFIL_PREFEITO) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO) ){
//			if(  in_array( $esdid, Array(WF_TIPO_EM_CADASTRAMENTO, WF_TIPO_EM_CORRECAO, WF_TIPO_OBRA_ARQUIVADA,WF_TIPO_EM_REFORMULACAO) ) ){
//				$hab = 'S';
//			}
//		}
	
	?>
</form>
<?php

function prePegarDocid( $preid ) {

	global $db;

	$sql = "SELECT
				docid
			FROM
				obras.preobra
			WHERE
			 	preid = " . (integer) $preid;

	return (integer) $db->pegaUm( $sql );

}

function prePegarEstadoAtual( $docid ) {

	global $db;

	$sql = "SELECT
				ed.esdid
			FROM
				workflow.documento d
			INNER JOIN workflow.estadodocumento ed ON ed.esdid = d.esdid
			WHERE
				d.docid = " . $docid;

	$estado = (integer) $db->pegaUm( $sql );

	return $estado;

}

function pegaArrayPerfil($usucpf){

	global $db;

	$sql = "SELECT
				pu.pflcod
			FROM
				seguranca.perfil AS p
			LEFT JOIN seguranca.perfilusuario AS pu ON pu.pflcod = p.pflcod
			WHERE
				p.sisid = '{$_SESSION['sisid']}'
				AND pu.usucpf = '$usucpf'";

	$pflcod = $db->carregar( $sql );

	foreach($pflcod as $dados){
		$arPflcod[] = $dados['pflcod'];
	}
	return $arPflcod;
}

function carregaAbasProInfancia($stPaginaAtual = null, $preid = null, $descItem = null){

	global $db;

	$oSubacaoControle = new SubacaoControle();

	$docid = prePegarDocid($preid);
	$esdid = prePegarEstadoAtual($docid);
	//$isReformulacao = $db->pegaUm("SELECT preidpai FROM obras.preobra WHERE preid='".$preid."'");
	$isReformulacao = $db->pegaUm("SELECT esd.esdid FROM workflow.documento d
									INNER JOIN obras.preobra p ON p.docid = d.docid
									INNER JOIN workflow.estadodocumento esd ON esd.esdid = d.esdid
									WHERE p.preid='".$preid."'");

	$lnkPendencias	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=analise&preid=".$preid;

	if($preid){

		$boTipoObra 			= $oSubacaoControle->verificaTipoObra($preid, SIS_OBRAS);
		$boPlanilhaOrcamentaria = $oSubacaoControle->verificaPlanilhaOrcamentaria(null, SIS_OBRAS, $preid);
		$tipoObra 				= $oSubacaoControle->verificaTipoObra($preid, SIS_OBRAS);
		$pacFNDE 				= $oSubacaoControle->verificaObraFNDE($preid, SIS_OBRAS);

		$lnkDocumento   	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=documento&preid=".$preid;
		$lnkDocumentoFNDE   = "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=documentoFNDE&preid=".$preid;
//		$lnkDocumentoTipoA  = "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=documentoTipoA&preid=".$preid;

		if($boTipoObra){

			$lnkPlanilha	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=planilhaOrcamentaria&preid=".$preid;
			$lnkCronograma 	= "javascript:alert(\'Salve os dados da planilha or�ament�ria primeiro.\')";
			$lnkFotos 		= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=foto&preid=".$preid;

			if($boPlanilhaOrcamentaria){
				$lnkCronograma  = "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=cronograma&preid=".$preid;
				$lnkPendencias	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=analise&preid=".$preid;
			}

		}else{
			$lnkPlanilha	= "javascript:alert(\'Informe o tipo de obra e o endere�o na aba de dados do terreno.\')";
			$lnkCronograma 	= "javascript:alert(\'Informe o tipo de obra e o endere�o na aba de dados do terreno.\')";
			$lnkFotos 		= "javascript:alert(\'Informe o tipo de obra e o endere�o na aba de dados do terreno.\')";
		}

		$lnkDados 			= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=dados&preid=".$preid;
		$lnkQuestionario 	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=questionario&preid=".$preid;

	}else{

		$lnkPlanilha		= "javascript:alert(\'Salve os dados do terreno primeiro.\')";
		$lnkCronograma 		= "javascript:alert(\'Salve os dados do terreno primeiro.\')";
		$lnkDocumento 		= "javascript:alert(\'Salve os dados do terreno primeiro.\')";
//		$lnkDocumentoTipoA	= "javascript:alert(\'Salve os dados do terreno primeiro.\')";
		$lnkFotos 			= "javascript:alert(\'Salve os dados do terreno primeiro.\')";
		$lnkDados 			= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A";
		$lnkQuestionario 	= "javascript:alert(\'Salve os dados do terreno primeiro.\')";

	}

	$abas = array(
			0 => array("descricao" => "Dados do terreno", "link" => $lnkDados)
			);

	if($preid){
		array_push($abas, array("descricao" => "Relat�rio de vistoria", "link" => $lnkQuestionario));
		array_push($abas, array("descricao" => "Cadastro de fotos do terreno", "link" => $lnkFotos));
	}

	if($preid){

		if($pacFNDE == 't'){
			array_push($abas, array("descricao" => "Planilha or�ament�ria", "link" => $lnkPlanilha));
			array_push($abas, array("descricao" => "Cronograma F�sico-Financeiro", "link" => $lnkCronograma));
		}else if( $pacFNDE == 'f' && possuiPerfil( Array(PAR_PERFIL_COORDENADOR_GERAL, PAR_PERFIL_EQUIPE_MUNICIPAL,PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO, PAR_PERFIL_EQUIPE_ESTADUAL,PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO) ) ){
			array_push($abas, array("descricao" => "Planilha or�ament�ria", "link" => $lnkPlanilha));
		}

		array_push($abas, array("descricao" => "Documentos anexos", "link" => $lnkDocumento));

//		if($pacFNDE == 'f'){
//			array_push($abas, array("descricao" => "Projetos - Tipo A", "link" => $lnkDocumentoTipoA));
//		}

		$arrEstado = Array(WF_TIPO_EM_CADASTRAMENTO,WF_TIPO_EM_CORRECAO);
		$arrPerfilsAnalise = array(PAR_PERFIL_EQUIPE_MUNICIPAL,PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO, PAR_PERFIL_PREFEITO, PAR_PERFIL_ENGENHEIRO_FNDE, PAR_PERFIL_COORDENADOR_GERAL, PAR_PERFIL_COORDENADOR_TECNICO, PAR_PERFIL_SUPER_USUARIO);

		// se n�o for uma reformula��o
		if( $isReformulacao == WF_TIPO_EM_REFORMULACAO ) {
			if( possuiPerfil( Array(PAR_PERFIL_SUPER_USUARIO,PAR_PERFIL_CONSULTA) ) ){
				array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
			}elseif( possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL) || possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO) || possuiPerfil(PAR_PERFIL_PREFEITO) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO) ){
				if(  in_array( $esdid, Array(WF_PAR_OBRA_VAL_INDEF_REFORMULACAO, WF_TIPO_EM_CADASTRAMENTO, WF_TIPO_EM_CORRECAO, WF_TIPO_OBRA_ARQUIVADA) ) ){
					array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
				}
			}elseif( possuiPerfil(PAR_PERFIL_ENGENHEIRO_FNDE) ){
				if(  !in_array( $esdid, Array(WF_TIPO_OBRA_INDEFERIDA, WF_TIPO_OBRA_DEFERIDA, WF_TIPO_OBRA_CONDICIONADA, WF_TIPO_OBRA_INDEFERIDA_PRAZO, WF_TIPO_OBRA_APROVADA, WF_TIPO_OBRA_ARQUIVADA) ) ){
					array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
				}
			}elseif( possuiPerfil(PAR_PERFIL_COORDENADOR_GERAL) ){
				if(  !in_array( $esdid, Array(WF_TIPO_OBRA_INDEFERIDA, WF_TIPO_OBRA_DEFERIDA, WF_TIPO_OBRA_CONDICIONADA, WF_TIPO_OBRA_INDEFERIDA_PRAZO, WF_TIPO_OBRA_APROVADA, WF_TIPO_OBRA_ARQUIVADA) ) ){
					array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
				}
			}
		}elseif( possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL) || possuiPerfil(PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO) || possuiPerfil(PAR_PERFIL_PREFEITO) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL) || possuiPerfil(PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO) ){
			if(  in_array( $esdid, Array(WF_PAR_OBRA_VAL_INDEF_REFORMULACAO, WF_TIPO_EM_CADASTRAMENTO, WF_TIPO_EM_CORRECAO, WF_TIPO_OBRA_ARQUIVADA,WF_TIPO_EM_REFORMULACAO) ) ){
				array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
			}
		}



//		if( in_array($esdid,$arrEstado) && possuiPerfil($arrPerfilsAnalise)){
//
//			if(verificaWFpreObra( $preid ) == false){
//				array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
//			}
//			elseif($db->testa_superuser()){
//			array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));}
//
//
//		}elseif($db->testa_superuser()){
//			array_push($abas, array("descricao" => "Enviar para an�lise", "link" => $lnkPendencias));
//		}
	}

	$perfil = pegaArrayPerfil($_SESSION['usucpf']);
	$arSituacoes = array(WF_TIPO_EM_ANALISE_FNDE,
						 WF_TIPO_EM_ANALISE_REFORMULACAO,
						 WF_TIPO_EM_VALIDACAO_DEFERIMENTO_REFORMULACAO,
						 WF_TIPO_EM_ANALISE,
						 WF_TIPO_VALIDACAO_DILIGENCIA,
						 WF_TIPO_VALIDACAO_INDEFERIMENTO,
						 WF_TIPO_VALIDACAO_DEFERIMENTO,
						 WF_TIPO_OBRA_INDEFERIDA,
						 WF_TIPO_OBRA_DEFERIDA,
						 WF_TIPO_EM_ANALISE_VALIDACAO,
						 WF_TIPO_EM_ANALISE_DILIGENCIA,
						 WF_TIPO_OBRA_CONDICIONADA,
						 WF_TIPO_OBRA_INDEFERIDA_PRAZO,
						 WF_TIPO_OBRA_APROVADA,
						 WF_TIPO_OBRA_ARQUIVADA);
	$arSituacoesEquipe = array(WF_TIPO_EM_CORRECAO,WF_TIPO_OBRA_ARQUIVADA);
	// Mostra aba se o estado for em An�lise - FNDE
	if( in_array($esdid, $arSituacoes) && (in_array(PAR_PERFIL_SUPER_USUARIO, $perfil) ||
			in_array(PAR_PERFIL_ENGENHEIRO_FNDE, $perfil) ||
			in_array(PAR_PERFIL_CONSULTA, $perfil) ||
			in_array(PAR_PERFIL_COORDENADOR_GERAL, $perfil) ||
			( $esdid == WF_TIPO_EM_ANALISE_REFORMULACAO && $perfil == PAR_PERFIL_ENGENHEIRO_FNDE ))) {

			$lnkAnaliseEngenheiro	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=analiseEngenheiro&preid=".$preid;

			array_push($abas, array("descricao" => "An�lise de Engenharia", "link" => $lnkAnaliseEngenheiro));

	}elseif(in_array($esdid, $arSituacoesEquipe) && (in_array(PAR_PERFIL_SUPER_USUARIO, $perfil) ||
			in_array(PAR_PERFIL_ENGENHEIRO_FNDE, $perfil) ||
			in_array(PAR_PERFIL_CONSULTA, $perfil) ||
			in_array(PAR_PERFIL_COORDENADOR_GERAL, $perfil) ||
			( $esdid == WF_TIPO_EM_ANALISE_REFORMULACAO && $perfil == PAR_PERFIL_ENGENHEIRO_FNDE ) ||
			possuiPerfil(array(PAR_PERFIL_EQUIPE_MUNICIPAL,PAR_PERFIL_EQUIPE_MUNICIPAL_APROVACAO,PAR_PERFIL_EQUIPE_ESTADUAL,PAR_PERFIL_EQUIPE_ESTADUAL_APROVACAO, PAR_PERFIL_PREFEITO))
			//in_array(PAR_PERFIL_EQUIPE_MUNICIPAL, PAR_PERFIL_PREFEITO, $perfil)
			)){

			$lnkAnaliseEngenheiro	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=analiseEngenheiro&preid=".$preid;

			array_push($abas, array("descricao" => "An�lise de Engenharia", "link" => $lnkAnaliseEngenheiro));

	} elseif($isReformulacao == WF_TIPO_EM_REFORMULACAO) {
		$lnkAnaliseEngenheiro	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=analiseEngenheiro&preid=".$preid;

		array_push($abas, array("descricao" => "An�lise de Engenharia", "link" => $lnkAnaliseEngenheiro));

	}

	if(possuiPerfil(array(PAR_PERFIL_ADMINISTRADOR,PAR_PERFIL_ENGENHEIRO_FNDE))){

		$lnkListaObras	= "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=listaObras&preid=".$preid;
		$titulo = $_SESSION['par']['esfera'] == 'M' ? "Obras no Munic�pio" : "Obras no Estado";
		array_push($abas, array("descricao" => $titulo, "link" => $lnkListaObras));
	}

	$win = false;
	if((in_array(PAR_PERFIL_SUPER_USUARIO, $perfil) ||
	    in_array(PAR_PERFIL_ENGENHEIRO_FNDE, $perfil) ||
	    in_array(PAR_PERFIL_COORDENADOR_GERAL, $perfil) ||
	    ( $esdid == WF_TIPO_EM_ANALISE_REFORMULACAO && $perfil == PAR_PERFIL_ENGENHEIRO_FNDE ) ||
	    in_array(PAR_PERFIL_COORDENADOR_TECNICO, $perfil)) && ( $_GET['tipoAba'] == 'analiseEngenheiro' )){
	   	$win = true;
	}

	// Adiciona a aba de empenho
//	$lnkEmpenho = "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=empenho&preid=".$preid;
//	array_push($abas, array("descricao" => "Empenho", "link" => $lnkEmpenho));
//	$lnkPagamento = "par.php?modulo=principal/programas/proinfancia/popupProInfancia&acao=A&tipoAba=pagamento&preid=".$preid;
//	array_push($abas, array("descricao" => "Pagamento", "link" => $lnkPagamento));

	if( (in_array(PAR_PERFIL_SUPER_USUARIO, $perfil) ||
		 in_array(PAR_PERFIL_ENGENHEIRO_FNDE, $perfil) ||
		 in_array(PAR_PERFIL_COORDENADOR_GERAL, $perfil) ||
		 ( $esdid == WF_TIPO_EM_ANALISE_REFORMULACAO && $perfil == PAR_PERFIL_ENGENHEIRO_FNDE ) ) ) {
		array_push($abas, array("descricao" => "Documentos FNDE", "link" => $lnkDocumentoFNDE));
	}

	return montarAbasArray($abas, $stPaginaAtual, $win);
}

function delimitador($texto, $valor = null){

	if(!ereg('[^0-9]',$texto)){
		return $texto;
	}

	if(!$valor){
		$valor = 280;
	}

	if(strlen($texto) > $valor){
			$texto = substr($texto,0,$valor).'...';
	}

	return $texto;
}

?>