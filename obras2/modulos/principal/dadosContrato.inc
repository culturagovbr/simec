<?php

include  APPRAIZ."includes/cabecalho.inc";
echo'<br>';
?>
<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<form method="post" name="formulario" id="formulario" action="">
	<table class="tabela" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" align="center">
	<tbody>
		<tr>
			<td colspan="2" class="SubTituloCentro" style="background-color: #DCDCDC">Contrata��o da Obra</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Empresa Contratada:</td>
			<td>
				<span id="entnomeempresa">TECNOTEL AMBIENTAL REFORMAS E SERVICOS GERAIS LTDA</span>
				<input type="hidden" name="entidempresa" id="entidempresa" value="680578">
				<input type="button" name="pesquisar_entidade" value="Pesquisar" style="cursor: pointer;" onclick="inserirEmpresa(document.getElementById('entidempresa').value);">
				<?=obrigatorio(); ?>			  
			</td>
		</tr>
		<tr>
			<td class="subtitulodireita">Data de Assinatura do Contrato:</td>
			<td><?=campo_data2('crtdtassinatura', 'S', 'S','Data de Assinatura do Contrato', '', '', '', $crtdtassinatura, '', '', 'crtdtassinatura'); ?></td>
		</tr>
		<tr>
			<td class="subtitulodireita">Prazo de Vig�ncia do Contrato (dias):</td>
			<td><?=campo_texto('crtprazovigencia', 'S', 'S', '', 5, 10, '[#]', '', '', '', 0, 'id="crtprazovigencia"','', $crtprazovigencia, '' ); ?></td>
		</tr>
		<tr>
			<td class="subtitulodireita">Data de t�rmino do contrato:</td>
			<td><?=campo_data2('crtdttermino', 'N', 'N','Data de t�rmino do contrato', '', '', '', $crtdttermino, '', '', 'crtdttermino'); ?></td>
		</tr>
		<tr>
			<td colspan="2" class="SubTituloCentro" style="background-color: #DCDCDC">Sobre a Obra</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">C�pia da Obra Original</td>
			<td><? $db->monta_lista_simples($sql, $cabecalho, 50000, 5, 'S'); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Hist�rico de Paralisa��es</td>
			<td><? $db->monta_lista_simples($sql, $cabecalho, 50000, 5, 'S'); ?></td>
		</tr>
		<tr>
			<td class="subtitulodireita">Data da Ordem de Servi�o:</td>
			<td><?=campo_data2('crtdtordemservico', 'S', 'S','Data da Ordem de Servi�o', '', '', '', $crtdtordemservico, '', '', 'crtdtordemservico'); ?></td>
		</tr>
		<tr>
			<td class="subtitulodireita">In�cio de Execu��o da Obra:</td>
			<td><?=campo_data2('crtdtinicioexecucao', 'S', 'S','In�cio de Execu��o da Obra', '', '', '', $crtdtinicioexecucao, '', '', 'crtdtinicioexecucao'); ?></td>
		</tr>
		<tr>
			<td class="subtitulodireita">Prazo de Execu��o (dias):</td>
			<td><?=campo_texto('crtprazoexecucao', 'S', 'S', '', 5, 10, '[#]', '', '', '', 0, 'id="crtprazoexecucao"','', $crtprazoexecucao, '' ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">T�rmino de Execu��o da Obra:</td>
			<td><?=campo_data2('crtdtterminoexecucao', 'N', 'N','T�rmino de Execu��o da Obra', '', '', '', $crtdtterminoexecucao, '', '', 'crtdtterminoexecucao'); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor Contratado da Obra (R$):</td>
			<td><?=campo_texto('crtvalorexecucao', 'S', 'S', '', 20, 18, '[###.]###,##', '', '', '', 0, 'id="crtvalorexecucao"','', $crtvalorexecucao, '' ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">�rea/Quantidade a ser Constru�da:</td>
			<td><?=campo_texto('crtcustounitario', 'S', 'S', '', 20, 18, '[###.]###,##', '', '', '', 0, 'id="crtcustounitario"','', $crtcustounitario, '' ); 
				echo 'Unidade de Medida:' . $db->monta_combo("umdid", array(), 'S', '', '', '', '', '100', 'N');?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Custo Unit�rio R$:</td>
			<td><?=campo_texto('crtcustounitario', 'N', 'N', '', 20, 18, '[###.]###,##', '', '', '', 0, 'id="crtcustounitario"','', $crtcustounitario, '' ); ?> (R$ / Unidade de Medida)</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Percentual BDI:</td>
			<td><?=campo_texto('crtpercentualdbi', 'S', 'S', '', 20, 18, '[###.]###,##', '', '', '', 0, 'id="crtpercentualdbi"','', $crtpercentualdbi, '' ); ?> (Administra��o, taxas, emolumentos, impostos e lucro.)</td>
		</tr>
		<tr>
			<td colspan="2" class="SubTituloCentro" style="background-color: #DCDCDC">Supervis�es - Empresa</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Hist�rico de Supervis�es</td>
			<td><? $db->monta_lista_simples($sql, $cabecalho, 50000, 5, 'S'); ?></td>
		</tr>
		<tr>
			<td colspan="2" class="SubTituloCentro" style="background-color: #DCDCDC">Aditivos</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Hist�rico de Aditivos</td>
			<td><? $db->monta_lista_simples($sql, $cabecalho, 50000, 5, 'S'); ?>
			<a href="#" style="text-align: left; color: #0F55A9;" onclick="">Incluir Aditivo</a></td>
		</tr>
		<tr bgcolor="#C0C0C0">
			<td></td>
			<td>
				<input type="button" id="salvar" value="Salvar" onclick="validaRec(&#39;11&#39;)" style="cursor: pointer">
				<input type="button" value="Voltar" style="cursor: pointer" onclick="history.back(-1);">
			</td>
		</tr>
	</tbody>
	</table>
</form>