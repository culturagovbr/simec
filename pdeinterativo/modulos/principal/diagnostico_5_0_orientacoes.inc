<?
monta_titulo( "Orienta��es - Comunidade Escolar", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10"	align="center">
<tr>
	<td width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue">
	<p>Iniciaremos a 5� e pen�ltima dimens�o do Diagn�stico, que trata sobre a Comunidade Escolar. �Comunidade� diz respeito �quilo que � comum a v�rias pessoas e chama-se de �comunidade escolar� �s partes interessadas nas quest�es relativas � vida escolar. Mas poderia ser tamb�m chamada de �comunidade educativa�, pois envolve aspectos que extrapolam o ambiente escolar.</p>
	<p>Nesta dimens�o, abordaremos quatro segmentos importantes que comp�em a Comunidade Escolar, a saber: estudantes, docentes, funcion�rios n�o docentes e pais. Procuraremos conhecer um pouco mais sobre o perfil de cada segmento e as iniciativas destinadas a promover um maior envolvimento deles com a escola.</p>
	<p>Preencha os quadros conforme os comandos e, em seguida, assinale em que medida o Grupo de Trabalho concorda com cada pergunta.</p> 
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="anterior" value="Anterior" onclick="divCarregando();window.location='pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_4_gestao&aba1=diagnostico_4_4_sintesedimensao4';">
	<input type="button" name="proximo" value="Pr�ximo" onclick="divCarregando();window.location='pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_5_comunidadeescolar&aba1=diagnostico_5_1_estudantes';">
	</td>
</tr>
</table>