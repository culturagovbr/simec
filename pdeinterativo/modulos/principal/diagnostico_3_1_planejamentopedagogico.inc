<?
monta_titulo( "Planejamento Pedag�gico - Ensino e aprendizagem", "&nbsp;");

function calculaQuantidadePerguntasRespostas($dados) {
	global $db;
	
	if($dados['sqls']) {
		$sql_total = "select count(*) from ((".implode(") union all (",$dados['sqls']).")) foo";
		$tperguntas = $db->pegaUm($sql_total);
		
		foreach($dados['sqls'] as $sqlp) {
			$dados['sqlsn'][] = str_replace("pdeinterativo.pergunta", "pdeinterativo.pergunta p inner join pdeinterativo.respostapergunta rp on rp.prgid=p.prgid and pdeid='".$_SESSION['pdeinterativo_vars']['pdeid']."' and oppid in(".OPP_SEMPRE.",".OPP_MAIORIA_DAS_VEZES.")",$sqlp);
		}
		
		$sql_total = "select count(*) from ((".implode(") union all (",$dados['sqlsn']).")) foo";
		$trespostas = $db->pegaUm($sql_total);
		
	}
	
	return array('tperguntas' => $tperguntas, 'trespostas' => $trespostas);
	
}

?>
<script>

function diagnostico_3_1_planejamentopedagogico() {
	if(!verificaRespostasPerguntas()){
		alert('Favor responder todas as perguntas!');
		return false;
	}
	
	var totalPerguntas=parseInt(jQuery("[name^='perg[']:enabled").length/4);
	var max = parseInt(Math.floor(totalPerguntas/2))+1;
	var numRespS = parseInt(jQuery("[name^='perg['][value=<?=OPP_SEMPRE ?>]:enabled:checked").length);
	var numRespM = parseInt(jQuery("[name^='perg['][value=<?=OPP_MAIORIA_DAS_VEZES ?>]:enabled:checked").length);
	var totresp = numRespS+numRespM;
	if(totresp>max) {
		if(document.getElementById('tr_justificativasevidencias').style.display=='none') {
			document.getElementById('tr_justificativasevidencias').style.display='';
		}
		if(jQuery('#juedescricao').val()=='') {
			alert('Foram assinaladas mais de 50% das respostas com as op��es �Sempre� e �Na maioria das vezes�. � luz dos resultados analisados nas dimens�es 1 e 2, justifique as respostas.');
			return false;
		}
	} else {
		if(document.getElementById('tr_justificativasevidencias').style.display=='') {
			alert('A justificativa n�o � mais necess�ria e ser� removida.');
			document.getElementById('tr_justificativasevidencias').style.display='none';
			jQuery('juedescricao').val('');
		}
	}
	
	divCarregando();
	document.getElementById('formulario').submit();
}

</script>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
<tr>
	<td class="SubTituloDireita" width="10%"><font class="blue">Orienta��es</font></td>
	<td class="blue">
	<p>Neste momento, a escola dever� fazer uma reflex�o sobre o Projeto Pedag�gico, o Curr�culo e as Avalia��es. Estes temas, embora pare�am corriqueiros ou j� superados pela escola, muitas vezes representam os pontos mais fr�geis da escola, pois tendem apenas a reproduzir modelos e paradigmas. Sabemos que, em muitos casos, a equipe gestora aproveita o que j� vem sendo feito, sem questionar a efic�cia e a efetividade dessas pr�ticas e instrumentos.</p>
	<p>Assim, antes de assinalar uma resposta, � preciso que o Grupo de Trabalho reflita e debata sobre cada senten�a. N�o existe resposta "certa" ou "errada", apenas respostas verdadeiras e que correspondem � situa��o que mais se aproxima da realidade da escola.</p>
	<p>Nesta tela, se o total de respostas �sempre� e �na maioria das vezes� for superior � 50% do total de perguntas, � necess�rio que a escola apresente, no final da tela, justificativas ou evid�ncias sobre esses aspectos, �s luz dos resultados apresentados nas dimens�es 1 e 2. Ou seja, se os desafios do planejamento pedag�gico foram superados, porque os resultados da escola n�o s�o melhores?</p>
	<p>No caso das respostas �raramente� ou �nunca� a quest�o � apresentada na s�ntese da dimens�o como um poss�vel problema, cabendo � escola decidir se ele � cr�tico ou n�o.</p>
	</td>
</tr>
</table>

<form method="post" id="formulario">
<input type="hidden" name="togo" id="togo" value="/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_2_tempodeaprendizagem">
<input type="hidden" name="requisicao" value="diagnostico_3_1_planejamentopedagogico">
<?
$sql1 = "select * from pdeinterativo.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Projeto Pedag�gico'";
$sql2 = "select * from pdeinterativo.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Curr�culo'";
$sql3 = "select * from pdeinterativo.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Avalia��es'";
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
<tr>
	<td class="SubTituloDireita" width="10%">Projeto Pedag�gico</td>
	<td>
	<?php quadroPerguntas($sql1) ?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="10%">Curr�culo</td>
	<td>
	<?php quadroPerguntas($sql2) ?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="10%">Avalia��es</td>
	<td>
	<?php quadroPerguntas($sql3) ?>
	</td>
</tr>
<? $juedescricao = $db->pegaUm("select juedescricao from pdeinterativo.justificativaevidencias where abacod='diagnostico_3_1_planejamentopedagogico' AND pdeid='".$_SESSION['pdeinterativo_vars']['pdeid']."'"); ?>
<tr id="tr_justificativasevidencias" <?=(($juedescricao)?'':'style="display:none"') ?>>
	<td class="SubTituloDireita" width="10%">Justificativas/ Evid�ncias</td>
	<td>
	<? echo campo_textarea( 'juedescricao', 'S', 'S', '', '100', '5', '1000'); ?>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="btn_anterior" value="Anterior" onclick="divCarregando();window.location='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_0_orientacoes';" >
	<input type="button" name="btn_continuar" value="Salvar" onclick="document.getElementById('togo').value='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_1_planejamentopedagogico';diagnostico_3_1_planejamentopedagogico();">
	<input type="button" name="btn_continuar" value="Salvar e Continuar" onclick="document.getElementById('togo').value='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_2_tempodeaprendizagem';diagnostico_3_1_planejamentopedagogico();">
	<input type="button" name="btn_proximo" value="Pr�ximo" onclick="divCarregando();window.location='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_3_ensinoeaprendizagem&aba1=diagnostico_3_2_tempodeaprendizagem';" >
	</td>
</tr>
</table>
</form>