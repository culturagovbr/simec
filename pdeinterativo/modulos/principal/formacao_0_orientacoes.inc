<?
monta_titulo( "Orienta��es - PDE Escola", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
<tr>
	<td class="blue" width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue" >
		O Plano de Forma��o � a proposta da escola para aprimoramento do seu corpo docente e diretivo, e baseia-se nas respostas oferecidas nas telas "4.1 Dire��o" e "5.2 Docentes", do Diagn�stico. Ou seja, todos os profissionais indicados para participar de cursos de forma��o continuada e os Diretores, Vice Diretores e Equipe Pedag�gica ser�o exibidos na pr�xima tela e caber� � escola, em comum acordo com esses profissionais, propor o curso que considera mais adequado (no m�ximo um curso por pessoa). Caso a escola tenha conclu�do a Dimens�o 2 do Diagn�stico, o sistema indicar� as �reas de conhecimento cr�ticas e � recomend�vel escolher cursos convergentes com estes problemas.
		<br><br>Na tela "2.2 Ordem de Prioridade", a escola dever� definir a prioridade de atendimento, considerando que � invi�vel liberar simultaneamente todos os profissionais e, principalmente, que alguns docentes precisam ser capacitados prioritariamente, em fun��o de crit�rios que a pr�pria escola pode definir. Sugere-se que a escola observe as �reas de conhecimento cr�ticas identificadas no Diagn�stico (tela "2.4 �reas de conhecimento").
		<br><br>Caso, na tela "2.1 Proposta da Escola", a escola tenha indicado algum profissional do corpo docente ou diretivo para participar de cursos que admitem "demanda social", ou seja, que atendem tamb�m outros profissionais ou membros da comunidade escolar com influ�ncia direta sobre o cotidiano da escola (ex. pais, funcion�rios da escola, gestores municipais e estaduais ligados � educa��o etc.), na tela "2.3 Demanda Social" ser� poss�vel indicar tal demanda de acordo com o p�blico-alvo pr�-estabelecido para cada curso.
		<br><br>A tela "2.4 Sugest�o de curso" oferece a possibilidade de a escola apresentar uma sugest�o � Secretaria e ao MEC, limitado a um curso por escola. N�o � obrigat�rio apresentar sugest�es.
		<br><br>Lembre-se: ao finalizar o plano, clique em "Enviar para a Secretaria". O prazo para envio do Plano de Forma��o termina no dia 31/03/2012, mas a escola poder� continuar trabalhando no PDE Interativo, por�m sem alterar o Plano de Forma��o.
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="proximo" value="Pr�ximo" onclick="divCarregando();window.location='pdeinterativo.php?modulo=principal/planoestrategico&acao=A&aba=formacao_0_planoformacao&aba1=formacao_0_planoformacao';">
	</td>
</tr>
</table>