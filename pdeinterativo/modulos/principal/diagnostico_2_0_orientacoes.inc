<?
monta_titulo( "Orienta��es - Distor��o e aproveitamento", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10"	align="center">
<tr>
	<td width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue">
	<p>Agora, vamos iniciar a Dimens�o 2 do Diagn�stico. Aqui, vamos rever os dados de matr�cula (cadastrados no Censo Escolar) e identificar alguns aspectos relacionados � distor��o idade-s�rie, aproveitamento geral dos estudantes e �reas de conhecimento cr�ticas. Preencha os quadros conforme os comandos e, em seguida, assinale em que medida o Grupo de Trabalho concorda com cada pergunta.</p>
	<p>Esta dimens�o � uma das que exige mais concentra��o e an�lise cr�tica por parte do Grupo de Trabalho. Analise cuidadosamente as informa��es de cada tela porque elas seguem uma estrutura l�gica, onde os resultados de um tema influenciam nos demais.</p>
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
		<input type="button" name="btn_anterior" value="Anterior" onclick="window.location='pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_4_sintesedimensao1'" >
		<input type="button" name="btn_proximo" value="Pr�ximo" onclick="window.location='pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_2_distorcaoeaproveitamento&aba1=diagnostico_2_1_matriculas'" >
	</td>
</tr>
</table>