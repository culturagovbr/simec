<?
monta_titulo( "Orienta��es - Diagn�stico", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
<tr>
	<td class="blue" width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue" >
	<p>O Diagn�stico � uma das etapas mais importantes de todo tipo de planejamento, pois representa o momento em que os planejadores se defrontam com a realidade que pretendem alterar. Afinal, um planejamento existe para modificar uma situa��o. O principal objetivo do diagn�stico � ajudar a escola a fazer o seu "raio X�, ou seja, conhecer a situa��o atual e, a cada momento, tentar identificar os principais problemas e desafios a serem superados. E para que ele reflita bem essa realidade escolar, precisa ser elaborado coletivamente.</p>
	<p>No caso do PDE Interativo, n�o se trata apenas de responder e preencher os campos, mas de refletir sobre as informa��es que est�o sendo colocadas. Portanto, o Grupo de Trabalho deve avaliar cuidadosamente cada quest�o e debat�-las at� chegar a um entendimento comum e aceit�vel por todos. Para tanto, quanto mais informa��es relevantes puderem ser reunidas no diagn�stico, maiores as chances do plano ser bem elaborado. Neste sentido, o PDE Interativo dividiu o diagn�stico em 3 eixos e, em cada eixo, s�o inclu�das duas dimens�es. As dimens�es, por sua vez, subdividem-se em temas. Observe o quadro abaixo para entender a estrutura do Diagn�stico.</p>
	<table class="listagem" width="100%">
		<tr>
			<td class="SubTituloCentro">EIXO</td>
			<td class="SubTituloCentro">DIMENS�ES</td>
			<td class="SubTituloCentro">TEMAS</td>
		</tr>
		<tr>
			<td rowspan="6" align="center" class="blue">Resultados</td>
			<td rowspan="3" class="blue">Dimens�o 1 � Indicadores e taxas</td>
			<td class="blue">IDEB</td>
		</tr>
		<tr>
			<td class="blue">Taxas de rendimento</td>
		</tr>
		<tr>
			<td class="blue">Prova Brasil</td>
		</tr>
		<tr>
			<td rowspan="3" class="blue">Dimens�o 2 � Distor��o e aproveitamento	Matr�cula</td>
			<td class="blue">Distor��o idade-s�rie</td>
		</tr>
		<tr>
			<td class="blue">Aproveitamento escolar</td>
		</tr>
		<tr>
			<td class="blue">�reas de conhecimento</td>
		</tr>
		<tr>
			<td rowspan="5" align="center" class="blue">Interven��o direta</td>
			<td rowspan="2" class="blue">Dimens�o 3 � Ensino e Aprendizagem</td>
			<td class="blue">Planejamento pedag�gico</td>
		</tr>
		<tr>
			<td class="blue">Tempo de aprendizagem</td>
		</tr>
		<tr>
			<td rowspan="3" class="blue">Dimens�o 4 � Gest�o</td>
			<td class="blue">Dire��o</td>
		</tr>
		<tr>
			<td class="blue">Processos</td>
		</tr>
		<tr>
			<td class="blue">Finan�as</td>
		</tr>
		<tr>
			<td rowspan="6" align="center" class="blue">Interven��o parcial ou indireta</td>
			<td rowspan="4" class="blue">Dimens�o 5 � Comunidade Escolar</td>
			<td class="blue">Estudantes</td>
		</tr>
		<tr>
			<td class="blue">Docentes</td>
		</tr>
		<tr>
			<td class="blue">Demais profissionais</td>
		</tr>
		<tr>
			<td class="blue">Pais e comunidade</td>
		</tr>
		<tr>
			<td rowspan="2" class="blue">Dimens�o 6 - Infraestrutura</td>
			<td class="blue">Instala��es</td>
		</tr>
		<tr>
			<td class="blue">Equipamentos</td>
		</tr>
	</table>
	
	<p>Naturalmente, os problemas n�o s�o estanques em cada eixo, mas esta divis�o ajuda a entender onde se localizam os problemas. No caso do Eixo 1, as informa��es s�o mais objetivas e refletem como est� o desempenho da escola em rela��o a alguns indicadores relevantes para a Educa��o. Tamb�m ajudam a equipe escolar a localizar alguns problemas em rela��o �s turmas e disciplinas cr�ticas, focalizando suas a��es.</p>
	<p>O Eixo 2, n�o por acaso, est� no centro do diagn�stico. Ele re�ne os elementos sobre os quais a equipe gestora tem maiores condi��es de intervir, pois s�o quest�es que dependem diretamente da sua atua��o. � o momento que exige maior capacidade de auto-cr�tica da equipe escolar, discutindo seus problemas sem receios e sem acusa��es, afinal, o que a escola faz n�o � produto apenas de uma ou duas pessoas, mas de todo o grupo. Ali�s, o objetivo do plano n�o � atribuir culpas, e sim buscar solu��es.</p>
	<p>O Eixo 3, por sua vez, apresenta fatores que podem ser enfrentados pela equipe gestora, mas exigem maior capacidade de mobiliza��o e motiva��o. � muito comum que as escolas creditem seus maus resultados aos temas deste eixo, alegando, por exemplo, baixo n�vel de envolvimento dos estudantes, dos docentes ou dos pais. Ou acreditando que a simples melhoria na infraestrutura resolveria todos os problemas, o que raramente acontece. Resolver os desafios do Eixo 3 exige mais do que esperar que os outros fa�am. Exige criatividade, lideran�a, negocia��o e perseveran�a.</p>
	<p>A seguir, explicaremos, dimens�o por dimens�o, como realizar o diagn�stico. Mas lembre-se: o Diagn�stico deve ser elaborado coletivamente, pois n�o se trata apenas de "responder" e "preencher" os campos, mas de refletir sobre as informa��es que est�o sendo colocadas. Portanto, re�na o Grupo de Trabalho e discuta cada t�pico at� chegar a um entendimento comum e aceit�vel por todos.</p>

	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
		<input type="button" name="btn_anterior" value="Anterior" onclick="window.location='pdeinterativo.php?modulo=principal/primeirospassos&acao=A&aba=terceiropasso'" >
		<input type="button" name="btn_proximo" value="Pr�ximo" onclick="window.location='pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas';">
	</td>
</tr>
</table>