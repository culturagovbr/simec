<?
monta_titulo( "Orienta��es - Indicadores e Taxas", "&nbsp;");
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
<tr>
	<td width="10%" align="center"><img src="../imagens/bussola.png"></td>
	<td class="blue" >
	<p>Esta � a 1� dimens�o do Diagn�stico. Nela, vamos conhecer os principais indicadores de desempenho da escola, quais sejam: o IDEB, as taxas de rendimento (aprova��o, reprova��o e abandono) e a Prova Brasil.</p>
	<p>Algumas informa��es poder�o ser capturadas automaticamente das bases de dados do MEC. Para tanto, basta seguir as orienta��es descritas em cada tabela.</p>
	<p>Observe que sempre que os �ndices e taxas n�o demonstrarem aumento nos �ltimos dois anos ou medi��es, esta informa��o passar� a constar automaticamente como um poss�vel problema a ser priorizado. Vamos l�?</p> 
	</td>
</tr>
</table>
<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
<tr>
	<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
	<td>
	<input type="button" name="btn_anterior" value="Anterior" onclick="divCarregando();window.location='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_0_orientacoes';" >
	<input type="button" name="btn_proximo" value="Pr�ximo" onclick="divCarregando();window.location='/pdeinterativo/pdeinterativo.php?modulo=principal/diagnostico&acao=A&aba=diagnostico_1_indicadoresetaxas&aba1=diagnostico_1_1_ideb';" >
	</td>
</tr>
</table>