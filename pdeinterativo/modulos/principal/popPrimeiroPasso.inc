<html>
	<head>
		<title>SIMEC - Sistema Integrado de Monitoramento Execu��o e Controle do Minist�rio da Educa��o</title>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
		<script language="JavaScript" src="../../includes/funcoes.js"></script>
		<script type="text/javascript" src="/includes/JQuery/jquery.js"></script>
	</head>
	<body>
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
			<tr>
				<td bgcolor="#c4c4c4" align="center">
					<b>Perfis e atribui��es do GT</b>
				</td>
			</tr>
			<tr>
				<td>
				� recomend�vel que o Conselho Escolar assuma a tarefa de elabora��o do planejamento da escola, pois geralmente os seus membros representam os diversos segmentos da escola, inclusive a lideran�a formal da escola (o diretor ou diretora). Caso a escola n�o possua Conselho Escolar, sugere-se que seja criado um Grupo de Trabalho (GT).
				<br /><br />
				Em todo caso, a equipe deve incluir, al�m da equipe gestora da escola, um(a) representante dos docentes, um dos pais ou respons�veis e um representante dos estudantes, de todos os turnos e n�veis.
				<br /><br />
				O n�mero de pessoas depender� da estrutura da escola e da disposi��o em participar voluntariamente da elabora��o e acompanhamento do plano. Recomenda-se a designa��o de at� 10 (dez) pessoas, sob o risco de inviabilizar ou retardar a elabora��o do planejamento.
				<br /><br />
				S�o responsabilidades do Grupo de Trabalho: convocar reuni�es, elaborar o plano, encaminhar e acompanhar a an�lise do plano junto � Secretaria de Educa��o, acompanhar a implementa��o e execu��o das a��es e promover avalia��es cont�nuas do plano.
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" value="Fechar" onclick="self.close();" />
				</td>
			</tr>
		</table>
	</body>
</html>