<html>
	<head>
		<title>SIMEC - Sistema Integrado de Monitoramento Execu��o e Controle do Minist�rio da Educa��o</title>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
		<script language="JavaScript" src="../../includes/funcoes.js"></script>
		<script type="text/javascript" src="/includes/JQuery/jquery.js"></script>
	</head>
	<body>
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
			<tr>
				<td bgcolor="#c4c4c4" align="center">
					<b>Perfil do(a) Coordenador(a)</b>
				</td>
			</tr>
			<tr>
				<td>
					O(A) Coordenador(a) do plano deve ser, necessariamente, um membro do Grupo de Trabalho, escolhido pelo pr�prio GT. Um(a) candidato(a) natural ao cargo de coordenador(a) do PDE Escola � o(a) coordenador(a) pedag�gico(a) da escola. Ele(a) tem por fun��o principal animar o processo de elabora��o, orientar o grupo e coordenar as a��es que devem ser tomadas para a elabora��o, a execu��o, o monitoramento e a avalia��o do plano.
					<br /><br />
					Uma caracter�stica essencial do(a) coordenador(a) do plano � que ele(a) conhe�a bem a escola, entenda os principais indicadores educacionais e saiba interpret�-los, ajudando o GT a identificar os principais desafios e definir as a��es necess�rias para enfrent�-los.
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" value="Fechar" onclick="self.close();" />
				</td>
			</tr>
		</table>
	</body>
</html>