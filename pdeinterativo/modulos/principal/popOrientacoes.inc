<html>
	<head>
		<title>SIMEC - Sistema Integrado de Monitoramento Execu��o e Controle do Minist�rio da Educa��o</title>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
		<script language="JavaScript" src="../../includes/funcoes.js"></script>
		<script type="text/javascript" src="/includes/JQuery/jquery.js"></script>
	</head>
	<body>
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="10" cellPadding="10" align="center">
			<tr>
				<td bgcolor="#c4c4c4" align="center">
					<b>Primeiros passos do PDE Escola</b>
				</td>
			</tr>
			<tr>
				<td>
				As a��es que antecedem a elabora��o do Plano de Desenvolvimento da Escola s�o:
				<br />
				&nbsp;&nbsp;&nbsp;a) estudar a metodologia;<br />
				&nbsp;&nbsp;&nbsp;b) convidar o Conselho Escolar para elaborar o plano ou constituir um Grupo do Trabalho com a comunidade escolar;<br />
				&nbsp;&nbsp;&nbsp;c) indicar o(a) Coordenador(a) do plano;<br />
				&nbsp;&nbsp;&nbsp;d) conhecer os membro do Comit� de An�lise e Aprova��o da Secretaria de Educa��o; e<br />
				&nbsp;&nbsp;&nbsp;e) divulgar junto � comunidade escolar o in�cio do processo de elabora��o do planejamento da escola.
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" value="Fechar" onclick="self.close();" />
				</td>
			</tr>
		</table>
	</body>
</html>