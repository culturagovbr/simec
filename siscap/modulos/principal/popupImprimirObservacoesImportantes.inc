<?php
monta_titulo( 'Observa��es Importantes', '' );

?>
<style type="">
	@media print {.notprint { display: none } .div_rolagem{display: none} }	
	@media screen {.notscreen { display: none; }
	
	.div_rolagem{ overflow-x: auto; overflow-y: auto; height: 50px;}
	
</style>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
<table id="tblform" class="tabela" width="95%" bgcolor="#f5f5f5" cellspacing="1" cellpadding="2" align="center">
	<tr>
		<td width="35%" align="center">
		
				<div style="width:40%; text-align:left;">
				<p style="margin: 20px 0; font-size: 16px;"><b>ATEN��O</b></p>
	
				<p>- O simples preenchimento e envio do formul�rio n�o garante sua participa��o no evento
				pretendido. Aguarde sua confirma��o por e-mail.</p>
				
				<p>- Ao enviar sua ficha de pr�-inscri��o, sua chefia imediata receber�, automaticamente,
				por e-mail, uma mensagem para autorizar sua participa��o.</p>
				
				<p>- A autoriza��o de sua chefia imediata � condi��o para que sua pr�-inscri��o seja
				avaliada pela equipe t�cnica do CETREMEC.</p>
				
				<p>- Caso haja desist�ncia, entre em contato nos ramais do CETREMEC 7393, 7369, 7377, 7371, 
				com anteced�ncia m�nima de 48 horas.</p>
				
				<p><a href="http://intramec.mec.gov.br/component/docman/doc_download/1525-2010bol42supl.html" class="doclink">
				- Acesse aqui a portaria n� 1.507, de 28 de Outubro de 2010, que expede instru��es
				espec�ficas para a implementa��o da Pol�tica Nacional de Desenvolvimento de Pessoal,
				no �mbito da administra��o direta do Minist�rio da Educa��o.</a></p>
				</div>	
				
		
		</td>
	</tr>
</table>
<table id="tblform" class="tabela" width="95%" bgcolor="#f5f5f5" cellspacing="1" cellpadding="2" align="center">
	<tr bgcolor="#D0D0D0">
		<td colspan="6" style="text-align: center" class="div_rolagem">
			<input type="button" id="btImprimir" value="Imprimir" onclick="javascript: window.print();" />
			<input type="button" name="fechar" value="Fechar" onclick="javascript: window.close();">
		</td>
	</tr>
</table>