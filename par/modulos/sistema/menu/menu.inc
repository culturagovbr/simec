<?
 /*
   Sistema Simec
   Setor respons�vel: SPO-MEC
   Desenvolvedor: Equipe Consultores Simec
   Analista: Gilberto Arruda Cerqueira Xavier, Cristiano Cabral (cristiano.cabral@gmail.com)
   Programador: Gilberto Arruda Cerqueira Xavier (e-mail: gacx@ig.com.br), Cristiano Cabral (cristiano.cabral@gmail.com)
   M�dulo:menu.inc
   Finalidade: permitir o cadastro de itens de menu
   */
   	
$modulo=$_REQUEST['modulo'] ;//

if ($_REQUEST['act'] == 'inserir')
{
	//verifica se existe m�dulo igual cadastrado
	$sql = "SELECT count(*) AS total FROM menu WHERE mnulink<>'' AND sisid = ". $_SESSION['sisid'] . " AND TRIM(mnulink)='".trim($_REQUEST['mnulink'])."'";
	$RS = $db->recuperar($sql,$res);
	if ($RS['total']>0)
	{
?>
   	<script language="JavaScript">alert('Este Menu/M�dulo j� se encontra cadastrado!');history.back();</script>
<?
	   	$db -> close();
		exit();
	}
   	// fazer inser��o de menu na base de dados.
   	if (! $_REQUEST['abacod']) $_REQUEST['abacod']='null';
   	$sql = "INSERT INTO seguranca.menu (mnucod,mnuidpai,mnudsc,mnutransacao,mnulink,mnutipo,mnustile,mnuhtml,mnuimagem,mnusnsubmenu,mnushow,mnustatus,abacod,sisid) 
   			VALUES ("
   .$_REQUEST['mnucod'].",";
   if ($_REQUEST['mnuidpai']) $sql = $sql.$_REQUEST['mnuidpai'].","; else $sql = $sql.' null,';
   $sql = $sql .
   "'".$_REQUEST['mnudsc']."',".
   "'".$_REQUEST['mnutransacao']."',".
   "'".$_REQUEST['mnulink']."',".
   "'".$_REQUEST['mnutipo']."',".
   "'".$_REQUEST['mnustile']."',".
   "'".$_REQUEST['mnuhtml']."',".
   "'".$_REQUEST['mnuimagem']."',".
   "'".$_REQUEST['mnusnsubmenu']."',".
   "'".$_REQUEST['mnushow']."',".
   "'A',".$_REQUEST['abacod'].",".$_SESSION['sisid'].") RETURNING mnuid";
   $saida = $db->pegaUm($sql);
   
   #Vincula Menu aos perfis selecionado
   $mnuid = $saida;
   if($mnuid){
   		$sql = "DELETE FROM seguranca.perfilmenu WHERE mnuid = ".$mnuid;
		$db->executar($sql);
	}
	if($mnuid && $_REQUEST['pflcod'][0] != "" && $_REQUEST['pflcod_campo_flag'] == "1"){
		foreach($_REQUEST['pflcod'] as $pflcod){
			$sqlI = "INSERT INTO seguranca.perfilmenu (pflcod,mnuid) VALUES ($pflcod,".$mnuid.");";
  			$db->executar($sqlI);
		}
	}
	
	if( $_REQUEST['abacod'] != 'null' ){
		$Totabacod = $db->pegaUm('SELECT COUNT(abacod) FROM seguranca.aba_menu WHERE mnuid='.$mnuid.' AND abacod='.$_REQUEST['abacod']);
		
		if( $Totabacod == '0' ){
			$sql = 'INSERT INTO seguranca.aba_menu (abacod, mnuid) VALUES ('.$_REQUEST['abacod'].','.$mnuid.')';
			$db->executar($sql);
		}
	}
   
   $db->commit();
   $db->sucesso($modulo);
}
if ($_REQUEST['act']=='alterar')
{
	//verifica se existe outro m�dulo igual alterado
	$sql = "SELECT COUNT(*) as total FROM seguranca.menu WHERE mnulink<>'' AND sisid = " . $_SESSION['sisid'] . " AND mnulink='".$_REQUEST['mnulink']."' AND mnuid<>".$_REQUEST['mnuid_int'];
	$RS = $db->recuperar($sql,$res);
	if ($RS['total']>0)
	{
?>
   	<script language="JavaScript">alert('Este Menu/M�dulo j� se encontra cadastrado!');history.back();</script>
<?
   		$db -> close();
   		exit();
   	}
   	// fazer altera��o do menu na base de dados.
   	if ($_REQUEST['mnusnsubmenu'] == 't') $_REQUEST['mnusnsubmenu'] = 'true' ; else $_REQUEST['mnusnsubmenu'] = 'false';
	if ($_REQUEST['mnushow'] == 't') $_REQUEST['mnushow'] = 'true' ; else $_REQUEST['mnushow'] = 'false';
	$sql = "UPDATE seguranca.menu SET 
				mnucod 			= {$_REQUEST['mnucod']}, 
				mnudsc 			= '{$_REQUEST['mnudsc']}', 
				mnutransacao	= '{$_REQUEST['mnutransacao']}',  
   				mnulink			= '{$_REQUEST['mnulink']}',
   				mnutipo			= '{$_REQUEST['mnutipo']}',   
   				mnustile		= '{$_REQUEST['mnustile']}',  
   				mnuhtml			= '{$_REQUEST['mnuhtml']}',  
   				mnuimagem		= '{$_REQUEST['mnuimagem']}',
   				mnuidpai		= ".($_REQUEST['mnuidpai']?$_REQUEST['mnuidpai']:'NULL').", 
   				abacod 			= ".($_REQUEST['abacod']?$_REQUEST['abacod']:'NULL').",  
   				mnusnsubmenu	= {$_REQUEST['mnusnsubmenu']},
   				mnushow			= {$_REQUEST['mnushow']}
			WHERE 
				mnuid=".$_REQUEST['mnuid_int'];
    $saida = $db->executar($sql);
    
	$mnuid = $_REQUEST['mnuid_int'];
   	if($mnuid){
   		$sql = "DELETE FROM seguranca.perfilmenu WHERE mnuid = ".$mnuid;
		$db->executar($sql);
	}
	if($mnuid && $_REQUEST['pflcod'][0] != "" && $_REQUEST['pflcod_campo_flag'] == "1"){
		foreach($_REQUEST['pflcod'] as $pflcod){
			$sqlI = "INSERT INTO seguranca.perfilmenu (pflcod,mnuid) VALUES ($pflcod,".$mnuid.");";
  			$db->executar($sqlI);
		}
	}
	if( $_REQUEST['abacod'] ){
		$Totabacod = $db->pegaUm('SELECT COUNT(abacod) FROM seguranca.aba_menu WHERE mnuid='.$mnuid.' AND abacod='.$_REQUEST['abacod']);
		
		if( $Totabacod == '0' ){
			$sql = 'INSERT INTO seguranca.aba_menu (abacod, mnuid) VALUES ('.$_REQUEST['abacod'].','.$mnuid.')';
			$db->executar($sql);
		}
	}   
    
    $db->commit();
	?>
	<script>
		alert('Opera��o realizada com sucesso');
		location.href="par.php?modulo=sistema/menu/menu&acao=A&mnuid_int=<?=$mnuid ?>";
	</script>
<?
	$db -> close();
	exit();
}

if ($_POST['exclui'] > 0) 
{
	$sql = "SELECT p.pfldsc FROM seguranca.perfilmenu pm
			INNER JOIN seguranca.perfil p ON p.pflcod = pm.pflcod 
			WHERE pm.mnuid = {$_POST['exclui']} AND pm.pmnstatus = 'A'";
	$arrPerfil = $db->carregarColuna($sql);
	
	if( $arrPerfil[0] ){
		echo "<script language='JavaScript'>
				alert('N�o � poss�vel excluir esse menu, pois ele est� vinculado ao(s) perfil(is): ".implode(', ', $arrPerfil)."');
				window.location.href = window.location;
			</script>";
		exit();
	} else {
		$sql = "DELETE FROM seguranca.estatistica WHERE mnuid=".$_POST['exclui'];
		$db->executar($sql);
		$sql = "DELETE FROM seguranca.auditoria WHERE mnuid=".$_POST['exclui'];
		$db->executar($sql);	  
		// n�o pode deixar escluir se j� estiver associado � algum perfil
		$sql = "DELETE FROM seguranca.perfilmenu WHERE mnuid=".$_POST['exclui'];
	    $saida = $db->executar($sql);
		
	    $sql = "DELETE FROM seguranca.menu WHERE mnuid=".$_POST['exclui'];
	    $db->executar($sql);
	    unset($_POST['exclui']);
	    $db->commit();
	    $db->sucesso($modulo);  
	}
	exit();
}

include APPRAIZ."includes/cabecalho.inc";
?>
<br>
<?
$parametros = array('','','');
$db->cria_aba($abacod_tela,$url,$parametros);

if ($_REQUEST['acao']=='I' or ($_REQUEST['acao']=='A' and $_REQUEST['mnuid_int']<>'')){

	//t�tulo da p�gina
	if ($_REQUEST['acao']=='I') $titulo_modulo = 'Incluir Menu'; else $titulo_modulo = 'Alterar Menu';
	monta_titulo($titulo_modulo,'<img src="../imagens/obrig.gif" border="0"> Indica Campo Obrigat�rio.');
	
	if($_REQUEST['mnuid_int'] and $_REQUEST["act"]=='') { 
	       $sql= "SELECT * FROM seguranca.menu WHERE mnuid=".$_REQUEST['mnuid_int'];    
		   $saida = $db->recuperar($sql,$res);
		   // ver($saida,$sql,d);
	       if(is_array($saida)) {foreach($saida as $k=>$v) ${$k}=$v;}
	
	} else { 
	
	    //recupera todas as variaveis que veio pelo post
		foreach($_REQUEST as $k=>$v) ${$k}=$v;
	} 
?>
<form method="POST"  name="formulario">
<input type='hidden' name="modulo" value="<?=$modulo?>">
<input type='hidden' name="mnuid_int" value=<?=$_REQUEST['mnuid_int']?>>
<input type='hidden' name='exclui' value=0>
<input type='hidden' name='act' value=0>
<input type='hidden' name='acao' value=<?=$_REQUEST['acao']?>>
	<table  class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
		<tr>
	       	<td align='right' class="SubTituloDireita">C�digo:</td>
	        <td>
	        	<? if (! $_REQUEST['mnuid']) $habil='S' ;else $habil= 'N';?>
				<?=campo_texto('mnucod','S',$habil,'',6,5,'#####','');?>
        	</td>
      	</tr>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Descri��o:</td>
	        <td><?=campo_texto('mnudsc','S','S','',50,50,'','');?></td>
      	</tr>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Transa��o:</td>
	        <td><?=campo_texto('mnutransacao','S','S','',50,50,'','');?></td>
      	</tr>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Tipo:</td>
	        <td>
	         	<select name="mnutipo" onchange="document.formulario.submit();" class="CampoEstilo">
					<option value=""></option>
					<option value="1" <?if ($mnutipo=="1") print "selected";?>>1</option>
					<option value="2" <?if ($mnutipo=="2") print "selected";?>>2</option>
					<option value="3" <?if ($mnutipo=="3") print "selected";?>>3</option>
					<option value="4" <?if ($mnutipo=="4") print "selected";?>>4</option>
				</select>
				<?=obrigatorio();?>
			</td>
      	</tr>
      	<?if ($mnutipo <> "" and $mnutipo <> "1") {?>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Menu Pai:</td>
	        <td>
	         	<?
				$sql="SELECT mnuid as codigo, mnudsc as descricao FROM seguranca.menu where mnutipo = " . ($mnutipo-1) . " and mnusnsubmenu='t' and sisid=".$_SESSION['sisid']."  order by mnudsc";
				$db->monta_combo("mnuidpai",$sql,'S',"Selecione o Menu",'','');
		 		?>
	    	 	<?=obrigatorio();?>
    	 	</td>
        </tr>
      	<?}?>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Possui Sub-menu?</td>
	        <td>
	        <input type="radio" name="mnusnsubmenu" value="t" onchange="submenu('S');" <?=($mnusnsubmenu=='t'?"CHECKED":"")?>> Sim
	            <input type="radio" name="mnusnsubmenu" value="f" onchange="submenu('N');" <?=($mnusnsubmenu=='f'?"CHECKED":"")?>> N�o
	        </td>
      	</tr> 
      	<tr>
	        <td id="sub0" align='right'  class="SubTituloDireita">Link:</td>
	        <td id="sub1"><?=campo_texto('mnulink','N','S','',50,100,'','');?></td>
      	</tr>
      	<tr>
	        <td id="sub2" align='right'  class="SubTituloDireita">Aba:</td>
	        <td id="sub3"><?$sql="SELECT abacod as codigo, abadsc as descricao FROM seguranca.aba where sisid=".$_SESSION['sisid'];
				$db->monta_combo("abacod",$sql,'S',"Selecione a Aba",'','');?>
			</td>
      	</tr>
	  	<tr>
	        <td id="sub4" align='right'  class="SubTituloDireita">Faz parte da �rvore?</td>
	        <td id="sub5"><input type="radio" name="mnushow" value="t" <?=($mnushow=='t'?"CHECKED":"")?>> Sim
	            <input type="radio" name="mnushow" value="f" <?=($mnushow=='f'?"CHECKED":"")?>> N�o
			</td>
      	</tr>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Estilo:</td>
	        <td><?=campo_texto('mnustile','N','S','',50,150,'','');?></td>
      	</tr>
      	<tr>
	        <td align='right'  class="SubTituloDireita">Html:</td>
	        <td><?=campo_textarea('mnuhtml','N','S','',60,5,500);?></td>
		</tr>
		<tr>
	        <td align='right'  class="SubTituloDireita">Imagem:</td>
	        <td><?=campo_texto('mnuimagem','N','S','',50,150,'','');?></td>
      	</tr>
      	<?php //PERDIL DE USU�RIOS
        if($_SESSION['sisid']){
	        $sql = "SELECT 
						pflcod as CODIGO,
						pfldsc as DESCRICAO 
					FROM 
						seguranca.perfil 
					WHERE 
						pflstatus='A'
						AND sisid=".$_SESSION['sisid']."
					ORDER BY
						pfldsc ";
	        
			if($_REQUEST['mnuid_int']){
				$sqlCarregado = "SELECT 
									pflcod as CODIGO,
									pfldsc as DESCRICAO 
								FROM 
									seguranca.perfil 
								WHERE 
									pflstatus='A'
									AND sisid=".$_SESSION['sisid']."
									AND pflcod IN (SELECT pflcod FROM seguranca.perfilmenu WHERE mnuid = ".$_REQUEST['mnuid_int'].")
								ORDER BY
									pfldsc ";
			}
			mostrarComboPopup( 'Perfil Associado', 'pflcod',  $sql, $sqlCarregado, 'Selecione o Perfil' ); 
		}
	if   ($_REQUEST["mnuid_int"]) { ?>
		<tr bgcolor="#CCCCCC">
	   		<td></td>
	   		<td><input type="button" name="btalterar" value="Alterar" onclick="validar_cadastro('A')" class="botao">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="btcancelar" value="Voltar" onclick="history.back();" class="botao"></td>
 		</tr>
<? } else { ?>
		<tr bgcolor="#CCCCCC">
   			<td></td><td><input type="button" name="btinserir" value="Incluir" onclick="validar_cadastro('I')" class="botao">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="btcancelar" value="Voltar" onclick="history.back();" class="botao"></td>
 		</tr>
<? } ?>
 	</table>
</form>
<?php 
}elseif($_REQUEST['acao']=='A' and !$_REQUEST['mnuid_int']){

//t�tulo da p�gina
monta_titulo($titulo_modulo,'Escolha uma a��o desejada referente ao Menu / M�dulo : 	<img src="../imagens/alterar.gif" border="0" align="absmiddle"> = Alterar / 	<img src="../imagens/excluir.gif" border="0" align="absmiddle"> = Excluir / 	<img src="../imagens/gif_inclui.gif" border="0" align="absmiddle"> = Incluir');
?>
<form method="POST"  name="formulario">
	<input type='hidden' name='mnuidpai'>
	<input type='hidden' name='mnutipo'>
	<input type='hidden' name='exclui'>
</form>
<?
//teste utilizando a fun��o Monta Lista
$cabecalho = array('A��es','C�digo','Menu / M�dulo','Vis�vel','Transa��o', 'Link');
//$sql = "select acacod, acadsc from acao";
$sql= "select  case when mnusnsubmenu=true then '<img border=\"0\" src=\"../imagens/alterar.gif\" title=\"Alterar Menu\" onclick=\"altera_menu('||mnuid||')\">&nbsp;&nbsp;&nbsp;<img border=\"0\" src=\"../imagens/excluir.gif\" title=\"Excluir Menu\" onclick=\"excluir_menu('||mnuid||')\">&nbsp;&nbsp;&nbsp;<img border=\"0\" src=\"../imagens/gif_inclui.gif\" title=\"Incluir Menu em � '||mnudsc||'\" onclick=\"incluir_menu('||mnuid||','||mnutipo||')\">' else '<img border=\"0\" src=\"../imagens/alterar.gif\" title=\"Alterar Menu\" onclick=\"altera_menu('||mnuid||')\">&nbsp;&nbsp;&nbsp;<img border=\"0\" src=\"../imagens/excluir.gif\" title=\"Excluir Menu\" onclick=\"excluir_menu('||mnuid||')\">' end as acao, mnucod, case when mnutipo=2 then '&nbsp;&nbsp;<img src=\"../imagens/seta_filho.gif\" align=\"absmiddle\">'||mnudsc when  mnutipo=3 then '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"../imagens/seta_filho.gif\" align=\"absmiddle\">'||mnudsc when  mnutipo=4 then '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"../imagens/seta_filho.gif\" align=\"absmiddle\">'||mnudsc else mnudsc end as mnudsc, case when mnushow=false then '<font color=#808080>N�o</font>' else '<font color=#008000>Sim</font>' end as mnushow, 
		mnutransacao, mnulink from seguranca.menu where mnustatus = 'A' and sisid=".$_SESSION['sisid']."  order by mnucod";
$db->monta_lista_simples($sql,$cabecalho,10000,20,'','','');
}
?>
<script>
<?if ($_REQUEST["mnuid_int"]){?>
if (document.formulario.mnusnsubmenu[0].checked)
{
	document.formulario.mnulink.disabled = true;
	document.formulario.abacod.disabled = true;
	for (i=0;i<6;i++)
	{
		document.getElementById("sub"+i).style.visibility = "hidden";
		document.getElementById("sub"+i).style.display = "none";
	}
}
<?
}
?>
function submenu(op)
{
	if (op == 'S')
	{
		document.formulario.mnulink.disabled = true;
		document.formulario.abacod.disabled = true;
		document.formulario.mnushow[0].checked = true;
		document.formulario.mnushow[1].checked = false;
		for (i=0;i<6;i++)
			{
			document.getElementById("sub"+i).style.visibility = "hidden";
			document.getElementById("sub"+i).style.display = "none";
			}
	}
	else
	{
		document.formulario.mnulink.disabled = false;
		document.formulario.abacod.disabled = false;
		for (i=0;i<6;i++)
		{
			document.getElementById("sub"+i).style.visibility = "visible";
			document.getElementById("sub"+i).style.display = "";
		}
	}
}

function altera_menu(cod) 
{
	location.href = '<?=$_SESSION['sisdiretorio']?>.php?modulo=sistema/menu/menu&acao=A&mnuid_int='+cod;
}
  
function incluir_menu(codpai, tipo) 
{
	document.formulario.mnuidpai.value = codpai;
	document.formulario.mnutipo.value = tipo+1;
	location.href = '<?=$_SESSION['sisdiretorio']?>.php?modulo=sistema/menu/menu&acao=I&mnuidpai='+document.formulario.mnuidpai.value+'&mnutipo='+document.formulario.mnutipo.value;
}
  
function excluir_menu(cod) 
{ 
	if( window.confirm( "Confirma a exclus�o do �tem "+ cod + " no Menu?") )
	{
		document.formulario.exclui.value = cod;
		document.formulario.submit();
    } else document.formulario.exclui.value = 0;
}
  
function validar_cadastro(cod) 
{    	
	if (!validaBranco(document.formulario.mnucod, 'C�digo')) return;	
	if (!validaBranco(document.formulario.mnudsc, 'Descri��o')) return;	
	if (!validaBranco(document.formulario.mnutransacao, 'Transa��o')) return;
	if (!validaBranco(document.formulario.mnutipo, 'Tipo')) return;			
	if (document.formulario.mnutipo.value != "1" )
	{		
		if (!validaBranco(document.formulario.mnuidpai, 'Menu Pai')) return;
	}		
	if (!validaRadio(document.formulario.mnusnsubmenu,'Possui sub-menu')) return;
	if (document.formulario.mnusnsubmenu[1].checked) 
	{
		if (!validaBranco(document.formulario.mnulink, 'Link')) return;
		if (!validaRadio(document.formulario.mnushow,'Faz Parte da �rvore')) return;
	}else	
	{
		document.formulario.mnulink.value = '';
		document.formulario.abacod.value = '';
	}
	selectAllOptions( document.formulario.pflcod );
	if (cod == 'I') document.formulario.act.value = 'inserir'; else document.formulario.act.value = 'alterar';
	document.formulario.submit();
}   

function onOffCampo( campo )
{
	var div_on 	= document.getElementById( campo + '_campo_on' );
	var div_off = document.getElementById( campo + '_campo_off' );
	var input 	= document.getElementById( campo + '_campo_flag' );
		
	if ( div_on.style.display == 'none' ){
		div_on.style.display = 'block';
		div_off.style.display = 'none';
		input.value = '1';
	}else{
		div_on.style.display = 'none';
		div_off.style.display = 'block';
		input.value = '0';
	}
}      
</script>