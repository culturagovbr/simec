<?php

interface PainelImp {
    /**
     * Busca Painel por Usuario Logado
     */
    public function getPainelByUsuario();
}
