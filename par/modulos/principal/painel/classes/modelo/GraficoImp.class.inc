<?php

interface GraficoImp {
    /**
     * Busca Painel por Usuario Logado
     */
    public function getGraficoByPainel();
}
