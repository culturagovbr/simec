<?php 
include_once APPRAIZ.'includes/classes/RequestHttp.class.inc';
 
$obMunicipio	= new Municipio();
if( $_REQUEST['geraPDF'] )
{
	$municipio		= $obMunicipio->descricaoMunicipio($_SESSION['par']['muncod'], false);
	
	$sql = "SELECT
			e.entnome
		FROM
			par.entidade e
		INNER JOIN par.instrumentounidade 	iu 	ON iu.inuid = e.inuid
		INNER JOIN territorios.municipio 	m 	ON m.muncod = iu.muncod
		INNER JOIN territorios.estado 		est ON est.estuf = m.estuf
		WHERE
			iu.muncod = '".$_SESSION['par']['muncod']."' AND e.dutid = ".DUTID_PREFEITO;
	/*
	$sql = "SELECT 
				entprefeito.entnome
            FROM 
            	entidade.entidade entprefeito
           	INNER JOIN 
           		entidade.funcaoentidade funprefeito ON entprefeito.entid = funprefeito.entid
            INNER JOIN 
            	entidade.funentassoc feaprefeito ON feaprefeito.fueid = funprefeito.fueid
            INNER JOIN 
            	entidade.entidade entprefeitura ON entprefeitura.entid = feaprefeito.entid
            INNER JOIN 
            	entidade.funcaoentidade funprefeitura ON funprefeitura.entid = entprefeitura.entid
            INNER JOIN 
            	entidade.endereco entd ON entd.entid = entprefeitura.entid
            INNER JOIN 
            	territorios.municipio mun ON entd.muncod = mun.muncod
            WHERE 
            	funprefeito.funid = 2 AND 
            	--funprefeitura.funid = 1 AND 
            	mun.muncod ='".$_SESSION['par']['muncod']."'";
	*/
	$prefeito = $db->pegaUm($sql);
	
	$sql = "SELECT
				tprcampus
			FROM
				par.termopronatec
			WHERE
				muncod = '".trim($_SESSION['par']['muncod'])."'";
	$instituto = $db->pegaUm($sql);
	
	if( $_SESSION['par']['muncod'] != '5300108' )
	{
		$pref =	'<br />
			 	'.$prefeito.'
			 	<br /><br />
			 	Prefeito(a) de '.$municipio.'';
	}
	
	$html = '<center><font size="14px"><b>TERMO DE COMPROMISSO</b></font></center>
			 <br /><br />
			 <p>
			 Com a finalidade de ser credenciado como munic�pio sede para implanta��o do campus '.$municipio.' do Instituto Federal de Educa��o Profissional, Ci�ncia e Tecnologia '.ucwords(strtolower($instituto)).', na qualidade de representante legal, devidamente autorizado, firmo em nome  do munic�pio de '.$municipio.' o compromisso  de promover a transfer�ncia dominial, no prazo m�ximo de 150 dias, a partir da assinatura deste termo,  devidamente legalizado, de �rea de terra ou infraestrutura  f�sica edificada  em conson�ncia com os requisitos listados  neste documento e aprovado pelo Instituto Federal  com vistas � instala��o  do campus.
			 <br /><br />
			 Ciente de que o n�o cumprimento da transfer�ncia do im�vel para propriedade do ente federado at� a data prevista, autoriza a institui��o respons�vel pela implanta��o da unidade a buscar munic�pio alternativo  para execu��o do pleito.
			 <br /><br />
			 <center>
			 '.$municipio.', '.date('d').' de '.ucwords(strtolower(pegaMes())).' de 2011
			 <br /><br /><br /><br />
			 __________________________________________
			 '.$pref.'
			 </center>
			 </p>';
	
	$trans = get_html_translation_table(HTML_ENTITIES);
	$html = strtr($html, $trans);
	$html = str_replace("&lt;", "<", $html);
	$html = str_replace("&gt;", ">", $html);
	
	$http = new RequestHttp();
	$http->toPdfDownload($html, 'termo_compromisso');
	die;
}


function pegaMes()
{
	$mes = date('m');
	
	switch ($mes)
	{
		case 1: $mes = "JANEIRO"; break;
		case 2: $mes = "FEVEREIRO"; break;
		case 3: $mes = "MAR�O"; break;
		case 4: $mes = "ABRIL"; break;
		case 5: $mes = "MAIO"; break;
		case 6: $mes = "JUNHO"; break;
		case 7: $mes = "JULHO"; break;
		case 8: $mes = "AGOSTO"; break;
		case 9: $mes = "SETEMBRO"; break;
		case 10: $mes = "OUTUBRO"; break;
		case 11: $mes = "NOVEMBRO"; break;
		case 12: $mes = "DEZEMBRO"; break;
	}
	
	return $mes;
}
?>
<html>
	<head>
		<title>SIMEC - Sistema Integrado de Monitoramento Execu��o e Controle do Minist�rio da Educa��o</title>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
		<link rel="stylesheet" href="/includes/JQuery/jquery-ui-1.8.4.custom/css/jquery-ui.css" type="text/css" media="all" />
		<script language="JavaScript" src="../../includes/funcoes.js"></script>
		<script type="text/javascript" src="../includes/entidadesn.js"></script>
		<script type="text/javascript" src="/includes/JQuery/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="/includes/JQuery/jquery-ui-1.8.4.custom.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../includes/jquery-validate/css/validate.css" />
		<script type="text/javascript" src="../includes/jquery-validate/jquery.validate.js"></script>
		<script type="text/javascript" src="../includes/jquery-validate/localization/messages_ptbr.js"></script>
		<script type="text/javascript" src="../includes/jquery-validate/lib/jquery.metadata.js"></script>
		<style>
		.ui-widget
		{
			font-size: 10px;
		}
		</style>
		<script type="text/javascript">
		jQuery(document).ready(function()
		{
			jQuery('#btOk').click(function()
			{
				jQuery( "#dialog-message" ).dialog( "close" );
			});
			
		});
		
		jQuery(function() {
			jQuery( "#dialog:ui-dialog" ).dialog( "destroy" );
			
			jQuery( "#dialog-message" ).dialog({
				modal: true,
				height: 600,
				width: 550
			});
		});
		</script>
	</head>
	<body>
	<?php if($_REQUEST['tipoAba']=='TermoCompromisso'||$_REQUEST['tipoAba']==''){ ?>
		<div id="dialog-message" title="TERMO DE ADES�O">
			<p style="font-weight:bold;text-align:center;">
				MINIST�RIO DA EDUCA��O<br />
SECRETARIA DE EDUCA��O PROFISSIONAL E TECNOL�GICA<br />
DIRETORIA DE DESENVOLVIMENTO DA REDE FEDERAL DE EPCT
			</p>
			<br />
			<p style="font-weight:bold;text-align:center;">
				PLANO DE EXPANS�O DA REDE FEDERAL DE EDUCA��O<br />
				PROFISSIONAL, CIENT�FICA E TECNOL�GICA - 2011/2014
			</p>
			<br />
			<p style="font-weight:bold;text-align:center;">
				TERMO DE ADES�O 
			</p>
			<br />
			<p style="font-weight:bold;text-align:left;">
				 CONTEXTUALIZA��O
			</p>
			<p style="text-align:left;">
			O Plano de Expans�o da Rede Federal de Educa��o Profissional, Cient�fica e Tecnol�gica com unidades a serem instaladas no quadri�nio 2011/2014, constitui-se na iniciativa do Governo Federal, por interm�dio do Minist�rio da Educa��o, de implantar, nos pr�ximos quatro anos, 201 (duzentas e uma) novas unidades da Rede, sendo 81 j� em processo de implanta��o e 120 objeto do atual termo de ades�o. Podendo assim oferecer ao pa�s condi��es favor�veis � forma��o e qualifica��o profissional nos diversos n�veis e modalidades de ensino, suporte ao desenvolvimento da atividade produtiva, oportunidades de gera��o e dissemina��o de conhecimentos cient�ficos e tecnol�gicos e est�mulo ao desenvolvimento socioecon�mico em n�veis local e regional.
			<br /><br />
			As novas unidades de ensino estar�o localizadas nos munic�pios, distribu�dos nos 26 estados e no Distrito Federal, relacionados no Anexo I. A defini��o das localidades contempladas orientou-se por uma abordagem multicriterial, fundamentada em an�lise de vari�veis econ�micas, demogr�ficas, socioambientais, culturais e geogr�ficas abaixo relacionadas:
			<br /><br />
			1) PARA DEFINI��O DE N�MERO DE UNIDES POR ESTADO
			<br />
			<ul>
				<li>Popula��o do estado em rela��o � popula��o do Pa�s; </li>
				<li>Presen�a de escolas federais e/ou estaduais de educa��o Profissional em atividade; </li>
				<li>Rela��o inversa do �ndice de Desenvolvimento da Educa��o B�sica - IDEB por estado;</li>
				<li>Percentual de jovens entre 14 e 18 anos nas s�ries finais do ensino fundamental.</li>
			</ul>
			2) PARA DEFINI��O DE MUNIC�PIOS
			<ul>
				<li>Universaliza��o de atendimento aos Territ�rios da Cidadania; </li>
				<li>Atendimento aos munic�pios populosos e com baixa receita per capita; </li>
				<li>Munic�pios com percentual elevado de extrema pobreza;  </li>
				<li>Atendimento priorit�rio aos munic�pios ou microrregi�es com popula��o acima de 50.000 habitantes;   </li>
				<li>Universaliza��o do atendimento �s mesorregi�es Brasileiras; </li>
				<li>Munic�pios em microrregi�es n�o atendidas por escolas federais; </li>
				<li>Munic�pios com Arranjos Produtivos Locais -APLs identificados; </li>
				<li>Entorno de grandes investimentos;</li>
				<li>Interioriza��o da oferta p�blica de Educa��o Profissional e Ensino Superior.</li>	
			</ul>
			</p>
			<p style="font-weight:bold;text-align:left;">DAS CARACTER�STICAS DO PLANO</p>
			<p style="text-align:left;">
			O Minist�rio da Educa��o, na qualidade de �rg�o respons�vel pela implanta��o das novas unidades de ensino e de mantenedor da Rede Federal de Educa��o Profissional, Cient�fica e Tecnol�gica, ser� o principal agente na realiza��o dos investimentos em obras de constru��o, amplia��o e reforma de espa�os f�sicos; aquisi��o de equipamentos, mobili�rios e acervo bibliogr�fico para as atividades administrativas e did�tico-pedag�gicas, incluindo as pr�ticas laboratoriais; sele��o e contrata��o de pessoal docente e t�cnico administrativo para adequado funcionamento das unidades; e aloca��o de recursos financeiros destinados � gest�o e manuten��o dos novos estabelecimentos de ensino.
			<br /><br />
			Para confirma��o da implanta��o do novo campus do Instituto Federal, conforme proposto, caber� ao Munic�pio (e/ou Distrito Federal) hospedeiro apenas a doa��o � Uni�o/ Instituto Federal correspondente, de �rea de terra ou infraestrutura pr�-existente compat�vel com a instala��o da referida unidade, que se dar� atrav�s da assinatura de TERMO DE COMPROMISSO, por parte do munic�pio pr�-selecionado, se comprometendo a promover a doa��o de terreno, terra nua e/ou im�vel edificado que atendam os requisitos abaixo relacionados:
			</p>
			<p style="font-weight:bold;text-align:center;">
			Terreno - �rea nua
			</p>
			<p style="text-align:left;">
			<ul>
				<li>Terreno no per�metro urbano destinado � implanta��o de unidade de ensino que atuar�, prioritariamente, no setor de ind�stria, com�rcio e/ou servi�os. A �rea disponibilizada dever� ser de no m�nimo 20.000 m� (metros quadrados), com �rea ideal acima de 50.000 m�.</li>
				<li>Terreno em �rea rural, com afastamento limite m�ximo de 08 Km do per�metro urbano, destinado � implanta��o de unidade de ensino que atuar�, prioritariamente, no setor agropecu�rio. A �rea dever� ter no m�nimo 60 hectares, com �rea ideal acima dos 150 hectares.</li>
				<li>O im�vel indicado dever� apresentar condi��es adequadas de interliga��o �s redes p�blicas de abastecimento de �gua, eletrifica��o e telefonia.</li>
				<li>O im�vel indicado dever� apresentar condi��es favor�veis de acesso aos alunos e servidores, mediante exist�ncia de:</li>
				<ul>
					<li>Pavimenta��o nas vias de tr�fego que d�o acesso ao im�vel indicado;  linhas regulares de transporte urbano ou rural; ou servi�os p�blicos de transporte escolar.</li>
					<li>Caracter�sticas topogr�ficas favor�veis � realiza��o de obras civis.</li>
					<li>Caracter�sticas geol�gicas adequadas no caso de im�veis rurais, como a exist�ncia de veio de �gua perene e de um m�nimo de 50% de �rea agricult�vel.</li>
					<li>Comprova��o da inexist�ncia de �bices de natureza ambiental, jur�dica e dominial, que possam inviabilizar ou retardar a sua utiliza��o para os fins em quest�o.</li>
				</ul>
			</ul>
			</p>
			<br />
			<p style="font-weight:bold;text-align:center;">
			Im�vel edificado
			</p>
			<p style="text-align:left;">
			Im�vel edificado poder� substituir a doa��o de terreno, <b>agilizando o processo de implanta��o da escola</b>, desde que vistoriado e aprovado pelo Instituto Federal respons�vel e que atenda os requisitos abaixo:
			<br />
			<ul>
				<li>Possuir �rea compat�vel com a proposta e facilidade de adapta��o da estrutura existente para fins de constitui��o de espa�os adequados � pr�tica pedag�gica e de forma��o profissional</li>
				<li>Confirma��o, mediante parecer t�cnico expedido por �rg�o ou entidade competente, sobre a inexist�ncia de patologias ou anomalias de constru��o que impliquem comprometimento estrutural na edifica��o indicada pelo proponente</li>
				<li>Promover doa��o legal e definitiva do bem im�vel a fim de ser incorporado ao patrim�nio do Instituto Federal respons�vel pela instala��o da unidade.</li>
			</ul>
			</p>
			<br />
			<p style="font-weight:bold;text-align:left;">OBSERVA��ES:</p>
			<p style="text-align:left;">
			&nbsp;&nbsp;&nbsp;A. No caso de a regulariza��o definitiva do im�vel demandar per�odo que  coloque em risco a implanta��o da unidade  no prazo programado,  o munic�pio respons�vel ser� substitu�do por outro munic�pio  com as mesmas caracter�sticas.
			<br />
			&nbsp;&nbsp;&nbsp;B. Terrenos e/ou im�veis edificados, a serem  disponibilizados  ser�o cadastrados  em tela pr�pria deste m�dulo, e   somente ser�o aceitos ap�s vistoria e aprova��o por parte do corpo dirigente do Instituto Federal respons�vel pela implanta��o da unidade.
			</p>
			<br />
			<center><input type="button" value="OK" style="width:80px;" id="btOk" /></center>
			<br />
		</div>
		<?php } ?>
		<?php
		
		$anoref = $_GET['ano'] ? $_GET['ano'] : '2011';
		$_SESSION['par']['preid'] 	= ($_REQUEST['preid']) ? $_REQUEST['preid'] : $_SESSION['par']['preid'];
		
		$_SESSION['par']['prog'] = $_REQUEST['prog'] != $_SESSION['par']['prog'] ? $_REQUEST['prog'] : 'nulo';
		
		if($_REQUEST['preid'] && !$_GET['muncod']){
			$_SESSION['par']['preid'] 	= $_REQUEST['preid'];
			$_SESSION['par']['estuf'] 	= $_SESSION['par']['estuf'] ? $_SESSION['par']['estuf'] : $obMunicipio->recuperarUF($_SESSION['par']['muncod']);
		}
		
		if($_GET['muncod']){
			$_SESSION['par']['preid'] 	= $_GET['preid'];
			$_SESSION['par']['muncod'] 	= $_GET['muncod'];
			$_SESSION['par']['estuf'] 	= $obMunicipio->recuperarUF($_SESSION['par']['muncod']);
		}
		
		if(!$_REQUEST['preid']){
			unset($_SESSION['par']['preid']);
		}
		
		//includes
		
		require_once APPRAIZ . "includes/ActiveRecord/classes/Endereco.php";
		require_once APPRAIZ . 'includes/workflow.php';
		require_once APPRAIZ . "includes/classes/fileSimec.class.inc";
		
		//Fim includes
		$_SESSION['sisdiretorio'] = 'par';
		
		$tipoAba = ($_REQUEST['tipoAba']) ? $_REQUEST['tipoAba'] : 'TermoCompromisso';
		$chamada = 'pronatec'.ucwords($tipoAba);
		
		$arParametros = array('visao' => $chamada);
		$obItensComposicaoControle = new ItensComposicaoControle();
		
		$obItensComposicaoControle->visualizacaoItensComposicaoPronatec($arParametros);
		?>
	</body>
</html>