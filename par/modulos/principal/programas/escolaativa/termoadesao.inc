<?php 
				
define('ESCOLADATERRA_ADERIRAM', 				1);
define('ESCOLADATERRA_NAOADERIRAM', 			2);
define('ESCOLADATERRA_CLASSESMULTISERIADAS', 	3);				

if($_REQUEST['requisicao'] == 'adesaoPrograma'){
	
	$sql = "update par.escolaativa set 
				usucpfescolaativa = '{$_SESSION['usucpf']}',
				esasituacaoadesao = ".$_REQUEST['esasituacaoadesao']."
			where 
				inuid = {$_SESSION['par']['inuid']} ";
	
	$db->executar($sql);
	if($db->commit()){
		echo $_REQUEST['esasituacaoadesao'];
		die;
	}
	echo 0;
}

if($_SESSION['par']['inuid']){
	
	$sql = "select
				esaid, 
				esasituacaoadesao 
			from 
				par.escolaativa 
			where 
				inuid = {$_SESSION['par']['inuid']}";
	
	$rs = $db->pegaLinha($sql);
	if($rs) extract($rs);	
}

//Chamada de programa
include_once APPRAIZ . "includes/cabecalho.inc";
echo "<br/>";



$linha1 = 'Escola da Terra'; 
if($_SESSION['par']['muncod']){
	
	$sql = "select mundescricao, estuf from territorios.municipio where muncod = '{$_SESSION['par']['muncod']}'";
	$rsMunicipio = $db->pegaLinha($sql);
	$linha2 = $rsMunicipio['mundescricao'].' - '.$rsMunicipio['estuf'];
	
}else{
	
	$linha2 = $_SESSION['par']['estuf'];
}
monta_titulo($linha1, $linha2);

if(isset($esasituacaoadesao) && $esasituacaoadesao == 1){
	echo "<script>
			window.location.href = 'par.php?modulo=principal/programas/escolaativa/listaEscolas&acao=A';
		  </script>";
}else
if(isset($esasituacaoadesao) && (int) $esasituacaoadesao == 2){
	$stResposta =  "N�o confirmou a ades�o.";	
} else
if (isset($esasituacaoadesao) && $esasituacaoadesao == 3){
	$stResposta = "N�o h� classes multisseriadas.";	
} else 
if(!isset($esaid)){
	$stResposta = "O munic�pio n�o possui escola ativa.";	
}

?>
<?php if(!isset($esasituacaoadesao)): ?>
	<?php echo "<br/>"; monta_titulo('Termo de Ades�o',''); ?>
	<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
	<script>
		$(function(){
			$('.adesaoEscolaTerra').click(function(){
	
				$.ajax({
					url		: 'par.php?modulo=principal/programas/escolaativa/termoadesao&acao=A',
					type	: 'post',
					data	: 'requisicao=adesaoPrograma&esasituacaoadesao='+this.id,
					success	: function(e){
						if(e == 1){
							window.location.href = 'par.php?modulo=principal/programas/escolaativa/listaEscolas&acao=A';						
						}else
						if(e == 2 || e == 3){
							window.location.href = 'par.php?modulo=principal/programas/escolaativa/termoadesao&acao=A';
						}else{
							alert('Erro ao realizar opera��o!');
							window.location.href = 'par.php?modulo=principal/programas/escolaativa/termoadesao&acao=A';
						}						
					}
				});
			});
		});
	</script>
	<center>
		<table bgcolor="#f5f5f5" align="center" class="tabela" >
			<tr>
				<td width="90%" align="center">
					<br />
					<div style="overflow: auto; height:400px; width: 70%; background-color: rgb(250,250,250); border-width:1px; border-style:solid;" align="left" >
						<div style="width: 95%; magin-top: 10px; padding:10px;">
						
						<center>
						<img width="80" height="80" src="/imagens/brasao.gif" />
						<h2>
						<p>MINIST�RIO DA EDUCA��O<br/>
						SECRETARIA DE EDUCA��O CONTINUADA, ALFABETIZA��O  <br/>DIVERSIDADE E INCLUS�O<br/>
						DIRETORIA DE POL�TICAS DE EDUCA��O DO CAMPO, <br/>IND�GENA E PARA AS RELA��ES �TNICO-RACIAIS.</p>
						</h2>
						</center>
						
						<font size="2">
							<p>Senhor (a) Secret�rio (a) de Educa��o,</p>
							
							<p>O Minist�rio da Educa��o, no �mbito das a��es do Plano de Desenvolvimento da Educa��o, lan�a o Programa Nacional de Educa��o do Campo, que estabelece 
							dentre seus atos o Programa Escola da Terra, para beneficiar estados e munic�pios de todas as regi�es do Pa�s. O Programa destina-se �s escolas do 
							campo que oferecem os anos iniciais do ensino fundamental, nos sistemas municipal e estadual, em turmas organizadas sob a forma de multisseria��o.<br/> 
							Para o ano em curso, ser� feita a ades�o dos estados e munic�pios que n�o foram atendidos pelo Programa "Escola Ativa".</p>
							
							<p>Para fazer a ades�o, o (a) secret�rio (a) de educa��o dever� acessar o Plano de A��es Articuladas do estado/munic�pio no Simec, pelo endere�o eletr�nico 
							<a href="http://simec.mec.gov.br" target="_blank">http://simec.mec.gov.br</a>, onde est� dispon�vel o Termo de Ades�o. Para orientar o processo de ades�o, encaminha-se um arquivo com os procedimentos a 
							serem adotados (Passo a Passo da Ades�o 2012 - Programa Escola da Terra).</p>
							 
							<p>Ressalta-se que a ades�o pode ser realizada pelos dirigentes que t�m escolas com classes multisseriadas na sua rede de ensino e tem interesse em
							implementar o Programa Escola da Terra; para aqueles que j� aderiram ao Programa Escola Ativa em anos anteriores e desejam expandir para outras 
							escolas multisseriadas da rede, que porventura ainda n�o foram atendidas, o processo poder� ser realizado a partir de 2013.</p>
							
							<p>Colocamo-nos � disposi��o para quaisquer esclarecimentos que se fizerem necess�rios.</p>					
							
							<p>Atenciosamente,<br/>
							Coordena��o Geral de Pol�ticas de Educa��o do Campo/SECADI/MEC.<br/>
							<a href="mailto:coordenacaoeducampo@mec.gov.br">coordenacaoeducampo@mec.gov.br</a></p>
						</font>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<p>
		<input type="button" name="aceito" value="Iniciar Processo de Ades�o" class="adesaoEscolaTerra" id="1"/>
		<input type="button" name="naceito" value="N�o Confirmo a Ades�o" class="adesaoEscolaTerra" id="2" />
		<br/>
		<input type="button" name="naohaclassesmulti" value="N�o h� Classes Multisseriadas" class="adesaoEscolaTerra" id="3" />
		<input type="button" name="cancelar" value="Sair" onclick="window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa'" />
	</center>
<?php else: ?>
	<table bgcolor="#f5f5f5" align="center" class="tabela" >
		<tr>
			<td align="center" valign="middle" height="40">				
				<font size="2"><?php echo $stResposta; ?></font>
			</td>
		</tr>
	</table>
	<center>
		<p><input type="button" name="cancelar" value="Sair" onclick="window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa'" /></p>
	</center>
<?php endif; ?>
