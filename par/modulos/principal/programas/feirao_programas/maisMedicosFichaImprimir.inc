<?php
include_once '_funcoes_maismedicos.php';

if($_SESSION['par']['rqmid']){
	$infor = visualizarInfor($_SESSION['par']['rqmid']);
	extract($infor);
}

$sus_representante = verificarExisteGestorRepres();
$parceria = verificarParceria();
$adesao = verificarAdesao();
$ficha = recuperaDadosFicha($_SESSION['par']['rqmid']);
extract($ficha);

$mes = date('m');
switch ($mes){
	case 1: $mes = "Janeiro"; break;
	case 2: $mes = "Fevereiro"; break;
	case 3: $mes = "Mar�o"; break;
	case 4: $mes = "Abril"; break;
	case 5: $mes = "Maio"; break;
	case 6: $mes = "Junho"; break;
	case 7: $mes = "Julho"; break;
	case 8: $mes = "Agosoto"; break;
	case 9: $mes = "Setembro"; break;
	case 10: $mes = "Outubro"; break;
	case 11: $mes = "Novembro"; break;
	case 12: $mes = "Dezembro"; break;
}
?>

<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
<html>
<head>
    <title>Mais M�dicos - Ficha de Avalia��o T�cnico</title>
    <style>
        @media print {
                .notprint {
                    display: none;
                }
                .div_rolagem {
                    display: none;
                }
        }
        .div_rolagem{
            overflow-x: auto;
            overflow-y: auto;
            height: 50px;
        }
        .notscreen {
            display: none;
        }
        .bordaarredonda{
            background:#FFFFFF;
            color:#000; border: #000 1px solid;
            padding: 5px;
            -moz-border-radius:3px 3px;
            -webkit-border-radius:3px 3px;
            border-radius:3px 3px;
            width:98%;
            text-align:left;
        }
        .quebra {
            page-break-after: always !important;
            height: 0px;
            clear: both;
        }
    </style>
</head>
<body>
<table border="0" width="105%" cellspacing="1" cellpadding="5" border="0" align="center">
 	<tr>
 		<td>
			<table width="105%" border="0" cellpadding="0" cellspacing="0" class="notscreen1 debug">
				<tr bgcolor="#ffffff">
					<td valign="top" align="center">
						<img src="../imagens/brasao.gif" width="45" height="45" border="0">
						<br><b>MINIST�RIO DA EDUCA��O<br/>
						SECRETARIA DE REGULA��O E SUPERVIS�O DA EDUCA��O SUPERIOR</b> <br />
					</td>
				</tr>
			</table>
 		</td>
 	</tr>
</table>
<br>
<table border="0" width="105%" cellspacing="1" cellpadding="5" border="0" align="center">
 	<tr>
 		<td>
			<table width="105%" border="0" cellpadding="0" cellspacing="0" class="notscreen1 debug">
				<tr bgcolor="#ffffff">
					<td valign="top" align="center">
						<p align="center" style="margin-bottom: 0px;"><font face="Arial, sans-serif" size="2"><b>Ficha de Avalia��o</b></font></p><br>
					</td>
				</tr>
				<tr bgcolor="#ffffff">
					<td valign="top" align="center">
						<p align="center" style="margin-bottom: 0px;"><font face="Arial, sans-serif" size="2"><b><?php echo recuperaMunicipioEstado(); ?></b></font></p><br>
					</td>
				</tr>
			</table>
 		</td>
 	</tr>
</table>
<table id="termo" width="105%" align="center" border="0" cellpadding="3" cellspacing="1">
	<tr>
		<td style="font-size: 12px; font-family:arial;">
			<table align="center" border="0" cellspacing="1" cellpadding="3" width="90%">
				<tr>
					<td align="left"><b><font size="2">1. PRIMEIRA ETAPA DA PR�-SELE��O</font></b></td>
				</tr>
				<tr>
					<td align="left"><b>O munic�pio atende aos crit�rios abaixo:</b></td>
				</tr>
				<tr>
					<td align="center">
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>1.1 Ter 70 mil ou mais habitantes?</b>&nbsp;<?php echo $rqmquestao03 > 70000 ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsaval70mil; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>1.2 N�o se constitui capital de estado?</b>&nbsp;<?php echo $rqmquestao04 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavacapitalestado; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>1.3 N�o possui curso de medicina em seu territ�rio?</b>&nbsp;<?php echo $rqmquestao05 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalcursomedicina; ?></font></p>
						</div>
					</td>
				</tr>
				<tr>
					<td height="25"></td>
				</tr>
				<tr>
					<td align="left"><b><font size="2">2. SEGUNDA ETAPA DA PR�-AN�LISE</font></b></td>
				</tr>
				<tr>
					<td align="center">
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.1 n�mero de leitos dispon�veis SUS por aluno maior ou igual a 5 (cinco), ou seja, para um curso com 50 vagas, o munic�pio dever� possuir, no m�nimo, 250 leitos dispon�veis SUS?</b>&nbsp;<?php echo $rqmquestao06 > 250 ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsaval250leitos; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.2 Estabeleceu parcerias?</b>&nbsp;<?php echo $parceria == 't' ? 'Sim' : 'N�o'; ?></font></p>
						<p align="justify"><font size="2"><?php echo $rqmobsavalparceria; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.3 N�mero de alunos por equipe de aten��o b�sica menor ou igual a 3 (tr�s), considerando o m�nimo de 17 (dezessete) equipes?</b>&nbsp;<?php echo $rqmquestao07 <= 3 ? 'checked="Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalalunoequipbasica; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.4 Exist�ncia de leitos de urg�ncia e emerg�ncia ou Pronto Socorro?</b>&nbsp;<?php echo $rqmquestao08 == 't' ? 'Sim' : 'N�o'; ?></font></p>
						<p align="justify"><font size="2"><?php echo $rqmobsavalleitourgencia; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.5 Exist�ncia de, pelo menos 3 (tr�s), Programas de Resid�ncia M�dica nas especialidades priorit�rias: (1) Cl�nica M�dica; (2) Cirurgia; (3)Ginecologia-Obstetr�cia; (4) Pediatria e (5) Medicina de Fam�lia e Comunidade?</b>&nbsp;<?php echo $rqmquestao10 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalresidmedica; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.6 Aderiu ao Termo de Compromisso de Resid�ncia?</b>&nbsp;<?php echo $rqmaceitetermoresidencia == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobstermoresidencia; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.7 Ades�o pelo munic�pio ao Programa Nacional de Melhoria do Acesso e da Qualidade na Aten��o B�sica - PMAQ, do Minist�rio da Sa�de?</b>&nbsp;<?php echo $rqmquestao11 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalpmaq; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.8 Exist�ncia de Centro de Aten��o Psicossocial - CAPS?</b>&nbsp;<?php echo $rqmquestao12 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalcaps; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.9 Hospital de ensino ou unidade hospitalar com potencial para hospital de ensino, conforme legisla��o de reg�ncia?</b>&nbsp;<?php echo $rqmquestao13 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalhospensino; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.10 Exist�ncia de hospital com mais de 100 (cem) leitos exclusivos para o curso?</b>&nbsp;<?php echo $rqmquestao14 == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsaval100leitos; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.11 Indicou o gestor local do sus e o representante legal?</b>&nbsp;<?php echo $sus_representante == 't' ? 'Sim' : 'N�o'; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsavalgestorlocal; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.12 Aderiu ao Termo de Ades�o de munic�pios?</b>&nbsp;<?php echo $adesao == 'S' ? 'Sim' : 'N�o'; ?></font></p>
						<p align="justify"><font size="2"><?php echo $rqmobsavaltermomunic; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.13 Apresentou a documenta��o comprobat�ria?</b>&nbsp;<?php echo $rqmsitdoccomprobatoria == 't' ? 'Sim' : 'N�o'; ?></font>
							<div class="bordaarredonda">
							<font size="2" style="text-align: justify;"><b>a) oficio de apresenta�ao apresentado assinado pelo dirigente municipal?</b>&nbsp;<?php echo $rqmoficiodirigente == 't' ? 'Sim' : 'N�o'; ?></font>
							<p align="justify"><font size="2"><?php echo $rqmobsavaldocoficio; ?></font></p>
							</div>
							<br>
							<div class="bordaarredonda">
							<font size="2" style="text-align: justify;"><b>b) c�pia do documento de RG e CPF do dirigente municipal e do gestor local do SUS?</b>&nbsp;<?php echo $rqmcopiargcpfdir == 't' ? 'Sim' : 'N�o'; ?></font>
							<p align="justify"><font size="2"><?php echo $rqmobsavaldocrgcpf; ?></font></p>
							</div>
							<br>
							<div class="bordaarredonda">
							<font size="2" style="text-align: justify;"><b>c) Projeto de melhorias da estrutura de equipamentos e programas de sa�de existentes no munic�pio?</b>&nbsp;<?php echo $rqmprojetomelhoria == 't' ? 'Sim' : 'N�o'; ?></font>
							<p align="justify"><font size="2"><?php echo $rqmobsavalprojetomelhoria; ?></font></p>
							</div>
							<br>
							<div class="bordaarredonda">
							<font size="2" style="text-align: justify;"><b>d) documentos comprobat�rios dos itens 3.1 e 3.3 do Edital?</b>&nbsp;<?php echo $rqmdocitem3133 == 't' ? 'Sim' : 'N�o'; ?></font>
							<p align="justify"><font size="2"><?php echo $rqmobsavalitens3133; ?></font></p>
							</div>
							<br>
							<div class="bordaarredonda">
							<font size="2" style="text-align: justify;"><b>e) documentos comprobat�rios dos itens 4.1, 4.2 e 4.3 do Edital?</b>&nbsp;<?php echo $rqmdocitem414243 == 't' ? 'Sim' : 'N�o'; ?></font>
							<p align="justify"><font size="2"><?php echo $rqmobsaval414243; ?></font></p>
							</div>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>2.14 S�ntese da avalia��o?</font>
						<p align="justify"><font size="2"><?php echo $rqmsintesesegundaetapa; ?></font></p>
						</div>
						<br>
					</td>
				</tr>
				<tr>
					<td height="25"></td>
				</tr>
				<tr>
					<td align="left"><b><font size="2">3. TERCEIRA ETAPA DA PR�-AN�LISE</font></b></td>
				</tr>
				<tr>
					<td align="left"><b>O projeto de melhorias atende aos objetivos do edital n� 03, quanto �:</b></td>
				</tr>
				<tr>
					<td align="center">
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.1 Amplia��o de equipes de aten��o b�sica?</b>&nbsp;<?php echo $ampliacaoequipebasica; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsampliacaoequipebasica; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.2 Amplia��o de leitos dispon�veis SUS (especificar o tipo de leito)?</b>&nbsp;<?php echo $ampliacaoleitos; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsampliacaoleitos; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.3 Cria��o/amplia��o de leitos de urg�ncia e emerg�ncia?</b>&nbsp;<?php echo $leitosurgencia; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsleitosurgencia; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.4 Hospital de ensino ou unidade com potencial de hospital de ensino?</b>&nbsp;<?php echo $hospitalensino; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobshospitalensino; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.5 Constru��o/Amplia��o de hospital com leitos exclusivos para o curso?</b>&nbsp;<?php echo $hospitalleitoscurso; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobshospitalleitoscurso; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.6 Cria��o/amplia��o de programas de sa�de?</b>&nbsp;<?php echo $ampliacaoprogsaude; ?></font>
						<p align="justify"><font size="2"><?php echo $rqmobsampliacaoprogsaude; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.7 Outras informa��es?</b></font>
						<p align="justify"><font size="2"><?php echo $rqmoutrainfo; ?></font></p>
						</div>
						<br>
						<div class="bordaarredonda">
						<font size="2" style="text-align: justify;"><b>3.8 S�ntese da avalia��o e sugest�o de deferimento ou indeferimento?</b></font>
						<p align="justify"><font size="2"><?php echo $rqmsinteseterceiraetapa; ?></font></p>
						</div>
						<br>
						<p align="right" style="margin-right: 0.98in;"><font size="2">Bras�lia-DF <?php echo date('d'); ?> de <?php echo $mes; ?> 2013</font></p>
					</td>
				</tr>
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table id="termo" width="105%" align="center" border="0" cellpadding="3" cellspacing="1">
	<tr>
		<td class="div_rolagem" style="text-align: center;">
			<input type="button" name="impressao" id="impressao" value="Imprimir" onclick="window.print();" />
			<input type="button" name="fechar" id="fechar" value="Fechar" onclick="window.close();" />
		</td>
	</tr>
</table>
</body>
</html>