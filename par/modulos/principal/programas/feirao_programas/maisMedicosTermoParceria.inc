<?php 
include_once '_funcoes_maismedicos.php';
$municipio = recuperarMunicipio();
$total_leitos = totalLeitos($_REQUEST['pmmid']);
$regiao_saude = recuperarRegiaoSaude();
$municipio_parceiro = recuperarMunicipioParceiro($_REQUEST['pmmid']);
$m = recuperaDadosPrefeitura();
$gestor_sus = recuperaGestorSusMunicipio();
?>

<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>

<html>
<head>
	<title>Mais M�dicos - Termo de Parceria</title>
	<style>
            @media print { 
                .notprint {
                    display: none;
                }
                .div_rolagem {
                    display: none
                }
            }	
            .div_rolagem{ 
                overflow-x: auto; 
                overflow-y: auto; 
                height: 50px;
            }
            .notscreen {
                display: none;
            }				
            .bordaarredonda { 
                background:#FFFFFF; 
                color:#000; border: #000 1px solid; 
                padding: 10px; 
                -moz-border-radius:10px 10px;
                -webkit-border-radius:10px 10px;
                border-radius:10px 10px;
                width:95%;
                text-align:left;
            }		
            .quebra{ 
                page-break-after: always !important;
                height: 0px; 
                clear: both;
            }	
	</style>
</head>
<body>
<table border="0" width="85%" cellspacing="1" cellpadding="5" border="0" align="center">
 	<tr>
 		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="notscreen1 debug">
				<tr bgcolor="#ffffff"> 	
					<td valign="top" align="center">
						<b>TERMO DE PARCERIA<br/>				
						<?php echo $municipio; ?></b> <br />
					</td>
				</tr>
			</table>						 		
 		</td>
 	</tr>
</table>
<br>
<table id="termo" width="95%" align="center" border="0" cellpadding="3" cellspacing="1">
	<tr>
		<td style="font-size: 12px; font-family:arial;">
			<table align="center" border="0" cellspacing="1" cellpadding="3" width="85%">
				<tr>
					<td align="center">
						<p style="text-align: justify; font-size: 15px;">
				        Os munic�pios de <?php echo $municipio; ?> e de <?php echo $municipio_parceiro; ?> da Regi�o de Sa�de <?php echo $regiao_saude; ?> estabelecem entre si o presente Termo de Parceria, com o intuito de garantir o compromisso assumido 
				        junto ao Minist�rio da Educa��o, no �mbito do Edital n� 3, de 22 de outubro de 2013, que trata da pr�-sele��o de munic�pios para implanta��o de curso de gradua��o em medicina por institui��o de educa��o superior privada.
				        </p>
					</td>
				</tr>	
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>	
				<tr>
					<td align="center">
						<p style="text-align: justify; font-size: 15px;">
	        			Para consecu��o do objeto estabelecido no referido Edital, o munic�pio <?php echo $municipio; ?> se compromete a colocar � disposi��o da Institui��o de Educa��o Superior privada vencedora de chamamento p�blico a ser realizado pelo Minist�rio da Educa��o, <?php echo $total_leitos; ?> 
	        			(quantidade) leitos, (50, no m�nimo), a fim de complementar o n�mero de leitos exigidos pelo Edital n� 3, de 22 de outubro de 2013, para oferta como cen�rio de ensino do curso de medicina a ser potencialmente implantado no munic�pio de <?php echo $municipio; ?>.
	        			</p>
					</td>
				</tr>	
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>					
				<tr>
					<td><p style="text-align: right;"><?php echo $municipio; ?> ,_____ de ______ 2013</p></td>
				</tr>
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>		
				<tr>
					<td><p style="text-align: center;">______________________________________________</p></td>
				</tr>	
				<tr>
					<td><p style="font-family: Arial, verdana; font-size: 11px; text-align: center;"><?php echo $m['prefeito'] ? $m['prefeito'] : 'N�o Informado';?> - Dirigente Municipal</p></td>
				</tr>	
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>		
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>	
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>					
				<tr>
					<td><p style="text-align: center;">______________________________________________</p></td>
				</tr>		
				<tr>
					<td><p style="font-family: Arial, verdana; font-size: 11px; text-align: center;"><?php echo $gestor_sus ? $gestor_sus : 'N�o Informado';?> - Gestor Local SUS</p></td>
				</tr>	
				<tr>
					<td><p style="text-align: right;">&nbsp;</p></td>
				</tr>										
			</table>
		</td>
	</tr>
</table>		
<br>
<table id="termo" width="95%" align="center" border="0" cellpadding="3" cellspacing="1">
	<tr>
		<td class="div_rolagem" style="text-align: center;">
			<input type="button" name="impressao" id="impressao" value="Imprimir" onclick="window.print();" />
			<input type="button" name="fechar" id="fechar" value="Fechar" onclick="window.close();" />
		</td>
	</tr>
</table>
</body>
</html>