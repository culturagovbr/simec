<?PHP

if( $_SESSION['par']['muncod'] == '' || $_SESSION['par']['prgid'] == '' ){
	
    echo "
        <script>
                alert('Sess�o expirada.');
                window.location.href = 'par.php?modulo=inicio&acao=C';
        </script>
    ";
    die();
}

include_once APPRAIZ . 'includes/cabecalho.inc';
include_once '_funcoes_maismedicos.php';

if($_REQUEST['requisicao'] == 'salvarInfor'){
	global $db; 
    $rqmid = salvarInfor($_POST);
    
    if($rqmid){
    	$_SESSION['par']['rqmid'] = $rqmid;
    	$_SESSION['abaMaisMedicosIM'] = 'S';
    	echo "<script>window.location.href = 'par.php?modulo=principal/programas/feirao_programas/maisMedicosInforMunicipio&acao=A';</script>";
    } else {
    	$_SESSION['abaMaisMedicosIM'] = 'N';
    	echo "<script>window.location.href = 'par.php?modulo=principal/programas/feirao_programas/maisMedicosCondicoesPart&acao=A';</script>";
    }
    exit();
}

$rqmid = verificarRQMID();

unset($_SESSION['par']['municipioliberado']);
$municipioLiberado = verificarMunicipioLiberado();

if($rqmid){
	$_SESSION['par']['rqmid'] = $rqmid;
}

if($municipioLiberado){
	$_SESSION['par']['municipioliberado'] = $municipioLiberado;
}

$populacao = verificarPopulacao();
$capital = verificarCapitalEstado();
$capital ? $capital = 't' : $capital = 'f';
$dadosm = verificarDadosMunicipios();

if($dadosm){
	extract($dadosm); 
}

#N�O POSSUI OFERTA DE CURSO DE MEDICINA EM SEU TERRIT�RIO. 
$oferta = naoPossuiOfertaCurso();

if( $_SESSION['par']['muncod'] != '2601201' ){
    
    if( ($populacao < 70000 || $capital == 't' || $oferta == 'S') ){
        $hab_maismedicos = 'N';
    } else {
        $hab_maismedicos = 'S';
    }
    
}else{
    $hab_maismedicos = 'S';
}

if(empty($_SESSION['par']['rqmid'])){
    unset($_SESSION['abaMaisMedicosIM']);
} else {
    $_SESSION['abaMaisMedicos'] = 'S';
    $_SESSION['abaMaisMedicosIM'] = 'S';
}

if(empty($_SESSION['par']['municipioliberado'])){
	unset($_SESSION['abaMaisMedicosManifestacao']);
} else {
	$_SESSION['abaMaisMedicosManifestacao'] = 'S';
}

echo '<br>';
echo carregaAbasProFuncionario('par.php?modulo=principal/programas/feirao_programas/maisMedicosCondicoesPart&acao=A');
monta_titulo($titulo_modulo, recuperaMunicipioEstado());
$_SESSION['maismedicos'] = true;

$arrayPerfil = pegaArrayPerfil($_SESSION['usucpf']); 
?>

<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript">
    function cancelar() {
        window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa';
    }
</script>

<table class="tabela" height="100%" align="center" cellspacing="0" cellpadding="3" >
    <tr class="subtituloEsquerda">
        <td height="10" width="45%">
            <input type="button" value="Voltar" onclick="window.location.href = '?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa';" id="btnVoltar" />
        </td>
    </tr>
</table>

<br>

<form id="formulario" method="post" name="formulario" action="par.php?modulo=principal/programas/feirao_programas/maisMedicosCondicoesPart&acao=A">
	<input type="hidden" id="requisicao" name="requisicao" value="salvarInfor"/>
	<input type="hidden" id="muncod" name="muncod" value="<?php echo $_SESSION['par']['muncod'];?>"/>
	<input type="hidden" id="prgid" name="prgid" value="<?php echo $_SESSION['par']['prgid']; ?>"/>		
	<input type="hidden" id="rqmquestao03" name="rqmquestao03" value="<?php echo $populacao; ?>"/>
	<input type="hidden" id="rqmquestao04" name="rqmquestao04" value="<?php echo $capital; ?>"/>
	<input type="hidden" id="rqmquestao05" name="rqmquestao05" value="<?php echo $oferta == 'S' ? 't': 'f'; ?>"/>
	<input type="hidden" id="rqmquestao06" name="rqmquestao06" value="<?php echo $leitos_sus; ?>"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" border="0">
	    <tr>
	        <td height="25"></td>
	    </tr>
	    <tr>
	        <td>
                <?php if($hab_maismedicos == 'N'){?>
                	<p style="margin-left: 38%; text-decoration: underline; font-size: 14px; font-weight: bold; color:#FF0000;"><b>MUNIC�PIO N�O ATENDE AS CONDI��ES DE PARTICIPA��O<b></p>
                <?php } ?>
	            <span style="margin-left: 3%; text-decoration: underline; font-size: 14px; font-weight: bold;">
	                CONDI��ES DE PARTICIPA��O
	            </span>
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                Na primeira etapa desta pr�-sele��o, ser�o analisadas a relev�ncia e a necessidade social da oferta de curso de medicina no munic�pio. O munic�pio dever� atender, obrigatoriamente, aos seguintes crit�rios:
	            </p>
	            <?php
                        if( $_SESSION['par']['muncod'] != '2601201' ){
                            if($populacao < 70000){ 
                    ?>
                                <p style="margin-left: 5%; font-size: 13px; color:#FF0000;"><b>a) ter 70 mil ou mais habitantes, conforme dados do Instituto Brasileiro de Geografia e Estat�stica (IBGE), Censo 2012.</b></p>    
                    <?      } else { ?>
                                <p style="margin-left: 5%; font-size: 13px;">a) ter 70 mil ou mais habitantes, conforme dados do Instituto Brasileiro de Geografia e Estat�stica (IBGE), Censo 2012.</p>
                    <?      } 
                        }else{
                    ?>
                                <p style="margin-left: 5%; font-size: 13px;" > a) ter 70 mil ou mais habitantes, conforme dados do Instituto Brasileiro de Geografia e Estat�stica (IBGE), Censo 2012.</p>
                    <?  } ?>
	            <?php if($capital =='t'){ ?>
                    <p style="margin-left: 5%; font-size: 13px; color:#FF0000;"><b>b) n�o se constituir capital do Estado.</b></p>
	            <?php } else { ?>
                    <p style="margin-left: 5%; font-size: 13px;">b) n�o se constituir capital do Estado.</p>
	            <?php } ?>
	            <?php if($oferta == 'S'){ ?>
                    <p style="margin-left: 5%; font-size: 13px; color:#FF0000;"><b> c) n�o possuir oferta de curso de medicina em seu territ�rio. </b></p>
                <?php } else { ?>
                    <p style="margin-left: 5%; font-size: 13px;">c) n�o possuir oferta de curso de medicina em seu territ�rio.</p>
                <?php } ?>
	            <p style="margin-left: 5%; font-size: 13px;">
	                3.2 Os munic�pios que n�o atenderem ao disposto nos itens 3.1 e 3.2 ser�o considerados eliminados da presente pr�-sele��o.
	            </p>
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                Na segunda etapa desta pr�-sele��o, ser�o analisados a estrutura de equipamentos p�blicos e os programas de sa�de existentes no munic�pio, segundo dados do Minist�rio da Sa�de. O munic�pio dever� atender, obrigatoriamente, aos seguintes crit�rios:
	            </p>
	            
	            <p style="margin-left: 5%; font-size: 13px;">
	                a) n�mero de leitos dispon�veis SUS por aluno maior ou igual a 5 (cinco), ou seja, para um curso com 50 vagas, o munic�pio dever� possuir, no m�nimo, 250 leitos dispon�veis SUS.
	            <p style="margin-left: 5%; font-size: 13px;">
	                b) n�mero de alunos por equipe de aten��o b�sica menor ou igual a 3 (tr�s), considerando o m�nimo de 17 (dezessete) equipes;
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                c) exist�ncia de leitos de urg�ncia e emerg�ncia ou Pronto Socorro;                
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                d) exist�ncia de, pelo menos 3 (tr�s), Programas de Resid�ncia M�dica nas especialidades priorit�rias: (1) Cl�nica M�dica; (2) Cirurgia; (3)Ginecologia-Obstetr�cia; (4) Pediatria e (5) Medicina de Fam�lia e Comunidade;
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                e) ades�o pelo munic�pio ao Programa Nacional de Melhoria do Acesso e da Qualidade na Aten��o B�sica - PMAQ, do Minist�rio da Sa�de;                
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                f) exist�ncia de Centro de Aten��o Psicossocial - CAPS;
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                g) hospital de ensino ou unidade hospitalar com potencial para hospital de ensino, conforme legisla��o de reg�ncia; e
	            </p>
	            <p style="margin-left: 5%; font-size: 13px;">
	                h) exist�ncia de hospital com mais de 100 (cem) leitos exclusivos para o curso.
	            </p>
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                Os munic�pios que n�o atenderem ao disposto no item 3.4, de acordo com os dados apresentados pelo pr�prio munic�pio e validados pela SERES - em conformidade com os dados do Minist�rio da Sa�de, ressalvadas as hip�teses dos itens 4.1 a 4.3, ser�o considerados eliminados da presente pr�-sele��o.
	            </p>
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                A terceira etapa da pr�-sele��o consistir� na an�lise de projeto de melhoria da estrutura de equipamentos p�blicos e programas de sa�de existentes no munic�pio.
	            </p>
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                Para a realiza��o da terceira etapa desta pr�-sele��o, a SERES, a seu crit�rio, poder� designar equipes de especialistas para an�lise de projeto de melhoria da estrutura de equipamentos p�blicos e programas de sa�de no munic�pio, assim como para realiza��o de avalia��o in loco.
	            </p>
	            <br>
	        </td>
	    </tr>
	    <tr>
	        <td align="center" bgcolor="#CCCCCC" colspan="2">
                <?php if( $hab_maismedicos == 'S' ){ ?>
                <input type="submit" value="Continuar" id="btnContinuar"/>
                <input type="button" value="Cancelar" id="btnCancelar" onclick="cancelar();"/>
                <?php } else { ?>
                <input type="button" value="OK" id="btnCancelar" onclick="cancelar();"/>
                <?php } ?>
	        </td>
	    </tr>						
	</table>
</form>


<script type="text/javascript" language="javascript">
	<?php if($_SESSION['par']['rqmid']){ ?>
		jQuery('#btnContinuar').attr('disabled', 'disabled');
		jQuery('#btnCancelar').attr('disabled', 'disabled');
	<?php } ?>
	<?php if(in_array(PAR_PERFIL_PREFEITO, $arrayPerfil)){ ?>
		jQuery("#btnContinuar").attr('disabled','disabled');
	<?php } ?>	
</script>
