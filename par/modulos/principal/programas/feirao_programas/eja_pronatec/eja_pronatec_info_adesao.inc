<?PHP
    include_once '_funcoes_eja_pronatec.php';

    if($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
    }

    if( !$_SESSION['par']['prgid'] ){
        $db->sucesso( 'principal/planoTrabalho&acao=A&tipoDiagnostico=programa', '', 'Ocorreu um ploblema. Selecione o programa novamente!' );
        die();
    }

    include APPRAIZ . "includes/cabecalho.inc";
    echo '<br>';

    #PEGA, SELECIONA O MUNIC�PIO OU ESTADO SELECIONADO OU LOGADO PELO USU�RIO. � USADO PARA CRIAR O TITULO!.
    $sql = " SELECT prgdsc FROM par.programa WHERE prgid = {$_SESSION['par']['prgid']} ";
    $nomePrograma = $db->pegaUm($sql);

    if($_SESSION['par']['muncod']){
        $sql = "SELECT mundescricao, estuf FROM territorios.municipio WHERE muncod = '{$_SESSION['par']['muncod']}'";
        $rsMunicipio = $db->pegaLinha($sql);
        $descricao = $rsMunicipio['mundescricao'].' - '.$rsMunicipio['estuf'];

        $munBloqueado = bucarMunicipioBloqueado();

    }else{
        $sql = "SELECT estdescricao, estuf FROM territorios.estado WHERE estuf = '{$_SESSION['par']['estuf']}'";
        $rsEstado = $db->pegaLinha($sql);
        $descricao = $rsEstado['estdescricao'].' - '.$rsEstado['estuf'];
    }

    monta_titulo($nomePrograma, $descricao);
    echo '<br/>';

    $aderiu = verificaAdesaoEJA();

    #VERIFICA SE O PROGRAMA ESTA NA DATA HABIL PARA ADES�O.
    $prog_habil = programaDataHabil( $_SESSION['par']['prgid'] );

    if($prog_habil == 'S'){
        $habilita = 'onclick="continuaAdesao()"';
    }else{
        $habilita = 'disabled="disabled"';
    }

    if($munBloqueado == 'S'){
        $habilita = 'disabled="disabled"';
    }else{
        if( $aderiu == 'S'){
            $habilita = 'disabled="disabled"';
        }else{
            $habilita = 'onclick="continuaAdesao()"';
        }

    }
    echo carregaAbasProFuncionario('par.php?modulo=principal/programas/feirao_programas/eja_pronatec/eja_pronatec_info_adesao&acao=A');

?>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<link href="../includes/jquery-autocomplete/jquery.autocomplete.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script	type="text/javascript" src="../includes/jquery-autocomplete/jquery.autocomplete.js"></script>

<script type="text/javascript">

    function continuaAdesao(){
        $('#requisicao').val('continuaAdesaoPronatec');
        $('#formulario').submit();
    }
</script>


<form action="" method="POST" id="formulario" name="formulario">

    <input type="hidden" id="requisicao" name="requisicao" value=""/>

    <center>
        <table bgcolor="#f5f5f5" align="center" class="tabela listagem" >
            <tr>
                <td width="90%" align="center">
                    <br>
                    <fieldset style="width: 70%;">
                        <legend style="font-family: sans-serif; font-size: 14px;"> ORIENTA��ES </legend>
                                <? if($munBloqueado == 'S'){ ?>
                                        <p style="text-indent: 5%; text-align: justify; font-family: sans-serif; font-size: 14px; line-height:200%;">
                                            <span style="font-weight: bold;">
                                                ESTE MUNIC�PIO N�O PODER� REALIZAR ESTA ADES�O POR N�O ATENDER AOS REQUISITOS ESTABELECIDOS NA PORTARIA N� 125 DE 13 DE FEVEREIRO DE 2014. ESSES CRIT�RIOS S�O:
                                            </span>
                                        </p>
                                        <br>
                                        <p style="text-indent: 0%; text-align: justify; font-family: sans-serif; font-size: 14px; line-height:200%;">
                                            Art. 4� - Est�o aptos a serem unidades demandantes da SECADI para oferta de cursos de EJA articulada � Educa��o Profissional, no �mbito do PRONATEC:
                                            <br>
                                            I - Os estados e o Distrito Federal;
                                            <br>
                                            II - Os munic�pios que atendam pelo menos um dos seguintes crit�rios:
                                            <br>
                                            a) ter aderido a Resolu��o FNDE/CD n� 48, no ano de 2012 ou no ano de 2013 e ter solicitado matr�culas de "EJA integrada � qualifica��o profissional";
                                            <br>
                                            b) ser Polo da Educa��o Inclusiva, Direito � Diversidade;
                                            <br>
                                            c) ser integrante do G100: munic�pios populosos, com baixa receita per capita e alta vulnerabilidade socioecon�mica;
                                            <br>
                                            d) integrar o Plano Juventude Viva;
                                            <br>
                                            e) estar entre os 20 munic�pios com o maior n�mero de Escolas do Campo, de acordo com o Censo do INEP, por unidade da federa��o;
                                            <br>
                                            f) ter comunidades remanescentes de quilombos certificadas ou tituladas pela Funda��o Palmares; ou
                                            <br>
                                            g) ser capital ou ter mais de 200 mil habitantes.
                                        </p>

                                <? }else{ ?>

                                        <img width=70px" src="../imagens/brasao.gif" style="margin-bottom: 15px;">

                                        <p style="font-size: 14px; font-weight: bold; margin-bottom: 10px;">
                                            MINIST�RIO DA EDUCA��O SECRETARIA DE EDUCA��O CONTINUADA, ALFABETIZA��O, DIVERSIDADE E INCLUS�O
                                        </p>
                                        <p style="font-size: 14px; font-weight: bold;">
                                            DIRETORIA DE POL�TICAS DE ALFABETIZA��O E EDUCA��O DE JOVENS E ADULTOS
                                        </p>
                                        <br>
                                        <p style="font-size: 14px; font-weight: bold; text-decoration: underline; margin-bottom: 25px;">
                                            COMUNICADO
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; margin-bottom: 10px;">

                                            PORTARIA N� 125 DE 13 DE FEVEREIRO DE 2014 - Disp�e sobre a oferta de Educa��o de Jovens e Adultos - EJA com Qualifica��o Profissional no �mbito do

                                            Programa Nacional de Acesso ao Ensino T�cnico e Emprego - PRONATEC pelos Munic�pios, de que trata a Lei n� 12.513 de 26 de outubro de 2011

                                            <a href="http://pronatec.mec.gov.br/institucional-90037/base-legal" target="_blank"> pronatec.mec.gov.br/institucional-90037/base-legal </a> e da Resolu��o CD/FNDE n� 48 de 11 de dezembro de 2013

                                            <a href="http://www.fnde.gov.br/fnde/legislacao/resolucoes" target="_blank"> www.fnde.gov.br/fnde/legislacao/resolucoes</a>. Leia a �ntegra da Portaria.

                                            <a href="http://pesquisa.in.gov.br/imprensa/servlet/INPDFViewer?jornal=1&pagina=15&data=14/02/2014&captchafield=firistAccess" target="_blank"> CLIQUE AQUI </a>

                                        </p>

                                        <!--
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            1 - Acessar o SIMEC por meio do seguinte endere�o eletr�nico: http://simec.mec.gov.br ;
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            2 - Informe o CPF e a Senha do(a) Prefeito(a) ou do(a) Secret�rio(a) Estadual de Educa��o;
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            3 - Se for Unidade da Federa��o, aparecer� a seguinte tela:
                                        </p>

                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/1_tela.png">
                                        <br>
                                        <br>

                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            4 - Clicar na op��o "Programa", indicado dentro do c�rculo vermelho; Aparecer� a tela abaixo. Se for munic�pio, a tela acima n�o aparecer�, o programa ir� diretamente para a tela abaixo;
                                        </p>

                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/2_tela.png">
                                        <br>
                                        <br>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            5 - Clique no banner marcado "EJA PRONATEC";
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            6 - A tela a seguir ir� aparecer;
                                        </p>
                                         -->
                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/3_tela.png">
                                        <br>
                                        <br>

                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            1 - Ap�s a leitura do comunicado, clique em "Continuar Ades�o", caso queira realizar a ades�o, ou caso contr�rio em "Sair";
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            2 - Se a op��o for por "Continuar Ades�o", a tela abaixo ser� apresentada. Nela estar� contida a pr�via do Termo de Ades�o que em outro momento poder� ser impresso, assinado pelo Prefeito ou Secret�rio Estadual de Educa��o, com firma reconhecida para ser encaminhado ao Minist�rio da Educa��o no endere�o indicado no pr�prio Termo;
                                        </p>

                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/4_tela.png">
                                        <br>
                                        <br>

                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            3 - Se a op��o for por "Iniciar Processo", a tela abaixo ser� aberta. Se a op��o for por "Cancelar Processo", a Ades�o ser� inicializada. Se a op��o for "Sair", o Sistema encaminhar� o usu�rio para a p�gina principal do SIMEC;
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            4 - Ao "Iniciar Processo", aparecer� a seguinte tela:
                                        </p>

                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/5_tela.png">
                                        <br>
                                        <br>

                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            5 - Aparecer� a seguinte tela:
                                        </p>

                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/6_tela.png">
                                        <br>
                                        <br>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            6 - Informar todos os campos e Salvar, quando quiser continuar a preencher os outros formul�rios ou Cancelar;
                                        </p>
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            7 - Preencher o formul�rio, abaixo, com os dados do "Supervisor de Demandas". O "Supervisor de Demandas" dever� ser, obrigatoriamente, servidor(a) da Secretaria Municipal de Educa��o ou Servidor(a) da Secretaria Estadual de Educa��o, indicados pelo(a) Prefeito(a) ou Secret�rio(a) Estadual de Educa��o, respectivamente. O ?Supervisor de Demandas? ser� o ente de liga��o entre o munic�pio ou o estado com o Minist�rio da Educa��o. No campo "Data In�cio", dever� ser informada a data prevista para iniciar o(s) curso(s). Salvar quando for continuar a preencher os demais formul�rios ou Cancelar;
                                        </p>
                                        
                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/7_tela.png">
                                        <br>
                                        <br>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            8 - Na tela abaixo, preencher os campos quando houver p�blico para as respectivas modalidades. Se n�o houver p�blico em alguma modalidade, informar n�mero 0 (zero). Salvar quando for preencher outros formul�rios ou Cancelar;
                                        </p>
                                        
                                        <br>
                                            <img width=860px" src="../imagens/eja_pronatec/8_tela.png">
                                        <br>
                                        <br>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            9 - No formul�rio a seguir, anexar o Projeto Pol�tico Pedag�gico Integrado (PPPintegrado). Sugerimos que o PPPintegrado seja redigido no Microsoft Word e salvado no formato PDF; 
                                        </p>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            10 - No mesmo formul�rio, ap�s o preenchimento dos campos, ir em "estado atual" no lado direito da tela e tramitar o processo de ades�o ao Minist�rio da Educa��o para an�lise;
                                        </p>
                                        
                                        <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                            11 - Ap�s a an�lise realizada pelo Minist�rio da Educa��o, imprimir o Termo de Ades�o, colher a assinatura do(a) Prefeito(a), autenticar o documento e encaminha-lo ao MEC.    
                                        </p>
                                        
                                <? } ?>
                    </fieldset>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="float: center; width: 70%; ">
                        <input type="button" name="continua" value="Continuar Ades�o" <?=$habilita;?>/>
                        <!-- <input type="button" name="continua" value="Continuar Ades�o" onclick="continuaAdesao();"/> -->

                        <input type="button" name="cancelar" value="Sair" onclick="window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa'" />
                    </div>
                </td>
            </tr>
        </table>
    </center>
</form>



