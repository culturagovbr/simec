<?PHP
    include_once '_funcoes_maismedicos_2015.php';

    if($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
    }

    #� USADO APENAS QUANDO ESSA PAGINA � CHAMADA PELO RELAT�RIO MAIS M�DICO.
    if( $_REQUEST['rel_muncod'] != '' && $_REQUEST['rel_prgid'] != '' ){
        $_SESSION['par']['prgid']   = $_REQUEST['rel_prgid'];
        $_SESSION['par']['muncod']  = $_REQUEST['rel_muncod'];

        if( $_SESSION['par']['muncod'] != '' ){
            $inuid = $db->pegaUm("SELECT inuid FROM par.instrumentounidade WHERE muncod = '{$_SESSION['par']['muncod']}'");
            $_SESSION['par']['adpid'] = $db->pegaUm("select max(adpid) from par.pfadesaoprograma where pfaid = 27 and inuid = {$inuid}");
        }
    }

    if( !$_SESSION['par']['prgid'] || $_SESSION['par']['muncod'] == '' ){
        $db->sucesso( 'principal/planoTrabalho&acao=A&tipoDiagnostico=programa', '', 'Ocorreu um ploblema. Selecione o programa novamente!' );
        die();
    }

    include APPRAIZ . "includes/cabecalho.inc";
    echo '<br>';
    monta_titulo("Mais M�dicos - Edital n� 1/2015", $titulo_modulo.' <br> '.recuperaMunicipioEstado());
    echo '<br>';

    $rqmid = verificarRQMID();
    if( $rqmid != '' ){
        $_SESSION['par']['rqmid'] = $rqmid;
        $resp = verificaInformacaoMunicipio( $rqmid );
    }

    if( $resp['rqmaceitetermoresidencia'] == 't'){
        $habilita = 'disabled="disabled"';
    }else{
        $habilita = 'onclick="continuaAdesao()"';
    }

    echo carregaAbasProFuncionario('par.php?modulo=principal/programas/feirao_programas/mais_medicos_2015/condicoes_participacao&acao=A');

    $populacao  = verificarPopulacao();
    $capital    = verificarCapitalEstado();
    $oferta     = possuiOfertaCurso();
    $infoMun    = informacaoMunicipios_2015();

    $aderiu = verificaAdesaoMaisMedicoNovo();

    $habilitado = verificaDataFechamento();

?>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<link href="../includes/jquery-autocomplete/jquery.autocomplete.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script	type="text/javascript" src="../includes/jquery-autocomplete/jquery.autocomplete.js"></script>

<script type="text/javascript">

    function continuaAdesao(){
        $('#continua').attr('disabled', true);
        $('#requisicao').val('continuaAdesao');
        $('#formulario').submit();
    }
</script>


<form action="" method="POST" id="formulario" name="formulario">

    <input type="hidden" id="requisicao" name="requisicao" value=""/>

    <input type="hidden" id="muncod" name="muncod" value="<?=$_SESSION['par']['muncod'];?>"/>
    <input type="hidden" id="prgid" name="prgid" value="<?=$_SESSION['par']['prgid'];?>"/>
    <input type="hidden" id="rqmquestao03" name="rqmquestao03" value="<?=$infoMun['ifmpopulacao'];?>"/>
    <input type="hidden" id="rqmquestao04" name="rqmquestao04" value="<?=$capital;?>"/>
    <input type="hidden" id="rqmquestao05" name="rqmquestao05" value="<?=$oferta;?>"/>
    <input type="hidden" id="rqmquestao06" name="rqmquestao06" value="<?=$infoMun['ifmhospleito'];?>"/>

    <center>
        <table bgcolor="#f5f5f5" align="center" class="tabela" >
            <tr>
                <td width="90%" align="center">
                    <br>
                    <fieldset style="width: 70%;">
                        <legend style="font-family: sans-serif; font-size: 14px;"> CONDI��ES DE PARTICIPA��O </legend>
                            <p style="text-indent: 5%; text-align: justify; font-family: sans-serif; font-size: 14px; line-height:200%;">

                                <? if($aderiu == 'N'){ ?>
                                        <span style="font-weight: bold;">
                                            Observa��o: "Este munic�pio n�o aceitou a ades�o ao Programa Mais M�dico - Edital n� 1/2015. Dessa forma n�o � possiv�l dar continuidade ao porcesso".
                                        </span>
                                        <br><br>
                                <? } ?>
                            </p>
                            <img width=70px" src="../imagens/brasao.gif">
                            <br><br>

                            <p style="font-size: 14px; font-weight: bold;">
                                MINIST�RIO DA EDUCA��O
                            </p>
                            <p style="font-size: 13px; font-weight: bold; text-align: left;">
                                COMUNICADO
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                Os munic�pios chamados a aderir a este Edital foram pr�-selecionados de acordo com os crit�rios de relev�ncia e necessidade social da oferta de curso de medicina, ouvido o Minist�rio da Sa�de - MS.
<!--                            </p>
                            <p style="font-size: 12px; text-align: justify;">-->
                                Em obedi�ncia ao art. 1�, inciso I, da Lei n� 12.871, de 2013, e visando corrigir assimetrias regionais concernentes � propor��o de m�dicos por habitantes, o perfil dos munic�pios pr�-selecionados prev� o atendimento cumulativo aos seguintes crit�rios:
                            </p>
<!--                            <p style="font-size: 12px; text-align: justify;">
                                A resolu��o em quest�o tem o objetivo de transfer�ncia direta de recursos financeiros aos estados, munic�pios e Distrito Federal para a manuten��o e desenvolvimento de <b>novas turmas de Educa��o de Jovens e Adultos</b> oferecidas pelas redes p�blicas de ensino, <b>na modalidade presencial</b>, cujas matr�culas ainda n�o tenham sido contempladas com recursos do Fundo de Manuten��o e Desenvolvimento da Educa��o B�sica e de Valoriza��o dos Profissionais da Educa��o (FUNDEB).
                            </p>-->
                            <p style="font-size: 12px; font-weight: bold; text-align: justify;">
                                I - n�o se constituem como capital de Estado;
                            </p>
                            <p style="font-size: 13px; font-weight: bold; text-align: left;">
                                II - n�o possuem oferta de curso de medicina em seu territ�rio ou na regi�o de sa�de;
                            </p>
                            <p style="font-size: 12px; font-weight: bold; text-align: justify;">
                                III - possuem mais de 50.000 habitantes, de acordo com os dados do Instituto Brasileiro de Geografia e Estat�stica, estimativas do IBGE 2014;
                            </p>
                            <p style="font-size: 12px; font-weight: bold; text-align: justify;">
                                IV - est�o localizados em Unidades da Federa��o - UFs que, conforme tabela constante do Anexo I:
                            </p>
                            <p style="font-size: 13px; text-align: justify;">
                                a) possuem rela��o vaga em curso de medicina por dez mil habitantes inferior a 1,34, considerando os cursos de medicina previstos no plano de expans�o p�blica do Programa Mais M�dicos ou de munic�pios constantes dos Anexos I e II da Portaria SERES n� 543, de 4 de setembro de 2014 e de acordo com dados da Secretaria de Gest�o do Trabalho e Educa��o em Sa�de - SGTES, do MS.
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                b) possuem rela��o m�dicos por mil habitantes inferior a 2,7, de acordo com dados da Secretaria de Gest�o do Trabalho e Educa��o em Sa�de - SGTES, do MS.
                            </p>
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                V - est�o distantes, pelo menos, setenta e cinco quil�metros de local de curso de medicina pr�-existente; de cursos de medicina previstos no plano de expans�o p�blica do Programa Mais M�dicos ou de munic�pios constantes dos Anexos I e II da Portaria SERES n� 543, de 4 de setembro de 2014.
                            </p>
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                VI - est�o localizados em regi�o de sa�de que apresenta estrutura de equipamentos p�blicos, cen�rios de aten��o na rede e programas de sa�de adequados para comportar a oferta de curso de gradua��o em medicina, conforme os crit�rios previstos no art. 3� deste Edital.
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                Na verifica��o in <b>loco</b>, ser� observado se o munic�pio pr�-selecionado atende aos seguintes crit�rios:
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                a) n�mero de leitos do SUS dispon�veis por aluno, em quantidade igual ou maior a cinco, tendo em vista a abertura de turmas com, no m�nimo, cinquenta alunos;
                                <br>
                                b) n�mero de alunos por Equipe de Aten��o B�sica - EAB menor ou igual a tr�s, tendo em vista a abertura de turmas, com, no m�nimo, cinquenta alunos;
                                <br>
                                c) exist�ncia de leitos de urg�ncia e emerg�ncia ou Pronto Socorro;
                                <br>
                                d) exist�ncia de, pelo menos, tr�s Programas de Resid�ncia M�dica nas especialidades priorit�rias, preferencialmente em Medicina Geral de Fam�lia e Comunidade;
                                <br>
                                e) ades�o pelo munic�pio ao Programa Nacional de Melhoria do Acesso e da Qualidade na Aten��o B�sica - PMAQ, do MS;
                                <br>
                                f) exist�ncia de Centro de Aten��o Psicossocial - CAPS; e
                                <br>
                                g) exist�ncia de hospital de ensino ou unidade hospitalar com mais de 80 (oitenta) leitos, com potencial para hospital de ensino, conforme legisla��o de reg�ncia.
                            </p>
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                A ades�o expressa ao edital e a assun��o dos compromissos estabelecidos no Termo s�o indispens�veis para a sele��o do munic�pio para implementa��o do curso de gradua��o em Medicina no �mbito do Programa Mais M�dicos.
                            </p>
                    </fieldset>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="float: center; width: 70%; ">
                        <?PHP
                            if( $habilitado == 'S' ){
                        ?>
                            <input type="button" name="continua" id="continua" value="Continuar Ades�o" <?=$habilita;?>>
                        <?PHP
                            }else{
                        ?>
                            <input type="button" name="continua_dis" id="continua_dis" value="Continuar Ades�o" disabled="disabled">
                        <?PHP
                            }
                        ?>
                        <input type="button" name="cancelar" id="cancelar" value="Sair" onclick="window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa'" />
                    </div>
                </td>
            </tr>
        </table>
    </center>
</form>



