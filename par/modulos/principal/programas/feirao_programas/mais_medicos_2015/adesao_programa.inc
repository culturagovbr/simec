<?php
include_once '_funcoes_maismedicos_2015.php';

if($_REQUEST['requisicao'] == 'salvarTermo'){
    salvarAdesao($_POST);
    exit();
}
$m          = recuperaDadosPrefeitura();
$gestor_sus = recuperaGestorSusMunicipio();

include_once APPRAIZ . 'includes/cabecalho.inc';
echo '<br>';
monta_titulo("Mais M�dicos - Edital n� 1/2015", $titulo_modulo.' <br> '.recuperaMunicipioEstado());

echo carregaAbasProFuncionario('par.php?modulo=principal/programas/feirao_programas/mais_medicos_2015/adesao_programa&acao=A');

$aderiu = verificarAdesao();
$arrayPerfil = pegaArrayPerfil($_SESSION['usucpf']);

$habilitado = verificaDataFechamento();
?>

<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript">
    function salvarTermo(aceite){
        if(aceite == 'S'){
            jQuery("#adpresposta").val('S');
        } else {
            jQuery("#adpresposta").val('N');

            if( !confirm('Voc� clicou em "N�o aceito.". Caso confirme esta op��o o munic�pio n�o poder� participar do Edital n� 1/2015. Deseja confirmar?') ){
                return false;
            }
        }

        $('#requisicao').val('salvarTermo');
        $('#form').submit();
    }

    function sair(){
        window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa';
    }

    function imprimirAdesao(){
        return window.open('par.php?modulo=principal/programas/feirao_programas/mais_medicos_2015/adesao_programa_imp&acao=A', 'modelo', "height=600,width=950,scrollbars=yes,top=50,left=200");
    }
</script>

<div id="div_termo">
    <form id="form" name="form" method="POST">
	<input type="hidden" id="requisicao" name="requisicao" value=""/>
	<input id="adpresposta" type="hidden" name="adpresposta" value=""/>
	<input id="tapid" type="hidden" name="tapid" value="28"/>

	<table bgcolor="#f5f5f5" align="center" class="tabela">
            <tr>
                <td width="90%" align="center">
                    <?PHP
                        if( $aderiu != 'S' || $aderiu == ''){
                    ?>
                            <div style="overflow: auto; height:350px; width: 70%; background-color: rgb(250,250,250); border-width:1px; border-style:solid;" align="center" >
                    <?PHP
                        }else{
                    ?>
                            <fieldset style="width: 97%; background: #fff; margin-bottom:35px;">
                            <legend><a id="link_termo_adesao" href="javascript:void(0)"> Ades�o ao Programa </a></legend>
                            <div id="div_termo_adesao" class="div_termo_adesao" style="overflow: auto; margin-bottom: 15px; display: block;">

                            <table bgcolor="#ffffff" align="center">
                                <tr>
                                    <td width="90%" align="center">
                    <?PHP
                        }
                    ?>
                            <!-- Termo -->
                            <div style="width: 95%; magin-top: 10px;">
                                <br>
                                <p align="center" style="margin-bottom: 0px;"><font face="Arial, sans-serif" size="3"><b>Termo de Ades�o ao Programa Mais M�dicos</b></font></p><br>

                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        TERMO DE ADES�O QUE ENTRE SI CELEBRAM A SECRETARIA DE REGULA��O E SUPERVIS�O DA EDUCA��O SUPERIOR DO MINIST�RIO DA EDUCA��O E O MUNIC�PIO DE <?php echo strtoupper($m['municipio']);?>, NO ESTADO DO <?php echo strtoupper($m['estado']);?>,
                                        PARA DISPONIBILIZA��O DA ESTRUTURA DE EQUIPAMENTOS P�BLICOS, CEN�RIOS DE ATEN��O NA REDE E PROGRAMAS DE SA�DE NECESS�RIOS � AUTORIZA��O DE FUNCIONAMENTO DE CURSOS DE MEDICINA.
                                    </font>
                                </p>

                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        O MINIST�RIO DA EDUCA��O, CNPJ n� 00.394.445/0001-01, neste ato representado por MARTA WENDEL ABRAMO, Secret�ria de Regula��o e Supervis�o da Educa��o Superior, com endere�o na
                                        Esplanada dos Minist�rios, Bloco "L", 1� andar, sala 100 - CEP 70.047-900, Bras�lia (DF), e o MUNIC�PIO DE <?php echo strtoupper($m['municipio'])."-".strtoupper($m['estuf']);?>,
                                        <?php echo $m['endereco'];?>, neste ato representado por <?php echo $m['prefeito'];?> (qualifica��o), nos termos da Lei n� 12.871, de 22 de outubro de 2013, resolvem celebrar o
                                        presente Termo de Ades�o para implanta��o e funcionamento de cursos de medicina, por institui��o de educa��o superior privada, mediante as cl�usulas e condi��es seguintes:
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        1. CL�USULA PRIMEIRA - DO OBJETO
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        1.1. O presente termo tem por objeto a ades�o do Munic�pio de <?php echo $m['municipio']."-".$m['estuf'];?> ao Chamamento P�blico previsto no Edital n� 1, de 1� de abril de 2015 e
                                        defini��o de obriga��es e responsabilidades do Munic�pio no oferecimento de estrutura de equipamentos p�blicos, cen�rios de aten��o na rede e programas de sa�de necess�rios �
                                        autoriza��o de funcionamento de curso de gradua��o em medicina a ser ofertado por institui��o de educa��o superior privada nos termos do art. 3�, inciso IV, da Lei n� 12.871/2013.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        2. CL�USULA SEGUNDA - DAS OBRIGA��ES DO MUNIC�PIO
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        2.1. Para consecu��o do objeto estabelecido neste Termo de Ades�o, o Munic�pio dever� disponibilizar a estrutura de equipamentos p�blicos, cen�rios de aten��o na rede e programas
                                        de sa�de existentes no munic�pio e na regi�o de sa�de, previstos no artigo 4�, da Portaria Normativa n� 5, de 1� de abril de 2015 e no item 3.2.2 do Edital n� 1/2015, al�m de
                                        outros que podem ser estabelecidos pela SERES:
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        a) n�mero de leitos do SUS por aluno, maior ou igual a 5 (cinco);
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        b) exist�ncia de Equipes Multiprofissionais de Aten��o Domiciliar - EMAD;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        c) n�mero de alunos por Equipe de Aten��o B�sica (EAB) menor ou igual a 3 (tr�s);
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        d) exist�ncia de leitos de urg�ncia e emerg�ncia ou Pronto Socorro;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        e) grau de comprometimento dos leitos SUS para utiliza��o acad�mica;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        f) exist�ncia de, pelo menos, 03 (tr�s) Programas de Resid�ncia M�dica nas especialidades priorit�rias, conforme legisla��o de reg�ncia;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        g) ades�o pelo munic�pio ao Programa Nacional de Melhoria do Acesso e da Qualidade na Aten��o B�sica - PMAQ;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        h) exist�ncia de Centro de Aten��o Psicossocial - CAPS; e
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        i) hospital de ensino ou unidade hospitalar com mais de 80 (oitenta) leitos, com potencial para hospital de ensino, conforme legisla��o de reg�ncia.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        3. CL�USULA TERCEIRA - DAS OBRIGA��ES DA SECRETARIA DE REGULA��O E SUPERVIS�O DO MINIST�RIO DA EDUCA��O
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">3.1. Constituem obriga��es da SERES:</font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        a) selecionar os Munic�pios para implanta��o e funcionamento de cursos de medicina, por institui��es de educa��o superior privadas;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        b) celebrar Termo de Ades�o com o munic�pio selecionado, acompanhar e monitorar a implanta��o do curso de medicina naquela localidade;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        c) selecionar as institui��es de educa��o superior privadas para oferta de cursos de gradua��o em medicina nos munic�pios selecionados;
                                    </font>
                                </p>
                                <p align="justify" style="margin-left: 0.98in;">
                                    <font size="3" style="font-size: 11pt">
                                        d) editar normas complementares necess�rias ao cumprimento do estabelecido neste Termo de Ades�o.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        4. CL�USULA QUARTA - DA VIG�NCIA
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        4.1. Os compromissos assumidos pelo Munic�pio no presente Termo de Ades�o est�o vinculados � regularidade da oferta do curso pela institui��o de educa��o superior.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        5. CL�USULA QUINTA - DA RESCIS�O
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        5.1. No caso de rescis�o do presente Termo, cumpre ao Munic�pio informar � institui��o de educa��o superior privada ofertante do curso e � Secretaria de Regula��o
                                        e Supervis�o da Educa��o Superior, com anteced�ncia m�nima de 90 (noventa) dias, a fim de preservar a continuidade da oferta do curso.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        6. CL�USULA S�TIMA - DA PUBLICA��O
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        6.1. O presente Termo de Ades�o estar� dispon�vel no Sistema Integrado de Monitoramento e Controle <a href="http://simec.mec.gov.br">http://simec.mec.gov.br</a>.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        7. CL�USULA OITAVA - DAS ALTERA��ES
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        7.1. As eventuais altera��es do presente Termo de Ades�o ser�o realizadas por meio de termo aditivo acordado entre os part�cipes.
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        8. CL�USULA NONA - DA SOLU��O DE CONTROV�RSIAS
                                    </font>
                                </p>
                                <p align="justify" style="text-indent: 0.59in;">
                                    <font size="3">
                                        8.1. Eventual controv�rsia surgida durante a execu��o do presente Termo de Ades�o poder� ser dirimida administrativamente entre os part�cipes ou,
                                        em seguida, perante a C�mara de Concilia��o e Arbitragem da Administra��o Federal da Advocacia - Geral da Uni�o e, se invi�vel, posteriormente
                                        perante o foro da Justi�a Federal - Se��o Judici�ria do Distrito Federal.
                                    </font>
                                </p>
                                <br><br>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">Bras�lia-DF, ___ de ____________ de 2015.</font>
                                </p>
                                <br><br>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">____________________________________</font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">
                                        <b>MARTA WENDEL ABRAMO</b>
                                    </font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">SECRET�RIA DE REGULA��O E SUPERVIS�O DA EDUCA��O SUPERIOR</font>
                                </p>
                                <br><br>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">_____________________________________________________</font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3"><b><?php echo $m['prefeito']; ?></b></font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">PREFEITO MUNICIPAL DE <?php echo strtoupper($m['municipio'])."-".strtoupper($m['estuf']);?></font>
                                </p>
                                <br><br>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">_____________________________________________________</font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3"><b><?php echo $gestor_sus ? $gestor_sus : 'N�o Informado'; ?></b></font>
                                </p>
                                <p align="right" style="margin-right: 0.98in;">
                                    <font size="3">GESTOR LOCAL DO SISTEMA �NICO DE SA�DE</font>
                                </p>
                            </div>
                        <!-- FECHAR -->
                        <?PHP
                            if( $aderiu == 'S' ){
                        ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                    <?PHP
                        }else{
                    ?>
                            </div>
                    <?PHP
                        }
                    ?>
                </td>
            </tr>
        </table>

        <table bgcolor="#f5f5f5" align="center" class="tabela" >
            <tr>
                <?PHP
                    if( $aderiu == 'N' || $aderiu == '' ){
                ?>
                    <td align="center">
                        <div style="float: center; width: 70%;">
                             <?PHP
                                if( $habilitado == 'S' ){
                            ?>
                                    <input id="aceito" type="button" name="aceito" value="Aceito" onclick="salvarTermo('S')">
                                    <input id="naceito" type="button" name="naceito" value="N�o Aceito" onclick="salvarTermo('N');">
                            <?PHP
                                }else{
                            ?>
                                    <input id="aceito" type="button" name="aceito" value="Aceito" disabled="disabled">
                                    <input id="naceito" type="button" name="naceito" value="N�o Aceito" disabled="disabled">
                            <?PHP
                                }
                            ?>
                            <input id="sair" type="button" name="sair" value="Sair" onclick="sair();">
                        </div>
                    </td>
                <?PHP
                    }else{
                ?>
                    <td align="center">
                        <div style="float: center; width: 70%;">
                            <input id="imprimir" type="button" name="imprimir" value="Visualizar Impress�o" onclick="imprimirAdesao();">
			</div>
                    </td>
                <?PHP
                    }
                ?>
            </tr>
	</table>
    </form>
</div>