<?php
include_once '_funcoes_maismedicos_2015.php';

$m          = recuperaDadosPrefeitura();
$gestor_sus = recuperaGestorSusMunicipio();

$aderiu = verificarAdesao();
$arrayPerfil = pegaArrayPerfil($_SESSION['usucpf']);
?>

<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>

<html>
<head>
    <title>Mais M�dicos - Termo de Ades�o</title>
    <style>
            @media print {
                .notprint {
                    display: none;
                }
                .div_rolagem {
                    display: none;
                }
            }
            .div_rolagem{
                overflow-x: auto;
                overflow-y: auto;
                height: 50px;
            }
            .notscreen {
                display: none;
            }
            .bordaarredonda {
                background:#FFFFFF;
                color:#000; border: #000 1px solid;
                padding: 10px;
                -moz-border-radius:10px 10px;
                -webkit-border-radius:10px 10px;
                border-radius:10px 10px;
                width:95%;
                text-align:left;
            }
            .quebra{
                page-break-after: always !important;
                height: 0px;
                clear: both;
            }
    </style>
</head>

<body>
<!--    <fieldset style="width: 97%; background:#fff; margin-bottom:35px;">
        <legend> Ades�o ao Programa </legend>-->
            <table bgcolor="#ffffff" align="center" class="quebra">
                <tr>
                    <td width="90%" align="center">
                        <div style="width: 95%; magin-top:10px;">
                            <br>
                            <p align="center" style="margin-bottom: 0px;">
                                <font face="Arial, sans-serif" size="3"><b>Termo de Ades�o ao Programa Mais M�dicos - Edital n� 1/2015</b></font>
                            </p>

                            <br>

                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    TERMO DE ADES�O QUE ENTRE SI CELEBRAM A SECRETARIA DE REGULA��O E SUPERVIS�O DA EDUCA��O SUPERIOR DO MINIST�RIO DA EDUCA��O E O MUNIC�PIO DE <?php echo strtoupper($m['municipio']);?>, NO ESTADO DO <?php echo strtoupper($m['estado']);?>,
                                    PARA DISPONIBILIZA��O DA ESTRUTURA DE EQUIPAMENTOS P�BLICOS, CEN�RIOS DE ATEN��O NA REDE E PROGRAMAS DE SA�DE NECESS�RIOS � AUTORIZA��O DE FUNCIONAMENTO DE CURSOS DE MEDICINA.
                                </span>
                            </p>

                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    O MINIST�RIO DA EDUCA��O, CNPJ n� 00.394.445/0001-01, neste ato representado por MARTA WENDEL ABRAMO, Secret�ria de Regula��o e Supervis�o da Educa��o Superior, com endere�o na
                                    Esplanada dos Minist�rios, Bloco "L", 1� andar, sala 100 - CEP 70.047-900, Bras�lia (DF), e o MUNIC�PIO DE <?php echo strtoupper($m['municipio'])."-".strtoupper($m['estuf']);?>,
                                    <?php echo $m['endereco'];?>, neste ato representado por <?php echo $m['prefeito'];?> (qualifica��o), nos termos da Lei n� 12.871, de 22 de outubro de 2013, resolvem celebrar o
                                    presente Termo de Ades�o para implanta��o e funcionamento de cursos de medicina, por institui��o de educa��o superior privada, mediante as cl�usulas e condi��es seguintes:
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    1. CL�USULA PRIMEIRA - DO OBJETO
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    1.1. O presente termo tem por objeto a ades�o do Munic�pio de <?php echo $m['municipio']."-".$m['estuf'];?> ao Chamamento P�blico previsto no Edital n� 1, de 1� de abril de 2015 e
                                    defini��o de obriga��es e responsabilidades do Munic�pio no oferecimento de estrutura de equipamentos p�blicos, cen�rios de aten��o na rede e programas de sa�de necess�rios �
                                    autoriza��o de funcionamento de curso de gradua��o em medicina a ser ofertado por institui��o de educa��o superior privada nos termos do art. 3�, inciso IV, da Lei n� 12.871/2013.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    2. CL�USULA SEGUNDA - DAS OBRIGA��ES DO MUNIC�PIO
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    2.1. Para consecu��o do objeto estabelecido neste Termo de Ades�o, o Munic�pio dever� disponibilizar a estrutura de equipamentos p�blicos, cen�rios de aten��o na rede e programas
                                    de sa�de existentes no munic�pio e na regi�o de sa�de, previstos no artigo 4�, da Portaria Normativa n� 5, de 1� de abril de 2015 e no item 3.2.2 do Edital n� 1/2015, al�m de
                                    outros que podem ser estabelecidos pela SERES:
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    a) n�mero de leitos do SUS por aluno, maior ou igual a 5 (cinco);
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    b) exist�ncia de Equipes Multiprofissionais de Aten��o Domiciliar - EMAD;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    c) n�mero de alunos por Equipe de Aten��o B�sica (EAB) menor ou igual a 3 (tr�s);
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    d) exist�ncia de leitos de urg�ncia e emerg�ncia ou Pronto Socorro;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    e) grau de comprometimento dos leitos SUS para utiliza��o acad�mica;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    f) exist�ncia de, pelo menos, 03 (tr�s) Programas de Resid�ncia M�dica nas especialidades priorit�rias, conforme legisla��o de reg�ncia;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    g) ades�o pelo munic�pio ao Programa Nacional de Melhoria do Acesso e da Qualidade na Aten��o B�sica - PMAQ;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    h) exist�ncia de Centro de Aten��o Psicossocial - CAPS; e
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    i) hospital de ensino ou unidade hospitalar com mais de 80 (oitenta) leitos, com potencial para hospital de ensino, conforme legisla��o de reg�ncia.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    3. CL�USULA TERCEIRA - DAS OBRIGA��ES DA SECRETARIA DE REGULA��O E SUPERVIS�O DO MINIST�RIO DA EDUCA��O
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size:13px;">
                                    3.1. Constituem obriga��es da SERES:
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    a) selecionar os Munic�pios para implanta��o e funcionamento de cursos de medicina, por institui��es de educa��o superior privadas;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    b) celebrar Termo de Ades�o com o munic�pio selecionado, acompanhar e monitorar a implanta��o do curso de medicina naquela localidade;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    c) selecionar as institui��es de educa��o superior privadas para oferta de cursos de gradua��o em medicina nos munic�pios selecionados;
                                </span>
                            </p>
                            <p align="justify" style="margin-left: 0.98in;">
                                <span style="font-size: 12px;">
                                    d) editar normas complementares necess�rias ao cumprimento do estabelecido neste Termo de Ades�o.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    4. CL�USULA QUARTA - DA VIG�NCIA
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    4.1. Os compromissos assumidos pelo Munic�pio no presente Termo de Ades�o est�o vinculados � regularidade da oferta do curso pela institui��o de educa��o superior.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    5. CL�USULA QUINTA - DA RESCIS�O
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    5.1. No caso de rescis�o do presente Termo, cumpre ao Munic�pio informar � institui��o de educa��o superior privada ofertante do curso e � Secretaria de Regula��o
                                    e Supervis�o da Educa��o Superior, com anteced�ncia m�nima de 90 (noventa) dias, a fim de preservar a continuidade da oferta do curso.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    6. CL�USULA S�TIMA - DA PUBLICA��O
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    6.1. O presente Termo de Ades�o estar� dispon�vel no Sistema Integrado de Monitoramento e Controle <a href="http://simec.mec.gov.br">http://simec.mec.gov.br</a>.
                                </font>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    7. CL�USULA OITAVA - DAS ALTERA��ES
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    7.1. As eventuais altera��es do presente Termo de Ades�o ser�o realizadas por meio de termo aditivo acordado entre os part�cipes.
                                </span>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    8. CL�USULA NONA - DA SOLU��O DE CONTROV�RSIAS
                                </font>
                            </p>
                            <p align="justify" style="text-indent: 0.59in;">
                                <span style="font-size: 12px;">
                                    8.1. Eventual controv�rsia surgida durante a execu��o do presente Termo de Ades�o poder� ser dirimida administrativamente entre os part�cipes ou,
                                    em seguida, perante a C�mara de Concilia��o e Arbitragem da Administra��o Federal da Advocacia - Geral da Uni�o e, se invi�vel, posteriormente
                                    perante o foro da Justi�a Federal - Se��o Judici�ria do Distrito Federal.
                                </span>
                            </p>
                            <br><br>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px;">
                                    Bras�lia-DF, ___ de ____________ de 2015.
                                </span>
                            </p>
                            <br><br>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px;">____________________________________</span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    MARTA WENDEL ABRAMO
                                </span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    SECRET�RIA DE REGULA��O E SUPERVIS�O DA EDUCA��O SUPERIOR
                                </span>
                            </p>
                            <br><br>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px;">
                                    _____________________________________________________
                                </span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    <?php echo $m['prefeito']; ?>
                                </span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    PREFEITO MUNICIPAL DE <?php echo strtoupper($m['municipio'])."-".strtoupper($m['estuf']);?>
                                </span>
                            </p>
                            <br><br>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px;">
                                    _____________________________________________________
                                </span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    <?php echo $gestor_sus ? $gestor_sus : 'N�o Informado'; ?>
                                </span>
                            </p>
                            <p align="right" style="margin-right: 0.98in;">
                                <span style="font-size: 12px; font-weight: bold;">
                                    GESTOR LOCAL DO SISTEMA �NICO DE SA�DE
                                </span>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
<!--    </fieldset>-->

    <table bgcolor="#f5f5f5" align="center" class="tabela notprint">
        <tr>
            <td align="center">
                <div style="float: center; width: 70%;">
                    <input id="imprimir" type="button" name="imprimir" value="Imprimir" onclick="window.print();">
                </div>
            </td>

        </tr>
    </table>