<?PHP
    include_once '_funcoes_eja.php';

    if($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
    }

    if( !$_SESSION['par']['prgid'] ){
        $db->sucesso( 'principal/planoTrabalho&acao=A&tipoDiagnostico=programa', '', 'Ocorreu um ploblema. Selecione o programa novamente!' );
        die();
    }

    include APPRAIZ . "includes/cabecalho.inc";
    echo '<br>';

    #PEGA, SELECIONA O MUNIC�PIO OU ESTADO SELECIONADO OU LOGADO PELO USU�RIO. � USADO PARA CRIAR O TITULO.
    $sql = " SELECT prgdsc FROM par.programa WHERE prgid = {$_SESSION['par']['prgid']} ";
    $nomePrograma = $db->pegaUm($sql);

    if($_SESSION['par']['muncod']){
        $sql = "SELECT mundescricao, estuf FROM territorios.municipio WHERE muncod = '{$_SESSION['par']['muncod']}'";
        $rsMunicipio = $db->pegaLinha($sql);
        $descricao = $rsMunicipio['mundescricao'].' - '.$rsMunicipio['estuf'];

        $munBloqueado = bucarMunicipioBloqueado();

    }else{
        $descricao = $_SESSION['par']['estuf'];
    }

    monta_titulo($nomePrograma, $descricao);
    echo '<br/>';

    $aderiu = verificaAdesaoEJA();

    #VERIFICA SE O PROGRAMA ESTA NA DATA HABIL PARA ADES�O.
    /*
    $prog_habil = programaDataHabil( $_SESSION['par']['prgid'] );

    if($prog_habil == 'S'){
        $habilita = 'onclick="continuaAdesao()"';
    }else{
        $habilita = 'disabled="disabled"';
    }
    */

    if($munBloqueado != ''){
        $habilita = 'disabled="disabled"';
    }else{
        if( $aderiu == 'S'){
            $habilita = 'disabled="disabled"';
        }else{
            $habilita = 'onclick="continuaAdesao()"';
        }

        echo carregaAbasProFuncionario('par.php?modulo=principal/programas/feirao_programas/eja/eja_informativo_adesao&acao=A');
    }

?>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<link href="../includes/jquery-autocomplete/jquery.autocomplete.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script	type="text/javascript" src="../includes/jquery-autocomplete/jquery.autocomplete.js"></script>

<script type="text/javascript">

    function continuaAdesao(){
        $('#requisicao').val('continuaAdesao');
        $('#formulario').submit();
    }
</script>


<form action="" method="POST" id="formulario" name="formulario">

    <input type="hidden" id="requisicao" name="requisicao" value=""/>

    <center>
        <table bgcolor="#f5f5f5" align="center" class="tabela" >
            <tr>
                <td width="90%" align="center">
                    <br>
                    <fieldset style="width: 70%;">
                        <legend style="font-family: sans-serif; font-size: 14px;"> ORIENTA��ES </legend>
                            <p style="text-indent: 5%; text-align: justify; font-family: sans-serif; font-size: 14px; line-height:200%;">

                                <? if($munBloqueado != ''){ ?>
                                        <span style="font-weight: bold;">
                                            Observa��o: "Este munic�pio n�o est� apto a realizar esta ades�o. Constatou-se, por meio da vers�o preliminar do censo escolar de 2013,
                                            que nesse ano  houve redu��o das matr�culas, em compara��o ao ano de 2012".
                                        </span>
                                        <br><br>
                                <? } ?>
                                <!--
                                O Minis�rio da Educa��o transfere recusos financeiros a titulo de apoio � manuten��o de novas turmas de Educa��o de Joves e Adultos oferecidas
                                pelas rede p�blicas de ensino que tenham matr�culas ainda n�o contempladas com recursos do FUNDEB. Para ter acesso a esse recursos os Munic�pios,
                                os Estados e o Distrito Federal precisam acessar o sistema e preencher todos os campos do formul�rio eletr�nico para que a ades�o seja efetivada em 2014.
                                � recomend�vel a leitura pr�via da Resolu��o por meio do link "N�o Informado".
                                -->
                            </p>

                            <img width=70px" src="../imagens/brasao.gif">

                            <p style="font-size: 14px; font-weight: bold;">
                                MINIST�RIO DA EDUCA��O SECRETARIA DE EDUCA��O CONTINUADA, ALFABETIZA��O, DIVERSIDADE E INCLUS�O
                            </p>
                            <p style="font-size: 14px; font-weight: bold;">
                                DIRETORIA DE POL�TICAS DE ALFABETIZA��O E EDUCA��O DE JOVENS E ADULTOS
                            </p>
                            <p style="font-size: 14px; font-weight: bold; text-decoration: underline;">
                                Comunicado
                            </p>
                            <p style="font-size: 13px; font-weight: bold; text-align: left;">
                                I - APRESENTA��O
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                Informa-se por meio desse, que a nova Resolu��o CD/FNDE N� 48 de 11 de dezembro de 2013, foi divulgada no site do FNDE no link: <a href="http://www.fnde.gov.br/fnde/legislacao/resolucoes" target="_blank">http://www.fnde.gov.br/fnde/legislacao/resolucoes</a>
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                A Resolu��o CD/FNDE N� 48 de 11 de dezembro de 2013, altera o art. 4�, o caput e o � 2� do art. 6�, o art. 8�, o caput do art. 15, no qual se inclui o � 4�, e os arts. 18 e 23 da <b>Resolu��o CD/FNDE n� 48, de 2 de outubro de 2012.</b> Dessa forma, se faz necess�rio o conhecimento do inteiro teor dessa Resolu��o.
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                A resolu��o em quest�o tem o objetivo de transfer�ncia direta de recursos financeiros aos estados, munic�pios e Distrito Federal para a manuten��o e desenvolvimento de <b>novas turmas de Educa��o de Jovens e Adultos</b> oferecidas pelas redes p�blicas de ensino, <b>na modalidade presencial</b>, cujas matr�culas ainda n�o tenham sido contempladas com recursos do Fundo de Manuten��o e Desenvolvimento da Educa��o B�sica e de Valoriza��o dos Profissionais da Educa��o (FUNDEB).
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                Os benefici�rios dessa iniciativa s�o as pessoas com 15 anos ou mais que n�o completaram o ensino fundamental ou m�dio, e que ainda n�o foram cadastradas no Censo Escolar (Educacenso) de 2013. Dentre as quais s�o considerados priorit�rios os egressos do Programa Brasil Alfabetizado, popula��es do campo, comunidades quilombolas, povos ind�genas e pessoas privadas de liberdade.
                            </p>
                            <p style="font-size: 13px; font-weight: bold; text-align: left;">
                                II - PER�ODO ESTABELECIDO PARA A REALIZA��O DA ADES�O
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                A ades�o do estado, Distrito Federal ou munic�pio ser� permitida no Sistema Integrado de Monitoramento, Execu��o e Controle do Minist�rio da Educa��o (SIMEC) a partir do dia 17/12/2013, no portal eletr�nico <a href="http://simec.mec.gov.br" target="_blank">http://simec.mec.gov.br</a> , que ficar� aberto por 30 dias, a contar da data de abertura.
                            </p>
                            <p style="font-size: 13px; font-weight: bold; text-align: left;">
                                III - PROCEDIMENTOS PARA A ADES�O - PASSO A PASSO
                            </p>
                            <p style="font-size: 12px; text-align: justify;">
                                <b>Passo 01 - Acessar o SIMEC, pelo endere�o eletr�nico</b> <a href="http://simec.mec.gov.br" target="_blank">http://simec.mec.gov.br,</a> <b>clique na Aba PAR e aparecer� a primeira tela.</b>
                            </p>
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 02 - Na mesma tela, tela inical, Clicar o �cone: Financiamento de Novas Turmas de Educa��o de Jovens e Adultos
                            </p>

                            <img width=860px" src="../imagens/eja/1_tela.png">
                            <!--
                            <img width=860px" src="../imagens/eja/segunda_tela.png">
                            -->
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 03: Clicar em:  continuar a ades�o.
                            </p>

                            <img width=860px" src="../imagens/eja/3_tela.png">
                            <br>
                            <b>.</b><br>
                            <b>.</b><br>
                            <b>.</b><br>
                            <img width=860px" src="../imagens/eja/3_1_tela.png">

                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Ap�s clicar continuar a ades�o aparecer� uma mensagem de boas vindas � a��o de financiamento de novas turmas.
                            </p>

                            <img width=860px" src="../imagens/eja/4_tela.png">

                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 04: Clicar em: Iniciar o Processo
                            </p>
                            
                            <img width=860px" src="../imagens/eja/5_tela.png">
                            
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 05: Preencher os campos indicados e salvar no final. Nesta p�gina est�o dispon�veis as informa��es sobre as matr�culas de EJA no Censo Escolar de 2010 a 2013 e o n�mero de egressos do Programa Brasil Alfabetizado.
                            </p>
                            
                            <img width=860px" src="../imagens/eja/6_tela.png">
                            
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 06: A tela abaixo aparecer� automaticamente. Nela ser�o informadas as escolas onde as novas turmas ir�o funcionar. A rela��o das escolas surgir� ap�s duplo click no ret�ngulo apontado abaixo. Ap�s indicar todas as escolas, clicar em salvar.
                            </p>
                            
                            <img width=860px" src="../imagens/eja/7_tela.png">
                            
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Caso seja necess�rio remover uma escola informada indevidamente, basta selecional� e clicar no "X" (vermelho)
                            </p>
                            
                            <img width=330px" src="../imagens/eja/8_tela.png">
                            
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 07: Ap�s salvar, clicar em: enviar para an�lise do MEC  conforme indica��o abaixo. Ap�s o envio n�o ser� poss�vel alterar o documento antes da an�lise t�cnica do MEC.
                            </p>
                            
                            <img width=860px" src="../imagens/eja/9_tela.png">
                            
                            <p style="font-size: 12px; text-align: justify; font-weight: bold;">
                                Passo 08: Ap�s a aprova��o do MEC ser� poss�vel imprimir a ficha de aprova��o com todas as informa��es sobre o processo de an�lise. A ficha pode ser impressa, mas N�O PRECISA ser enviada ao MEC. 
                            </p>
                            
                            <img width=860px" src="../imagens/eja/10_tela.png">

                    </fieldset>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="float: center; width: 70%; ">
                        <input type="button" name="continua" value="Continuar Ades�o" <?=$habilita;?>/>
                        <input type="button" name="cancelar" value="Sair" onclick="window.location.href = 'par.php?modulo=principal/planoTrabalho&acao=A&tipoDiagnostico=programa'" />
                    </div>
                </td>
            </tr>
        </table>
    </center>
</form>



