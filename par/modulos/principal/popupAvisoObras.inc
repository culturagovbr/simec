<div align="center" style="font-family: 'Arial',serif; font-size: 14px;">
<div style="width:95%;  float: right; " >




<pre  align="center" >
<b style="text-align: center;" > REFORMULA��O DE OBRAS � METODOLOGIAS INOVADORAS

COMUNICADO
</b>
</pre> 
</div>
<div style="width:95%;  float: right; "  align="justify" >
<pre align="justify" >
	O Fundo Nacional de Desenvolvimento da Educa��o (FNDE) far� reformula��o das obras de munic�pios
interessados e que assinaram o termo de compromisso com a utiliza��o de Metodologias Inovadoras, com 
o seguinte calend�rio:

	a) A partir de 04/05/2015 - Munic�pios que n�o contrataram com as empresas vencedoras das Atas 
de Registro de Pre�os; (detalhes - link)

	b) A partir de 01/06/2015 � Munic�pios que contrataram e realizaram distratos com as empresas 
vencedoras das Atas de Registro de Pre�os, cujas obras ainda n�o tenham iniciadas.

	Os munic�pios enquadrados no item A n�o precisam encaminhar of�cio de solicita��o ao FNDE, pois
as a��es ser�o encaminhas automaticamente para o processo de reformula��o no SIMEC, m�dulo PAR.

	Os munic�pios que se enquadram no item B, devem inserir o distrato no SIMEC-m�dulo Obras 2.0, 
aba Documentos  e encaminhar e-mail comunicando o fato para o e-mail  reformula��o_cgest@fnde.gov.br.
(detalhes - link)

	A reformula��o ocorrer� em duas �reas: uma na tipologia de obra e outra na
 poss�vel troca de terreno. As creches de Tipo B ser�o substitu�das pelas de Tipo 
 Padr�o 1 � com constru��o convencional, e as de Tipo C, pelas de Tipo Padr�o 2 � tamb�m 
 com constru��o convencional:
	<b>1. Troca de Terreno:</b> 04 de maio de 2015 dever� ser inserida a seguinte documenta��o:
<b>Aba Dados do terreno:</b>
	<b>� Nome do terreno;</b>
	<b>� Endere�o - </b>Logradouro, N�mero, Complemento e Bairro do novo terreno;
	<b>� Coordenadas</b> do novo terreno;

<b>Aba Relat�rio de vistoria:</b>
	� Informa��es t�cnicas do novo terreno.

<b>Aba Cadastro de fotos do terreno:</b>
	� Relat�rio fotogr�fico - fotos do novo terreno com legenda;

<b>Aba Documentos anexos</b>
	<b>� Estudo de Demanda -</b> novo estudo de demanda caso o terreno esteja em outro bairro;
	<b>� Planta de localiza��o - </b>planta indicando a localiza��o do novo terreno na malha 
urbana do munic�pio (mapa da cidade); 
	<b>� Planta de situa��o -</b> planta indicando as dimens�es, lotes vizinhos e ruas de acesso
 do novo terreno; 
	<b>� Planta de loca��o -</b> planta com o projeto padr�o FNDE da Escola (vers�o at� 2014) 
inserido no novo terreno; 
	<b>� Levantamento planialtim�trico - </b>planta com indica��o das curvas de n�vel do novo 
terreno a cada metro de altura; 
	<b>� Dominialidade assinada.</b> Documento de propriedade do terreno atualizado ou (como alternativa)
 a Declara��o de 

<b>2. Tipologia de Obras:</b> novas informa��es a partir do dia 11 de maio.
* Os projetos padr�o de Tipo 1 e de Tipo 2 estar�o dispon�veis oportunamente no site do FNDE 
(www.fnde.gov.br).

</pre>

</div>
</div>
