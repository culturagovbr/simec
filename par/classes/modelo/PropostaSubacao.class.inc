<?php
	
class PropostaSubacao extends Modelo
{
	
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = "par.propostasubacao";	

    /**
     * Chave primaria.
     * @var array
     * @access protected
     */
    protected $arChavePrimaria = array( "ppsid" );

    /**
     * Atributos
     * @var array
     * @access protected
     */    
    protected $arAtributos     = array(
									  	'ppsid' => null, 
									  	'ppsdsc' => null, 
									  	'ppsordem' => null,
    									'ppaid' => null,
    									'frmid' => null,
									    'prgid' => null,
									    'indid' => null,
									    'ppsestrategiaimplementacao' => null,
									    'undid' => null,
									    'ppspeso' => null,
    									'foaid' => null,
    									'ppsobra' => null,
    									'ppsptres' => null,
    									'ppsnaturezadespesa' => null,
    									'ppscronograma' => null,
    									'ppsmonitora' => null,
    									'ptsid' => null,
    									'ppsobjetivo' => null,
    									'ppstexto' => null,
    									'ppscobertura' => null,
    									'ppscarga' => null
									  );
									  
									  
	public function recuperaDadosFormGuiaSubacao($indid)
	{
		
		$sql = "SELECT  d.dimcod as codigodimensao,			
			            a.arecod as codigoarea,			
			            i.indcod as codigoindicador,			
			            d.dimdsc as descricaodimensao,			
			            a.aredsc as descricaoarea,			
			            i.inddsc as descricaoindicador,			
			            c.crtid  as idcriterio,			
			            c.crtdsc as descricaocriterio,			
			            c.crtpontuacao as pontuacao			
			            --cps.crtid as idcriteriosubacao			
			FROM par.dimensao d			
			INNER JOIN par.area a ON a.dimid = d.dimid			
			INNER JOIN par.indicador i ON i.areid = a.areid			
			INNER JOIN par.criterio c ON c.indid = i.indid AND c.crtstatus = 'A'	
			--LEFT JOIN par.criteriopropostasubacao cps ON cps.crtid = c.crtid			
			WHERE i.indid = {$indid}		
			ORDER BY c.crtpontuacao";
		
		return $this->carregar($sql);
	}
	
	public function recuperarSubacaoGuia($indid)
	{
		$sql = "SELECT DISTINCT
				  sub.ppsid,
				  sub.ppsdsc,
				  sub.ppsordem,
				  sub.frmid,
				  sub.prgid,
				  sub.indid,
				  sub.ppsestrategiaimplementacao,
				  sub.undid,
				  sub.ppspeso,
				  sub.foaid,
				  sub.ppsobra,
				  sub.ppsptres,
				  sub.ppsnaturezadespesa,
				  sub.ppscronograma,
				  sub.ppsmonitora,
				  sub.ptsid				    
				FROM {$this->stNomeTabela} sub
				WHERE sub.indid = {$indid}
				ORDER BY sub.ppsordem";
		
		return $this->carregar($sql);
	}
	
	public function recuperarSubacoesPorCriterio($crtid)
	{
		$sql = "SELECT * FROM par.propostasubacao pps
				INNER JOIN par.criteriopropostasubacao cps ON pps.ppsid = cps.ppsid
				WHERE crtid = {$crtid} order by ppsordem";

		return $this->carregar($sql);
	}
	
	public function carregaPropostaSubacaoPorSbaid($sbaid)
	{
		$sql = "SELECT DISTINCT 
                  itr.itrdsc,
                  dim.dimdsc,
                  are.aredsc,
                  ind.inddsc,
                  -- pps.ppsdsc,
                  'Constru��o de unidade de Educa��o Infantil' AS ppsdsc,
                  'A��es do PAC 2' as foadescricao,
                  -- foa.foadescricao,
                  'Quando a rede existente de ensino fundamental, na �rea rural, em assentamentos, comunidades ind�genas e/ou quilombolas, possui pr�dios pr�prios ou cedidos com infraestrutura f�sica adequada. As instala��es apresentam condi��es adequadas de seguran�a, salubridade e conforto ambiental. Al�m disso, as instala��es garantem acessibilidade, os banheiros s�o adequados e compat�veis com a faixa et�ria dos usu�rios. Os pr�dios apresentam espa�os adequados e atendem aos padr�es m�nimos. Todas as escolas da rede possuem salas de recursos multifuncionais implantadas e disp�em de espa�o f�sico adequado para seu funcionamento. A infraestrutura, de modo geral, apresenta salas de aula em quantidade suficiente para a demanda, existem laborat�rios de Inform�tica e Ci�ncias, sala de recursos audiovisuais, biblioteca, cozinha, refeit�rio, quadra de esporte e espa�os de lazer e conviv�ncia.'
                  as estrategiaimplementacao,
                  -- sba.estrategiaimplementacao,
                  'Unidade Escolar' as unddsc,
                  -- und.unddsc,
                  -- frm.frmdsc,
                  'MEC - Transfer�ncia volunt�ria' as frmdsc,
                  CASE WHEN pps.ppscronograma = 1 THEN 'Global'
                       WHEN pps.ppscronograma = 2 THEN 'Por escola'
                       ELSE 'CPF'
                  END AS crono
            FROM par.propostasubacao pps
            INNER JOIN par.indicador ind ON ind.indid = pps.indid
            INNER JOIN par.criterio crt ON crt.indid = ind.indid AND crt.crtstatus = 'A'
            INNER JOIN par.propostaacao ppa ON ppa.crtid = crt.crtid
            INNER JOIN par.area are ON are.areid = ind.areid
            INNER JOIN par.dimensao dim ON dim.dimid = are.dimid
            INNER JOIN par.instrumento itr ON itr.itrid = dim.itrid
            INNER JOIN par.subacaotemporaria sba ON pps.ppsid = sba.ppsid
            INNER JOIN par.formaatendimento foa ON foa.foaid = pps.foaid
            INNER JOIN par.unidademedida und ON und.undid = pps.undid
            INNER JOIN par.formaexecucao frm ON frm.frmid = pps.frmid
            WHERE sba.subid = {$sbaid} ";

		return $this->pegaLinha($sql);
	}
	
	public function recuperarMunicipiosPorSubacao($ppsid)
	{
		$sql = "SELECT mun.estuf, mun.muncod , mundescricao FROM par.propostasubacaomunicipio pps
				INNER JOIN territorios.municipio mun ON mun.muncod = pps.muncod
				WHERE ppsid = {$ppsid} order by mun.estuf, mundescricao";

		return $this->carregar($sql);
	}
	
	public function recuperarEstadosPorSubacao($ppsid)
	{
		$sql = "SELECT est.estuf, est.estdescricao FROM par.propostasubacaoestados ppe
				INNER JOIN territorios.estado est ON est.estuf = ppe.estuf
				WHERE ppsid = {$ppsid} order by estdescricao";

		return $this->carregar($sql);
	}
									  
}									  