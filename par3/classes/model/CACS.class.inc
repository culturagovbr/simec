<?php

class Par3_Model_CACS extends Modelo
{
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = "integracao.cacs";

    /**
     * Chave primaria.
     * @var array
     * @access protected
     */
    protected $arChavePrimaria = array();

    /**
     * Atributos
     * @var array
     * @access protected
     */
    protected $arAtributos = array(
		'co_mun_ibge_conselho' => null,
	    'esf_adm_conselho' => null,
	    'sit_mandato' => null,
	    'uf_conselho' => null,
	    'mun_conselho' => null,
	    'email_conselho' => null,
	    'cpf_conselheiro' => null,
	    'no_conselheiro' => null,
	    'email_conselheiro' => null,
	    'sit_conselheiro' => null,
	    'ds_segmento' => null,
	    'tp_membro' => null,
	    'ds_funcao' => null
	);
    
    public function __construct() 
    {
    	$GLOBALS['nome_bd'] = 'dbcorporativo';
    	$GLOBALS['servidor_bd'] = '192.168.222.45';
    	$GLOBALS['porta_bd'] = '5433';
    	$GLOBALS['usuario_db'] = 'simec';
    	$GLOBALS['senha_bd'] = 'phpsimecao';
    }
    
	public function listarConselheiros($muncod)
	{
		$sql = "SELECT * 
			    FROM {$this->stNomeTabela}
			    WHERE co_mun_ibge_conselho = '{$muncod}'";

		return $this->carregar($sql);
	}
}