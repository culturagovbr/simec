<?php

include_once "_funcoes.php";

$perfis = pegaPerfilGeral();

if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

if($_REQUEST['requisicao']) {
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}


include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - SISPACTO", "" );


if(in_array(PFL_COORDENADORIES,$perfis)) {
	
	/*
	$relatorios = array(
			0=>array('codigo'=>'relatpersonal_turmasgerais.inc','descricao'=>'Informa��es sobre dados gerais das turmas'),
			//1=>array('codigo'=>'relatpersonal_relatoexperiencia.inc','descricao'=>'Relato Experi�ncia'),
			//2=>array('codigo'=>'relatpersonal_aprendizagemmat.inc','descricao'=>'Informa��es sobre aprendizagem (Matem�tica)'),
			//3=>array('codigo'=>'relatpersonal_aprendizagempor.inc','descricao'=>'Informa��es sobre aprendizagem (Portugu�s)'),
			//4=>array('codigo'=>'relatpersonal_materiais.inc','descricao'=>'Informa��es a entrega dos materiais'),
			//5=>array('codigo'=>'relatpersonal_aprendizagemdid.inc','descricao'=>'Informa��es sobre uso do materiais did�ticos'),
			//6=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO 2014'),
			//7=>array('codigo'=>'relatpersonal_impressaoana.inc','descricao'=>'Relat�rio de impress�o ANA'),
			//8=>array('codigo'=>'relatpersonal_questoesatv8.inc','descricao'=>'Relat�rio das quest�es da atividade 8'),
			//9=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Quantitativos do SISPACTO 2014')

	);
	*/

} else {

	
	$relatorios = array(//0=>array('codigo'=>'relatpersonal_erroscadastrossgb.inc','descricao'=>'Relat�rio de erros no cadastro de bolsistas no SGB'),
						//1=>array('codigo'=>'relatpersonal_aceitacaotermocompromisso.inc','descricao'=>'Relat�rio de ades�o ao termo de compromisso'),
						//2=>array('codigo'=>'relatpersonal_errospagamentossgb.inc','descricao'=>'Relat�rio de erros no envio de pagamentos no SGB'),
						//3=>array('codigo'=>'relatpersonal_bolsistasparados.inc','descricao'=>'Relat�rio de Bolsistas sem tramita��o do pagamento'),
						0=>array('codigo'=>'relatpersonal_abrangencia.inc','descricao'=>'Abrang�ncia das IES'),
						1=>array('codigo'=>'relatpersonal_profturmas.inc','descricao'=>'Informa��es sobre Turmas de Professores'),
						2=>array('codigo'=>'relatpersonal_profturmas2.inc','descricao'=>'Informa��es sobre Turmas de Professores 2')
						//17=>array('codigo'=>'relatpersonal_relatoriofinal.inc','descricao'=>'Situa��o dos Relat�rios Finais')
						
						);
	
}


?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadossispacto';
		var janela = window.open( '', 'relatoriopersonlizadossispacto', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '400', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>