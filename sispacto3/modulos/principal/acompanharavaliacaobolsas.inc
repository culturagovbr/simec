<?
if(!$_SESSION['sispacto3'][$sis]['uncid']) {
	$al = array("alert"=>"Usu�rio n�o vinculado com nenhuma universidade","location"=>"sispacto3.php?modulo=inicio&acao=C");
	alertlocation($al);
}
?>
<script>
function wf_exibirHistorico( docid )
{
	var url = 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/geral/workflow/historico.php' +
		'?modulo=principal/tramitacao' +
		'&acao=C' +
		'&docid=' + docid;
	window.open(
		url,
		'alterarEstado',
		'width=675,height=500,scrollbars=yes,scrolling=no,resizebled=no'
	);
}

function abrirDetalhes(id) {
	if(document.getElementById('img_'+id).title=='mais') {
		document.getElementById('tr_'+id).style.display='';
		document.getElementById('img_'+id).title='menos';
		document.getElementById('img_'+id).src='../imagens/menos.gif'
	} else {
		document.getElementById('tr_'+id).style.display='none';
		document.getElementById('img_'+id).title='mais';
		document.getElementById('img_'+id).src='../imagens/mais.gif'

	}

}

</script>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="10" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2">Acompanhamento de avalia��es e bolsas</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao("/sispacto3/sispacto3.php?modulo=principal/{$sis}/".(($sis=='universidade')?'universidadeexecucao':(($sis=='coordenadorlocal')?'coordenadorlocalexecucao':$sis))."&acao=A&aba=acompanhamentoavaliacaobolsas"); ?></td>
</tr>
<tr>
	<td colspan="2">
	<?
	
	$sql = "SELECT p.plpmaximobolsas 
			FROM sispacto3.identificacaousuario i 
			INNER JOIN sispacto3.tipoperfil t ON t.iusd = i.iusd 
			INNER JOIN sispacto3.pagamentoperfil p ON p.pflcod = t.pflcod 
			WHERE i.iusd='".$_SESSION['sispacto3'][$sis]['iusd']."'";
	
	$nmaximobolsas = $db->pegaUm($sql);
		
	$sql = "SELECT f.fpbid as codigo, 
				   rf.rfuparcela ||'� Parcela ( Ref. ' || m.mesdsc || ' / ' || fpbanoreferencia ||' )' as descricao,
				   COALESCE((
				   	SELECT e.esddsc || ' ( ' || to_char(h.htddata,'dd/mm/YYYY HH24:MI') || ' )' as s FROM sispacto3.pagamentobolsista p 
					INNER JOIN workflow.documento d ON d.docid = p.docid 
					INNER JOIN workflow.estadodocumento e ON e.esdid = d.esdid 
					LEFT JOIN workflow.historicodocumento h ON h.hstid = d.hstid 
					WHERE p.iusd='".$_SESSION['sispacto3'][$sis]['iusd']."' AND p.fpbid=f.fpbid
					),'') as statuspagamento  
			FROM sispacto3.folhapagamento f 
			INNER JOIN sispacto3.folhapagamentouniversidade rf ON rf.fpbid = f.fpbid AND rf.pflcod=(SELECT pflcod FROM sispacto3.tipoperfil WHERE iusd=".$_SESSION['sispacto3'][$sis]['iusd'].") 
			INNER JOIN public.meses m ON m.mescod::integer = f.fpbmesreferencia
			WHERE rf.uncid='".$_SESSION['sispacto3'][$sis]['uncid']."' AND to_char(NOW(),'YYYY-mm-dd')::date>=(fpbanoreferencia::text||'-'||lpad(fpbmesreferencia::text, 2, '0')||'-01')::date
			ORDER BY (fpbanoreferencia::text||'-'||lpad(fpbmesreferencia::text, 2, '0')||'-15')::date 
			LIMIT ".$nmaximobolsas;
	
	$folhapagamento = $db->carregar($sql);
	
	if($folhapagamento[0]) {
		
		echo '<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="8" align="center">';
		echo '<tr><td class="SubTituloEsquerda" colspan="2">Extrato de pagamento/avalia��es</td></tr>';
		echo '<tr><td class="SubTituloCentro" width="30%">Parcela</td><td class="SubTituloCentro" width="70%">Situa��o pagamento(Data de atualiza��o)</td></tr>';
		
		foreach($folhapagamento as $fl) {
			
			echo '<tr><td class="SubTituloEsquerda">'.$fl['descricao'].'<br>
				   		<img src="../imagens/mais.gif" style="cursor:pointer;" title="mais" id="img_ava_'.$fl['codigo'].'" onclick="abrirDetalhes(\'ava_'.$fl['codigo'].'\');"> <span style=font-size:xx-small;>Avalia��o</span><br>
				   		<img src="../imagens/mais.gif" style="cursor:pointer;" title="mais" id="img_pag_'.$fl['codigo'].'" onclick="abrirDetalhes(\'pag_'.$fl['codigo'].'\');"> <span style=font-size:xx-small;>Pagamento</span>
				   	  </td>
					  <td><font size=3><b>'.(($fl['statuspagamento'])?$fl['statuspagamento']:'').'</b></font></td></tr>';
			echo '<tr style="display:none;" id="tr_ava_'.$fl['codigo'].'"><td colspan="2">';
			echo '<p align="center"><b>INFORMA��ES SOBRE AVALIA��ES</b></p>';
			
			$sql = "SELECT * FROM sispacto3.mensario WHERE fpbid='".$fl['codigo']."' AND iusd='".$_SESSION['sispacto3'][$sis]['iusd']."'";
			$mensario = $db->pegaLinha($sql);
			
			if($mensario['menid']) consultarDetalhesAvaliacoes(array('menid'=>$mensario['menid']));
			else echo '<p align=center style=color:red;>N�o existem avalia��es nesse per�odo de refer�ncia</p>';
			
			echo '</td></tr>';
			echo '<tr style="display:none;" id="tr_pag_'.$fl['codigo'].'"><td colspan="2">';
			echo '<p align="center"><b>INFORMA��ES SOBRE PAGAMENTO</b></p>';
			
			$sql = "SELECT pboid FROM sispacto3.pagamentobolsista WHERE fpbid='".$fl['codigo']."' AND iusd='".$_SESSION['sispacto3'][$sis]['iusd']."'";
			$pboid = $db->pegaUm($sql);
			
			if($pboid) {
				consultarDetalhesPagamento(array('pboid'=>$pboid));
			} else {
				echo "<p align=center style=color:red;>N�o existem pagamentos nesse per�odo de refer�ncia</p>";	
				
				$restricao = pegarRestricaoPagamento(array('iusd' => $_SESSION['sispacto3'][$sis]['iusd'], 'fpbid' => $fl['codigo']));
				
				echo "<table class=\"listagem\" bgcolor=\"#f5f5f5\" cellSpacing=\"5\" cellPadding=\"10\" align=\"center\">";
				echo "<tr>";
				echo "<td class=\"SubTituloDireita\"><b>Poss�vel restri��o:</b></td>";
				echo "<td><b>".$restricao."</b></td>";
				echo "</tr>";
				echo "</table>";
			} 
			
			
			echo '</td></tr>';
			
		}
		echo '<tr><td class="SubTituloCentro" colspan="2">';
		criarBotoesNavegacao(array('url' => $_SERVER['REQUEST_URI']));
		echo '</td></tr>';
		echo '</table>';
		
		echo '<table class="tabela" bgcolor="#f5f5f5" cellSpacing="5" cellPadding="1" align="center">';
		echo '<tr><td colspan="3" style="font-size:xx-small;"><p>Prezado bolsista, ap�s ser APROVADO no fluxo de avalia��o, o pagamento das bolsas no �mbito do Pacto Nacional pela Alfabetiza��o na Idade Certa obedece ao seguinte fluxo:</p></td></tr>';
		echo '<tr><td class="SubTituloCentro" style="font-size:xx-small;">Status de pagamento</td><td class="SubTituloCentro" style="font-size:xx-small;">Tempo m�dio</td><td class="SubTituloCentro" style="font-size:xx-small;">Descri��o</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Aguardando autoriza��o IES</td><td style="font-size:xx-small;">aprox. 2 dias</td><td style="font-size:xx-small;">O bolsista foi avaliado e considerado apto a receber a bolsa. A libera��o do pagamento est� aguardando autoriza��o final pela Universidade respons�vel pela forma��o.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Autorizado IES</td><td style="font-size:xx-small;">aprox. 3 dias</td><td style="font-size:xx-small;">O pagamento da bolsa foi autorizado pela Universidade e est� sendo processado pelos sistemas do MEC.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Aguardando autoriza��o SGB</td><td style="font-size:xx-small;">aprox. 3 dias</td><td style="font-size:xx-small;">O pagamento da bolsa est� no Sistema de Gest�o de Bolsas, aguardando autoriza��o do MEC.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Aguardando pagamento</td><td style="font-size:xx-small;">aprox. 9 dias</td><td style="font-size:xx-small;">O pagamento da bolsa foi autorizado pelo SGB e est� em processamento.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Enviado ao Banco</td><td style="font-size:xx-small;">aprox. 7 dias</td><td style="font-size:xx-small;">A ordem banc�ria referente ao pagamento da bolsa foi emitida.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Pagamento efetivado</td><td style="font-size:xx-small;">-</td><td style="font-size:xx-small;">O pagamento foi creditado em conta e confirmado pelo banco.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Pagamento n�o autorizado FNDE</td><td style="font-size:xx-small;">-</td><td style="font-size:xx-small;">O pagamento da bolsa n�o foi autorizado pelo FNDE, pois o bolsista recebe bolsa de outro programa do MEC.</td></tr>';
		echo '<tr><td style="font-size:xx-small;">Pagamento recusado</td><td style="font-size:xx-small;">aprox. 2 dias</td><td style="font-size:xx-small;">Pagamento recusado em fun��o de algum erro de registro. Ser� reencaminhado a IES respons�vel pela forma��o.</td></tr>';
		echo '<tr><td colspan="3" style="font-size:xx-small;"><p><b>Observa��o: Caso o seu status no fluxo de pagamento esteja em BRANCO, significa que o m�s de referencia ainda n�o teve o seu fluxo de avalia��o conclu�do. Voc� deve procurar a coordena��o local do PACTO ou a IES respons�vel pela forma��o do seu munic�pio.</b></p></td></tr>';
		echo '</table>';
					
		
		
	} else {
		$al = array("alert"=>"A universidade do Usu�rio n�o possui per�odo de refer�ncia atribu�do","location"=>"sispacto3.php?modulo=inicio&acao=C");
		alertlocation($al);
	}

	?>
	</td>
</tr>

<tr>
	<td class="SubTituloDireita" width="20%">&nbsp;</td>
	<td>
	<? if($goto_ant) : ?>
	<input type="button" value="Anterior" onclick="divCarregando();window.location='<?=$goto_ant ?>';">
	<? endif; ?>
	<? if($goto_pro) : ?>
	<input type="button" value="Pr�ximo" onclick="divCarregando();window.location='<?=$goto_pro ?>';">
	<? endif; ?>
	</td>
</tr>
</table>