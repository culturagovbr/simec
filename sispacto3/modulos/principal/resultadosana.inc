<?

include_once APPRAIZ . "includes/library/simec/Grafico.php";

$grafico = new Grafico(Grafico::K_TIPO_COLUNA);

if(is_numeric($_REQUEST['tpacodigoescola'])) {

	$sql = "SELECT 
			id_escola, no_entidade, upper(rede) as rede, no_municipio, upper(uf) as uf, upper(inse) as inse, pc_formacao_docente,
			qt_aluno_previsto, qt_aluno_presente, nivel_1_leit, nivel_2_leit, nivel_3_leit, nivel_4_leit,
			nivel_sempontuacao_esc, nivel_1_esc, nivel_2_esc, nivel_3_esc, nivel_4_esc,
			nivel_1_mat, nivel_2_mat, nivel_3_mat, nivel_4_mat, fk_cod_municipio 
			FROM sispacto3.ana WHERE id_escola='".$_REQUEST['tpacodigoescola']."'";
	
	$ana = $db->pegaLinha($sql);

}

?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro"><span style="font-size:large;">RESULTADOS DA AVALIA��O NACIONAL DA ALFABETIZA��O - 2013</span></td>
</tr>
<? if($ana) : ?>

<tr>
	<td>
	
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">INEP:</span></td>
			<td><?=$ana['id_escola']?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">ESCOLA / REDE:</span></td>
			<td><?=$ana['no_entidade']?> / <?=$ana['rede']?></td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">MUN�CIPIO / UF:</span></td>
			<td><?=$ana['no_municipio']?> / <?=$ana['uf']?></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<p>A Avalia��o Nacional de Alfabetiza��o (ANA) pretende diagnosticar os n�veis de alfabetiza��o e letramento em L�ngua Portuguesa e alfabetiza��o Matem�tica, apontando fatores contextuais sobre as condi��es do trabalho em cada escola. Os resultados de desempenho apresentados nesta primeira edi��o, realizada em 2013, devem ser interpretados considerando as informa��es do contexto escolar.</p>
			<p>Os resultados de desempenho nas �reas avaliadas s�o expressos em escalas de profici�ncia. As escalas de L�ngua Portuguesa (Leitura) e de Matem�tica da ANA 2013 s�o compostas por quatro n�veis progressivos e cumulativos. Isso significa uma organiza��o da menor para a maior profici�ncia. Quando um percentual de alunos foi posicionado em determinado n�vel da escala, pode-se pressupor que, al�m de terem desenvolvido as habilidades referentes a este n�vel, provavelmente tamb�m desenvolveram as habilidades referentes aos n�veis anteriores.</p>
			<p>A escala de profici�ncia de L�ngua Portuguesa (Escrita) tamb�m � composta por quatro n�veis e, no geral, pressup�e a progress�o da aprendizagem de um n�vel para outro. Contudo, � importante ressaltar que o processo de aquisi��o da escrita n�o ocorre em etapas lineares.</p>
			</td>
		</tr>
	</table>
	
	</td>
</tr>


<tr>
	<td>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
	
		<tr>
			<td class="SubTituloCentro" colspan="2"><span style="font-size:large;">INDICADORES CONTEXTUAIS</span></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<p>O Indicador de N�vel Socioecon�mico e o Indicador de Adequa��o da Forma��o Docente produzem informa��es sobre o contexto em que cada escola desenvolve o trabalho educativo.</p>
			<p>O Indicador de N�vel Socioecon�mico possibilita, de modo geral, situar o p�blico atendido pela escola em um estrato ou n�vel social, apontando o padr�o de vida referente a cada um de seus estratos. Esse indicador � calculado a partir da escolaridade dos pais e da posse de bens e contrata��o de servi�os pela fam�lia dos alunos. Para melhor caracterizar as escolas foram criados sete grupos, de modo que, no Grupo 1, est�o as escolas com n�vel socioecon�mico mais baixo e, no Grupo 7, com n�vel socioecon�mico mais alto.</p>
			<p>O Indicador de Forma��o Docente analisa, em cada escola, a forma��o dos docentes dos anos iniciais do Ensino Fundamental que lecionam L�ngua Portuguesa e Matem�tica. Apresenta, assim, o percentual de disciplinas de L�ngua Portuguesa e Matem�tica que s�o regidas por professores com Licenciatura em Pedagogia/Normal Superior, Licenciatura em Letras-L�ngua Portuguesa ou Matem�tica, respectivamente.</p>
			</td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">N�VEL SOCIOECON�MICO:</span></td>
			<td><?=$ana['inse'] ?></td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">FORMA��O DOCENTE:</span></td>
			<td><?=$ana['pc_formacao_docente'] ?>%</td>
		</tr>
		
	
	</table>
	</td>
</tr>

<tr>
	<td>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
	
		<tr>
			<td class="SubTituloCentro" colspan="2"><span style="font-size:large;">PARTICIPA��O NA AVALIA��O</span></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<p>O quadro a seguir mostra a previs�o de estudantes para participar da ANA, com base nos dados do Censo Escolar, e o n�mero de estudantes que efetivamente realizou as provas.</p>
			</td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">ESTUDANTES PREVISTOS:</span></td>
			<td><?=$ana['qt_aluno_previsto'] ?></td>
		</tr>
		
		<tr>
			<td class="SubTituloDireita" width="30%"><span style="font-size:medium;">ESTUDANTES QUE REALIZARAM AS PROVAS:</span></td>
			<td><?=$ana['qt_aluno_presente'] ?></td>
		</tr>
		
	
	</table>
	</td>
</tr>


<tr>
	<td>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
	
		<tr>
			<td class="SubTituloCentro" colspan="2"><span style="font-size:large;">DISTRIBUI��O DOS ALUNOS DA ESCOLA POR N�VEL DE PROFICI�NCIA EM LEITURA</span></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$dadosleit[] = array('valor' => $ana['nivel_1_leit'], 'descricao' => 'N�vel 1', 'categoria' => '');
			$dadosleit[] = array('valor' => $ana['nivel_2_leit'], 'descricao' => 'N�vel 2', 'categoria' => '');
			$dadosleit[] = array('valor' => $ana['nivel_3_leit'], 'descricao' => 'N�vel 3', 'categoria' => '');
			$dadosleit[] = array('valor' => $ana['nivel_4_leit'], 'descricao' => 'N�vel 4', 'categoria' => '');
			
			$grafico->setFormatoTooltip("function() { return '<span><span style=\"color: ' + this.series.color + '\">' + this.series.name + '</span>: <b>' + number_format(this.y, 2, ',', '.') + ' %</b>'; }")->montarGraficoLinha($dadosleit);
			?>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
				<tr>
				<td class="SubTituloDireita" width="25%">N�vel 1: Desempenho at� 425 pontos</td>
				<td>Ler palavras diss�labas, triss�labas e poliss�labas com estruturas sil�bicas can�nicas, com base em imagem. Ler palavras diss�labas, triss�labas e poliss�labas com estruturas sil�bicas n�o can�nicas, com base em imagem.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 2: Desempenho maior que 425 at� 525 pontos</td>
				<td>Identificar a finalidade de textos como convite, cartaz, texto instrucional (receita) e bilhete. Localizar informa��o expl�cita em textos curtos (com at� cinco linhas) em g�neros como piada, parlenda, poema, tirinha (hist�ria em quadrinhos em at� tr�s quadros), texto informativo e texto narrativo. Identificar o assunto de textos, cujo assunto pode ser identificado no t�tulo ou na primeira linha em g�neros como poema e texto informativo. Inferir o assunto de um cartaz apresentado em sua forma est�vel, com letras grandes e mensagem curta e articula��o da linguagem verbal e n�o verbal.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 3: Desempenho maior que 525 at� 625 pontos</td>
				<td>Inferir o assunto de texto de divulga��o cient�fica para crian�as. Localizar informa��o expl�cita, situada no meio ou final do texto, em g�neros como lenda e cantiga folcl�rica. Identificar o referente de um pronome pessoal do caso reto em g�neros como tirinha e poema narrativo. Inferir rela��o de causa e consequ�ncia em g�neros como tirinha, anedota, f�bula e texto de literatura infantil. Inferir sentido com base em elementos verbais e n�o verbais em tirinha. Reconhecer significado de express�o de linguagem figurada em g�neros como poema narrativo, texto de literatura infantil e tirinha.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 4: Desempenho maior que 625 pontos</td>
				<td>Inferir sentido de palavra em texto verbal. Reconhecer os participantes de um di�logo em uma entrevista ficcional. Inferir sentido em texto verbal. Reconhecer rela��o de tempo em texto verbal. Identificar o referente de pronome possessivo em poema.</td>
				</tr>
				
			</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$sql = "(
					SELECT '<span style=float:right;font-size:medium;>Total Estado</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_leit||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_leit||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_leit||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_leit||' %</span>' as nivel_4_leit
					FROM sispacto3.anaregiao WHERE removeacento(uf) ilike removeacento('".$ana['uf']."') AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;>Total Munic�pio</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_leit||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_leit||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_leit||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_leit||' %</span>' as nivel_4_leit
					FROM sispacto3.anamunicipio WHERE fk_cod_municipio='".$ana['fk_cod_municipio']."' AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;><b>Sua escola</b></span>' as descricao,
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_1_leit']." %</b></span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_2_leit']." %</b></span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_3_leit']." %</b></span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_4_leit']." %</b></span>' as nivel_4_leit
					)";
			$cabecalho = array("&nbsp;","N�vel 1","N�vel 2","N�vel 3","N�vel 4");
			$db->monta_lista_simples($sql,$cabecalho,1000,5,'N','100%','',true, false, false, false);
			?>
			</td>
		</tr>
	
	</table>
	</td>
</tr>

<tr>
	<td>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
	
		<tr>
			<td class="SubTituloCentro" colspan="2"><span style="font-size:large;">DISTRIBUI��O DOS ALUNOS DA ESCOLA POR N�VEL DE PROFICI�NCIA EM ESCRITA</span></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$dadosesc[] = array('valor' => $ana['nivel_sempontuacao_esc'], 'descricao' => 'Sem pontua��o', 'categoria' => '');
			$dadosesc[] = array('valor' => $ana['nivel_1_esc'], 'descricao' => 'N�vel 1', 'categoria' => '');
			$dadosesc[] = array('valor' => $ana['nivel_2_esc'], 'descricao' => 'N�vel 2', 'categoria' => '');
			$dadosesc[] = array('valor' => $ana['nivel_3_esc'], 'descricao' => 'N�vel 3', 'categoria' => '');
			$dadosesc[] = array('valor' => $ana['nivel_4_esc'], 'descricao' => 'N�vel 4', 'categoria' => '');
			
			$grafico->setFormatoTooltip("function() { return '<span><span style=\"color: ' + this.series.color + '\">' + this.series.name + '</span>: <b>' + number_format(this.y, 2, ',', '.') + ' %</b>'; }")->montarGraficoLinha($dadosesc);
			?>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
				<tr>
				<td class="SubTituloDireita" width="25%">N�vel 1: Desempenho at� 400 pontos</td>
				<td>Neste n�vel, foram agrupados desde os alunos que, em geral, s�o capazes de:<br>- Escrever palavras com s�labas can�nicas (consoante e vogal) e n�o can�nicas, com alguma dificuldade, pela omiss�o e/ou troca de letras;<br> at� os que s�o capazes de:<br>- Escrever ortograficamente palavras marcadas pela presen�a de s�labas can�nicas.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 2: Desempenho maior que 400 at� 500 pontos</td>
				<td>Escrever ortograficamente palavras com s�labas n�o can�nicas;<br>Escrever textos incipientes apresentados na forma de apenas uma frase;<br>Produzir textos narrativos, a partir de uma dada situa��o, que apresentam aus�ncia ou inadequa��o dos elementos formais (segmenta��o, pontua��o, ortografia, concord�ncia verbal e concord�ncia nominal) e da textualidade (coes�o e coer�ncia), evidenciando ainda um distanciamento da norma padr�o da l�ngua.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 3: Desempenho maior que 500 at� 580 pontos</td>
				<td>Escrever textos narrativos com mais de uma frase, a partir de uma situa��o dada;<br>Produzir textos narrativos com poucas inadequa��es relativas � segmenta��o, concord�ncia verbal e concord�ncia nominal, embora com algum comprometimento dos elementos formais e da textualidade, evidenciando uma aproxima��o � norma padr�o da l�ngua.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 4: Desempenho maior que 580 pontos</td>
				<td>Produzir textos narrativos, a partir de uma situa��o dada, atendendo adequadamente ao uso de elementos formais e da textualidade, evidenciando o atendimento � norma padr�o da l�ngua.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">Sem Pontua��o</td>
				<td>Cadernos de prova que n�o foram pontuados por conter a escrita de palavras sem rela��o sem�ntica com a imagem apresentada ou escrita incompreens�vel.</td>
				</tr>
				
			</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$sql = "(
					SELECT '<span style=float:right;font-size:medium;>Total Estado</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_esc||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_esc||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_esc||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_esc||' %</span>' as nivel_4_leit
					FROM sispacto3.anaregiao WHERE removeacento(uf) ilike removeacento('".$ana['uf']."') AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;>Total Munic�pio</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_esc||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_esc||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_esc||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_esc||' %</span>' as nivel_4_leit
					FROM sispacto3.anamunicipio WHERE fk_cod_municipio='".$ana['fk_cod_municipio']."' AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;><b>Sua escola</b></span>' as descricao,
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_1_esc']." %</b></span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_2_esc']." %</b></span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_3_esc']." %</b></span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_4_esc']." %</b></span>' as nivel_4_leit
					)";
			$cabecalho = array("&nbsp;","N�vel 1","N�vel 2","N�vel 3","N�vel 4");
			$db->monta_lista_simples($sql,$cabecalho,1000,5,'N','100%','',true, false, false, false);
			?>
			<br>
			<span style="font-size:small;">* Os percentuais exibidos n�o incluem os cadernos de prova "Sem Pontua��o".</span>
			</td>
		</tr>
	
	</table>
	</td>
</tr>

<tr>
	<td>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%">
	
		<tr>
			<td class="SubTituloCentro" colspan="2"><span style="font-size:large;">DISTRIBUI��O DOS ALUNOS DA ESCOLA POR N�VEL DE PROFICI�NCIA EM MATEM�TICA</span></td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$dadosmat[] = array('valor' => $ana['nivel_1_mat'], 'descricao' => 'N�vel 1', 'categoria' => '');
			$dadosmat[] = array('valor' => $ana['nivel_2_mat'], 'descricao' => 'N�vel 2', 'categoria' => '');
			$dadosmat[] = array('valor' => $ana['nivel_3_mat'], 'descricao' => 'N�vel 3', 'categoria' => '');
			$dadosmat[] = array('valor' => $ana['nivel_4_mat'], 'descricao' => 'N�vel 4', 'categoria' => '');
			
			$grafico->setFormatoTooltip("function() { return '<span><span style=\"color: ' + this.series.color + '\">' + this.series.name + '</span>: <b>' + number_format(this.y, 2, ',', '.') + ' %</b>'; }")->montarGraficoLinha($dadosmat);
			?>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
				<tr>
				<td class="SubTituloDireita" width="25%">N�vel 1: Desempenho at� 425 pontos</td>
				<td>Reconhecer representa��o de figura geom�trica plana ou espacial em objetos de uso cotidiano; maior frequ�ncia em gr�fico de colunas; planifica��o de figura geom�trica espacial (paralelep�pedo); horas e minutos em rel�gio digital. Associar objeto de uso cotidiano � representa��o de figura geom�trica espacial; Contar objetos dispostos em forma organizada ou n�o; Comparar medidas de comprimento em objetos do cotidiano.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 2: Desempenho maior que 425 at� 525 pontos</td>
				<td>Reconhecer nomenclatura de figura geom�trica plana; valor monet�rio de c�dula; figura geom�trica plana em uma composi��o com v�rias outras. Associar a escrita por extenso de n�meros naturais com at� tr�s algarismos � sua representa��o simb�lica; valor monet�rio de uma c�dula a um agrupamento de moedas e c�dulas; Completar sequ�ncia num�rica crescente de n�meros naturais n�o consecutivos. Comparar n�meros naturais com at� tr�s algarismos n�o ordenados. Estimar uma medida entre dois n�meros naturais com dois algarismos; Resolver problema de adi��o sem reagrupamento.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 3: Desempenho maior que 525 at� 575 pontos</td>
				<td>Reconhecer frequ�ncias iguais em gr�fico de colunas; composi��o de n�meros naturais com at� tr�s algarismos, apresentada por extenso Completar sequ�ncia num�rica decrescente de n�meros naturais n�o consecutivos. Calcular adi��o de duas parcelas com reagrupamento; Associar valor monet�rio de um conjunto de moedas ao valor de uma c�dula; a representa��o simb�lica de n�meros naturais com at� tr�s algarismos � sua escrita por extenso; Resolver problema de subtra��o, com n�meros naturais de at� dois algarismos, com ideia de comparar e retirar e problema de divis�o com ideia de repartir.</td>
				</tr>
				
				<tr>
				<td class="SubTituloDireita">N�vel 4: Desempenho maior que 575 pontos</td>
				<td>Reconhecer composi��o e decomposi��o aditiva de n�meros naturais com at� tr�s algarismos; medidas de tempo em rel�gios anal�gicos; informa��es em gr�fico de barras. Calcular subtra��o de n�meros naturais com at� tr�s algarismos com reagrupamento. Associar medidas de tempo entre rel�gio anal�gico e digital. Resolver problema de subtra��o como opera��o inversa da adi��o� com n�meros naturais; problemas com a ideia de comparar n�meros naturais de at� tr�s algarismos; problema de multiplica��o com a ideia de proporcionalidade; problema de multiplica��o com a ideia de combina��o; problema de divis�o com ideia de proporcionalidade e problema que envolve medidas de tempo (dias de semanas).</td>
				</tr>
				
			</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
			<?
			$sql = "(
					SELECT '<span style=float:right;font-size:medium;>Total Estado</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_mat||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_mat||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_mat||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_mat||' %</span>' as nivel_4_leit
					FROM sispacto3.anaregiao WHERE removeacento(uf) ilike removeacento('".$ana['uf']."') AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;>Total Munic�pio</span>' as descricao, 
						   '<span style=float:right;font-size:medium;>'||nivel_1_mat||' %</span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_2_mat||' %</span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_3_mat||' %</span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;>'||nivel_4_mat||' %</span>' as nivel_4_leit
					FROM sispacto3.anamunicipio WHERE fk_cod_municipio='".$ana['fk_cod_municipio']."' AND rede ilike '".$ana['rede']."'
					) UNION ALL (
					SELECT '<span style=float:right;font-size:medium;><b>Sua escola</b></span>' as descricao,
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_1_mat']." %</b></span>' as nivel_1_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_2_mat']." %</b></span>' as nivel_2_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_3_mat']." %</b></span>' as nivel_3_leit, 
						   '<span style=float:right;font-size:medium;><b>".$ana['nivel_4_mat']." %</b></span>' as nivel_4_leit
					)";
			$cabecalho = array("&nbsp;","N�vel 1","N�vel 2","N�vel 3","N�vel 4");
			$db->monta_lista_simples($sql,$cabecalho,1000,5,'N','100%','',true, false, false, false);
			?>
			</td>
		</tr>
	
	</table>
	</td>
</tr>
<? else :?>

<tr>
	<td align="center"><span style="font-size:large;color:red;">ESCOLA N�O FOI ENCONTRADA NOS REGISTROS. INEP SELECIONADO : <?=$_REQUEST['tpacodigoescola'] ?></span></td>
</tr>

<? endif;?>

</table>