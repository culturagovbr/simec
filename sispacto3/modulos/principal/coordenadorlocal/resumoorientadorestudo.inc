<?
verificarCoordenadorLocalTermoCompromisso(array("picid"=>$_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['picid']));

$consulta = verificaPermissao();

$perfis = pegaPerfilGeral();


$estado = wf_pegarEstadoAtual( $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['docid'] );
$estadoturma = wf_pegarEstadoAtual( $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['docidturma'] );

if($estado['esdid'] != ESD_ELABORACAO_COORDENADOR_LOCAL && $estadoturma['esdid'] == ESD_FECHADO_TURMA) {
	$consulta = true;
}

if(in_array(PFL_COORDENADORIES,$perfis)) {
	$consulta = true;
}


$ar = array("estuf" 	  => $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['estuf'],
			"muncod" 	  => $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['muncod'],
			"dependencia" => $_SESSION['sispacto3']['esfera']);

$totalalfabetizadores = carregarTotalAlfabetizadores($ar);

?>
<script>
function salvarJustificativas(goto) {

	<? foreach(array_keys($_PERGUNTA_JUSTIFICATIVA) as $key) : ?>
	var nchecks = jQuery("[name^='tjuid[<?=$key ?>]']:enabled").length;
	
	if(nchecks > 0) {
		var ncheckeds = jQuery("[name^='tjuid[<?=$key ?>]']:enabled:checked").length;
		if(ncheckeds == 0) {
			alert('Marque o tipo de Justificativa : <?=$_TIPO_ORIENTADORES[$key] ?>');
			return false;
		}
		if(jQuery("[name^='joecomentario[<?=$key ?>]']").val()=='') {
			alert('Justificativa em branco : <?=$_TIPO_ORIENTADORES[$key] ?>');
			return false;
		}
	}
	<? endforeach; ?>
	
	jQuery('#goto').val(goto);	
	document.getElementById('formulario').submit();

}

jQuery(document).ready(function() {
<? if($consulta) : ?>
jQuery("[name^='salvar']").css('display','none');
jQuery("[name^='salvarcontinuar']").css('display','none');
<? endif; ?>
});
</script>
<form method="post" id="formulario">
<input type="hidden" name="requisicao" value="inserirJustificativas">
<input type="hidden" name="goto" id="goto" value="">
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2">Orientadores de Estudo</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="25%">Orienta��es</td>
	<td>
	<? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="25%">N�mero de professores Alfabetizadores da Rede (CENSO-2014)</td>
	<td><?=campo_texto('totalalfabetizadores', "N", "N", "N�mero de professores Alfabetizadores da Rede (CENSO-2014)", 8, 7, "#######", "", '', '', 0, 'id="totalalfabetizadores"', '', $totalalfabetizadores['total']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="25%">N�mero de orientadores de estudos a serem cadastrados</td>
	<td><?=campo_texto('total_a_serem_cadastrados', "N", "N", "N�mero de orientadores de estudos a serem cadastrados", 8, 7, "#######", "", '', '', 0, 'id="total_a_serem_cadastrados"', '', $totalalfabetizadores['total_orientadores_a_serem_cadastrados']); ?></td>
</tr>

<tr>
	<td colspan="2">
	<table width="100%">
	<tr>
	<td>
	<? $justificativas_respostas = carregarJustificativasRespostas(array("picid"=>$_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['picid'])); ?>	
	<? foreach($_TIPO_ORIENTADORES as $key => $value) : ?>
		<? $orientadoresestudo = carregarDadosIdentificacaoUsuario(array("picid"=>$_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['picid'],"pflcod"=>PFL_ORIENTADORESTUDO,"iustipoorientador"=>$key)); ?>
		<? if($orientadoresestudo) : ?>
		<table class="listagem" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
		<tr>
			<td class="SubTituloEsquerda" colspan="4"><?=$value ?></td>
		</tr>
		<tr>
			<td class="SubTituloCentro" width="20%">CPF</td>
			<td class="SubTituloCentro" width="40%">Nome</td>
			<td class="SubTituloCentro" width="40%">E-mail</td>
		</tr>
		<? foreach($orientadoresestudo as $oe) : ?>
		<tr>
			<td width="20%"><?=mascaraglobal($oe['iuscpf'],"###.###.###-##") ?></td>
			<td width="40%"><?=$oe['iusnome'] ?></td>
			<td width="40%"><?=$oe['iusemailprincipal'] ?></td>
		</tr>
		<? endforeach; ?>
		</table>
		<? endif; ?>
		<? if($totalalfabetizadores['total_orientadores_a_serem_cadastrados'] > 0 && ($totalalfabetizadores['total_orientadores_a_serem_cadastrados']-$totalorientadores) != count($orientadoresestudo)) : ?>
		<? $tipojustificativas = $db->carregar("SELECT tjuid as codigo, tjudesc as descricao FROM sispacto3.tipojustificativa WHERE tjustatus='A' AND tjutipo='".$key."'") ?>
		<? if($tipojustificativas) : ?>
		<table class="listagem" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
		<tr>
  
			<td colspan="3">
			<table class="tabela" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
			<tr>
				<td class="SubTituloEsquerda" colspan="2"><?=$_PERGUNTA_JUSTIFICATIVA[$key] ?></td>
			</tr>
			<tr>
				<td class="SubTituloDireita" width="25%">Tipo de Justificativa</td>
				<td>
				<?
				echo "<table width=100%>";
				foreach($tipojustificativas as $tj) :
					echo "<tr><td width=5%><input type=\"checkbox\" name=\"tjuid[{$key}][]\" value=\"".$tj['codigo']."\" ".((in_array($tj['codigo'], (($justificativas_respostas[$key]['tjuids'])?$justificativas_respostas[$key]['tjuids']:array())))?"checked":"")." ".(($consulta)?"disabled":"")."></td><td>".$tj['descricao']."</td></tr>";
				endforeach;
				echo "</table>";
				?>
				</td>
			</tr>
			<tr>
				<td class="SubTituloDireita">Coment�rios adicionais</td>
				<td><? echo campo_textarea( "joecomentario[{$key}]", 'S', (($consulta)?'N':'S'), '', '70', '4', '250', '', '', '', '', '', $justificativas_respostas[$key]['joecomentario']); ?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		<? endif; ?>
		<? endif; ?>
		<? $totalorientadores += count($orientadoresestudo); ?>
	<? endforeach; ?>
	</td>
	<td width="3%" valign="top">
	<?
	/* Barra de estado atual e a��es e Historico */
	wf_desenhaBarraNavegacao( $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['docid'], array('picid' => $_SESSION['sispacto3']['coordenadorlocal'][$_SESSION['sispacto3']['esfera']]['picid']) );
	?>
	</td>
	</tr>
	</table>
	</td>
</tr>

<tr>
	<td class="SubTituloDireita" width="25%">&nbsp;</td>
	<td>
	<?=criarBotoesNavegacao(array('url' => $_SERVER['REQUEST_URI'],'funcao' => 'salvarJustificativas')) ?>
	</td>
</tr>
</table>
</form>