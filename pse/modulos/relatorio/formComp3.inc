<?php
if ($_POST['execucao']){
	ini_set("memory_limit","2048M");
	$true = true;
	include("resultComp3.inc");
	exit;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$vpflcod = pegaPerfil($_SESSION['usucpf']);

monta_titulo('Relat�rio Componente 3', '&nbsp;');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Relat�rio</title>
        <script type="text/javascript" src="../includes/funcoes.js"></script>
        <script type="text/javascript" src="../includes/prototype.js"></script>
        <script type="text/javascript"><!--
        function gerarRelatorio(tipo_relatorio) {
                var formulario = document.formulario;

                if (tipo_relatorio == 'html') {
                    document.getElementById('tipo_relatorio').value = 'html';
                } else {
                    document.getElementById('tipo_relatorio').value = 'xls';
                }

                selectAllOptions(formulario.estado);
                selectAllOptions(formulario.municipio);

                var janela = window.open('', 'relatorio', 'width=900,height=600,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1');
                formulario.target = 'relatorio';
                formulario.submit();

                janela.focus();
            }

            /**
             * Alterar visibilidade de um campo.
             * 
             * @param string indica o campo a ser mostrado/escondido
             * @return void
             */
            function onOffCampo(campo)
            {
                var div_on = document.getElementById(campo + '_campo_on');
                var div_off = document.getElementById(campo + '_campo_off');
                var input = document.getElementById(campo + '_campo_flag');
                if (div_on.style.display == 'none')
                {
                    div_on.style.display = 'block';
                    div_off.style.display = 'none';
                    input.value = '1';
                }
                else
                {
                    div_on.style.display = 'none';
                    div_off.style.display = 'block';
                    input.value = '0';
                }
            }

--></script>
    </head>
    <body>
        <form name="formulario" id="formulario" action="" method="post">
            <input type="hidden" name="tipo_relatorio" id="tipo_relatorio" value="">
            <input type="hidden" name="execucao" id="execucao" value="listar">
            <table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="border-bottom:none;">

                <tr>
                    <td class="subtituloDireita">Exerc�cio:</td>
                    <td>
                        <?php
                        $sql = "
                                    select distinct c3anopactuacao as codigo, c3anopactuacao as descricao
				from pse.monitoracomp3";

                        $ano = $_POST['ano'];
                        $db->monta_combo('ano', $sql, 'S', '', null, null, null, null, 'N', 'ano', null, '', $ano, '', null, null);
                        ?>
                    </td>
                </tr>
                <?php
                // Filtros do relat�rio
                if ($vpflcod == SECRETARIA_ESTADUAL) {
                    $stSql = "SELECT est.estuf AS codigo,
                                     estdescricao AS descricao
                              FROM territorios.estado est
                              INNER JOIN pse.usuarioresponsabilidade usu ON est.estuf = usu.estuf  and rpustatus = 'A'
                              WHERE usucpf = '{$_SESSION['usucpf']}' ";
                              
                              $stSqlCarregados = "SELECT est.estuf AS codigo,
                                     estdescricao AS descricao
                              FROM territorios.estado est
                              INNER JOIN pse.usuarioresponsabilidade usu ON est.estuf = usu.estuf  and rpustatus = 'A'
                              WHERE usucpf = '{$_SESSION['usucpf']}'";

                } else if ($vpflcod == SECRETARIA_MUNICIPAL) {
                    $stSql = "SELECT est.estuf AS codigo,
                estdescricao AS descricao
FROM territorios.estado est
INNER JOIN territorios.municipio mun ON mun.estuf = est.estuf
INNER JOIN pse.usuarioresponsabilidade usu ON mun.muncod = usu.muncod and rpustatus = 'A'
WHERE usucpf = '{$_SESSION['usucpf']}' ";

  $stSqlCarregados = "SELECT est.estuf AS codigo,
                estdescricao AS descricao
FROM territorios.estado est
INNER JOIN territorios.municipio mun ON mun.estuf = est.estuf
INNER JOIN pse.usuarioresponsabilidade usu ON mun.muncod = usu.muncod and rpustatus = 'A'
WHERE usucpf = '{$_SESSION['usucpf']}'";
                } else {

                    $stSql = "SELECT
							estuf AS codigo,
							estdescricao AS descricao
						  FROM
						  	territorios.estado";
                    $stSqlCarregados = "";
                }
                mostrarComboPopup('Estado', 'estado', $stSql, $stSqlCarregados, 'Selecione o(s) Estado(s)');

                if ($vpflcod == SECRETARIA_ESTADUAL) {
                    $stSql = "SELECT mun.muncod AS codigo ,
                                      mun.estuf || ' - ' || replace(mundescricao, '\'', '') AS descricao
                               FROM territorios.municipio mun
                               INNER JOIN pse.usuarioresponsabilidade usu ON mun.estuf = usu.estuf and rpustatus = 'A'
                               WHERE usucpf = '{$_SESSION['usucpf']}' 
                               ORDER BY descricao";
                               
                               $stSqlCarregados = "";
//                                 $stSqlCarregados = "SELECT mun.muncod AS codigo ,
//                                      mun.estuf || ' - ' || replace(mundescricao, '\'', '') AS descricao
//                               FROM territorios.municipio mun
//                               INNER JOIN pse.usuarioresponsabilidade usu ON mun.estuf = usu.estuf and rpustatus = 'A'
//                               WHERE usucpf = '{$_SESSION['usucpf']}' 
//                               ORDER BY descricao";
                } else if ($vpflcod == SECRETARIA_MUNICIPAL){
                
                    $stSql = "SELECT mun.muncod AS codigo,
       mun.estuf || ' - ' || replace(mundescricao, '\'', '') AS descricao
FROM territorios.municipio mun
INNER JOIN pse.usuarioresponsabilidade usu ON mun.muncod = usu.muncod
WHERE usucpf = '{$_SESSION['usucpf']}'
  AND rpustatus = 'A'
ORDER BY descricao;";

                $stSqlCarregados = "SELECT mun.muncod AS codigo,
       mun.estuf || ' - ' || replace(mundescricao, '\'', '') AS descricao
FROM territorios.municipio mun
INNER JOIN pse.usuarioresponsabilidade usu ON mun.muncod = usu.muncod
WHERE usucpf = '{$_SESSION['usucpf']}'
  AND rpustatus = 'A'
ORDER BY descricao;";
                }else{

                    $stSql = "SELECT
							muncod AS codigo,
							estuf || ' - ' || replace(mundescricao, '\'', '') AS descricao
						  FROM
						  	territorios.municipio	
						  WHERE
						  	1=1
						  ORDER BY
						  	descricao";
                }
                $where = array(
                    array(
                        "codigo" => "estuf",
                        "descricao" => "Estado <b style='size: 8px;'>(sigla)</b>",
                        "numeric" => false
                    )
                );
                mostrarComboPopup('Munic�pio', 'municipio', $stSql, $stSqlCarregados, 'Selecione o(s) Munic�pio(s)', $where);
                ?>

                <tr>
                    <td align="center" colspan="2">
                        <input type="button" name="GerarRelatorio" value="Gerar Relat�rio HTML" onclick="javascript:gerarRelatorio('html');"/>
                        <input type="button" name="GerarRelatorio" value="Gerar Relat�rio XLS" onclick="javascript:gerarRelatorio('xls');"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
