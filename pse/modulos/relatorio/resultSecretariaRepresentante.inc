<?

if ( $_REQUEST['filtrosession'] ){
	$filtroSession = $_REQUEST['filtrosession'];
}

if ($_POST['agrupador']){
	header( 'Content-Type: text/html; charset=iso-8859-1' ); 
}
ini_set("memory_limit","250M");
set_time_limit(0);
?>
<html>
	<head>
		<script type="text/javascript" src="../includes/funcoes.js"></script>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
	</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">	
<?php
include APPRAIZ. 'includes/classes/relatorio.class.inc';

$sql   = monta_sql();
$dados = $db->carregar($sql);
$agrup = monta_agp();
$col   = monta_coluna();
$r = new montaRelatorio();
$r->setAgrupador($agrup, $dados); 
$r->setColuna($col);
$r->setBrasao(true);
$r->setTotNivel(false);
$r->setEspandir(false);
$r->setMonstrarTolizadorNivel(false);
$r->setTotalizador(true);
$r->setTolizadorLinha(false);

//$r->setBrasao(true);
echo $r->getRelatorio();
?>
</body>
</html>
<?php 
function monta_sql(){
	global $filtroSession;
	
	extract($_POST);
	
	$select = array();
	$from	= array();
	

	if( $estado[0] && ( $estado_campo_flag || $estado_campo_flag == '1' )){
		$where[0] = " AND m.estuf " . (( $estado_campo_excludente == null || $estado_campo_excludente == '0') ? ' IN ' : ' NOT IN ') . " ('" . implode( "','", $estado ) . "') ";		
	}
	if( $municipio[0] && ( $municipio_campo_flag || $municipio_campo_flag == '1' )){
		$where[1] = " AND m.muncod " . (( $municipio_campo_excludente == null || $municipio_campo_excludente == '0') ? ' IN ' : ' NOT IN ') . " ('" . implode( "','", $municipio ) . "') ";		
	}
	if( $portaria[0] && ( $portaria_campo_flag ||$portaria_campo_flag == '1' )){
		$where[2] = " AND port.porano " . (( $portaria_campo_excludente == null || $portaria_campo_excludente == '0') ? ' IN ' : ' NOT IN ') . " ('" . implode( "','", $portaria ) . "') ";		
	}
	if( $esfera[0] != '' && ( $esfera_campo_flag ||$esfera_campo_flag == '1' )){
		$where[3] = " AND p.empflagestmun " . (( $esfera_campo_excludente == null || $esfera_campo_excludente == '0') ? ' IN ' : ' NOT IN ') . " ('" . implode( "','", $esfera ) . "') ";		
	}
	if( $exer[0] && ( $exer_campo_flag || $exer_campo_flag == '1' )){
		$where[4] = " AND p.empano " . (( $exer_campo_excludente == null || $exer_campo_excludente == '0') ? ' IN ' : ' NOT IN ') . " ('" . implode( "','", $exer ) . "') ";		
	}
	
	$sql = "SELECT
					distinct(p.empid), m.estuf as estado, m.mundescricao as municipio,s.semsecretariaestadualmunicipal,
					case when p.empflagestmun = 'm' then 'Municipal' else 'Estadual' end as esfera,
					case when s.semsecretariaestadualmunicipal = 'E' then 'Estadual' when s.semsecretariaestadualmunicipal = 'M' then 'Municipal' end as tipo,
					s.semnomerepresecretariasaude as secsaude,
					s.sememailrepresecretariasaude as secsaudeemail,
					s.semtelefonerepresecretariasaude as secsaudetel,
					s.semcargofuncaorepresecretariasa as secsaudecargo,
					s.semnomerepresecretariaeducacao as seceducacao,
					s.sememailrepresecretariaeducacao as seceducacaoemail,
					s.semtelefonerepresecretariaeduca as seceducacaotel,
					s.semcargofuncaorepresecretariaed as seceducacaocargo
				FROM 
					pse.portariapse port
				INNER JOIN pse.estadomunicipiopse p ON port.pormunicipio = p.muncod
				INNER JOIN pse.secretariaestmun s ON p.empid = s.empid
				INNER JOIN territorios.municipio m ON m.muncod = p.muncod
				--INNER JOIN entidade.endereco ende ON ende.muncod = m.muncod
				--INNER JOIN pse.escolapse ep ON ep.entid = ende.entid
				--INNER JOIN pse.pseanoescolamun ano ON ano.empid = p.empid
				WHERE
					m.estuf <> ''
					AND s.semsecretariaestadualmunicipal <> 'x'
					".$where[0]."
					".$where[1]."
					".$where[2]."
					".$where[3]."
					".$where[4]."
				ORDER BY
					m.estuf, m.mundescricao";
//	ver ($sql);
	return $sql;
}

function monta_agp(){
	$agrupador = $_POST['agrupador'];
		
	$agp = array(
				"agrupador" => array(),
				"agrupadoColuna" => array(
											"tipo",
											"secsaude",
											"secsaudeemail",
											"secsaudetel",
											"secsaudecargo",
											"seceducacao",
											"seceducacaoemail",
											"seceducacaotel",
											"seceducacaocargo"
											/*"projetado",	
									   		"autorizado", 
 									   		"provimento",
 									   		"publicado",
 									   		"homologado",
											"lepvlrprovefetivados",
											"provimentosnaoefetivados",
											"provimentopendencia",
										    "homocolor"
										    */
										  )	  
				);
	
	$count = 1;
		$i = 0;
		foreach( $_POST['esfera'] as $esfera => $valor ){
			if( $valor == 'm' ){
				$vari = "PSE - Programa Sa�de na Escola - Rela��o de Contato/Representante- Secretarias Municipais<br>";
				$i++;
			} elseif( $valor == 'e' ){
				$vari = "PSE - Programa Sa�de na Escola - Rela��o de Contato/Representante- Secretarias Estaduais<br>";
				$i++;
			}
		}
		
		if( $i > 1 || $i == 0 ){
			$vari = "PSE - Programa Sa�de na Escola - Rela��o de Contato/Representante- Secretarias Estaduais e Municipais<br>";	
		}
	foreach ($agrupador as $val):
		if($count == 1){
			$var = $vari;
		} else {
			$var = "";		
		}
		switch ($val) {
			case 'estado':
				array_push($agp['agrupador'], array(
													"campo" => "estado",
											  		"label" => "$var Estado")										
									   				);				
		   		continue;
		    break;
		    case 'municipio':
				array_push($agp['agrupador'], array(
													"campo" => "municipio",
											  		"label" => "$var Munic�pio")										
									   				);				
		   		continue;
		    break;
		}
		$count++;
	endforeach;
	return $agp;
}

function monta_coluna(){
	
	$coluna    = array(
						array(
							  "campo" => "tipo",
					   		  "label" => "Tipo",
						),
						array(
							  "campo" => "secsaude",
					   		  "label" => "Representante da Secretaria de Sa�de",
						),
						array(
							  "campo" => "secsaudeemail",
					   		  "label" => "E-mail do representante da Secretaria de Sa�de",
						),
						array(
							  "campo" => "secsaudetel",
					   		  "label" => "Telefone do representante da Secretaria de Sa�de",
							  "type"  => "string"
						),
						array(
							  "campo" => "secsaudecargo",
					   		  "label" => "Cargo do representante da Secretaria de Sa�de",
							  "type"  => "string"
						),
						array(
							  "campo" => "seceducacao",
					   		  "label" => "Representante da Secretaria de Educa��o",
						),
						array(
							  "campo" => "seceducacaoemail",
					   		  "label" => "E-mail do representante da Secretaria de Educa��o",
						),
						array(
							  "campo" => "seceducacaotel",
					   		  "label" => "Telefone do representante da Secretaria de Educa��o",
							  "type"  => "string"
						),
						array(
							  "campo" => "seceducacaocargo",
					   		  "label" => "Cargo do representante da Secretaria de Educa��o",
							  "type"  => "string"
						)
					);
		/*			
	if ( $permissao ){				
		array_push($coluna, array(
								  "campo" 	 => "homologado",
						   		  "label" 	 => "Concursos <br/>Homologados <br/>(D)",
								  "type"  	 => "numeric",
								  "html"  	 => "<span style='color:{color}'>{homologado}</span>",
								  "php" 	 => array(
									  					"expressao" => "{homologado} > {autorizado}",
														"var" 		=> "color",
														"true"	    => "red",
														"false"     => "#0066CC",
									  				 )	
							)
				  );			
	}else{
		array_push($coluna, array(
								  "campo" 	 => "homologado",
						   		  "label" 	 => "Concursos <br/>Homologados <br/>(D)",
								  "type"  	 => "numeric"
								 )
				  );			

	}
	
	array_push($coluna,array(
							  "campo" => "provimento",
					   		  "label" => "Provimentos <br/>Autorizados <br/>(E)",
							  "type"  => "numeric"
						),
						array(
							  "campo" => "lepvlrprovefetivados",
					   		  "label" => "Provimentos <br/>Efetivados <br/>(F)",
							  "type"  => "numeric"
						),
						array(
							  "campo" => "provimentosnaoefetivados",
					   		  "label" => "Provimentos <br/>N�o Efetivados <br/>(G)=(E)-(F)",
							  "type"  => "numeric"
						)
				);	

				
	if ( $permissao ){				
		array_push($coluna, array(
									  "campo" => "provimentopendencia",
							   		  "label" => "Aut. Provimento <br/>Pendente <br/>(H)=(D)-(E)",
									  "type"  => "numeric",
									  "html"  => "{valorcampo}",
									  "php"   => array(
									  					"expressao" => "({homologado} - {provimento} >= 0) || ({provimentopendencia} > 0)",
														"type"		=> "string",
									  					"var"       => "valorcampo",
														"true"		=> "{provimentopendencia}",
														"false"		=> "<span style='color:red;'>-</span>"					
									  				   )
								)
				  );			
	}else{
		array_push($coluna, array(
									  "campo" => "provimentopendencia",
							   		  "label" => "Aut. Provimento <br/>Pendente <br/>(H)=(D)-(E)",
									  "type"  => "numeric"		
								  )
				  );			

	}		*/	

	return $coluna;			  	
	
}
?>
