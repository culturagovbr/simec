<?php

if ($_REQUEST['exibirRelatorio']=='SIM'){
	ini_set("memory_limit","256M");
	include("resultComponentesGrafico.inc");
	exit; 
}

include APPRAIZ . 'includes/cabecalho.inc';
include "resultComponentesGraficoBack.php";

print '<br/>';

monta_titulo( 'Relat�rio Componentes PSE - Gr�fico', '&nbsp;' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Relat�rio</title>
<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/prototype.js"></script>
<script type="text/javascript">

function gerarRelatorio(){
	inicializarCamposFormulario();
	var janela = window.open( '', 'relatorio', 'width='+screen.width+',height='+screen.height+',status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
	document.formulario.target = 'relatorio';	
	document.formulario.submit();
	janela.focus();
}

function inicializarCamposFormulario() {
	selectAllOptions( document.formulario.estado );
	selectAllOptions( document.formulario.municipio );
	selectAllOptions( document.formulario.comp1 );
	selectAllOptions( document.formulario.comp2 );
	selectAllOptions( document.formulario.comp3 );
	selectAllOptions( document.formulario.comp4 );	
}


var div_on;
var div_off;
var input;

/**
 * Uso chamado pela fun��o 'mostrarComboPopup'. 
 */
function onOffCampo(campo) {
	recuperarCamposEspecificos(campo); 
	settarCamposDivComValorOpostoAoAtual();
}

function recuperarCamposEspecificos(campo) {
	div_on  = document.getElementById( campo + '_campo_on' );
	div_off = document.getElementById( campo + '_campo_off' );
	input   = document.getElementById( campo + '_campo_flag' );	
}

function settarCamposDivComValorOpostoAoAtual() {
	var divOnEstaDesativo = div_on.style.display == 'none'; 
	if ( divOnEstaDesativo ) {
		div_on.style.display  = 'block';
		div_off.style.display = 'none';
		input.value = '1';
	}
	else {
		div_on.style.display = 'none';
		div_off.style.display = 'block';
		input.value = '0';
	}
}

</script>
</head>
<body>
<form name="formulario" id="formulario" action="" method="post">
<input type="hidden" name="exibirRelatorio" value="SIM" />	
<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="border-bottom:none;">
	<tr>
		<td class="SubTituloDireita" valign="top">Agrupadores</td>
		<td>
			Brasil
			<div style="padding-left:0px;">
				<img src="../imagens/seta_filho.gif" align="absmiddle"/> Estado 
			</div>
			<div style="padding-left:20px;">
				<img src="../imagens/seta_filho.gif" align="absmiddle"/> Munic�pio 
			</div>
			<div style="padding-left:40px;">
				<img src="../imagens/seta_filho.gif" align="absmiddle"/> Componente/Subcomponente 
			</div>
			<div style="padding-left:60px;">
				<img src="../imagens/seta_filho.gif" align="absmiddle"/> A��o
			</div>
			<div style="padding-left:80px;">
				<img src="../imagens/seta_filho.gif" align="absmiddle"/> Quantidade de a��es
			</div>															
		</td>
	</tr>
		<?php
				//----- Estados -----------------------------------------------------------------------------------------------------------
				mostrarComboPopup( 'Estado', 'estado', obterSqlComboBoxEstados(), "", 'Selecione o(s) Estado(s)' ); 
				
				//----- Munic�pios --------------------------------------------------------------------------------------------------------
				$elementosArrayWhereEstadoUfParaMunicipios = array(	array(	"codigo" 	=> "estuf",
																			"descricao" => "Estado <b style='size: 8px;'>(sigla)</b>",
																			"numeric"	=> false
																	   	  ) );
				mostrarComboPopup( 'Munic�pio', 'municipio' ,  obterSqlComboBoxMunicipios()      , "", 'Selecione o(s) Munic�pio(s)', $elementosArrayWhereEstadoUfParaMunicipios );    
			
				//----- Componente I ------------------------------------------------------------------------------------------------------
				mostrarComboPopup( 'Componente I'  , 'comp1',  obterSqlComboBoxComponenteUm()    , "", 'Selecione a avalia��o da condi��o de sa�de dos estudantes referente ao Componente I' ); 
				
				//----- Componente II -----------------------------------------------------------------------------------------------------
				mostrarComboPopup( 'Componente II' , 'comp2',  obterSqlComboBoxComponenteDois()  , "", 'Selecione a promo��o da sa�de e preven��o referente ao Componente II' ); 
				
				//----- Componente III ----------------------------------------------------------------------------------------------------
				mostrarComboPopup( 'Componente III', 'comp3',  obterSqlComboBoxComponenteTres()  , "", 'Selecione a educa��o permanente e capacita��o dos profissionais da educa��o e da sa�de de jovens para o pse referente ao Componente III' ); 
			
				//----- Componente IV -----------------------------------------------------------------------------------------------------
				mostrarComboPopup( 'Componente IV' , 'comp4',  obterSqlComboBoxComponenteQuatro(), "", 'Selecione o monitoramento e avalia��o da sa�de dos estudantes referente ao Componente IV' );
		?>
	<tr>
		<td class="SubTituloDireita" align="right">
			Cada gr�fico ser� representado por</td>
		<td>
		<?php		
				$sqlRepresentacaoMaiorCadaGrafico = 
					"
						select 1 as CODIGO, 'Brasil' as DESCRICAO 
						union all
						select 2 as CODIGO, 'Estado' as DESCRICAO 
						union all
						select 3 as CODIGO, 'Munic�pio' as DESCRICAO 
						union all
						select 4 as CODIGO, 'Componente/Subcomponente' as DESCRICAO
						union all
						select 5, 'A��o' as DESCRICAO 					
					";
				$db->monta_combo( "nivelRepresentacaoMaior", $sqlRepresentacaoMaiorCadaGrafico, 'S', '', '', '', '', '', '210' );
		?>
		</td>
	</tr>		
	<tr>
		<td class="SubTituloDireita" align="right" width="30%">
			Cada por��o de cada gr�fico conter� somat�rios de valores por</td>
		<td>
		<?php		
				$sqlPorcaoSomatorioCadaGrafico = 
					"
						select 2 as CODIGO, 'Estado' as DESCRICAO 
						union all
						select 3 as CODIGO, 'Munic�pio' as DESCRICAO 
						union all
						select 4 as CODIGO, 'Componente/Subcomponente' as DESCRICAO
						union all
						select 5, 'A��o' as DESCRICAO
						union all
						select 6 as CODIGO, 'Quantidade de a��es' as DESCRICAO 
					";
				$db->monta_combo( "nivelPorcaoSomatorio", $sqlPorcaoSomatorioCadaGrafico, 'S', '', 'filtro', '', '', '210' );
		?>
		</td>
	</tr>			
	<tr>
		<td align="center" colspan="2">
			<input type="button" name="Gerar Relat�rio" value="Gerar Relat�rio" onclick="javascript:gerarRelatorio();"/>
		</td>
	</tr>
</table>
</form>
</body>
</html>
