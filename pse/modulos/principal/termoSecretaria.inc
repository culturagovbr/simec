<?php
include  APPRAIZ . 'pse/modulos/principal/permissao.inc';
$muncod = $_SESSION['pse']['muncod'];
//echo $muncod;
$sql ="	SELECT a.empid,c.mundescricao, a.semnomesecretarioeducacao, d.muncodcapital
		FROM pse.secretariaestmun a
		INNER JOIN
			pse.estadomunicipiopse b ON b.empid = a.empid	
		INNER JOIN
			territorios.municipio c ON c.muncod = b.muncod
		LEFT JOIN
			territorios.estado d ON d.muncodcapital = b.muncod
		WHERE c.muncod = '$muncod' AND a.semsecretariaestadualmunicipal='M'";
$dados = $db->pegalinha($sql);

$govmunicipal		= ($dados['mundescricao']==''?'______________________________________':$dados['mundescricao']);
$nomesec			= ($dados['semnomesecretarioeducacao']==''?'______________________________________':$dados['semnomesecretarioeducacao']);
$capital			= ($dados['muncodcapital']==''?0:$dados['muncodcapital']);
$cnpj				= "o n� ____________________";
$paragrafo = "O Secret�rio de Educa��o indica, abaixo, o nome de seu respectivo representante 
interlocutor do PSE e encaminha este Termo de Ades�o ao Minist�rio da Educa��o, ap�s o 
preenchimento do Cadastro Estado/Munic�pio, no referido s�tio eletr�nico. E, por estar assim de acordo com as disposi��es 
deste, o Secret�rio Municipal de Educa��o firma o presente Termo de Ades�o ao PSE. ";

$assinatura1 = "______________________________<br>$nomesec<br>Secret�rio Municipal de Educa��o";

if($capital>0){
	$sql ="	SELECT semnomesecretarioeducacao 
			FROM pse.secretariaestmun
			WHERE empid = $dados[empid]";
	$dados = $db->pegalinha($sql);

	$nomesec = ($dados['semnomesecretarioeducacao']==''?'________________________':$dados['semnomesecretarioeducacao']);
	$secestadual = " e da <b>Secretaria Estadual de Educa��o</b> ";
	$cnpj = "os n�s ____________________ e ____________________, respectivamente";
	$paragrafo = "Os Secret�rios de Educa��o indicam, abaixo, o nome de seus respectivos representantes 
	interlocutores do PSE e encaminham este Termo de Ades�o ao Minist�rio da Educa��o, ap�s o 
	preenchimento do Cadastro Estado/Munic�pio, no referido s�tio eletr�nico. E, por estarem assim de acordo com as disposi��es 
	deste, o Secret�rio Municipal de Educa��o e o Secret�rio Estadual de Educa��o firmam o presente Termo de Ades�o ao PSE.";
	$assinatura2 = "<td align='center'><b>______________________________<br>$nomesec<br>Secret�rio Estadual de Educa��o</b></td>";
}


$secretario		= ($capital==0?'Municipal':'Estadual');
$estadocivil	= '_______________';
$rg				= '_______________';
$exprg			= '_______________';
$cpf			= '_______________';
		
		
		
?>
<STYLE TYPE="text/css">
.quebra {
    page-break-after: always;
}
</STYLE>
<p align="right"><font size="1"><a href="#" onclick="self.print();">Imprimir</a></font></p>
<body onload="self.print();">
<table align="center" cellspacing="1" cellpadding="4">
	<tr>
		<td align="center"><img src="../imagens/brasao.gif" border="0" width="60" height="60"></td>
	</tr>
	<tr>
		<td align="center">
			<b>Minist�rio da Educa��o<br>
			Secretaria de Educa��o Continuada, Alfabetiza��o e Diversidade<br>
			Diretoria de educa��o Integral, Direitos Humanos e Cidadania</b>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br><br>
			<b>TERMO DE ADES�O</b>
			<br><br>
		</td>
	</tr>
</table>


<p align="justify">

O <b>Governo Municipal de <?=$govmunicipal;?></b>, por interm�dio de sua <b>Secretaria Municipal de Educa��o</b> 
<?=$secestadual;?>, inscritas no CNPJ sob <?=$cnpj;?>, 
neste ato representado por seu Secret�rio <?=$secretario;?> de Educa��o, <b><?=$nomesec;?></b>, <?=$estadocivil;?>, portador da 
carteira de identidade n� <?=$rg;?>, expedida por <?=$exprg;?>, e inscritos no CPF sob o 
n�. <?=$cpf;?>, considerando o que disp�e a Constitui��o Federal, as Leis n�. 8.080/90 e n�. 8.142/90 
da Sa�de e a Lei n�. 9.394/96 da Educa��o celebra o presente Termo de Ades�o ao Programa Sa�de na 
Escola, que se efetivar� por meio de gest�o intersetorial entre Educa��o e Sa�de no �mbito federal, 
estadual, municipal e do Distrito Federal.
<br><br>
As responsabilidades da gest�o municipal assumidas por meio deste Termo de Ades�o est�o expl�citas no 
Projeto do PSE Municipal e no Cadastro Estado/Munic�pio, este localizado no s�tio eletr�nico http://simec.mec.gov.br. 
<br><br>
<?=$paragrafo;?>
<br><br>
<center>
<?=$govmunicipal;?>, _____ de _____________________ de 20___.
<br><br><br><br>
<table border='0' width="100%">
	<tr>
		<td align='center'><b><?=$assinatura1;?></b></td>
		<?=$assinatura2;?>
	</tr>
</table>
</center>
<br><br>
<table border='0' width="100%">
	<tr>
		<td colspan="2"><b><u>Representante(s):</u></b></td>
	</tr>	
	<tr>
		<td>
			<b>Secretaria Municipal de Educa��o</b><br>
			Nome:<br>
			e-mail:<br>
			Telefone(s):
		</td>	
		<?php if($capital>0){?>
		<td>
		<b>Secretaria Estadual de Educa��o</b><br>
		Nome:<br>
		e-mail:<br>
		Telefone(s):
		</td>
		<?php }?>
	</tr>
</table>	
</p>    


</body>