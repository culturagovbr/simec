<?php
include  APPRAIZ . 'pse/modulos/principal/permissao.inc';
VerSessao();
$entid = $_SESSION['pse']['entid'];

$sql ="	SELECT	ent.entid,entd.entcodent, ent.entnome, ende.estuf, mun.mundescricao,ende.endlog,ende.endcom,ende.endbai,
				ent.entnumcpfcnpj,professor.entid as identf, professor.entnome as dir, professor.entnumrg as profrg, professor.entnumcpfcnpj as profcpf,
				professor.entorgaoexpedidor as exprg  
		FROM entidade.entidade ent				
		LEFT JOIN 
			entidade.funcaoentidade feEscola on feEscola.entid = ent.entid
		LEFT JOIN 
			entidade.funentassoc assocprofessores on assocprofessores.entid = ent.entid
		LEFT JOIN 
			entidade.funcaoentidade feProfessor on feProfessor.fueid = assocprofessores.fueid	
		LEFT JOIN 
			entidade.entidade professor on professor.entid = feProfessor.entid
		LEFT JOIN
			entidade.endereco ende ON ende.entid = ent.entid
		INNER JOIN
			entidade.entidade2 ent2 ON ent2.entid = ent.entid
		INNER JOIN
			entidade.entidadedetalhe entd ON entd.entid = ent.entid		
		INNER JOIN
			territorios.municipio mun ON mun.muncod = ende.muncod
		INNER JOIN
			territorios.estado est ON est.estuf = mun.estuf
		WHERE	ent.entstatus='A' AND 
				ent2.funid = 3
				and ent.entid = '$entid'";
				
		$dados	= $db->pegalinha($sql);
		
		$escola			= ($dados['entnome']==''?'______________________________________':$dados['entnome']);
		$diretor		= ($dados['dir']==''?'______________________________________':$dados['dir']);
		$cnpj			= ($dados['entnumcpfcnpj']==''?'______________________':$dados['entnumcpfcnpj']);
		$inep			= ($dados['entcodent']==''?'_______________':$dados['entcodent']);
		$conselho		= ($dados['conselho']==''?'___________________________________':$dados['conselho']);
		$rg				= ($dados['profrg']==''?'_______________':$dados['profrg']);
		$exprg			= ($dados['exprg']==''?'_______________':$dados['exprg']);
		$cpf			= ($dados['profcpf']==''?'__________________':$dados['profcpf']);
		$endconselho	= ($dados['endconselho']==''?'______________________________________________________':$dados['endconselho']);
		
		
		
?>
<STYLE TYPE="text/css">
.quebra {
    page-break-after: always;
}
</STYLE>
<p align="right"><a href="#" onclick="self.print();">Imprimir</a></p>
<body onload="self.print();">
<table align="center" cellspacing="1" cellpadding="4">
	<tr>
		<td align="center"><img src="../imagens/brasao.gif" border="0" width="100" height="100"></td>
	</tr>
	<tr>
		<td align="center">
			<b>Minist�rio da Educa��o<br>
			Secretaria de Educa��o Continuada, Alfabetiza��o e Diversidade<br>
			Diretoria de educa��o Integral, Direitos Humanos e Cidadania</b>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br><br><br><br>
			<b>TERMO DE COMPROMISSO</b>
			<br><br><br><br>
		</td>
	</tr>
</table>
<br><br><br>
<table align="center" cellspacing="10" cellpadding="5">
<tr>
<td>
<p align="justify">

Entre <br>
<b>Minist�rio da Educa��o</b><br>
<b>Secretaria de Educa��o Continuada, Alfabetiza��o e Diversidade � SECAD/MEC</b>
<br><br>
Localizado na<br> 
Esplanada dos Minist�rios<br> 
SECAD/MEC<br>
Bloco L � 6� andar<br>
70070-914 Bras�lia, Brasil
<br><br>
E
<br><br>
Escola <b><?=$escola;?>/C�digo INEP n� <?=$inep;?></b>, inscritas no CNPJ sob n� <?=$cnpj;?>,
neste ato representada por seu(sua) Diretor(a)<b><?=$diretor;?></b>, portador(a) 
da carteira de identidade n� <?=$rg;?>, expedida por <?=$exprg;?>, e inscrito(a) no CPF sob o n� <?=$cpf;?>,
Em co-responsabilidade com seu Conselho Escolar <b><?=$conselho;?></b>, 
localizado na <?=$endconselho;?>.
<br><br> 
Em concord�ncia com o estabelecido na Portaria n�......../2009 de ades�o ao Programa Sa�de na Escola relativamente 
ao apoio institucional � Escola <b><?=$escola;?></b> e ao Conselho Escolar <b><?=$conselho;?></b>, para 
a implementa��o do Programa Sa�de na Escola, em conformidade com o objetivo maior de contribuir para a forma��o 
integral dos estudantes por meio de a��es de promo��o, preven��o e aten��o � sa�de, no �mbito escolar, com vistas 
ao enfrentamento das vulnerabilidades que comprometem o pleno desenvolvimento de crian�as e jovens da rede p�blica 
de ensino � celebra o presente Termo de Compromisso com o Minist�rio da Educa��o, por meio do Programa Sa�de 
na Escola, o qual se efetivar� por meio de gest�o intersetorial entre Educa��o e Sa�de, nos seguintes termos:
<br><br> 
<br><br>

<br class="quebra">

<b>Objeto</b>
<br><br> 
1.	Apoio Institucional, por meio do aporte t�cnico-formativo e de fortalecimento da articula��o e interlocu��o 
entre a escola e os profissionais das Unidades B�sicas de Sa�de/ESF - Unidade Local Integrada (ULI), na constru��o 
da interface educa��o e sa�de, assim como, por meio da distribui��o de materiais cl�nicos e did�tico-pedag�gicos 
� Escola <b><?=$escola;?></b>, por interm�dio de seu representante legal, o(a) Diretor(a)<b><?=$diretor;?></b>, 
<br><br>
<br>
<b>Dos Compromissos das Partes</b>
<br><br> 
<b>Artigo I</b>
<br><br>
O Minist�rio da Educa��o, por interm�dio da Secretaria de Educa��o Continuada, Alfabetiza��o e Diversidade � 
SECAD/MEC, no �mbito do Programa Sa�de na Escola � se compromete a fornecer aporte t�cnico-formativo e de fortalecimento da articula��o e constru��o da interface educa��o e sa�de, no �mbito escolar, assim como distribuir materiais cl�nicos e did�tico-pedag�gicos para a Institui��o beneficiada mediante a devolu��o do presente Termo de Compromisso, assinado por seu(s) representante(s) legal(is). 
<br><br> 
<b>Artigo II</b>
<br><br>
Em uma perspectiva intersetorial, a Escola <b><?=$escola;?>/C�digo INEP n� <?=$inep;?>)</b> se compromete a atuar no 
Territ�rio de Responsabilidade, definido segundo a �rea de abrang�ncia da Estrat�gia Sa�de da Fam�lia, ou no 
planejamento integrado com a Unidade B�sica de Sa�de de refer�ncia; tornando poss�vel a aproxima��o entre os 
sistemas da sa�de e da educa��o favorecendo, por meio da elabora��o de planejamento local integrado (Agenda 
Educa��o e Sa�de), estrat�gias entre a comunidade escolar, a partir de seu projeto pol�tico-pedag�gico, e a 
Unidade B�sica de Sa�de/ESF. 
<br><br>
<b>Artigo III</b>
<br><br>
O planejamento das a��es do PSE dever� considerar: a gest�o democr�tica, o contexto escolar e social; o diagn�stico 
local e capacidade operativa em sa�de do escolar; assim como a parceria com outras �reas como meio ambiente, 
esporte e lazer, cultura, outros.
<br><br>
<b>Artigo IV</b>
<br><br>
As a��es a serem implementadas, por meio da gest�o intersetorial local entre educa��o e sa�de, dever�o contemplar 
os cinco componentes que integram o programa, conforme planejamento local, quais sejam: Avalia��o das Condi��es 
de Sa�de dos estudantes de escolas p�blicas; Promo��o da Sa�de e Preven��o; Educa��o Permanente e Capacita��o 
dos Profissionais da Educa��o e da Sa�de e de Jovens; Monitoramento e Avalia��o da Sa�de dos Estudantes e, 
por fim, o componente de Monitoramento e Avalia��o do Programa. 
<br><br>
<br class="quebra">
<b>Artigo V</b>
<br><br>
� parte integrante deste Termo de Compromisso, a ser realizado pelas escolas que integram o programa e ora 
assumem o compromisso de sua implementa��o, o preenchimento no s�tio eletr�nico http://simec.mec.gov.br, dos 
seguintes formul�rios de acompanhamento do PSE:
<br><br> 
1.	Cadastro Escola (anual);<br>
2.	M�dulo Unidade Local Integrada � ULI (acompanhamento integrado entre Escola/ESF - semestral)<br>
<br><br>
<b>Artigo VI</b>
<br><br>
A escola se compromete a convidar para participa��o no planejamento e implementa��o do Programa Sa�de na 
Escola seu respectivo Conselho Escolar e o Conselho da Crian�a e do Adolescente local, quando houver.
<br><br>
<br>
Em nome do Minist�rio da Educa��o.
<br><br><br><br>   
_________________________________________________________<br> 
<b>Sr. Andr� L�zaro<br>
Secret�rio de Educa��o Continuada, Alfabetiza��o e Diversidade</b><br> 
<br><br><br><br>

Em nome da Escola<br>
<?=$escola;?>/C�digo INEP n� <?=$inep;?>)<br>
<br><br><br><br>
_________________________________________________________<br> 
Assinatura do(a) Diretor(a)<br>
<br><br><br><br>

Em nome do Conselho Escolar <?=$conselho;?><br>
<br><br><br><br>
_________________________________________________________<br> 
Assinatura do(a) Presidente do Conselho<br>
<br><br><br><br>





______________________/ _____,    ____ de _______________ de 20____.
<br>
(Local/UF)

</p>    
</td>
</tr>
</table>         


</body>