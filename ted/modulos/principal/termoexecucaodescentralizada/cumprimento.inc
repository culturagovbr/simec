<?php
//Cabe�alho do sistema
include  APPRAIZ."includes/cabecalho.inc";

//Verifica se existe tcpid setado, sen�o, efetua redirecionamento
Ted_Utils_Model::capturaTcpid(true);

//Declara��o de Objetos
$gerenciadorAba = new Ted_Model_GerenciadorDeAbas();
$previsao = new Ted_Form_Previsao();
$cumprimento = new Ted_Form_Cumprimento();

//Setando o tcpid da sess�o no formul�rio
$previsao->getElement('tcpid')->setValue(Ted_Utils_Model::capturaTcpid());

?>
<section class="col-md-12">
	<ol class="breadcrumb">
		<li><a href="ted.php?modulo=inicio&acao=C"><?=$_SESSION['sisdsc']; ?></a></li>
		<li class="">Cadastro de Termo de Coopera��o</li>
		<li class="active">Cumprimento</li>
	</ol>
	<?php $gerenciadorAba->capturaAbaAtiva();?>
	<nav class="col-md-2">
		<?php echo $gerenciadorAba->apresentaAbas(); ?>
		<?php echo Ted_Utils_Model::montaInformacaoTermo();?>		
	</nav>	
	<section class="col-md-10">
		<section class="well">
			<h4 class="text-center">Relat�rio de Cumprimento</h4>
		</section>	
		<br>			
		<section>
			<?php echo $cumprimento->showForm(); ?>
		</section>
	</section>			
</section>