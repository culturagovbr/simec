<form class="well form-horizontal" name="<?=$this->element->getName(); ?>" id="<?=$this->element->getId(); ?>" action="<?= $this->element->getAction(); ?>" method="<?= $this->element->getMethod(); ?>" role="form">

	<?= $this->element->tcpid; ?>
    <section class="well">
		<h4 class="text-center">Dados da Entidade Proponente</h4>
	</section>	
	<div class="form-group">        
        <label class="control-label col-md-2" for="">CNPJ:</label>        
        <div class="col-md-10">
        </div>
    </div>
    <div id="fndeblocked" class="form-group">
	   	<label class="control-label col-md-2" for="">Nome da Entidade:</label>
	   	<div class="col-md-10">
	   	</div>
	</div>
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Endere�o:</label>
	    <div class="col-md-10">
		</div>
	</div>	    
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">UF:</label>
	    <div class="col-md-10">
		</div>
	</div>	
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Munic�pio:</label>
	    <div class="col-md-10">
		</div>
	</div>		
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">CEP:</label>
	    <div class="col-md-10">
		</div>
	</div>
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Telefone com DDD:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">C�digo da UO:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">C�digo da UG:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">C�digo da Gest�o:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Nome do Respons�vel:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">CPF do Respons�vel:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">SIAPE do Respons�vel:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Identidade do Respons�vel:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Data Emiss�o:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Org�o Expedidor:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Cargo:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">E-mail do Respons�vel:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">N� da Portaria ou Decreto de Nomea��o:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
	<div class="form-group ">        
		<label class="control-label col-md-2" for="">Data de Publica��o:</label>
	    <div class="col-md-10">
		</div>
	</div>	 
													 			 		   
	<div class="well">
		<h4 class="text-center">Dados do Objeto da Descentraliza��o do Cr�dito</h4>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Nota(s) de Cr�dito</label>
		<div class="col-md-10">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Execuca��o do Objeto</label>
		<div class="col-md-10">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Atividades Previstas</label>
		<div class="col-md-10">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Meta Prevista</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Atividades Executadas</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Meta Executada</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Dificuldades Encontradas na Execuca��o da Descentraliza��o</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Medidas Adotadas para Sanar as Dificuldades de Modo a Assegurar o Cumprimento do Objeto</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Coment�rios Adicionais</label>
		<div class="col-md-10"></div>
	</div>
	<div class="well">
		<h4 class="text-center">Detalhamento do Cr�dito Or�ament�rio Recebido</h4>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Valor Recebido (R$ 1,00)</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Valor Utilizado (R$ 1,00)</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Valor Devolvido (R$ 1,00)</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">NC de Devolu��o</label>
		<div class="col-md-10"></div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2" for="">Anexar arquivo</label>
		<div class="col-md-10"></div>
	</div>
	
    <hr />
    <div class="form-group">
    	<div class="col-md-offset-2">
    		<button type="submit" class="btn btn-primary" name="submit" id="submit">Gravar</button>
    	</div>
    </div>
    
</form>