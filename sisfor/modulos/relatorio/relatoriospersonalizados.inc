<?php

include_once "_funcoes.php";
include_once "_constantes.php";

if($_REQUEST['requisicao']) {
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}


$perfis = pegaPerfilGeral();

if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - SISFOR", "" );

$relatorios = array(//0=>array('codigo'=>'relatpersonal_erroscadastrossgb.inc','descricao'=>'Relat�rio de erros no cadastro de bolsistas no SGB'),
						//1=>array('codigo'=>'relatpersonal_aceitacaotermocompromisso.inc','descricao'=>'Relat�rio de ades�o ao termo de compromisso'),
						//2=>array('codigo'=>'relatpersonal_errospagamentossgb.inc','descricao'=>'Relat�rio de erros no envio de pagamentos no SGB'),
						//3=>array('codigo'=>'relatpersonal_bolsistasparados.inc','descricao'=>'Relat�rio de Bolsistas sem tramita��o do pagamento'),
						//0=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Relat�rio de Quantitativos'),
						//5=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO'),
						//6=>array('codigo'=>'relatpersonal_numerobolsistas.inc','descricao'=>'N�mero de bolsistas do PACTO'),
						//7=>array('codigo'=>'relatpersonal_turmasgerais.inc','descricao'=>'Informa��es sobre dados gerais das turmas'),
						//8=>array('codigo'=>'relatpersonal_professorespacto.inc','descricao'=>'Professores participando do Pacto'),
						//9=>array('codigo'=>'relatpersonal_aprendizagem.inc','descricao'=>'Informa��es sobre aprendizagem'),
						//10=>array('codigo'=>'relatpersonal_turmasformadores.inc','descricao'=>'Turmas de formadores'),
						//11=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Quantitativos do SISPACTO'),
						//1=>array('codigo'=>'relatpersonal_dataplanoatv.inc','descricao'=>'Relat�rio das datas dos planos de atividades'),
						0=>array('codigo'=>'relatpersonal_contatosbolsistas.inc','descricao'=>'Relat�rio de contato dos bolsistas'),
						1=>array('codigo'=>'relatpersonal_cursistas.inc','descricao'=>'Relat�rio dos cursistas do SISFOR'),
						2=>array('codigo'=>'relatpersonal_bolsasprojeto.inc','descricao'=>'Relat�rio qtds de bolsas no projeto'),
						3=>array('codigo'=>'relatpersonal_infogeralcursos.inc','descricao'=>'Informa��es gerais sobre os cursos'),
						4=>array('codigo'=>'relatpersonal_cursoandamento_cargahoraria.inc','descricao'=>'Informa��es sobre o curso em andamento: Carga hor�ria'),
						5=>array('codigo'=>'relatpersonal_cursoandamento_vigencia.inc','descricao'=>'Informa��es sobre o curso em andamento: Vig�ncia'),
						6=>array('codigo'=>'relatpersonal_cursoandamento_vagaspreenchidas.inc','descricao'=>'Informa��es sobre o curso em andamento: Vagas Preenchidas'),
						7=>array('codigo'=>'relatpersonal_formacao_quantitativo.inc','descricao'=>'Informa��es sobre os perfis atuantes na forma��o: Quantitativos'),
						8=>array('codigo'=>'relatpersonal_formacao_escolaridade.inc','descricao'=>'Informa��es sobre os perfis atuantes na forma��o: Escolaridade'),
						9=>array('codigo'=>'relatpersonal_formacao_sexo.inc','descricao'=>'Informa��es sobre os perfis atuantes na forma��o: Sexo'),
						10=>array('codigo'=>'relatpersonal_formacao_idade.inc','descricao'=>'Informa��es sobre os perfis atuantes na forma��o: Idade'),
						11=>array('codigo'=>'relatpersonal_bolsasformacao.inc','descricao'=>'Informa��es sobre as bolsas vinculadas/pagas aos perfis de forma��o'),
						12=>array('codigo'=>'relatpersonal_execucao2014.inc','descricao'=>'Relat�rio de execu��o 2014'),
						13=>array('codigo'=>'relatpersonal_valoresies.inc','descricao'=>'Relat�rio de valores IES'),
						14=>array('codigo'=>'relatpersonal_vigenciacursos.inc','descricao'=>'Vig�ncia do curso e dos bolsistas'),
						15=>array('codigo'=>'relatpersonal_bolsistasemtermo.inc','descricao'=>'Lista de Bolsistas sem termo de compromisso'),
						16=>array('codigo'=>'relatpersonal_andamentocursos.inc','descricao'=>'Andamento dos cursos')
						//17=>array('codigo'=>'relatpersonal_relatoriofinal.inc','descricao'=>'Situa��o dos Relat�rios Finais')
			
						
						);
	



?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadossispacto';
		var janela = window.open( '', 'relatoriopersonlizadossispacto', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>