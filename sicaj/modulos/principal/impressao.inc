<?php

/**
 * verifica se existe o ID do pedido
 */
if (!isset($_GET['id'])) {
    header('location:sicaj.php?modulo=principal/listar&acao=A');
    die();
}
/*
 * Gerando a Chave do Documento
 */
require_once APPRAIZ . 'www/validacaodocumento/gerarchave.php';

/* Verificando se o documento j� foi gerado com Chave */
$sql = sprintf("select vldid from sicaj.pedidocdovalidado where pdcid = %d and pcvstatus = 'A' and tipo = 'C'", (int) $_GET['id']);

$vldid = $db->pegaUm($sql);

if ($vldid) {
    baixarDocumentoValidado($vldid);
} else {
    $stmt = sprintf("select pdcid,
    						numprocessoadm,
    						pdcnumprocessojudicial,
    						pdcjuizodacao,
    						pdccodobjeto,
    						pdccodacaojudicial,
    						pdcnumbeneficioacao,
    						TO_CHAR(pdcdatainicio, 'DD/MM/YYYY') as pdcdatainicio,
    						pdcvalorcdo,
    						pdcvalorexecatual,
    						pdcvalorexecpassado,
                                                CASE WHEN pdc.docid IS NOT NULL
                                                    THEN (
                                                        SELECT TO_CHAR(htddata, 'DD/MM/YYYY')
                                                        FROM workflow.historicodocumento
                                                        WHERE docid = (SELECT docid FROM sicaj.pedidocdo WHERE pdcid = pdc.pdcid)
                                                        ORDER BY htddata DESC LIMIT 1
                                                        )
                                                    ELSE
                                                        '-'
                                                END AS datastatus
 					   from sicaj.pedidocdo pdc
    				  where pdcid = %d", (int) $_GET['id']);

    $row = $db->pegaLinha($stmt);

    /**
     * verifica se retornou a row do banco
     */
    if (!$row) {
        header('location:sicaj.php?modulo=principal/listar&acao=A');
        die();
    }

    $total_cdo = ($row['pdcvalorexecatual'] + $row['pdcvalorexecpassado']);

    $data = $row['datastatus'] != '-' ? explode('/',$row['datastatus']) : explode('/',date('d/m/Y'));
    $dataPorExtenso = $data[0] . " de " . pegaMesAno((int) $data[1]) . " de " . $data[2];
    $row['pdcvalorexecatual'] = number_format($row['pdcvalorexecatual'], 2, ',', '.');
    $row['pdcvalorexecpassado'] = number_format($row['pdcvalorexecpassado'], 2, ',', '.');
    $total_cdo = number_format($total_cdo, 2, ',', '.');

    $key = md5('brasao');
    $pathImg = APPRAIZ . '/www/imagens/brasao.JPG';
    $brasao = baixarImagem($key, $pathImg);

    $html = <<<htmldesaida
        	<div align="center">
        		<img src="data:image/jpg;base64,$brasao" />
		    	<h4>
		    		MINIST�RIO DA EDUCA��O<br />
					SECRETARIA EXECUTIVA<br />
		        	SUBSECRETARIA DE PLANEJAMENTO E OR�AMENTO
		        </h4>
			    <p>Esplanada dos Minist�rios Bloco "L" - Anexo I - 1� Andar<br /> (61) 2022-8801 - E-mail: spo@mec.gov.br</p>
        	</div>

		    <p align="right">Bras�lia,  {$dataPorExtenso}.</p>

		    <p>Assunto: <strong>Manifesta��o de disponibilidade or�ament�ria.</strong></p>

		    <p>Senhor Dirigente de Recursos Humanos.</p>

		    <p style="text-align:justify;">1.	Para fins de cumprimento do Art. 4�, inciso II, da Portaria MP n� 17, de 6 de fevereiro de 2001,
		        segue em anexo c�pia da tela SICAJ onde este �rg�o setorial manifestou a exist�ncia de previs�o or�ament�ria
		        para as despesas decorrentes do efetivo cumprimento da decis�o judicial em ep�grafe, nos termos do Art. 5�
		        da referida Portaria, relativo a seguinte a��o judicial:</p>

		    <ul>
		        <li>N�mero do processo administrativo: {$row['numprocessoadm']}</li>
		        <li>N�mero do processo judicial: {$row['pdcnumprocessojudicial']}</li>
		        <li>Juizo da a��o: {$row['pdcjuizodacao']}</li>
		        <li>C�digo do objeto cadastrado no SICAJ: {$row['pdccodobjeto']}</li>
		        <li>A��o cadastrada no SICAJ: {$row['pdccodacaojudicial']}</li>
		        <li>Numero de Benef�ci�rio da a��o: {$row['pdcnumbeneficioacao']}</li>
		        <li>Data do �nicio da efic�cia temporal: {$row['pdcdatainicio']}</li>
		    </ul>

		    <p style="text-align:justify;">2.	As despesas correr�o � conta das dota��es constantes da Lei Or�ament�ria Anual, e suas respectivas suplementa��es,
		        relativas �s despesas de Pessoal e Encargos Sociais consignadas nessa unidade or�ament�ria, e suas respectivas suplementa��es.</p>
		    <p>3.   Valores</p>

		    <ul>
		        <li>Valor no Exerc�cio Atual: R$ {$row['pdcvalorexecatual']} </li>
		        <li>Valor no Exerc�cio Anterior: R$ {$row['pdcvalorexecpassado']}</li>
		        <li>Valor total do CDO: R$ {$total_cdo}</li>
		    </ul>

    		<p align="right">
		        {{simec_chave}}
    		</p>
htmldesaida;

    $vldid = gerarDocumentoValidado($html);

    $sql = "INSERT INTO sicaj.pedidocdovalidado (pdcid, vldid, pcvstatus) VALUES ('{$_GET['id']}', '{$vldid}', 'A')";
    $db->executar($sql);
    $db->commit();

    baixarDocumentoValidado($vldid);
}
?>
