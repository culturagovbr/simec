<?php

$htddataini = "";
$htddatafim = "";

ini_set("memory_limit","1024M");
set_time_limit(0);

$docid = pegarDocid( $entid );
if ( $_REQUEST['formulario'] ) {
		$codigo  = $_REQUEST['codigo'];
	} else {
		$codigo = $_SESSION['sisid'];
	}
	
if ($_POST || $_GET['entid']){
	include 'resultRelProdutividade.php';
	exit;
}
include APPRAIZ. '/includes/Agrupador.php';
include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( 'Relat�rio de Produtividade', 'Relat�rio de Produtividade' );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Relat�rio</title>
<script src="/includes/calendario.js"></script>
<script type="text/javascript" src="/includes/prototype.js"></script>
<script type="text/javascript">
function gerarRelatorio(){
	var formulario = document.formulario;
	var  htddataini = document.formulario.htddataini.value;
	var  htddataini = document.formulario.htddataini.value;
	
	
	selectAllOptions( formulario.f_estuf );	
	selectAllOptions( formulario.f_municipio );	
	selectAllOptions( formulario.f_esdid );
	selectAllOptions( formulario.f_pflcod );
		
	var janela = window.open( '', 'relatorio', 'width=900,height=600,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
	formulario.target = 'relatorio';	
	formulario.submit();
	janela.focus();
}

function onOffCampo( campo )
{
	var div_on = document.getElementById( campo + '_campo_on' );
	var div_off = document.getElementById( campo + '_campo_off' );
	var input = document.getElementById( campo + '_campo_flag' );
	if ( div_on.style.display == 'none' )
	{
		div_on.style.display = 'block';
		div_off.style.display = 'none';
		input.value = '1';
	}
	else
	{
		div_on.style.display = 'none';
		div_off.style.display = 'block';
		input.value = '0';
	}
}
</script>
</head>
<body>
<form name="formulario" id="formulario" action="pdeescola.php?modulo=relatorio/rel_produtividade&acao=A" method="post" enctype="multipart/form-data">	
<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="border-bottom:none;">
		<tr>
	        <td align='right' class="SubTituloDireita">Nome:</td>
	        <td>
				<?=campo_texto('usunome','N',$habil,'',30,200,'','');?>
	        </td>
	    </tr>
		<tr>
	        <td class="SubTituloDireita" valign="top">Per�odo da Situa��o</td>
	        <td>
	           <?= campo_data( 'htddataini', 'N','S','','','','' ); ?> (dd/mm/aaaa) &nbsp;&nbsp;&nbsp;at� &nbsp; 
	           <?= campo_data( 'htddatafim', 'N','S','','','','' ); ?> (dd/mm/aaaa)
	        </td>
    	</tr>
									
		<tr>
			<td class="SubTituloDireita" valign="top" onclick="javascript:onOffCampo( 'esdid' );">
				Situacao
				<input
					type="hidden"
					id="esdid_campo_flag"
					name="esdid_campo_flag"
					value="<?php echo $possuiSelecionado ? "1" : "0" ?>"
				/>
			</td>
			<td>
				<div
					id="esdid_campo_off"
					style="color:#a0a0a0;display:<?php echo !$possuiSelecionado ? "block" : "none" ?>"
					onclick="javascript:onOffCampo( 'esdid' );"
				><img src="../imagens/combo-todos.gif" border="0" align="middle"></div>
				<div id="esdid_campo_on" style="display:<?php echo $possuiSelecionado ? "block" : "none" ?>;">
					<?php
						$sql_combo ="select esd.esdid as codigo, esd.esddsc as descricao from workflow.estadodocumento esd
									 left join workflow.tipodocumento tid on esd.tpdid = esd.tpdid
									  where tid.sisid='".$_SESSION['sisid']."' and tid.tpdid=29 and esd.tpdid=29";
						
						combo_popup(
							'f_esdid',						// nome do campo
							$sql_combo,						// sql de captura dos valores
							'Selecione a(s) Situa��es',	// label
							'400x400',						// tamanho do popup
							0,								// quantidade m�xima de itens
							array(),						// valores fixos
							'',								// mensagem
							'S',							// habilitado
							false,							// habilitar busca por c�digo
							true,							// habilitar flag cont�m
							10,
							400,
							'',
							'',
							'',
							array(
								  array(
								  		"codigo" 	=> "esd.esddsc",
								  		"descricao" => "Situa��o"
								  		)
								  )							
						);
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td class="SubTituloDireita" valign="top" onclick="javascript:onOffCampo( 'pflcod' );">
				Perfil
				<input
					type="hidden"
					id="pflcod_campo_flag"
					name="pflcod_campo_flag"
					value="<?php echo $possuiSelecionado ? "1" : "0" ?>"
				/>
			</td>
			<td>
				<div
					id="pflcod_campo_off"
					style="color:#a0a0a0;display:<?php echo !$possuiSelecionado ? "block" : "none" ?>"
					onclick="javascript:onOffCampo( 'pflcod' );"
				><img src="../imagens/combo-todos.gif" border="0" align="middle"></div>
				<div id="pflcod_campo_on" style="display:<?php echo $possuiSelecionado ? "block" : "none" ?>;">
					<?php
						$sql_combo ="SELECT
							 			pflcod AS codigo,
							 			pfldsc AS descricao
									 FROM
							 			seguranca.perfil
									 WHERE
							 			sisid = ".$codigo;
						
						combo_popup(
							'f_pflcod',						// nome do campo
							$sql_combo,						// sql de captura dos valores
							'Selecione o(s) perfil',	    // label
							'400x400',						// tamanho do popup
							0,								// quantidade m�xima de itens
							array(),						// valores fixos
							'',								// mensagem
							'S',							// habilitado
							false,							// habilitar busca por c�digo
							true,							// habilitar flag cont�m
							10,
							400,
							'',
							'',
							'',
							array(
								  array(
								  		"codigo" 	=> "pfldsc",
								  		"descricao" => "Perfil"
								  		)
								  )							
						);
					?>
				</div>
			</td>
		</tr>		
		<tr>
			<td class="SubTituloDireita" valign="top" onclick="javascript:onOffCampo( 'estuf' );">
				Estado
				<input
					type="hidden"
					id="estuf_campo_flag"
					name="estuf_campo_flag"
					value="<?php echo $possuiSelecionado ? "1" : "0" ?>"
				/>
			</td>
			<td>
				<div
					id="estuf_campo_off"
					style="color:#a0a0a0;display:<?php echo !$possuiSelecionado ? "block" : "none" ?>"
					onclick="javascript:onOffCampo( 'estuf' );"
				><img src="../imagens/combo-todos.gif" border="0" align="middle"></div>
				<div id="estuf_campo_on" style="display:<?php echo $possuiSelecionado ? "block" : "none" ?>;">
					<?php
						$sql_combo ="SELECT 
										estuf AS codigo,
										estuf || ' - ' || estdescricao AS descricao
									FROM
									    territorios.regiao r   
										INNER JOIN territorios.estado e ON e.regcod = r.regcod	 									
									ORDER BY
										estuf";
						
						combo_popup(
							'f_estuf',						// nome do campo
							$sql_combo,						// sql de captura dos valores
							'Selecione o(s) Estado(s)',	// label
							'400x400',						// tamanho do popup
							0,								// quantidade m�xima de itens
							array(),						// valores fixos
							'',								// mensagem
							'S',							// habilitado
							false,							// habilitar busca por c�digo
							true,							// habilitar flag cont�m
							10,
							400,
							'',
							'',
							'',
							array(
								  array(
								  		"codigo" 	=> "r.regdescricao",
								  		"descricao" => "Regi�o"
								  		)
								  )							
						);
					?>
				</div>
			</td>
		</tr>	
		<tr>
			<td class="SubTituloDireita" valign="top" onclick="javascript:onOffCampo( 'municipio' );">
				Munic�pio
				<input
					type="hidden"
					id="municipio_campo_flag"
					name="municipio_campo_flag"
					value="<?php echo $possuiSelecionado ? "1" : "0" ?>"
				/>
			</td>
			<td>
				<div
					id="municipio_campo_off"
					style="color:#a0a0a0;display:<?php echo !$possuiSelecionado ? "block" : "none" ?>"
					onclick="javascript:onOffCampo( 'municipio' );">
					<img src="../imagens/combo-todos.gif" border="0" align="middle">
				</div>
				<div id="municipio_campo_on" style="display:<?php echo $possuiSelecionado ? "block" : "none" ?>;">
					<?php
						$sql_combo ="SELECT
										--DISTINCT
										m.muncod AS codigo,
										m.estuf || ' - ' || mundescricao AS descricao
									FROM
										territorios.municipio m
									ORDER BY
										m.estuf";
						
						combo_popup(
							'f_municipio',						// nome do campo
							$sql_combo,						// sql de captura dos valores
							'Selecione o(s) munic�pio(s)',		// label
							'400x400',						// tamanho do popup
							0,								// quantidade m�xima de itens
							array(),						// valores fixos
							'',								// mensagem
							'S',							// habilitado
							false,							// habilitar busca por c�digo
							true,
							10,
							400,
							'',
							'',
							'',
							array(
								  array(
								  		"codigo" 	=> "m.estuf",
								  		"descricao" => "Unidade de federa��o"
								  		)
								  )							
							// habilitar flag cont�m
						);
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<input type="button" name="Gerar Relat�rio" value="Gerar Relat�rio" onclick="javascript:gerarRelatorio();"/>
			</td>
		</tr>
</table>		
</form>
</body>
</html>
<?php
function agrupador(){
	return array(
				array('codigo' => '76',
					  'descricao' => 'Em Elabora��o'),
				array('codigo' => '35',
					  'descricao' => 'Aguard. corre��o (Cadastramento)'),
				array('codigo' => '86',
					  'descricao' => 'Avalia��o comit� mun. ou estadual'),
				array('codigo' => '36',
					  'descricao' => 'Aguardando corre��o (Comit�)'),
				array('codigo' => '87',
					  'descricao' => 'Avalia��o MEC'),
				array('codigo' => '90',
					  'descricao' => 'Finalizado'),
				array('codigo' => '37',
					  'descricao' => 'Devolvido para Escola'),
				array('codigo' => '38',
					  'descricao' => 'Devolvido para o Comit�')						
			);
}
?>