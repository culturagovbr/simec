<?php
pde_verificaSessao();
/*
 * 
Function, salvar/editar
 */
function salva ($pjiid = null){
	global $db,$pdeid;
	
	if ($pjiid){
		$sql = sprintf("UPDATE
					 	 pdeescola.percentualjornadaintegral
					    SET
					     pjianoreferencia  		 = %s,
					     pjiresposta = '%s',
					     pjidataatualizacao  = %s
					    WHERE
					     pjiid = %d",
					$_POST['pjianoreferencia'] ? $_POST['pjianoreferencia'] : 'null',
					substr($_POST['pjiresposta'],0,350),
					'now()',
					$pjiid
				);
	}else{
		$sql = sprintf("INSERT INTO pdeescola.percentualjornadaintegral
					    (
					     pdeid,
					     pjianoreferencia,
					     pjiresposta,
					     pjidataatualizacao
					    )VALUES(
					     %d,
						 %s,
						 '%s',
						 '%s'
					    )",
					$pdeid,
					$_POST['pjianoreferencia'] ? $_POST['pjianoreferencia'] : 'null',
					substr($_POST['pjiresposta'],0,350),
					'now()'
				);
	}
	$db->executar($sql);
	
	$epfid = $db->pegaUm("SELECT
							epfid
						  FROM
						  	pdeescola.estruturaperfilfuncionamento
						  WHERE
						  	trim(epfnomearquivo) = 'p29' AND
						  	epfano = ".ANO_EXERCICIO_PDE_ESCOLA);
	
	$existe = $db->pegaUm("SELECT
							pepid
						 FROM
						 	pdeescola.pdeepf
						 WHERE
						 	pdeid = ".$pdeid." AND
						 	epfid = ".$epfid);
	
	if($existe == NULL) {
		$sql = "INSERT INTO pdeescola.pdeepf(pdeid,epfid) VALUES(".$pdeid.",".$epfid.")";
		$db->executar($sql);
	}
		 
	$existe_preenchimento = $db->pegaUm("SELECT 
												pprid
											 FROM 
											 	pdeescola.pdepreenchimento
											 WHERE
											 	pdeid = '$pdeid'
											 AND
											 	ppritem = 'p29'");
		if( $existe_preenchimento == NULL )
		{
			$sql_p = "INSERT INTO pdeescola.pdepreenchimento( pdeid, pprinstrumento, ppritem, pdeanoreferencia) VALUES ( '$pdeid', 'i1', 'p29','{$_SESSION['exercicio_atual']}')";
			$db->executar( $sql_p ); 
		}
		$db->commit();  
}

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$pjiid = $_POST['pjiid'] ? $_POST['pjiid'] : $_GET['pjiid'];
$pdeid = $_POST['pdeid'] ? $_POST['pdeid'] : $_SESSION['pdeid'];

if (!$pdeid){
	die('<script>
			alert(\'Falta parametros!\nTente novamente.\');
			history.go(-1);
		 </script>');
}

/***********************************************************
 * Componente que ir� montar os but�es do formul�rio
 * $cDado = array("instrumento" => 1) ==>> Paramentros que ser�o carregados pelo 
 * 										   __construct "Padr�es � classe".
 * $cForm = new formulario($cDado)    ==>> Instanciando a classe, passando os parametros, 
 * 										   n�o precisa dar include do arquivo na p�gina.
 * $cForm->direcPag('salvaP13( epaid )', array("epaid" => $epaid)); ==>> Faz o gerenciamento dos m�todos 
 * 																		1� parametro
 * 																		passa a fun��o que ser� aplicada nos bot�es 
 * 																		"anterior, pr�ximo, salvar anterior, salvar pr�ximo e salvar".
 * 																		2� parametro
 * 																		ser� um array de "�ndice" igual ao parametro da func�o 
 * 																		passada no 1� parametro e o valor ser� o desejado.
 * $cForm->montarbuttons("validaForm()") ==>> Localiza-se no final da tabela do formul�rio "dentro da tabela ainda", este monta os but�es.
 * 											  O parametro passado refere-se a fun��o javascript que validar� o formul�rio,
 * 											  devendo esta submeter o formul�rio. 
 * 
 ***********************************************************/

$cDado = array("instrumento" => 1);
$cForm = new formulario($cDado);
$cForm->direcPag('salva( pjiid )', array("pjiid" => $pjiid));

/*
///// Faz inser��o/atualiza��o e retorna � �rvore /////
if ($_POST):
	salva($pjiid);
	die('<script>
			alert(\'Opera��o realizada com sucesso!\');
			location.href = \'?modulo=principal/instrumento1/p25&acao=A\';
		 </script>');
endif;
*/

/*
 * Carrega dados
 */
$sql = "SELECT
		 *
		FROM
		 pdeescola.percentualjornadaintegral
		WHERE
		 pdeid = ".$pdeid;
$dados = (array) $db->pegaLinha($sql);

extract($dados);

include  APPRAIZ . 'includes/cabecalho.inc';
echo '<br />';
$titulo_modulo = 'Instrumento 1';
monta_titulo($titulo_modulo, '');
?>
<?=cabecalhoPDE(); ?>
<script type="text/javascript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css" />
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0">
<?=subCabecalho('29 - Qual o percentual de professores com jornada de trabalho em tempo integral na escola, atualmente?'); ?>
<form action="" method="post" name="formulario">
<input type='hidden' name='pjiid' value='<?=$pjiid ?>' />
<input type='hidden' name='pdeid' value='<?=$pdeid ?>' />
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
	<tr>
		<td class="SubTituloDireita">Ano:</td>
		<td width="60%">
		<?php
		$ano = $pjiid ? $pjianoreferencia : ANO_EXERCICIO_PDE_ESCOLA;
		if(!$ano) $ano = ANO_EXERCICIO_PDE_ESCOLA;
		echo $ano;
		?>
		<input type="hidden" name="pjianoreferencia" value="<?=$ano?>">
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita" valign="top">Resposta:</td>
		<td>
		<?= campo_textarea( 'pjiresposta', 'S', 'S', '', 80, 10, 350,'' ); ?>
		</td>
	</tr>
	<? $cForm->montarbuttons("validaForm()");?>				
</table>
<script type="text/javascript">
d = document;
function validaForm(){
	/*
	if (!d.formulario.elements['rotafetoudesempenho'][0].checked && !d.formulario.elements['rotafetoudesempenho'][1].checked) {
		alert("Campo 'H� turmas ou disciplinas sem professor?' � Obrigat�rio.");
		return;
	}
	*/
	if (d.formulario.elements['pjiresposta'].value == '') {
		alert("Campo 'Resposta' � Obrigat�rio.");
		return;
	}
	d.formulario.submit();
	return true;
}
</script>	