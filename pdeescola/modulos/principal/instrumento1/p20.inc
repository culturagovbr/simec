<?php
pde_verificaSessao();
$possuiProjetos = verificaProjetos($_SESSION['pdeid']);
if( $possuiProjetos ){
	$somenteLeitura = 'S';
}else{
	$somenteLeitura = 'N';
}
/*
 * 
Function, salvar/editar
 */
function salvaP16 ($mmpid = null){
	global $db,$pdeid;
	
	if ($mmpid){
		$sql = sprintf("UPDATE
					 	 pdeescola.mudancamedidaprojeto
					    SET
					     mmporganizacaoescola 	 	= '%s',
					     mmptaxaabandonoreprovacao  = '%s',
					     mmpcompromissoprofessor 	= '%s',
					     mmpambienteescolar			= '%s',
					     mmpenvolvimentopais		= '%s',
					     mmpnivelaprendizagemaluno	= '%s',
					     mmpfrequenciaaluno			= '%s',	
					     mmpoutro  					= '%s'
					    WHERE
					     mmpid = %d",
					$_POST['mmporganizacaoescola'],
					$_POST['mmptaxaabandonoreprovacao'],
					$_POST['mmpcompromissoprofessor'],
					$_POST['mmpambienteescolar'],
					$_POST['mmpenvolvimentopais'],
					$_POST['mmpnivelaprendizagemaluno'],
					$_POST['mmpfrequenciaaluno'],
					substr($_POST['mmpoutro'],1,150),
					$mmpid);
	}else{
		$sql = sprintf("INSERT INTO pdeescola.mudancamedidaprojeto
					    (
					     pdeid,
					     mmporganizacaoescola,
					     mmptaxaabandonoreprovacao,
					     mmpcompromissoprofessor,
					     mmpambienteescolar,
					     mmpenvolvimentopais,
					     mmpnivelaprendizagemaluno,
					     mmpfrequenciaaluno,
					     mmpoutro
					    )VALUES(
					     %d,
						 '%s',
						 '%s',
						 '%s',
						 '%s',
						 '%s',
						 '%s',
						 '%s',
						 '%s'					    
						)",
					$pdeid,
					$_POST['mmporganizacaoescola'],
					$_POST['mmptaxaabandonoreprovacao'],
					$_POST['mmpcompromissoprofessor'],
					$_POST['mmpambienteescolar'],
					$_POST['mmpenvolvimentopais'],
					$_POST['mmpnivelaprendizagemaluno'],
					$_POST['mmpfrequenciaaluno'],
					substr($_POST['mmpoutro'],1,150));
	}

	$db->executar($sql);
	
	$epfid = $db->pegaUm("SELECT
							epfid
						  FROM
						  	pdeescola.estruturaperfilfuncionamento
						  WHERE
						  	trim(epfnomearquivo) = 'p20' AND
						  	epfano = ".ANO_EXERCICIO_PDE_ESCOLA);
	
	$existe = $db->pegaUm("SELECT
							pepid
						 FROM
						 	pdeescola.pdeepf
						 WHERE
						 	pdeid = ".$pdeid." AND
						 	epfid = ".$epfid);
	
	if($existe == NULL) {
		$sql = "INSERT INTO pdeescola.pdeepf(pdeid,epfid) VALUES(".$pdeid.",".$epfid.")";
		$db->executar($sql);
	}
		
 
	$existe_preenchimento = $db->pegaUm("SELECT 
												pprid
											 FROM 
											 	pdeescola.pdepreenchimento
											 WHERE
											 	pdeid = '$pdeid'
											 AND
											 	ppritem = 'p20'");
		if( $existe_preenchimento == NULL )
		{
			$sql_p = "INSERT INTO pdeescola.pdepreenchimento( pdeid, pprinstrumento, ppritem, pdeanoreferencia) VALUES ( '$pdeid', 'i1','p20','{$_SESSION['exercicio_atual']}')";
			$db->executar( $sql_p );
 
		}
		$db->commit();
  
}

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$mmpid = $_POST['mmpid'] ? $_POST['mmpid'] : $_GET['mmpid'];
$pdeid = $_POST['pdeid'] ? $_POST['pdeid'] : $_SESSION['pdeid'];
$entid = $_POST['entid'] ? $_POST['entid'] : $_SESSION['entid'];

if (!$pdeid){
	die('<script>
			alert(\'Faltam parametros!\nTente novamente.\');
			history.go(-1);
		 </script>');
}

/***********************************************************
 * Componente que ir� montar os but�es do formul�rio
 * $cDado = array("instrumento" => 1) ==>> Paramentros que ser�o carregados pelo 
 * 										   __construct "Padr�es � classe".
 * $cForm = new formulario($cDado)    ==>> Instanciando a classe, passando os parametros, 
 * 										   n�o precisa dar include do arquivo na p�gina.
 * $cForm->direcPag('salvaP13( epaid )', array("epaid" => $epaid)); ==>> Faz o gerenciamento dos m�todos 
 * 																		1� parametro
 * 																		passa a fun��o que ser� aplicada nos bot�es 
 * 																		"anterior, pr�ximo, salvar anterior, salvar pr�ximo e salvar".
 * 																		2� parametro
 * 																		ser� um array de "�ndice" igual ao parametro da func�o 
 * 																		passada no 1� parametro e o valor ser� o desejado.
 * $cForm->montarbuttons("validaForm()") ==>> Localiza-se no final da tabela do formul�rio "dentro da tabela ainda", este monta os but�es.
 * 											  O parametro passado refere-se a fun��o javascript que validar� o formul�rio,
 * 											  devendo esta submeter o formul�rio. 
 * 
 ***********************************************************/

$cDado = array("instrumento" => 1);
$cForm = new formulario($cDado);
$cForm->direcPag('salvaP16( mmpid )', array("mmpid" => $mmpid));

/*
///// Faz inser��o/atualiza��o e retorna � �rvore /////
if ($_POST):
	salvaP16($mmpid);
	die('<script>
			alert(\'Opera��o realizada com sucesso!\');
			location.href = \'?modulo=principal/estrutura_avaliacao&acao=A&entid='.$entid.'\';
		 </script>');
endif;
*/

/*
 * Carrega dados
 */
$sql = "SELECT
		 *
		FROM
		 pdeescola.mudancamedidaprojeto
		WHERE
		 pdeid = ".$pdeid;
$dados = (array) $db->pegaLinha($sql);

extract($dados);

include  APPRAIZ . 'includes/cabecalho.inc';
echo '<br />';
$titulo_modulo = 'Instrumento 1';
monta_titulo($titulo_modulo, '');
?>
<?=cabecalhoPDE(); ?>
<script type="text/javascript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css" />
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0">
<?=subCabecalho('20 - O que mudou com a implanta��o das medidas ou projetos em rela��o � situa��o anterior (comprovar com fatos e dados)? Se alguma das respostas a seguir for afirmativa, especifique.'); ?>	
<form action="" method="post" name="formulario" onsubmit="return validaForm();">
<input type='hidden' name='mmpid' value='<?=$mmpid ?>' />
<input type='hidden' name='pdeid' value='<?=$pdeid ?>' />
<input type='hidden' name='entid' value='<?=$entid ?>' />
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
	<tr>
		<td class="SubTituloDireita">A organiza��o da escola?</td>
		<td width="60%">
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmporganizacaoescola',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">As taxas de abandono e reprova��o?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmptaxaabandonoreprovacao',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">Compromisso dos professores?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmpcompromissoprofessor',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">Ambiente escolar?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmpambienteescolar',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">Envolvimento do pais?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmpenvolvimentopais',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">N�vel de aprendizagem dos alunos?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmpnivelaprendizagemaluno',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">Frequ�ncia dos alunos?</td>
		<td>
		<?php
		$sql = " SELECT
				  't' AS codigo,
				  'Sim' AS descricao
				UNION
				 SELECT
				  'f' AS codigo,
				  'N�o' AS descricao
				ORDER BY
 				 descricao DESC";
		$db->monta_radio('mmpfrequenciaaluno',$sql,$somenteLeitura,0)
		?>
		</td>
	</tr>								
	<tr>
		<td class="SubTituloDireita">Outros (especificar)</td>
		<td><?= campo_texto( 'mmpoutro', 'N', $somenteLeitura, '', 80, 150, '', ''); ?></td>
	</tr>
	<? $cForm->montarbuttons("validaForm()");?>					
</table>
<script type="text/javascript">
d = document;
function validaForm(){
	
	<?php if( $possuiProjetos ) { ?>
		if (!d.formulario.elements['mmporganizacaoescola'][0].checked && !d.formulario.elements['mmporganizacaoescola'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmptaxaabandonoreprovacao'][0].checked && !d.formulario.elements['mmptaxaabandonoreprovacao'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmpcompromissoprofessor'][0].checked && !d.formulario.elements['mmpcompromissoprofessor'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmpambienteescolar'][0].checked && !d.formulario.elements['mmpambienteescolar'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmpenvolvimentopais'][0].checked && !d.formulario.elements['mmpenvolvimentopais'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmpnivelaprendizagemaluno'][0].checked && !d.formulario.elements['mmpnivelaprendizagemaluno'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}else if (!d.formulario.elements['mmpfrequenciaaluno'][0].checked && !d.formulario.elements['mmpfrequenciaaluno'][1].checked) {
			alert('Todas as perguntas devem ser respondidas!');
			return false;
		}
		d.formulario.submit();
		return true;	
	<?php
	}
	?>
}
</script>	