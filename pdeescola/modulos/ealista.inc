<?

/*** Tempor�rio, pois as escolas do EA ainda est�o em 2010... ***/
//$_SESSION["exercicio"] = 2011;

unset($_SESSION['eacid']);
unset($_SESSION['entid']);



?>
<!-- Inclue o JQuery -->
<script type="text/javascript" src="/includes/JQuery/jquery-1.4.2.js"></script>
<!-- Inclue o arquivo JS espec�fico da p�gina -->
<script type="text/javascript" src="./js/js_ealista.js"></script>

<script type="text/javascript" src="/includes/prototype.js"></script>
<script src="./js/eaajax.js" type="text/javascript"></script>

<?php

if ( !pdeescola_possui_perfil(PDEESC_PERFIL_SUPER_USUARIO) && pdeescola_possui_perfil(PDEESC_PERFIL_CAD_ESCOLA_ACESSIVEL) ){
	$_SESSION['bo_cadastrador_escola_acessivel'] = true;	
	$entidade = pdeescola_pegaescola($_SESSION['usucpf']);
?>
	<script>
		redirecionaEA('eaajax.php', 'tipo=redirecionaea&entid='+<?php echo $entidade;?>);
	</script>							  	
<?php 
}


/*unset($_SESSION['pdeid']);
if ( selecionarEntidade($_GET['entid']) ){
	header( "Location: pdeescola.php?modulo=principal/estrutura_avaliacao&acao=A" );
	exit();
}*/

if ($_POST['ajaxestuf']) {
	header('content-type: text/html; charset=ISO-8859-1');
	$sql = "select
			 muncod as codigo, mundescricao as descricao 
			from
			 territorios.municipio 
			where
			 estuf = '".$_POST['ajaxestuf']."' 
			order by
			 mundescricao asc";
	die($db->monta_combo( "muncod", $sql, 'S', 'Selecione um Munic�pio', '', '' ));
}

include APPRAIZ . 'includes/cabecalho.inc';

echo '<br/>';
echo montarAbasArray(carregaAbasEscolaAcessivel(), "/pdeescola/pdeescola.php?modulo=ealista&acao=E");

?>

<script type="text/javascript">
	function removerFiltro(){
		document.formulario.filtro.value = "";
		document.formulario.estuf.selectedIndex = 0;
		document.formulario.submit();
	}
</script>

<table align="center" border="0" class="tabela" cellpadding="3" cellspacing="1">
	<tbody>
		<tr>
			<td style="padding:15px; background-color:#e9e9e9; color:#404040; vertical-align: top;">
				<form action="" method="POST" name="formulario">
					<input type="hidden" name="acao" value="<?= $_REQUEST['acao'] ?>"/>
					<div style="float: left;">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td valign="bottom">
									Escola:
									<br/>
									<?php $escola = simec_htmlentities( $_REQUEST['escola'] ); ?>
									<?= campo_texto( 'escola', 'N', 'S', '', 50, 200, '', '' ); ?>
								</td>
								<td valign="bottom">
									C�digo Escola:
									<br/>
									<?php $entcodent = simec_htmlentities( $_REQUEST['entcodent'] ); ?>
									<?= campo_texto( 'entcodent', 'N', 'S', '', 30, 200, '', '' ); ?>
								</td>								
								<td>
									Tipo:
									<br>
									<?php
										$tpcid = $_POST['tpcid'];
										$sql = sprintf("SELECT
															'1,3' AS codigo,
															'Todas' AS descricao
														UNION ALL
														SELECT
															'1' AS codigo,
															'Estadual' AS descricao									
														UNION ALL
														SELECT 
															'3' AS codigo,
															'Municipal' AS descricao");
										$db->monta_combo( "tpcid", $sql, 'S', 'Selecione...', '', '' );
									?>				
								</td>
							</tr>
							<tr>
								<td>
									Situa��o:
									<br>
									<?php
									$esdid = $_REQUEST['esdid'];
									
									$sql = "SELECT
											 esdid AS codigo,
											 esddsc AS descricao
											FROM
											 workflow.estadodocumento et
											 INNER JOIN workflow.tipodocumento tp ON tp.tpdid = et.tpdid AND
											 	sisid = ".$_SESSION['sisid']."
											 WHERE et.tpdid = ".TPDID_ESCOLA_ACESSIVEL."
											ORDER BY
											 esdordem;";
									
									$db->monta_combo( "esdid", $sql, 'S', 'Selecione...', '', '' );
									?>
									<script type="text/javascript">
										var esdid 	= 	document.getElementsByName("esdid");
										var option	=	document.createElement('option');
										
										option.text = "N�o Iniciado";
										option.value = "naoiniciado";
										
										try {
										  esdid[0].add(option,null); // standards compliant
										} catch(ex) {
										  esdid[0].add(option); // IE only
										}
										
										if("<?=$esdid?>" == "naoiniciado") {
											esdid[0].options[5].selected = true;
										}
									</script>
								</td>
								<td valign="bottom">
									Estado
									<br/>
									<?php
									$estuf = $_REQUEST['estuf'];
									
									$sql = "select
											 e.estuf as codigo,
											 e.estdescricao as descricao 
											from
											 territorios.estado e 
											order by
											 e.estdescricao asc";
									$db->monta_combo( "estuf", $sql, 'S', 'Selecione...', 'filtraTipo', '' );
									?>
								</td>
								<td valign="bottom" id="municipio" style="visibility:<?= $_REQUEST['estuf'] ? 'visible' : 'hidden'  ?>;">
									Munic�pio
									<br/>
									<?
									if ($_REQUEST['estuf']) {
										
										$muncod = $_REQUEST['muncod'];
										
										$sql = "select
												 muncod as codigo, 
												 mundescricao as descricao 
												from
												 territorios.municipio
												where
												 estuf = '".$_REQUEST['estuf']."' 
												order by
												 mundescricao asc";
										$db->monta_combo( "muncod", $sql, 'S', 'Selecione...', '', '' );
									}
									?>	
								</td>								
							</tr>
							<tr>
								<td valign="bottom">
									Modalidade de Ensino
									<br/>
									<?
										$modalidade = $_REQUEST['modalidade'];
										
										$sql = "SELECT
													'F' AS codigo,
													'Ensino Fundamental' AS descricao									
												UNION ALL
												SELECT 
													'M' AS codigo,
													'Ensino M�dio' AS descricao";
										$db->monta_combo( "modalidade", $sql, 'S', 'Selecione...', '', '' );
									?>	
								</td>								
							</tr>
							<tr>
								<td><br/></td>
							</tr>
						</table>
						<div style="float: left;">
							<input type="button" name="" value="Pesquisar" onclick="return validaForm();"/>
						</div>
					</div>	
				</form>
			</td>
			<td style="padding:15px; background-color:#e9e9e9; color:#404040; vertical-align:middle; text-align:center;">
				<div style="background-color:#f4f4f4; width:100%; border:1px solid #a0a0a0;">
					<br />
					<b><font color="#000000">Para imprimir o Plano Consolidado clique no bot�o abaixo:</font></b><br /><br />
					<input type="button" id="btGeralConsolidado" name="btGeralConsolidado" value="Relat�rio Geral Consolidado" />
					<br /><br /><br />
				</div>
			</td>
		</tr>
	</tbody>
</table> 
<? ealista(); ?>

<script type="text/javascript">
<!--

d = document;

/*
* Faz requisi��o via ajax
* Filtra o municipio, atrav�z do parametro passado 'estuf'
*/
function filtraTipo(estuf) {
	td     = d.getElementById('municipio');
	select = d.getElementsByName('muncod')[0];
	
	/*
	* se estuf vazio
	* esconde o td do municipio e retorna
	*/
	if (!estuf){
		td.style.visibility = 'hidden';
		return;
	}

	// Desabilita o <select>, caso exista, at� que o resultado da pesquisa seja carregado
	if (select){
		select.disabled = true;
		select.options[0].text = 'Aguarde...';
		select.options[0].selected = true;
	}	
	
	// Faz uma requisi��o ajax, passando o parametro 'ordid', via POST
	var req = new Ajax.Request('pdeescola.php?modulo=ealista&acao=E', {
							        method:     'post',
							        parameters: '&ajaxestuf=' + estuf,
							        onComplete: function (res)
							        {			
							        	var inner = 'Munic�pio<br/>';
										td.innerHTML = inner+res.responseText;
										td.style.visibility = 'visible';
							        }
							  });
}	
    
                   
function validaForm()
{
  /*var p1 = document.formulario.preenchimento1;
  var p2 =  document.formulario.preenchimento2;
  
  if( isNaN( p1.value )  || isNaN( p2.value ) )
  {
  	   alert('S� � permitido n�meros no filtro por preenchimento');
	   p1.value = '';
	   p2.value = '';
	   return false;
  }*/
  document.formulario.submit();
  
}
 
</script>
