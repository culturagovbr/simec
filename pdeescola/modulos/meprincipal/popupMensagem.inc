<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
	</head>
	<body>
		<center>
			<?if($_REQUEST['id'] == 'EsporteLazer'){?>
			
				<h3>Diretor, voc� aderiu ao Esporte na Escola!!!</h3>
				<br />
				<div style="margin: 0 auto; padding: 0; height: 460px; width: 100%; border: none;" class="div_rolagem">
				
					<p style="text-align:justify;">
						O Minist�rio do Esporte parabeniza essa institui��o pela iniciativa de participar desse Programa que incentiva a pr�tica esportiva, integrando pol�tica esportiva educacional com a pol�tica de educa��o.
						<br>O Esporte na Escola � uma parceria entre o Minist�rio do Esporte e o Minist�rio da Educa��o, para promover o acesso ao esporte �s crian�as e aos jovens das escolas participantes do Mais Educa��o, da rede de ensino p�blico brasileira. 
						<br>O Esporte na Escola ser� desenvolvido no Macrocampo Esporte e Lazer do Mais Educa��o, de acordo com a proposta pedag�gica do esporte educacional, a qual proporcionar� aos alunos m�ltiplas viv�ncias esportivas. 
						<br>O respons�vel pelo desenvolvimento das atividades ser� o monitor, obrigatoriamente da �rea de Educa��o F�sica/Esporte (graduado ou graduando), selecionado pela Escola para atuar nesse Macrocampo. 
						</p>
					<br />
					<p style="text-align:justify;"><b>Principais benef�cios:</b></p>
					
					<p style="text-align:justify;">	
						� Acesso �s diversas modalidades coletivas e individuais, por meio do desenvolvimento de m�ltiplas viv�ncias esportivas;<br />
						� Kit de materiais esportivos diversificados;<br />
						� Kit esportivo para modalidade Atletismo: Conforme informado no momento da ades�o, nesta edi��o, a modalidade ATLETISMO ter� uma aten��o especial, inclusive com a destina��o de kit esportivo e m�dulo de capacita��o espec�fico;<br>
						� Materiais did�ticos (Livro de Fundamentos do PST e Cadernos de Apoio Pedag�gico � CAP);<br>
						� Capacita��o para os professores/monitores, necessitando que a escola garanta a inscri��o dos envolvidos;<br>
						� Acompanhamento pedag�gico realizado por profissionais vinculados �s Institui��es de Ensino Superior da regi�o.
					</p>
					<br />
					<p style="text-align:justify;">Para viabilizar o acompanhamento das atividades na escola e a participa��o dos monitores na capacita��o, <b>ser� necess�rio preencher o cadastro da escola, do diretor e do monitor no sistema do Segundo Tempo no s�tio eletr�nico: www.pst.uem.br.</b> Para acessar o sistema de cadastro � necess�rio:</p>
					<p style="text-align:justify;">	� Inserir o CPF do Diretor da escola como login;<br />
						� Digitar a senha 123456789: A senha deve ser modificada ap�s o 1� acesso.
					</p>
					<br />
					<p style="text-align:justify;">	
						� importante destacar que somente o CPF do diretor cadastrado no SIMEC ter� o acesso liberado, portanto � imprescind�vel que os dados cadastrais do diretor e da escola estejam atualizados no SIMEC. Assim que o MEC finalizar a rela��o dos diretores cadastrados, voc� receber� uma mensagem eletr�nica para que a escola proceda ao cadastramento no Sistema do Segundo Tempo.
						Estamos � disposi��o para esclarecimentos por e-mail segundotempo_maisedu@esporte.com.br ou pelos telefones (61) 3217-9490/9691/1490/1465.
					</p>
					<br />
					<p style="font-size: 10px" align="right">
						<b>
							MINIST�RIO DO ESPORTE<br>
							SECRETARIA NACIONAL DE ESPORTE, EDUCA��O, LAZER E INCLUS�O SOCIAL<br>
							DEPARTAMENTO DE DESENVOLVIMENTO E ACOMPANHAMENTO DE POL�TICAS E PROGRAMAS INTERSETORIAS<br>
							PROGRAMA SEGUNDO TEMPO
						</b>
					</p>
					
				</div>
				<br>
				<input type="button" value="FECHAR" onclick="closeMessage();" />
				
			<?}elseif($_REQUEST['id'] == 'PromocaoSaude'){?>
			
				<h3>VENHA PARA ESSE MOVIMENTO !</h3>
				<br />
				<div style="margin: 0 auto; padding: 0; height: 460px; width: 100%; border: none;" class="div_rolagem">
				
					<p style="text-align:justify;">
						Agora no Macrocampo Promo��o � Sa�de, sua escola poder� desenvolver m�ltiplas viv�ncias e a��es promotoras de sa�de, integradas em uma �nica proposta pedag�gica.
						<br>Em 2013 o Macrocampo  Promo��o � Sa�de teve uma aten��o especial com a articula��o do Programa Sa�de na Escola- PSE.  
						</p>
					<br />
					<p style="text-align:justify;"><b>NOVIDADES:</b></p>
					<br />
					<p style="text-align:justify;">As a��es desenvolvidas pelas escolas no Macrocampo Promo��o da Sa�de, contar� para o alcance de metas do Programa Sa�de na Escola (PSE).</p>
					<br />
					<p style="text-align:justify;">	
						<b>Aderindo ao Macrocampo</b> a escola receber� recursos de custeio e capital para aquisi��o de materiais espec�ficos para o desenvolvimento destas a��es articuladas aos demais macrocampos e atividades selecionadas pela escola.
					</p>
					<br />
					<p style="text-align:justify;">
						<b>
							Maiores informa��es entre <a href="http://www.sa�de.gov.br/pse" target="_blank">http://www.sa�de.gov.br/pse</a>
						</b>
					</p>
					
				</div>
				<br>
				<input type="button" value="FECHAR" onclick="closeMessage();" />
				
			<?}?>
			<!-- 
			<input type="button" value="Aderir ao Segunto Tempo" onclick="aderirPST('meajax.php', 'tipo=aderir_pst'); closeMessage();" />
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="N�o Aderir ao Segunto Tempo" onclick="naoAderirPST('meajax.php', 'tipo=nao_aderir_pst'); closeMessage();" />
			 -->
		</center>
	</body>
</html>