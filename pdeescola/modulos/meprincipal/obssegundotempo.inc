<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
	</head>
	<body>
		<center>
			<h3>TRAGA O SEGUNDO TEMPO PARA A SUA ESCOLA!</h3>
			<br />
			<p>Agora sua escola poder� aderir ao Programa Segundo Tempo do Minist�rio do Esporte e desenvolver<br />
			   m�ltiplas viv�ncias e modalidades esportivas integradas em uma �nica proposta pedag�gica</p>
			<br />
			<p>Benef�cios:</p>
			
			<p>	� Kits de materiais esportivos (1 Kit por escola);<br />
				� Materiais pedag�gicos e uniformes para alunos e monitores;<br />
				� Acompanhamento pedag�gico realizado por profissionais vinculados a institui��es de Ensino Superior;<br />
				� Capacita��o do monitor do Macrocampo Esporte e Lazer.
			</p>
			<br />
			<p>Para isso voc� precisa:</p>
			<p>	� Adotar a Proposta Pedag�gica voltada ao esporte educacional;<br />
				� Dispor de estrutura f�sica m�nima (interna ou externa), que n�o interfira nas aulas de educa��o f�sica curricular: um espa�o esportivo com no m�nimo 50 m2, dependendo das modalidades desenvolvidas, na escola ou nos espa�os externos utilizados pela escola para a pr�tica de esporte e lazer;<br />
				� Participar do processo de capacita��o e acompanhamento do Minist�rio do Esporte.
			</p>
			<br />
			<p>	Clique aqui caso deseje obter mais informa��es sobre o Segundo Tempo:<br />
				<a href="http://portal.esporte.gov.br/snee/segundotempo/maiseducacao">http://portal.esporte.gov.br/snee/segundotempo/maiseducacao</a><br />
			</p>
			<br />
			<input type="button" value="Aderir ao Segunto Tempo" onclick="aderirPST('meajax.php', 'tipo=aderir_pst'); closeMessage();" />
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="N�o Aderir ao Segunto Tempo" onclick="naoAderirPST('meajax.php', 'tipo=nao_aderir_pst'); closeMessage();" />
		</center>
	</body>
</html>