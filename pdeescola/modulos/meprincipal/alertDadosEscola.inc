<?

if($_SESSION['memid']){
	if($_REQUEST['salvaMsg']){
		
		$sql = "UPDATE pdeescola.memaiseducacao
				   SET memorientacao='".$_REQUEST['chk1']."', 
				   memorientacaocpf='".$_SESSION['usucpf']."', 
				   memmostrarnova='".$_REQUEST['chk2']."'
				 WHERE memid=".$_SESSION['memid'];
		$db->executar($sql);
		$db->commit();
		echo 'OK';
		die();
	}
	
	$sql = "select memorientacao, memmostrarnova from pdeescola.memaiseducacao
			WHERE memid=".$_SESSION['memid'];
	$dados = $db->pegaLinha($sql);
	if($dados) extract($dados);
}

header('content-type: text/html; charset=ISO-8859-1');

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../../includes/listagem.css'/>
		<script type="text/javascript" src="/includes/prototype.js"></script>
	</head>
	<body>
		<center>
			<div style="font-size:12px" >
					<center><b><font color=red>NOVIDADES IMPORTANTES</font></b></center>
					<br>
					
						<div style="margin: 0 auto; padding: 0; height: 420px; width: 100%; border: none;" class="div_rolagem">
						
						<p class=MsoListParagraphCxSpFirst style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>O redesenho do PME (Escolas Urbanas) em 2013</span></b><span
						style='font-size:10.0pt;line-height:115%'> responde ao desafio de superar a dicotomia entre turno e contra-turno de modo que as atividades escolhidas sejam incorporadas nas pr�ticas cotidianas dos professores e professoras. Desta forma a reorganiza��o do Macrocampo Acompanhamento Pedag�gico foi elaborada na perspectiva de equilibrar a agenda curricular de modo que a escola vislumbre a organiza��o do tempo integral, articulando as atividades do PME e os componentes curriculares. <b style='mso-bidi-font-weight:normal'>As escolas do
						campo permanecem com o mesmo desenho e regras de 2012.<o:p></o:p></b></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%'>A partir de 2013 o PME contemplar� atividades espec�ficas para <b style='mso-bidi-font-weight:normal'>jovens de <st1:metricconverter
						ProductID="15 a" w:st="on">15 a</st1:metricconverter> 17 anos</b> que ainda estejam no ensino fundamental. O objetivo � proporcionar a estes estudantes um espa�o educativo para aprendizagens e conviv�ncia, e assegurar sua perman�ncia para a conclus�o com qualidade do Ensino Fundamental.<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpLast style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%'>Escolas que comp�em o <b
						style='mso-bidi-font-weight:normal'>Pacto Nacional da Alfabetiza��o</b> na idade certa devem contemplar estudantes dos anos iniciais nas atividades do PME envolvendo a <u>atividade de Acompanhamento Pedag�gico, especificamente com a��es de alfabetiza��o e letramento.<o:p></o:p></u></span></p>
						
						<p class=MsoListParagraphCxSpFirst style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%;mso-font-kerning:1.5pt;mso-fareast-language:
						PT-BR'>Para garantir a efetividade do Programa Mais Educa��o e a conex�o das atividades do PME ao projeto pol�tico pedag�gico da escola, em 2013 o programa passar� a ofertar �s escolas urbanas <b style='mso-bidi-font-weight:normal'>sete
						<span class=SpellE>Macrocampos</span></b>, sendo eles: 1) Acompanhamento Pedag�gico 2) Comunica��o uso de M�dias, Cultura Digital e Tecnol�gica 3) Cultura, Artes e Educa��o Patrimonial; 4) Educa��o Ambiental e Sociedade Sustent�vel; 5) Esporte e Lazer; 6) Educa��o em Direitos Humanos e 7) Promo��o da Sa�de. As atividades dos macrocampos que deixam de existir foram incorporadas aos atuais macrocampos.</span><span
						style='font-size:10.0pt;line-height:115%'><o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>O <span class=SpellE>Macrocampo</span> Acompanhamento Pedag�gico</span></b><span
						style='font-size:10.0pt;line-height:115%'> continua sendo obrigat�rio, agora com apenas uma atividade que contemplar� as diferentes �reas do conhecimento envolvendo todas as atividades dispon�veis anteriormente (alfabetiza��o, matem�tica, hist�ria, ci�ncias, geografia e l�nguas estrangeiras). <b
						style='mso-bidi-font-weight:normal'>Essa atividade ser� denominada, Orienta��o
						de Estudos e Leitura</b> e tem por objetivo a articula��o entre o curr�culo estabelecido da escola e as atividades pedag�gicas propostas pelo PME, ensejando assim o permanente di�logo entre os professores da escola e os monitores que atuam no PME. Assim esta atividade dever� ser realizada com dura��o de uma hora � uma hora e meia, diariamente, sendo mediada por um monitor orientador de estudos, que seja preferencialmente um estudante de gradua��o  ou das Licenciaturas vinculado ao PIBID (Programa Institucional de Bolsa de Inicia��o � Doc�ncia), ou estudantes de gradua��o com est�gio supervisionado.<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%;
						text-autospace:ideograph-numeric'><span style='font-size:10.0pt;line-height:
						115%;mso-font-kerning:1.5pt;mso-fareast-language:PT-BR'>As atividades dos <span
						class=SpellE>macrocampos</span> �<b style='mso-bidi-font-weight:normal'>Educa��o
						<st1:PersonName ProductID="em Direitos Humanos" w:st="on">em Direitos Humanos</st1:PersonName></b>�
						e <b style='mso-bidi-font-weight:normal'>�Promo��o da Sa�de</b>�, quando escolhidas, devem ser articuladas como temas transversais nas demais atividades selecionadas no PME, sobretudo nos Macrocampos de Comunica��o uso de M�dias, Cultura Digital e Tecnol�gica (ex. jornal, r�dio escolar e etc) e Cultura, Artes e Educa��o Patrimonial (ex. teatro, dan�a e etc), considerando que a partir de agora, para estas duas atividades, ser�o disponibilizados recursos de custeio e capital para o desenvolvimento destas tem�ticas. A proposta � estimular nos estudantes a reflex�o e o di�logo sobre seus direitos e responsabilidades enquanto protagonistas de uma sociedade livre, pluralista e inclusiva, a partir do contexto escolar e social no qual est�o inseridos.  <o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%;
						text-autospace:ideograph-numeric'><span style='font-size:10.0pt;line-height:
						115%;mso-font-kerning:1.5pt;mso-fareast-language:PT-BR'>O PME traz as atividades ol�mpicas de: badminton, luta ol�mpica e v�lei de praia, al�m disso o PST transformou-se na atividade �Esporte na Escola/Atletismo e M�ltiplas Viv�ncias Esportivas�( basquete, futebol, futsal, handebol, voleibol e xadrez), que objetiva qualificar a a��o pedag�gica, por meio de uma proposta planejada, inclusiva, participativa, que possibilita o desenvolvimento de diversas modalidades, tendo o atletismo como base, e valoriza o prazer e o l�dico, fundamentada nos pressupostos do Esporte Educacional e articulada com o projeto pedag�gico da Escola. Essa atividade dever� ser realizada 2 vezes por semana e cada encontro com dura��o m�nima de uma hora, sendo mediada por um monitor, que seja preferencialmente um estudante de gradua��o da �rea da Educa��o F�sica ou Esporte, vinculado ao PIBID (Programa Institucional de Bolsa de Inicia��o � Doc�ncia), ou a outro est�gio supervisionado.</span><span style='font-size:10.0pt;line-height:115%;
						mso-font-kerning:1.5pt;mso-fareast-language:PT-BR'><o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%;
						text-autospace:ideograph-numeric'><span style='font-size:10.0pt;line-height:
						115%;mso-font-kerning:1.5pt;mso-fareast-language:PT-BR'>Para desenvolver as atividades, a escola receber� um kit de material esportivo diversificado, recurso para aquisi��o de kit de material espec�fico para o atletismo e um kit de material did�tico-pedag�gico, al�m de capacita��o para os monitores e acompanhamento das a��es.<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%'>Clique aqui caso deseje obter mais
						informa��es sobre o Esporte na Escola:<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><span
						class=GramE><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;
						line-height:115%'>http://</span></b></span><b style='mso-bidi-font-weight:normal'><span
						style='font-size:10.0pt;line-height:115%'></span></b><span style='font-size:
						10.0pt;line-height:115%'><a target="_blank"
						href="http://www.esporte.gov.br/snelis/segundotempo/maiseducacao/default.jsp"><b
						style='mso-bidi-font-weight:normal'>www.esporte.gov.br/snelis/segundotempo/maiseducacao/default.jsp</b></a><b
						style='mso-bidi-font-weight:normal'><o:p></o:p></b></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%;mso-font-kerning:1.5pt;mso-fareast-language:
						PT-BR'><o:p>&nbsp;</o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>CADASTRO NO SIMEC<o:p></o:p></span></b></p>
						
						<p class=MsoListParagraphCxSpLast style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>Atividades 2013 </span></b><span style='font-size:10.0pt;line-height:
						115%'>- Neste novo desenho a escola dever� optar pela atividade Orienta��o de Estudos e Leitura (obrigat�ria) e mais 3 atividades correspondentes aos demais Macrocampos. 
Caso a escola queira a 5� atividade ser� obrigatoriamente a atividade: Esporte na Escola/ Atletismo e m�ltiplas viv�ncias esportivas.
<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='margin-left:0.0pt;text-align:justify;line-height:
						10.0pt'><b><span style='font-size:10.0pt;color:black;mso-font-kerning:12.0pt'>Rela��o
						Escola-Comunidade: </span></b><span style='font-size:10.0pt;color:black;
						mso-font-kerning:12.0pt;mso-bidi-font-weight:bold'>incentiva e ap�ia a abertura das escolas nos finais de semana proporcionando o desenvolvimento de diversas oficinas com intuito de fortalecer a conviv�ncia comunit�ria, evidenciar a cultura popular, as express�es juvenis e o protagonismo da comunidade, al�m de contribuir para valorizar o territ�rio e os sentimentos de identidade e pertencimento.<b> A aba �Rela��o
						Escola-Comunidade&quot; ser� disponibilizada caso a escola opte pela abertura
						aos finais de semana,</b> momento em que dever�o ser informadas as oficinas tem�ticas de interesse da sua comunidade. Para o desenvolvimento desta a��o, ser�o destinados recursos de custeio e capital proporcionais aos estudantes declarados no Educacenso, destinado ao ressarcimento de oficineiros volunt�rios e aquisi��o de materiais para o desenvolvimento das oficinas.</span><span
						style='font-size:10.0pt'><o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><b><span
						style='font-size:10.0pt;line-height:115%;mso-font-kerning:12.0pt'>Jovens de <st1:metricconverter
						ProductID="15 a" w:st="on">15 a</st1:metricconverter> 17 anos: </span></b><span
						style='font-size:10.0pt;line-height:115%;mso-font-kerning:12.0pt;mso-bidi-font-weight:
						bold'>Esta a��o visa construir propostas de atividades com os jovens, que propiciem trabalhos integrados entre diferentes �reas de conhecimento tendo como objetivo principal o de orientar a cria��o de espa�o para pensar seu projeto de vida desenvolvendo: AUTORIA e AUTONOMIA do grupo de estudantes. Al�m disso, espera-se que tais atividades permitam aos jovens lan�ar um olhar sobre suas trajet�rias escolares, planejando e executando propostas de car�ter investigativo, bem como de organiza��o de a��es que lhe permitam prosseguir em seus estudos e realizar aproxima��es com o mundo do trabalho. Para o desenvolvimento da proposta, ser�o disponibilizados: materiais pedag�gicos de apoio; recursos de custeio e capital; ressarcimento de  <b style='mso-bidi-font-weight:
						normal'>monitor tutor</b> respons�vel por acompanhar este grupo de estudantes.<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><span
						style='font-size:10.0pt;line-height:115%;color:black;mso-font-kerning:12.0pt;
						mso-bidi-font-weight:bold'>Assim, a <b>aba �Jovens de <st1:metricconverter
						ProductID="15 a" w:st="on">15 a</st1:metricconverter> 17 anos&quot; ser�
						disponibilizada caso a escola opte pelo desenvolvimento desta atividade
						espec�fica</b>, devendo informar o n�mero de estudantes participantes da a��o. <o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>Espa�os PME:</span></b><span style='font-size:10.0pt;line-height:115%'> <b
						style='mso-bidi-font-weight:normal'>esta aba � de preenchimento obrigat�rio </b><span
						style='mso-bidi-font-weight:bold'>e destinado � inser��o de fotos e arquivos que mostrem os diferentes espa�os (dentro e fora da escola) onde ocorrem as atividades do Programa Mais Educa��o. No caso de escolas que j� fizeram modifica��es em suas estruturas por meio de recursos do PME, � importante postar imagens que mostrem o antes e o depois.</span><o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;line-height:115%'><b
						style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;line-height:
						115%'>Question�rio Monitoramento F�sico-Financeiro:</span></b><span
						style='font-size:10.0pt;line-height:115%'> <b style='mso-bidi-font-weight:normal'>esta
						aba � de preenchimento obrigat�rio.</b> No item 1 - Recursos PDDE/Educa��o Integral da escola dever� especificar os recursos referentes as rubricas de custeio e capital a partir do saldo informado pelo FNDE.<o:p></o:p></span></p>
						
						<p class=MsoListParagraphCxSpMiddle style='text-align:justify;margin-left:0.0pt'><b style='mso-bidi-font-weight:
						normal'><span style='font-size:10.0pt'>Tramita��o do Plano de Atendimento: </span></b><span
						style='font-size:10.0pt'>Sua escola acaba de sair do status n�o iniciado para o status em cadastramento e permanecer� assim at� o preenchimento de todas as abas do seu plano de atendimento. Ap�s o t�rmino do preenchimento do plano � necess�rio que voc� abra a aba pend�ncias para enviar o plano para a secretaria municipal/estadual. Basta clicar no bot�o, na lateral direita da tela, enviar para avalia��o na secretaria municipal/estadual.</span></p>
						
						</div>		
					
					
					<br>
					<table align="center">
						<tr>
							<td>
								<input type='checkbox' name='memorientacao' id='memorientacao' onclick="liciente();" <?if($memorientacao == 't') echo 'checked';?>>
								<b>Li e estou ciente do PME 2013</b>
								
								<span id="btn_fechar" style="display: <?if($memorientacao != 't') echo 'none';?>" align="right">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="button" value="Fechar Janela" onclick="closeMessage();" />
								</span>
								
								<br><br> 
								<input type='checkbox' name='memmostrarnova' id='memmostrarnova' onclick="liciente();" <?if($memmostrarnova == 't') echo 'checked';?>>
								<b>Deseja n�o visualizar essa mensagem novamente?</b>
								
							</td>
						</tr>
					</table>	
									
		 		  </div>
			<br />
			
		</center>
	</body>
</html>	



<script>
	function liciente(){
		
		var valor1 = '';
		var valor2 = '';
		
		if(document.getElementById('memorientacao').checked == true){
			valor1 = 't';
		}else{
			valor1 = 'f';
		}
		
		if(document.getElementById('memmostrarnova').checked == true){
			valor2 = 't';
		}else{
			valor2 = 'f';
		}
		
		var myAjax = new Ajax.Request(
			"pdeescola.php?modulo=meprincipal/alertDadosEscola&acao=A",
			{
				method: 'post',
				parameters: "salvaMsg=OK&chk1=" + valor1 + "&chk2=" + valor2,
				asynchronous: false,
				onComplete: function(resp) {
					
					if(resp.responseText == 'OK') {
						if(document.getElementById('memorientacao').checked == true){
							document.getElementById('btn_fechar').style.display = '';
						}
						else{
							document.getElementById('btn_fechar').style.display = 'none';
						}
					} 
					
					
				}
			});
	}
</script>