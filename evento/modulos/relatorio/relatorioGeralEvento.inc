<?php
$perfilEmenda = pegaPerfil($_SESSION["usucpf"]);

unset($_POST['acao']);
unset($_SESSION['emenda']['post']);

if ($_REQUEST['limpaSession']) {
    die;
}

require_once APPRAIZ . '/includes/Agrupador.php';
require_once APPRAIZ . 'includes/cabecalho.inc';
echo '<br/>';

monta_titulo('Relat�rios Geral - Eventos', '<b>Filtros de Pesquisa</b>');

function agrupador() {
    return array(
        array('codigo' => 'eveid',
            'descricao' => 'T�tulo do Evento'),
        array('codigo' => 'ungcod',
            'descricao' => 'Unidade Gestora'),
        array('codigo' => 'rcoid',
            'descricao' => 'Reuni�o do Comit�'),
        array('codigo' => 'tpeid',
            'descricao' => 'Tipo'),
        array('codigo' => 'evequantidadedias',
            'descricao' => 'N� de dias do Evento'),
        array('codigo' => 'evepublicoestimado',
            'descricao' => 'P�blico'),
        array('codigo' => 'estuf',
            'descricao' => 'UF'),
        array('codigo' => 'muncod',
            'descricao' => 'Munic�pio'),
        array('codigo' => 'evecustoprevisto',
            'descricao' => 'Custo(R$)'),
        array('codigo' => 'eveanopi',
            'descricao' => 'Ano do PI'),
        array('codigo' => 'plicod',
            'descricao' => 'N� PI'),
        array('codigo' => 'evenumeroprocesso',
            'descricao' => 'N� Processo')
    );
}

function colunas() {
    return array(
        array('codigo' => 'eveid',
            'descricao' => 'T�tulo do Evento'),
        array('codigo' => 'ungcod',
            'descricao' => 'Unidade Gestora'),
        array('codigo' => 'rcoid',
            'descricao' => 'Reuni�o do Comit�'),
        array('codigo' => 'tpeid',
            'descricao' => 'Tipo'),
        array('codigo' => 'evequantidadedias',
            'descricao' => 'N� de dias do Evento'),
        array('codigo' => 'evepublicoestimado',
            'descricao' => 'P�blico'),
        array('codigo' => 'estuf',
            'descricao' => 'UF'),
        array('codigo' => 'muncod',
            'descricao' => 'Munic�pio'),
        array('codigo' => 'evecustoprevisto',
            'descricao' => 'Custo(R$)'),
        array('codigo' => 'eveanopi',
            'descricao' => 'Ano do PI'),
        array('codigo' => 'plicod',
            'descricao' => 'N� PI'),
        array('codigo' => 'evenumeroprocesso',
            'descricao' => 'N� Processo'),
        array('codigo' => 'evedatainclusao',
            'descricao' => 'Ano do Evento')
    );
}
?>

    <script type="text/javascript" src="/includes/prototype.js"></script>

    <script type="text/javascript">
        function onOffCampo(campo) {
            var div_on = document.getElementById(campo + '_campo_on');
            var div_off = document.getElementById(campo + '_campo_off');
            var input = document.getElementById(campo + '_campo_flag');

            if (div_on.style.display == 'none') {
                div_on.style.display = 'block';
                div_off.style.display = 'none';
                input.value = '1';
            } else {
                div_on.style.display = 'none';
                div_off.style.display = 'block';
                input.value = '0';
            }
        }
        function geraPopRelatorio() {
            var form = $('formulario');

            if ($('evedatainclusao_campo_flag').value == "1") {
                selectAllOptions(form.evedatainclusao);
            }
            if ($('eveid_campo_flag').value == "1") {
                selectAllOptions(form.eveid);
            }
            if ($('ungcod_campo_flag').value == "1") {
                selectAllOptions(form.ungcod);
            }
            if ($('rcoid_campo_flag').value == "1") {
                selectAllOptions(form.rcoid);
            }
            if ($('tpeid_campo_flag').value == "1") {
                selectAllOptions(form.tpeid);
            }
            if ($('evequantidadedias_campo_flag').value == "1") {
                selectAllOptions(form.evequantidadedias);
            }
            if ($('evepublicoestimado_campo_flag').value == "1") {
                selectAllOptions(form.evepublicoestimado);
            }
            if ($('estuf_campo_flag').value == "1") {
                selectAllOptions(form.estuf);
            }
            if ($('muncod_campo_flag').value == "1") {
                selectAllOptions(form.muncod);
            }
            if ($('evecustoprevisto_campo_flag').value == "1") {
                selectAllOptions(form.evecustoprevisto);
            }
            if ($('eveanopi_campo_flag').value == "1") {
                selectAllOptions(form.eveanopi);
            }
            if ($('plicod_campo_flag').value == "1") {
                selectAllOptions(form.plicod);
            }
            if ($('evenumeroprocesso_campo_flag').value == "1") {
                selectAllOptions(form.evenumeroprocesso);
            }
            
            selectAllOptions(form.agrupador);
            selectAllOptions(form.coluna);
            form.target = 'page';
            var janela = window.open('evento.php?modulo=relatorio/popUpRelatorioGeralEvento=I', 'page', 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');
            janela.focus();
            form.submit();
        }
    </script>

    <form action="evento.php?modulo=relatorio/popUpRelatorioGeralEvento&acao=I" method="post" name="formulario" id="formulario">
        <input type="hidden" name="limpaSession" id="limpaSession" value="true">
        <table id="table" class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
            <tr>
                <td width="25%" class="SubTituloDireita">Ordena��o</td>
                <td>
                    <?php
                        $matriz = agrupador();
                        $campoAgrupador = new Agrupador('formulario');
                        $campoAgrupador->setOrigem('agrupadorOrigem', null, $matriz);
                        $campoAgrupador->setDestino('agrupador', null, array(
                            /* array('codigo' => 'autid',
                              'descricao' => 'Autor') */
                        ));
                        $campoAgrupador->exibir();
                    ?>

                </td>
            </tr>
            <tr>
                <td width="25%" c class="SubTituloDireita">Colunas</td>
                <td>
                    <?php
                        $coluna = colunas();
                        $campoColuna = new Agrupador('formulario');
                        $campoColuna->setOrigem('colunaOrigem', null, $coluna);
                        $campoColuna->setDestino('coluna', null, array(
                        /* array('codigo' => 'eveid',
                          'descricao' => 'T�tulo do Evento'),
                          array('codigo' => 'ungcod',
                          'descricao' => 'Unidade Gestora') */
                        ));
                        $campoColuna->exibir();
                    ?>
                </td>
            </tr>

            <?php
                #ANO DO EVENTO
                $evedatainclusao = $_REQUEST["evedatainclusao"];
                $sql = "
                    SELECT  DISTINCT CAST( DATE_PART('year', evedatainclusao) AS integer ) AS codigo,
                            CAST(DATE_PART('year', evedatainclusao) AS VARCHAR(4) ) as descricao
                    FROM evento.evento
                    WHERE evedatainclusao IS NOT NULL 
                    ORDER BY 1
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Ano do Evento', 'evedatainclusao', $sql, $stSqlCarregados, 'Selecione o "ano" do Evento');
                
                //T�tulo do Evento
                $eveid = $_REQUEST["eveid"];

                $sql = "
                    SELECT  DISTINCT eveid as codigo,
                            evetitulo as descricao
                    FROM evento.evento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('T�tulo do Evento', 'eveid', $sql, $stSqlCarregados, 'Selecione o(s) T�tulo do Evento(s)');

                //Unidade Gestora
                $ungcod = $_REQUEST["ungcod"];
                $sql = "
                    SELECT  DISTINCT u.ungcod as codigo,
                            u.ungdsc as descricao
                    FROM public.unidadegestora AS u
                    INNER JOIN evento.evento AS ev ON u.ungcod = ev.ungcod
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Unidade Gestora', 'ungcod', $sql, $stSqlCarregados, 'Selecione a(s) Unidade Gestora(s)');

                //Reuni�o do Comit�
                
                $rcoid = $_REQUEST["rcoid"];
                $sql = "
                    SELECT  DISTINCT rc.rcoid AS codigo,
                            rc.rcodescricao AS descricao
                    FROM evento.reuniaocomite as rc
                    INNER JOIN evento.evento AS ev ON ev.rcoid = rc.rcoid
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Reuni�o do Comit�', 'rcoid', $sql, $stSqlCarregados, 'Selecione o(s) Reuni�o do Comit�(s)');

                //Tipo:
                $tpeid = $_REQUEST["tpeid"];
                $sql = "
                    SELECT  DISTINCT tpeid AS codigo,
                            tpedescricao AS descricao
                    FROM evento.tipoevento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Tipo', 'tpeid', $sql, $stSqlCarregados, 'Selecione o(s) Tipo(s)');

                //N� de dias do Evento
                $evequantidadedias = $_REQUEST["evequantidadedias"];
                $sql = "
                    SELECT  DISTINCT evequantidadedias AS codigo,
                            evequantidadedias AS descricao
                    FROM evento.evento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('N� de dias do Evento', 'evequantidadedias', $sql, $stSqlCarregados, 'Selecione o(s) N� de dias do Evento(s)');

                //P�blico
                $evepublicoestimado = $_REQUEST["evepublicoestimado"];
                $sql = "
                    SELECT  DISTINCT evepublicoestimado AS codigo,
                            evepublicoestimado AS descricao
                    FROM evento.evento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('P�blico', 'evepublicoestimado', $sql, $stSqlCarregados, 'Selecione o(s) P�blico(s)');

                //UF
                $estuf = $_REQUEST["estuf"];
                $sql = "
                    SELECT  DISTINCT estuf AS codigo,
                            estuf AS descricao
                    FROM evento.evento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('UF', 'estuf', $sql, $stSqlCarregados, 'Selecione o(s) UF(s)');

                //Munic�pio
                $muncod = $_REQUEST["muncod"];

                $sql = "
                    SELECT  DISTINCT tm.muncod AS codigo,
                            tm.mundescricao AS descricao
                    FROM evento.evento AS ev
                    INNER JOIN territorios.municipio AS tm ON tm.muncod = ev.muncod
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Munic�pio', 'muncod', $sql, $stSqlCarregados, 'Selecione o(s) Munic�pio(s)');

                //Custo(R$)
                $evecustoprevisto = $_REQUEST["evecustoprevisto"];
                $sql = "
                    SELECT  DISTINCT evecustoprevisto AS codigo,
                            evecustoprevisto AS descricao
                    FROM evento.evento
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Custo(R$)', 'evecustoprevisto', $sql, $stSqlCarregados, 'Selecione o(s) Custo(R$)(s)');

                //Ano do PI
                $eveanopi = $_REQUEST["eveanopi"];
                $sql = "
                    SELECT  DISTINCT eveanopi AS codigo,
                            eveanopi AS descricao
                    FROM evento.evento
                    WHERE eveanopi <> ''
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('Ano do PI', 'eveanopi', $sql, $stSqlCarregados, 'Selecione o(s) Ano do PI(s)');

                //N� PI
                $plicod = $_REQUEST["plicod"];
                $sql = "
                    SELECT  DISTINCT mp.plicod AS codigo,
                            mp.plicod AS descricao
                    FROM monitora.pi_planointerno as mp
                    INNER JOIN public.unidadegestora AS u ON u.ungcod = mp.ungcod
                    INNER JOIN evento.evento AS ev ON ev.ungcod = u.ungcod
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('N� PI', 'plicod', $sql, $stSqlCarregados, 'Selecione o(s) Tipo(s)');

                //N� Processo
                $evenumeroprocesso = $_REQUEST["evenumeroprocesso"];

                $sql = "
                    SELECT  DISTINCT evenumeroprocesso AS codigo,
                            evenumeroprocesso AS descricao
                    FROM evento.evento
                    WHERE  evenumeroprocesso <> ''
                    ORDER BY descricao ASC
                ";
                $stSqlCarregados = "";
                mostrarComboPopup('N� Processo', 'evenumeroprocesso', $sql, $stSqlCarregados, 'Selecione o(s) N� Processo(s)');
            ?>

            <tr bgcolor="#D0D0D0">
                <td colspan="2" style="text-align: center;">
                    <input type="button" value="Pesquisar" onclick="geraPopRelatorio();" style="cursor: pointer;"/>
                </td>
            </tr>
        </table>
    </form>