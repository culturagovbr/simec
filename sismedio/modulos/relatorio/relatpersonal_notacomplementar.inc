<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
<p align="center" style="font-size:16px;"><b>Avalia��o complementar</b></p>
<?php

/* configura��es */
ini_set("memory_limit", "2048M");
set_time_limit(600);
/* FIM configura��es */

?>

<script>
function selecionarTipo(v) {
	
	if(v=='1') {
		document.getElementById('pflcodavaliador').disabled=false;
		document.getElementById('pflcodavaliado').disabled=false;
	}

	if(v=='2') {
		document.getElementById('pflcodavaliador').disabled=true;
		document.getElementById('pflcodavaliado').disabled=true;
	}
	
}
</script>

<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">
<tr>
	<td class="SubTituloDireita">Tipo avalia��o complementar : </td>
	<td>
	<? 
	$t[] = array("codigo" => "1", "descricao" => "Avalia��o Complementar de bolsistas");
	$t[] = array("codigo" => "2", "descricao" => "Avalia��o Complementar do conte�do");
	$db->monta_combo('tipoavaliacaocomplementar', $t, 'S', '', 'selecionarTipo', '', '', '', 'N', 'tipoavaliacaocomplementar', '', $_REQUEST['tipoavaliacaocomplementar']);
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita">Perfil do avaliador : </td>
	<td>
	<?
	$sql = "SELECT p.pflcod as codigo, p.pfldsc as descricao FROM seguranca.perfil p
		INNER JOIN sismedio.pagamentoperfil pp ON pp.pflcod = p.pflcod
		WHERE sisid='".SIS_MEDIO."'";
	$db->monta_combo('pflcodavaliador', $sql, (($_REQUEST['tipoavaliacaocomplementar']=='2')?'N':'S'), 'TODOS', '', '', '', '', 'N', 'pflcodavaliador', '', $_REQUEST['pflcodavaliador']);
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita">Perfil do avaliado : </td>
	<td>
	<?
	$sql = "SELECT p.pflcod as codigo, p.pfldsc as descricao FROM seguranca.perfil p
			INNER JOIN sismedio.pagamentoperfil pp ON pp.pflcod = p.pflcod
			WHERE sisid='".SIS_MEDIO."'";
	$db->monta_combo('pflcodavaliado', $sql, (($_REQUEST['tipoavaliacaocomplementar']=='2')?'N':'S'), 'TODOS', '', '', '', '', 'N', 'pflcodavaliado', '', $_REQUEST['pflcodavaliado']);
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita">Universidade : </td>
	<td>
	<?
	$sql = "SELECT u.uncid as codigo, uu.unisigla||' / '||uu.uninome as descricao FROM sismedio.universidadecadastro u 
			inner join sismedio.universidade uu on uu.uniid = u.uniid";
	$db->monta_combo('uncid', $sql, 'S', 'TODOS', '', '', '', '', 'N', 'uncid', '', $_REQUEST['uncid']);
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloDireita">Per�odo de refer�ncia : </td>
	<td>
	<?
	$sql = "SELECT f.fpbid as codigo, 'Ref. ' || m.mesdsc || ' / ' || fpbanoreferencia as descricao 
			FROM sismedio.folhapagamento f 
			INNER JOIN public.meses m ON m.mescod::integer = f.fpbmesreferencia
			WHERE f.fpbstatus='A'";
	$db->monta_combo('fpbid', $sql, 'S', 'TODOS', '', '', '', '', 'N', 'fpbid', '', $_REQUEST['fpbid']);
	?>
	</td>
</tr>
</table>
<?

echo '<p align="center"><input type="button" value="Aplicar" onclick="window.location=\'sismedio.php?modulo=relatorio/relatoriospersonalizados&acao=A&buscar=1&relatorio=relatpersonal_notacomplementar.inc&tipoavaliacaocomplementar=\'+document.getElementById(\'tipoavaliacaocomplementar\').value+\'&pflcodavaliador=\'+document.getElementById(\'pflcodavaliador\').value+\'&pflcodavaliado=\'+document.getElementById(\'pflcodavaliado\').value+\'&uncid=\'+document.getElementById(\'uncid\').value+\'&fpbid=\'+document.getElementById(\'fpbid\').value;this.disabled=true;;this.disabled=true;"> <input type=button value="Limpar" onclick="window.location=\'sismedio.php?modulo=relatorio/relatoriospersonalizados&acao=A&buscar=1&relatorio=relatpersonal_notacomplementar.inc\';"></p>';



if($_REQUEST['tipoavaliacaocomplementar']=='1') :


if($_REQUEST['pflcodavaliador'] && $_REQUEST['pflcodavaliado'] && ($_REQUEST['uncid'] || $_REQUEST['fpbid'])) :


if(in_array(PFL_COORDENADORIES,$perfis) && $_SESSION['sismedio']['universidade']['uncid']) {
	$f_pfl .= " AND i.uncid='".$_SESSION['sismedio']['universidade']['uncid']."'";
}


?>
<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
<tr>
	<td>
	<?
	$sql = "select uni.unisigla||' - '||uni.uninome as universidade,  
					i.iuscpf, 
					i.iusnome, 
					case when p.picid is null then 'Equipe IES'
	     				 when p.muncod is not null then m.estuf||' / '||m.mundescricao||'( Municipal )'
	     				 when p.estuf is not null then e.estuf||' / '||e.estdescricao||'( Estadual )' end as esfera,
					count(distinct i2.iusd) as avaliadores,
					avg(a.iccvalor) as nota 
from sismedio.respostasavaliacaocomplementar r 
inner join sismedio.itensavaliacaocomplementarcriterio a on a.iccid = r.iccid 
inner join sismedio.identificacaousuario i on i.iusd = r.iusdavaliado 
inner join sismedio.universidadecadastro unc on unc.uncid = i.uncid 
inner join sismedio.universidade uni on uni.uniid = unc.uniid 
inner join sismedio.tipoperfil t on t.iusd = i.iusd 
left join sismedio.pactoidadecerta p on p.picid = i.picid 
left join territorios.municipio m on m.muncod = p.muncod 
left join territorios.estado e on e.estuf = p.estuf
inner join sismedio.identificacaousuario i2 on i2.iusd = r.iusdavaliador 
inner join sismedio.tipoperfil t2 on t2.iusd = i2.iusd 
where t2.pflcod=".$_REQUEST['pflcodavaliador']." {$f_pfl} and t.pflcod=".$_REQUEST['pflcodavaliado']." and i.iusstatus='A' ".(($_REQUEST['uncid'])?"and i.uncid='".$_REQUEST['uncid']."'":"")." ".(($_REQUEST['fpbid'])?"and r.fpbid='".$_REQUEST['fpbid']."'":"")."
group by i.iuscpf, i.iusnome, p.picid, p.muncod, p.estuf, m.estuf, e.estuf, m.mundescricao, e.estdescricao, uni.unisigla, uni.uninome
order by 5";
	
	$cabecalho = array("CPF","Nome","Esfera","N�mero de avaliadores","Nota");
	$db->monta_lista_simples($sql,$cabecalho,100000,5,'N','100%','N');
	?>
	</td>

</tr>

</table>
<?
else :
 
echo '<p>Selecione o perfil do avaliador, o perfil avaliado e a universidade</p>';

endif;


elseif($_REQUEST['tipoavaliacaocomplementar']=='2') :


if($_REQUEST['uncid'] || $_REQUEST['fpbid']) :
?>
<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
<tr>
	<td>
	<?
	$sql = "select  uni.unisigla||' - '||uni.uninome as universidade, 
					pp.pfldsc,
					i.iacdsc,
					count(distinct r.iusdavaliador) as avaliadores,
					avg(a.iccvalor) as nota 
			from sismedio.respostasavaliacaocomplementar r 
			inner join sismedio.identificacaousuario ii on ii.iusd = r.iusdavaliador 
			inner join sismedio.universidadecadastro unc on unc.uncid = ii.uncid 
			inner join sismedio.universidade uni on uni.uniid = unc.uniid 
			inner join sismedio.tipoperfil t on t.iusd = ii.iusd 
			inner join sismedio.itensavaliacaocomplementarcriterio a on a.iccid = r.iccid 
			inner join sismedio.itensavaliacaocomplementar i on i.iacid = r.iacid 
			inner join seguranca.perfil pp on pp.pflcod = t.pflcod 
			where i.gicid=1 ".(($_REQUEST['uncid'])?"and ii.uncid='".$_REQUEST['uncid']."'":"")." ".(($_REQUEST['fpbid'])?"and r.fpbid='".$_REQUEST['fpbid']."'":"")." and ii.iusstatus='A'
			group by uni.unisigla, uni.uninome, i.iacdsc, pp.pfldsc
			order by 1,2,5";
	
	$cabecalho = array("IES","Perfil avaliador","Crit�rio","N�mero de avaliadores","Nota");
	$db->monta_lista_simples($sql,$cabecalho,100000,5,'N','100%','N');
	?>
	</td>

</tr>

</table>
<?
else :
 
echo '<p>Selecione a universidade</p>';

endif;

endif;
?>