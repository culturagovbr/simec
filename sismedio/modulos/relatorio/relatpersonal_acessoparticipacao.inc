<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
<p align="center" style="font-size:16px;"><b>Relat�rio de acesso e participa��o</b></p>
<?php

/* configura��es */
ini_set("memory_limit", "2048M");
set_time_limit(600);
/* FIM configura��es */

$sql = "select 
foo.lemuf,
sum(foo.total) as total,
sum(foo.totalvalidado) as totalvalidado,
round((sum(foo.totalvalidado)::numeric/sum(foo.total)::numeric)*100,2) as porcentvalidado,
sum(foo.totalemelab) as totalemelab,
round((sum(foo.totalemelab)::numeric/sum(foo.total)::numeric)*100,2) as porcentemelab,
sum(foo.totalemanalise) as totalemanalise,
round((sum(foo.totalemanalise)::numeric/sum(foo.total)::numeric)*100,2) as porcentemanalise,
sum(foo.totalseminteresse) as totalseminteresse,
round((sum(foo.totalseminteresse)::numeric/sum(foo.total)::numeric)*100,2) as porcentseminteresse,
sum(foo.validadasemies) as validadasemies,
round((sum(foo.validadasemies)::numeric/sum(foo.total)::numeric)*100,2) as porcentvalidadasemies,
(sum(foo.totalemelab)+sum(foo.totalemanalise)+sum(foo.totalseminteresse)+sum(foo.validadasemies)) as totalnaoparticipantes,
round(((sum(foo.totalemelab)+sum(foo.totalemanalise)+sum(foo.totalseminteresse)+sum(foo.validadasemies))::numeric/sum(foo.total)::numeric)*100,2) as porcentnaoparticipantes
	

from (

select lemuf, 
       1 as total, 
       case when d.esdid=1091 then 1 else 0 end as totalvalidado,
       case when (d.esdid=972 or d.esdid is null) then 1 else 0 end as totalemelab,
       case when d.esdid=973 then 1 else 0 end as totalemanalise,
       case when d.esdid=1151 then 1 else 0 end as totalseminteresse,
       case when (d.esdid=1091 and a.abrid is null) then 1 else 0 end as validadasemies
       
from sismedio.listaescolasensinomedio e
left join workflow.documento d on e.docid = d.docid 
left join sismedio.abrangencia a on a.lemcodigoinep = e.lemcodigoinep

) foo
group by foo.lemuf
order by foo.lemuf";

		

$cabecalho = array("UF",
				   "N� de Escolas que ofertam a Etapa do Ensino M�dio - Censo 2013",
				   "N� das Escolas Validadas pelas Secretarias de Educa��o (Participantes)",
				   "%",
				   "N� de Escolas - Em elabora��o (N�o participantes)",
				   "%",
				   "N� de Escolas - Em An�lise (N�o participantes)",
				   "%",
				   "N� de Escolas - que n�o t�m interesse/n�o t�m professores no Ensino M�dio (N�o participantes)",
				   "%",
				   "N� de Escolas - Validadas n�o atendidas por Institui��o P�blica de Ensino Superior (N�o participantes)",
				   "%",
				   "N� de Escolas n�o participantes",
				   "%");

$db->monta_lista_simples($sql,$cabecalho,100000,5,'N','100%','',true, false, false, true);

?>