<?php

include_once "_funcoes.php";

if($_REQUEST['requisicao']) {
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}


$perfis = pegaPerfilGeral();

if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - SISM�dio", "" );


if(in_array(PFL_COORDENADORIES,$perfis)) {

	$relatorios = array(
			0=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do SISM�dio'),
			1=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
			2=>array('codigo'=>'relatpersonal_atividadesobr.inc','descricao'=>'Relat�rio de Atividades obrigat�rias'),
			3=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do SISM�dio'),
			4=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
			5=>array('codigo'=>'relatpersonal_atividadesobr.inc','descricao'=>'Relat�rio de Atividades obrigat�rias')
			//1=>array('codigo'=>'relatpersonal_aprendizagemmat.inc','descricao'=>'Informa��es sobre aprendizagem (Matem�tica)'),
			//2=>array('codigo'=>'relatpersonal_aprendizagempor.inc','descricao'=>'Informa��es sobre aprendizagem (Portugu�s)'),
			//3=>array('codigo'=>'relatpersonal_materiais.inc','descricao'=>'Informa��es a entrega dos materiais'),
			//4=>array('codigo'=>'relatpersonal_aprendizagemdid.inc','descricao'=>'Informa��es sobre uso do materiais did�ticos'),
			//5=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO 2014'),
			//6=>array('codigo'=>'relatpersonal_impressaoana.inc','descricao'=>'Relat�rio de impress�o ANA')
	);

} else {

	$relatorios = array(//0=>array('codigo'=>'relatpersonal_erroscadastrossgb.inc','descricao'=>'Relat�rio de erros no cadastro de bolsistas no SGB'),
							//1=>array('codigo'=>'relatpersonal_aceitacaotermocompromisso.inc','descricao'=>'Relat�rio de ades�o ao termo de compromisso'),
							//2=>array('codigo'=>'relatpersonal_errospagamentossgb.inc','descricao'=>'Relat�rio de erros no envio de pagamentos no SGB'),
							//3=>array('codigo'=>'relatpersonal_bolsistasparados.inc','descricao'=>'Relat�rio de Bolsistas sem tramita��o do pagamento'),
							0=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Relat�rio de Quantitativos'),
							//5=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO'),
							//6=>array('codigo'=>'relatpersonal_numerobolsistas.inc','descricao'=>'N�mero de bolsistas do PACTO'),
							//7=>array('codigo'=>'relatpersonal_turmasgerais.inc','descricao'=>'Informa��es sobre dados gerais das turmas'),
							//8=>array('codigo'=>'relatpersonal_professorespacto.inc','descricao'=>'Professores participando do Pacto'),
							//9=>array('codigo'=>'relatpersonal_aprendizagem.inc','descricao'=>'Informa��es sobre aprendizagem'),
							//10=>array('codigo'=>'relatpersonal_turmasformadores.inc','descricao'=>'Turmas de formadores'),
							1=>array('codigo'=>'relatpersonal_dataplanoatv.inc','descricao'=>'Relat�rio das datas dos planos de atividades'),
							2=>array('codigo'=>'relatpersonal_contatosbolsistas.inc','descricao'=>'Relat�rio de contato dos bolsistas'),
							3=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do SISM�dio'),
							//13=>array('codigo'=>'relatpersonal_gestaomobilizacao.inc','descricao'=>'Relat�rio personalizados Gest�o e Mobiliza��o'),
							//14=>array('codigo'=>'relatpersonal_evasao.inc','descricao'=>'Relat�rio de evas�o do perfis'),
							4=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
							5=>array('codigo'=>'relatpersonal_atividadesobr.inc','descricao'=>'Relat�rio de Atividades obrigat�rias'),
							6=>array('codigo'=>'relatpersonal_aval_oe.inc','descricao'=>'Relat�rio Avalia��o final da Forma��o - OE'),
							7=>array('codigo'=>'relatpersonal_aval_fr.inc','descricao'=>'Relat�rio Avalia��o final da Forma��o - FR'),
							8=>array('codigo'=>'relatpersonal_adesao.inc','descricao'=>'Informa��es sobre ades�es'),
							9=>array('codigo'=>'relatpersonal_acessoparticipacao.inc','descricao'=>'Relat�rio de acesso e participa��o (UF)'),
							10=>array('codigo'=>'relatpersonal_eficienciarendimento.inc','descricao'=>'Relat�rio de Efici�ncia/Rendimento'),
							11=>array('codigo'=>'relatpersonal_bolsaspagasperfil.inc','descricao'=>'N�mero de Bolsas pagas por Perfil')
							//17=>array('codigo'=>'relatpersonal_relatoriofinal.inc','descricao'=>'Situa��o dos Relat�rios Finais')
				
							
							);
	
}
	



?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadossispacto';
		var janela = window.open( '', 'relatoriopersonlizadossispacto', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '400', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>