<?php 
include APPRAIZ ."includes/workflow.php";

$docid = pegaDocidEventoArea($evaid);

$habilNovoParecer = 'S';
$userResp = new UsuarioResponsabilidade();
$arAgdid  = $userResp->pegaRespAgenda();
if ( !verificaPerfil( array(PERFIL_SUPER_USUARIO, PERFIL_ADMINISTRADOR) ) && !in_array($agdid, $arAgdid) ){
	$arAevid = $userResp->pegaRespArea();
	$evento  = new Evento();
	$arEveid = $evento->pegaEventoPorArea( $arAevid, $agdid);
	$arEveid = ( $arEveid ? $arEveid : array(0) );

	if ( !in_array($eveid, $arEveid) ){
		die('<script>
				alert(\'O usu�rio n�o possui permiss�o de acesso ao evento!\');
				location.href=\'?modulo=principal/listaAgenda&acao=A\';
			 </script>');		
	}
	
	$esdid = pegaEstadoEventoArea( $docid );
	if ( !in_array( $esdid, $_ESTADOS_WF_AGENDA_UNIDADE ) ){
		$habilNovoParecer = 'N';		
	}
}

if ( $_SESSION['agenda']['eveid'] ){
	 
	$evento = new Evento();
	$dados = $evento->carregaTodosDados( $_SESSION['agenda']['eveid']);

	?>
	<table class="tabela" align="center">
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">C�digo</td>
			<td> <?php echo $dados['eveid']; ?></td>
		</tr>
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">Evento</td>
			<td> <?php echo $dados['tpedsc']; ?></td>
		</tr>
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">Data de In�cio</td>
			<td> <?php echo $dados['evedtinicio']; ?></td>
		</tr>
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">Solicitante</td>
			<td> <?php echo $dados['sltdsc']; ?></td>
		</tr>
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">Entidade</td>
			<td> <?php echo $dados['etadsc']; ?></td>
		</tr>
		<tr>
			<td width="200" class="SubTituloDireita" style="font-size: 10pt; font-weight: bold;">Assunto</td>
			<td> <?php echo $dados['eveassunto']; ?></td>
		</tr>
	</table>
<?php } ?>

<table class="tabela" align="center">
	<tr>
		<td class="SubTituloDireita" width="200">�rea:</td>
		<td>
		<?php
		$area = new AreaEnvolvida();
		$dados = $area->listaCombo();
		echo $db->monta_combo("aevid", $dados, 'N','Selecione...','carregaDirigente', '', '',240,'S','aevid');
		?>
		</td>
		<td rowspan="5" width="10" valign="top">
		<?php 
		$dados_wf = array();
		wf_desenhaBarraNavegacao($docid, $dados_wf);
		?>
		</td>
	</tr>
	<tr>
		<td class="SubTituloCentro" valign="top" colspan="2">Dirigente(s) Vinculado(s) � �rea</td>
	</tr>
	<tr>
		<td valign="top" id="td_lista_dirigente" colspan="2">
			<span>selecione uma �rea para visualizar seus dirigentes.</span>
		</td>
	</tr>
	<tr>
		<td class="SubTituloCentro" valign="top" colspan="2">
			Hist�rico de Encaminhamento Evento/�rea
			<?php 
			/*
			 * Valida edi��o do evento
			 */
			if ( $habilNovoParecer == 'S' ):
			?>
			<br>
			<a href="javascript:cadastrarParecer(<?php echo $evaid ?>);" style="float:left; margin-left:40px;">[Cadastrar Nova Informa��o]</a>
			<?php 
			endif;
			?>
		</td>
	</tr>
	<tr>
		<td valign="top" colspan="2">
			<div style="height: 150px; overflow: auto;">
			<?php
			$histEncaminhamento = new HistEncaminhamento();
			$sql 	= $histEncaminhamento->listaSQL( array('evaid' => $evaid, 'agdid' => $agdid) );
			
			$cabecalho = array("A��o", "Situa��o da Tramita��o", "Informa��o", "Informa��o Complementar", "Data/Hora de Inclus�o da Informa��o", "Usu�rio Respons�vel pela informa��o");
			$db->monta_lista($sql,$cabecalho,100,5,'N','center',$par2, "formulario");
			?>
			</div>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
	   <td>&nbsp;</td>
	   <td colspan="2">
	   	<input type="button" name="btalterar" value="Salvar" onclick="validarForm()" class="botao">
	   	<input type="button" name="btvoltar" value="Voltar" onclick="location.href='?modulo=principal/listaVinculoArea&acao=A';" class="botao">
	   	<?php 
	   	if ( $habilNovoParecer == 'S' ):
	   	?>
	   	&nbsp;&nbsp;&nbsp;&nbsp;
	   	<input type="button" name="btevento" value="Vincular Nova �rea" onclick="location.href='?modulo=principal/vinculoArea&acao=A';" class="botao">
	   	<?php 
	   	endif;
	   	?>
	   </td>
	</tr> 	
</table>

