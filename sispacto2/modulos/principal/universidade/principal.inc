<? 
/* configura��es */
ini_set("memory_limit", "2048M");
set_time_limit(600);
/* FIM configura��es */
?>
<script>
function selecionarPeriodoReferencia(fpbid) {

	if(fpbid!='') {
		ajaxatualizar('requisicao=exibirSituacaoMensario&fpbid='+fpbid+'&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','dv_situacao_mensario');
		ajaxatualizar('requisicao=exibirPorcentagemPagamento&fpbid='+fpbid+'&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','dv_porcentagem_pagamento');
	}
	
}

function carregarQtdTurmas_<?=PFL_FORMADORIES ?>(filtroconsulta) {
	ajaxatualizarAsync('requisicao=mediaTurmasPerfil&pflcod=<?=PFL_FORMADORIES ?>&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>&filtroconsulta='+filtroconsulta,'mediaTurmasPerfilFormador');
}
function carregarQtdTurmas_<?=PFL_ORIENTADORESTUDO ?>(filtroconsulta) {
	ajaxatualizarAsync('requisicao=mediaTurmasPerfil&pflcod=<?=PFL_ORIENTADORESTUDO ?>&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>&filtroconsulta='+filtroconsulta,'mediaTurmasPerfilOrientadoresEstudo');
}

</script>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2">Principal</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?></td>
</tr>
<tr>
	<td colspan="2">
	
	<? carregarInformes(array('pflcoddestino' => PFL_COORDENADORIES)); ?>
	
	</td>
<? if($_REQUEST['modulo'] == 'principal/universidade/universidadeexecucao') : ?>
</tr>
	<tr>
		<td colspan="2">
			<table width="100%">
			<tr>
				<td width="50%" valign="top">
				<script>
				function exibirSituacaoMensarioPadrao() {
					jQuery('#dv_situacao_mensario').html('<img src="../imagens/carregando.gif" align="absmiddle"> Carregando...');
					ajaxatualizarAsync('requisicao=exibirSituacaoMensario&fpbid=<?=$fpbid_padrao ?>&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','dv_situacao_mensario');
				}
				
				jQuery(document).ready(function() {
					ajaxatualizarAsync('requisicao=exibirAcessoUsuarioSimec&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','acessousuario');
					ajaxatualizarAsync('requisicao=exibirPorcentagemPagamento&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','dv_porcentagem_pagamento');
					ajaxatualizarAsync('requisicao=ausenciaOEIES&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','ausenciaOEIES');
					ajaxatualizarAsync('requisicao=ausenciaPAMunicipio&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','ausenciaPAMunicipio');
					ajaxatualizarAsync('requisicao=bolsistasSISPACTODirigentes&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','bolsistasSISPACTODirigentes');

					ajaxatualizarAsync('requisicao=mediaTurmasPerfil&pflcod=<?=PFL_FORMADORIES ?>&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','mediaTurmasPerfilFormador');
					ajaxatualizarAsync('requisicao=mediaTurmasPerfil&pflcod=<?=PFL_ORIENTADORESTUDO ?>&uncid=<?=$_SESSION['sispacto2']['universidade']['uncid'] ?>','mediaTurmasPerfilOrientadoresEstudo');
									
					
				});
				</script>
				<p align="center">Resumo acesso dos usu�rios ao SIMEC</p>
				<div id="acessousuario"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				<hr>
				<p align="center">Munic�pios que n�o fecharam as turmas</p>
				
				<div style="height:100px;overflow:auto;">
				<?
				exibirMunicipiosNaoFechados(array('uncid'=>$_SESSION['sispacto2']['universidade']['uncid']));
				?>
				</div>
				
				<div id="ausenciaOEIES"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				<br>
				<div id="ausenciaPAMunicipio"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				<br>
				<div id="bolsistasSISPACTODirigentes"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				
				
				</td>
				<td width="50%" valign="top">
				<?
				$fpbid_padrao = $db->pegaUm("SELECT fpbid FROM sispacto2.folhapagamento WHERE fpbanoreferencia='".date("Y")."' AND fpbmesreferencia='".date("m")."'");
				carregarPeriodoReferencia(array('uncid'=>$_SESSION['sispacto2']['universidade']['uncid'],'fpbid'=>$fpbid_padrao));
				?>
				<hr/>
				<p align="center">Resumo situa��es dos Mens�rios</p>
				<div style="height:150px;overflow:auto;" id="dv_situacao_mensario">A situa��o de <b>Aptos/N�o Aptos/Aprovados</b> demora cerca de 2 minutos para ser processada. Caso queira visualizar essa informa��o <a style="cursor:pointer;" onclick="exibirSituacaoMensarioPadrao();"><b>CLIQUE AQUI</b></a></div>
				<br>
				<p align="center">Porcentagem geral de pagamentos</p>
				
				<div id="dv_porcentagem_pagamento"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				
			 <p>Turmas de formadores</p>
			<p align="center">
			<select name="qtd[]" class="CampoEstilo" style="width: auto;font-size:x-small;" onchange="carregarQtdTurmas_<?=PFL_FORMADORIES ?>(this.value)">'
			<option value="BETWEEN 1 AND 5">At� 5 cursistas</option>
			<option value="BETWEEN 6 AND 10">De 6 a 10 cursistas</option>
			<option value="BETWEEN 11 AND 25">De 11 a 25 cursistas</option>
			<option value="BETWEEN 26 AND 34">De 26 a 34 cursistas</option>
			<option value="BETWEEN 35 AND 50">De 35 a 50 cursistas</option>
			<option value="BETWEEN 51 AND 75">De 51 a 75 cursistas</option>
			<option value="BETWEEN 76 AND 100">De 76 a 100 cursistas</option>
			<option value=">100">Mais de 100 cursistas</option>
			</select>
			</p>
			 
			 <div id="mediaTurmasPerfilFormador"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
			 <p>Turmas de Orientadores de Estudo</p>
			<p align="center">
			<select name="qtd[]" class="CampoEstilo" style="width: auto;font-size:x-small;" onchange="carregarQtdTurmas_<?=PFL_ORIENTADORESTUDO ?>(this.value)">'
			<option value="BETWEEN 1 AND 5">At� 5 cursistas</option>
			<option value="BETWEEN 6 AND 10">De 6 a 10 cursistas</option>
			<option value="BETWEEN 11 AND 25">De 11 a 25 cursistas</option>
			<option value="BETWEEN 26 AND 34">De 26 a 34 cursistas</option>
			<option value="BETWEEN 35 AND 50">De 35 a 50 cursistas</option>
			<option value="BETWEEN 51 AND 75">De 51 a 75 cursistas</option>
			<option value="BETWEEN 76 AND 100">De 76 a 100 cursistas</option>
			<option value=">100">Mais de 100 cursistas</option>
			</select>
			</p>
			 
			 <div id="mediaTurmasPerfilOrientadoresEstudo"><img src="../imagens/carregando.gif" align="absmiddle"> Carregando...</div>
				
				</td>
			</tr>
			</table>
		</td>
	</tr>
<? endif; ?>
	<tr>
		<td class="SubTituloDireita" width="20%">&nbsp;</td>
		<td>
		<?=criarBotoesNavegacao(array('url' => $_SERVER['REQUEST_URI'])) ?>
		</td>
	</tr>
</table>
<style>
.ui-dialog-title {
  text-align: center;
  width: 100%;
}
</style>
<div id="modalInstrumentos" style="display: none;" title="PESQUISA"></div>
<?

$sql = "SELECT iraid FROM sispacto2.instrumentosrespostasabertas WHERE iusd='".$_SESSION['sispacto2']['universidade']['iusd']."' AND (irapergunta1 IS NOT NULL OR irapergunta2 IS NOT NULL OR irapergunta3 IS NOT NULL OR irapergunta4 IS NOT NULL OR irapergunta5 IS NOT NULL)";
$iraid = $db->pegaUm($sql);

if(!$iraid) :

$html = '<form method="post" id="formulario_pesquisa" enctype="multipart/form-data"><input type="hidden" name="requisicao" value="salvarInstrumentosAbertos"><input type="hidden" name="iusd" value="'.$_SESSION['sispacto2']['universidade']['iusd'].'"><table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="100%"><tr><td>1. Quais as principais dificuldades na execu��o do processo de forma��o continuada no �mbito do PNAIC? Seja o mais detalhista poss�vel.</td></tr><tr><td><textarea name=irapergunta1 id=irapergunta1 cols=70 rows=6></textarea></td></tr><tr><td>2. Quais as principais facilidades na execu��o do processo de forma��o continuada no �mbito do PNAIC? Seja o mais detalhista poss�vel.</td></tr><tr><td><textarea name=irapergunta2 id=irapergunta2 cols=70 rows=6></textarea></td></tr><tr><td>3. Como voc� analisa a qualidade pedag�gica do material de forma��o do PNAIC? Seja o mais detalhista poss�vel.</td></tr><tr><td><textarea name=irapergunta3 id=irapergunta3 cols=70 rows=6></textarea></td></tr><tr><td>4. Quais s�o as barreiras e facilitadores que influem na realiza��o das suas atribui��es no �mbito do PNAIC? Seja o mais detalhista poss�vel.</td></tr><tr><td><textarea name=irapergunta4 id=irapergunta4 cols=70 rows=6></textarea></td></tr><tr><td>5. Em sua opini�o, qual � a contribui��o real do PNAIC para a alfabetiza��o das crian�as? Seja o mais detalhista poss�vel.</td></tr><tr><td><textarea name=irapergunta5 id=irapergunta5 cols=70 rows=6></textarea></td></tr><tr><td align=center><input type=button name=inserirpesquisa value="Gravar" onclick="gravarInstrumentosAbertos();"></td></tr></table></form>';

?>
<script>
	function gravarInstrumentosAbertos() {
		if(jQuery('#irapergunta1').val()=='') {
			alert('Preencha a pergunta 1.');
			return false;
		}
		if(jQuery('#irapergunta2').val()=='') {
			alert('Preencha a pergunta 2.');
			return false;
		}
		if(jQuery('#irapergunta3').val()=='') {
			alert('Preencha a pergunta 3.');
			return false;
		}
		if(jQuery('#irapergunta4').val()=='') {
			alert('Preencha a pergunta 4.');
			return false;
		}
		if(jQuery('#irapergunta5').val()=='') {
			alert('Preencha a pergunta 5.');
			return false;
		}

		jQuery('#formulario_pesquisa').submit();
	}


	jQuery("#modalInstrumentos").html('<div style=\"text-align:center;width: 95%;padding: 10px;margin: 0px;\"><?=$html ?></div>');
	
	jQuery("#modalInstrumentos").dialog({
        draggable:true,
        resizable:true,
        width: 800,
        height: 750,
        modal: true,
     	close: function(){} 
    });
</script>
<? 
endif;
?>