<script>
function exibirInstrumentos() {
	ajaxatualizar('requisicao=carregarInstrumentos&iusd=<?=$_SESSION['sispacto2']['professoralfabetizador']['iusd'] ?>','modalInstrumentos');

	jQuery("#modalInstrumentos").dialog('open');
	
}

function fecharInstrumentos() {
	jQuery("#modalInstrumentos").dialog('close');
}

function gravarRespostaInstrumento(p) {
	ajaxatualizar('requisicao=gravarRespostaInstrumento&iusd=<?=$_SESSION['sispacto2']['professoralfabetizador']['iusd'] ?>'+p,'');
}

function avancarInstrumento() {
	var problemas = false;
	jQuery("[id^='inoid_']").each(function() {
		if(jQuery(this).val()=='') {
			problemas = true;
		}
	});

	if(problemas==true) {
		alert('Todas quest�es devem estar preenchidas');
		return false;
	}

	ajaxatualizar('requisicao=carregarInstrumentos&iusd=<?=$_SESSION['sispacto2']['professoralfabetizador']['iusd'] ?>','modalInstrumentos');
	if(jQuery("#modalInstrumentos").html()=='') {
		jQuery("#modalInstrumentos").html('<p>Muito obrigada por sua participa��o! Ela ser� de grande import�ncia para a avalia��o do processo de forma��o do PNAIC. Se desejar encaminhar coment�rios e sugest�es � pesquisa, envie uma mensagem para avaliacaopnaic@mec.gov.br.</p><p align="center"><input type="button" name="fim" value="Fechar" onclick="fecharInstrumentos();"></p>');
		jQuery("#modalInstrumentos").dialog('open');
	} else {
		jQuery("#modalInstrumentos").dialog('open');
	}
	
}

function verResposta(inrid) {
	if(inrid=='') {
		ajaxatualizar('requisicao=carregarInstrumentos&iusd=<?=$_SESSION['sispacto2']['professoralfabetizador']['iusd'] ?>','modalInstrumentos');
	} else {
		ajaxatualizar('requisicao=carregarInstrumentos&iusd=<?=$_SESSION['sispacto2']['professoralfabetizador']['iusd'] ?>&inrid='+inrid,'modalInstrumentos');
	}
	jQuery("#modalInstrumentos").dialog('open');
}

<?

$intline = verificarInstrumentosPendentes(array('iusd'=>$_SESSION['sispacto2']['professoralfabetizador']['iusd']));

if($intline) :
?>
jQuery(document).ready(function() {

	jQuery("#modalInstrumentos").html('<div style=\"text-align:center;width: 80%;padding: 10px;border: 3px solid black;margin: 0px;\"><p>Caro Professor Alfabetizador,<p> <p>Convidamos voc� a participar da <b>Avalia��o das A��es de Forma��o Continuada do PNAIC</b>. A sua opini�o � de extrema import�ncia para a melhoria do processo de forma��o, pois ela produzir� informa��es que buscar�o representar a realidade do processo de forma��o no seu munic�pio, estado e regi�o.<p> <p>Sinta-se totalmente � vontade para dizer exatamente o que voc� pensa pois esta avalia��o � totalmente sigilosa.<p> <p>Desde j� agradecemos sua colabora��o.</p><p align="center"><input type="button" name="iniciar" value="Iniciar" onclick="exibirInstrumentos();"></p></div>');
	
	jQuery("#modalInstrumentos").dialog({
        draggable:true,
        resizable:true,
        width: 800,
        height: 600,
        modal: true,
     	close: function(){} 
    });


});
<? 
endif;
?>
</script>
<style>
.ui-dialog-title {
  text-align: center;
  width: 100%;
}
</style>

<div id="modalInstrumentos" style="display: none;" title="PESQUISA"></div>

<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2">Principal</td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?></td>
</tr>
<tr>
	<td colspan="2">
	<? carregarInformes(array('pflcoddestino' => PFL_PROFESSORALFABETIZADOR)); ?>
	</td>
</tr>
<? 

$perfis = pegaPerfilGeral();
if(!$perfis) $perfis = array(); 

?>
<? if($_SESSION['sispacto2']['professoralfabetizador']['iusdesativado']!='t') : ?>
	<tr>
		<td class="SubTituloDireita" width="20%">&nbsp;</td>
		<td>
			<input type="button" value="Pr�ximo" onclick="divCarregando();window.location='sispacto2.php?modulo=principal/professoralfabetizador/professoralfabetizador&acao=A&aba=dados';">
		</td>
	</tr>
<? elseif($db->testa_superuser() || in_array(PFL_CONSULTAMEC,$perfis)) : ?>
	<tr>
		<td colspan="2">
			<p>Os perfis de Adminstrador e Consulta MEC podem acessar as informa��es dos professores clicando</p>
			<input type="button" value="Acessar Professor Desabilitado" onclick="divCarregando();window.location='sispacto2.php?modulo=principal/professoralfabetizador/professoralfabetizador&acao=A&requisicao=visualizarDesabilitado&vis=professoralfabetizador';">
		</td>
	</tr>
<? endif; ?>
</table>