<?php 

include  APPRAIZ."includes/cabecalho.inc";

?>
<style>
<!--
.apresentacaoSinafor{
	width: 90%;
	border: 1px solid black;
	text-align: left;
	margin: 15px;
	padding: 15px;
	font-size: 14px;
}
-->
</style>

<script>

function acessarLista(){
	document.location.href = 'snf.php?modulo=principal/listaEscolas&acao=A';
}

</script>

<center>
	<div class="apresentacaoSinafor" >
	
		<center><h3>Seja bem-vindo ao Sinafor!<h3></h3></center>
		 
		<p>O Sistema Nacional de Forma��o � o m�dulo do SIMEC no qual as Secretarias de Educa��o realizam a prioriza��o e 
		valida��o dos Planos de Forma��o elaborado pelas escolas no m�dulo PDE Interativo.</p> 
		
		<p>Esta etapa do novo fluxo de demanda e oferta de forma��o serve para que a dire��o da Rede de Ensino defina escolas 
		priorit�rias, restrinja o n�mero de professores que estar�o em forma��o simultaneamente e concretize seu planejamento 
		do ano, al�m de j� ter dados para iniciar o planejamento dos anos seguintes.</p> 
		
		<p>Os crit�rios de prioriza��o s�o definidos por cada Secretaria. Para defini-los, recomenda-se que �a equipe de an�lise 
		seja formada pelos t�cnicos respons�veis pelo Plano de A��es Articuladas (PAR), pela forma��o docente na Secretaria e 
		pelo Comit� do PDE Escola. Este grupo de se reunir e definir os par�metros de valida��o.</p> 
		
		<p>A defini��o de crit�rios pr�vios, que ser�o utilizados para validar as indica��es, facilita a an�lise e, principalmente, 
		torna o processo mais transparente junto �s escolas. Ao final desta etapa, o sistema devolver� as informa��es sobre 
		aprova��o para o PDE Interativo e cada docente poder� saber se foi ou n�o aprovado. Entre os crit�rios que podem ser 
		considerados, destacam-se: Resultado das escolas no IDEB, localiza��o das escolas, �adequa��o dos cursos solicitados 
		�s necessidades atuais das escolas, investimento anterior nos docentes e gestores�, �experi�ncia individual, tipo de 
		v�nculo Profissional�, tempo de carreira, comprometimento, apoio institucional e percentual de pessoas indicadas em 
		rela��o ao total da rede.</p>
		
	</div>
	<input type="button" value="Acessar" onclick="acessarLista()" /> 
</center>