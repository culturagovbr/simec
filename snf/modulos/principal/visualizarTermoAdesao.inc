<?php
//Chamada de programa
include  APPRAIZ."includes/cabecalho.inc";
?>
<br />
<?php $db->cria_aba($abacod_tela,$url,''); ?>
<?php monta_titulo( "Termo de Ades�o", '&nbsp' ); ?>
<link href="../snf/geral/snf.css" type="text/css" rel="stylesheet"></link>
<table align="center" width="95%" border="0" cellpadding="0" cellspacing="0" class="tabela">
	<tr>
		<td align="center" >
			<fieldset style="text-align:justify;width:60%;font-size:14px;margin:5px"  >
				<legend>Instru��es</legend>
				<center><img src="../imagens/brasao.jpg" style="width:100px;height:100px"   /></center>
				<h3 class="center" >MINIST�RIO DA EDUCA��O</h3>
				<h3 class="center" >TERMO DE ADES�O � REDE NACIONAL DE FORMA��O CONTINUADA DOS PROFISSIONAIS DO MAGIST�RIO DA EDUCA��O B�SICA P�BLICA</h3>
				<p>O(A) [nome e sigla da Institui��o], inscrita no CNPJ sob o n� _______________, neste ato representado(a) por seu(sua) dirigente m�ximo(a),  [nome e CPF], resolve formalizar sua ades�o � Rede Nacional de Forma��o Continuada dos Profissionais do Magist�rio da Educa��o B�sica P�blica.</p>
				<h4>DO OBJETIVO</h4>
				<p>CL�USULA PRIMEIRA � Participar como Institui��o de Ensino Superior formadora da oferta de cursos e programas no �mbito da Rede Nacional de Forma��o Continuada dos Profissionais do Magist�rio da Educa��o B�sica P�blica, nos termos da Portaria MEC n� 1.328, de 23 de setembro de 2011, publicada na p�gina 14 da se��o 01 do Di�rio Oficial da Uni�o no dia 26 de setembro de 2011 e das demais normas que venham a substituir ou complementar a legisla��o vigente, habilitando-se ao recebimento de recursos do MEC destinados a fomentar as a��es da Rede e em atendimento �s demandas de forma��o continuada formuladas nos planos estrat�gicos de que tratam os artigos 4�, 5�, e 6� do Decreto n� 6.755, de 29 de janeiro de 2009.</p>
				<h4>DA ADES�O</h4>
				<p>CL�USULA SEGUNDA � Esta ades�o, solicitada de forma eletronicamente pelo titular da Institui��o de Ensino Superior ou Instituto Federal de Educa��o, Ci�ncia e Tecnologia, junto com o ato de nomea��o do signat�rio e do ato constitutivo do Comit� Gestor Institucional de Forma��o de Profissionais do Magist�rio da Educa��o B�sica, tem efic�cia ap�s valida��o pelo Minist�rio da Educa��o.</p>
				<p>Par�grafo �nico: O apoio financeiro concedido � Institui��o de Ensino Superior ou Instituto Federal de Educa��o, Ci�ncia e Tecnologia ser� realizado a partir do pr�ximo exerc�cio fiscal, desde que a ades�o ocorra at� 31 de maio, ou somente a partir do exerc�cio seguinte, se a ades�o for posterior.</p>
				<h4>DA PARTICIPA��O</h4>
				<p>CL�USULA TERCEIRA � A ades�o abrange Institui��es de Educa��o Superior (IES), p�blicas e comunit�rias sem fins lucrativos, e Institutos Federais de Educa��o, Ci�ncia e Tecnologia (IF) habilitados a ofertar cursos ou programas de forma��o continuada aos profissionais do magist�rio da educa��o b�sica de forma articulada com os sistemas de ensino e com os F�runs Estaduais Permanentes de Apoio � Forma��o Docente.</p>
				<p>Par�grafo �nico: Os cursos e programas de forma��o continuada, ap�s homologa��o no Comit� Gestor Institucional de Forma��o de Profissionais do Magist�rio da Educa��o B�sica, dever�o ser submetidos pelas Institui��es de Ensino Superior e Institutos Federais de Educa��o, Ci�ncia e Tecnologia, periodicamente, nos termos e prazos definidos pelos F�runs Estaduais Permanentes de Apoio � Forma��o Docente, para posterior aprova��o do fomento pelo MEC.</p> 
				<h4>DA VIG�NCIA</h4>
				<p>CL�USULA QUARTA � Uma vez formalizada a ades�o � Rede Nacional de Forma��o Continuada dos Profissionais do Magist�rio da Educa��o B�sica P�blica, sua vig�ncia � v�lida por tempo indeterminado, ou at� que seja solicitado o seu cancelamento pela Institui��o de Ensino Superior ou Instituto Federal de Educa��o, Ci�ncia e Tecnologia, a qualquer tempo, mediante of�cio assinado por seu titular ao Comit� Gestor da Pol�tica Nacional de Forma��o Inicial e Continuada de Profissionais da Educa��o B�sica, implicando a interrup��o definitiva do apoio financeiro aos cursos e programas fomentados pelo MEC.</p>
				<h4>DA ALTERA��O OU DESIST�NCIA</h4>
				<p>CL�USULA QUINTA � Fica a Institui��o de Ensino Superior ou Instituto Federal de Educa��o, Ci�ncia e Tecnologia obrigado a solicitar a altera��o do Plano de Trabalho para fomento de cursos e programas no �mbito da Rede Nacional de Forma��o Continuada dos Profissionais do Magist�rio da Educa��o B�sica, sempre que caracterizada necessidade de altera��o ou desist�ncia de oferta, mediante envio de of�cio do titular ao Comit� Gestor da Pol�tica Nacional de Forma��o Inicial e Continuada de Profissionais da Educa��o B�sica, para interrup��o do apoio financeiro, com dura��o sujeita aos mesmos prazos descritos no par�grafo segundo da Cl�usula Segunda.</p>
				<h4>DA PUBLICIDADE</h4>
				<p>CL�USULA SEXTA � As op��es por ades�o, seu cancelamento, altera��o ou desist�ncia de oferta ser�o divulgadas em listas publicadas no Portal do Minist�rio da Educa��o na internet.</p>
				<p>E, por estar de acordo com todas as condi��es e cl�usulas deste Termo de Ades�o, firmo o presente instrumento.</p>
   				<p><input type="checkbox" /> Declaro que li e concordo com o Termo de Ades�o.</p>
				<p class="center" >(Local e data)</p>
				<p class="center" >[Nome do dirigente m�ximo da Institui��o]</p>
				<p class="center" >[CPF]</p>
			</fieldset>
		</td>
	</tr>
	<tr class="center SubtituloTabela" >
		<td>
			<input type="button" onclick="window.location.href='snf.php?modulo=inicio&acao=C'" name="btn_voltar" value="Voltar"  />
		</td>
	</tr>
</table>
