<?php

	if($_REQUEST['info_curso'] == 'S'){ 
		
		if($_REQUEST['sucid']) {
			$sucid = $_REQUEST['sucid'];
			$where .= "Where sucid = '$sucid'";
		}		
		
		echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../includes/Estilo.css\"/>";
		echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../includes/listagem.css\"/>";		
		echo "<table class=tabela bgcolor=#f5f5f5 cellSpacing=1 cellPadding=3 align=center>";
		$sql = "
			Select 	upper(foccurso) as foccurso, 
					upper(focdescricao) as focdescricao, 
					upper(focjustificativa) as focjustificativa
			From snf.sugestaocurso
			$where
		";
		$t = $db->pegaLinha($sql);
		$_SESSION['titulo'] = $t['foccurso']; 
		
		echo "<td class=SubTituloCentro>Informa��o sobre o curso: ".$_SESSION['titulo']."</td>";
		echo "<tr>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>";
		
		$cabecalho = array("Curso.", "Descri�ao do Curso", "Justificativa pelo Curso");
		$db->monta_lista_simples($sql, $cabecalho, 1000000, 10, 'N', '100%', $par2);
		
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		exit;
	}
?>

<?php
	if($_REQUEST['requisicao']){
		$_REQUEST['requisicao']($_REQUEST);
		die;
	}

	include APPRAIZ. "includes/cabecalho.inc";
	print "<br/>";

	$abacod_tela = 57558;
	$url = 'snf.php?modulo=principal/sugestaoCurso&acao=A';
	$parametros = null;
	$arMnuid = array();
	$db->cria_aba($abacod_tela, $url, $parametros, $arMnuid);

	monta_titulo('Sugest�o de Curso' , obrigatorio()." Indica campos obrigat�rios.");	
?>

	<script type="text/javascript" src="../includes/funcoes.js"></script>
	<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../snf/geral/snf.css" ></link>
	<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
	
	<script>
	jQuery(document).ready(function(){
		jQuery('#salvar').click(function(){
			if( document.formulario.foccurso.value == '' ){
				alert( "O campo Curso � obrigat�rio." );
				return false;
			}
			if( document.formulario.focdescricao.value == '' ){
				alert( "O campo Descri��o � obrigat�rio." );
				return false;
			}
			if( document.formulario.focjustificativa.value == '' ){
				alert( "O campo Justificativa � obrigat�rio." );
				return false;
			}
			if( document.formulario.fexid[0].value == '' ){
				alert( "O campo Fun��o Exercida � obrigat�rio." );
				return false;
			}
			if( document.formulario.pk_cod_etapa_ensino[0].value == '' ){
				alert( "O campo Etapa de ensino em que leciona � obrigat�rio." );
				return false;
			}
			if( document.formulario.pk_cod_disciplina[0].value == '' ){
				alert( "O campo Disciplina em que leciona � obrigat�rio." );
				return false;
			}
			if( document.formulario.pk_cod_mod_ensino[0].value == '' ){
				alert( "O campo Modalidade em que leciona � obrigat�rio." );
				return false;
			}
			
			selectAllOptions(document.formulario.fexid);
			selectAllOptions(document.formulario.pk_cod_etapa_ensino);
			selectAllOptions(document.formulario.pk_cod_disciplina);
			selectAllOptions(document.formulario.pk_cod_mod_ensino);
	
			jQuery('#requisicao').val('enviarcurso');
			jQuery('#formulario').submit();
		});
	});
	
	function informacaoCurso(sucid) {
		if(document.getElementById('info_curso').value != '') {
			var formulario = document.formulario;
			//formulario.target = 'InformacaoCurso';
			var janela = window.open('snf.php?modulo=principal/sugestaoCurso&acao=A&info_curso=S&sucid='+sucid, '', 'width=660,height=345,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
			formulario.submit();
			janela.focus();
		} else {
			alert("Selecione um relat�rio");
			return false;
		}
	}
	
	function salvar_sugestao(){
		var sucid = jQuery('input:radio[name=sucid]:checked').val();
		if(sucid > 0){
			jQuery.ajax({
				   type: "POST",
				   url: 'snf.php?modulo=principal/sugestaoCurso&acao=A',
				   data: "requisicao=set_curso_sugestao&sucid=" + sucid,
				   success: function(msg){
				   		if(msg != "OK"){
				   			window.location.href = window.location;
				   		}else{
				   			alert('N�o foi poss�vel salvar o curso selecionado.');
				   		}
				   }
				 });
		}else{
			alert( "� necessario selecionar um curso." );
			return false;
		}
	}
	
	</script>

	<form id="formulario" name="formulario" action="#" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="requisicao" 	id="requisicao" value="">
		<input type="hidden" id="info_curso" name="info_curso" value="info_curso"/>
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="10" align="center">
			<tr>
				<td class="blue subtituloDireita" width="10%">Orienta��es</td>
				<td class="blue" colspan="2">
				<p>
					Nesta aba, aparecem as sugest�es feitas pelas escolas de novos cursos que poderiam 
					aparecer como op��o aos professores. Cada escola p�de sugerir apenas um curso.
					</p>
					<p>
		 			A Secretaria de Educa��o tamb�m pode enviar somente uma sugest�o de curso ao F�rum Estadual 
		 			Permanente de Apoio � Forma��o Docente. Pode ser um curso escolhido entre aqueles sugeridos pela 
		 			escola ou pode-se fazer uma nova sugest�o de curso.
		 			</p>
					<p>
					Caso seja feita op��o por um curso sugerido por uma escola, basta selecionar e salvar. Caso a op��o seja por 
					sugerir um novo curso, deve-se preencher o formul�rio abaixo e salvar. Neste momento, o curso vai ser inclu�do 
					na tabela onde constam as sugest�es das escolas. Deve-se marcar o curso preenchido pela Secretaria e salvar 
					novamente.
					</p>
				</td>
			</tr>
			<tr>
				<td align="right">Nome do Curso:</td>
				<td><?php echo campo_texto("foccurso",'S','S','Nome do Curso',"95","100","","","","","","id='foccurso'","") ?></td>
			</tr>
			<tr>
				<td align="right">Descri��o:</td>
				<td><?=campo_textarea('focdescricao', 'S', 'S', 'Descri��o', '100', '3', '500'); ?></td>
			</tr>
			<tr>
				<td align="right">Justificativa e diferen�a em rela��o aos cursos j� apresentados:</td>
				<td><?=campo_textarea('focjustificativa', 'S', 'S', 'Justificativa', '100', '5', '500'); ?></td>
			</tr>
			<tr>
				<td align="right">P�blico Alvo:</td>
				<td>Fun��o Exercida:<br />
					<?php 
						$sql = "
							Select	fexid AS codigo, 
									fexdesc AS descricao
							From catalogocurso.funcaoexercida
							Where fexstatus = 'A'
						 	Order by fexdesc
						 ";
						combo_popup('fexid', $sql, 'Fun��o', '400x400', '', '', '', 'S', '', '', 5, 500);
					?>
					
					<img border="0" src="../imagens/obrig.gif"><br/><br/>Etapa de ensino em que leciona:<br/>
					<?php 
						$sql = "
							Select	pk_cod_etapa_ensino AS codigo, 
									no_etapa_ensino AS descricao
							From educacenso_2011.tab_etapa_ensino
							order by cod_etapa_ordem
						";
						combo_popup('pk_cod_etapa_ensino', $sql, 'Etapas', '400x400', '',	'', '', 'S', '', '', 5, 500);
					?>
					
					<img border="0" src="../imagens/obrig.gif"><br/><br/>Disciplina em que leciona:<br/>
					<?php			
						$sql = "
							Select 	pk_cod_disciplina as codigo, 
									no_disciplina as descricao
							From educacenso_2011.tab_disciplina
							order by no_disciplina
						";
						combo_popup('pk_cod_disciplina', $sql, 'Disciplina', '400x400', '','', '', 'S', '', '', 5, 500);
					?>
					
					<img border="0" src="../imagens/obrig.gif"><br /><br />Modalidade em que leciona:<br />
					<?php 
						$sql = "
							Select  pk_cod_mod_ensino AS codigo, 
									no_mod_ensino AS descricao
							From educacenso_2011.tab_mod_ensino
							order by no_mod_ensino
						";
						combo_popup('pk_cod_mod_ensino', $sql, 'Modalidade', '400x400', '', '', '', 'S', '', '', 5, 500);
					
					?>
					<img border="0" src="../imagens/obrig.gif"><br /><br />
				</td>
			</tr>
		</table>			
		<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
			<tr>
				
				<td class="SubTituloDireita" width="20%">O que deseja fazer agora?</td>
				<td><input type="button" value="Voltar" onclick="history.back(-1);" />
					<!-- input type="button" id="salvar" value="Adicionar na Lista"  /-->
				</td>
			</tr>
		</table>
	</form>
<?php 
	$sql = "
		Select	Case when sucsugescolha = 'S' Then '<input type=\"radio\" checked name=\"sucid\" value=\"' || sucid || '\"/>' Else '<input type=\"radio\" name=\"sucid\" value=\"' || sucid || '\"/>' end as acao, 
				Case When pdicodinep = '' or pdicodinep isnull then '00000000' else pdicodinep end as pdicodinep, 
				Case When pdenome = '' or pdenome isnull then 'Inserido pela Secret�ria' else pdenome end as pdenome,
				pdiesfera,
				'<a href=\"javascript:void(0)\" onclick=\"informacaoCurso('||sucid||')\">&nbsp;'||foccurso||'</a>' as sucid
				
		From snf.sugestaocurso
		where pdiesfera = '".$_SESSION['snf']['esfera']."' and estuf = '".$_SESSION['snf']['est_uf']."' and muncod = '".$_SESSION['snf']['mun_cod']."'  
  		order by sucid desc
	";
//ver($sql, d)
	$cabecalho 		= array('A��o','C�digo INEP','Escola','Esfera','Curso');
	$db->monta_lista($sql, $cabecalho, 100, 10, 'N','','','','form_sugestao','','');

?>
	<!-- table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" width="95%">
		<tr>
			<td class="SubTituloDireita" width="20%">Salvar o curso Selecionado acima?</td>
			<td>
				<input type="button" name="salvar_sugestao" id="salvar_sugestao" value="Salvar o curso escolhido" onclick="salvar_sugestao();" />
			</td>
		</tr>
	</table -->