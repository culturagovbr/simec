<?php


if($_REQUEST['requisicao']){
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}

include  APPRAIZ."includes/cabecalho.inc";

echo "<br/>";

?>
<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
<script>
function setarInuId(estuf) {
	window.location='snf.php?modulo=principal/diagnosticoPar&acao=A&requisicao=setarInuIdPorEstuf&estuf='+estuf+'&goto=ide';
}
</script>
<?

$perfis = pegaPerfilGeral();

if((in_array(PERFIL_FORUM_APROVACAO,$perfis) || in_array(PERFIL_FORUM_DISTRIBUICAO,$perfis))) {
	
	$sql = "SELECT DISTINCT estuf FROM snf.usuarioresponsabilidade WHERE pflcod IN('".PERFIL_FORUM_APROVACAO."','".PERFIL_FORUM_DISTRIBUICAO."') AND usucpf='".$_SESSION['usucpf']."' AND rpustatus='A'";
	$estuf = $db->pegaUm($sql);
	
	if($estuf) setarInuIdPorEstuf(array("estuf" => $estuf));
	
}

/* montando o menu */
echo montarAbasArray(montaMenuAtendimentoForum(), $_SERVER['REQUEST_URI']);
/* fim montando o menu */

?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
	<thead>
		<th class="tdDegradde01" colspan="2" align="left">
			<h1 class="notprint">
				PARFOR
			</h1>
		</th>
	</thead>

	<tr>
		<td>

<p>O Plano Nacional de Forma��o de Professores da Educa��o B�sica � PARFOR visa induzir e fomentar a oferta emergencial de vagas em cursos de educa��o superior, gratuitos e de qualidade, nas modalidades presencial e a dist�ncia, para professores em exerc�cio na rede p�blica de educa��o b�sica, a fim de que estes profissionais possam ter acesso � forma��o exigida pela Lei de Diretrizes e Bases da Educa��o Nacional � LDBEN, contribuam para a melhoria da qualidade da educa��o b�sica.</p>  
<p>O acesso dos docentes aos cursos do Parfor � realizado por interm�dio da oferta de turmas em cursos de licenciatura, programas de segunda licenciatura � ofertado somente na modalidade a presencial - e forma��o pedag�gica das Institui��es de Educa��o Superior � IES.</p>
<p>Podem se pr�-inscrever nos cursos de licenciatura, docentes em exerc�cio na rede p�blica da educa��o b�sica que n�o tenham forma��o superior, ou que, mesmo tendo essa forma��o, se disponham a realizar o curso de licenciatura na �rea em que atua em sala de aula.</p>
<p>Nos Programas de Segunda Licenciatura, podem se pr�-inscrever docentes que j� possuam forma��o em licenciatura, mas que atuem em �rea distinta dessa forma��o. Nesse caso os docentes dever�o comprovar ter pelos menos tr�s anos de exerc�cio no magist�rio na educa��o b�sica e realizar sua pr�-inscri��o no curso correspondente � disciplina que ministra em sala de aula.</p> 
<p>Nos Programas de Forma��o pedag�gica, podem se pr�-inscrever docentes graduados n�o licenciados que se encontram no exerc�cio da doc�ncia na rede p�blica da educa��o b�sica.</p>
<p>Para concorrer � vaga nos cursos ofertados, os docentes devem: a) realizar seu cadastro e pr�- inscri��o na Plataforma Freire; b) estar cadastrado no Educacenso na fun��o docente da rede p�blica de educa��o b�sica; e ter sua pr�-inscri��o validada pela Secretaria de educa��o ou �rg�o equivalente a  que estiver vinculado.</p>
<p>A pr�-inscri��o n�o garante a matr�cula. A efetiva��o da matr�cula est� condicionada � comprova��o dos requisitos para a participa��o e �s normas e procedimentos acad�micas da Institui��o de Educa��o Superior - IES ofertante do curso.</p>
<p>Para informa��es sobre calend�rio clique no link <a href="http://freire.mec.gov.br/index/calendario" target="_blank">http://freire.mec.gov.br/index/calendario</a>.</p>

		</td>
	</tr>
</table>

