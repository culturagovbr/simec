<?php
unset($_SESSION['snf']['insid']);

//Chamada de programa
include  APPRAIZ."includes/cabecalho.inc";
$arrPerfil = pegaPerfilGeral();
?>
<br />
<?php 
	monta_titulo( "Apresenta��o", '&nbsp' ); 
?>
<link href="../snf/geral/snf.css" type="text/css" rel="stylesheet"></link>
<table align="center" width="95%" border="0" cellpadding="0" cellspacing="0" class="tabela">
	<tr>
		<td align="center" >
			<div class="termoAdesao" >
				<h2 class="center" >Apresenta��o</h2>
				<h4 class="center" >Rede Nacional de Forma��o Continuada dos Profissionais do Magist�rio da Educa��o B�sica</h4>
				<p><b>Institui��o:</b> <a target="_blank" title="Portaria 1.328"  href="../snf/geral/portaria_1328_2011.pdf"  >Portaria n� 1.328</a>, de 23 de setembro de 2011, publicada no Di�rio Oficial da Uni�o de 26 de setembro de 2011, se��o 1, p�gina 14.</p>
				<p><b>Composi��o:</b> � formada pelas Institui��es de Educa��o Superior (IES), p�blicas e comunit�rias sem fins lucrativos, e pelos Institutos Federais de Educa��o, Ci�ncia e Tecnologia (IF).</p>
				<p><b>Objetivo:</b> apoiar as a��es de forma��o continuada de profissionais do magist�rio da educa��o b�sica e em atendimento �s demandas de forma��o continuada formuladas nos planos estrat�gicos de que tratam os artigos 4�, 5�, e 6� do <a target="_blank" title="Portaria 1.328"  href="../snf/geral/decreto_6755_2009.pdf"  >Decreto n� 6.755</a>, de 29 de janeiro de 2009.</p>
				<p><b>Coordena��o e supervis�o:</b> exercida pelo Comit� Gestor da Pol�tica Nacional de Forma��o Inicial e Continuada de Profissionais da Educa��o B�sica, institu�do pela <a target="_blank" title="Portaria 1.328"  href="../snf/geral/portaria_1087_2011.pdf"  >Portaria n� 1.087</a>, de 10 de agosto de 2011.</p>
				<p><b>Forma de atua��o:</b> em articula��o com os sistemas de ensino e com os F�runs Estaduais Permanentes de Apoio � Forma��o Docente.</p>
				<p><b>Ingresso das IES e IF � Rede:</b> por meio do termo de ades�o eletr�nico dispon�vel abaixo, tendo como anexo o ato constitutivo do Comit� Gestor Institucional de Forma��o de Profissionais do Magist�rio da Educa��o B�sica, conforme disposto na <a target="_blank" title="Portaria 1.328"  href="../snf/geral/resolucao_1_2011.pdf"  >Resolu��o n� 1</a>, de 17 de agosto de 2011, do Comit� Gestor da Pol�tica Nacional de Forma��o Inicial e Continuada de Profissionais da Educa��o B�sica.</p>
			</div>
		</td>
	</tr>
	<tr class="center SubtituloTabela" >
		<td>
			<input type="button" onclick="window.location.href='snf.php?modulo=principal/visualizarTermoAdesao&acao=A'" name="btn_visualizar_modelo" value="Visualizar Termo de Ades�o"  />
			<?php if(!in_array(PERFIL_CONSULTA,$arrPerfil)): ?>
				<input type="button" onclick="window.location.href='snf.php?modulo=principal/aderirTermoAdesao&acao=A'" onclick="" name="btn_proceder_adesao" value="Proceder � Ades�o"  />
			<?php endif; ?>
		</td>
	</tr>
</table>
