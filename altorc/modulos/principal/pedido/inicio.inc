<?php
/**
 * Cria��o/Edi��o de novo pedido de altera��o or�ament�ria.
 * $Id: inicio.inc 96859 2015-05-04 20:39:09Z maykelbraz $
 */

/**
 * Fun��es de apoio ao pedido de altera��o or�ament�ria.
 * @see _funcoesalteracaoorcamentaria.php
 */
require APPRAIZ . "www/altorc/_funcoesalteracaoorcamentaria.php";

// -- Valida��o da sess�o, que as vezes est� quebrando
if (!session_id()) {
    header('Loaction: altorc.php?modulo=inicio&acao=C');
    die();
}

// -- Mensagens para a interface do usu�rio
$fm = new Simec_Helper_FlashMessage('altorc/pedido');
// -- Momento de cr�dito atual - para limita��o de permiss�es
list($mcridAtual, $mcrtipocancelamento) = momentoDeCreditoAtual($_SESSION['exercicio'], array(), true);

// -- Carregando informa��es do pedido
$dadospao = array();
if (dadosAlteracaoOrcamentariaNaSessao() // -- 1: Dados j� armazenados na sess�o
    // -- 2: Pedido j� criado, mas dados n�o armazenados na sess�o OU novo pedido
    || dadosDoPedidoAlteracaoOrcamentaria($_REQUEST['dados'])) {
    // -- Copia os dados da sess�o para a vari�vel
    $dadospao = &$_SESSION['altorc']['pedido']['dados'];
    // -- Verificando se um novo pedido pode ser criado
    if (('novoPedido' == $_REQUEST['requisicao']) && ($_REQUEST['dados']['mcrid'] != $mcridAtual)) {
        $fm->addMensagem(
            'N�o � poss�vel criar novos pedidos para o momento de cr�dito escolhido, pois o este momento n�o est� aberto.',
            Simec_Helper_FlashMessage::ERRO
        );
        header('Location: altorc.php?modulo=principal/pedido&acao=A');
        die();
    }
} else {
    $fm->addMensagem(
        'Os dados do pedido n�o foram encontrados.',
        Simec_Helper_FlashMessage::ERRO
    );
    header('Location: altorc.php?modulo=principal/pedido&acao=A');
    die();
}

// -- Processamento de requisi��es
// -- todo: fazer redirecionamento pra tirar mensagem de reenviar formul�rio
if (chaveTemValor($_POST, 'requisicao')) {
    switch ($_POST['requisicao']) {
        case 'programas':
            echo carregarProgramas($_POST['dados']['acacod'], $_SESSION['exercicio'], true, $_POST['dados']['unicod']);
            die();
        case 'salvarFisico':
            // -- Tentando salvar as altera��es f�sicas do localizador
            if (salvarFisicoDoLocalizador($dadospao, $_POST['valacrescimo'], $_POST['valreducao'])) {
                $fm->addMensagem('Altera��o do f�sico realizada com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel executar a altera��o do f�sico.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'alterarPO':
            // -- Tentando salvar as altera��es do PO
            if (salvarFinanceiroDoPO($_POST['dados'], $dadospao)) {
                $fm->addMensagem('Altera��o do financeiro realizada com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel executar a altera��o do financeiro.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'criarPO';
            // -- Tentando salvar as altera��es do PO
            if (salvarFinanceiroDoPO($_POST['dados'], $dadospao, 'N')) {
                $fm->addMensagem('Nova entrada financeira criada com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel criar a nova entrada financeira.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'salvarJustificativas':
            if (salvarJustificativas($_POST['just'], $dadospao['paoid'], $_SESSION['usucpf'], $_SESSION['usunome'])) {
                $fm->addMensagem('As justificativas foram salvas com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel salvar as justificativas.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'excluirCredito':
            if (excluirCredito($_POST['dados'])) {
                $fm->addMensagem('O cr�dito selecionado foi exclu�do com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel excluir o cr�dito selecionado.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'limparCredito':
            if (limparCredito($_POST['dados'])) {
                $fm->addMensagem('O cr�dito selecionado foi limpo com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel limpar o cr�dito selecionado.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'limparFuncional':
            if (limparFuncional($_POST['dados'])) {
                $fm->addMensagem('O localizador selecionado foi limpo com sucesso.');
            } else {
                $fm->addMensagem('N�o foi poss�vel limpar o localizador selecionado.', Simec_Helper_FlashMessage::ERRO);
            }
            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
        case 'detalharLocalizador':
            $dadospao['snaid'] = $_POST['dados'][0];
            $dadospao['paoid'] = $_POST['dados'][1];
            $detalharPO = true;
            require(dirname(__FILE__) . '/detalhamento/listarAlteracoesPO.inc');
            die();
        case 'alterarTipoCredito':
            if(alterarTipoCredito($_REQUEST['dados']['paoid'], $_REQUEST['dados']['tcrid'])){
                $fm->addMensagem ('Tipo de cr�dito alterado com sucesso.');
                dadosDoPedidoAlteracaoOrcamentaria($dadospao);
            }else
                $fm->addMensagem ('N�o foi poss�vel alterar o tipo de cr�dito.',  Simec_Helper_FlashMessage::ERRO);

            header('Location: ' . $_SERVER['REQUEST_URI']);
            die();
    }
// -- Requisi��o de edi��o de um localizador
} elseif (chaveTemValor($_REQUEST, 'dados') && chaveTemValor($_REQUEST['dados'], 'snaid')) {
    $_SESSION['altorc']['pedido']['dados']['snaid'] = $_REQUEST['dados']['snaid'];
    header('Location: ' . substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '&')));
    die();
} elseif (chaveTemValor($_REQUEST, 'target')) {
    header('Location: ' . substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '?'))
        . '?modulo=principal/pedido/inicio&acao=A&aba=resumo#collapseSIOP');
    die();
}

// -- Calculo de suplementa��es:
$dadosfinanceiro = suplementacoes($dadospao['paoid']);
$podeSalvar = podeAlterarPedido($dadospao['paoid']);
/**
 * Cabe�alho simec.
 * @see cabecalho.inc
 */
include APPRAIZ . "includes/cabecalho.inc";
?>
<link rel="stylesheet" href="/proporc/css/print.css" media="print" />
<style type="text/css">
label{margin-top:10px}
.page-header{margin-bottom:3px;padding-bottom:2px;font-weight:bold}
</style>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
    $('#formularioAlterarTipoCredito').css('display','none');
    $('#tcrid_chosen').css('width','100%');
    $('#btnVoltar').click(function(){
        window.location = 'altorc.php?modulo=principal/pedido&acao=A';
    });
    $('#modal-alert .modal-dialog').css('width', '80%');
});
function detalharRetornoSIOP(paoid)
{
    $.post('altorc.php?modulo=principal/pedido&acao=A', {'dados[paoid]':paoid,'requisicao':'retornoSIOP'}, function(data){
        $('#modal-alert .modal-title').text('Retorno SIOP');
        $('#modal-alert .modal-body').html(data);
        $('#modal-alert').modal();
    });
}
function alterarTipoCredito()
{
    $('#modal-confirm .modal-body p').html($('#formularioAlterarTipoCredito').css('display','inherit'));
    $('.modal-dialog').css('width','70%');
    $('#modal-confirm .modal-title').html('Alterar Tipo de Cr�dito - Pedido n� <?php echo $dadospao['paoid'] .' - '. $dadospao['paodsc']; ?>');
    $('#modal-confirm .btn-primary').html('Salvar');
    $('#modal-confirm .btn-primary').attr('onclick','atualizarTipoCredito();');
    $('#modal-confirm .btn-primary').attr('type','button');
    $('#modal-confirm .btn-default').html('Fechar');
    $('.modal-dialog').show();
    $('#modal-confirm').modal();
}

function atualizarTipoCredito()
{
    if($('#tcrid').val() == ""){
        alert('Voc� deve selecionar um Tipo de Cr�dito para prosseguir.');
    }else{
        $('#formularioAlteraTipoCredito').val('alterarTipoCredito');
        $('#formularioAlteraTC').submit();
    }
}
</script>
<div class="row col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $_SESSION['sisdiretorio']; ?>.php?modulo=inicio&acao=C"><?php echo $_SESSION['sisabrev']; ?></a></li>
        <li><a id="btnVoltar" href="#">Pedidos</a></li>
        <li class="active">Visualiza��o de pedido</li>
    </ol>
    <!-- IN�CIO FORMUL�RIO MODAL ALTERAR TIPO DE CR�DITO -->
    <div id="formularioAlterarTipoCredito">
        <form class="form-horizontal" method="post" id="formularioAlteraTC" action="">
            <input type="hidden" name="requisicao" id="formularioAlteraTipoCredito" value="" />
            <input type="hidden" name="dados[paoid]" value="<?=$dadospao['paoid']  ?>" />
            <section class="form-group">
                <label class="control-label col-md-2">Tipo de Cr�dito</label>
                <section class="col-md-10">
                    <?php
                    $opcoesTcrid = array();
                    $opcoesTcrid = carregarTipoCredito($_SESSION['exercicio'], $dadospao['mcrid'], $asJSON = false);
                    $db->monta_combo('dados[tcrid]', $opcoesTcrid, 'S', 'Selecione um Tipo de cr�dito', null, null, null, null, 'N', 'tcrid', null, $dadospao['tcrid'], null, 'class="form-control chosen-select"');
                    ?>
                </section>
            </section>
        </form>
    </div>
    <!-- FIM -- FORMUL�RIO MODAL ALTERAR TIPO DE CR�DITO -->
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong>
                <?php if (!isset($dadospao['paoid'])) {
                    echo 'Informa��es do novo pedido';
                } else {
                    echo "Informa��es do pedido n� {$dadospao['paoid']} - {$dadospao['paodsc']}";
                }
                ?>
                </strong>
            </div>
            <table class="table">
                <tr>
                    <td><strong>Momento de cr�dito:</strong></td>
                    <td style="width:60%"><?php echo $dadospao['mcrdsc']; ?></td>
                    <td><strong>IUSIOP:</strong></td>
                    <td><?php echo $dadospao['siopid']; ?></td>
                </tr>
                <tr>
                    <td><strong>Unidade Or�ament�ria:</strong></td>
                    <td><?php echo $dadospao['unidsc']; ?></td>
                    <td><strong>Status:</strong></td>
                    <td><?php echo statusNoSIOP($dadospao['paostatus'], array('paoid2' => $dadospao['paoid'])); ?></td>
                </tr>
                <tr>
                    <td><strong>Tipo de cr�dito:</strong></td>
                    <td><?php echo $dadospao['tcrcod'] . ' - '. $dadospao['tcrdsc']; ?></td>
                    <td colspan="2" style="text-align:center">
                    <? if (isset($_REQUEST['dados']['paoid'])): ?>
                        <button type="button" class="btn btn-info" onclick="alterarTipoCredito();">
                            <span class="glyphicon glyphicon-edit"></span> Alterar tipo de Cr�dito
                        </button>
                    <? endif; ?>
                    <? if (isset($_REQUEST['aba']) && 'resumo' == $_REQUEST['aba']): ?>
                        <button type="button" class="btn btn-success notprint" id="print-resumo">
                            <span class="glyphicon glyphicon-print"></span> Imprimir
                        </button>
                    <?php endif; ?>
                    </td>
                </tr>
            </table>
            <div class="panel-heading">
                <table style="color:whitesmoke;width:100%">
                    <tr>
                        <td style="width:16%"><strong>Sup. Cancelamento:</strong></td>
                        <td style="width:16%"><?php echo $dadosfinanceiro['supcancelamento']; ?></td>
                        <td style="width:16%"><strong>Cancelamento:</strong></td>
                        <td style="width:16%"><?php echo $dadosfinanceiro['cancelamento']; ?></td>
                        <td style="width:16%"><strong>Diferen�a:</strong></td>
                        <td style="width:16%"><?php echo $dadosfinanceiro['diferenca']; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Sup. Excesso:</strong></td>
                        <td><?php echo $dadosfinanceiro['supexcesso']; ?></td>
                        <td><strong>Sup. Superavit:</strong></td>
                        <td><?php echo $dadosfinanceiro['supsuperavit']; ?></td>
                        <td><strong>Total Suplementado:</strong></td>
                        <td><?php echo $dadosfinanceiro['suptotal']; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Sup. Opera��o Cr�dito:</strong></td>
                        <td><?php echo $dadosfinanceiro['supopcredito']; ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br style="clear:both" class="noprint" />
<?php
// -- Identificando a aba ativa
$abaAtiva = (isset($_REQUEST['aba'])?$_REQUEST['aba']:'funcionais');
// -- Limpando snaid, caso a aba funcionais seja acessada (para esconder a aba detalhamento)
if ('detalhamento' != $abaAtiva) {
    unset($dadospao['snaid']);
}

// -- URL base das abas
$urlBaseDasAbas = '/altorc/altorc.php?modulo=principal/pedido/inicio&acao=A&aba=';

$listaAbas = array();
// -- @todo: Exibir a aba de detalhamento apenas quando um localizador for selecionado
// -- abafuncionais, sempre visivel
$listaAbas[] = array("id" => 1, "descricao" => "Funcionais", "link" => "{$urlBaseDasAbas}funcionais");

// -- Exibe a aba de detalhamento, apenas se um pedido de localizador estiver sendo editado ou criado
if (isset($dadospao['snaid'])) {
    $listaAbas[] = array("id" => 2, "descricao" => "Detalhamento", "link" => "{$urlBaseDasAbas}detalhamento");
}
// -- Se j� existir um pedido (ou seja, ao menos um localizador alterado) exibe as abas de justificativa e resumo
if (isset($dadospao['paoid'])) {
    $listaAbas[] = array("id" => 3, "descricao" => "Justificativas", "link" => "{$urlBaseDasAbas}justificativa");
    $listaAbas[] = array("id" => 4, "descricao" => "Resumo / Tr�mite", "link" => "{$urlBaseDasAbas}resumo");
}

// -- HTML das abas
echo montarAbasArray($listaAbas, "{$urlBaseDasAbas}{$abaAtiva}");

/**
 * Inclu�ndo o arquivo de acordo com a aba selecionada.
 * Para cada aba, � chamado o arquivo in�cio dentro do diret�rio
 * com o mesmo nome da aba selecionada.
 */
require_once(dirname(__FILE__) . "/{$abaAtiva}/inicio.inc");
?>
</div>