<?
 /*
   Sistema Simec
   Setor responsvel: MEC
   Desenvolvedor: Equipe Consultores Simec
   Analista: Cristiano Cabral (cristiano.cabral@gmail.com)
   Programador: Cristiano Cabral (cristiano.cabral@gmail.com)
   Mdulo:inicio.inc
   Finalidade: permitir abrir a p�gina inicial
    */

//Chamada de programa
include  APPRAIZ."includes/cabecalho.inc";

$vpflcod = arrayPerfil();

if( in_array(PERFIL_SUPER_USUARIO, $vpflcod) || in_array(PERFIL_ADMINISTRADOR, $vpflcod)  ){
	die('<script>
		location.href = "?modulo=principal/painel&acao=A";
		//location.href = "?modulo=principal/lista&acao=A";
		</script>');
}else{
	die('<script>
		//location.href = "?modulo=principal/painel&acao=A";
		location.href = "?modulo=principal/lista&acao=A";
		</script>');
}

?>
<br>

<table align="center" width="95%" border="0" cellpadding="0" cellspacing="0" class="listagem2">
	<tr bgcolor="#e7e7e7">
	  <td><h1>Bem-vindo</h1></td>
	</tr>
</table>
