<?PHP
    verificaExisteSessaoMM();
    
    #ATRIBUI O VALOR NAS VARIAVEIS DE SESSAO
if( $_REQUEST['muncod'] != '' ){
        iniciaVariaveisSessao( $_REQUEST['muncod'], 'M' );
        
        if( $_SESSION['maismedicomec']['muncod'] != '' ){ 
            $etqid = cadastraEntidadeQuestionarioMunicipio( $_SESSION['maismedicomec']['muncod'] );
            
            if( $etqid != '' ){
                iniciaVariaveisSessao( $etqid, 'E' );
            }
        }
        
    }else{
        if( $_SESSION['maismedicomec']['muncod'] == '' ){ 
            $db->sucesso('principal/inicio_direcionamento', '&acao=A', 'N�o foi possiv�l acessar o sistema, Ocorreu um problema interno ou houve perca de sess�o. Tente novamente mais tarde!');
        }
    }
//      ver(ABA_QUESTIONARIO_MEDICINA);
    include_once APPRAIZ . 'includes/cabecalho.inc';
    echo '<br>';
    $abacod_tela    = ABA_QUESTIONARIO_MEDICINA;
    $url            = 'maismedicomec.php?modulo=principal/instrumentoavaliacao/maisMedicoQuestAvaliacao&acao=A';
    $parametros     = '';

    $db->cria_aba($abacod_tela, $url, $parametros);
    
//     $_SESSION['maismedicos'] = true;    
?>

<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
<link rel='stylesheet' type='text/css' href='../includes/Estilo.css'/>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<link href="../includes/jquery-autocomplete/jquery.autocomplete.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script	type='text/javascript' src='../includes/jquery-autocomplete/jquery.autocomplete.js'></script>


<script type="text/javascript">

//     function voltarListagemMunicipio(){
//         window.location.href = 'maismedicomec.php?modulo=principal/instrumentoavaliacao/lista_grid_municipio&acao=A';
//     }
    
</script>

<form action="" method="POST" id="formulario" name="formulario">
    <input type="hidden" id="requisicao" name="requisicao" value=""/>
    <input type="hidden" id="PDF" name="PDF" value=""/>
    <input type="hidden" id="muncod" name="muncod" value="<?=$_SESSION['maismedicomec']['muncod'];?>"/>

    <div id = "div_rolagem" class="notprint">
        <table style="padding:15px; background-color:#e9e9e9; color:#404040;" align="center" border="0" class="tabela" cellpadding="3" cellspacing="1">
            <tr>
                <td colspan="2" class="subtituloCentro">
                    <input type="button" name="bt_imprimir" value="Imprimir" onclick="javascript: window.print();">
                </td>
            </tr>
        </table>
    </div>
    
    <table align="center" bgcolor="#f5f5f5" border="1" cellpadding="3" cellspacing="1" class="tabela">
        <tr>
            <td>
                <?PHP
                    $arrPerguntas = array(1,2,3,4,5);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );
                ?>
                <tr>
                    <td class ="SubTituloCentro" colspan="2" style="height: 35px;"> 1. Avalia��o documental. Relatar os documentos avaliados e a sua pertin�ncia/import�ncia.</td>
                </t>
                <?PHP
                    #perguntas inativadas: 7,8,9
                    $arrPerguntas = array(6,10,11,12,13,14,15);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );
               ?>
                <tr>
                   <td class ="SubTituloCentro" colspan="4"> 2. Descri��o e coment�rios sobre aspectos gerais da proposta.</td>
                </t>
                <?PHP
                    #PERGUNTA 16 � A PERGUNTA AGRUPADORA DAS 217,218 E 17,18,19,20,21,22,23,24,25,26,27,28,29
                    $arrPerguntas = array(16);
                    echo montaPerguntasQuestionarioImpressao( '', 'Aspectos Gerais', '', $arrPerguntas );

                    $arrPerguntas = array(217,218);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );

                    $arrPerguntas = array(17,18,19,20,21,22,23,24,25,26,27,28,29);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );

                    #BUSCA TODAS UNIDADES DE SA�DE RELACIONADAS COM O MUNIC�PIO DO TIPO 1 - HOSPITAL
                    $sql = "
                        SELECT  ussid,
                                ussdsc
                        FROM maismedicomec.unidservicosaude
                        WHERE tusid = 1 AND ussstatus = 'A' AND etqid = {$_SESSION['maismedicomec']['etqid']} 
                        ORDER BY 1
                    ";
                    $dados = $db->carregar($sql);
                    if( $dados != '' ){
                        foreach( $dados as $ussid ){
                ?>
                            <tr>
                                <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de (Hospital) - <?=$ussid['ussdsc'];?> </td>
                            </t>

                <?PHP
                            #PERGUNTA 216 � A PERGUNTA AGRUPADORA DAS 30,35,36
                            $arrPerguntas = array(216);
                            echo montaPerguntasQuestionarioImpressao( 'A - 1)', 'Unidades de Interna��o', '', $arrPerguntas, $ussid['ussid'] );

                            #PERGUNTA 216 � A PERGUNTA AGRUPADORA DAS 30,35,36
                            $arrPerguntas = array(30,35,36);
                            echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(38,39,40,41);
                            echo montaPerguntasQuestionarioImpressao( 'A - 2)', 'Unidades Cirurgicas', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(42,43,44,45,46,47,48,49);
                            echo montaPerguntasQuestionarioImpressao( 'A - 3)', 'Recursos Proped�uticos', '', $arrPerguntas, $ussid['ussid'] );

                            #perguntas inativadas: 52,53,54,55
                            $arrPerguntas = array(50,51,56,57);
                            echo montaPerguntasQuestionarioImpressao( 'A - 4)', 'Unidade de Urg�ncia/Emerg�ncia', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(58,59);
                            echo montaPerguntasQuestionarioImpressao( 'A - 5)', 'Recursos de Hemoterapia ', '', $arrPerguntas, $ussid['ussid'] );

                            #perguntas inativadas: 62,64,63,65,66
                            $arrPerguntas = array(60,67,68,69);
                            echo montaPerguntasQuestionarioImpressao( 'A - 6)', 'Resid�ncia m�dica', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(70,71);
                            echo montaPerguntasQuestionarioImpressao( 'A - 7)', 'Ambiente de Pr�tica para alunos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(72);
                            echo montaPerguntasQuestionarioImpressao( 'A - 8)', 'Papel do Hospital no servi�o de sa�de do munic�pio', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(73);
                            echo montaPerguntasQuestionarioImpressao( 'A - 9)', 'Avalia��o do gestor municipal', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(74);
                            echo montaPerguntasQuestionarioImpressao( 'A - 10)', 'Dados Estat�sticos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(215);
                            echo montaPerguntasQuestionarioImpressao( 'A - 11)', 'Coment�rios Gerais sobre o Hospital', '', $arrPerguntas, $ussid['ussid'] );
                        }
                    }
                ?>

                <tr>
                    <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de - Conjunto Hospitalar. </td>
                </t>

                <?PHP
                    $arrPerguntas = array(75,76,77,78,79);
                    echo montaPerguntasQuestionarioImpressao( 'A - 11)', 'Conjunto Hospitalar do Munic�pio', '', $arrPerguntas );

                    #BUSCA TODAS UNIDADES DE SA�DE RELACIONADAS COM O MUNIC�PIO DO TIPO 2 - UBS
                    $sql = "
                        SELECT  ussid,
                                ussdsc
                        FROM maismedicomec.unidservicosaude
                        WHERE tusid = 2 AND ussstatus = 'A' AND etqid = {$_SESSION['maismedicomec']['etqid']}
                        ORDER BY 1
                    ";
                    $dados = $db->carregar($sql);
                    if( $dados != '' ){
                        foreach( $dados as $ussid ){
                ?>
                            <tr>
                                <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de (UBS) - <?=$ussid['ussdsc'];?> </td>
                            </t>
                <?PHP
                            $arrPerguntas = array(80,81);
                            echo montaPerguntasQuestionarioImpressao( 'B - 1)', 'Instala��es F�sicas', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(82,83);
                            echo montaPerguntasQuestionarioImpressao( 'B - 2)', 'Consult�rios', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(84,85);
                            echo montaPerguntasQuestionarioImpressao( 'B - 3)', 'Sala de Curativos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(86,87);
                            echo montaPerguntasQuestionarioImpressao( 'B - 4)', 'Sala de Vacina��o', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(88,89,90);
                            echo montaPerguntasQuestionarioImpressao( 'B - 5)', 'Farm�cia B�sica', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(91,92);
                            echo montaPerguntasQuestionarioImpressao( 'B - 6)', 'Sala de Reuni�es', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(93,94);
                            echo montaPerguntasQuestionarioImpressao( 'B - 7)', 'Banheiros', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(95,96);
                            echo montaPerguntasQuestionarioImpressao( 'B - 8)', 'Internet', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(97,98);
                            echo montaPerguntasQuestionarioImpressao( 'B - 9)', 'Telesa�de', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(99,100);
                            echo montaPerguntasQuestionarioImpressao( 'B - 10)', 'Materiais e Instrumentos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(104,105,106,107,108);
                            echo montaPerguntasQuestionarioImpressao( 'B - 11)', 'ESF', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(112);
                            echo montaPerguntasQuestionarioImpressao( 'B - 12)', 'Popula��o Vinculada', '', $arrPerguntas, $ussid['ussid'] );

                            #perguntas inativadas: 113,115,116,117,119,121,122,123
                            $arrPerguntas = array(113, 119);
                            echo montaPerguntasQuestionarioImpressao( 'B - 13)', 'Normas de funcionamento', '', $arrPerguntas, $ussid['ussid'] );

                            #perguntas inativadas: 126,128
                            $arrPerguntas = array(125,127,129,130,131,132,133,134,135,136);
                            echo montaPerguntasQuestionarioImpressao( 'B - 14)', 'Atividades desenvolvidas', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(137,140,141,142,143);
                            echo montaPerguntasQuestionarioImpressao( 'B - 15)', 'Profissionais em Atividades', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(138,139);
                            echo montaPerguntasQuestionarioImpressao( 'B - 16)', 'Sistema de refer�ncia', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(144,145);
                            echo montaPerguntasQuestionarioImpressao( 'B - 17)', 'A��es de Educa��o Permanentes', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(146);
                            echo montaPerguntasQuestionarioImpressao( 'B - 18)', 'Dados Estat�sticos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(147,148);
                            echo montaPerguntasQuestionarioImpressao( 'B - 19)', 'Alunos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(149,150);
                            echo montaPerguntasQuestionarioImpressao( 'B - 20)', 'NASF', '', $arrPerguntas, $ussid['ussid'] );
                        }
                    }
                ?>

                <tr>
                   <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de - Equipe de Aten��o B�sica. </td>
                </t>

                <?PHP
                    $arrPerguntas = array(151,152);
                    echo montaPerguntasQuestionarioImpressao( 'B - 21)', ' Equipes de Unidades B�sicas', '', $arrPerguntas);

                    #BUSCA TODAS UNIDADES DE SA�DE RELACIONADAS COM O MUNIC�PIO DO TIPO 3 - UPA
                    $sql = "
                        SELECT  ussid,
                                ussdsc
                        FROM maismedicomec.unidservicosaude
                        WHERE tusid = 3 AND ussstatus = 'A' AND etqid = {$_SESSION['maismedicomec']['etqid']}
                        ORDER BY 1
                    ";
                    $dados = $db->carregar($sql);
                    if( $dados != '' ){
                        foreach( $dados as $ussid ){
                ?>
                            <tr>
                                <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de (UPA) - <?=$ussid['ussdsc'];?> </td>
                            </t>
                <?PHP
                            $arrPerguntas = array(153);
                            echo montaPerguntasQuestionarioImpressao( 'C - 1)', 'Instala��es F�sicas', 'Adequa��o, funcionalidade etc', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(154,155);
                            echo montaPerguntasQuestionarioImpressao( 'C - 2)', 'Normas de funcionamento/atendimento', '', $arrPerguntas, $ussid['ussid'] );

                            #perguntas inativadas: 157,158
                            $arrPerguntas = array(156);
                            echo montaPerguntasQuestionarioImpressao( 'C - 3)', 'Procedimentos realizados', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(159);
                            echo montaPerguntasQuestionarioImpressao( 'C - 4)', 'Leitos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(160);
                            echo montaPerguntasQuestionarioImpressao( 'C - 5)', 'Acesso a Internet', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(162,163);
                            echo montaPerguntasQuestionarioImpressao( 'C - 6)', 'Telesa�de', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(164,165);
                            echo montaPerguntasQuestionarioImpressao( 'C - 7)', 'Alunos', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(166);
                            echo montaPerguntasQuestionarioImpressao( 'C - 8)', 'Dados Estat�sticos', '', $arrPerguntas, $ussid['ussid'] );
                        }
                    }

                    #BUSCA TODAS UNIDADES DE SA�DE RELACIONADAS COM O MUNIC�PIO DO TIPO 4 - CAPS
                    $sql = "
                        SELECT  ussid,
                                ussdsc
                        FROM maismedicomec.unidservicosaude
                        WHERE tusid = 4 AND ussstatus = 'A' AND etqid = {$_SESSION['maismedicomec']['etqid']}
                        ORDER BY 1
                    ";
                    $dados = $db->carregar($sql);
                    if( $dados != '' ){
                        foreach( $dados as $ussid ){
                ?>
                            <tr>
                                <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de (CAPS) - <?=$ussid['ussdsc'];?> </td>
                            </t>
                <?PHP
                            $arrPerguntas = array(170,171,172,173,174,175,176,177);
                            echo montaPerguntasQuestionarioImpressao( 'D - 1)', 'CAPS', '', $arrPerguntas, $ussid['ussid'] );
                        }
                    }

                    #BUSCA TODAS UNIDADES DE SA�DE RELACIONADAS COM O MUNIC�PIO DO TIPO 5 - AMBULATORIO
                    $sql = "
                        SELECT  ussid,
                                ussdsc
                        FROM maismedicomec.unidservicosaude
                        WHERE tusid = 5 AND ussstatus = 'A' AND etqid = {$_SESSION['maismedicomec']['etqid']}
                        ORDER BY 1
                    ";
                    $dados = $db->carregar($sql);
                    if( $dados != '' ){
                        foreach( $dados as $ussid ){
                ?>
                            <tr>
                                <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de (Ambulat�rio) - <?=$ussid['ussdsc'];?> </td>
                            </t>
                <?PHP
                            $arrPerguntas = array(180,181);
                            echo montaPerguntasQuestionarioImpressao( 'E - 1)', 'Consult�rios', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(182,183);
                            echo montaPerguntasQuestionarioImpressao( 'E - 2)', 'Salas de Reuni�es', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(184,185);
                            echo montaPerguntasQuestionarioImpressao( 'E - 3)', 'Acesso a Internet', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(186,187);
                            echo montaPerguntasQuestionarioImpressao( 'E - 3)', 'Acesso � Telesa�de', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(188);
                            echo montaPerguntasQuestionarioImpressao( 'E - 5)', 'Especialidades Atendidas', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(189,190,191);
                            echo montaPerguntasQuestionarioImpressao( 'E - 6)', 'Profissionais em Atividade', '', $arrPerguntas, $ussid['ussid'] );

                            $arrPerguntas = array(192,194);
                            echo montaPerguntasQuestionarioImpressao( 'E - 7)', 'Alunos', '', $arrPerguntas,$ussid['ussid']);

                            $arrPerguntas = array(195);
                            echo montaPerguntasQuestionarioImpressao( 'E - 8)', 'Dados Estat�sticos', '', $arrPerguntas, $ussid['ussid']);
                        }
                    }
                ?>

                <tr>
                    <td class ="SubTituloCentro" colspan="3"> 4. Outros Servi�os de Sa�de.</td>
                </t>

                <?PHP
                    $arrPerguntas = array(196);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );
                ?>

                <tr>
                    <td class ="SubTituloCentro" colspan="3"> 5. Rede de Ate��o � sa�de.</td>
                </t>
                <?PHP
                    $arrPerguntas = array(197);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );
                ?>

                <tr>
                    <td class ="SubTituloCentro" colspan="3"> 6. Conclus�o.</td>
                </t>
                <?PHP
                    $arrPerguntas = array(220);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );

                    $arrPerguntas = array(198);
                    echo montaPerguntasQuestionarioImpressao( '', '', '', $arrPerguntas );
                ?>
            </td>
        </tr>
    </table>
    <div id = "div_rolagem" class="notprint">
        <table style="padding:15px; background-color:#e9e9e9; color:#404040;" align="center" border="0" class="tabela" cellpadding="3" cellspacing="1">
            <tr>
                <td colspan="2" class="subtituloCentro">
                    <input type="button" name="bt_imprimir" value="Imprimir" onclick="javascript: window.print();">
                </td>
            </tr>
        </table>
    </div
