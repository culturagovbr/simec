<?php
    #VERIFICA SE VARI�VEIS DE SESS�O FOI INICIADA. cASO N�O, REDIRECIONA PARA PAGINA INICIAL.
    verificaExisteSessaoMM();

    if($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
    }

    include APPRAIZ . "includes/cabecalho.inc";
    echo "<br>";
    monta_titulo('Instrumento de Avalia��o',"Servi�os de sa�de do munic�pio, {$_SESSION['maismedicomec']['mundescricao']}. <br> Candidato a sediar curso de medicina em IES privada." );
    echo "<br>";

    $abacod_tela    = ABA_QUESTIONARIO_MEDICINA;
    $url            = 'maismedicomec.php?modulo=principal/instrumentoavaliacao/perg_ss_hospital&acao=A';
    $parametros     = '';

    $tipoUnidadeSaude = 'Hospital';

    $db->cria_aba($abacod_tela, $url, $parametros);

?>

<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
<link rel='stylesheet' type='text/css' href='../includes/Estilo.css'/>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<link href="../includes/jquery-autocomplete/jquery.autocomplete.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script	type='text/javascript' src='../includes/jquery-autocomplete/jquery.autocomplete.js'></script>


<script type="text/javascript">

    function abrirGrupoPergunta(obj, opc){

        var name= $(obj).attr('name');

        if(opc == 'mais'){
            $("#sinal_mais_"+name).css("display", "none");
            $("#sinal_menos_"+name).css("display", "");

            $("."+name).css("display", "");

        }else{
            $("#sinal_mais_"+name).css("display", "");
            $("#sinal_menos_"+name).css("display", "none");

            $("."+name).css("display", "none");
        }
    }

    function buscarDadosUnidadeSaude( ussid ){

        $.ajax({
            type    : "POST",
            url     : window.location.href,
            data    : "requisicao=buscarDadosUnidadeSaude&ussid="+ussid,
            success: function(resp){
                var dados = $.parseJSON(resp);

                $.each(dados, function(index, value) {
                   $('#'+index).val(value);
                });
            }
        });
        $('#div_formulario_perguntas').css('display', '');

        buscarRespostaUnidadeSaude( ussid );
    }

    function buscarRespostaUnidadeSaude( ussid ){        
        $('#formulario').each (function(){
            this.reset();
        });

        $.ajax({
            type    : "POST",
            url     : window.location.href,
            data    : "requisicao=buscarRespostaUnidadeSaude&ussid="+ussid,
            success: function(resp){
                var dados = $.parseJSON(resp);

                var indice = '';
                var resp = '';
                var tipo = '';

                $.each(dados, function(index, value) {
                    $.each(value, function(index, value) {
                        if(index == 'prgid'){
                            indice = value;
                        }
                        if(index == 'rusresposta'){
                            resp = value;
                        }
                        if(index == 'prgtipo'){
                            tipo = value;
                        }
                    });

                    if(tipo == 'T' || tipo == 'C'){
                        $('textarea[name=pergunta['+indice+']]').val(resp);
                    }

                    if(tipo == 'N'){
                        $('input:text[name=pergunta['+indice+']]').val(resp);
                    }

                    if(tipo == 'B'){
                        $('#pergunta_'+resp+'_'+indice).attr('checked', true);
                    }

                    if(tipo == 'M'){
                        $('#pergunta_'+resp+'_'+indice).attr('checked', true);
                    }

                    $('input:hidden[name=status['+indice+']]').val('U');

                });
            }
        });
    }

    function cadastraUnidadeSaude( param ){
        //HOSPITAL = 1
        var url = 'maismedicomec.php?modulo=principal/instrumentoavaliacao/cad_unidade_saude&acao=A&tusid=1';
        window.open( url, 'alterarEstado', 'width=650, height=300, scrollbars=yes, scrolling=no, resizebled=no' );
    }

    function excluirUnidadeSaude(ussid){
        //HOSPITAL = 1
        var confirma = confirm("Deseja realmente excluir o Registro?");
        if( confirma ){
            $.ajax({
                type    : "POST",
                url     : window.location.href,
                data    : "requisicao=excluirUnidadeSaude&tusid=1&ussid="+ussid,
                success: function(data){
                    $("#div_grid_lista_unidades").html(data);
                    divCarregado();
                }
            });
        }
    }

    function liberaSelecaoUnidadeSaude(obj){
        var name  = $(obj).attr('name');
        var radio_box = $('input:radio[name='+name+']:checked');

        if(radio_box.val() ==  'S'){
            $('#dvi_botao_adicionar_unidade').css('display', '');
        }else{
            $('#dvi_botao_adicionar_unidade').css('display', 'none');
        }

    }

    function salvarQuestUnidadeSaude(){
        var erro;
        var campos = '';

        var ussid = $('#ussid');

        if(!ussid.val()){
            alert('O campo "� necessario Selecionar uma Unidade de Sa�de" � um campo obrigat�rio!');
            erro = 1;
            return false;
        }

        $.each($(".obrigatorio"), function(i, v){
            if( $(this).attr('type') != 'radio' ){
                if ( $(v).val() == '' ){
                    erro = 1;
                    campos += '- ' + $(this).attr('title') + " \n";
                }
            }else{
                var name  = $(this).attr('name');
                var value = $(this).attr('value');
                var radio_box = $('input:radio[name='+name+']:checked');

                if(!radio_box.val()){
                    erro = 1;
                    if(value == 'S'){
                        campos += '- ' + $(this).attr('title') + " \n";
                    }
                }
            }
        });

        if( erro > 0 ){
            alert('Existem "campos obrigatorios" vazios! S�o eles: \n'+campos);
            return false;
        }

        if(!erro){
            $('#requisicao').val('salvarQuestUnidadeSaude');
            $('#formulario').submit();
        }
    }

    function voltarAbaPerguntas(){
        window.location.href = 'maismedicomec.php?modulo=principal/instrumentoavaliacao/perg_desc_apectos_gerais&acao=A';
    }

</script>

<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
    <tr>
        <td class ="SubTituloDireita" width="25%">Nome do Hospital</td>
        <td id="td_nome_unidade_saude"> <input type="text" class="disabled" id="ussdsc" size="45" disabled="disabled">&nbsp;</td>
        <td class ="SubTituloDireita" width="25%">CNES n�</td>
        <td id="td_cnes_unidade_saude"> <input type="text" class="disabled" id="usscnes" disabled="disabled">&nbsp;</td>
    </tr>
</table>

<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center" >
    <tr>
        <td style="padding: 0px;" width="15%">
            <span style="cursor: pointer" onclick="cadastraUnidadeSaude('H');" title="Nova Unidade de Sa�de" >
                <img align="absmiddle" src="/imagens/gif_inclui.gif" width="11px"/> Adicionar Hospital(is)
            </span>
        </td>
    </tr>
</table>

<div id="div_grid_lista_unidades">
    <?PHP
        montaListaGridUnidadeSaude( UNIDADE_SAUDE_HOSPITAL );
    ?>
</div>

<form action="" method="POST" id="formulario" name="formulario">
    <input type="hidden" id="requisicao" name="requisicao" value=""/>
    <input type="hidden" id="ussid" name="ussid" value=""/>

    <div id="div_formulario_perguntas" style="display: none;">
        <table align="center" bgcolor="#f5f5f5" border="0" cellpadding="3" cellspacing="1" class="tabela">
            <tr>
                <td width="95%" style="vertical-align: top;">
                    <table align="center" bgcolor="#f5f5f5" border="0" cellpadding="3" cellspacing="1" class="tabela listagem">
                        <tr>
                            <td class ="SubTituloCentro" colspan="3"> 3. Servi�os de sa�de - <?=$tipoUnidadeSaude;?>.</td>
                        </t>
                        <?PHP
                            #PERGUNTA 216 � A PERGUNTA AGRUPADORA DAS 30,35,36
                            $arrPerguntas = array(216);
                            echo montaPerguntasQuestionario( 'A - 1)', 'Unidades de Interna��o', '', $arrPerguntas, NULL, 'A_1' );

                            #PERGUNTA 216 � A PERGUNTA AGRUPADORA DAS 30,35,36
                            #perguntas inativadas: 32,31,33,34,36,37
                            $arrPerguntas = array(30,35,36);
                            echo montaPerguntasQuestionario( '', '', '', $arrPerguntas, NULL, 'A_1' );

                            $arrPerguntas = array(38,39,40,41);
                            echo montaPerguntasQuestionario( 'A - 2)', 'Unidades Cirurgicas', '', $arrPerguntas, NULL, 'A_2' );

                            $arrPerguntas = array(42,43,44,45,46,47,48,49);
                            echo montaPerguntasQuestionario( 'A - 3)', 'Recursos Proped�uticos', '', $arrPerguntas, NULL, 'A_3' );

                            #perguntas inativadas: 52,53,54,55
                            $arrPerguntas = array(50,51,56,57);
                            echo montaPerguntasQuestionario( 'A - 4)', 'Unidade de Urg�ncia/Emerg�ncia', '', $arrPerguntas, NULL, 'A_4' );

                            $arrPerguntas = array(58,59);
                            echo montaPerguntasQuestionario( 'A - 5)', 'Recursos de Hemoterapia ', '', $arrPerguntas, NULL, 'A_5' );

                            #perguntas inativadas: 62,64,63,65,66
                            $arrPerguntas = array(60,67,68,69);
                            echo montaPerguntasQuestionario( 'A - 6)', 'Resid�ncia m�dica', '', $arrPerguntas, NULL, 'A_6' );

                            $arrPerguntas = array(70,71);
                            echo montaPerguntasQuestionario( 'A - 7)', 'Ambiente de Pr�tica para alunos', '', $arrPerguntas, NULL, 'A_7' );

                            $arrPerguntas = array(72);
                            echo montaPerguntasQuestionario( 'A - 8)', 'Papel do Hospital no servi�o de sa�de do munic�pio', '', $arrPerguntas, NULL, 'A_8' );

                            $arrPerguntas = array(73);
                            echo montaPerguntasQuestionario( 'A - 9)', 'Avalia��o do gestor municipal', '', $arrPerguntas, NULL, 'A_9' );

                            $arrPerguntas = array(74);
                            echo montaPerguntasQuestionario( 'A - 10)', 'Dados Estat�sticos', '', $arrPerguntas, NULL, 'A_10' );

                            $arrPerguntas = array(215);
                            echo montaPerguntasQuestionario( 'A - 11)', 'Coment�rios Gerais sobre o Hospital', '', $arrPerguntas, NULL, 'A_11' );
                        ?>
                    </table>
                </td>
                <td width="5%" style="vertical-align: top;">
                    <?PHP
                        require_once APPRAIZ . "includes/workflow.php";
                        $etqid = $_SESSION['maismedicomec']['etqid'];
                        if($etqid != ''){
                            $docid = buscarDocidAvaliacaoMM( $etqid );

                            if($docid != ''){
                                $dados_wf = array("cprid" => $cprid);
                                wf_desenhaBarraNavegacao($docid, $dados_wf);
                            }
                        }
                    ?>
                </td>
            </tr>
        </table>

        <table align="center" bgcolor="#f5f5f5" border="0" cellpadding="3" cellspacing="1" class="Tabela listagem">
            <tr>
                <td class="SubTituloCentro" colspan="4">
                    <?PHP
                        $habilita = habilitaPerfilEstadoAcao();

                        if( $habilita == 'S' ){
                    ?>
                            <input type="button" id="voltar" name="voltar" value="Voltar" title="Volta a Listagem de Munic�pios" onclick="voltarAbaPerguntas();"/>
                            <input type="button" id="salvarQuest" name="salvarQuest" value="Salvar" onclick="salvarQuestUnidadeSaude();"/>
                    <?PHP
                        }else{
                    ?>
                            <input type="button" id="voltar" name="voltar" value="Voltar" title="Volta a Listagem de Munic�pios" onclick="voltarAbaPerguntas();"/>
                            <input type="button" id="salvar_sem_continui" name="salvar_sem_continui" value="Salvar" disabled="disabled">
                    <?PHP
                        }
                    ?>
                </td>
            </tr>
        </table>
    </div>
</form>