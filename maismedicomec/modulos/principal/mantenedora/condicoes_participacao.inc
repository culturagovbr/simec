<?PHP

    include APPRAIZ . "includes/cabecalho.inc";
    monta_titulo( 'Mais M�dicos - MEC', 'Condi��es de Participa��o' );

    $abacod_tela    = ABA_CADASTRO_MANTENEDORA;
    $url            = 'maismedicomec.php?modulo=principal/mantenedora/condicoes_participacao&acao=A';
    $parametros     = '';

    $db->cria_aba($abacod_tela, $url, $parametros);
    
	if($_SESSION['maismedicomec']['mntid'] == ''){
        $mntid = $_REQUEST['mntid'];

        if( $_SESSION['maismedicomec']['mntid'] == '' ){
            $_SESSION['maismedicomec']['mntid'] = $mntid;
        }
    }else{
        $mntid = $_SESSION['maismedicomec']['mntid'];
    }

?>

<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript">
</script>

<form id="formulario" method="post" name="formulario" >
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" border="0">
	    <tr>
	        <td height="25"></td>
	    </tr>
	    <tr>
	        <td>
                <?php if($hab_maismedicos == 'N'){?>
                	<p style="margin-left: 38%; text-decoration: underline; font-size: 14px; font-weight: bold; color:#FF0000;"><b>MUNIC�PIO N�O ATENDE AS CONDI��ES DE PARTICIPA��O<b></p>
                <?php } ?>
	            <span style="margin-left: 3%; text-decoration: underline; font-size: 14px; font-weight: bold;">
	                CONDI��ES DE PARTICIPA��O
	            </span>
	            
	            <br><br>
	            
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                Poder� participar desta chamada p�blica mantenedora de IES legalmente constitu�da no Pa�s que tenha, pelo menos, 1 (uma) mantida j� credenciada integrante do Sistema Federal de Ensino e com cadastro no Sistema e-MEC at� a data de publica��o do presente Edital.
	            </p>
	            
	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                A mantenedora dever� indicar a mantida que ser� respons�vel pela oferta do curso de gradua��o em medicina ou propor o credenciamento de IES ou de campus fora de sede, nos termos dos itens 3.1.1, 3.1.2 e 3.1.3 do Edital. 
	            </p>

	            <p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                As mantenedoras poder�o apresentar propostas que contemplem uma das op��es abaixo: 
	            </p>
	            
	            <ul style="list-style-type:disc;margin-left: 5%; font-size: 13px;">
				  <li>Autoriza��o de curso de gradua��o em medicina para mantida que detenha autoriza��o de funcionamento no munic�pio para o qual est� concorrendo;</li>
				  <li>Credenciamento de nova mantida no munic�pio para o qual est� concorrendo e autoriza��o de curso de gradua��o em medicina; </li>
				  <li>Credenciamento de campus fora de sede (no caso exclusivo de universidades credenciadas na Unidade da Federa��o do munic�pio para o qual est� concorrendo) e autoriza��o de curso de gradua��o em medicina.</li>
				</ul>
				
				<p style="margin-left: 5%; text-align: justify; font-size: 13px; font-weight: bold;">
	                N�o poder�o participar deste processo de sele��o:  
	            </p>
	            
	            <ul style="list-style-type:disc;margin-left: 5%; font-size: 13px;">
				  <li>Cons�rcio de mantidas; </li>
				  <li>A mantenedora que teve processo de credenciamento de Institui��o de Educa��o Superior ou de autoriza��o de curso de gradua��o em medicina indeferidos nos �ltimos dois anos, a contar da data de publica��o do ato de indeferimento.</li>
				</ul>

	        </td>
	    </tr>
	    <!-- 
	    <tr>
	        <td align="center" bgcolor="#CCCCCC" colspan="2">
                <?php if( $hab_maismedicos == 'S' ){ ?>
                <input type="submit" value="Continuar" id="btnContinuar"/>
                <input type="button" value="Cancelar" id="btnCancelar" onclick="cancelar();"/>
                <?php } else { ?>
                <input type="button" value="OK" id="btnCancelar" onclick="cancelar();"/>
                <?php } ?>
	        </td>
	    </tr>
	     -->						
	</table>
</form>


