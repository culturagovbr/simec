<?php
	
class Pagamento_RemessaCabecalho extends Modelo{
	
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = "maismedicos.pagamento_remessacabecalho";	

    /**
     * Chave primaria.
     * @var array
     * @access protected
     */
    protected $arChavePrimaria = array( "rmcid" );

    /**
     * Atributos
     * @var array
     * @access protected
     */    
    protected $arAtributos     = array(
									  	'rmcid' => null, 
									  	'cs_tipo_registro' => null, 
									  	'nu_seq_registro' => null, 
									  	'cs_tipo_lote' => null, 
									  	'dt_hora' => null, 
									  	'id_banco' => null, 
									  	'cs_meio_pagto' => null, 
									  	'dt_gravacao_lote' => null, 
									  	'nu_seq_lote' => null, 
									  	'nu_cod_conv' => null, 
									  	'nu_ctrl_trans' => null, 
									  	'arqid' => null, 
									  );
    
    public function processarCadastro()
    {
    
    	/* Preenchimento dos campos de cabe�alho */
    	$arrDado['cs_tipo_registro'] = TRGID_CABECALHO; //Dicion�rio de Dados: 1 para Header, 2 para Detalhe e 3 para Trailer.
    	$arrDado['nu_seq_registro'] = "0000001"; //Sequencial para cada envio de lote, cabe�alho recebe sempre 0000001
    	$arrDado['cs_tipo_lote'] = TPLID_CADASTRO; //Dicion�rio de dados: 21 para cadastro e 20 para cr�ditos
    	//$arrDado['dt_hora'] = "now()"; //Padr�o do SIMEC, autom�tico no banco
    	$arrDado['id_banco'] = "001"; //ID Fixo: 001
    	$arrDado['cs_meio_pagto'] = "01"; //ID Fixo: 01
    	$arrDado['dt_gravacao_lote'] = date("Ymd"); //Data em que o arquivo foi gravado, no formato YYYYMMDD
    	$nu_seq_lote = verificaTransmissaoLoteDataAtual();
    	$arrDado['nu_seq_lote'] = preencherComZero(($nu_seq_lote),2); //N�mero sequencial, incrementado quando mais de um lote for enviado na mesma data
    	$arrDado['nu_cod_conv'] = "000526"; //ID Fixo: 000526
    	//In�cio - N�mero incrementado a cada transmiss�o de lote (serial)
    	$sql = "select count(rmcid) from $this->stNomeTabela where cs_tipo_lote = 2";
    	$nu_ctrl_trans = $this->pegaUm($sql);
    	$nu_ctrl_trans = $nu_ctrl_trans ? $nu_ctrl_trans : "0";
    	$nu_ctrl_trans = preencherComZero(($nu_ctrl_trans+1),6);
    	$arrDado['nu_ctrl_trans'] = $nu_ctrl_trans;
    	//Fim - N�mero incrementado a cada transmiss�o de lote (serial)
    
    	$this->popularDadosObjeto($arrDado);
    	$rmcid = $this->salvar();
    	if($rmcid){
    		 
    		$detalhe = new Pagamento_RemessaDetalhe();
    		$detalhe->salvarDetalhe($rmcid,$arrDado['nu_seq_lote']);
    		 
    		$this->commit();
    		$_SESSION['maismedicos']['remessa']['alert'] = "Opera��o realizada com sucesso.";
    	}else{
    		$_SESSION['maismedicos']['remessa']['alert'] = "N�o foi poss�vel realizar a opera��o.";
    	}
    
    	header("Location: maismedicos.php?modulo=principal/pagamento/geraRemessaCadastro&acao=A");
    	exit;
    }
     
    public function gerarArquivoRemessaCadastro($rmcid)
    {
    
    	$txt = $this->preparaTxtRemessaCadastroCabecalho($rmcid);
    
    	$detalhe = new Pagamento_RemessaDetalhe();
    	$txt.= $detalhe->preparaTxtRemessaCadastroDetalhe($rmcid);
    
    	if(!is_dir(APPRAIZ . 'arquivos/maismedicos')) {
    		mkdir(APPRAIZ . 'arquivos/maismedicos', 0777);
    	}
    	 
    	if(!is_dir(APPRAIZ . 'arquivos/maismedicos/remessacadastro')) {
    		mkdir(APPRAIZ . 'arquivos/maismedicos/remessacadastro', 0777);
    	}
    	 
    	$caminho = APPRAIZ . 'arquivos/maismedicos/remessacadastro/'.$rmcid.'.txt';
    
    	$fp = fopen($caminho, "w+");
    
    	stream_set_write_buffer($fp, 0);
    	fwrite($fp, $txt);
    	fclose($fp);
    
    	$filename = $caminho;
    	header( 'Content-type: text/plain' );
    	header( 'Content-Disposition: attachment; filename=remessa_'.$rmcid);
    	readfile( $caminho );
    	exit();
    
    }
     
    public function preparaTxtRemessaCadastroCabecalho($rmcid)
    {
    	$txt = $this->arAtributos['cs_tipo_registro'];
    	$txt.= $this->arAtributos['nu_seq_registro'];
    	//In�cio Tipo de Lote
    	$tipo_lote = new TipoLote($this->arAtributos['cs_tipo_lote']);
    	$txt.= $tipo_lote->tplcod;
    	//Fim Tipo de Lote
    	$txt.= preencherComZero($this->arAtributos['id_banco'],3);
    	$txt.= preencherComZero($this->arAtributos['cs_meio_pagto'],2);
    	$txt.= $this->arAtributos['dt_gravacao_lote'];
    	$txt.= $this->arAtributos['nu_seq_lote'];
    	$txt.= preencherComEspacoVazio(" ",7); //Filler
    	$txt.= $this->arAtributos['nu_cod_conv'];
    	$txt.= preencherComEspacoVazio(" ",56); //Filler
    	$txt.= $this->arAtributos['nu_ctrl_trans'];
    	$txt.= preencherComEspacoVazio(" ",140); //Filler
    	$txt.= "\n";
    	//dbg(strlen($txt));
    	//dbg($txt,1);
    	return $txt;
    }
     
    public function validarRemessaCadastro()
    {
    
    	$conteudo = file_get_contents($_FILES['arquivoremesssa']['tmp_name']);
    	$arrCabecalho['cs_tipo_registro'] = substr($conteudo, 0,1);
    	$arrCabecalho['nu_seq_registro'] = substr($conteudo, 1,7);
    	$arrCabecalho['cs_tipo_lote'] = substr($conteudo, 8,2);
    	$arrCabecalho['id_banco'] = substr($conteudo, 10,3);
    	$arrCabecalho['cs_meio_pagto'] = substr($conteudo, 13,2);
    	$arrCabecalho['dt_gravacao_lote'] = substr($conteudo, 15,8);
    	$arrCabecalho['nu_seq_lote'] = substr($conteudo, 23,2);
    	$arrCabecalho['filler_01'] = substr($conteudo, 25,7);
    	$arrCabecalho['nu_cod_conv'] = substr($conteudo, 31,6);
    	$arrCabecalho['filler_02'] = substr($conteudo, 38,56);
    	$arrCabecalho['nu_ctrl_trans'] = substr($conteudo, 94,6);
    	$arrCabecalho['filler_03'] = substr($conteudo, 100,140);
    	$cabecalho = substr($conteudo, 0,240);
    	$arrDetalhe = explode("\n",$conteudo);
    	if($arrDetalhe){
    		foreach($arrDetalhe as $linha => $det){
    			if($det && $linha != 0 && $linha != (count($arrDetalhe)-2) ){
    				$arrBeneficiario = false;
    				$arrBeneficiario['cs_tipo_registro'] = substr($det, 0,1);
    				$arrBeneficiario['nu_seq_registro'] = substr($det, 2,6);
    				$arrBeneficiario['cs_tipo_lote'] = substr($det, 8,2);
    				$arrBeneficiario['nu_nib'] = substr($det, 10,10);
    				$arrBeneficiario['id_orgao_pagador'] = substr($det, 20,6);
    				$arrBeneficiario['cs_especie'] = substr($det, 26,3);
    				$arrBeneficiario['id_agencia_conv'] = substr($det, 29,8);
    				$arrBeneficiario['in_prestacao_unica'] = substr($det, 37,1);
    				$arrBeneficiario['nu_conta'] = substr($det, 38,10);
    				$arrBeneficiario['nu_cpf'] = substr($det, 48,11);
    				$arrBeneficiario['filler_01'] = substr($det, 59,13);
    				$arrBeneficiario['nm_beneficiario'] = substr($det, 72,28);
    				$arrBeneficiario['id_nit'] = substr($det, 100,11);
    				$arrBeneficiario['te_endereco'] = substr($det, 111,40);
    				$arrBeneficiario['te_bairro'] = substr($det, 151,17);
    				$arrBeneficiario['nu_cep'] = substr($det, 168,8);
    				$arrBeneficiario['dt_nasc'] = substr($det, 176,8);
    				$arrBeneficiario['nm_mae'] = substr($det, 184,32);
    				$arrBeneficiario['nu_dia_util'] = substr($det, 216,2);
    				$arrBeneficiario['cs_tipo_dado_cad'] = substr($det, 218,2);
    				$arrBeneficiario['dt_ult_atu_end'] = substr($det, 220,8);
    				$arrBeneficiario['nu_seq_rms'] = substr($det, 228,6);
    				$arrBeneficiario['sit_cmd'] = substr($det, 234,4);
    				$arrBeneficiario['sit_rms'] = substr($det, 238,1);
    				$arrBeneficiario['filler_02'] = substr($det, 239,1);
    				$detalhe[] = $arrBeneficiario;
    			}elseif($linha == (count($arrDetalhe)-2)) //Trailer (Rodap�)
    			{
    				$arrRodape['cs_tipo_registro'] = substr($det, 0,1);
    				$arrRodape['nu_seq_registro'] = substr($det, 1,7);
    				$arrRodape['cs_tipo_lote'] = substr($det, 8,2);
    				$arrRodape['id_banco'] = substr($det, 10,3);
    				$arrRodape['qt_reg_detalhe'] = substr($det, 13,8);
    				$arrRodape['nu_seq_lote'] = substr($det, 21,2);
    				$arrRodape['filler'] = substr($det, 23,217);
    			}
    		}
    	}
    
    	//Pesquisa qual � o registro que deve receber o arquivo de retorno
    	$sql = "select
    	rmcid
    	from
    	maismedicos.pagamento_remessacabecalho
    	where
    	cs_tipo_registro = '{$arrCabecalho['cs_tipo_registro']}'
    	and
   				nu_seq_registro = '{$arrCabecalho['nu_seq_registro']}'
       				and
   				cs_tipo_lote = ".TPLID_CADASTRO."
       				and
       				id_banco = ".BNCID_BANCO_BRASIL."
   			and
       			cs_meio_pagto = '{$arrCabecalho['cs_meio_pagto']}'
    		--and
    		--dt_gravacao_lote = '{$arrCabecalho['dt_gravacao_lote']}'
    		and
    		nu_seq_lote = '{$arrCabecalho['nu_seq_lote']}'
    		and
    		nu_cod_conv = '{$arrCabecalho['nu_cod_conv']}'
    		and
    		nu_ctrl_trans = '{$arrCabecalho['nu_ctrl_trans']}'
    		";
    		//dbg($sql,1);
    		$rmcid = $this->pegaUm($sql);
   	//$rmcid = 70; //Remover esta linha !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    		if(!$rmcid){
    		$_SESSION['maismedicos']['remessa']['alert'] = "O arquivo de retorno n�o corresponde a nenhuma remessa.";
   		header("Location: maismedicos.php?modulo=principal/validarRemessaCadastro&acao=A");
       		exit;
   	}else{
   		$aqrid = $this->gravaRetornoRemessa();
   		if($aqrid){
    		$sql = "update maismedicos.pagamento_remessacabecalho set arqid = $aqrid where rmcid = $rmcid";
    			$this->executar($sql);
    			$this->commit();
    		}else{
    		$_SESSION['maismedicos']['remessa']['alert'] = "N�o foi poss�vel fazer o upload do arquivo.";
    		header("Location: maismedicos.php?modulo=principal/validarRemessaCadastro&acao=A");
   			exit;
    			}
    			 
   		if($detalhe){
   			$num_encontrados=0;
   			$num_nao_encontrados=0;
    			foreach($detalhe as $det){
    			$det['nm_beneficiario'] = trim($det['nm_beneficiario']);
    			$det['te_endereco'] = trim($det['te_endereco']);
    				$det['te_bairro'] = trim($det['te_bairro']);
    						$det['nm_mae'] = trim($det['nm_mae']);
    						$sql = "select
    						rmdid
    								from
    								maismedicos.pagamento_remessadetalhe
    								where
    								rmcid = $rmcid
    						and
    						cs_tipo_registro = '{$det['cs_tipo_registro']}'
    						and
    						nu_seq_registro = '{$det['nu_seq_registro']}'
    						and
    						cs_tipo_lote = ".TPLID_CADASTRO."
    						and
    								nu_nib = '{$det['nu_nib']}'
    								and
    								id_orgao_pagador = '{$det['id_orgao_pagador']}'
    								and
    								id_agencia_conv = '{$det['id_agencia_conv']}'
    								and
    										in_prestacao_unica = '{$det['in_prestacao_unica']}'
   						and
    										nu_cpf = '{$det['nu_cpf']}'
    										and
   							nm_beneficiario = '{$det['nm_beneficiario']}'
       							and
       							te_endereco = '{$det['te_endereco']}'
       							and
       							te_bairro = '{$det['te_bairro']}'
       							and
       							nu_cep = '{$det['nu_cep']}'
       							and
       							dt_nasc = '{$det['dt_nasc']}'
       							and
       							nm_mae = '{$det['nm_mae']}'
       							";
       								
       							$rmdid = $this->pegaUm($sql);
       							if($rmdid){
       							$num_encontrados++;
       							//Recupera o retorno
       							$sql = "select strid from maismedicos.situacaoregistro where strcod = '{$det['sit_cmd']}'";
       							$strid = $this->pegaUm($sql);
       							if($strid){
       							$sql = "update maismedicos.pagamento_remessadetalhe set strid = $strid where rmdid = $rmdid";
       							$this->executar($sql);
       							$this->commit();
       							}
       							}else{
       							$num_nao_encontrados++;
       							}
       							}
    
       							$_SESSION['maismedicos']['remessa']['alert'] = "Retorno: $num_encontrados atualizados. $num_nao_encontrados n�o atualizados.";
       							header("Location: maismedicos.php?modulo=principal/validarRemessaCadastro&acao=A");
       							exit;
    			}
    			 
    			}
    			}
  
   public function gravaRetornoRemessa($descricao = "Arquivo de Retorno da Remessa de Cadastro",$campo = "arquivoremesssa")
       {
        
       require_once APPRAIZ . 'includes/classes/fileSimec.class.inc';
    	$file = new FilesSimec(null,null,"maismedicos");
        	$file->setUpload($descricao,$campo,false,false);
    	return $file->getIdArquivo();
       							}
       							 
       							public function listarDetalheRemessa($rmcid)
       							{
       							$sql = "select
       							ben.bennome,
       							case when ben.bentipo = 'T'
       							then 'benor'
       							else 'Supervisor'
       							end as funcao,
       							uni.uninome,
       							coalesce(str.strdsc,'N/A') as strdsc
       							from
       							maismedicos.pagamento_beneficiario ben
       							inner join
       							maismedicos.universidade uni ON uni.uniid = ben.uniid
       							inner join
       							maismedicos.pagamento_remessadetalhe det ON det.benid = ben.benid
       							left join
       							maismedicos.situacaoregistro str ON str.strid = det.strid
       							where
       							det.rmcid = $rmcid
       							order by
       							ben.bennome";
       							//dbg($sql,1);
       							$arrCab = array("Nome","Fun��o","Institui��o","Situa��o");
	   	$this->monta_lista($sql, $arrCab, 100, 10, "N", "center", "");
       							}
       							 
       							 		public function listarDetalheRemessaCredito($rmcid, $excel = false)
       							 		{
       							 			$sql = "select
       							 			ben.bencpf,
       							 			ben.bennome,
       							 			case when ben.bentipo = 'T'
       							 			then 'benor'
       							 			else 'Supervisor'
       							 			end as funcao,
       							 			case when ben.bentipo = 'T'
       							 			then '5000'
       							 			else '4000'
       							 			end as valor,
       							 			fpgdsc,
       							 			uni.uninome,
       							 			coalesce(sit.strdsc,'N/A') as tpodsc
       							 			from
       							 			maismedicos.pagamento_beneficiario ben
       							 			inner join
       							 			maismedicos.universidade uni ON uni.uniid = ben.uniid
       							 			inner join
       							 			maismedicos.pagamento_remessadetalhe det ON det.benid = ben.benid
       							 			inner join
       							 			maismedicos.pagamento_remessacabecalho cab ON cab.rmcid = det.rmcid
       							 			inner join
       							 			maismedicos.folhapagamento fpg ON fpg.fpgid = cab.fpgid
       							 			left join
       							 			maismedicos.situacaoregistro sit ON sit.strcod = det.cs_ocorrencia
       							 			where
       							 			det.rmcid = $rmcid
       							 			order by
       							 			ben.bennome";
       							 			//dbg($sql);
       							 					$arrCab = array("CPF", "Nome","Fun��o","Valor (R$)","Descri��o","Institui��o","Situa��o");
       							 					if($excel){
       							 					$this->monta_lista_tabulado($sql, $arrCab, 100000000, 100000);
       							 			}else{
       							 			$this->monta_lista($sql, $arrCab, 100, 10, "S", "center", "");
       							 			}
       							 			 }
       							 			 
       							 			public function processarRemessaCredito()
       							 			{
       							 			/* Preenchimento dos campos de cabe�alho */
//        							 				$folha = new FolhaPagamento();
//        							 						$arrFolha['tfpid'] = TFPID_PAGAMENTO;
//        							 						$arrFolha['fpgdsc'] = "Folha de Pagamento do M�s de ".retornaMesMaisMedicos($_POST['apgmes'])." de ".$_POST['apgano'];
//        							 						$arrFolha['fpgmes'] = $_POST['apgmes'].$_POST['apgano'];
//        							 						$arrFolha['usucpf'] = $_SESSION['usucpf'];
//        							 								$folha->popularDadosObjeto($arrFolha);
//        							 								$fpgid = $folha->salvar();
//        							 								$folha->commit();
       							 								$aut = new Pagamento_Autorizacao();
       							 								$aut->checkTutoresSelecionados($_POST['benid']);
       							 								//$arrDado['fpgid'] = $fpgid; //Folha de Pagamento
       							 								$arrDado['cs_tipo_registro'] = TRGID_CABECALHO; //Dicion�rio de Dados: 1 para Header, 2 para Detalhe e 3 para Trailer.
	   	$arrDado['nu_seq_registro'] = "0000001"; //Sequencial para cada envio de lote, cabe�alho recebe sempre 0000001
       							 								$arrDado['cs_tipo_lote'] = TPLID_CREDITO; //Dicion�rio de dados: 21 para cadastro e 20 para cr�ditos
       							 								$arrDado['id_banco'] = "001"; //ID Fixo: 001
       							 								$arrDado['cs_meio_pagto'] = "01"; //ID Fixo: 01
	   	$arrDado['dt_gravacao_lote'] = date("Ymd"); //Data em que o arquivo foi gravado, no formato YYYYMMDD
       							 								$nu_seq_lote = verificaTransmissaoLoteDataAtual();
       							 								$arrDado['nu_seq_lote'] = preencherComZero(($nu_seq_lote),2); //N�mero sequencial, incrementado quando mais de um lote for enviado na mesma data
       							 								$arrDado['nu_cod_conv'] = "000526"; //ID Fixo: 000526
       							 								
       							 								//In�cio - N�mero incrementado a cada transmiss�o de lote (serial)
       							 								$sql = "select count(rmcid)+3 from $this->stNomeTabela where cs_tipo_lote = 1"; //+ 3 pq a contagem j� come�ou em 3.
       							 									$nu_ctrl_trans = $this->pegaUm($sql);
       							 									$nu_ctrl_trans = $nu_ctrl_trans ? $nu_ctrl_trans : "0";
       							 									$nu_ctrl_trans = preencherComZero(($nu_ctrl_trans+1),6);
	   	$arrDado['nu_ctrl_trans'] = $nu_ctrl_trans;
    	   	//Fim - N�mero incrementado a cada transmiss�o de lote (serial)
	  
    	   			$this->popularDadosObjeto($arrDado);
	   	$rmcid = $this->salvar();
	   	if($rmcid){
	   
    	   	$detalhe = new Pagamento_RemessaDetalhe();
    	   	$detalhe->salvarDetalheCredito($rmcid,$arrDado['nu_seq_lote'],$fpgid);
    	   	 
    	   	$this->commit();
    	   	$_SESSION['maismedicos']['remessa']['alert'] = "Opera��o realizada com sucesso.";
    	   	}else{
    	   	$_SESSION['maismedicos']['remessa']['alert'] = "N�o foi poss�vel realizar a opera��o.";
	   	}
	  
    	   	header("Location: maismedicos.php?modulo=principal/pagamento/geraRemessaCredito&acao=A");
    	   			exit;
    	   			}
    	   			 
   function gerarArquivoRemessaCredito($rmcid)
       {
       $txt = $this->preparaTxtRemessaCreditoCabecalho($rmcid);
    
       		$detalhe = new Pagamento_RemessaDetalhe();
       		$txt.= $detalhe->preparaTxtRemessaCreditoDetalhe($rmcid);
    
       		if(!is_dir(APPRAIZ . 'arquivos/maismedicos')) {
       		mkdir(APPRAIZ . 'arquivos/maismedicos', 0777);
   	}
       	 
       	if(!is_dir(APPRAIZ . 'arquivos/maismedicos/remessacredito')) {
       	mkdir(APPRAIZ . 'arquivos/maismedicos/remessacredito', 0777);
    	   	}
    	   	 
    	   	$caminho = APPRAIZ . 'arquivos/maismedicos/remessacredito/'.$rmcid.'.txt';
    
   	$fp = fopen($caminho, "w+");
    
       	stream_set_write_buffer($fp, 0);
       	fwrite($fp, $txt);
       	fclose($fp);
    
       	$filename = $caminho;
   	header( 'Content-type: text/plain' );
       	header( 'Content-Disposition: attachment; filename=remessa_credito_'.$rmcid);
       			readfile( $caminho );
       			exit();
    	   	}
    	   	 
   public function preparaTxtRemessaCreditoCabecalho($rmcid)
   {
       $txt = $this->arAtributos['cs_tipo_registro'];
    	   	$txt.= $this->arAtributos['nu_seq_registro'];
    	   	//In�cio Tipo de Lote
   	$tipo_lote = new TipoLote($this->arAtributos['cs_tipo_lote']);
       	$txt.= $tipo_lote->tplcod;
   	//Fim Tipo de Lote
       	$txt.= preencherComZero($this->arAtributos['id_banco'],3);
       	$txt.= preencherComZero($this->arAtributos['cs_meio_pagto'],2);
       	$txt.= $this->arAtributos['dt_gravacao_lote'];
       	$txt.= $this->arAtributos['nu_seq_lote'];
       	$txt.= preencherComZero("0",7); //Filler
       	$txt.= $this->arAtributos['nu_cod_conv'];
       	$txt.= preencherComEspacoVazio(" ",56); //Filler
       			$txt.= $this->arAtributos['nu_ctrl_trans'];
   	$txt.= preencherComEspacoVazio(" ",140); //Filler
       	$txt.= "\n";
       	//dbg(strlen($txt));
       	//dbg($txt,1);
   	return $txt;
       	}
       			 
       	public function validarRemessaCredito()
       	{
    
       			 
       			$conteudo = file_get_contents($_FILES['arquivoremesssa']['tmp_name']);
       	$arrCabecalho['cs_tipo_registro'] = substr($conteudo, 0,1);
       	$arrCabecalho['nu_seq_registro'] = substr($conteudo, 1,7);
       	$arrCabecalho['cs_tipo_lote'] = substr($conteudo, 8,2);
       	$arrCabecalho['id_banco'] = substr($conteudo, 10,3);
       	$arrCabecalho['cs_meio_pagto'] = substr($conteudo, 13,2);
       	$arrCabecalho['dt_gravacao_lote'] = substr($conteudo, 15,8);
       	$arrCabecalho['nu_seq_lote'] = substr($conteudo, 23,2);
       	$arrCabecalho['dt_comp_movto'] = substr($conteudo, 25,6);
       	$arrCabecalho['nu_cod_conv'] = substr($conteudo, 31,6);
       	$arrCabecalho['filler_01'] = substr($conteudo, 37,57);
       	$arrCabecalho['nu_ctrl_trans'] = substr($conteudo, 94,6);
       	$arrCabecalho['filler_03'] = substr($conteudo, 100,140);
       	$cabecalho = substr($conteudo, 0,240);
    
       	$arrDetalhe = explode("\n",$conteudo);
    
       	if($arrDetalhe){
       	foreach($arrDetalhe as $linha => $det){
       	if($det && $linha != 0 && $linha != (count($arrDetalhe)-2) ){
       	$arrBeneficiario = false;
       			$arrBeneficiario['cs_tipo_registro'] = substr($det, 0,1);
       					$arrBeneficiario['nu_seq_registro'] = substr($det,2,6);
       							$arrBeneficiario['cs_tipo_lote'] = substr($det, 8,2);
       									$arrBeneficiario['nu_nib'] = substr($det, 10,10);
       											$arrBeneficiario['dt_fim_periodo'] = substr($det, 20,8);
       											$arrBeneficiario['dt_ini_periodo'] = substr($det, 28,8);
       											$arrBeneficiario['cs_natur_credito'] = substr($det, 36,2);
       											$arrBeneficiario['dt_mov_credito'] = substr($det, 38,8);
       											$arrBeneficiario['id_orgao_pagador'] = substr($det, 46,6);
       											$arrBeneficiario['vl_credito'] = substr($det, 52,12);
       											$arrBeneficiario['cs_unid_monet'] = substr($det, 64,1);
       													$arrBeneficiario['dt_fim_validade'] = substr($det, 65,8);
       													$arrBeneficiario['dt_ocorrencia'] = substr($det, 65,8);
       													$arrBeneficiario['dt_ini_validade'] = substr($det, 73,8);
       													$arrBeneficiario['in_cr_bloquado'] = substr($det, 81,1);
       													$arrBeneficiario['nu_conta'] = substr($det, 82,10);
   				$arrBeneficiario['cs_origem_rocamento'] = substr($det, 92,2);
       						$arrBeneficiario['in_pioneira'] = substr($det, 94,1);
       								$arrBeneficiario['filler_01'] = substr($det, 95,5);
       										$arrBeneficiario['id_nit'] = substr($det, 100,11);
       	$arrBeneficiario['cs_tipo_credito'] = substr($det, 111,2);
       	$arrBeneficiario['cs_especie'] = substr($det, 113,3);
       			$arrBeneficiario['nu_seq_rms'] = substr($det, 115,6);
   				$arrBeneficiario['nu_seq_credito'] = (int)substr($det, 122,11);
       						$arrBeneficiario['sit_cmd'] = substr($det, 133,4);
       								$arrBeneficiario['cs_ocorrencia'] = substr($det, 133,4);
       								$arrBeneficiario['sit_rms'] = substr($det, 137,1);
       								$arrBeneficiario['filler_02'] = substr($det, 138,95);
       								$arrBeneficiario['nu_ctrl_cred'] = substr($det, 233,7);
       								$detalhe[] = $arrBeneficiario;
       	}elseif($linha == (count($arrDetalhe)-2)) //Trailer (Rodap�)
       	{
       	$arrRodape['cs_tipo_registro'] = substr($det, 0,1);
       	$arrRodape['nu_seq_registro'] = substr($det, 1,7);
       	$arrRodape['cs_tipo_lote'] = substr($det, 8,2);
       	$arrRodape['id_banco'] = substr($det, 10,3);
       	$arrRodape['qt_reg_detalhe'] = substr($det, 13,8);
       	$arrRodape['vl_reg_detalhe'] = substr($det, 21,17);
       	$arrRodape['nu_seq_lote'] = substr($det, 38,2);
       	$arrRodape['filler'] = substr($det, 40,200);
       	}
       	}
       	}
       	 
       	//Pesquisa qual � o registro que deve receber o arquivo de retorno
       	$sql = "select
   				rmcid
       	from
       	maismedicos.pagamento_remessacabecalho
       	where
   				cs_tipo_registro = '{$arrCabecalho['cs_tipo_registro']}'
       				and
       				nu_seq_registro = '{$arrCabecalho['nu_seq_registro']}'
       	and
       	cs_tipo_lote = ".TPLID_CREDITO."
       	and
       	id_banco = ".BNCID_BANCO_BRASIL."
       	and
       			cs_meio_pagto = '{$arrCabecalho['cs_meio_pagto']}'
   			--and
   				--dt_gravacao_lote = '{$arrCabecalho['dt_gravacao_lote']}'
   			and
   				nu_seq_lote = '{$arrCabecalho['nu_seq_lote']}'
       				and
       				nu_cod_conv = '{$arrCabecalho['nu_cod_conv']}'
   			--and
       						--arqid is null
       							and
       							nu_ctrl_trans = '{$arrCabecalho['nu_ctrl_trans']}'
       							";
       							//dbg($sql,1);
       							$rmcid = $this->pegaUm($sql);
       							if(!$rmcid){
   			$_SESSION['maismedicos']['remessa']['alert'] = "O arquivo de retorno n�o corresponde a nenhuma remessa de cr�dito.";
   			header("Location: maismedicos.php?modulo=principal/validarRemessaCredito&acao=A");
       													exit;
       													}else{
       													$aqrid = $this->gravaRetornoRemessa("Arquivo de Retorno da Remessa de Cr�dito");
       													if($aqrid){
       													$sql = "update maismedicos.pagamento_remessacabecalho set arqid = $aqrid where rmcid = $rmcid";
       													$this->executar($sql);
       															$this->commit();
       													}else{
       													$_SESSION['maismedicos']['remessa']['alert'] = "N�o foi poss�vel fazer o upload do arquivo.";
       													header("Location: maismedicos.php?modulo=principal/validarRemessaCredito&acao=A");
       													exit;
       													}
       														
       																if($detalhe){
       													$num_encontrados=0;
       													$num_nao_encontrados=0;
       													foreach($detalhe as $det){
       													$sql = "select
       													rmdid
       													from
       													maismedicos.pagamento_remessadetalhe
       															where
       															rmcid = $rmcid
       															and
       															cs_tipo_registro = '{$det['cs_tipo_registro']}'
       															and
       															nu_seq_registro = '{$det['nu_seq_registro']}'
       															and
       															nu_seq_credito = '{$det['nu_seq_credito']}'
       																	and
       																	cs_tipo_lote = ".TPLID_CREDITO."
       																	and
       																	nu_nib = '{$det['nu_nib']}'
       																	and
       																	id_orgao_pagador = '{$det['id_orgao_pagador']}'
       																		";
       																		$rmdid = $this->pegaUm($sql);
       																		if($rmdid){
       																		$num_encontrados++;
    
       																		$sql = "update
       																		maismedicos.pagamento_remessadetalhe
       																		set
       																		cs_ocorrencia	= '{$det['cs_ocorrencia']}',
       																		id_nit 			= '{$det['id_nit']}',
      									cs_especie 		= '{$det['cs_especie']}',
       																		nu_seq_rms 		= '{$det['nu_seq_rms']}',
      									sit_cmd			= '{$det['sit_cmd']}',
          									sit_rms 		= '{$det['sit_rms']}',
          									nu_ctrl_cred	= '{$det['nu_ctrl_cred']}',
          									dt_ocorrencia 	= '{$det['dt_ocorrencia']}'
          									where
          									rmdid = $rmdid";
          											$this->executar($sql);
    
          													if($this->commit() && $det['cs_ocorrencia'] == '0000'){
          													$this->enviaEmailValidarRemessaCredito($rmdid, true);
          									}
    
          									}else{
          									$num_nao_encontrados++;
          									}
          									}
          									$_SESSION['maismedicos']['remessa']['alert'] = "Retorno: $num_encontrados atualizados. $num_nao_encontrados n�o atualizados.";
          									header("Location: maismedicos.php?modulo=principal/validarRemessaCredito&acao=A");
          									exit;
          									}
          										
          									}
          									}
          									 
          									public function conteudoEmailValidarRemessaCredito($arDados = array())
          									{
          									$txt = "<p>Prezado(a)&nbsp;<b>{$arDados['nome']}</b>,</p>
    
          									<p>Informamos que sua bolsa de {$arDados['tipobolsa']} do Projeto Mais M�dicos para o Brasil, referente ao exerc�cio de {$arDados['mesano']},
          									no valor de R$ {$arDados['valor']}. {$arDados['vlrextenso']}, j� est� dispon�vel para saque, na conta espec�fica do benef�cio.</p>
          										
          									<p>Favor dirigir-se � ag�ncia do Banco do Brasil n� {$arDados['agbanco']}, informada no cadastro, para pegar seu Cart�o de Pagamento de
          									Benef�cio (consulte o endere�o da ag�ncia <a href=\"http://www37.bb.com.br/portalbb/redeAtendimento/Inicio,2,2316,2316.bbx\" target=\"_blank\">aqui</a> link para a consulta de ag�ncias BB).</p>
    
          									<p>Para resgatar o cart�o e cadastrar senha, � indispens�vel a apresenta��o do documento de identifica��o cadastrado no Projeto.</p>
          										
          									<p>De posse do seu cart�o, o saque na conta-benef�cio poder� ser efetuado em qualquer Terminal de Autoatendimento (Caixa Eletr�nico) do Banco do Brasil.</p>
          										
          									<p>Se porventura o seu cart�o ainda n�o estiver dispon�vel na ag�ncia n� {$arDados['agbanco']}, � poss�vel efetuar o saque do benef�cio diretamente no
          									caixa de qualquer ag�ncia do Banco do Brasil. Voc� deve imprimir este comprovante e apresent�-lo ao caixa da ag�ncia.</p>
          										
          									<p>N� do Conv�nio: 526</p>
          										
          									<p>N� do Benef�cio: {$arDados['numero_ben']}</p>
          										
          									<p>CPF do Benefici�rio: {$arDados['cpf']}</p>
          										
          									<p>&nbsp;</p>
			
					<b>OBS:</b> O cr�dito desta bolsa ter� validade de 90 dias, expirando em {$arDados['dt_fim_validade']}, quando o valor ser� estornado pelo banco.
			
    							<p>&nbsp;</p>
    
    									<p>Projeto Mais M�dicos para o Brasil<br/>
    											EBSERH - Empresa Brasileira de Servi�os Hospitalares<br/>
    											MEC - Minist�rio da Educa��o<br/></p>
   					";
    
    											return $txt;
          									}
          									 
          									public function enviaEmailValidarRemessaCredito($rmdid = null, $boEnvia = true)
          									{
    
   			if($rmdid){
       			$sql = "select
       			bennome,
       			benemail,
       			bentipo,
   							apgmes,
    											apgano,
    											benagencia,
    													bencpf,
    															nu_nib,
    																	to_char(to_date(dt_fim_validade,'YYYYMMDD'),'DD/MM/YYYY') as dt_fim_validade
   						from maismedicos.pagamento_remessadetalhe d
       								join maismedicos.pagamento_beneficiario t on d.benid = t.benid
       										join maismedicos.pagamento_autorizacao a ON a.rmdid = d.rmdid
       												where d.rmdid = $rmdid";
       													
       												$arDetalhe = $this->pegaLinha($sql);
       													
       												$arDados['nome'] 		= $arDetalhe['bennome'] ? $arDetalhe['bennome'] : 'n�o informado';
   				$arDados['email'] 		= $arDetalhe['benemail'] ? $arDetalhe['benemail'] : 'n�o informado';
       						$arDados['tipobolsa'] 	= $arDetalhe['bentipo'] ? ($arDetalhe['bentipo'] == 'T' ? 'benoria' : 'supervis�o' ) : 'n�o informado';
       						$arDados['mesano'] 		= $arDetalhe['apgmes'] ? $arDetalhe['apgmes'].'/'.$arDetalhe['apgano'] : 'n�o informado';
       						$arDados['valor'] 		= $arDetalhe['bentipo'] ? ($arDetalhe['bentipo'] == 'T' ? '5.000,00' : '4.000,00') : 'n�o informado';
       						$arDados['vlrextenso']	= $arDetalhe['bentipo'] ? valorMonetarioExtenso($arDetalhe['bentipo'] == 'T' ? 5000.00 : 4000.00) : '(por extenso)';
       						$arDados['agbanco']		= $arDetalhe['benagencia'] ? $arDetalhe['benagencia'] : 'n�o informado';
       						$arDados['cpf']			= $arDetalhe['bencpf'] ? formatar_cpf($arDetalhe['bencpf']) : 'n�o informado';
   				$arDados['numero_ben']	= $arDetalhe['nu_nib'] ? $arDetalhe['nu_nib'] : 'n�o informado';
       				$arDados['dt_fim_validade']	= $arDetalhe['dt_fim_validade'] ? $arDetalhe['dt_fim_validade'] : 'n�o informado';
       					
       				$conteudo = $this->conteudoEmailValidarRemessaCredito($arDados);
       						}
    
       						if($boEnvia){
       							
    	   			require_once APPRAIZ . 'includes/phpmailer/class.phpmailer.php';
    	   			require_once APPRAIZ . 'includes/phpmailer/class.smtp.php';
    	   
    				$mensagem = new PHPMailer();
    				$mensagem->persistencia = $this;
    				$mensagem->Host         = "localhost";
    				$mensagem->Mailer       = "smtp";
    	   	
    				$mensagem->From     = 'noReply@mec.gov.br';
    				$mensagem->FromName = 'SIMEC';
    
    				if(in_array($_SESSION['baselogin'], array('simec_desenvolvimento', 'simec_espelho_producao'))){
    					$mensagem->AddAddress( 'henrique.couto@ebserh.gov.br', 'Henrique Xavier Couto'  );
    					$mensagem->AddAddress( $_SESSION['usuemail'], $_SESSION['usunome']  );
    					$mensagem->AddAddress( 'wescley.lima@ebserh.gov.br', 'Wescley Guedes Lima'  );
    				}else{
    					$mensagem->AddAddress( $arDados['email'], $arDados['nome']  );
    				}
    
    				$mensagem->Subject = 'SIMEC - Mais M�dicos - Bolsa '.ucwords($arDados['tipobolsa']); //assunto
    				$mensagem->Body = $conteudo; //conteudo
    				$mensagem->IsHTML( true );
    
    				if($mensagem->Send()){
    					$sql = "update maismedicos.pagamento_remessadetalhe set bo_email_enviado = true, dt_envio_email = now() where rmdid = {$rmdid}";
    					$this->executar($sql);
    					$this->commit();
    				}
       			}
    
       			return $conteudo;
       		}
}