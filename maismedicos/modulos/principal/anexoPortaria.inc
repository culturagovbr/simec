<!doctype html>
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Connection" content="Keep-Alive">
<meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
<title>Anexo</title>
<script type="text/javascript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<style>
@media print 
{
    .noPrint 
    {
        display:none;
    }
}
</style>
<script type="text/javascript">
this._closeWindows = false;
</script>
</head>
<body style="margin:10px; padding:0; background-color: #fff;">
<div style="text-align:justify;" >
<?php
$uniid = $_GET['uniid'];

if($uniid){
	$sql = "select
				usunome as nome,
				uninome as entnome,
				unicnpj as entnumcpfcnpj,
				mun.mundescricao,
				mun.estuf
			from
				seguranca.usuario usu
			inner join
				maismedicos.usuarioresponsabilidade ur ON ur.usucpf = usu.usucpf
			inner join
				maismedicos.universidade uni ON uni.uniid = ur.uniid
			inner join
				territorios.municipio mun ON mun.muncod = uni.muncod
			where
				ur.rpustatus = 'A'
			and
				uni.uniid = $uniid";
	$arrDados = $db->pegaLinha($sql);
	$arrReitor = $arrDados;
	if(!$arrDados)
	{
		echo "<script>alert('Favor atribuir o Reitor da Universidade');window.close();</script>";
		exit;
	}
}else{
	echo "<script>alert('Favor informar a Universidade');window.close();</script>";
	exit;
}

?>
<p><b>PROGRAMA MAIS M�DICOS</b></p>

<p>Termo de Pr�-Ades�o ao Projeto Mais M�dicos</p>

<p>Pelo presente termo a <?php echo $arrDados['entnome'] ?>, com sede no munic�pio de <?php echo $arrDados['mundescricao'] ?> - <?php echo $arrDados['estuf'] ?>, inscrita no CNPJ/MF sob o n� <?php echo mascara_global_maismedicos_tela($arrDados['entnumcpfcnpj'],"##.###.###/####-##") ?>, neste ato representada por seu Magnifico (a) Reitor (a) <?php echo $arrReitor['nome'] ?>, doravante, intitulada UNIVERSIDADE manifesta inten��o de pr�-ades�o Projeto Mais M�dicos.</p>

<p>Cl�usula Primeira - Do Objeto</p>

<p>O presente termo de ades�o tem por objeto viabilizar a tutoria e supervis�o presencial e a dist�ncia de m�dicos formados em institui��es de educa��o superior brasileiras ou com diploma revalidado no Brasil e m�dicos formados em institui��es de educa��o superior estrangeiras, por meio de interc�mbio m�dico internacional inscritos Projeto Mais M�dicos, nos termos da Medida Provis�ria no 621, de 2013, e na Portaria Interministerial MS/MEC no 1.369, de 8 de julho de 2013.</p>

<p>Cl�usula Segunda - Das Obriga��es</p>

<p>Para consecu��o do objeto do presente termo a UNIVERSIDADE dever�:</p>

<p>I - Indicar um tutor acad�mico que iniciar� suas atividades a partir da assinatura do presente termo e, no m�nimo, tr�s tutores que compor�o cadastro reserva;</p>
<p>II - Indicar a unidade respons�vel pela avalia��o e autoriza��o de pagamento das bolsas de tutoria e supervis�o acad�micas;</p>
<p>III - Cadastrar via sistema SIMEC, no m�dulo rede federal, por meio do endere�o eletr�nico http://simec.mec.gov.br, os tutores indicados e a unidade de avalia��o e autoriza��o de pagamento de bolsas;</p>
<p>IV - Definir mecanismo de avalia��o e autoriza��o de pagamento das bolsas de tutoria e supervis�o.</p>
<p>V - Firmar, em caso de valida��o, termo de ades�o com o Minist�rio da Educa��o.</p> 

<p>Cl�usula Terceira - Dos Tutores Acad�micos</p>

<p>I - O Tutor Acad�mico ser� escolhido pela UNIVERSIDADE dentre os docentes da �rea m�dica, vinculados, preferencialmente, a �rea de conhecimento de sa�de coletiva ou correlata ou cl�nica geral;</p>
<p>II - O tutor acad�mico � respons�vel pela orienta��o acad�mica e pelo planejamento das atividades do supervisor;</p>
<p>III - Os tutores do cadastro reserva poder�o ser convocados de acordo com o n�mero de m�dicos selecionados para o programa;</p>
<p>IV - Para o desenvolvimento de suas atividades o tutor acad�mico receber� bolsa-tutoria no valor de R$ 5.000,00 (cinco mil reais).</p>
<p>V - S�o atribui��es do tutor acad�mico, sem preju�zo de outras que vierem a ser definidas pela coordena��o do Projeto Mais M�dicos:</p>
<p>a) coordenar as atividades acad�micas da integra��o ensino-servi�o, atuando em coopera��o com os supervisores e os gestores do SUS;</p>
<p>b) indicar, em plano de trabalho, as atividades a serem executadas pelos m�dicos participantes e supervisores e a metodologia de acompanhamento e avalia��o;</p>
<p>c) monitorar o processo de acompanhamento e avalia��o a ser executado pelos supervisores, garantindo sua continuidade;</p>
<p>d) integrar as atividades do curso de especializa��o �s atividades de integra��o ensino-servi�o;</p>
<p>e) relatar � institui��o p�blica de ensino superior � qual est� vinculado a ocorr�ncia de situa��es nas quais seja necess�ria a ado��o de provid�ncia pela institui��o; e</p>
<p>f) apresentar relat�rios peri�dicos da execu��o de suas atividades no Projeto Mais M�dicos � institui��o p�blica de ensino superior � qual est� vinculado e � Coordena��o do Mais M�dicos.</p>

<p><?php echo $arrDados['mundescricao'] ?> - <?php echo $arrDados['estuf'] ?> / <?php echo date("d/m/Y") ?></p>

<p>_____________________________________________</p>
<p><?php echo $arrReitor['nome'] ?></p>
</div>
<div class="noPrint">
	<input type="button" name="btn_imprimir" value="Imprimir" onclick="window.print()" />
	<input type="button" name="btn_fechar" value="Fechar" onclick="window.close()" />
</div>
</body>
</html>