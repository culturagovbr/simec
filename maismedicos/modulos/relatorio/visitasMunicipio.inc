<?php 

if($_REQUEST['requisicao']){
	include 'visitasMunicipio_result.inc';
	die;
}

include APPRAIZ . 'includes/cabecalho.inc';
include APPRAIZ . 'includes/Agrupador.php';

echo "<br>";
$titulo_modulo = "Relat�rio Visitas por Munic�pio";
monta_titulo( $titulo_modulo, 'Selecione os filtros e agrupadores desejados' );
$arPerfil = arrayPerfil();
?>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script>

	function exibeRelatorioVisitasMunicipio(tipo)
	{
		document.filtro.requisicao.value = tipo;

		selectAllOptions(document.getElementById('muncod'));
		selectAllOptions(document.getElementById('uniid'));
		selectAllOptions(document.getElementById('supervisor'));
		selectAllOptions(document.getElementById('medico'));
	
		document.filtro.target = 'resultadoVisitasMunicipio';
		var janela = window.open( '', 'resultadoVisitasMunicipio', 'width=780,height=465,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
		janela.focus();
	
		document.filtro.submit();
	}

	/**
	 * Alterar visibilidade de um bloco.
	 * 
	 * @param string indica o bloco a ser mostrado/escondido
	 * @return void
	 */
	function onOffBloco( bloco )
	{
		var div_on = document.getElementById( bloco + '_div_filtros_on' );
		var div_off = document.getElementById( bloco + '_div_filtros_off' );
		var img = document.getElementById( bloco + '_img' );
		var input = document.getElementById( bloco + '_flag' );
		if ( div_on.style.display == 'none' )
		{
			div_on.style.display = 'block';
			div_off.style.display = 'none';
			input.value = '0';
			img.src = '/imagens/menos.gif';
		}
		else
		{
			div_on.style.display = 'none';
			div_off.style.display = 'block';
			input.value = '1';
			img.src = '/imagens/mais.gif';
		}
	}
	
	/**
	 * Alterar visibilidade de um campo.
	 * 
	 * @param string indica o campo a ser mostrado/escondido
	 * @return void
	 */
	function onOffCampo( campo )
	{
		var div_on = document.getElementById( campo + '_campo_on' );
		var div_off = document.getElementById( campo + '_campo_off' );
		var input = document.getElementById( campo + '_campo_flag' );
		if ( div_on.style.display == 'none' )
		{
			div_on.style.display = 'block';
			div_off.style.display = 'none';
			input.value = '1';
		}
		else
		{
			div_on.style.display = 'none';
			div_off.style.display = 'block';
			input.value = '0';
		}
	}

</script>
<form action="" method="post" name="filtro">
	<input type="hidden" name="requisicao" value="" />
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">
		<?php if(!in_array(PERFIL_SUPERVISOR, $arPerfil) && !in_array(PERFIL_TUTOR, $arPerfil)): ?>
		<tr>
			<td class="subtitulodireita">UF</td>
			<td>
				<?php				
				$sql = "select 
							estuf as codigo,
							estdescricao as descricao
						from territorios.estado
						order by estuf";
				
				$db->monta_combo('estuf',$sql,'S','Selecione...',null,null,null,null,'N','estuf');
				?>
			</td>
		</tr>
		<?php 
		mostrarComboPopupMunicipios();
		?>
		<tr>
			<td class="subtitulodireita">S� capitais</td>
			<td><input type="checkbox" name="socapitais" /></td>
		</tr>
		<?php endif; ?>
		<?php 
		if(in_array(PERFIL_SUPERVISOR, $arPerfil) || in_array(PERFIL_TUTOR, $arPerfil)){
			$arWhere[] = "usr.usucpf = '{$_SESSION['usucpf']}'";
		}
		
		// -- Situa��o
		$sql_carregados = '';
		
		$stSql = "select 
						u.uniid as codigo, 
						u.uninome as descricao 
				from maismedicos.universidade u
				left join maismedicos.usuarioresponsabilidade usr on usr.uniid = u.uniid
					and usr.pflcod in (".implode(",",$arPerfil).")
				where unistatus = 'A'
				".($arWhere ? " and ".implode(" and ", $arWhere) : "")." 
				order by uninome asc";
		
		mostrarComboPopup('Institui��o Supervisora', 'uniid', $stSql, $sql_carregados, 'Selecione a(s) Institui��o(�es) Supervisora(s)');
		
		$sql_carregados = '';
		
		$stSql = "select distinct
					tutcpf as codigo,
					tutnome as descricao
				from maismedicos.tutor t
				left join maismedicos.usuarioresponsabilidade usr on usr.uniid = t.uniid
					and usr.pflcod in (".implode(",",$arPerfil).")
				where tuttipo = 'S'
				and tutstatus = 'A'
				".($arWhere ? " and ".implode(" and ", $arWhere) : "")."
				order by tutnome";
		
		$where = array(
				array("codigo"=>"tutnome","descricao"=>"Nome"),
				array("codigo"=>"tutcpf","descricao"=>"CPF")
		);
		mostrarComboPopup('Supervisores', 'supervisor', $stSql, $sql_carregados, 'Selecione o(s) Supervisor(es)', $where);
		
		$sql_carregados = '';
		
		$stSql = "select
	 				mdccpf as codigo,
	 				mdcnome as descricao
	 			from maismedicos.medico m
				left join maismedicos.universidade u on u.idunasus = m.coinstituicaosupervisora
				left join maismedicos.usuarioresponsabilidade usr on usr.uniid = u.uniid
					and usr.pflcod in (".implode(",",$arPerfil).")
	 			where mdcstatus = 'A'
				".($arWhere ? " and ".implode(" and ", $arWhere) : "")."
	 			order by removeacento(lower(trim(mdcnome))) asc";
		
		$where = array(
				array("codigo"=>"mdcnome","descricao"=>"Nome"),
				array("codigo"=>"mdccpf","descricao"=>"CPF")
		);
		mostrarComboPopup('M�dicos', 'medico', $stSql, $sql_carregados, 'Selecione o(s) M�dico(s)',$where);
		?>
		<tr>
			<td class="subtitulodireita">Per�odo</td>
			<td>
				<?php
				$a=0;
				for($x=1;$x<13;$x++){
					$meses_periodo[$a]['codigo'] = $x;
					$meses_periodo[$a]['descricao'] = mes_extenso($x);
					$a++;
				}
				
				$db->monta_combo('mesvisitainicio',$meses_periodo,'S','Selecione...',null,null,null,null,'N','mesvisita');
				
				$b=0;
				for($y=2013;$y<2018;$y++){
					$anos_periodo[$b]['codigo'] = $y;
					$anos_periodo[$b]['descricao'] = $y;
					$b++;
				}
				
				$db->monta_combo('anovisitainicio',$anos_periodo,'S','Selecione...',null,null,null,null,'N','anovisita');
				
				echo "&nbsp;IN�CIO<br/>";
				
				$a=0;
				for($x=1;$x<13;$x++){
					$meses_periodo[$a]['codigo'] = $x;
					$meses_periodo[$a]['descricao'] = mes_extenso($x);
					$a++;
				} 
				
				$db->monta_combo('mesvisitafim',$meses_periodo,'S','Selecione...',null,null,null,null,'N','mesvisitafim');
				
				$b=0;
				for($y=2013;$y<2018;$y++){
					$anos_periodo[$b]['codigo'] = $y;
					$anos_periodo[$b]['descricao'] = $y;
					$b++;
				}
				
				$db->monta_combo('anovisitafim',$anos_periodo,'S','Selecione...',null,null,null,null,'N','anovisitafim');
				?>
				FIM
			</td>
		</tr>		
		<tr>
			<td class="subtituloDireita">Ciclo</td>
			<td>
				<?php 
				$arCiclos = array(
						array('codigo'=>1,'descricao'=>'Primeiro ciclo'),
						array('codigo'=>2,'descricao'=>'Segundo ciclo'),
						array('codigo'=>3,'descricao'=>'Terceiro ciclo'),
						array('codigo'=>4,'descricao'=>'Quarto ciclo'),
						array('codigo'=>5,'descricao'=>'Quinto ciclo')
					);
				
				$db->monta_combo('ciclo',$arCiclos,'S','Selecione...',null,null,null,null,'N','clico');
				?>
			</td>
		</tr>	
		<tr>
			<td class="subtitulodireita">Mostrar DSEI</td>
			<td><input type="checkbox" name="incluirdsei" checked/></td>
		</tr>
		<?php if(!in_array(PERFIL_SUPERVISOR, $arPerfil) && !in_array(PERFIL_TUTOR, $arPerfil)): ?>
		<tr>
			<td class="subtitulodireita">Qtde. de Meses (colunas)</td>
			<td><?php echo campo_texto('qtde_meses', 'N', 'S', '', '5', '4', '####', '', '', '', '', '', '', 1); ?></td>
		</tr>
		<?php else: ?>
			<input type="hidden" name="qtdemeses" value="10000" />
		<?php endif; ?>	
		
		<tr>
			<td bgcolor="#CCCCCC"></td>
			<td bgcolor="#CCCCCC">
				<input type="button" value="Visualizar" onclick="exibeRelatorioVisitasMunicipio('exibir');" style="cursor: pointer;"/>
				<input type="button" value="Visualizar XLS" onclick="exibeRelatorioVisitasMunicipio('exibirxls');" style="cursor: pointer;"/>
			</td>
		</tr>
	</table>
</form>