<?php 
if($_REQUEST['requisicao']=='gerarRelatorio'){
	ini_set("memory_limit","456M");
	ob_clean();
	include "relatorio_FluxoProcesso_resultado.inc";
	exit();
}

include_once APPRAIZ . "includes/cabecalho.inc";
echo "<br/>";

monta_titulo( 'Relat�rio de Fluxo Processos', '<b> Demonstra todas as movimenta��es de entrada e saida de processos para o advogado no per�odo </b>' ); 
?>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>	
<script language="javascript" type="text/javascript">
	jQuery.noConflict();

	function pesquisarManifestacao(){
		var formulario = document.formulario;
        selectAllOptions(document.getElementById('advid'));
        selectAllOptions(document.getElementById('coonid'));
		if(jQuery('[name=htddatainicial]').val() == '' || jQuery('[name=htddatfinal]').val() == ''){
			alert('O campo "Per�odo" � obrigat�rio!');
			jQuery('[name=htddatainicial]').focus();
			return false;
		} else {
			if(!validaDataMaior(formulario.htddatainicial,formulario.htddatfinal)){
				alert("Data Inicial n�o pode ser maior Data Final!");
				jQuery('[name=htddatainicial]').focus();
				return false;
		    }
		} 

		if( jQuery("[name='relA']").is(':checked') ){
	        if ( jQuery('#advid').val() == '' || jQuery('#advid').val() == null ) {
	            alert('Selecione pelo menos um Advogado!');
	            jQuery('#advid').focus();
	            return false;
	        }
	    }

	    if( jQuery("[name='relC']").is(':checked') ){
	    	if( jQuery('#coonid').val() == '' || jQuery('#coonid').val() == null ){
	    		alert('Selecione pelo menos uma Cordena��o!');1111122225665
	            jQuery('#coonid').focus();
	            return false;
	    	}
	    }

    	if(jQuery('#relatorio:checked').length <= 0){
    		alert('Selecione um "Tipo de Relat�rio"!');
    		jQuery('#relatorio').focus();
    		return false;
    	}	

		var janela = window.open( '', 'relatorio', 'width=900,height=600,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
		formulario.target = 'relatorio';
		formulario.submit();
		janela.focus();
	} 

	function abrepopupAdvogados(){
	    window.open('/conjur/combo_advogados_bandalarga.php','Advogados','width=500,height=500,scrollbars=1');
	}

	function abrepopupCoordenacoes(){
	    window.open('/conjur/combo_coordenacoes_bandalarga.php','Advogados','width=500,height=500,scrollbars=1');
	}
</script>

<form name="formulario" id="formulario" method="post">
	<input type="hidden" id="requisicao" name="requisicao" value="gerarRelatorio"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="border-bottom:none;">
		<tr>
			<td class="subtitulodireita">Per�odo:</td>
			<td>
				<?php echo campo_data2('htddatainicial', 'S', 'S', 'Per�odo Inicial', 'S', '', '', $htddatainicial); ;?>&nbsp;&nbsp;&nbsp;&nbsp;a
				&nbsp;&nbsp;&nbsp;&nbsp;<?php echo campo_data2('htddatfinal', 'S', 'S', 'Per�odo Final', 'S', '', '', $htddatfinal);  ?>		
			</td>
		</tr>
        <tr>
	        <td class="subtitulodireita">Advogado(s):</td>
            <td>
    	        <select multiple="multiple" name="advid[]" onDblClick="abrepopupAdvogados();" id="advid" style="width:400px;" >
        	        <option value="">Duplo clique para selecionar da lista</option>
                </select>
            </td>
        </tr>
        <tr>
	        <td class="subtitulodireita">Coordena��o(�es):</td>
            <td>
    	        <select multiple="multiple" name="coonid[]" onDblClick="abrepopupCoordenacoes();" id="coonid" style="width:400px;" >
        	        <option value="">Duplo clique para selecionar da lista</option>
                </select>
            </td>
        </tr>
        <tr>
	        <td class="subtitulodireita">Tipo de Relat�rio:</td>
            <td>
				<input type="checkbox" class="normal" value="A" name="relA" id="relatorio" style="margin-top: -1px;">&nbsp;Advogado
				<input type="checkbox" class="normal" value="C" name="relC" id="relatorio" style="margin-top: -1px;">&nbsp;Coordena��o
            </td>
        </tr>        
		<tr>
			<td align="center" bgcolor="#DCDCDC" colspan="2">
				<input type="button" value="Pesquisar" id="btnPesquisar" onclick="pesquisarManifestacao();"/>
				<input type="reset" value="Limpar" id="btnLimpar"/>
			</td>
		</tr>				
	</table>
</form>

<script>
// limpar combos quando reset do form acionado
jQuery('document').ready(function(){
	jQuery("[type='reset']").click(function(){
		jQuery('#advid').html('<option value="">Duplo clique para selecionar da lista</option>');
		jQuery('#coonid').html('<option value="">Duplo clique para selecionar da lista</option>');
	});
});
</script>