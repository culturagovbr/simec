<?php
function getmicrotime()
{list($usec, $sec) = explode(" ", microtime());
 return ((float)$usec + (float)$sec);} 


date_default_timezone_set ('America/Sao_Paulo');

$_REQUEST['baselogin'] = "simec_espelho_producao";

/* configura��es */
ini_set("memory_limit", "2048M");
set_time_limit(0);
/* FIM configura��es */

define( 'BASE_PATH_SIMEC', realpath( dirname( __FILE__ ) . '/../../../' ) );

error_reporting( E_ALL ^ E_NOTICE );

$_REQUEST['baselogin']  = "simec_espelho_producao";//simec_desenvolvimento

// carrega as fun��es gerais
require_once BASE_PATH_SIMEC . "/global/config.inc";
include_once APPRAIZ . "includes/classes_simec.inc";
include_once APPRAIZ . "includes/funcoes.inc";
include_once APPRAIZ . "seguranca/www/_funcoesmonitoramento.php";

// CPF do administrador de sistemas
if(!$_SESSION['usucpf']) {
	$_SESSION['usucpforigem'] = '00000000191';
	$_SESSION['usucpf'] = '00000000191';
}

$_SESSION['sisid'] = '4';

$microtime = getmicrotime();


// abre conex�o com o servidor de banco de dados
$db = new cls_banco();

if($_REQUEST['ano'])$ano=$_REQUEST['ano'];
else $ano=date("Y");

if($_REQUEST['mes'])$mes=$_REQUEST['mes'];
else $mes=date("m");


//integrar com servidor do pdeinterativo

$sql = "select sisid, sisdsc, sisdiretorio
		from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
		'SELECT sisid, sisdsc, sisdiretorio FROM seguranca.sistema WHERE sisstatus=''A''
		            ') 
		as pd (
		sisid integer,
		sisdsc varchar,
		sisdiretorio varchar
		)";

$sistemas = $db->carregar($sql);

if($sistemas[0]) {
	foreach($sistemas as $sis) {

		$Tinicio = getmicrotime();

		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE USU�RIOS DISTINTOS(NU)
		*/

		unset($dadosnu);
		
		$sql = "select num, dia
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT COUNT(DISTINCT usucpf) as num, to_char(estdata, ''DD'') as dia FROM seguranca.estatistica WHERE sisid=''{$sis['sisid']}'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."'' AND estdata > (NOW()-interval ''10 days'') GROUP BY to_char(estdata, ''DD'') ORDER BY dia
				            ') 
				as pd (
				num varchar,
				dia varchar
				)";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$diasnu[] = $d['dia'];
				$dadosnu[$d['dia']] = $d['num'];
			}
			
			$sql = "select num
					from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
					'SELECT COUNT(DISTINCT usucpf) as num FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''
					            ') 
					as pd (
					num varchar)";

			$dadosnu['NULL'] = $db->pegaUm($sql);
			
		}

		if($diasnu) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasnu)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NU."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NU."'";
		$db->executar($sql);
		
		if($dadosnu) {
			foreach($dadosnu as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NU."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE USU�RIOS DISTINTOS(NU)
		*/
		
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE ERROS(NE)
		*/
		
		unset($dadosne);
		
		
		$sql = "select num, dia
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT COUNT(au.oid) as num, to_char(auddata, ''DD'') as dia FROM seguranca.auditoria au
					    INNER JOIN seguranca.menu me ON au.mnuid=me.mnuid
						WHERE me.sisid=''".$sis['sisid']."'' AND au.audtipo=''X'' AND to_char(auddata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''  AND auddata > (NOW()-interval ''10 days'')
						GROUP BY to_char(auddata, ''DD'') ORDER BY dia
						            ')
						as pd (
						num varchar,
						dia varchar
						)";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$diasne[]           = $d['dia'];
				$dadosne[$d['dia']] = $d['num'];
			}
			
			
			$sql = "select num
					from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
					'SELECT COUNT(au.oid) as num FROM seguranca.auditoria au
				    INNER JOIN seguranca.menu me ON au.mnuid=me.mnuid
					WHERE me.sisid=''".$sis['sisid']."'' AND au.audtipo=''X'' AND to_char(auddata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''
						            ')
						as pd (
						num varchar
						)";
				
			$dadosne['NULL'] = $db->pegaUm($sql);
		}
		
		if($diasne) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasne)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NE."'";
			$db->executar($sql);
		}
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NE."'";
		$db->executar($sql);
		
		if($dadosne) {
			foreach($dadosne as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NE."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE ERROS(NE)
		*/
		
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO TEMPO M�DIO DE PROCESSAMENTO POR P�GINA (TM)
		*/
		
		unset($dadostm);
		
		$sql = "select num, dia
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT ROUND(CAST(AVG(esttempoexec)as numeric),2) as num, to_char(estdata, ''DD'') as dia FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."'' AND estdata > (NOW()-interval ''10 days'') GROUP BY to_char(estdata, ''DD'') ORDER BY dia
						            ')
						as pd (
						num varchar,
						dia varchar
						)";

		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$diastm[]           = $d['dia'];
				$dadostm[$d['dia']] = $d['num'];
			}
			
			$sql = "select num
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT ROUND(CAST(AVG(esttempoexec)as numeric),2) as num FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''
						            ')
						as pd (
						num varchar
						)";
				
			$dadostm['NULL'] = $db->pegaUm($sql);
		}
		
		if($diastm) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diastm)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".TM."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".TM."'";
		$db->executar($sql);
		
		if($dadostm) {
			foreach($dadostm as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".TM."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO TEMPO M�DIO DE PROCESSAMENTO POR P�GINA (TM)
		*/
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE REQUISI��ES (NR)
		*/
		
		unset($dadosnr);
		
		$sql = "select num, dia
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT COUNT(oid) as num, to_char(estdata, ''DD'') as dia FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."'' AND estdata > (NOW()-interval ''10 days'') GROUP BY to_char(estdata, ''DD'') ORDER BY dia
						            ')
						as pd (
						num varchar,
						dia varchar
						)";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$diasnr[]           = $d['dia'];
				$dadosnr[$d['dia']] = $d['num'];
			}
			
			
			$sql = "select num
					from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
					'SELECT COUNT(oid) as num FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''
							            ')
							as pd (
							num varchar
							)";
			
			$dadosnr['NULL'] = $db->pegaUm($sql);
		}
		
		if($diasnr) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasnr)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NR."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".NR."'";
		$db->executar($sql);
		
		if($dadosnr) {
			foreach($dadosnr as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NR."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE REQUISI��ES (NR)
		*/
		
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO RELA��O ERROS POR REQUISI��ES (PE)
		*/
		
		unset($dadospe);
		
		if($dadosne) {
			foreach($dadosne as $da => $d) {
				if($dadosnr[$da]) {
					if(is_numeric($da)) {
						$diaspe[]     = $da;
					}
					$dadospe[$da] = round(($d/$dadosnr[$da])*100,4);
				}
			}
		}
		
		if($diaspe) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diaspe)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".PE."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".PE."'";
		$db->executar($sql);
		
		if($dadospe) {
			foreach($dadospe as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".PE."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO RELA��O ERROS POR REQUISI��ES (PE)
		*/
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO MEMORIA UTILIZADA (MU)
		*/
		
		unset($dadosmu);
		
		$sql = "select num, dia
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT ROUND(CAST(AVG(estmemusa)as numeric),2) as num, to_char(estdata, ''DD'') as dia FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."'' AND estdata > (NOW()-interval ''10 days'') GROUP BY to_char(estdata, ''DD'') ORDER BY dia
						            ')
						as pd (
						num varchar,
						dia varchar
						)";

		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$diasmu[]           = $d['dia'];
				$dadosmu[$d['dia']] = $d['num'];
			}
			
			$sql = "select num
					from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
					'SELECT ROUND(CAST(AVG(estmemusa)as numeric),2) as num FROM seguranca.estatistica WHERE sisid=''".$sis['sisid']."'' AND to_char(estdata, ''YYYY-MM'')=''".sprintf("%04d-%02d", $ano, $mes)."''
							            ')
							as pd (
							num varchar
							)";
				
			$dadosmu['NULL'] = $db->pegaUm($sql);
		}
		
		if($diasmu) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasmu)."') AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".MU."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND monsisdiretorio='".$sis['sisdiretorio']."' AND tmoid='".MU."'";
		$db->executar($sql);
		
		if($dadosmu) {
			foreach($dadosmu as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".MU."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO MEMORIA UTILIZADA (MU)
		*/
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UA)
		*/
		
		unset($dadosua);
		
		$sql = "select num
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT COUNT(DISTINCT usucpf) as num FROM seguranca.usuario_sistema WHERE sisid=''".$sis['sisid']."'' AND suscod=''A''
						            ')
						as pd (
						num varchar
						)";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$dadosua[date("d")] = $d['num'];
			}
			
			
			$dadosua['NULL'] = $dadosua[date("d")];
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND monsisdiretorio='".$sis['sisdiretorio']."' AND (mondia IS NULL OR mondia=".date("d").") AND tmoid='".UA."'";
		$db->executar($sql);
		if($dadosua) {
			foreach($dadosua as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".UA."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UA)
		*/
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UP)
		*/
		
		unset($dadosup);
		
		$sql = "select num
				from dblink('host=192.168.222.184 user=sysdbsimec_consulta password=sysdbsimec_consulta port=5432 dbname=dbsimec_pde',
				'SELECT COUNT(DISTINCT usucpf) as num FROM seguranca.usuario_sistema WHERE sisid=''".$sis['sisid']."'' AND suscod=''P''
						            ')
						as pd (
						num varchar
						)";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			foreach($dados as $d) {
				$dadosup[date("d")] = $d['num'];
			}
				
			$dadosup['NULL'] = $dadosup[date("d")];
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND monsisdiretorio='".$sis['sisdiretorio']."' AND (mondia IS NULL OR mondia=".date("d").") AND tmoid='".UP."'";
		$db->executar($sql);
		if($dadosup) {
			foreach($dadosup as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, monsisdsc, monsisdiretorio, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".UP."', '".$sis['sisdsc']." (PDEInterativo)', '".$sis['sisdiretorio']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UP)
		*/
		
		$db->commit();
		
		$HTML .= $sis['sisdsc']." foi carregado... ".number_format( ( getmicrotime() - $Tinicio ), 4, ',', '.' )."s<br>";
		$_TOTTEMPO[] = getmicrotime() - $Tinicio;
		
		
	}
}

if($_REQUEST['cargapdeinterativo']) {
	echo $HTML;
	echo "<pre>";
	print_r($_TOTTEMPO);
	echo "</pre>";
    $db->close();
	exit;
}




$sql = "SELECT * FROM seguranca.sistema WHERE sisstatus='A'";
$sistemas = $db->carregar($sql);

if($sistemas[0]) {
	foreach($sistemas as $sis) {
		
		$Tinicio = getmicrotime();
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE USU�RIOS DISTINTOS(NU)
		 */
		
		unset($dadosnu);
		
		$sql = "SELECT COUNT(DISTINCT usucpf) as num, to_char(estdata, 'DD') as dia FROM estatistica.estatistica_".sprintf("%04d_%02d", $ano, $mes)." WHERE sisid='".$sis['sisid']."' AND to_char(estdata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."' GROUP BY to_char(estdata, 'DD') ORDER BY dia";
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalnu = 0;
			foreach($dados as $d) {
				$diasnu[] 			= $d['dia'];
				$dadosnu[$d['dia']] = $d['num'];
				$totalnu 			+= $d['num'];
			}
			
			$sql = "SELECT COUNT(DISTINCT usucpf) as num FROM estatistica.estatistica_".sprintf("%04d_%02d", $ano, $mes)." WHERE sisid='".$sis['sisid']."' AND to_char(estdata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."'";
			$dadosnu['NULL'] = $db->pegaUm($sql);
		}

		if($diasnu) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasnu)."') AND sisid='".$sis['sisid']."' AND tmoid='".NU."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".NU."'";
		$db->executar($sql);
		
		if($dadosnu) {
			foreach($dadosnu as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NU."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE USU�RIOS DISTINTOS(NU)
		 */
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE ERROS(NE)
		 */
		
		unset($dadosne);
		
		$sql = "SELECT COUNT(au.audid) as num, to_char(auddata, 'DD') as dia FROM auditoria.auditoria_".sprintf("%04d_%02d", $ano, $mes)." au 
			    INNER JOIN seguranca.menu me ON au.mnuid=me.mnuid 
				WHERE me.sisid='".$sis['sisid']."' AND au.audtipo='X' AND to_char(auddata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."'  
				GROUP BY to_char(auddata, 'DD') ORDER BY dia";
		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalne = 0;
			foreach($dados as $d) {
				$diasne[]           = $d['dia'];
				$dadosne[$d['dia']] = $d['num'];
				$totalne 			+= $d['num'];
			}
			
			$dadosne['NULL'] = $totalne;
		}

		if($diasne) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasne)."') AND sisid='".$sis['sisid']."' AND tmoid='".NE."'";
			$db->executar($sql);
		}
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".NE."'";
		$db->executar($sql);
		
		if($dadosne) {
			foreach($dadosne as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NE."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE ERROS(NE)
		 */

		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO TEMPO M�DIO DE PROCESSAMENTO POR P�GINA (TM)
		 */
		
		unset($dadostm);
		
		$sql = "SELECT ROUND(CAST(AVG(esttempoexec)as numeric),2) as num, to_char(estdata, 'DD') as dia FROM estatistica.estatistica_".sprintf("%04d_%02d", $ano, $mes)." WHERE sisid='".$sis['sisid']."' AND to_char(estdata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."' GROUP BY to_char(estdata, 'DD') ORDER BY dia";		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totaltm = 0;
			foreach($dados as $d) {
				$diastm[]           = $d['dia'];
				$dadostm[$d['dia']] = $d['num'];
				$totaltm 			+= $d['num'];
			}
			
			$dadostm['NULL'] = ((count($dadostm))?round($totaltm/count($dadostm),2):'0.00');
		}

		if($diastm) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diastm)."') AND sisid='".$sis['sisid']."' AND tmoid='".TM."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".TM."'";
		$db->executar($sql);
		
		if($dadostm) {
			foreach($dadostm as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".TM."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO TEMPO M�DIO DE PROCESSAMENTO POR P�GINA (TM)
		 */
		

		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO N�MERO DE REQUISI��ES (NR)
		 */
		
		unset($dadosnr);
		
		$sql = "SELECT COUNT(oid) as num, to_char(estdata, 'DD') as dia FROM estatistica.estatistica_".sprintf("%04d_%02d", $ano, $mes)." WHERE sisid='".$sis['sisid']."' AND to_char(estdata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."' GROUP BY to_char(estdata, 'DD') ORDER BY dia";		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalnr = 0;
			foreach($dados as $d) {
				$diasnr[]           = $d['dia'];
				$dadosnr[$d['dia']] = $d['num'];
				$totalnr 			+= $d['num'];
			}
			$dadosnr['NULL'] = $totalnr;
		}

		if($diasnr) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasnr)."') AND sisid='".$sis['sisid']."' AND tmoid='".NR."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".NR."'";
		$db->executar($sql);
		
		if($dadosnr) {
			foreach($dadosnr as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".NR."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO N�MERO DE REQUISI��ES (NR)
		 */
		
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO RELA��O ERROS POR REQUISI��ES (PE)
		 */
		
		unset($dadospe);
		
		if($dadosne) {
			foreach($dadosne as $da => $d) {
				if($dadosnr[$da]) { 
					if(is_numeric($da)) {
						$diaspe[]     = $da;
					}
					$dadospe[$da] = round(($d/$dadosnr[$da])*100,4);
				}
			}
		}

		if($diaspe) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diaspe)."') AND sisid='".$sis['sisid']."' AND tmoid='".PE."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".PE."'";
		$db->executar($sql);
		
		if($dadospe) {
			foreach($dadospe as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".PE."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".$valor."');";
				$db->executar($sql);
			}
		}

		/*
		 * FIM CARGA NO TIPO MONITORAMENTO RELA��O ERROS POR REQUISI��ES (PE)
		 */
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO MEMORIA UTILIZADA (MU)
		 */
		
		unset($dadosmu);
		
		$sql = "SELECT ROUND(CAST(AVG(estmemusa)as numeric),2) as num, to_char(estdata, 'DD') as dia FROM estatistica.estatistica_".sprintf("%04d_%02d", $ano, $mes)." WHERE sisid='".$sis['sisid']."' AND to_char(estdata, 'YYYY-MM')='".sprintf("%04d-%02d", $ano, $mes)."' GROUP BY to_char(estdata, 'DD') ORDER BY dia";		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalmu = 0;
			foreach($dados as $d) {
				$diasmu[]           = $d['dia'];
				$dadosmu[$d['dia']] = $d['num'];
				$totalmu			+= $d['num'];
			}
			
			$dadosmu['NULL'] = ((count($dadosmu))?round($totalmu/count($dadosmu),2):'0.00');
		}
		
		if($diasmu) {
			$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IN('".implode("','",$diasmu)."') AND sisid='".$sis['sisid']."' AND tmoid='".MU."'";
			$db->executar($sql);
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND mondia IS NULL AND sisid='".$sis['sisid']."' AND tmoid='".MU."'";
		$db->executar($sql);
		
		if($dadosmu) {
			foreach($dadosmu as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".MU."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO MEMORIA UTILIZADA (MU)
		 */
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UA)
		 */
		
		unset($dadosua);
		
		$sql = "SELECT COUNT(DISTINCT usucpf) as num FROM seguranca.usuario_sistema WHERE sisid='".$sis['sisid']."' AND suscod='A'";		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalua = 0;
			foreach($dados as $d) {
				$dadosua[date("d")] = $d['num'];
				$totalua 			+= $d['num'];
			}
			$dadosua['NULL'] = $totalua;
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND sisid='".$sis['sisid']."' AND (mondia IS NULL OR mondia=".date("d").") AND tmoid='".UA."'";
		$db->executar($sql);
		if($dadosua) {
			foreach($dadosua as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".UA."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UA)
		 */		
		
		/*
		 * INICIO CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UP)
		 */
		
		unset($dadosup);
		
		$sql = "SELECT COUNT(DISTINCT usucpf) as num FROM seguranca.usuario_sistema WHERE sisid='".$sis['sisid']."' AND suscod='P'";		
		$dados = $db->carregar($sql);
		if($dados[0]) {
			$totalup = 0;
			foreach($dados as $d) {
				$dadosup[date("d")] = $d['num'];
				$totalup            += $d['num'];
			}
			
			$dadosup['NULL'] = $totalup;
		}
		
		$sql = "DELETE FROM seguranca.monitoramento WHERE monano='".$ano."' AND monmes='".$mes."' AND sisid='".$sis['sisid']."' AND (mondia IS NULL OR mondia=".date("d").") AND tmoid='".UP."'";
		$db->executar($sql);
		if($dadosup) {
			foreach($dadosup as $dia => $valor) {
				$sql = "INSERT INTO seguranca.monitoramento(
		            	tmoid, sisid, monano, monmes, mondia, mondatacarga, monvalor)
		    			VALUES ('".UP."', '".$sis['sisid']."', '".$ano."', '".$mes."', ".$dia.", NOW(), '".(($valor)?$valor:"0")."');";
				$db->executar($sql);
			}
		}
		
		/*
		 * FIM CARGA NO TIPO MONITORAMENTO USUARIOS ATIVOS (UP)
		 */		
		
		$db->commit();
		
		$HTML .= $sis['sisdsc']." foi carregado... ".number_format( ( getmicrotime() - $Tinicio ), 4, ',', '.' )."s<br>";
		$_TOTTEMPO[] = getmicrotime() - $Tinicio;
		
	}
	if($_TOTTEMPO) {
		foreach($_TOTTEMPO as $t) {
			$tf += $t; 
		}
		$HTML .= "Tempo total de carga... ".$tf."s<br>";
	}
}

$sql = "UPDATE seguranca.agendamentoscripts SET agstempoexecucao='".round((getmicrotime() - $microtime),2)."' WHERE agsfile='_cargamonitoramento.php'";
$db->executar($sql);
$db->commit();

$db->close();

require_once APPRAIZ . 'includes/phpmailer/class.phpmailer.php';
require_once APPRAIZ . 'includes/phpmailer/class.smtp.php';
$mensagem = new PHPMailer();
$mensagem->persistencia = $db;
$mensagem->Host         = "localhost";
$mensagem->Mailer       = "smtp";
$mensagem->FromName		= "SISTEMA DE CARGA NO MONITORAMENTO";
$mensagem->From 		= "simec@mec.gov.br";
$mensagem->AddAddress( "alexandre.dourado@mec.gov.br", "Alexandre Dourado" );
$mensagem->Subject = "Carga no monitoramento";
$mensagem->Body = $HTML;
$mensagem->IsHTML( true );
$mensagem->Send();

?>