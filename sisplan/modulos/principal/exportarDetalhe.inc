<?php
$where = array();

// Verificando responsabilidades
$unidadeExecutora	= new UnidadeExecutora();
$responsabilidades	= $unidadeExecutora->getResponsabilidadeUnidadesByUsuario();

if( !$db->testa_superuser() ){
	if(is_array( $responsabilidades ) && !empty( $responsabilidades )){
		$cond 		= " uexid IN ('".implode("','", $responsabilidades)."') ";
		$condLista	= " uex.uexid IN ('".implode("','", $responsabilidades)."') ";
	}else{
		$cond = $condLista = " FALSE ";
	}
}else{
	$cond = $condLista = " TRUE ";
}

$where[] = $condLista;

header( 'Content-Type: application/vnd.ms-excel' );
header( 'Expires: 0' );
header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
header( 'Content-Disposition: attachment; filename="iphan_planejamentos_' . date("YmdHi") . '.xls"' );

					$arCabecalho = array("ID Planejamento",
                                         "Unidade Executora",
										 "Tipo de Projeto",
                                         "Situa��o",
										 "Prioridade",
										 "N�mero do PI",
										 "N�mero da nota de cr�dito",
                                         "T�tulo",
										 "Objetivos",
										 "Justificativa",
										 "Descri��o das Atividades",
										 "Vincula��o ao Planejamento Estrat�gico",
										 "Outras Iniciativas",
                                         "Tipo de Instrumento",
                                         "Segmento Cultural",
                                         "Meta PNC",
                                         "Data de �nicio",
                                         "Data de T�rmino",
                                         "Contribui para alguma Meta PPA?",
                                         "Emenda parlamentar",
                                         "Tipo de autor",
                                         "Partido",
                                         "Autor",
										 "A��es presentes nos APPCs",
										 "Localiza��o da a��o",
										 "Pa�s",
										 "Unidade Federativa",
										 "Munic�pio",
										 "Abrang�ncia",
										 "Programa",
										 "A��o",
										 "Unidade Or�ament�ria",
										 "Localizador",
										 "Plano Or�ament�rio",
										 "Produto Secund�rio - Produto",
										 "Produto Secund�rio - Unidade Medida",
										 "Produto Secund�rio - Meta",
										 "Valor Previsto (anual) - Custeio",
										 "Valor Previsto (anual) - Capital",
										 "Valor Previsto (anual) - Total",
                                         "Naturezas",
                                         "Contrata��es",
										 "Cronograma F�sico Janeiro",
										 "Cronograma F�sico Fevereiro",
										 "Cronograma F�sico Mar�o",
										 "Cronograma F�sico Abril",
										 "Cronograma F�sico Maio",
										 "Cronograma F�sico Junho",
										 "Cronograma F�sico Julho",
										 "Cronograma F�sico Agosto",
										 "Cronograma F�sico Setembro",
										 "Cronograma F�sico Outubro",
										 "Cronograma F�sico Novembro",
										 "Cronograma F�sico Dezembro",
										 "Cronograma Financeiro Capital Janeiro",
										 "Cronograma Financeiro Capital Fevereiro",
										 "Cronograma Financeiro Capital Mar�o",
										 "Cronograma Financeiro Capital Abril",
										 "Cronograma Financeiro Capital Maio",
										 "Cronograma Financeiro Capital Junho",
										 "Cronograma Financeiro Capital Julho",
										 "Cronograma Financeiro Capital Agosto",
										 "Cronograma Financeiro Capital Setembro",
										 "Cronograma Financeiro Capital Outubro",
										 "Cronograma Financeiro Capital Novembro",
										 "Cronograma Financeiro Capital Dezembro",
										 "Cronograma Financeiro Custeio Janeiro",
										 "Cronograma Financeiro Custeio Fevereiro",
										 "Cronograma Financeiro Custeio Mar�o",
										 "Cronograma Financeiro Custeio Abril",
										 "Cronograma Financeiro Custeio Maio",
										 "Cronograma Financeiro Custeio Junho",
										 "Cronograma Financeiro Custeio Julho",
										 "Cronograma Financeiro Custeio Agosto",
										 "Cronograma Financeiro Custeio Setembro",
										 "Cronograma Financeiro Custeio Outubro",
										 "Cronograma Financeiro Custeio Novembro",
										 "Cronograma Financeiro Custeio Dezembro",
										 "Cronograma Or�ament�rio Capital Janeiro",
										 "Cronograma Or�ament�rio Capital Fevereiro",
										 "Cronograma Or�ament�rio Capital Mar�o",
										 "Cronograma Or�ament�rio Capital Abril",
										 "Cronograma Or�ament�rio Capital Maio",
										 "Cronograma Or�ament�rio Capital Junho",
										 "Cronograma Or�ament�rio Capital Julho",
										 "Cronograma Or�ament�rio Capital Agosto",
										 "Cronograma Or�ament�rio Capital Setembro",
										 "Cronograma Or�ament�rio Capital Outubro",
										 "Cronograma Or�ament�rio Capital Novembro",
										 "Cronograma Or�ament�rio Capital Dezembro",
										 "Cronograma Or�ament�rio Custeio Janeiro",
										 "Cronograma Or�ament�rio Custeio Fevereiro",
										 "Cronograma Or�ament�rio Custeio Mar�o",
										 "Cronograma Or�ament�rio Custeio Abril",
										 "Cronograma Or�ament�rio Custeio Maio",
										 "Cronograma Or�ament�rio Custeio Junho",
										 "Cronograma Or�ament�rio Custeio Julho",
										 "Cronograma Or�ament�rio Custeio Agosto",
										 "Cronograma Or�ament�rio Custeio Setembro",
										 "Cronograma Or�ament�rio Custeio Outubro",
										 "Cronograma Or�ament�rio Custeio Novembro",
										 "Cronograma Or�ament�rio Custeio Dezembro"
					);


					$detalhePlanejamento = new DetalhePlanejamento();
					$rs = $detalhePlanejamento->listaByWhereXls($where, false);

                    $arConfig = array("style" 			=> "width:100%;",
									  "totalLinha" 		=> true,
									  "totalRegistro" 	=> true);

					//Definindo todas as colunas de texto como string
					$arParamCol[0] = array("type" => Lista::TYPESTRING);
					$arParamCol[1] = array("type" => Lista::TYPESTRING);
					$arParamCol[2] = array("type" => Lista::TYPESTRING);
					$arParamCol[3] = array("type" => Lista::TYPESTRING);
					$arParamCol[4] = array("type" => Lista::TYPESTRING);
					$arParamCol[5] = array("type" => Lista::TYPESTRING);
					$arParamCol[6] = array("type" => Lista::TYPESTRING);
					$arParamCol[7] = array("type" => Lista::TYPESTRING);
					$arParamCol[8] = array("type" => Lista::TYPESTRING);
					$arParamCol[9] = array("type" => Lista::TYPESTRING);

					$oLista = new Lista($arConfig);
					$oLista->setCabecalho( $arCabecalho );
					$oLista->setCorpo( $rs, $arParamCol );
					$oLista->show();
    die();