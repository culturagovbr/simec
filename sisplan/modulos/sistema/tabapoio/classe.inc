<?php

switch( $_REQUEST['evento'] ){

	case 'cadastrar_fonte':
 
		if( $_REQUEST['alterar'] != 'S' )
		{
			$sql = sprintf("INSERT INTO sisplan.classe 
								(clsdsc, clsstatus) 
							VALUES 
								('%s', '%s')",
							$_REQUEST['clsdsc'],
							$_REQUEST['clsstatus']);
		}
		else 
		{
			$sql = sprintf("UPDATE sisplan.classe SET
								clsdsc = '%s',
								clsstatus = '%s'
							WHERE
								clsid = %s",
							$_REQUEST['clsdsc'],
							$_REQUEST['clsstatus'],
							$_REQUEST['clsid']);
		}
		
		if ( !$db->executar( $sql ) ) {
			$db->rollback();
		} else {
			$db->commit();
		}

		$db->sucesso( $_REQUEST['modulo'], $parametros );

		break;
		
	case 'excluir_fonte':
		
		$sql = sprintf("DELETE FROM 
							sisplan.classe 
						WHERE 
							clsid = %s", 
						$_REQUEST['clsid']);
		
		if ( !$db->executar( $sql ) ) {
			$db->rollback();
		} else {
			$db->commit();
		}

		$db->sucesso( $_REQUEST['modulo'], $parametros );
		
		break;
		
	case 'editar_fonte':
		$sql = "SELECT 
					clsid, 
					clsdsc,
					clsstatus
				FROM 
					sisplan.classe 
				WHERE
					clsid = '{$_REQUEST['clsid']}'";
		
		$dados = $db->carregar($sql);
				
		$clsid     = $dados[0]['clsid'];
		$clsdsc    = $dados[0]['clsdsc'];
		$clsstatus = $dados[0]['clsstatus'];
		$alterar   = 'S';
		break;
}

// CABE�ALHO
include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';
monta_titulo( 'Classe', 'Tabelas de Apoio - PPA' );
?>


<script language="javascript" type="text/javascript">
	
	function cadastrar_controle()
	{
		if ( validar_formulario_controle() ) 
		{
			document.controle.evento.value = 'cadastrar_fonte';
			document.controle.submit();
		}
	}

	function validar_formulario_controle()
	{
		var validacao = true;
		var mensagem = 'Os seguintes campos n�o foram preenchidos:';
		
		if ( document.controle.clsdsc.value == '' ) 
		{
			mensagem += '\nDescri��o';
			validacao = false;
		}
		
		if ( document.controle.clsstatus.value == '' ) 
		{
			mensagem += '\nSitua��o';
			validacao = false;
		}
  
		if ( !validacao ) 
			alert( mensagem );

		return validacao;
	}
	
	function pesquisar_controle () 
	{
		document.controle.evento.value = 'pesquisar_fonte';
		document.controle.submit();
	}
	
	
	function limpar_controle () 
	{
		location.href = '<?=$_SESSION['sisarquivo'] . ".php?modulo=".$_REQUEST['modulo']."&acao=A"?>';
	}
	
	
	function excluir_controle( clsid )
	{
		if ( confirm( 'Deseja excluir o Desafio Estrat�gico?' ) ) 
		{
			document.controle.clsid.value = clsid;
			document.controle.evento.value = 'excluir_fonte';
			document.controle.submit();
		}
	}
	
	function editar_controle ( clsid )
	{
		document.controle.clsid.value = clsid;
		document.controle.evento.value = 'editar_fonte';
		document.controle.submit();
	}

	
	function ltrim( value ){
		var re = /\s*((\S+\s*)*)/;
		return value.replace(re, "$1");
	}
	
	function rtrim( value ){
		var re = /((\s*\S+)*)\s*/;
		return value.replace(re, "$1");
	}
	
	function trim( value ){
		return ltrim(rtrim(value));
	}

</script>
<table class="tabela" bgcolor="#fbfbfb" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
			<!-- NOVO CONTROLE -->
				<form method="post" name="controle">
					<input type="hidden" name="evento" value=""/>
					<input type="hidden" name="clsid" value="<?=$clsid?>"/>
					<input type="hidden" name="alterar" value="<?= $alterar == "" ? "" : "S"; ?>"/>
					<table class="tabela" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="width: 100%;">
						<tr>
							<td align='right' class="SubTituloDireita" style="vertical-align:top; width:25%;">Descri��o:</td>
							<td>
								<?= campo_texto( 'clsdsc', 'S', 'S', '', 40, 400, '', '' ); ?>
							</td>
						</tr>
						<tr>
							<td align='right' class="SubTituloDireita" style="vertical-align:top; width:25%;">Situa��o:</td>
							<td>
								<input type="radio" id="A" name="clsstatus" value="A" <?= $clsstatus == 'A' || $clsstatus == '' ? "checked='checked'" : ""; ?>  /> Ativo 
								<input type="radio" id="I" name="clsstatus" value="I" <?= $clsstatus == 'I' ? "checked='checked'" : ""; ?>/> Inativo
													
							</td>
						</tr>
						<tr style="background-color: #cccccc">
							<td align='right' style="vertical-align:top; width:25%">&nbsp;</td>
							<td>
							<input type="button" name="botao" value="Pesquisar" onclick="pesquisar_controle();"/>
							<input type="button" name="botao" value="Limpar" onclick="limpar_controle();"/>
							<input type="button" name="botao" value="Gravar" onclick="cadastrar_controle();"/>
							</td>
						</tr>
					</table>
				</form>
		</td>
	</tr>
</table>
<!-- LISTA -->
<?php

unset( $sql );

$cabecalho = array( 'A��es', 'Descri��o', 'Situa��o' );

$img = "<img border=\"0\" src=\"../imagens/alterar.gif\" title=\" Alterar Programa\" onclick=\"editar_controle(' || '\'' || cls.clsid || '\'' || ')\" >";
$img_1 = "<img border=\"0\" src=\"../imagens/excluir.gif\" title=\" Excluir Programa\" onclick=\"excluir_controle(' || '\'' || cls.clsid || '\'' || ')\" >";

if ( $_REQUEST['evento'] == 'pesquisar_fonte' )
{
	$where = "";
		
	if ( !empty($_REQUEST['clsdsc']) )
		$where .= "AND cls.clsdsc ILIKE '%{$_REQUEST['clsdsc']}%'";
		
	if ( !empty($_REQUEST['clsstatus']) )
		$where .= "AND cls.clsstatus = '{$_REQUEST['clsstatus']}'";
}
					 
$sql = "SELECT 
			'{$img}&nbsp;{$img_1}' AS acao, 	
			cls.clsdsc,
			CASE  
				WHEN cls.clsstatus = 'I' THEN 'Inativo'
				ELSE 'Ativo'
			END as clsstatus
		FROM 
			sisplan.classe cls
		WHERE 
			1=1
		{$where}";

// exibe o resultado da consulta
$db->monta_lista( $sql, $cabecalho, 100, 20, '', '', '' );
?>