<?

//Chamada de programa
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$academico = new academico();
?>
<script>
    function acessaPagina(url){
        window.location = url;
    }
</script>
<style>
    .acima:hover{
        background-color: #ffffcc;
    }
</style>
<?php
function retornaAbas()
{
    global $db;
    $sql = "
        SELECT
            menu.mnuid,
            menu.mnudsc,
            menu.mnulink,
            menu.mnutransacao
        FROM seguranca.menu, seguranca.aba_menu
        WHERE menu.mnuid = aba_menu.mnuid
            AND aba_menu.abacod = 57135
            AND menu.sisid = {$_SESSION['sisid']}
            AND menu.mnuid IN (
                SELECT DISTINCT m2.mnuid FROM seguranca.perfilmenu m2, seguranca.perfilusuario p
                WHERE m2.pflcod = p.pflcod AND p.usucpf = '{$_SESSION['usucpf']}' AND mnuid <> 13355
               )
        ORDER BY menu.mnudsc";
    $resultSet = $db->carregar($sql);
    return $resultSet;
}

if ($_REQUEST['carga']) {
	$_SESSION['academico']['orgid'] = $_SESSION['academico']['orgid'] ? $_SESSION['academico']['orgid'] : $_REQUEST["orgid"];
    $abas = retornaAbas();
    $saida = "";
    if(is_array($abas)){
        foreach($abas as $key => $aba){
            $bg = 'white';
            if($key % 2 ==  1){
                $bg = '#f9f9f9';
            }
            $transacao = str_replace(' ' , '' , $aba['mnutransacao']);
            $saida .= <<<HTML
                <tr bgcolor onmouseover="this.bgColor='#cce0ff';" onmouseout="this.bgColor='$bg';" style="cursor:pointer;" onclick="acessaPagina('{$aba['mnulink']}&entidunidade={$_REQUEST["carga"]}');" style="">
                    <td valign="top" style="font-size:12px; border:1px solid #ccc; padding: 2px 0; width: 30px;" class="text-center">
                        <span style="color:#6495ED;" class="glyphicon glyphicon-hand-right"></span>
                    </td>
                    <td valign="top" class="title" style="font-size:12px; padding: 2px 0 5px 5px; border:1px solid #ccc;"> {$aba['mnudsc']} </td>
                </tr>
HTML;
        }
    }
?>
<div class="col-md-8" style="padding:10px; width: 97%; border:1px solid; color:#D3D3D3; box-shadow:2px 0px 3px #696969;">
    <div class="col-md-3" style="position:static;">
        <table style='width: 100%;' border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th colspan='2'>Abas</th>
            </tr>                    
            <? echo $saida == '' ? '' : $saida ?>
        </table>
    </div>
    <div class='col-md-8' style='position:static;'>
        <table style='width: 100%;'>
            <tr>
                <th>Lista de Campus</th>
            </tr>
            <tr>
                <td><?$academico->academico_listacampus( $_REQUEST["carga"], $_SESSION['academico']['orgid']);?></td>
            </tr>
        </table>
		<?
		if ( !verificaPerfil( PERFIL_DIRETOR_CAMPUS ) ) {
			?>
			<table style='width: 100%;'>
				<tr>
					<th>Reitoria</th>
				</tr>
				<tr>
					<td><?$academico->academico_listareitoria($_REQUEST["carga"], $_SESSION['academico']['orgid']); ?></td>
				</tr>
			</table>
		<?
		}
		?>
    </div>  
</div>
<?PHP
    die;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$orgid = academico_existeorgao($_REQUEST["orgid"]) ? $_REQUEST["orgid"] : $_SESSION['academico']['orgid']; 

//if ( empty( $_GET['orgid'] ) ){
//	$_REQUEST['orgid'] = $_SESSION['academico']["orgid"];
//}

// limpa todas as sess�es do m�dulo
unset($_SESSION['academico']);

if( $_REQUEST['atualizaCampus'] ){
	$entidNovo = $_REQUEST['entidNovo'];
	$sql = "UPDATE entidade.entidade SET entstatus= 'I' WHERE entid ={$entidNovo}"; 
	$db->executar($sql);
	$db->commit();
}
if( $_REQUEST['atualizaReitorias'] ){
	$entidID = $_REQUEST['entidID'];
	$sql = "UPDATE entidade.entidade SET entstatus= 'I' WHERE entid ={$entidID}"; 
	$db->executar($sql);
	$db->commit();
}

// Monta as abas com os org�o de responsabilidade do usu�rio (se nao for superUser)
$res = academico_pega_orgao_permitido();

if ($res[0]["id"] != ''){
	$orgaoPermitido = false;
	foreach($res as $dadosOrg){
		if ($dadosOrg['id'] == $orgid && $orgid != ''){
			$orgaoPermitido = true;
			continue;
		}
	}
	if ( !$orgaoPermitido ){
		$orgid = $res[0]['id'];
	}
	
//	$org = $_REQUEST['orgid'] ? $_REQUEST['orgid'] : $res[0]['id'];
	$link = '/academico/academico.php?modulo=inicio&acao=C&orgid=' . $orgid;
	$_SESSION['academico']['orgid'] = $orgid;
	
	echo montarAbasArray($res, $link );
	monta_titulo( 'Lista de Institui��es', '' );

$arrPerfil = pegaPerfilGeral();

if( 1==2 && (in_array(PERFIL_SUPERUSUARIO,$arrPerfil) || in_array(PERFIL_ADMINISTRADOR,$arrPerfil) || in_array(PERFIL_IFESCADASTRO,$arrPerfil) || in_array(PERFIL_MECCADASTRO,$arrPerfil) || in_array(PERFIL_CADASTROGERAL,$arrPerfil)) ) {
	/*
	$texto = "<div style=\"font-size:12px\" >Senhores Usu�rios,<br /><br />
	Informamos que foi implementada no Sistema Integrado de Monitoramento Execu��o e Controle - SIMEC, no M�dulo Rede Federal, a funcionalidade <b>\"Solicita��o Viagem Exterior\"</b> destinada a agilizar o processo de Autoriza��o Coletiva para concess�o de di�rias e passagens ao exterior pelo Ministro de Estado da Educa��o, desde que referidos aos programas de treinamento, capacita��o, qualifica��o, interc�mbio acad�mico, coopera��o internacional, p�s-gradua��o e inova��o, mediante a aprova��o dos conselhos superiores dos respectivos �rg�o/entidades e a especifica��o do n�mero de participantes, conforme definido no artigo 4�, � 1�, da Portaria MEC n�. 446, de 20 de abril de 2011, dever�o seguir os seguintes passos: <a target=\"_blank\" href=\"manualsolicitacaoviagem.pdf\" >clique aqui</a>.</div>";
	*/
	
	/*
	$texto = "<div style=\"font-size:12px\" >Senhores Usu�rios,<br /><br />
	Informamos que est� dispon�vel no Sistema Integrado de Monitoramento Execu��o e Controle - SIMEC, no M�dulo Rede Federal, a funcionalidade <b>�Solicita��o Viagem Exterior�</b>, visando o cumprimento do estabelecido pelo Decreto 7.689 de 02/03/2012, Portaria MPOG n� 75 de 08/03/2012, Portaria MEC n� 362 de 10/04/2012 e Portaria MEC n� 441 de 25/04/2012.
	<br><br>
	Para acessar obter maiores informa��es relacionadas aos procedimentos para utiliza��o do sistema: <a target=\"_blank\" href=\"manualsolicitacaoviagem.pdf\" >clique aqui</a>.</div>";
	*/
	
	$texto = "<div style=\"font-size:12px\" >Caros reitores das Institui��es Federais de Ensino Superior e dirigentes de escolas m�dicas ao n�vel federal (diretores ou coordenadores de curso):<br /><br />
	<p>Inicia-se no dia 11 de julho de 2013 a manifesta��o de interesse das IFES em serem part�cipes do Programa Mais M�dicos para o Brasil, anunciado pela presidenta Dilma Roussef, pelo ministro da Educa��o Aloizio Mercadante e pelo ministro da Sa�de Alexandre Padilha no dia 08 de julho de 2013.</p> 
	<p>Este programa tem como finalidade de garantir o provimento de m�dicos para �reas de dif�cil fixa��o e profissionais, assim como garantir segundo ciclo de forma��o m�dica para se avan�ar na constru��o do perfil de egresso estabelecido pelas Diretrizes Curriculares Nacionais (DCN�s) dos cursos de gradua��o em Medicina, na perspectiva de forma��o de um m�dico generalista, cr�tico, reflexivo e humanista.</p>
	<p>Dentro deste conjunto de medidas, h� a��es interministeriais entre Minist�rio da Educa��o e Minist�rio da Sa�de que estabelecem pol�ticas voltadas para expans�o de vagas para resid�ncia m�dica e investimentos na rede de servi�os de sa�de. </p>
	<p>O apoio das IFES ser� fundamental para avan�ar na constru��o das mudan�as na forma��o implicadas com a defini��o do segundo ciclo de forma��o, bem como apoiar, na forma de tutoria e preceptoria, as atividades de supervis�o direta estabelecidas com os profissionais dos servi�os de sa�de.</p> 
	<p>Para manifesta��o e interesse pela institui��o, � necess�rio a assinatura de termo de pr� ades�o pelo reitor da institui��o,  conforme previsto na Portaria Normativa n� 14, de 9 de julho de 2013, do GM/MEC. Este dever� ser inserido no SIMEC, em aba espec�fica inclu�da dentro do m�dulo rede federal. O termo dever� ser digitalizado e inserido no sistema, bem como enviado c�pia impressa, via SEDEX com Aviso de Recebimento (AR), para o endere�o da DIFES � Diretoria de Desenvolvimento da Rede de Institui��es federais de Ensino Superior, Minist�rio da Educa��o � MEC, Edif�cio Sede, Bloco �L�, 3� Andar, Sala 303, Esplanada dos Minist�rios, Bras�lia-DF. CEP: 70047-900.</p>
	<p>Lembramos que o prazo para pr�-ades�o ser� at� �s 23:59 hs do dia 25 de julho de 2013.</p>
	<p>Ap�s este momento, haver� o prazo para que as IFES, caso necess�rio, realizem consultas a seus conselhos universit�rios para ratifica��o da ades�o definitiva. Esta tem previs�o de ocorrer, mediante assinatura de novo termo, entre os dias 10 a 13 de agosto de 2013.</p>
	<p>Quaisquer d�vidas no telefone (61) 2022-8158, ou pelo endere�o eletr�nico: rosa.nader@mec.gov.br</p>
	<p>Contamos com o apoio de todos para avan�armos na constru��o do direito � sa�de no Brasil e na garantia das mudan�as necess�rias no �mbito da forma��o m�dica.</p>
	</div>";

	popupAlertaGeral($texto,'900px',"470px");
}

if(in_array(PERFIL_REITOR,$arrPerfil) && $orgid==1 && 1==2){
	$texto = "<div style=\"font-size:12px\" >Caros reitores das Institui��es Federais de Ensino Superior e dirigentes de escolas m�dicas ao n�vel federal (diretores ou coordenadores de curso):<br /><br />
	<p>Inicia-se no dia 11 de julho de 2013 a manifesta��o de interesse das IFES em serem part�cipes do Programa Mais M�dicos para o Brasil, anunciado pela presidenta Dilma Roussef, pelo ministro da Educa��o Aloizio Mercadante e pelo ministro da Sa�de Alexandre Padilha no dia 08 de julho de 2013.</p> 
	<p>Este programa tem como finalidade de garantir o provimento de m�dicos para �reas de dif�cil fixa��o e profissionais, assim como garantir segundo ciclo de forma��o m�dica para se avan�ar na constru��o do perfil de egresso estabelecido pelas Diretrizes Curriculares Nacionais (DCN�s) dos cursos de gradua��o em Medicina, na perspectiva de forma��o de um m�dico generalista, cr�tico, reflexivo e humanista.</p>
	<p>Dentro deste conjunto de medidas, h� a��es interministeriais entre Minist�rio da Educa��o e Minist�rio da Sa�de que estabelecem pol�ticas voltadas para expans�o de vagas para resid�ncia m�dica e investimentos na rede de servi�os de sa�de. </p>
	<p>O apoio das IFES ser� fundamental para avan�ar na constru��o das mudan�as na forma��o implicadas com a defini��o do segundo ciclo de forma��o, bem como apoiar, na forma de tutoria e preceptoria, as atividades de supervis�o direta estabelecidas com os profissionais dos servi�os de sa�de.</p> 
	<p>Para manifesta��o e interesse pela institui��o, � necess�rio a assinatura de termo de pr� ades�o pelo reitor da institui��o,  conforme previsto na Portaria Normativa n� 14, de 9 de julho de 2013, do GM/MEC. Este dever� ser inserido no SIMEC, em aba espec�fica inclu�da dentro do m�dulo rede federal. O termo dever� ser digitalizado e inserido no sistema, bem como enviado c�pia impressa, via SEDEX com Aviso de Recebimento (AR), para o endere�o da DIFES � Diretoria de Desenvolvimento da Rede de Institui��es federais de Ensino Superior, Minist�rio da Educa��o � MEC, Edif�cio Sede, Bloco �L�, 3� Andar, Sala 303, Esplanada dos Minist�rios, Bras�lia-DF. CEP: 70047-900.</p>
	<p>Lembramos que o prazo para pr�-ades�o ser� at� �s 23:59 hs do dia 25 de julho de 2013.</p>
	<p>Ap�s este momento, haver� o prazo para que as IFES, caso necess�rio, realizem consultas a seus conselhos universit�rios para ratifica��o da ades�o definitiva. Esta tem previs�o de ocorrer, mediante assinatura de novo termo, entre os dias 10 a 13 de agosto de 2013.</p>
	<p>Quaisquer d�vidas no telefone (61) 2022-8158, ou pelo endere�o eletr�nico: rosa.nader@mec.gov.br</p>
	<p>Contamos com o apoio de todos para avan�armos na constru��o do direito � sa�de no Brasil e na garantia das mudan�as necess�rias no �mbito da forma��o m�dica.</p>
	</div>";

	popupAlertaGeral($texto,'900px',"470px");
}

if( in_array(PERFIL_ASSESSORIA_ALTA_GESTAO,$arrPerfil) || in_array(PERFIL_ALTA_GESTAO,$arrPerfil) || in_array(PERFIL_SUPERUSUARIO,$arrPerfil) || in_array(PERFIL_CADASTROGERAL,$arrPerfil) || in_array(PERFIL_IFESCADBOLSAS,$arrPerfil) || in_array(PERFIL_IFESCADCURSOS,$arrPerfil)  || in_array(PERFIL_IFESCADASTRO,$arrPerfil) ) {
	
		switch ( $orgid ){
			case '1':
				$funid = " in ('" . ACA_ID_UNIVERSIDADE . "')";
			break;
			case '2':
				$funid = " in ('" . ACA_ID_ESCOLAS_TECNICAS . "')";
			break;
			case '3':
				$funid = " in ('" . ACA_ID_UNIDADES_VINCULADAS . "')";
			break;
		}
	
	if ( in_array(PERFIL_ASSESSORIA_ALTA_GESTAO,$arrPerfil) || in_array(PERFIL_ALTA_GESTAO,$arrPerfil) || $db->testa_superuser() || academico_possui_perfil(PERFIL_ADMINISTRADOR) || academico_possui_perfil_sem_vinculo() || academico_possui_perfil(PERFIL_MECCADASTRO) || academico_possui_perfil_resp_tipo_ensino()){

			$sql = "SELECT
						e.entid as codigo,
						entsig || ' - ' || UPPER(entnome) as descricao
					FROM
						entidade.entidade e 
					INNER JOIN
						entidade.funcaoentidade ef ON ef.entid = e.entid
					LEFT JOIN 
						entidade.endereco ed ON e.entid = ed.entid
					WHERE
						e.entstatus = 'A' AND ef.funid ". $funid . "
					GROUP BY e.entid, e.entnome , e.entsig
					ORDER BY
						 e.entsig, e.entnome";
			
		}else{
			
			$sql = "SELECT
						e.entid as codigo,
						entsig || ' - ' || UPPER(entnome) as descricao
					FROM
						entidade.entidade e 
					INNER JOIN
						entidade.funcaoentidade ef ON ef.entid = e.entid
					LEFT JOIN 
						entidade.endereco ed ON e.entid = ed.entid
					INNER JOIN
						academico.usuarioresponsabilidade ur ON ur.entid = e.entid
																AND ur.rpustatus = 'A'
					WHERE
						e.entstatus = 'A' AND ef.funid ". $funid . " AND ur.usucpf = '{$_SESSION['usucpf']}'
					GROUP BY e.entid, e.entnome,  e.entsig
					ORDER BY
						 e.entsig, e.entnome";
			
		}
		
		$arrDados = $db->carregar($sql);
		
		/*
		if($arrDados){
			if(count($arrDados) > 1){
				$combo = $db->monta_combo("entid",$arrDados,"S","Selecione a Institui��o...","exibeSolicitacaoDecreto","","","","","",true);
			}else{
				foreach($arrDados as $dado){
					$solicitacao = $dado['codigo'];
				}
			}
				
			$texto = "<center>
					<img src=\"../imagens/alerta_sistema.gif\" />
				    <p><font size=3 color=red><b>Aten��o!</b></font></p>
	           	     Solicita��o de excepcionalidade decreto 7446 de 1� de Mar�o de 2011.<br />
	           	     Prazo para envio das solicita��es: 13/04/2011.<br />
	           	     ".($combo ? "Selecione: ".$combo."<br />" : "<a href=\"academico.php?modulo=principal/solicitacaodecreto&acao=A&entid=$solicitacao\" >Clique aqui</a>");
		
			popupAlertaGeral($texto,"580px","260px");
		}
		*/
}
	
?>
	<form name="formulario" id="pesquisar" method="POST" action="">
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding=3 align="center">
			<tr>
				<td  bgcolor="#CCCCCC" colspan="2"><b>Argumentos da Pesquisa</b></td>
			</tr>
			<tr>
				<td class="subtitulodireita">Institui��o</td>
				<td>
					<?php
					
						switch ( $orgid ){
							case '1':
								$funid = " in ('" . ACA_ID_UNIVERSIDADE . "')";
							break;
							case '2':
								$funid = " in ('" . ACA_ID_ESCOLAS_TECNICAS . "')";
							break;
							case '3':
								$funid = " in ('" . ACA_ID_UNIDADES_VINCULADAS . "')";
							break;
						}
							
						$sql_unidade = "SELECT
											e.entid as codigo,
											e.entnome as descricao
										FROM
											entidade.entidade e
										INNER JOIN
											entidade.funcaoentidade ef ON ef.entid = e.entid
										WHERE
											e.entstatus = 'A' AND ef.funid {$funid}
										ORDER BY
											e.entnome";
						$entid = $_REQUEST['entid'] ? $_REQUEST['entid'] : ''; 
						
						$db->monta_combo("entid", $sql_unidade, "S", "Todas as Institui��es", '', '', '', '', 'N','entid');
					?>
				</td>
			</tr>
			<tr>
				<td class="SubTituloDireita">UF</td>
				<td>
					<?php
						
						$sql_uf = "SELECT
									estuf as codigo,
									estdescricao as descricao
								FROM
									territorios.estado";
						
						$estuf = $_REQUEST['estuf'] ? $_REQUEST['estuf'] : '';
						
						$db->monta_combo("estuf", $sql_uf, "S", "Todas", '', '', '', '150', 'N','estuf');
						
					?>
				</td>
			</tr>
			<tr>
				<td bgcolor="#CCCCCC"></td>
				<td bgcolor="#CCCCCC">
					<input style="cursor:pointer;" type="submit" value="Pesquisar" />
					<input style="cursor:pointer;" type="button" value="Ver Todos" onCLick="academico_verTodasUnidades();" />
				</td>
			</tr>
		</table>
	</form>
<?php

	// lista as unidades
	$academico->academico_listaunidades( $orgid );

}else{ ?>
	
	<?php monta_titulo( 'Lista de Institui��es', '' ); ?>
	<table align="center" border="0" cellpadding="5" cellspacing="1" class="tabela" cellpadding="0" cellspacing="0">
		<tr style="text-align: center; color: #ff0000;">
			<td>
				Usu�rio sem permiss�es para visualizar unidades. <br/>
				Favor entrar em contato com o gestor do m�dulo para verificar as responsabilidades de seu perfil.
			</td>
		</tr>
	</table>

<?php } ?>

<script type="text/javascript" src="/includes/prototype.js"></script>
<script>
	function academico_abreSistema( entid ){
		window.location.href = '/academico/academico.php?modulo=principal/dadosentidade&acao=A&entidunidade=' + entid;
	}
	
	function academico_verTodasUnidades(){
		var form = window.document.getElementById("pesquisar");
		
		for( var i=0; i < form.elements.length; i++ ){
			var campo = form.elements[i];
			
			switch(campo.type){
				case "select-one":
					campo.options[0].selected = true;
			}
		}
		
		form.submit();
		
	}

	var params;
	function formatarParametros(){
    	params = Form.serialize($('pesquisar'));
	}

	function desabilitarConteudo( id ){
		var url = 'academico.php?modulo=inicio&acao=A&carga='+id;
		if ( document.getElementById('img'+id).name == '-' ) {
			url = url + '&subAcao=retirarCarga';
			var myAjax = new Ajax.Request(
				url,
				{
					method: 'post',
					asynchronous: false
				});
		}
	}
	function abredadoscampus( entid ){
		return window.location = '?modulo=principal/alterarCampus&acao=A&entid=' + entid;
	}
	function abredadosreitoria( entid ){
		return window.location = '?modulo=principal/alterarReitoria&acao=A&entid=' + entid;
	}

	
	function inativarcampus( entidade ){
		if(confirm("Deseja realmente excluir o Campus"))
		  {
			var myAjax = new Ajax.Request('academico.php?modulo=inicio&acao=C', {
		        method:     'post',
		        parameters:  '&atualizaCampus=true&entidNovo=' + entidade,
		        onComplete: function (res){	
	       			alert("Campus excluido com sucesso!");
	       			window.location.href = window.location.href;
		        }
		  	}); 
		  }
	}
	function inativarreitoria( entidID ){
		if(confirm("Deseja realmente excluir a Reitoria"))
		  {
			var myAjax = new Ajax.Request('academico.php?modulo=inicio&acao=C', {
		        method:     'post',
		        parameters:  '&atualizaReitorias=true&entidID=' + entidID,
		        onComplete: function (res){	
	       			alert("Reitoria excluida com sucesso!");
	       			window.location.href = window.location.href;
		        }
		  	}); 
		  }
	}
	
	function exibeSolicitacaoDecreto(entid)
	{
		window.location = 'academico.php?modulo=principal/solicitacaodecreto&acao=A&entid=' + entid;
	}
	
	function aviso(){
		alert("O sistema abre no dia 02 de julho de 2012.");
	}
</script>