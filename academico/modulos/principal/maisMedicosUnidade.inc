<?php
include_once(APPRAIZ."academico/classes/MaisMedicos.class.inc");
include_once(APPRAIZ."academico/classes/HospitalUnidade.class.inc");
include_once(APPRAIZ."academico/classes/MaisMedicosResponsabilidade.class.inc");

if($_GET['hsuid']){
	$hsuid = $_GET['hsuid'];
	$_SESSION['academico']['hsuid'] = $hsuid;
}

if(!$_SESSION['academico']['hsuid'])
{
	$arrPerfil = pegaPerfilGeral();
	if(!in_array(PERFIL_MAIS_MEDICOS_UNIDADE,$arrPerfil)){
		$erro_js = "Voc� n�o possui perfil para visualizar esta tela.";
	}else{
		$sql = "select hsuid from academico.usuarioresponsabilidade where usucpf = '{$_SESSION['usucpf']}' and pflcod = ".PERFIL_MAIS_MEDICOS_UNIDADE." and rpustatus = 'A'";
		$arrHsuid = $db->carregarColuna($sql);
		if(!$arrHsuid){
			$erro_js = "Voc� n�o possui Unidade vinculada.";
		}
		elseif(count($arrHsuid) > 1){
			header("Location: academico.php?modulo=principal/listarMaisMedicosUnidade&acao=A");
		}else{
			$hsuid = $arrHsuid[0];
		}
	}
	
}else{
	$hsuid = $_SESSION['academico']['hsuid'];
}

if($erro_js)
{
	echo "<script>alert('$erro_js');history.back(-1);</script>";
	exit;
}

function cabecalhoUnidadeMaisMedicos($hsuid)
{
	global $db;
	
	$unidade = new HospitalUnidade($hsuid);
	$cabecalho = '<table class="tabela" align="center">'
				   . '	<tr>'
				   . '		<td class="SubTituloEsquerda" style="text-align:right;" >Unidade</td>'
				   . '		<td width="80%" class="SubTituloDireita" style="text-align:left;background:#EEE;"> ' . $unidade->hsudsc . '</td>'
				   . '	</tr>'
				   . '	</table>';
				   
	return $cabecalho;
}

function textoHTML2($hsuid)
{
	?>
<div style="text-align:justify;">
<center>
<h2>PORTARIA NORMATIVA N� 14, DE 9 DE JULHO DE 2013</h2>
<p><b>MINIST�RIO DA EDUCA��O</b></p>
<p><b>GABINETE DO MINISTRO</b></p>
<p><b>DOU de 10/07/2013 (n� 131, Se��o 1, p�g. 18)</b></p>
</center>
<p>Disp�e sobre os procedimentos de ades�o das institui��es federais de educa��o superior ao Projeto Mais M�dicos e d� outras provid�ncias.</p>
<p>O MINISTRO DE ESTADO DA EDUCA��O no uso da atribui��o que lhe confere o art. 87, inciso II da Constitui��o Federal, e tendo em vista o disposto na Medida Provis�ria n� 621, de 8 de julho de 2013, bem como na Portaria Interministerial MS/MEC n� 1.369, de 8 de julho de 2013, resolve:</p>
<p>Art. 1� - Poder�o aderir ao Projeto Mais M�dicos as institui��es federais de educa��o superior que ofere�am curso de Medicina.</p>
<p>� 1� - As institui��es federais de educa��o superior interessadas em aderir ao Projeto Mais M�dicos dever�o apresentar termo de pr�-ades�o, conforme o modelo do <a href="javascript:abreLinkAnexo(<?php echo $hsuid ?>)" >Anexo I</a> desta Portaria, no per�odo de 11 a 15 de julho de 2013, ao Minist�rio da Educa��o.</p>
<p>� 2� - As institui��es dever�o indicar, no momento da pr�ades�o, um tutor acad�mico respons�vel pelas atividades e, no m�nimo, tr�s tutores acad�micos para fins de cadastro de reserva, que atendam aos requisitos da Portaria Interministerial MS/MEC n� 1.369, de 8 de julho de 2013 e desta Portaria.</p>
<p>� 3� - As institui��es dever�o cadastrar via sistema SIMEC, no m�dulo rede federal, por meio do endere�o eletr�nico <a href="http://simec.mec.gov.br" target="_blank">http://simec.mec.gov.br</a>, os tutores indicados no termo de pr�-ades�o.</p>
<p>� 4� - No momento da pr�-ades�o as institui��es dever�o indicar a unidade respons�vel pela avalia��o e autoriza��o de pagamento das bolsas de tutoria e supervis�o acad�micas.</p>
<p>Art. 2� - O Minist�rio da Educa��o decidir� sobre a valida��o do termo de pr�-ades�o das institui��es que atenderem aos requisitos previstos no art. 1� desta Portaria, observadas as necessidades do Projeto Mais M�dicos.</p>
<p>Par�grafo �nico - Em caso de manifesta��o de interesse de mais de uma institui��o por unidade da federa��o, ser� dada prefer�ncia �quela sediada na capital, caso persista o empate, ser� selecionada �quela que ofertar curso de Medicina h� mais tempo.</p>
<p>Art. 3� - As institui��es que tiverem seus termos de pr�-ades�o validados pelo Minist�rio da Educa��o dever�o firmar termo de ades�o no prazo m�ximo de 10 (dez) dias ap�s a divulga��o das institui��es selecionadas.</p>
<p>Par�grafo �nico - O termo de ades�o estar� dispon�vel para assinatura das institui��es selecionadas no sistema SIMEC, no m�dulo rede federal, por meio do endere�o eletr�nico <a href="http://simec.mec.gov.br" target="_blank">http://simec.mec.gov.br</a>, e conter�, no m�nimo, as seguintes obriga��es para a institui��o:</p>
<p>I - atuar em coopera��o com os entes federativos, as Coordena��es Estaduais do Projeto e organismos internacionais, no �mbito de sua compet�ncia, para execu��o do Projeto Mais M�dicos;</p>
<p>II - coordenar o acompanhamento acad�mico do Projeto;</p>
<p>III - ratificar a unidade respons�vel pela avalia��o e autoriza��o de pagamento das bolsas de tutoria e supervis�o acad�micas, indicada no termo de pr�-ades�o;</p>
<p>IV - definir mecanismo de avalia��o e autoriza��o de pagamento das bolsas de tutoria e supervis�o;</p>
<p>V - ratificar a indica��o dos tutores acad�micos do Projeto, feita no termo de pr�-ades�o;</p>
<p>VI - definir crit�rios e mecanismo de sele��o de supervisores;</p>
<p>VII - realizar sele��o dos supervisores do Projeto;</p>
<p>VIII - monitorar e acompanhar as atividades dos supervisores e tutores acad�micos no �mbito do Projeto;</p>
<p>IX - ofertar os m�dulos de acolhimento e avalia��o aos m�dicos intercambistas; e</p>
<p>X - ofertar cursos de especializa��o e atividades de pesquisa, ensino e extens�o aos m�dicos participantes.</p>
<p>Art. 4� - Os tutores acad�micos ser�o selecionados pela institui��o entre os docentes da �rea m�dica, preferencialmente vinculados � �rea de sa�de coletiva ou correlata, ou � �rea de cl�nica m�dica.</p>
<p>� 1� - Os tutores acad�micos perceber�o bolsa-tutoria, na forma prevista no termo de ades�o.</p>
<p>� 2� - Os tutores acad�micos ser�o respons�veis pela orienta��o acad�mica e pelo planejamento das atividades do supervisor, trabalhando em parceria com as Coordena��es Estaduais do Projeto, e tendo, no m�nimo, as seguintes atribui��es:</p>
<p>I - coordenar as atividades acad�micas da integra��o ensinoservi�o, atuando em coopera��o com os supervisores e os gestores do SUS;</p>
<p>II - indicar, em plano de trabalho, as atividades a serem executadas pelos m�dicos participantes e supervisores, bem como a metodologia de acompanhamento e avalia��o;</p>
<p>III - monitorar o processo de acompanhamento e avalia��o a ser executado pelos supervisores, garantindo sua continuidade;</p>
<p>IV - integrar as atividades do curso de especializa��o �s atividades de integra��o ensino-servi�o;</p>
<p>V - relatar � institui��o p�blica de ensino superior � qual esteja vinculado a ocorr�ncia de situa��es nas quais seja necess�ria a ado��o de provid�ncia pela institui��o; e</p>
<p>VI - apresentar relat�rios peri�dicos da execu��o de suas atividades no Projeto � institui��o � qual esteja vinculado e � Coordena��o do Projeto.</p>
<p>Art. 5� - Os supervisores ser�o selecionados entre profissionais m�dicos por meio de edital conforme crit�rios e mecanismos estabelecidos pela institui��o aderente e validados pela Coordena��o Estadual do Projeto Mais M�dicos.</p>
<p>� 1� - Os supervisores selecionados perceber�o bolsa, conforme avalia��o e autoriza��o das institui��es aderentes, na forma prevista no termo de ades�o.</p>
<p>� 2� - Os supervisores selecionados ser�o respons�veis pelo acompanhamento e fiscaliza��o das atividades de ensino-servi�o do m�dico participante, em conjunto com o gestor do SUS no Munic�pio, e ter�o, no m�nimo, as seguintes atribui��es:</p>
<p>I - realizar visita peri�dica para acompanhar atividades dos m�dicos participantes;</p>
<p>II - estar dispon�vel para os m�dicos participantes, por meio de telefone e internet;</p>
<p>III - aplicar instrumentos de avalia��o presencialmente; e</p>
<p>IV - acompanhar e fiscalizar, em conjunto com o gestor do SUS, o cumprimento da carga hor�ria de 40 horas semanais prevista pelo Projeto para os m�dicos participantes, por meio de sistema de informa��o disponibilizado pela Coordena��o do Programa.</p>
<p>Art. 6� - Esta Portaria entra em vigor na data de sua publica��o.</p>
<p>ALOIZIO MERCADANTE OLIVA</p>
<?php
}

function recuperaArquivo($arqid)
{
	global $db;
	$sql = "select * from public.arquivo where arqid = $arqid";
	return $db->pegaLinha($sql);
}

if($_GET['arquivo'])
{
	include_once APPRAIZ . "includes/classes/file.class.inc";
	include_once APPRAIZ . "includes/classes/fileSimec.class.inc";
	$file = new FilesSimec();
	$file-> getDownloadArquivo($_GET['arquivo']);
	exit;
}
 
if($_REQUEST['requisicaoAjax'] && $_REQUEST['classe']){
	$n = new $_REQUEST['classe'];
	$n->$_REQUEST['requisicaoAjax']();
	die;
}
if($_REQUEST['requisicao'] && $_REQUEST['classe']){
	$n = new $_REQUEST['classe'];
	$n->$_REQUEST['requisicao']();
}

require_once APPRAIZ . "includes/cabecalho.inc";
echo '<br/>';

monta_titulo( "Mais M�dicos", "");

// cria o cabe�alho padr�o do m�dulo
echo cabecalhoUnidadeMaisMedicos($hsuid);

$_SESSION['academico']['hsuid'] = $hsuid;

$maisMedicos = new MaisMedicos();
$arrDados = $maisMedicos->recupaPorUnidade($hsuid);
if($arrDados){
	extract($arrDados);
}
if($maisMedicos->msminteresse != "t"){
	$exibe_termo_aceite = true;
}else{
	$exibe_termo_aceite = false;
}
?>
<script>
var verificaPendencias=true;
function abreTermo(){

	return windowOpen('academico.php?modulo=principal/termo&acao=A', 'blank', 'height=600,width=750,status=yes,toolbar=no,menubar=yes,scrollbars=yes,location=no,resizable=yes');
}
</script>
<style>.link{cursor:pointer}</style>
<script type="text/javascript" src="../includes/JQuery/jquery-1.10.2.min.js"></script>
<form name="formulario" id="formulario" method="post" action="" enctype="multipart/form-data" >
<input type="hidden" id="requisicao" name="requisicao" value="salvarMaisMedicos" />
<input type="hidden" id="classe" name="classe" value="MaisMedicos" />
<input type="hidden" id="msminteresse" name="msminteresse" value=""" />
<input type="hidden" id="msmid" name="msmid" value="<?php echo $maisMedicos->msmid?>" />
<input type="hidden" id="hsuid" name="hsuid" value="<?php echo $hsuid?>" />
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
	<?php if($exibe_termo_aceite): ?>
		<tr>
			<td colspan="2" align="center" >
				<div style="width:90%;height:400px;border:solid 1px black;overflow:auto;padding:3px;background-color:#fff">
				<?php echo textoHTML2($hsuid) ?>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<?php $data = date("Ymd")?>
				<?php if( ($data >= 20130711 && $data <= 20130725) || $db->testa_superuser()):?>
					<input type="button" name="btn_aceitar" value="Realizar Pr�-Ades�o" onclick="aceitarTermo()" />
				<?php else: ?>
					A data para realizar a ades�o ao Programas Mais M�dicos ocorreu entre os dias 11 e 25 de julho de 2013. Aguadem novas informa��es.
				<?php endif; ?>
				<?php if($_GET['hsuid']): ?>
	    			<input type="button" name="btn_voltar" value="Voltar" onclick="window.location.href='academico.php?modulo=principal/listarMaisMedicosUnidade&acao=A'" />
	    		<?php endif;?>
			</td>
		</tr>
	<?php else: ?>
		<tr>
			<td class="subtituloCentro" colspan="2">�rg�o de Avalia��o e Autoriza��o de Pagamentos - <a onclick="abreLinkAnexo(<?php echo $hsuid ?>)" >Abrir Termo</a>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" >
				<p>A unidade de avalia��o e autoriza��o ser� respons�vel pelo acompanhamento do trabalho realizado pelos tutores, supervisores e os m�dicos, sendo a principal respons�vel pelas autoriza��es mensais dos pagamentos das bolsas recebidas pelos tutores do programa.</p>
			</td>
		</tr>
		<tr>
			<td class="subtituloDireita" width='250px;' >Nome da Unidade de Avalia��o e Autoriza��o de Pagamentos</td>
			<td>
				<?php echo campo_texto('msmsetorresponsavel', 'S', "S", '', 60, 255, '', '', 'left', '', 0, 'id="msmsetorresponsavel"', ''); ?>
			</td>
		</tr>
		<tr>
			<td class="subtituloDireita" >Telefone</td>
			<td>
				<?php echo campo_texto('msmdddsetor', 'N', "S", '', 2, 2, '##', '', 'left', '', 0, 'id="msmdddsetor"', ''); ?> - <?php echo campo_texto('msmfonesetor', 'S', "S", '', 10, 10, '####-####', '', 'left', '', 0, 'id="msmfonesetor"', ''); ?>
			</td>
		</tr>
		<tr>
			<td class="subtituloDireita" >Pessoa Respons�vel</td>
			<td>
				<?php echo campo_texto('msmresponsavelsetor', 'S', "S", '', 60, 255, '', '', 'left', '', 0, 'id="msmresponsavelsetor"', ''); ?>
			</td>
		</tr>
		<tr>
			<td class="subtituloDireita" >Email do Setor Respons�vel</td>
			<td>
				<?php echo campo_texto('msmemailsetor', 'S', "S", '', 60, 255, '', '', 'left', '', 0, 'id="msmemailsetor"', ''); ?>
			</td>
		</tr>
		<tr>
    		<td colspan="2" class="SubtituloCentro" >Anexo do Termo de Pr�-Ades�o</td>
		</tr>
		<tr>
			<td colspan="2" align="center" >
				<p>Favor anexar abaixo o <A href="javascript:abreLinkAnexo(<?php echo $hsuid ?>);" >termo de pr�-ades�o</a> assinado.</p>
			</td>
		</tr>
		<tr>
	    	<td class="SubTituloDireita">Termo de Pr�-Ades�o Assinado</td>
	        <td id="td_arquivo">
			<?php if($arqid): ?>
				<?php $arrArquivo = recuperaArquivo($arqid)  ?>
				<a href="javascript:downloadAnexo(<?php echo $arqid ?>)" ><?php echo $arrArquivo['arqnome'] ?>.<?php echo $arrArquivo['arqextensao'] ?></a>
				<?php if(!$somente_leitura): ?>
					<img src="../imagens/excluir.gif" class="link" onclick="excluirAnexo(<?php echo $arqid ?>)" />
				<?php endif; ?>
				<input type="hidden" name="arquivo" id="arquivo" value="<?php echo $arqid ?>" />
			<?php else: ?>
				<input type="file" name="arquivo" id="arquivo" />
			<?php endif; ?> 
			</td>
		</tr>
		<tr>
    		<td colspan="2" class="SubtituloCentro" >Tutor(es)</td>
		</tr>
		<tr>
    		<td colspan="2" >
    			<span title="Cadastrar Novo Tutor" class="link" style="font-weight:bold" onclick="cadNovoTutor()"><img style="vertical-align:middle" src="../imagens/gif_inclui.gif" /> Novo Tutor</span>
    		</td>
		</tr>
	<?php endif; ?>
</table>
</form>
<div id="div_lista_tutores">
<center>
<?php 
	if(!$exibe_termo_aceite){
		$tutor = new MaisMedicosResponsabilidade();
		$tutor->listaTutores(null,$hsuid);
	}
?>
</center>
</div>
<?php if(!$exibe_termo_aceite): ?>
	<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
		<tr>
	    	<td class="subtituloCentro" colspan="2" >
	    		<input type="button" name="btn_salvar" value="Salvar" onclick="salvarMaisMedicos()" />
	    		<?php if($_GET['hsuid']): ?>
	    			<input type="button" name="btn_voltar" value="Voltar" onclick="window.location.href='academico.php?modulo=principal/listarMaisMedicosUnidade&acao=A'" />
	    		<?php endif;?>
	    	</td>
		</tr>
	</table>
<?php endif; ?>
<script>
function aceitarTermo()
{
	$("#msminteresse").val("sim");
	$("#classe").val("MaisMedicos");
	$("#requisicao").val("salvarMaisMedicos");
	$("#formulario").submit();
}

function cadNovoTutor()
{
	return windowOpen('academico.php?modulo=principal/cadTutoresUnidade&acao=A','blank','height=700,width=700,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes' );
}

function alterarTutor(entid)
{
	return windowOpen('academico.php?modulo=principal/cadTutoresUnidade&acao=A&entid='+entid,'blank','height=700,width=700,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes' );
}

function salvarMaisMedicos()
{
	var erro = 0;
	$("[class~=obrigatorio]").each(function() { 
		if(!this.value || this.value == "Selecione..."){
			erro = 1;
			alert('Favor preencher todos os campos obrigat�rios.');
			this.focus();
			return false;
		}
	});
	/*if(erro == 0 && !$("#arquivo").val()){
		erro = 1;
		alert('Favor informar o Anexo do Termo de Pr�-Ades�o.');
		return false;
	}*/
	if(erro == 0){
		$("#classe").val("MaisMedicos");
		$("#requisicao").val("salvarMaisMedicos");
		$("#formulario").submit();
	}
}

function excluirTutor(mmrid,hsuid)
{
	if(confirm("Deseja realmente exluir este tutor?"))
	{
		$.ajax({
		   type: "POST",
		   url: window.location,
		   data: "requisicaoAjax=excluirTutor&classe=MaisMedicosResponsabilidade&mmrid="+mmrid+"&hsuid="+hsuid,
		   success: function(msg){
			   	alert('Opera��o realizada com sucesso.');
		   		$('#div_lista_tutores').html( msg );
		   }
		 });
	}
}

function listaTutoresAjax(hsuid)
{
	$.ajax({
	   type: "POST",
	   url: window.location,
	   data: "requisicaoAjax=listaTutoresAjax&classe=MaisMedicosResponsabilidade&hsuid="+hsuid,
	   success: function(msg){
	   		$('#div_lista_tutores').html( msg );
	   }
	 });
}

function atribuirTitularidade(entidtutor,hsuid)
{
	$.ajax({
		   type: "POST",
		   url: window.location,
		   data: "requisicaoAjax=atribuirTitularidade&classe=MaisMedicosResponsabilidade&entidtutor="+entidtutor+"&hsuid="+hsuid,
		   success: function(msg){
			   	//alert('Opera��o realizada com sucesso.');
		   }
	});
}

function abreLinkAnexo(hsuid)
{
	return windowOpen('academico.php?modulo=principal/anexoPortaria&acao=A&hsuid='+hsuid,'blank','height=700,width=700,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes' );
}

function downloadAnexo(arqid)
{
	window.location.href="academico.php?modulo=principal/maisMedicos&acao=A&arquivo="+arqid;
}

function excluirAnexo(arqid)
{
	if(confirm("Deseja realmente exluir este arquivo?"))
	{
		$.ajax({
		   type: "POST",
		   url: window.location,
		   data: "requisicaoAjax=excluirAnexo&classe=MaisMedicos&arqid="+arqid,
		   success: function(msg){
		   		$("#arquivo").val("");
		   		$('#td_arquivo').html( "<input type=\"file\" name=\"arquivo\" id=\"arquivo\" />" );
		   }
		 });
	}
}

$(function() {
	<?php if($_SESSION['academico']['maismedicos']['alert']): ?>
		alert('<?php echo $_SESSION['academico']['maismedicos']['alert'] ?>');
		<?php unset($_SESSION['academico']['maismedicos']['alert']) ?>
	<?php endif; ?>
});
</script>