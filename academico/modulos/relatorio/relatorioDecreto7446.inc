<?php

if ($_POST['agrupador']){
	ini_set("memory_limit","2048M");
	$true = true;
	include("relatorioDecreto7446Resultado.inc");
	exit;
}
//Quando recuperado o par�metro enviado via Ajax, valor ser� testado logicamente nas segunites condi��es:  
if($_POST['tipo'] == 'educacao'){
	//Se " educacaoId == 1 " ser�o apresentadas apenas as Institui��es da Educa��o Superior.
	if($_POST['educacaoId'] == '1'){ 
			$whereTipoEnsino  =  " WHERE ef.funid = ' " . ACA_ID_UNIVERSIDADE . " ' ";
	//Sen�o se " educacaoId == 2 " ser�o apresentadas apenas  as Institui��es da Educa��o Profissional.
	}else if ($_POST['educacaoId'] == '2'){
			$whereTipoEnsino  =  " WHERE ef.funid = ' " . ACA_ID_ESCOLAS_TECNICAS . " ' ";
	}else if ($_POST['educacaoId'] == '3'){
			$whereTipoEnsino  =  " WHERE ef.funid = '16' ";
	}
}	
include APPRAIZ. '/includes/Agrupador.php';
include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( 'Decreto 7446 de 1� de mar�o de 2011', '&nbsp;' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Relat�rio</title>
<script src="../includes/calendario.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery2.js"></script>
<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/prototype.js"></script>
<script type="text/javascript">
function gerarRelatorio(req){
	var formulario = document.formulario;

	document.getElementById('req').value = req;
	
	if (formulario.elements['agrupador'][0] == null){
		alert('Selecione pelo menos um agrupador!');
		return false;
	}	
	
	selectAllOptions( formulario.agrupador );
	selectAllOptions( formulario.instituicao );
	selectAllOptions( formulario.tiposolicitacao );
	selectAllOptions( formulario.gndcod );
	selectAllOptions( formulario.momento );
			
	var janela = window.open( '', 'relatorio', 'width=900,height=600,status=1,menubar=1,toolbar=0,scrollbars=1,resizable=1' );
	formulario.target = 'relatorio';	
//	ajaxRelatorio();
	formulario.submit();
	
	janela.focus();
}

/**
 * Alterar visibilidade de um campo.
 * 
 * @param string indica o campo a ser mostrado/escondido
 * @return void
 */
function onOffCampo( campo )
{
	var div_on = document.getElementById( campo + '_campo_on' );
	var div_off = document.getElementById( campo + '_campo_off' );
	var input = document.getElementById( campo + '_campo_flag' );
	if ( div_on.style.display == 'none' )
	{
		div_on.style.display = 'block';
		div_off.style.display = 'none';
		input.value = '1';
	}
	else
	{
		div_on.style.display = 'none';
		div_off.style.display = 'block';
		input.value = '0';
	}
}

function ajaxRelatorio(){
	var formulario = document.formulario;
	var divRel 	   = document.getElementById('resultformulario');

	divRel.style.textAlign='center';
	divRel.innerHTML = 'carregando...';

	var agrupador = new Array();	
	for(i=0; i < formulario.agrupador.options.length; i++){
		agrupador[i] = formulario.agrupador.options[i].value; 
	}	
	
	var tipoensino = new Array();
	for(i=0; i < formulario.f_tipoensino.options.length; i++){
		tipoensino[i] = formulario.f_tipoensino.options[i].value; 
	}		
	
	var param =  '&agrupador=' + agrupador + 
				 '&f_tipoensino=' + tipoensino +
				 '&f_tipoensino_campo_excludente=' + formulario.f_tipoensino_campo_excludente.checked +
				 '&f_tipoensino_campo_flag=' + formulario.f_tipoensino_campo_flag.value;
	 
    var req = new Ajax.Request('academico.php?modulo=inicio&acao=C', {
						        method:     'post',
						        parameters: param,
						        onComplete: function (res)
						        {
							    	divRel.innerHTML = res.responseText;
						        }
							});
	
}
//Quando selecionado o radiobutton em um tipo de Educa��o, passa por par�metro o valor do mesmo, via Ajax.   
function selecionaEducacao(idEducacao){
	var eduId = idEducacao.value;
	var url = '?modulo=relatorio/relatorioDecreto7446&acao=A'; 
	new Ajax.Request(url,{
		method: 'post',
		parameters: '&tipo=educacao&educacaoId=' + eduId, 
			onComplete: function(res){
				res.responseText;
			}
		}
	);
}
</script>
</head>
<body>
<form name="formulario" id="formulario" action="" method="post">
<input type="hidden" name="req" id="req" value="" />	
<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" style="border-bottom:none;">
	<tr>
		<td class="SubTituloDireita" valign="top">Tipo de Ensino</td>	
		<td>
		<?php
			$sql = " SELECT
							orgid AS codigo,
							orgdesc AS descricao
						FROM
							academico.orgao 
						Where
							orgstatus = 'A'
						UNION ALL
						SELECT 3 AS codigo,
							'Hospitais' as descricao
						ORDER BY 2 ASC;";
			$db->monta_combo("f_tipoensino", $sql, 'S', "Todos", 'selecionaEducacao(this);', '', '', '', 'N', 'prgid');
	    ?>		

		</td>
	</tr>
	<tr> 
		<td class="SubTituloDireita" valign="top">Agrupadores</td>
		<td>
			<?
			$matriz = agrupador();
			$campoAgrupador = new Agrupador( 'formulario' );
			$campoAgrupador->setOrigem( 'agrupadorOrigem', null, $matriz );
			$campoAgrupador->setDestino( 'agrupador', null, null);
			$campoAgrupador->exibir();
			?>
		</td>
	</tr>
		<?php
			
				// Filtros do relat�rio
			
				// Exerc�cio
				/*
				$stSql = " SELECT DISTINCT
								prtano AS codigo,
								prtano AS descricao
							FROM 
								academico.portarias
							%s
							ORDER BY
								prtano ";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Exerc�cio', 'exercicio',  $stSql, $stSqlCarregados, 'Selecione o(s) Exerc�cio(s)' ); 
			*/
				// instituicao
				$stSql = " SELECT DISTINCT
								e.entid AS codigo,
								CASE ef.funid
									WHEN " . ACA_ID_UNIVERSIDADE . " THEN 'Ed. Superior - ' || e.entnome
									ELSE 'Ed. Profissional - ' || e.entnome
								END AS descricao 
							FROM 
								entidade.entidade e
							INNER JOIN
								entidade.funcaoentidade ef ON ef.entid = e.entid
															  AND ef.funid IN ( '" . ACA_ID_UNIVERSIDADE . "', '" . ACA_ID_ESCOLAS_TECNICAS . "' )		
							INNER JOIN
								academico.acumuladoprojetado ep ON e.entid = ep.entidentidade
							$whereTipoEnsino 
							%s
							ORDER BY
								descricao, e.entid ";
							//dbg($stSql);
				$stSqlCarregados = "";
				mostrarComboPopup( 'Institui��o', 'instituicao',  $stSql, $stSqlCarregados, 'Selecione a(s) Institui��o(�es)' ); 

				/*
				// Campus
				$stSql = " SELECT DISTINCT
								e.entid AS codigo,
								e.entnome AS descricao
							FROM 
								entidade.entidade e
							INNER JOIN
								academico.acumuladoprojetado ep ON e.entid = ep.entidcampus
							%s
							ORDER BY
								e.entnome, e.entid ";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Campus', 'campus',  $stSql, $stSqlCarregados, 'Selecione o(s) Campus(s)' ); 
			
				// Programa
				$stSql = " SELECT
								prgid AS codigo,
								prgdsc AS descricao
							FROM 
								academico.programa
							%s
							ORDER BY
								prgdsc ";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Programa', 'programa',  $stSql, $stSqlCarregados, 'Selecione o(s) Programa(s)' ); 
				
				// Classe
				$stSql = " SELECT
								clsid AS codigo,
								clsdsc AS descricao
							FROM 
								academico.classes
							%s
							ORDER BY
								clsdsc ";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Classe', 'classe',  $stSql, $stSqlCarregados, 'Selecione a(s) Classe(s)' ); 
				*/
				// Tipo Solicita��o
				$stSql = " SELECT
								tpaid AS codigo,
								tpadsc AS descricao
							FROM 
								academico.tiposolicitacao
							ORDER BY
								tpadsc ";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Tipo de Solicita��o', 'tiposolicitacao',  $stSql, $stSqlCarregados, 'Selecione o(s) Tipo(s) de Solicita��o(�es)' ); 
				
				// GNDcod
				$sql = "SELECT 
							gndcod as codigo, 
							gndcod|| ' - '||gnddsc as descricao
						FROM 
							public.gnd
						WHERE
							gndstatus = 'A'
						ORDER BY
							gnddsc";
				$stSqlCarregados = "";
				mostrarComboPopup( 'C�digo GND', 'gndcod',  $sql, $stSqlCarregados, 'Selecione o(s) C�digo(s) GND' );
				
				// Momento
				$sql = "SELECT
							slfid as codigo,
							slfdsc as descricao
						FROM academico.sldfase
						WHERE
							slfstatus = 'A'
						ORDER BY
							slfdsc";
				$stSqlCarregados = "";
				mostrarComboPopup( 'Momento', 'momento',  $sql, $stSqlCarregados, 'Selecione o(s) Momento(s)' ); 
				
			?>
	<tr> 
		<td class="SubTituloDireita" valign="top">Valor</td>
		<td>
			<?=campo_texto('valorMin','','S','',20,20,'[.###],##','','','','','id="valorMin"');?>
			at� 
			<?=campo_texto('valorMax','','S','',20,20,'[.###],##','','','','','id="valorMax"');?>
		</td>
	</tr>
	<tr> 
		<td class="SubTituloDireita" valign="top">Valor</td>
		<td>
			<?
			$opts = Array(Array('codigo'=>'>' ,'descricao'=>'Maior que'),
						  Array('codigo'=>'>=','descricao'=>'Maior Igual a'),
						  Array('codigo'=>'=' ,'descricao'=>'Igual a'),
						  Array('codigo'=>'<' ,'descricao'=>'Menor que'),
						  Array('codigo'=>'<=','descricao'=>'Menor igual a'));	

			$db->monta_combo('operador',$opts,'S','Todos','','','','','');
			echo '&nbsp;&nbsp';
			echo campo_texto('valorOp','','S','',20,20,'[.###],##','','','','','id="valorMax"');
			?>
		</td>
	</tr>
	<tr> 
		<td class="SubTituloDireita" valign="top">Expandir Relat�rio</td>
		<td>
			<?
			$expandir = 0;
			$sql = "(SELECT
						1 as codigo,
						'Expandir' as descricao)
					UNION ALL
					(SELECT
						0 as codigo,
						'N�o Expandir' as descricao)";	

			$db->monta_radio('expandir', $sql, '',1);
			?>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="button" name="Gerar Relat�rio" value="Gerar Relat�rio" onclick="javascript:gerarRelatorio(1);"/>
			<input type="button" name="Gerar Relat�rio XLS" value="Gerar Relat�rio XLS" onclick="javascript:gerarRelatorio(2);"/>
		</td>
	</tr>
</table>
</form>
</body>
<?
function agrupador(){
	return array(

				array('codigo' => 'instituicao',
					  'descricao' => 'Institui��o'),				
				array('codigo' => 'tiposolicitacao',
					  'descricao' => 'Tipo de Solicita��o'),
				array('codigo' => 'detalhessolicitacao',
					  'descricao' => 'Detalhes da Solicita��o'),
				array('codigo' => 'gnd',
					  'descricao' => 'C�digo GND'),
				array('codigo' => 'tipoensino',
					  'descricao' => 'Tipo de Ensino'),
				array('codigo' => 'momento',
					  'descricao' => 'Momento'),
				);
}
?>
</html>