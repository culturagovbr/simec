<?php
	
// Inclui componente de relat�rios
include APPRAIZ. 'includes/classes/relatorio.class.inc';

// instancia a classe de relat�rio
$rel = new montaRelatorio();

// monta o sql, agrupador e coluna do relat�rio
$sql       = acaMontaSqlRelCursos();
$agrupador = acaMontaAgpRelCursos();
$coluna    = acaMontaColunaRelCursos();
$dados 	   = $db->carregar( $sql );

$rel->setAgrupador( $agrupador, $dados ); 
$rel->setColuna( $coluna );
$rel->setTotNivel( true );


?>


<html>
	<head>
		<title> Simec - Sistema Integrado de Monitoramento do Minist�rio da Educa��o </title>
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css">
		<link rel="stylesheet" type="text/css" href="../includes/listagem.css">
	</head>
	<body>
		<center>
			<!--  Cabe�alho Bras�o -->
			<?php echo monta_cabecalho_relatorio( '95' ); ?>
		</center>
		
		<!--  Monta o Relat�rio -->
		<?php echo $rel->getRelatorio(); ?>
		
	</body>
</html>