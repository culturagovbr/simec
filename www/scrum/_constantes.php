<?php 

/**
 * Perfil: Gestor de programa
 */
define('PERFIL_GESTORPROGRAMA' , 1067);

/**
 * Perfil: Consulta
 */
define('PERFIL_CONSULTA' , 1154);

/**
 * Status de "Criado" do entregavel
 */
define('ENTREGAVEL_STATUS_CRIADO' , 1);

/**
 * Status de "Em execucao" do entregavel
 */
define('ENTREGAVEL_STATUS_EM_EXECUCAO' , 2);

/**
 * Status de "Pronto" do entregavel
 */
define('ENTREGAVEL_STATUS_PRONTO' , 5);

/**
 * Status de "Entregavel" do entregavel
 */
define('ENTREGAVEL_STATUS_CANCELADO' , 6);

/**
 * Status de "Aguardando verificacao" do entregavel
 */
define('ENTREGAVEL_STATUS_AGUARDANDO_VERIFICACAO' , 3);
