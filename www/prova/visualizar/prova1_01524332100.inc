<?php 
include  APPRAIZ."includes/cabecalho.inc";
echo'<br>';
monta_titulo( "Prova SIMEC", 'Quest�o 01 - Liste as informa��es solicitadas' );

/*
 * DICAS:
 * 
 * Com a fun��o "carregar" tem-se o resultado da query em um array. Ex:
 * 
 * 		$sql = "SELECT dia, mes, ano FROM data WHERE condicao = condicao";
 * 		$arrDados = $db->carregar($sql); 
 * 
 * RESULTADO DO $arrDados:
 * 
 * 		[0] => Array
	        (
	            [dia] => 24
	            [mes] => 03
	            [ano] => 1984
	        )
	
	    [1] => Array
	        (
	            [dia] => 22
	            [mes] => 10
	            [ano] => 1983
	        )
	
	    [2] => Array
	        (
	            [dia] => 27
	            [mes] => 12
	            [ano] => 2012
	        )
 * 
 */
 
 $sql_prof = 'SELECT *
 				FROM prova.professor as pro
 ';
 
 $sql_turmas = "SELECT tur.turnome,
 					   to_char(tur.turdtabr,'dd/mm/YYYY') as turdtabr,
 					   -- tur.turdtabr,
 					   to_char(tur.turdtfec,'dd/mm/YYYY') as turdtfec,
 					   -- tur.turdtfec,
 					   tur.qtdalu,
 					   pro.pronome
 		   		FROM prova.turma as tur
 		   		LEFT JOIN prova.professor as pro
 		   		ON tur.proid = pro.proid
 		   ";
 $arrProf   = $db->carregar($sql_prof);
 $arrTurmas = $db->carregar($sql_turmas);
 
 //echo '<pre>';
 //print_r($arrProf);
 //print_r($arrTurmas);
 //echo '</pre>';

?>
<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
	function navega(questao){
		location.href = "/prova/prova.php?modulo=principal/prova"+questao+"&acao=A";
	}
</script>
<table style="background-color: #F5F5F5;" align="center" border="1" width="95%" class="tabela" cellpadding="3" cellspacing="2">
	<tr bgcolor="gray">
		<td>
			<b>Nome da Turma</b>
		</td>
		<td>
			<b>Dia de abertura da turma</b>
		</td>
		<td>
			<b>Dia do fechamento da turma</b>
		</td>
		<td>
			<b>Quantidade de Alunos</b>
		</td>
		<td>
			<b>Professor respons�vel</b>
		</td>
	</tr>
	
	<?php
	
		foreach($arrTurmas AS $k => $v ){
			echo '<tr>';
			echo '<td>&nbsp;'.$v['turnome'].'</td>';
			echo '<td>&nbsp;'.$v['turdtabr'].'</td>';
			echo '<td>&nbsp;'.$v['turdtfec'].'</td>';
			echo '<td>&nbsp;'.$v['qtdalu'].'</td>';
			echo '<td>&nbsp;'.$v['pronome'].'</td>';
			echo '</tr>';
		}
	
	?>
	
	<tr>
		<td>
			
		</td>
		<td>
			
		</td>
		<td>
			
		</td>
		<td>
			
		</td>
		<td>
			
		</td>
	</tr>
</table>


<table cellspacing="1" cellpadding="3" bgcolor="#f5f5f5" align="center" width="95%" class="tabela">
	<tbody>
		<tr bgcolor="#cccccc">
			<td>
				<div style="float:left; width:30%; text-align: left;">
					<input disabled="disabled" value="Quest�o anterior" type="button" id="anterior">
				</div>
				<div style="float:right; width:30%; text-align: right;">
					<input value="Pr�xima Quest�o" onclick="navega(2)" type="button" id="proximo">
				</div>
			</td>
		</tr>
	</tbody>
</table>