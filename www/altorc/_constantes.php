<?php
/**
 * $Id$
 */
define('MODULO', $_SESSION['sisdiretorio']);

/**
 * 
 */
define('STDOC_EM_PREENCHIMENTO', 1092);
define('STDOC_ANALISE_SPO', 1093);
define('STDOC_AJUSTE_UO', 1094);
define('STDOC_CADASTRAR_SIOP', 1095); // Status Confirmado no SIOP

/**
 *
 */
define('TPDOC_PEDIDO_ALTERACAO_ORCAMENTARIA', 177);

// -- Perfis
define('PFL_SUPER_USUARIO', 1187);
define('PFL_CGO_EQUIPE_ORCAMENTARIA', 1189);
define('PFL_UO_EQUIPE_TECNICA', 1188);
