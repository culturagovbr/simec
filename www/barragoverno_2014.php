<link href="/library/simec/css/barra_brasil.css" rel="stylesheet">

<!-- BOOTSTRAP BARRA BRASIL -->
<div class="bbrasil" style="margin-bottom: 15px;">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="row rowbrasil">
                    <!-- Barra Brasil -->
                    <div id="barra-brasil">
                        <div class="barra">
                            <ul>
                                <li><a title="Acesso � informa��o" class="ai"
                                       href="http://www.acessoainformacao.gov.br">www.sic.gov.br</a></li>
                                <li><a title="Portal de Estado do Brasil" class="brasilgov"
                                       href="http://www.brasil.gov.br">www.brasil.gov.br</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /Barra Brasil -->
                </div>
            </div>
        </div>
    </div>
</div><!-- /BOOTSTRAP BARRA BRASIL -->