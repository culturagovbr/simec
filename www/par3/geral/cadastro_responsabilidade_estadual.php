<?php

// carrega as fun��es espec�ficas do m�dulo
include_once '_constantes.php';
include_once '_funcoes.php';
include_once '_componentes.php';
include_once 'autoload.php';

include_once APPRAIZ.'includes/library/simec/view/Helper.php';

$simec = new Simec_View_Helper();

?>
<!-- Mainly scripts -->
<script src="/zimec/public/temas/simec/js/jquery-2.1.1.js"></script>
<script src="/zimec/public/temas/simec/js/bootstrap.min.js"></script>
<script language="JavaScript" src="/estrutura/js/funcoes.js"></script>

<!-- Data picker -->
<script src="/zimec/public/temas/simec/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="/zimec/public/temas/simec/js/plugins/datapicker/locales/bootstrap-datepicker.pt-BR.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<!--<script src="js/html5shiv.js"></script>-->
<!--<script src="js/respond.min.js"></script>-->
<![endif]-->

<link href="/zimec/public/temas/simec/fonts/awesome/font-awesome.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/fonts/glyphicons/glyphicons.css" rel="stylesheet">

<link href="/zimec/public/temas/simec/css/bootstrap.min.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/chosen/chosen.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/gritter/jquery.gritter.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<link href="/zimec/public/temas/simec/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

<link href="/zimec/public/temas/simec/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/dropzone/basic.css" rel="stylesheet">

<link href="/zimec/public/temas/simec/css/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<link href="/zimec/public/temas/simec/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/nanoscroll/nanoscroller.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/plugins/jstree/style.min.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/animate.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/style.min.css" rel="stylesheet">
<link href="/zimec/public/temas/simec/css/simec.css" rel="stylesheet">