<?php

include "config.inc";
include APPRAIZ . "includes/classes_simec.inc";
include APPRAIZ . "includes/funcoes.inc";

function rksort( &$array )
{
	if ( is_array( $array ) == false )
	{
		return;
	}
	ksort( $array );
	foreach ( array_keys( $array ) as $key )
	{
		rksort( $array[$key] );
	}
}

/**
 * Remove os campos n�o utilizados
 */
function tratarInput( &$traducao )
{
	rksort( $traducao );
	if ( is_array( $traducao ) == false )
	{
		return;
	}
	foreach ( $traducao as $nomeCampo => &$campo )
	{
		$quantidadeVazio = 0;
		foreach ( $campo as $key => $dados )
		{
			if ( $dados['destino'] == '' )
			{
				$quantidadeVazio++;
			}
		}
		if ( count( $campo ) == $quantidadeVazio )
		{
			unset( $traducao[$nomeCampo] );
		}
	}
}

$db = new cls_banco();

define( 'SIAF_DIR_REF_FILES', APPRAIZ . 'financeiro/arquivos/siafi/teste/' );
define( 'SIAF_DIR_TXT_FILES', APPRAIZ . 'financeiro/arquivos/siafi/teste/' );
define( 'SIAF_MAX_MONTH', 13 );

class ImportacaoSiaf
{
	
	/**
	 * Estruturas que definem o conte�do dos arquivos a serem importados.
	 *
	 * @var unknown_type
	 */
	protected $estrutura = array();

	/**
	 * Relaciona um campo definido em um arquivo do SIAF � um campo de uma
	 * tabela do sistema.
	 *
	 * @var unknown_type
	 */
	protected $traducao = array();
	
	/**
	 * Enter description here...
	 *
	 * @var cls_banco
	 */
	protected $db = null;
	
	public function __construct()
	{
		global $db;
		$this->db = $db;
	}
	
	protected function carregarEstrutura( $tipo )
	{
		// verifica se estrutura j� foi carregada
		if ( array_key_exists( $tipo, $this->estrutura ) )
		{
			return;
		}
		
		// carrega arquivo que define estrutura
		$ref = $this->pegarRef( $tipo );
		
		// o indice de cada campo � o nome, que cont�m as subchaves:
		// inicio, tamanho, dividir e repeticoes
		$campos = array();
		
		// utilizado para indicar onde se inicia um campo na linha
		// vari�vel incremental
		$inicio = 0;
		
		// percorre arquivo linha a linha
		// o arquivo � carregado todo para a mem�ria, pois seu tamanho n�o � grande
		foreach ( $ref as $linha )
		{
			// pega defini��es da linha
			$linha = trim( $linha );
			preg_match( '/([^\s]+)[\s]{1,}[a-z]{1}[\s]{1}(.*)/i', $linha, $match );
			
			// define nome
			$nome = trim( $match[1] );
			
			// trata nome para os casos de campos que se repetem por meses
			if ( $nome{strlen($nome)-1} == ')' )
			{
				$nome = substr( $nome, 0, strpos( $nome, '(' ) - 1 );
			}
			
			// verifica se o campo j� existe
			// ocorre em campos que se repetem por meses
			if ( array_key_exists( $nome, $campos ) == true )
			{
				// a quantidade de vezes que o campo aparece � repetida
				$campos[$nome]['repeticoes']++;
				// o in�cio � alterado para a leitura do pr�ximo campo
				$inicio += $campos[$nome]['tamanho'];
				continue;
			}
			
			// define tamanho e se campo possui casas decimais
			// para o caso de decimais o tamanho da parte fracionada �
			// adicionada ao tamanho original do registro
			$casas_decimais = 0;
			if ( strpos( $match[2], ',' ) !== false )
			{
				$valores = explode( ',', $match[2] );
				$casas_decimais = $valores[1];
				$match[2] = $valores[0] + $valores[1];
			}
			$tamanho = (integer) $match[2];
			
			// define se � preciso realizar opera��es com o valor
			// ocorre nos casos de campos que possuem casas decimais
			$dividir = pow( 10, $casas_decimais );
			if ( $dividir == 1 )
			{
				$dividir = null;
			}
			
			// armazena defini��o da linha
			$campos[$nome] = array(
				'inicio' => $inicio,
				'tamanho' => $tamanho,
				'dividir' => $dividir,
				'repeticoes' => 1
			);
			
			// incrementa ponteiro de posi��o para a leitura de cada campo
			$inicio += $tamanho;
		}
		
		// monta estrutura final que define os dados presentes nos arquivos do tipo
		$this->estrutura[$tipo] = array(
			'campos' => $campos,
			// tamanho de cada linha no arquivo
			// ao final da leitura o a variavel aponta para o final da linha
			'tamanho_registro' => $inicio
		);
	}

	/**
	 * Importa dados de um arquivo SIOF
	 *
	 * ...
	 *
	 * @param string $arquivo
	 */
	public function importarArquivo( $arquivo )
	{
		// define caminho para o arquivo
		$caminho = SIAF_DIR_TXT_FILES . $arquivo;
		
		// verifica se arquivo existe
		if ( file_exists( $caminho ) == false )
		{
			return;
		}
		
		// captura sigla/abrevia��o
		preg_match( '/[^_]+_(.*)_[0-9]{8}\.txt/', $arquivo, $match );
		$tipo = $match[1];
		
		// carrega estrutura para realizar importa��o
		$estrutura = $this->pegarEstrutura( $tipo );
		
		// para a quebra linha utilizam \r\n
		$tamanhoQuebraLinha = 2;
		
		// tamanho de cada linha
		$tamanho = $estrutura['tamanho_registro'] + $tamanhoQuebraLinha;
		
		// vari�veis utilizadas para fins estat�sticos relativo ao arquivo
		$totalRegistros = 0;
		$totalRegistrosRepetidos = 0;
		
		// l� arquivo linha a linha
		$handle = fopen( SIAF_DIR_TXT_FILES . $arquivo, 'r' );
		while( !feof( $handle ) )
		{
			
			// vari�vel utilizada para fins estat�sticos relativos � linha
			$totalRegistrosLinha = 0;
			
			// l� registro completo
			$registroBruto = fread( $handle, $tamanho );
			if ( strlen( $registroBruto ) != $tamanho )
			{
				// TODO indicar erro de linha incorreta
				continue;
			}
			
			// remove quebra de linha ao final do registro
			$registroBruto = substr( $registroBruto, 0, -$tamanhoQuebraLinha );

			// um registro bruto pode conter v�rios registros
			
			// armazena campos que n�o se repetem
			$camposAgrupadores = array();
			
			// armazena os registros finais
			$registros = array();
			
			// percorre o conte�do da linha campo a campo
			// a leitura � realizada de acordo com as defini��es da estrutura
			foreach ( $estrutura['campos'] as $nomeCampo => $dadosCampo )
			{
				// os campos que n�o se repetem s�o armazenados na lista de agrupadores
				if ( $dadosCampo['repeticoes'] == 1 )
				{
					$camposAgrupadores[$nomeCampo] = $dado = substr( $registroBruto, $dadosCampo['inicio'],  $dadosCampo['tamanho'] );
				}
				// os campos que se repetem s�o inseridos direto na lista de registros
				else
				{
					// caso o registro se repita
					for ( $mes = 1; $mes <= $dadosCampo['repeticoes']; $mes++ )
					{
						if ( $mes > SIAF_MAX_MONTH )
						{
							continue;
						}
						// cria o registro caso ele n�o exista
						if ( array_key_exists( $mes, $registros ) == false )
						{
							$registros[$mes] = array();
						}
						$registros[$mes][$nomeCampo] = substr( $registroBruto, $dadosCampo['inicio'],  $dadosCampo['tamanho'] );
					}
				}
			}
			
			// case n�o haja campos que se repetem, s� existe um registro
			if ( count( $registros ) == 0 )
			{
				$registros = $camposAgrupadores;
			}
			// caso haja campos que se repetem, faz merge de cada registro com os agrupadores
			else foreach ( $registros as &$registro )
			{
				$registro = array_merge( $camposAgrupadores, $registro );
			}
			
			// realiza opera��o com os registros
			foreach ( $registros as $registro )
			{
				$this->importarRegistro( $registro, $tipo );
			}
			
		}
		fclose( $handle );
	}
	
	protected function importarRegistro( $registro, $tipo )
	{
		// pegar informa��es que trduzem para onde os dados devem ir
		var_dump( $tipo );
		var_dump( $registro );
	}
	
	/**
	 * Captura um arquivo .ref para um determinado tipo.
	 *
	 * O arquivo .ref carregado � o primeiro encontrado. Caso n�o exista um
	 * arquivo para o tipo determinado ou o tipo seja 'Saldo_Contabil' um texto
	 * vazio � retornado.
	 *
	 * @return string[]
	 */
	protected function pegarRef( $tipo )
	{
		// verifica se tipo � saldo cont�bil
		if ( $tipo == 'Saldo_Contabil' )
		{
			return array();
		}
		
		// lista arquivos do tipo
		$arquivos = glob( SIAF_DIR_REF_FILES . '*_' . $tipo . '_*.ref' );
		return count( $arquivos ) == 0 ? '' : file( current( $arquivos ) ) ;
	}
	
	public function pegarEstrutura( $tipo )
	{
		$this->carregarEstrutura( $tipo );
		return $this->estrutura[$tipo];
	}
	
	public function pegarCampos( $tipo )
	{
		$estrutura = $this->pegarEstrutura( $tipo );
		return $estrutura['campos'];
	}
	
	public function salvarParametros( $tipo, $tabela, $parametros )
	{
		$estrutura = $this->pegarEstrutura( $tipo );
		$campos = $estrutura['campos'];
		$traducao = array();
		foreach ( $parametros as $nomeCampo => $dadosCampo )
		{
			if ( array_key_exists( $nomeCampo, $campos ) == false )
			{
				continue;
			}
			$ponteiro = $campos[$nomeCampo]['inicio'];
			foreach ( $dadosCampo as $dadosItemImportacao )
			{
				if ( $dadosItemImportacao['destino'] != '' )
				{
					$novoItem = array(
						'inicio' => (integer) $ponteiro,
						'tamanho' => (integer) $dadosItemImportacao['tamanho'],
						'destino' => $dadosItemImportacao['destino'],
						'agrupador' => isset( $dadosItemImportacao['agrupador'] ) ? 1 : 0
					);
					array_push( $traducao, $novoItem );
				}
				$ponteiro += $dadosItemImportacao['tamanho'];
			}
		}
		
		// remove todas as entradas para o tipo especificado
		$sql = "DELETE FROM xxx WHERE tipo = '" . $tipo . "'";
		dbg( $sql );
		//$this->db->executar( $sql );
		$sql = "SELE FROM yyy WHERE tipo = '" . $tipo . "'";
		dbg( $sql );
		//$this->db->executar( $sql );
		
		// insere as novas entradas
		$sql = "INSERT INTO xxx ( tabela, tipo ) VALUES ( '" . $tabela . "', '" . $tipo . "' )";
		dbg( $sql );
		//$this->db->executar( $sql );
		foreach ( $traducao as $item )
		{
			$sql = "INSERT INTO xxx ( tipo, inicio, tamanho, destino, agrupador ) VALUES ( '" . $tipo . "', " . $item['inicio'] . ", " . $item['tamanho'] . ", '" . $item['destino'] . "', " . $item['agrupador'] . " )";
			dbg( $sql );
			//$this->db->executar( $sql );
		}
	}
	
}

$siaf = new ImportacaoSiaf();
//$siaf->importarArquivo( 'DOCA_NC_20060919.txt' );
//exit();

$parametros = $_REQUEST['campo'];
tratarInput( $parametros );
if ( $parametros )
{
	$siaf->salvarParametros( $_REQUEST['tipoOrigem'], $_REQUEST['tabelaDestino'], $parametros );
	dbg( 1, 1 );
}

$tipoOrigem = array(
	'NC' => $siaf->pegarCampos( 'NC' ),
	'ND' => $siaf->pegarCampos( 'ND' ),
	'NE' => $siaf->pegarCampos( 'NE' ),
	'NL' => $siaf->pegarCampos( 'NL' )
);

//var_dump( $tipoOrigem, 1 );

$tabela = array();
foreach ( $db->pegarTabelas( 'financeiro' ) as $nomeTabela )
{
	$tabela[$nomeTabela] = $db->pegarColunas( $nomeTabela, 'financeiro' );
}

//	dbg( $tabela, 1 );

?>
<html>
	<head>
		
		<script type="text/javascript">
			var aux = null;
			var idDisponivel = 0; // inteiro para indicar indices no formulario
		</script>
		
		<!-- DEFINE TIPOS PARA IMPORTACAO -->
		<script type="text/javascript">
			var tipoOrigem = new Array();
			<? foreach ( $tipoOrigem as $nomeTipo => $camposDados ) : ?>
				tipoOrigem['<?= $nomeTipo ?>'] = new Array();
				<? foreach ( $camposDados as $nomeCampo => $campoDados ) : ?>
					aux = new Array();
					aux['nome'] = '<?= $nomeCampo ?>';
					aux['tamanho'] = '<?= $campoDados['tamanho'] ?>';
					tipoOrigem['<?= $nomeTipo ?>'].push( aux );
				<? endforeach; ?>
			<? endforeach; ?>
		</script>
		<!-- FIM DEFINE TIPOS PARA IMPORTACAO -->
		
		<!-- DEFINE TABELAS -->
		<script type="text/javascript">
			var tabela = new Array();
			<? foreach ( $tabela as $nomeTabela => $camposTabela ) : ?>
				tabela['<?= $nomeTabela ?>'] = new Array();
				<? foreach ( $camposTabela as $nomeCampo ) : ?>
					tabela['<?= $nomeTabela ?>'].push( '<?= $nomeCampo ?>' );
				<? endforeach; ?>
			<? endforeach; ?>
		</script>
		<!-- FIM DEFINE TABELAS -->
		
		<script type="text/javascript">
			
			function alterarDestino()
			{
				var tabelaDestino = document.getElementById( 'tabelaDestino' ).value;
				var select = null;
				
				// atualiza combos
				var j, k, i = 0;
				while ( select = document.getElementById( 'selectDestino_' + ( i++ ) ) )
				{
					// remove as op��es de todos os selects
					while ( select.options.length > 0 )
					{
						select.options[0] = null;
					}
					if ( tabelaDestino == '' )
					{
						continue;
					}
					// adicona as op��es
					k = tabela[tabelaDestino].length;
					select.options[0] = new Option( '', '', false, false );
					for ( j = 0; j < k; j++ )
					{
						select.options[select.options.length] = new Option( tabela[tabelaDestino][j], tabela[tabelaDestino][j], false, false );
					}
				}
				// FIM atualiza combos
			}
			
			
			function adicionarCampoOrigem( idTr, atualizarCombosDestino )
			{
				var tabelaTraducao = document.getElementById( 'tabelaTraducao' );
				var i, j = tabelaTraducao.rows.length;
				var tr = null;
				for ( i = 0; i < j; i++ )
				{
					if ( tabelaTraducao.rows[i].id == idTr )
					{
						tr = tabelaTraducao.rows[i];
						break;
					}
				}
				if ( tr == null )
				{
					return;
				}
				
				var tipo = tr.cells[0].innerHTML.substr( 0, tr.cells[0].innerHTML.indexOf( ' ' ) );
				var tamanho = tr.cells[2].innerHTML;
				
				var celAgrupador = tr.cells[1];
				var celTamanho = tr.cells[3];
				var celDestino = tr.cells[4];
				
				var input = document.createElement( 'input' );
				input.type = 'checkbox';
				input.name = 'campo[' + tipo + '][' + idDisponivel + '][agrupador]';
				input.value = '1';
				input.checked = false;
				if ( celAgrupador.innerHTML )
				{
					celAgrupador.appendChild( document.createElement( 'br' ) );
				}
				celAgrupador.appendChild( input );
				
				var input = document.createElement( 'input' );
				input.type = 'text';
				input.size = '3';
				input.name = 'campo[' + tipo + '][' + idDisponivel + '][tamanho]';
				input.value = tamanho;
				input.checked = false;
				celTamanho.appendChild( input );
				
				var select = document.createElement( 'select' );
				select.id = 'selectDestino_' + idDisponivel;
				select.name = 'campo[' + tipo + '][' + idDisponivel + '][destino]';
				if ( celDestino.innerHTML )
				{
					celDestino.appendChild( document.createElement( 'br' ) );
				}
				celDestino.appendChild( select );
				
				if ( atualizarCombosDestino == true )
				{
					alterarDestino();
				}
				
				idDisponivel++;
			}
			
			function alterarTipoOrigem()
			{
				idDisponivel = 0;
				
				var tabelaTraducao = document.getElementById( 'tabelaTraducao' );
				var tipo = document.getElementById( 'tipoOrigem' ).value;
				
				// remove linhas da tabela
				while ( tabelaTraducao.rows.length > 2 )
				{
					tabelaTraducao.rows[2].parentNode.removeChild( tabelaTraducao.rows[2] );
				}
				// FIM remove linhas da tabela
				
				if ( tipo == '' )
				{
					return;
				}
				
				var i, linha, link, celOrigem, celAgrupador, celTamanho, celDestino, input, select, j = tipoOrigem[tipo].length;
				for ( i = 0; i < j; i++ )
				{
					//adicionarCampoOrigem( tipoOrigem[tipo][i]['nome'], tipoOrigem[tipo][i]['tamanho'], i + 2, true );
					// cria linha
					linha = tabelaTraducao.insertRow( tabelaTraducao.rows.length );
					linha.id = 'idTr_' + i;
					
					// cria celula que exibe nome da campo de origem
					celOrigem = linha.insertCell( 0 );
					celOrigem.appendChild( document.createTextNode( tipoOrigem[tipo][i]['nome'] + ' ' ) );
					link = document.createElement( 'a' );
					link.href = 'javascript:adicionarCampoOrigem( \'' + linha.id + '\', true );';
					link.appendChild( document.createTextNode( 'separar' ) );
					celOrigem.appendChild( link );
					
					// cria celula que exibe checkbox indicando se arquivo � ou n�o agrupador
					celAgrupador = linha.insertCell( 1 );
					celAgrupador.style.textAlign = 'center';
					
					// cria celula que exibe tamanho original do campo
					celTamanhoOriginal = celAgrupador = linha.insertCell( 2 );
					celTamanhoOriginal.style.textAlign = 'center';
					celTamanhoOriginal.appendChild( document.createTextNode( tipoOrigem[tipo][i]['tamanho'] ) );
					
					// cria celula que exibe tamanho do campo a ser lido
					celTamanho = celAgrupador = linha.insertCell( 3 );
					celTamanho.style.textAlign = 'center';
					
					// cria celula que exibe o destino do campo origem
					celDestino = linha.insertCell( 4 );
					celDestino.style.textAlign = 'center';
					
					adicionarCampoOrigem( linha.id, false );
				}
				
				// atualiza lista de campos destino nos combos
				alterarDestino();
			}
			
		</script>
		
		<style>
			*{ font-size: 8pt; }
		</style>
		
		<title>Importa��o SIOF</title>
	</head>
	<body>
		<form action="" method="post">
			<table width="620" id="tabelaTraducao" align="center" border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">ORIGEM</td>
					<td align="center">AG.</td>
					<td align="center" colspan="2">TAMANHO</td>
					<td align="center">PARA</td>
				</tr>
				<tr>
					<td width="300" align="center">
						<select name="tipoOrigem" id="tipoOrigem" onchange="alterarTipoOrigem();">
							<option value=""></option>
							<? foreach ( array_keys( $tipoOrigem ) as $nomeTipo ) : ?>
								<option value="<?= $nomeTipo ?>"><?= $nomeTipo ?></option>
							<? endforeach; ?>
						</select>
					</td>
					<td width="50">&nbsp;</td>
					<td width="30">&nbsp;</td>
					<td width="40">&nbsp;</td>
					<td width="200" align="center">
						<select name="tabelaDestino" id="tabelaDestino" onchange="alterarDestino();">
							<option value=""></option>
							<? foreach ( array_keys( $tabela ) as $nomeTabela ) : ?>
								<option value="<?= $nomeTabela ?>"><?= $nomeTabela ?></option>
							<? endforeach; ?>
						</select>
					</td>
				</tr>
				
			</table>
			<center>
				<br/>
				<input type="submit" name="enviar" value="enviar"/>
			</center>
		</form>
	</body>
</html>











