<?php
define("WORKFLOW_SOLICITACAO_SERVICO", 26);
define("WORKFLOW_ORDEM_SERVICO", 	   27);
define("WORKFLOW_CONTAGEM_PF", 	   38);

define("WF_ESTADO_ELABORACAO",   250); //Em Revis�o / Requisitante
define("WF_ESTADO_PRE_ANALISE",  551); 
define("WF_ESTADO_ANALISE",      246);
define("WF_ESTADO_DETALHAMENTO", 247);
define("WF_ESTADO_AVALIACAO", 	 248);
define("WF_ESTADO_APROVACAO", 	 249);
define("WF_ESTADO_EXECUCAO", 	 252);
define("WF_ESTADO_FINALIZADA", 	 253);
define("WF_ESTADO_CANCELADA_SEM_CUSTO", 	 251);
define("WF_ESTADO_CANCELADA_COM_CUSTO", 	 300);
define("WF_ESTADO_AGUARDANDO_PAGAMENTO", 	 361);

define("WF_ESTADO_OS_PENDENTE",  254);
define("WF_ESTADO_OS_EXECUCAO",   255);
define("WF_ESTADO_OS_VERIFICACAO",  256); //Em Avalia��o 
define("WF_ESTADO_OS_FINALIZADA", 257);
define("WF_ESTADO_OS_ATESTO_TECNICO", 263);
define("WF_ESTADO_OS_APROVACAO", 264);
define("WF_ESTADO_OS_CANCELADA_SEM_CUSTO", 	301);
define("WF_ESTADO_OS_CANCELADA_COM_CUSTO", 	302);
define("WF_ESTADO_OS_PAUSA", 				511);
define("WF_ESTADO_OS_AGUARDANDO_PAGAMENTO", 512);
define("WF_ESTADO_OS_DIVERGENCIA", 			513);

define("WF_ESTADO_SS_PAUSA", 				515);

define("WF_ACAO_SOL_EXECUCAOFINAL", 669);
define("WF_ACAO_OS_EMEXECUCAO", 658);
define("WF_ACAO_OS_CANCELA_SEM_CUSTO", 760);

define("WF_ESTADO_CPF_PENDENTE",			  272);
define("WF_ESTADO_CPF_AGUARDANDO_CONTAGEM",   273);
define("WF_ESTADO_CPF_AVALIACAO",  			  274);
define("WF_ESTADO_CPF_APROVACAO",  			  275);
define("WF_ESTADO_CPF_REVISAO",  			  276);
define("WF_ESTADO_CPF_FINALIZADA",  		  277);
define("WF_ESTADO_CPF_CANCELADA",  			  303);
define("WF_ESTADO_CPF_AGUARDANDO_PAGAMENTO",  371);
define("WF_ESTADO_CPF_DIVERGENCIA", 		  514);
//define("WF_ESTADO_CPF_PAUSA", 				  483);

define("TPANEXO_SOLICITACAO_OUTROS", 1);
define("TPANEXO_RELATORIO_PF", 26);
define("TPANEXO_RELATORIO_PF_ESTIMADA", 28);
define("TPANEXO_RELATORIO_PF_DETALHADA", 29);

define( "TIPO_EMPRESA_FABRICA" , 78 );
define( "TIPO_GESTOR_CONTRATO" , 79 );

define("TPS_PF", 6);

define("SISID_FABRICA", 87);
define("SISID_DEMANDAS", 44);

define("PERFIL_SUPER_USUARIO", 494);

//Perfil
define("PERFIL_CONTAGEM_PF",            504); //PREPOSTO_EFICACIA
define("PERFIL_ADMINISTRADOR",          586);
define("PERFIL_REQUISITANTE",           496);
define("PERFIL_FISCAL_CONTRATO",        497);
define("PERFIL_GESTOR_CONTRATO",        499);
define("PERFIL_PREPOSTO",               500); //SQUADRA
define("PERFIL_ESPECIALISTA_SQUADRA",   555);
define("PERFIL_CONSULTA_GERAL",         608);
define("PERFIL_GERENTE_PROJETO",        771);

//Tipos de OS
define("TIPO_OS_GERAL", 1);
define("TIPO_OS_CONTAGEM_ESTIMADA", 2);
define("TIPO_OS_CONTAGEM_DETALHADA", 3);

//situa��o solicita��o no workflow.tipodocumento - tpdid
define("TIPO_SITUACAO_SOLICITACAO", 26);

//envia email
define('REMETENTE_WORKFLOW_EMAIL', 'fabrica@mec.gov.br');
define('REMETENTE_WORKFLOW_NOME',  'M�dulo F�brica');

/***********************
 * WORKFLOW DEMANDAS
 ***********************/
define('DEMANDA_WORKFLOW_GENERICO',   35);
define("WF_DEMANDA_ACAO_ANALISE_PARA_ATENDIMENTO", 184);
define("WF_DEMANDA_ACAO_ANALISE_PARA_CANCELADO", 189);
define("WF_DEMANDA_ACAO_ATENDIMENTO_PARA_CANCELADO", 491);

// Termo
define("TIPO_TERMO_RECEBIMENTO_PROVISORIO", 7);//"Termo Recebimento Provis�rio"
define("TIPO_TERMO_SOLICITACAO_SERVICO", 8);//"Termo Solicita��o Servi�o"
