<?php 

$_REQUEST['baselogin'] = "simec_espelho_producao";

// carrega as fun��es gerais
include_once "config.inc";
include_once APPRAIZ . "includes/funcoes.inc";
include_once APPRAIZ . "includes/classes_simec.inc";

/* configura��es do relatorio - Memoria limite de 1024 Mbytes */
ini_set("memory_limit", "1024M");
set_time_limit(0);
/* FIM configura��es - Memoria limite de 1024 Mbytes */

$db = new cls_banco();

if($_GET['entidEscola']){
	header('content-type:text/xml; charset=ISO-8859-1');
	$arrEntid = array(6002,6004,6005,6009,6007,6008);
	
	$sql = "SELECT 
				CASE WHEN split_part(medlatitude, '.', 4) <>'N' THEN
					(((split_part(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int)))*(-1)
				ELSE
					((SPLIT_PART(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int))
				END as latitude,
               ((split_part(medlongitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlongitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlongitude, '.', 1)::int))*(-1) as longitude

            FROM 
            	entidade.endereco en
            WHERE 
            	entid='".$_GET['entidEscola']."'";
	
	$dadosEscola = $db->pegaLinha($sql);
	
	$sql = "select
				mpcnome,
				CASE WHEN (medlatitude is not null AND medlatitude <> '...S' AND medlatitude <>'00000000') THEN 
					(CASE WHEN split_part(medlatitude, '.', 4) <>'N' THEN
						(((split_part(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int)))*(-1)
					 ELSE
					 	((SPLIT_PART(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int))
					 END
	            END as latitude,
				CASE WHEN (medlongitude is not null AND medlongitude <> '...W' AND medlongitude <> '..' ) THEN 
						(CASE WHEN (SPLIT_PART(medlongitude, '.', 1) <>'' AND SPLIT_PART(medlongitude, '.', 2) <>'' AND split_part(medlongitude, '.', 3) <>'') THEN
						               ((split_part(medlongitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlongitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlongitude, '.', 1)::int))*(-1)
						 END) 
				END as longitude,
				mun.mundescricao,
				ed.estuf
			FROM
				pdeescola.mepontocultura e
			LEFT JOIN 
				entidade.endereco ed ON e.entid = ed.entid
			LEFT JOIN 
				territorios.municipio mun ON ed.muncod = mun.muncod";

	$dados = $db->carregar($sql);
	
	if($dados):
			
			$conteudo .= "<markers> "; // inicia o XML
			
			foreach($dados as $d):
											
				$conteudo .= "<marker "; //inicia um ponto no mapa
				$conteudo .= "entnome=\"". $d['entnome'] ."\" "; // adiciona o nome da institui��o;
				$conteudo .= "mundsc=\"". $d['mundescricao'] ."\" "; // adiciona a descri��o do munic�pio;
				$conteudo .= "estuf=\"". $d['estuf'] ."\" "; // adiciona UF;
				$conteudo .= "lat='{$d['latitude']}' "; // adiciona a latitude;
				$conteudo .= "lng='{$d['longitude']}' "; //adiciona a longitude;
				$conteudo .= "/> ";
			
			endforeach;
			
			$conteudo .= "</markers> ";
			print $conteudo;
			
		endif;
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="br" lang="br">
<head>
<title>Google Maps</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<?php
//Pega endere�o para marca��o de ponto
if($_GET['entid']){
	
	$sql = "select
				entnome,
				CASE 
				WHEN (medlatitude is not null AND medlatitude <> '...S') THEN 
					( 	 --############### LATITUDE ###################### --
								CASE WHEN (SPLIT_PART(medlatitude, '.', 1) <>'' AND SPLIT_PART(medlatitude, '.', 2) <>'' AND split_part(medlatitude, '.', 3) <>'') THEN
					               CASE WHEN split_part(medlatitude, '.', 4) <>'N' THEN
					                   (((split_part(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int)))*(-1)
					                ELSE
					                   ((SPLIT_PART(medlatitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlatitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlatitude, '.', 1)::int))
					               END
					            ELSE
					            -- Valores do IBGE convertidos em  decimal
					            CASE WHEN (length (medlatitude)=8) THEN
					                CASE WHEN length(REPLACE('0' || medlatitude,'S','')) = 8 THEN
					                    ((SUBSTR(REPLACE('0' || medlatitude,'S',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE('0' || medlatitude,'S',''),3,2)::double precision/60)+(SUBSTR(REPLACE('0' || medlatitude,'S',''),1,2)::double precision))*(-1)
					                ELSE
					                    (SUBSTR(REPLACE('0' || medlatitude,'N',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE('0' || medlatitude,'N',''),3,2)::double precision/60)+(SUBSTR(REPLACE('0' || medlatitude,'N',''),1,2)::double precision)
					                END
					            ELSE
					                CASE WHEN length(REPLACE(medlatitude,'S','')) = 8 THEN
					                   ((SUBSTR(REPLACE(medlatitude,'S',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE(medlatitude,'S',''),3,2)::double precision/60)+(SUBSTR(REPLACE(medlatitude,'S',''),1,2)::double precision))*(-1)
					                ELSE
					                  0--((SUBSTR(REPLACE(medlatitude,'N',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE(medlatitude,'N',''),3,2)::double precision/60)+(SUBSTR(REPLACE(medlatitude,'N',''),1,2)::double precision))
					                END
					            END 
					            END
					       --############### LATITUDE ###################### --
					  ) 
				ELSE (
						 --############### LATITUDE ###################### --
								CASE WHEN (SPLIT_PART(munmedlat, '.', 1) <>'' AND SPLIT_PART(munmedlat, '.', 2) <>'' AND split_part(munmedlat, '.', 3) <>'') THEN
					               CASE WHEN split_part(munmedlat, '.', 4) <>'N' THEN
					                   (((split_part(munmedlat, '.', 3)::double precision / 3600) +(SPLIT_PART(munmedlat, '.', 2)::double precision / 60) + (SPLIT_PART(munmedlat, '.', 1)::int)))*(-1)
					                ELSE
					                   ((SPLIT_PART(munmedlat, '.', 3)::double precision / 3600) +(SPLIT_PART(munmedlat, '.', 2)::double precision / 60) + (SPLIT_PART(munmedlat, '.', 1)::int))
					               END
					            ELSE
					            -- Valores do IBGE convertidos em  decimal
					            CASE WHEN (length (munmedlat)=8) THEN
					                CASE WHEN length(REPLACE('0' || munmedlat,'S','')) = 8 THEN
					                    ((SUBSTR(REPLACE('0' || munmedlat,'S',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE('0' || munmedlat,'S',''),3,2)::double precision/60)+(SUBSTR(REPLACE('0' || munmedlat,'S',''),1,2)::double precision))*(-1)
					                ELSE
					                    (SUBSTR(REPLACE('0' || munmedlat,'N',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE('0' || munmedlat,'N',''),3,2)::double precision/60)+(SUBSTR(REPLACE('0' || munmedlat,'N',''),1,2)::double precision)
					                END
					            ELSE
					                CASE WHEN length(REPLACE(munmedlat,'S','')) = 8 THEN
					                   ((SUBSTR(REPLACE(munmedlat,'S',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE(munmedlat,'S',''),3,2)::double precision/60)+(SUBSTR(REPLACE(munmedlat,'S',''),1,2)::double precision))*(-1)
					                ELSE
					                  0--((SUBSTR(REPLACE(munmedlat,'N',''),5,4)::double precision/3600000)+(SUBSTR(REPLACE(munmedlat,'N',''),3,2)::double precision/60)+(SUBSTR(REPLACE(munmedlat,'N',''),1,2)::double precision))
					                END
					            END
					            END
					         --############### LATITUDE ###################### --
				     )
				END as latitude,
				CASE 
					WHEN (medlongitude is not null AND medlongitude <> '...W' AND medlongitude <> '..' ) THEN 
						( 	 --############### LONGITUDE ###################### --
						            CASE WHEN (SPLIT_PART(medlongitude, '.', 1) <>'' AND SPLIT_PART(medlongitude, '.', 2) <>'' AND split_part(medlongitude, '.', 3) <>'') THEN
						               ((split_part(medlongitude, '.', 3)::double precision / 3600) +(SPLIT_PART(medlongitude, '.', 2)::double precision / 60) + (SPLIT_PART(medlongitude, '.', 1)::int))*(-1)
						            ELSE
						                -- Valores do IBGE convertidos em  decimal
						               (SUBSTR(REPLACE(medlongitude,'W',''),1,2)::double precision + (SUBSTR(REPLACE(medlongitude,'W',''),3,2)::double precision/60)) *(-1)
						            END
						         --############### FIM LONGITUDE ###################### --
						  ) 
					ELSE (
							 --############### LONGITUDE ###################### --
						            CASE WHEN (SPLIT_PART(munmedlog, '.', 1) <>'' AND SPLIT_PART(munmedlog, '.', 2) <>'' AND split_part(munmedlog, '.', 3) <>'') THEN
						               ((split_part(munmedlog, '.', 3)::double precision / 3600) +(SPLIT_PART(munmedlog, '.', 2)::double precision / 60) + (SPLIT_PART(munmedlog, '.', 1)::int))*(-1)
						            ELSE
						                -- Valores do IBGE convertidos em  decimal
						               (SUBSTR(REPLACE(munmedlog,'W',''),1,2)::double precision + (SUBSTR(REPLACE(munmedlog,'W',''),3,2)::double precision/60)) *(-1)
						            END
						         --############### FIM LONGITUDE ###################### --
					     )
				END as longitude,
				mun.mundescricao,
				ed.estuf
			FROM
				entidade.entidade e
			LEFT JOIN 
				entidade.endereco ed ON e.entid = ed.entid
			LEFT JOIN 
				territorios.municipio mun ON ed.muncod = mun.muncod
			WHERE
				e.entid = {$_GET['entid']}
			AND
				e.entstatus = 'A'";

$dados = $db->pegaLinha($sql);
	
}

function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

$local= explode("/",curPageURL());?>
<?if ($local[2]=="simec.mec.gov.br" ){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxQhVwj8ALbvbyVgNcB-R-H_S2MIRxTIdhrqjcwTK3xxl_Nu_YMC5SdLWg" type="text/javascript"></script>
	<? $Gkey = "ABQIAAAAwN0kvNsueYw8CBs704pusxQhVwj8ALbvbyVgNcB-R-H_S2MIRxTIdhrqjcwTK3xxl_Nu_YMC5SdLWg"; ?>
<? } ?>
<?if ($local[2]=="simec-d.mec.gov.br"){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxRYtD8tuHxswJ_J7IRZlgTxP-EUtxT_Cz5IMSBe6d3M1dq-XAJNIvMcpg" type="text/javascript"></script>
	<? $Gkey = "ABQIAAAAwN0kvNsueYw8CBs704pusxRYtD8tuHxswJ_J7IRZlgTxP-EUtxT_Cz5IMSBe6d3M1dq-XAJNIvMcpg"; ?> 
<? } ?>
<?if ($local[2]=="simec" ){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxTNzTBk8zukZFuO3BxF29LAEN1D1xSIcGWxF7HCjMwks0HURg6MTfdk1A" type="text/javascript"></script>
	<? $Gkey = "ABQIAAAAwN0kvNsueYw8CBs704pusxTNzTBk8zukZFuO3BxF29LAEN1D1xSIcGWxF7HCjMwks0HURg6MTfdk1A"; ?>
<? } ?>
<?if ($local[2]=="simec-d"){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxTFm3qU4CVFuo3gZaqihEzC-0jfaRTY9Fe8UfzYeoYDxtThvI3nGbbZEw" type="text/javascript"></script>
	<? $Gkey = "ABQIAAAAwN0kvNsueYw8CBs704pusxTFm3qU4CVFuo3gZaqihEzC-0jfaRTY9Fe8UfzYeoYDxtThvI3nGbbZEw"; ?> 
<? } ?>
<?if ($local[2]=="simec-local"){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxRzjpIxsx3o6RYEdxEmCzeJMTc4zBSMifny_dJtMKLfrwCcYh5B01Pq_g" type="text/javascript"></script>
	<? $Gkey = "ABQIAAAAwN0kvNsueYw8CBs704pusxRzjpIxsx3o6RYEdxEmCzeJMTc4zBSMifny_dJtMKLfrwCcYh5B01Pq_g"; ?> 	
<? } ?>
<?if ($local[2]=="painel.mec.gov.br"){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAwN0kvNsueYw8CBs704pusxTPkFYZwQy2nvpGvFj08HQmPOt9ZBT2EJmQsTms0WQqU_5GvEj7bMZd7g" type="text/javascript"></script>
	<? $GKey = "ABQIAAAAwN0kvNsueYw8CBs704pusxTPkFYZwQy2nvpGvFj08HQmPOt9ZBT2EJmQsTms0WQqU_5GvEj7bMZd7g"; ?> 	
<? } ?>
<script type="text/javascript">

var markerGroups = { '1': [], '2': []};

function initialize() {
	if (GBrowserIsCompatible()) { // verifica se o navegador � compat�vel
			map = new GMap2(document.getElementById("google_map")); // inicila com a div mapa
			var zoom = 4;	var lat_i = -14.689881; var lng_i = -52.373047;	//Brasil	
			map.setCenter(new GLatLng(lat_i,lng_i), parseInt(zoom)); //Centraliza e aplica o zoom

			
			// In�cio Controles
			map.addControl(new GMapTypeControl());
			map.addControl(new GLargeMapControl3D());
	        map.addControl(new GOverviewMapControl());
	        map.enableScrollWheelZoom();
	        map.addMapType(G_PHYSICAL_MAP);
	        // Fim Controles
	
	}
}

function criaPontoEscola(){
	html = "<div style=\"font-family:verdana;font-size:11px;padding:10px\" >";
	html += "<b>Institui��o:</b> <?php echo $dados['entnome']?><br />";
	html += "<b>Localiza��o:</b> <?php echo $dados['mundescricao']?>/<?php echo $dados['estuf']?><br /><br />";
	html += "</div>";
	
	var title = '<?php echo $dados['entnome']?>';
	
	var baseIcon1 = new GIcon();
	baseIcon1.iconSize = new GSize(15, 17);
	baseIcon1.iconAnchor = new GPoint(11, 13);
	baseIcon1.infoWindowAnchor = new GPoint(9, 2);
	baseIcon1.image='/imagens/icones/tr.png';
					
	var point = new GLatLng(<?php echo $dados['latitude']?>,<?php echo $dados['longitude']?>);				
	
	// Cria o marcador na tela
	var marker = createMarker(point,title,baseIcon1,html);
	
	markerGroups['1'].push(marker);
	
	map.addOverlay(marker);
	
	map.setCenter(new GLatLng(<?php echo $dados['latitude']?>,<?php echo $dados['longitude']?>), 5); //Centraliza e aplica o zoom
	

}

function criaPontosCulturais(xml_filtro){	

	var baseIcon2 = new GIcon();
	baseIcon2.iconSize = new GSize(15, 17);
	baseIcon2.iconAnchor = new GPoint(11, 13);
	baseIcon2.infoWindowAnchor = new GPoint(9, 2);
	baseIcon2.image='/imagens/icones/tb.png';
		
	// Criando os Marcadores com o resultado
	GDownloadUrl(xml_filtro, function(data) {
		var xml = GXml.parse(data);
		
		var markers = xml.documentElement.getElementsByTagName("marker");
		
		if(markers.length > 0) {
			var lat_ant=0;
			var lng_ant=0;
				
			for (var i = 0; i < markers.length; i++) {
						
				var entnome = markers[i].getAttribute("entnome");
				var mundsc = markers[i].getAttribute("mundsc");
				var estuf = markers[i].getAttribute("estuf");
				title = entnome; 
										
				icon = baseIcon2;
				
				var lat = markers[i].getAttribute("lat");
				var lng = markers[i].getAttribute("lng");
				
				var html;
						
				html = "<div style=\"font-family:verdana;font-size:11px;padding:10px\" >";
				html += "<b>Institui��o:</b> " + entnome + "<br />";
				html += "<b>Localiza��o:</b> " + mundsc + "/" + estuf + "<br /><br />";
				html += "</div>";

				// Verifica pontos em um mesmo lugar e move o seguinte para a direita
				if(lat_ant==markers[i].getAttribute("lat") && lng_ant==markers[i].getAttribute("lng"))
					var point = new GLatLng(markers[i].getAttribute("lat"),	markers[i].getAttribute("lng"));
				else
					var point = new GLatLng(markers[i].getAttribute("lat"),	parseFloat(markers[i].getAttribute("lng"))+0.0005);				
	
				lat_ant=markers[i].getAttribute("lat");
				lng_ant=markers[i].getAttribute("lng");
				
				// Cria o marcador na tela
				var marker = createMarker(point,title,icon,html);
				
				markerGroups['2'].push(marker);
				
				map.addOverlay(marker);
				
		}
		}else{
			alert("N�o existem pontos culturais pr�ximos!")
		}
	});
}

function createMarker(posn, title, icon, html,muncod) {
	var marker = new GMarker(posn, {title: title, icon: icon, draggable:false });
	if(html != false){
		GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(html);
		});
	}
	return marker;
}

function exibePontos(obj){
	
	if(obj.checked == true){
		if(markerGroups[obj.value]){
			for (var i = 0; i < markerGroups[obj.value].length; i++) {
		        var marker = markerGroups[obj.value][i];
		        marker.show();
			}
		}	
	}else{
		if(markerGroups[obj.value]){
			for (var i = 0; i < markerGroups[obj.value].length; i++) {
		        var marker = markerGroups[obj.value][i];
		        marker.hide();
			}
		}
	}	
		

}

function getLatLng(address,endid) {
	geocoder = new GClientGeocoder();
	if (geocoder) {
		geocoder.getLatLng(
		address,
		function(point) {
			if (!point) {
			//alert(address + " Endere�o n�o encontrado!");
			//document.getElementById("endid_" + destino).innerHTML
			} else {
				document.getElementById("update").innerHTML += "UPDATE entidade.endereco set medlatitude = '" + formatoEnderecoCoordenada(point.lat(),'lat') + "', medlongitude = '" + formatoEnderecoCoordenada(point.lng(),'lng') + "' where endid = " + endid + ";<br>";
				//document.getElementById("endid_" + destino).innerHTML = point.lat().toFixed(5);
				//document.getElementById("lng").innerHTML = point.lng().toFixed(5);
//				map.clearOverlays()
//				map.setCenter(point, 14);
//				var marker = new GMarker(point, {draggable: true});  
//				map.addOverlay(marker);
//				GEvent.addListener(marker, "dragend", function() {
//					var pt = marker.getPoint();
//					map.panTo(pt);
//					document.getElementById("lat").innerHTML = pt.lat().toFixed(5);
//					document.getElementById("lng").innerHTML = pt.lng().toFixed(5);
//				});
			
//				GEvent.addListener(map, "moveend", function() {
//					map.clearOverlays();
//					var center = map.getCenter();
//					var marker = new GMarker(center, {draggable: true});
//					map.addOverlay(marker);
//					document.getElementById("lat").innerHTML = center.lat().toFixed(5);
//					document.getElementById("lng").innerHTML = center.lng().toFixed(5);
//
//					GEvent.addListener(marker, "dragend", function() {
//						var pt = marker.getPoint();
//						map.panTo(pt);
//						document.getElementById("lat").innerHTML = pt.lat().toFixed(5);
//						document.getElementById("lng").innerHTML = pt.lng().toFixed(5);
//					});
// 
//				});
			}
		}
		);
	}
}


function formatoEnderecoCoordenada(coordenada, tipocoordenada) {
	
	var dmsCorDeg='';
	var dmsCorMin='';
	var dmsCorSec='';
	var dmsCorPolo='';
	var dmsCorMinVals='';
	var ddCorVal='';
	
	if(coordenada.toString().substr(0,1) == "-") {
		ddCorVal = coordenada.toString().substr(1,coordenada.toString().length-1);
	} else {
		ddCorVal = coordenada.toString();
	}
		
	// Graus Lat 
	ddCorVals = ddCorVal.split(".");
	if(ddCorVals[0]) {
		dmsCorDeg = ddCorVals[0];
	}
	
	// * 60 = mins
	if(ddCorVals[1]) {
		ddCorRemainder  = ("0." + ddCorVals[1]) * 60;
		dmsCorMinVals   = ddCorRemainder.toString().split(".");
		dmsCorMin = dmsCorMinVals[0];
	}
	
	// * 60 novamente = secs
	if(dmsCorMinVals[1]) {
		ddCorMinRemainder = ("0." + dmsCorMinVals[1]) * 60;
		dmsCorSec  = Math.round(ddCorMinRemainder);
	}
		
	if (coordenada.toString().substr(0,1) == "-") {
		if(tipocoordenada == 'lat') {
			dmsCorPolo = "S";
		} else {
			dmsCorPolo = "W";		
		}
	} else {
		if(tipocoordenada == 'lat') {
			dmsCorPolo = "N";
		} else {
			dmsCorPolo = "E";		
		}
	}
	
	return dmsCorDeg+'.'+dmsCorMin+'.'+dmsCorSec+'.'+dmsCorPolo;

}


</script>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css">
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'>

</head>
<body>
<div id="update"></div>
<?php 

$sql = "select 
	endcep, 
	endlog,
	e.endid,
	mun.mundescricao,
	e.estuf
	from 
		entidade.endereco e 
	inner join 
		pdeescola.mepontobiblioteca me ON me.endid = e.endid 
	left join
		territorios.municipio mun ON e.muncod = mun.muncod  
	where 
		(medlatitude is null or medlatitude = '...S' or medlatitude = '00000000')
	and 
		(medlongitude is null or medlongitude = '...W' or medlongitude = '00000000')
	and
		--(endlog is not null or endcep is not null)
		(CHAR_LENGTH(endlog) > 5 or CHAR_LENGTH(endcep) > 5)
	LIMIT 1000 OFFSET 0";

/*
$sql = "select 
	endcep, 
	endlog,
	me.endid,
	mun.mundescricao,
	me.estuf
	from
		pdeescola.mepontocultura m 
	inner join 
		entidade.endereco me  ON me.endid = m.endid 
	left join
		territorios.municipio mun ON me.muncod = mun.muncod  
	where endcep is not null";
*/

$endereco = $db->carregar($sql);

dbg(count($endereco));

$endereco = !$endereco ? array() : $endereco;

$t=1;

echo "<script>";

foreach($endereco as $end){
	echo "getLatLng(\"".$end['endcep'].", ".$end['mundescricao'].", ".$end['estuf']."\",".$end['endid'].");";
	if($t==20) {
		sleep(5);
		$t=1;
	}
	
}

echo "</script>";
//die;
//include (APPRAIZ."includes/classes/GoogleMaps.class.inc");
//
//// Instancia a classe
//$gmaps = new gMaps($Gkey);
//
//// Pega os dados (latitude, longitude e zoom) do endere�o:
//$endereco = 'Av. Brasil, 1453, Rio de Janeiro, RJ';
//$dados = $gmaps->geolocal($endereco);
//
//// Exibe os dados encontrados:
//dbg($dados);
?>
<? die; ?>
<div id="lat">0</div>
<div id="lng">0</div>
<table width="100%" border="0" class="tabela" cellpadding="3" cellspacing="1">
	<tr>
		<td colspan="2" align='right' class="SubTituloEsquerda" ><? echo $dados['entnome'] ?></td>
	</tr>
	<tr>
		<td bgcolor="#F7F7F7" valign="top" style="width:120px"  >
			<input type="checkbox" checked="checked" name="ponto1" onclick="exibePontos(this)" value="1" id="chk_ponto1" /> <img src="/imagens/icones/tr.png" /> Escola <br />
			<input type="checkbox" checked="checked" name="ponto2" onclick="exibePontos(this)" value="2" id="chk_ponto2" /> <img src="/imagens/icones/tb.png" /> Pontos Culturais
		</td>
		<td valign="top"><div style="height:550px;width:100%"  id="google_map"></div></td>
	</tr>
	<tr>
		<td bgcolor="#DCDCDC" colspan="2" align='center' ><input type="button" name="fechar" value="Fechar" onclick="window.close();" /></td>
	</tr>
</table>

<script>
	initialize();
	criaPontoEscola();
	criaPontosCulturais("pdeescola.php?modulo=principal/mapaEntidade&acao=A&entidEscola=<? echo $_GET['entid']?>");
	//getLatLng('71535-255');
</script>
</body>
</html>