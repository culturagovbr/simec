<head>
    <title>Criatividade na Educa��o</title>

    <!-- BEGIN META -->
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="css/theme-default/bootstrap.css?1422792965" />
    <link type="text/css" rel="stylesheet" href="css/theme-default/materialadmin.css?1425466319" />
    <link type="text/css" rel="stylesheet" href="css/theme-default/font-awesome.min.css?1422529194" />
    <link type="text/css" rel="stylesheet" href="css/theme-default/libs/select2/select2.css" />
    <link type="text/css" rel="stylesheet" href="css/theme-default/material-design-iconic-font.min.css?1421434286" />
    <link type="text/css" rel="stylesheet" href="css/theme-default/libs/wizard/wizard.css?1425466601" />

    <link rel='stylesheet' href='css/style.css' type='text/css'>
    <!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/libs/utils/html5shiv.js?1403934957"></script>
    <script type="text/javascript" src="js/libs/utils/respond.min.js?1403934956"></script>
    <![endif]-->
    
    <script src="js/libs/jquery/jquery-1.11.2.min.js"></script>
</head>