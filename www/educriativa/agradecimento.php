<!DOCTYPE html>
<html lang="pt-BR">
<?php require "header.php"; ?>
<body class="menubar-hoverable header-fixed ">

	<!-- BEGIN HEADER-->
	<!-- barra do governo -->
	<div id="barra-brasil">
	  <a href="http://brasil.gov.br" class="barraGoverno">Portal do Governo Brasileiro</a>
	</div>
	<!-- fim barra do governo -->

	<div class="container">
	  <div id="topo">
	    <div class="row">
	      <div class="col-md-3 col-sm-6 col-xs-6 logo">
	      	<img src="img/logo.png" alt="">
	       </div>
	    </div> <!--  row -->
	  </div> <!--  topo -->
	</div> <!--  container -->
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
	
		<!-- BEGIN CONTENT-->
		<div id="content" class="section-body contain-lg shadow">
			<section>
				<!-- BEGIN VALIDATION FORM WIZARD -->
				<div class="row">
	                <div class="col-lg-12 col-sm-12 col-xs-12">
	                    <div class="alert alert-success" role="alert">
	                        <p>Envio do questionario efetuado com sucesso.</p>
	                        <p>Obrigado pela colaboração!</p>
	                    </div>
	                </div>
				</div><!--end .row -->
				<!-- END VALIDATION FORM WIZARD -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->

	</div><!--end #base-->
	<!-- END BASE -->

	<?php require_once "footer.php"; ?>

</body>
