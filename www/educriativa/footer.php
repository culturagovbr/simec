<!-- BEGIN JAVASCRIPT -->
<script src="js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="js/libs/bootstrap/bootstrap.min.js"></script>
<script src="js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="js/libs/spin.js/spin.min.js"></script>
<script src="js/libs/autosize/jquery.autosize.min.js"></script>
<script src="js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="js/libs/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="js/libs/jquery-validation/dist/additional-methods.min.js"></script>
<script src="js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="js/libs/bootbox/bootbox.js"></script>
<script src="js/core/source/App.js"></script>
<script src="js/core/source/AppNavigation.js"></script>
<script src="js/core/source/AppOffcanvas.js"></script>
<script src="js/core/source/AppCard.js"></script>
<script src="js/core/source/AppForm.js"></script>
<script src="js/core/source/AppNavSearch.js"></script>
<script src="js/core/source/AppVendor.js"></script>
<script src="js/core/demo/Demo.js"></script>
<script src="js/core/demo/DemoFormWizard.js"></script>
<script src="js/libs/select2/select2.js"></script>
<script src="js/core/demo/DemoFormComponents.js"></script>
<script src="js/scripts.js?t=1ytg65gy"></script>

<script src="http://static00.mec.gov.br/barragoverno/barra.js" type="text/javascript"></script>
<!-- END JAVASCRIPT -->