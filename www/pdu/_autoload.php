<?php

define( 'APPRAIZ_ZEND', APPRAIZ . 'includes/library/Zend/');

define('CLASSES_GERAL', APPRAIZ . "includes/classes/");
define('CLASSES_CONTROLE', APPRAIZ . 'pdu/classes/controllers/');
define('CLASSES_MODELO', APPRAIZ . 'pdu/classes/models/');
define('CLASSES_VISAO', APPRAIZ . 'pdu/classes/views/');
//define('CLASSES_VISAO'  , APPRAIZ . 'includes/classes/view/');
//define('CLASSES_HTML'  , APPRAIZ . 'includes/classes/html/');

set_include_path(
        CLASSES_GERAL . PATH_SEPARATOR .
        CLASSES_CONTROLE . PATH_SEPARATOR .
        CLASSES_MODELO . PATH_SEPARATOR .
        CLASSES_VISAO . PATH_SEPARATOR .
//                    CLASSES_HTML . PATH_SEPARATOR . 
        get_include_path()
);

function __autoload($class)
{

    require_once APPRAIZ . "includes/library/simec/funcoes.inc";
    require_once APPRAIZ . "includes/library/simec/abstract/Controller.php";
    require_once APPRAIZ . "includes/library/simec/abstract/Model.php";
    

    if (PHP_OS != "WINNT")
    { // Se "n�o for Windows"
        $separaDiretorio = ":";
        $include_path = get_include_path();
        $include_path_tokens = explode($separaDiretorio, $include_path);
    } else
    { // Se for Windows
        $separaDiretorio = ";c:";
        $include_path = get_include_path();
        
        $include_path = str_replace('.;', 'c:', strtolower($include_path));
        $include_path = str_replace('/', '\\', $include_path);
        
        $include_path_tokens = explode($separaDiretorio, $include_path);
        $include_path_tokens = str_replace("//", "/", $include_path_tokens);
//        $include_path_tokens[0] = explode(";", $include_path_tokens[0]);
        
        $include_path_tokens[0] = str_replace('c:', '', $include_path_tokens[0]);
        
//        ver(PHP_OS, $include_path_tokens, $include_path,d);
        
    }

    foreach ($include_path_tokens as $prefix) {
//        $file = pathinfo($prefix, PATHINFO_BASENAME); //end(explode('/',$prefix));
//        $file = ucfirst(substr($file, 0, -1));


        $file = array_pop(explode('_', $class));
//        
        $pathModule = $prefix . $file . '.php';
        if (file_exists($pathModule))
            require_once $pathModule;

        $path[0] = $prefix . $class . '.class.inc';
        $path[1] = $prefix . $class . '.php';

        foreach ($path as $thisPath) {
            if (file_exists($thisPath))
            {
                require_once $thisPath;
                return;
            }
        }
        
    }

    
}

urlAction();

/**
     * Metodo responsavel por pegar a action da controller e renderizar na tela.
     *
     * @name urlAction
     * @return void
     *
     * @throws Exception Controller n�o foi informada!
     * @throws Exception Action n�o foi informada!
     * @throws Exception Controller n�o existe!
     * @throws Exception Action n�o existe!
     *
     * @author Ruy Junior Ferreira Silva <ruy.silva@mec.gov.br>
     * @since 10/06/2013
     */
    function urlAction()
    {
        // Se tiver o nome da controller e o nome da action,
        // Significa que e uma requisicao.
        if (isset($_POST['controller']) && isset($_POST['action']))
        {
            header("Content-Type: text/html; charset=ISO-8859-1");
//            header("Content-Type: text/html; charset=UTF-8");
//            echo '<head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>';
//            echo '<head><meta http-equiv=\'Content-Type\' content=\'text/html; charset=ISO-8895-1\'>
//    <meta name="viewport" content="width=device-width, initial-scale=1.0"></head>';
            // Encapsulando dados post
            $nameController = 'Controller_' . ucfirst($_POST['controller']);
            $nameAction = $_POST['action'] . 'Action';

            
            // Validacoes
//            if (!$nameController)
//                throw new Exception('Controller n�o foi informada!');
//            if (!$nameAction)
//                throw new Exception('Action n�o foi informada!');
//            if (!class_exists($nameController))
//                throw new Exception('Controller n�o existe!');
//            if (!method_exists($nameController, $nameAction))
//                throw new Exception('Action n�o existe!');

            // Estanciando class Controller e exibindo na tela a action.
            $controller = new $nameController;
            echo $controller->$nameAction();
            
            
            
            // -- Caso a p�gina requisitada seja uma p�gina existente, realiza o log de estat�sticas - Verifica��o necess�ria pois
            // -- ainda n�o foi poss�vel reproduzir o erro no sistema financeiro que faz com que todas as imagens do tema do sistema
            // -- sejam requeridas pelo browers como um m�dulo. Esta mesma verifica��o � feita no controleAcesso no momento de
            // -- incluir os arquivos.
            if (file_exists(realpath(APPRAIZ . $_SESSION['sisdiretorio'] . "/modulos/" . $_REQUEST['modulo'] . ".inc"))) {
                simec_gravar_estatistica();
            }
            exit;
        }
    }
