<?php
 /**
 * File containing the Ticket's urgency labels and colours.
 * 
 * @category Constants
 * @package PHPSupportTickets
 * @author Ian Warner, <iwarner@triangle-solutions.com> 
 * @author Nicolas Connault, <nick@connault.com.au> 
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version SVN: $Id: urgency.php 4 2005-12-13 01:47:15Z nicolas $
 * @since File available since Release 1.1.1.1
 * \\||
 */

/**
* @ignore
* Sets up the labels and colors for the urgency levels of Tickets
*/
/*
define ('PHPST_URGENCY_LABEL_1', 'Low');
define ('PHPST_URGENCY_LABEL_2', 'Medium');
define ('PHPST_URGENCY_LABEL_3', 'High');
define ('PHPST_URGENCY_LABEL_4', 'Urgent');
*/
define ('PHPST_URGENCY_LABEL_1', 'Baixa');
define ('PHPST_URGENCY_LABEL_2', 'M�dia');
define ('PHPST_URGENCY_LABEL_3', 'Alta');
define ('PHPST_URGENCY_LABEL_4', 'Urgente');

define ('PHPST_URGENCY_COLOUR_1', 'FFCC99');
define ('PHPST_URGENCY_COLOUR_2', 'FF9966');
define ('PHPST_URGENCY_COLOUR_3', 'FF6633');
define ('PHPST_URGENCY_COLOUR_4', 'FF3300');
?>