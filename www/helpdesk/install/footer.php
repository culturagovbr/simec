<?php
/**
 * @category  Procedural File
 * @package   PHP Support Tickets
 * @author    Ian Warner <iwarner@triangle-solutions.com>
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version   CVS: $Id: $
 * @since     File available since Release 1.1.1.1
 * \\||
 */
?>
<center><br />

<table class="footer" summary="Footer links">
  <tr>
    <td>
    <a href="http://www.triangle-solutions.com" target="_blank" title="Triangle Solutions">Triangle Solutions Ltd</a> |
    <a href="http://www.phpsupporttickets.com" target="_blank" title="PHP Support Tickets">PHP Support Tickets 2.2</a>
    </td>
  </tr>
</table>
<br />
<table class="footer" summary="Feedback and Donations">
  <tr>
    <td>

    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
    <table class="footer" summary="Donations">
      <tr>
        <td>Donate through PayPal!</td>
      </tr>
      <tr>
        <td>
        <input type="hidden" name="cmd" value="_xclick" />
        <input type="hidden" name="business" value="iwarner@triangle-solutions.com" />
        <input type="hidden" name="item_name" value="PHP Support Tickets" />
        <input type="hidden" name="item_number" value="phpst_triangle" />
        <input type="hidden" name="no_note" value="1" />
        <input type="hidden" name="currency_code" value="GBP" />
        <input type="hidden" name="tax" value="0" />
        <input type="image" src="https://www.paypal.com/images/x-click-but04.gif" name="submit" title="Support Tickets Secure Payment With PayPal" alt="Support Tickets Secure Payment With PayPal" />
        </td>
      </tr>
    </table>
    </form>

    </td>
    <td>

    <form action="http://www.hotscripts.com/cgi-bin/rate.cgi" method="post" target="_blank">
    <table class="footer" summary="Hotscripts Ratings">
      <tr>
        <td>Rate it at HotScripts.com!</td>
      </tr>
      <tr>
        <td>
        <select name="rate" size="1">
        <option value="5">Excellent</option>
        <option value="4">Very Good</option>
        <option value="3">Good</option>
        <option value="2">Fair</option>
        <option value="1">Poor</option>
        </select>
        <input type="hidden" name="ID" value="22679" />
        <input type="hidden" name="external" value="1" />
        <input type="submit" name="submit" value="Vote" /></td>
      </tr>
    </table>
    </form>

    </td>
    <td>

    <form action="http://www.scriptsearch.com/cgi-bin/rateit.cgi" method="post" target="_blank">
    <table class="footer" summary="Scriptsearch Ratings">
      <tr>
        <td>Rate it at ScriptSearch.com</td>
      </tr>
      <tr>
        <td>
        <select name="rate" size="1">
        <option value="5">Excellent!</option>
        <option value="4">Very Good</option>
        <option value="3">Good</option>
        <option value="2">Fair</option>
        <option value="1">Poor</option>
        </select>
        <input type="hidden" name="ID" value="9272" />
        <input type="submit" name="submit" value="Vote" />
        </td>
      </tr>
    </table>
    </form>

    </td>
  </tr>
</table>

</center>
</body>
</html>