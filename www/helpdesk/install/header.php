<?php
/**
 * Provides the header element for the install script.
 *
 * @package   phpsupporttickets_procedural
 * @author    Triangle Solutions Ltd
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version   SVN: $Id: $
 * @todo      Need to sort out the charset value
 * \\||
 */
?>

<!-- PHP Support Tickets Application - Triangle Solutions Ltd /-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>PHP Support Tickets Application - Triangle Solutions Ltd</title>

<meta http-equiv="content-type" content="text/xml; charset=iso-8859-1" />

<meta name="description" content="PHP Support Tickets Application - Triangle Solutions Ltd" />
<meta name="keywords"content="support,tickets,customer,php,application,mysql,triangle,solutions,ltd" />
<meta name="copyright" content="Triangle Solutions Ltd" />
<meta name="rating" content="General" />
<meta name="robots" content="All" />

<style type="text/css" title="currentStyle" media="screen">
  @import url("../css/install_style.css");
</style>

</head>
<body>