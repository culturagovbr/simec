<?php
/**
 * File containing the Note class.
 * 
 * @category Class
 * @package PHPSupportTickets
 * @author Ian Warner, <iwarner@triangle-solutions.com> 
 * @author Nicolas Connault, <nick@connault.com.au> 
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version SVN: $Id: class.Note.php 4 2005-12-13 01:47:15Z nicolas $
 * @since File available since Release 1.1.1.1
 * \\||
 */
/**
 * user defined includes
 * 
 * @include 
 */

require_once_check(PHPST_PATH . 'classes/class.User.php');

/**
 * user defined constants
 * 
 * @ignore 
 */

/**
 * Short description of class Note
 * 
 * @access public 
 * @package PHPSupportTickets
 */
class Note {
    // --- ATTRIBUTES ---
    // --- OPERATIONS ---
} 
/* end of class Note */

?>