<?php
/**
 * File containing the English Word Constants.
 *
 * @category Procedural
 * @package PHPSupportTickets
 * @author Ian Warner, <iwarner@triangle-solutions.com>
 * @author Nicolas Connault, <nick@connault.com.au>
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version SVN: $Id: eng.php 4 2005-12-13 01:47:15Z nicolas $
 * @since File available since Release 1.1.1.1
 * \||
 */
define ('PHPST_ACCOUNT', 'Account');
define ('PHPST_ACTIVATE', 'Activate');
define ('PHPST_ADD', 'Add');
define ('PHPST_ADMIN', "Admin");
define ('PHPST_ADMIN_STATUS', 'Admin Status');
define ('PHPST_ALL', "All");
define ('PHPST_ANSWER', 'Answer');
define ('PHPST_ASSISTANT', "Assistant");
define ('PHPST_ATTACHMENT', 'Attachment: ');
define ('PHPST_BROWSE', "Browse");
define ('PHPST_BROWSE_USERS', "Browse Users");
define ('PHPST_CELL_PHONE', "Cell Phone");
define ('PHPST_CLICK', 'Click on the Ticket Number to read the ticket');
define ('PHPST_CLICK_DEPARTMENT', 'Click on the Department\'s ID to view its details');
define ('PHPST_CLICK_USER', 'Click on the User\'s ID to view the User\'s details');
define ('PHPST_CLIENT', "Client");
define ('PHPST_CLOSED', 'Closed');
define ('PHPST_CLOSETICKET', 'Close Ticket');
define ('PHPST_COMPANY', "Company");
define ('PHPST_COMPANY_DETAILS', "Company Details");
define ('PHPST_CONTACT_USER', "Contact User");
define ('PHPST_CREATE_DEPARTMENT', "New Department");
define ('PHPST_CREATE_USER', "New User");
define ('PHPST_CURRENT_PAGE', 'Current Page: ');
define ('PHPST_DATE', 'Date');
define ('PHPST_DATE_JOINED', 'Date joined');
define ('PHPST_DELETE_TICKET', "Delete Ticket");
define ('PHPST_DELETE_DEPARTMENT_SUCCESS', "The department and all its relationships with existing users have been deleted.");
define ('PHPST_DELETE', 'Delete');
define ('PHPST_DEPARTMENT', "Department");
define ('PHPST_DEPARTMENT_EXISTS', "This department already exists, please choose a different name.");
define ('PHPST_DEPARTMENT_INSERT_SUCCESS', "Department successfully added");
define ('PHPST_DEPARTMENTS', 'Departments');
define ('PHPST_DEPTICKETS', "Department Tickets");
define ('PHPST_DESCRIPTION', 'Description');
define ('PHPST_DETAILS', "Ticket Details");
define ('PHPST_DIALOG_QUESTION', 'Dialog Question');
define ('PHPST_DOB', "Date of Birth");
define ('PHPST_EDIT_TICKET', "Edit Ticket");
define ('PHPST_EDIT', 'Edit');
define ('PHPST_EMAIL', 'Email');
define ('PHPST_EMAILOPT', 'Emails');
define ('PHPST_EMAIL_SEND_FAILURE', 'Failed to send notification email.');
define ('PHPST_FILES', "Files");
define ('PHPST_FNAME', "First Name");
define ('PHPST_GO', 'Go');
define ('PHPST_HELP', 'Help');
define ('PHPST_HIGH', 'High');
define ('PHPST_ID', "ID");
define ('PHPST_IM', "IM Address");
define ('PHPST_INDEX_ENGLISH', 'English');
define ('PHPST_INDEX_FAILURE', 'Sorry, the username or password you have entered are incorrect.');
define ('PHPST_INDEX_JAPANESE', 'Japanese');
define ('PHPST_INDEX_LOGIN', 'Login');
define ('PHPST_INDEX_PASSWORD', 'Password');
define ('PHPST_INDEX_USERNAME', 'Username');
define ('PHPST_INFORMATION', 'Information');
define ('PHPST_JOBTITLE', "Job Title");
define ('PHPST_LNAME', "Last Name");
define ('PHPST_LOGGEDAS', 'You are logged in as: ');
define ('PHPST_LOGIN', 'Log In');
define ('PHPST_LOGINBACK', 'Back');
define ('PHPST_LOGINPAGE', '<b>You have entered an invalid username/password combination. Please try again.</b><br /><br />If you are a registered user on this site then hit the button above that says Resend Details.<br /><br />Enter your email and we will email your details to you.');
define ('PHPST_LOW', 'Low');
define ('PHPST_MEDIUM', 'Medium');
define ('PHPST_MNAME', "Middle Name(s)");
define ('PHPST_MOBILE_STATUS', "Mobile Status");
define ('PHPST_MOD', 'Moderator');
define ('PHPST_MODS', 'Moderators');
define ('PHPST_MYACCOUNT', "My Account");
define ('PHPST_MYTICKETS', "My Tickets");
define ('PHPST_NAME', "Name");
define ('PHPST_NEW', 'New Ticket');
define ('PHPST_NEWTICKET_INSTRUCTIONS', 'Please fill in all the information. And make sure the
					question is very explicit as to what the problem is - some guidelines
					follow:
					<ul>
					<li>Type of question (bug / content / Other)</li>
					<li>When did you see this (date and time)</li>
					<li>Is there a location to see this bug (URL / Media)</li>
					<li>Description (detailed but concise)</li>
					</ul>

					Make sure all fields are filled in. Please choose the department that
					best suits this ticket.
					<b>Allowed FILE TYPES for attachments:</b>
                <ul>
                  <li>image/jpeg (.jpeg/.jpg)</li>
                  <li>image/gif (.gif)</li>
                  <li>image/png (.png)</li>
                  <li>application/msword (.doc)</li>
                  <li>application/pdf (.pdf)</li>
                  <li>application/octet-stream (.csv)</li>
                  <li>application/zip (.zip)</li>
                  <li>text/plain (.txt)</li>
                  <li>text/css (.css)</li>
                  <li>text/html (.htm/.html/.xhtml)</li>
                  <li>text/xml (.xml)</li>
                  <li>text/x-javascript (.js)</li>
                </ul>');
define ('PHPST_NEW_SUPPORT_TICKET', 'New Support Ticket, all fields required.');
define ('PHPST_NEXT', 'Next');
define ('PHPST_NICKNAME', "Nickname");
define ('PHPST_NOS', 'There are no contacts listed under ');
define ('PHPST_NOTES', "Notes");
define ('PHPST_NOTES_ADD', 'Add Note');
define ('PHPST_NOTES_BODY', 'Body');
define ('PHPST_NOTES_TITLE', 'Title');
define ('PHPST_NOTE_INSERT_FAILURE', "Could not add Note");
define ('PHPST_NOTE_INSERT_SUCCESS', "Note added");
define ('PHPST_NO', 'No');
define ('PHPST_NO_MODS', 'No Moderators');
define ('PHPST_NO_RESULTS', 'Sorry but the search returned Zero results please try again.');
define ('PHPST_NO_TICKETS', 'No Tickets');
define ('PHPST_NO_USERS', 'No Users');
define ('PHPST_OFF', 'Off');
define ('PHPST_ON', 'On');
define ('PHPST_OPEN', 'Open');
define ('PHPST_OPTIONS', 'Settings');
define ('PHPST_OPTION_1', 'Email mods in the department in which a new ticket is posted');
define ('PHPST_OPTION_2', 'Email owner of ticket when a new answer is posted for that ticket');
define ('PHPST_OPTION_3', 'Email owner of ticket when the ticket\'s status changes');
define ('PHPST_OPTION_4', 'Email user when registered');
define ('PHPST_OPTION_5', 'Enable posting of notes by Moderators');
define ('PHPST_OPTION_6', 'Enable rating of answers by Clients');
define ('PHPST_OPTION_7', 'Recent tickets: number of days to include');
define ('PHPST_ORDERBY', 'Order By');
define ('PHPST_PAGENAME_', 'Home');
define ('PHPST_PAGENAME_BROWSEDEPARTMENTS', 'Browse Departments');
define ('PHPST_PAGENAME_OPTIONS', 'Options');
define ('PHPST_PAGENAME_BROWSETICKETS', 'Browse Tickets');
define ('PHPST_PAGENAME_BROWSEUSERS', 'Browse Users');
define ('PHPST_PAGENAME_NEWANSWER', 'New Answer');
define ('PHPST_PAGENAME_NEWTICKET', 'New Ticket');
define ('PHPST_PAGENAME_CREATEDEPARTMENT', 'New Department');
define ('PHPST_PAGENAME_CREATETICKET', 'New Ticket');
define ('PHPST_PAGENAME_CREATEUSER', 'New User');
define ('PHPST_PAGENAME_DELETEDEPARTMENT', 'Delete Department');
define ('PHPST_PAGENAME_HOME', 'Home');
define ('PHPST_PAGENAME_LOGIN', 'Login');
define ('PHPST_PAGENAME_REGISTER', 'Register');
define ('PHPST_PAGENAME_UPDATEDEPARTMENT', 'Update Department');
define ('PHPST_PAGENAME_VIEWDEPARTMENT', 'Edit Department');
define ('PHPST_PAGENAME_VIEWTICKET', 'Edit Ticket');
define ('PHPST_PAGENAME_VIEWUSER', 'Edit User');
define ('PHPST_PAGES', 'Pages');
define ('PHPST_PASSWORD', 'Password');
define ('PHPST_PERSONAL_DETAILS', "Personal Details");
define ('PHPST_PHPST', "PHP Support Tickets");
define ('PHPST_POSTEDBY', 'Posted By');
define ('PHPST_PREVIOUS', 'Previous');
define ('PHPST_PRIVACY', 'Privacy');
define ('PHPST_PRIVATE', 'Private');
define ('PHPST_PRIVATE_STATUS', 'Privacy Status');
define ('PHPST_PRIVILEGES', 'Privileges');
define ('PHPST_PUBLIC', 'Public');
define ('PHPST_QUESTION', 'Question');
define ('PHPST_RATINGS', 'Ratings');
define ('PHPST_RECENT', 'Recent Tickets');
define ('PHPST_RECORD', 'record');
define ('PHPST_RECORDS', 'records');
define ('PHPST_REGCONF', 'You have been registered and sent a confirmation email -  ');
define ('PHPST_REGEMAIL', 'Email:');
define ('PHPST_REGISTER', 'Register');
define ('PHPST_REGNAME', 'Name:');
define ('PHPST_REGPAGBACK', 'Back');
define ('PHPST_REGPAGE', 'Please complete all fields. You will be sent a registration email.<ul><li>Name - Keep this to Forename and Surname only please.</li><li>Username - Use 6-16 Chars only, no spaces, [a-z][0-9] only.</li><li>Password - Use 6-16 Chars only, no spaces, [a-z][0-9] only.</li><li>Email - make sure this is a valid email address</li></ul>');
define ('PHPST_REGPAGEERR', 'Please complete all the fields and make sure the email is the correct format.<br /><br />Hit the back button to try again.');
define ('PHPST_REGPASS', 'Password:');
define ('PHPST_REGSUBJECT', 'Thank You For Registering');
define ('PHPST_REGSUBMIT', 'Submit');
define ('PHPST_REGUSER', 'Username:');
define ('PHPST_REGUSERERR', 'Sorry someone else is already using that Username.<br /><br />Hit the back button to try again.');
define ('PHPST_REOPENTICKET', 'Reopen Ticket');
define ('PHPST_REPLIES', 'Replies');
define ('PHPST_RESEND', 'Resend Details');
define ('PHPST_RESENDBACK', 'Back');
define ('PHPST_RESENDERROR', 'Sorry, the details you have entered do not match anything we have in our Database.<br /><br />Hit the back button to try again.');
define ('PHPST_RESENDPAGE', 'The email must match the one you used when you created your account.<br />We will then send the username and password to the email given.');
define ('PHPST_RESPOND', 'Respond');
define ('PHPST_RESPONSE', 'Response');
define ('PHPST_SEARCH', "Search");
define ('PHPST_SEARCHERR', 'Sorry but the search returned Zero results please try again.');
define ('PHPST_SEARCHMSG', 'Click on the Ticket Number to read the ticket.');
define ('PHPST_SEARCHTITLE', 'Search Results:');
define ('PHPST_SELECTION', 'Selection: ');
define ('PHPST_STATUS', 'Status');
define ('PHPST_STTITLE', 'Support Tickets Manager');
define ('PHPST_SUBJECT', 'Subject');
define ('PHPST_SUBMIT', 'Submit');
define ('PHPST_SUFFIX', "Suffix");
define ('PHPST_SUSPEND', "Suspend");
define ('PHPST_TICKET', 'Ticket');
define ('PHPST_TICKETID', 'Ticket ID');
define ('PHPST_TICKETS', 'Tickets');
define ('PHPST_TICKETS_DEPARTMENTS', 'Departments');
define ('PHPST_TICKETS_TICKETS', 'Tickets');
define ('PHPST_TICKETS_ANSWERS', 'Answers');
define ('PHPST_TICKETS_TITLE', 'Tickets (open/total)');
define ('PHPST_TICKET_INSERT_FAILURE', "Could not add Ticket:");
define ('PHPST_TICKET_INSERT_SUCCESS', "Ticket added");
define ('PHPST_TIME', 'Time');
define ('PHPST_TITLE', "Title");
define ('PHPST_TITLECLO', 'Closed Tickets');
define ('PHPST_TITLELINK', 'Triangle Solutions Ltd');
define ('PHPST_TITLELOG', 'Logout');
define ('PHPST_TITLEOPE', 'Open Tickets');
define ('PHPST_TITLEREQ', 'New Ticket');
define ('PHPST_TRACKING_DATE', 'Date');
define ('PHPST_TRACKING_ID', 'ID');
define ('PHPST_TRACKING_IP', 'IP');
define ('PHPST_TRACKING_SUCCESS', 'Success');
define ('PHPST_TRACKING_USERNAME', 'Username');
define ('PHPST_UPLOAD_ATTACHMENT_SUCCESS', 'Attachment uploaded');
define ('PHPST_UPLOAD_FILETYPE', 'The file that you uploaded was of type ');
define ('PHPST_UPLOAD_NOT_ALLOWED', ' which is not allowed,	you are only allowed to upload files of the type:');
define ('PHPST_UPLOAD_NO_ATTACHMENT', 'No attachment uploaded');
define ('PHPST_UPLOAD_PARTIAL', 'Sorry we could only partially upload this file, please try again.');
define ('PHPST_UPLOAD_SUBMITTED', 'You submitted: ');
define ('PHPST_UPLOAD_TOO_BIG_FOR_PHP', 'This file exceeds the PHP upload size.');
define ('PHPST_UPLOAD_TOO_BIG_FOR_TOOL', 'This file exceeds the Maximum allowable size within this tool.');
define ('PHPST_UPLOAD_SIZE', 'Size: ');
define ('PHPST_UPLOAD_TYPE', 'Type: ');
define ('PHPST_URGENCY', 'Urgency');
define ('PHPST_URGENT', 'Urgent');
define ('PHPST_USER', "Mod");
define ('PHPST_USERID', "ID");
define ('PHPST_USERNAME', 'Username');
define ('PHPST_USERS', "Users");
define ('PHPST_USER_ACCOUNT', 'User Account Details');
define ('PHPST_USER_ALREADY_EXISTS', "This username already exists in our Database, please choose another one.");
define ('PHPST_USER_INSERT', "New User");
define ('PHPST_USER_INSERT_FAILURE', "Could not insert User");
define ('PHPST_USER_INSERT_SUCCESS', "User inserted");
define ('PHPST_USER_UPDATE_FAILURE', "Could not update User");
define ('PHPST_USER_UPDATE_SUCCESS', "User updated");
define ('PHPST_VIEW', 'View');
define ('PHPST_VIEW_TICKET', 'View Ticket');
define ('PHPST_YES', 'Yes');
define ('VALIDATE_DESCRIPTION_EMPTY', "Please enter a description");
define ('VALIDATE_EMPTY', "Please enter a ");
define ('VALIDATE_EMPTY_ADMIN_STATUS', "Please enter a value for the Admin Status");
define ('VALIDATE_EMPTY_BODY', "Please enter a message for this ticket/answer");
define ('VALIDATE_EMPTY_CITY', "Please enter your City");
define ('VALIDATE_EMPTY_DEPARTMENT', "Please enter a Department name");
define ('VALIDATE_EMPTY_EMAIL', "Please enter an email address");
define ('VALIDATE_EMPTY_FULLNAME', "Please enter your full name");
define ('VALIDATE_EMPTY_PASSWORD', "Please enter a Password");
define ('VALIDATE_EMPTY_PHONE', "Please enter your phone number");
define ('VALIDATE_EMPTY_SUBJECT', "Please enter a subject");
define ('VALIDATE_EMPTY_STREET', "Please enter a street");
define ('VALIDATE_EMPTY_USERNAME', "Please enter a username");
define ('VALIDATE_EMPTY_ZIP', "Please enter your Zip or Post code");
define ('VALIDATE_INVALID_ADMIN_STATUS', "Admin status must be 'Admin', 'Mod' or 'User'");
define ('VALIDATE_INVALID_ALPHA', "Please enter only letters");
define ('VALIDATE_INVALID_ALPHANUMERIC', "Please enter only letters and numbers");
define ('VALIDATE_INVALID_DATE', "Invalid date of birth");
define ('VALIDATE_INVALID_EMAIL', "Invalid Email address");
define ('VALIDATE_INVALID_NAME', "Must contain only letters and hyphen (-)");
define ('VALIDATE_INVALID_PASSWORD', "Invalid password: 6-16 Chars no spaces, [a-z][0-9] only.");
define ('VALIDATE_INVALID_PHONE', "Invalid phone number.");
define ('VALIDATE_INVALID_SUFFIX', "Suffix must either be empty or 'Jnr'");
define ('VALIDATE_INVALID_STREET', "Must contain only letters and numbers");
define ('VALIDATE_INVALID_URL', "Invalid URL");
define ('VALIDATE_INVALID_USERNAME', "Invalid username: 6-16 Chars no spaces, [a-z][0-9] only.");
define ('VALIDATE_INVALID_ZIP', "Invalid zip code");
define ('VALIDATE_EMPTY_NAME', "Please enter a name");
?>