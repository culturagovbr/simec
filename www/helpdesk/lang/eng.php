<?php
/**
 * File containing the English Word Constants.
 *
 * @category Procedural
 * @package PHPSupportChamados
 * @author Ian Warner, <iwarner@triangle-solutions.com>
 * @author Nicolas Connault, <nick@connault.com.au>
 * @copyright (C) 2001 Triangle Solutions Ltd
 * @version SVN: $Id: eng.php 4 2005-12-13 01:47:15Z nicolas $
 * @since File available since Release 1.1.1.1
 * \||
 */
define ('PHPST_ACCOUNT', 'Conta');
define ('PHPST_ACTIVATE', 'Ativar');
define ('PHPST_ADD', 'Incluir');
define ('PHPST_ADMIN', "Admin");
define ('PHPST_ADMIN_STATUS', 'Admin Status');
define ('PHPST_ALL', "Todos");
define ('PHPST_ANSWER', 'Resposta');
define ('PHPST_ASSISTANT', "Assistente");
define ('PHPST_ATTACHMENT', 'Anexo: ');
define ('PHPST_BROWSE', "Procurar");
define ('PHPST_BROWSE_USERS', "Procurar Usu�rios");
define ('PHPST_CELL_PHONE', "Celular");
define ('PHPST_CLICK', 'Clique no n�mero do chamado para ler');
define ('PHPST_CLICK_DEPARTMENT', 'Clique no setor para ver detalhes');
define ('PHPST_CLICK_USER', 'clique no ID do usu�rio para ver detalhes');
define ('PHPST_CLIENT', "Cliente");
define ('PHPST_CLOSED', 'Finalizado');
define ('PHPST_CLOSETICKET', 'Finalizar Chamado');
define ('PHPST_COMPANY', "Empresa");
define ('PHPST_COMPANY_DETAILS', "Dados da Empresa");
define ('PHPST_CONTACT_USER', "Entrar em contato com o usu�rio");
define ('PHPST_CREATE_DEPARTMENT', "Novo Departamento");
define ('PHPST_CREATE_USER', "Novo Usu�rio");
define ('PHPST_CURRENT_PAGE', 'P�gina Atual: ');
define ('PHPST_DATE', 'Data');
define ('PHPST_DATE_JOINED', 'Data de entrada');
define ('PHPST_DELETE_TICKETS', "Apagar Chamado");
define ('PHPST_DELETE_DEPARTMENT_SUCCESS', "The department and all its relationships with existing users have been deleted.");
define ('PHPST_DELETE', 'Apagar');
define ('PHPST_DEPARTMENT', "Departamento");
define ('PHPST_DEPARTMENT_EXISTS', "Este departamento j� existe.");
define ('PHPST_DEPARTMENT_INSERT_SUCCESS', "Departmento inclu�do com sucesso");
define ('PHPST_DEPARTMENTS', 'Departmentos');
define ('PHPST_DEPTICKETS', "Chamados do Departmento");
define ('PHPST_DESCRIPTION', 'Descri��o');
define ('PHPST_DETAILS', "Detalhes do Chamado");
define ('PHPST_DIALOG_QUESTION', 'Pergunta');
define ('PHPST_DOB', "Data de Nascimento");
define ('PHPST_EDIT_TICKETS', "Editar Chamado");
define ('PHPST_EDIT', 'Editar');
define ('PHPST_EMAIL', 'Email');
define ('PHPST_EMAILOPT', 'Emails');
define ('PHPST_EMAIL_SEND_FAILURE', 'Falha ao enviar email.');
define ('PHPST_FILES', "Arquivos");
define ('PHPST_FNAME', "Primeiro Nome");
define ('PHPST_GO', 'OK');
define ('PHPST_HELP', 'Ajuda');
define ('PHPST_HIGH', 'Alta');
define ('PHPST_ID', "ID");
define ('PHPST_IM', "IM Address");
define ('PHPST_INDEX_ENGLISH', 'English');
define ('PHPST_INDEX_FAILURE', 'Sorry, the username or password you have entered are incorrect.');
define ('PHPST_INDEX_JAPANESE', 'Japanese');
define ('PHPST_INDEX_LOGIN', 'Login');
define ('PHPST_INDEX_PASSWORD', 'Password');
define ('PHPST_INDEX_USERNAME', 'Username');
define ('PHPST_INFORMATION', 'Informa��es');
define ('PHPST_JOBTITLE', "Trabalho");
define ('PHPST_LNAME', "�ltimo Nome");
define ('PHPST_LOGGEDAS', 'Voc� est� logado: ');
define ('PHPST_LOGIN', 'Log In');
define ('PHPST_LOGINBACK', 'Voltar');
define ('PHPST_LOGINPAGE', '<b>You have entered an invalid username/password combination. Please try again.</b><br /><br />If you are a registered user on this site then hit the button above that says Resend Details.<br /><br />Enter your email and we will email your details to you.');
define ('PHPST_LOW', 'Baixa');
define ('PHPST_MEDIUM', 'M�dia');
define ('PHPST_MNAME', "Nomes do Meio");
define ('PHPST_MOBILE_STATUS', "Mobile Status");
define ('PHPST_MOD', 'Moderador');
define ('PHPST_MODS', 'Moderadores');
define ('PHPST_MYACCOUNT', "Minha Conta");
define ('PHPST_MYTICKETS', "Meus Chamados");
define ('PHPST_NAME', "Nome");
define ('PHPST_NEW', 'Novo Chamado');
define ('PHPST_NEWTICKET_INSTRUCTIONS', 'Please fill in all the information. And make sure the
					question is very explicit as to what the problem is - some guidelines
					follow:
					<ul>
					<li>Type of question (bug / content / Other)</li>
					<li>When did you see this (date and time)</li>
					<li>Is there a location to see this bug (URL / Media)</li>
					<li>Description (detailed but concise)</li>
					</ul>

					Make sure all fields are filled in. Please choose the department that
					best suits this Chamado.
					<b>Allowed FILE TYPES for attachments:</b>
                <ul>
                  <li>image/jpeg (.jpeg/.jpg)</li>
                  <li>image/gif (.gif)</li>
                  <li>image/png (.png)</li>
                  <li>application/msword (.doc)</li>
                  <li>application/pdf (.pdf)</li>
                  <li>application/octet-stream (.csv)</li>
                  <li>application/zip (.zip)</li>
                  <li>text/plain (.txt)</li>
                  <li>text/css (.css)</li>
                  <li>text/html (.htm/.html/.xhtml)</li>
                  <li>text/xml (.xml)</li>
                  <li>text/x-javascript (.js)</li>
                </ul>');
define ('PHPST_NEW_SUPPORT_TICKETS', 'New Support Chamado, all fields required.');
define ('PHPST_NEXT', 'Pr�ximo');
define ('PHPST_NICKNAME', "Nickname");
define ('PHPST_NOS', 'There are no contacts listed under ');
define ('PHPST_NOTES', "Obs.");
define ('PHPST_NOTES_ADD', 'Incluir Obs.');
define ('PHPST_NOTES_BODY', 'Corpo');
define ('PHPST_NOTES_TITLE', 'T�tulo');
define ('PHPST_NOTE_INSERT_FAILURE', "Imposs�vel incluir Obs.");
define ('PHPST_NOTE_INSERT_SUCCESS', "Obs. Inclu�da");
define ('PHPST_NO', 'N�o');
define ('PHPST_NO_MODS', 'Sem Moderadores');
define ('PHPST_NO_RESULTS', 'Sorry but the search returned Zero results please try again.');
define ('PHPST_NO_TICKETS', 'Sem Chamados');
define ('PHPST_NO_USERS', 'Sem Usu�rios');
define ('PHPST_OFF', 'Desligado');
define ('PHPST_ON', 'Ligado');
define ('PHPST_OPEN', 'Aberto');
define ('PHPST_OPTIONS', 'Configura��es');
define ('PHPST_OPTION_1', 'Email mods in the department in which a new Chamado is posted');
define ('PHPST_OPTION_2', 'Email owner of Chamado when a new answer is posted for that Chamado');
define ('PHPST_OPTION_3', 'Email owner of Chamado when the Chamado\'s status changes');
define ('PHPST_OPTION_4', 'Email user when registered');
define ('PHPST_OPTION_5', 'Enable posting of notes by Moderators');
define ('PHPST_OPTION_6', 'Enable rating of answers by Clients');
define ('PHPST_OPTION_7', 'Recent Chamados: number of days to include');
define ('PHPST_ORDERBY', 'Ordenado Por');
define ('PHPST_PAGENAME_', 'Inicial');
define ('PHPST_PAGENAME_BROWSEDEPARTMENTS', 'Procurar Departamentos');
define ('PHPST_PAGENAME_OPTIONS', 'Op��es');
define ('PHPST_PAGENAME_BROWSETICKETS', 'Procurar Chamados');
define ('PHPST_PAGENAME_BROWSEUSERS', 'Procurar Usu�rios');
define ('PHPST_PAGENAME_NEWANSWER', 'Nova Resposta');
define ('PHPST_PAGENAME_NEWTICKET', 'Novo Chamado');
define ('PHPST_PAGENAME_CREATEDEPARTMENT', 'Novo Departamento');
define ('PHPST_PAGENAME_CREATETICKET', 'Novo Chamado');
define ('PHPST_PAGENAME_CREATEUSER', 'Novo Usu�rio');
define ('PHPST_PAGENAME_DELETEDEPARTMENT', 'Apagar Departamento');
define ('PHPST_PAGENAME_HOME', 'Inicial');
define ('PHPST_PAGENAME_LOGIN', 'Login');
define ('PHPST_PAGENAME_REGISTER', 'Cadastro');
define ('PHPST_PAGENAME_UPDATEDEPARTMENT', 'Atualizar Departamento');
define ('PHPST_PAGENAME_VIEWDEPARTMENT', 'Editar Departamento');
define ('PHPST_PAGENAME_VIEWTICKET', 'Editar Chamado');
define ('PHPST_PAGENAME_VIEWUSER', 'Editar Usu�rio');
define ('PHPST_PAGES', 'P�ginas');
define ('PHPST_PASSWORD', 'Senha');
define ('PHPST_PERSONAL_DETAILS', "Meus Detalhes");
define ('PHPST_PHPST', "Chamados");
define ('PHPST_POSTEDBY', 'Inclu�do por');
define ('PHPST_PREVIOUS', 'Anterior');
define ('PHPST_PRIVACY', 'Privacidade');
define ('PHPST_PRIVATE', 'Privado');
define ('PHPST_PRIVATE_STATUS', 'Status de Privacidade');
define ('PHPST_PRIVILEGES', 'Privilegios');
define ('PHPST_PUBLIC', 'Publico');
define ('PHPST_QUESTION', 'Pergunta');
define ('PHPST_RATINGS', 'Avalia��es');
define ('PHPST_RECENT', 'Chamados Recentes');
define ('PHPST_RECORD', 'registro');
define ('PHPST_RECORDS', 'registros');
define ('PHPST_REGCONF', 'You have been registered and sent a confirmation email -  ');
define ('PHPST_REGEMAIL', 'Email:');
define ('PHPST_REGISTER', 'Cadastro');
define ('PHPST_REGNAME', 'Nome:');
define ('PHPST_REGPAGBACK', 'Voltar');
define ('PHPST_REGPAGE', 'Please complete all fields. You will be sent a registration email.<ul><li>Name - Keep this to Forename and Surname only please.</li><li>Username - Use 6-16 Chars only, no spaces, [a-z][0-9] only.</li><li>Password - Use 6-16 Chars only, no spaces, [a-z][0-9] only.</li><li>Email - make sure this is a valid email address</li></ul>');
define ('PHPST_REGPAGEERR', 'Please complete all the fields and make sure the email is the correct format.<br /><br />Hit the back button to try again.');
define ('PHPST_REGPASS', 'Senha:');
define ('PHPST_REGSUBJECT', 'Thank You For Registering');
define ('PHPST_REGSUBMIT', 'Enviar');
define ('PHPST_REGUSER', 'Username:');
define ('PHPST_REGUSERERR', 'Sorry someone else is already using that Username.<br /><br />Hit the back button to try again.');
define ('PHPST_REOPENTICKET', 'Reabrir Chamado');
define ('PHPST_REPLIES', 'Respostas');
define ('PHPST_RESEND', 'Re-enviar Detalhes');
define ('PHPST_RESENDBACK', 'Voltar');
define ('PHPST_RESENDERROR', 'Sorry, the details you have entered do not match anything we have in our Database.<br /><br />Hit the back button to try again.');
define ('PHPST_RESENDPAGE', 'The email must match the one you used when you created your account.<br />We will then send the username and password to the email given.');
define ('PHPST_RESPOND', 'Responder');
define ('PHPST_RESPONSE', 'Resposta');
define ('PHPST_SEARCH', "Procurar");
define ('PHPST_SEARCHERR', 'Sorry but the search returned Zero results please try again.');
define ('PHPST_SEARCHMSG', 'Click on the Chamado Number to read the Chamado.');
define ('PHPST_SEARCHTITLE', 'Search Results:');
define ('PHPST_SELECTION', 'Selection: ');
define ('PHPST_STATUS', 'Status');
define ('PHPST_STTITLE', 'Gerenciamento de Chamados');
define ('PHPST_SUBJECT', 'Assunto');
define ('PHPST_SUBMIT', 'Enviar');
define ('PHPST_SUFFIX', "Sulfixo");
define ('PHPST_SUSPEND', "Cancelar");
define ('PHPST_TICKETS', 'Chamados');
define ('PHPST_TICKETID', 'ID do Chamado');
define ('PHPST_TICKETS_DEPARTMENTS', 'Departamentos');
define ('PHPST_TICKETS_TICKETS', 'Chamados');
define ('PHPST_TICKETS_ANSWERS', 'Respostas');
define ('PHPST_TICKETS_TITLE', 'Chamados (abertos/total)');
define ('PHPST_TICKETS_INSERT_FAILURE', "Imposs�vel incluir Chamado:");
define ('PHPST_TICKETS_INSERT_SUCCESS', "Chamado inclu�do");
define ('PHPST_TIME', 'Hora');
define ('PHPST_TITLE', "T�tulo");
define ('PHPST_TITLECLO', 'Chamados Finalizados');
define ('PHPST_TITLELINK', 'MEC');
define ('PHPST_TITLELOG', 'Sair');
define ('PHPST_TITLEOPE', 'Abrir Chamados');
define ('PHPST_TITLEREQ', 'Novo Chamado');
define ('PHPST_TRACKING_DATE', 'Data');
define ('PHPST_TRACKING_ID', 'ID');
define ('PHPST_TRACKING_IP', 'IP');
define ('PHPST_TRACKING_SUCCESS', 'Successo');
define ('PHPST_TRACKING_USERNAME', 'Username');
define ('PHPST_UPLOAD_ATTACHMENT_SUCCESS', 'Anexo enviado');
define ('PHPST_UPLOAD_FILETYPE', 'The file that you uploaded was of type ');
define ('PHPST_UPLOAD_NOT_ALLOWED', ' which is not allowed,	you are only allowed to upload files of the type:');
define ('PHPST_UPLOAD_NO_ATTACHMENT', 'No attachment uploaded');
define ('PHPST_UPLOAD_PARTIAL', 'Sorry we could only partially upload this file, please try again.');
define ('PHPST_UPLOAD_SUBMITTED', 'voc� enviou: ');
define ('PHPST_UPLOAD_TOO_BIG_FOR_PHP', 'This file exceeds the PHP upload size.');
define ('PHPST_UPLOAD_TOO_BIG_FOR_TOOL', 'This file exceeds the Maximum allowable size within this tool.');
define ('PHPST_UPLOAD_SIZE', 'Tamanho: ');
define ('PHPST_UPLOAD_TYPE', 'Tipo: ');
define ('PHPST_URGENCY', 'Prioridade');
define ('PHPST_URGENT', 'Urgente');
define ('PHPST_USER', "Mod");
define ('PHPST_USERID', "ID");
define ('PHPST_USERNAME', 'Username');
define ('PHPST_USERS', "Usu�rios");
define ('PHPST_USER_ACCOUNT', 'Detalhes do Usu�rio');
define ('PHPST_USER_ALREADY_EXISTS', "This username already exists in our Database, please choose another one.");
define ('PHPST_USER_INSERT', "New User");
define ('PHPST_USER_INSERT_FAILURE', "N�o foi poss�vel inserir usu�rio");
define ('PHPST_USER_INSERT_SUCCESS', "Usu�rio inserido");
define ('PHPST_USER_UPDATE_FAILURE', "N�o foi poss�vel atualizar usu�rio");
define ('PHPST_USER_UPDATE_SUCCESS', "Usu�rio atualizado");
define ('PHPST_VIEW', 'Vis�o');
define ('PHPST_VIEW_TICKETS', 'Visualiza��o Chamado');
define ('PHPST_YES', 'Sim');
define ('VALIDATE_DESCRIPTION_EMPTY', "Please enter a description");
define ('VALIDATE_EMPTY', "Please enter a ");
define ('VALIDATE_EMPTY_ADMIN_STATUS', "Please enter a value for the Admin Status");
define ('VALIDATE_EMPTY_BODY', "Please enter a message for this Chamado/answer");
define ('VALIDATE_EMPTY_CITY', "Please enter your City");
define ('VALIDATE_EMPTY_DEPARTMENT', "Please enter a Department name");
define ('VALIDATE_EMPTY_EMAIL', "Please enter an email address");
define ('VALIDATE_EMPTY_FULLNAME', "Please enter your full name");
define ('VALIDATE_EMPTY_PASSWORD', "Please enter a Password");
define ('VALIDATE_EMPTY_PHONE', "Please enter your phone number");
define ('VALIDATE_EMPTY_SUBJECT', "Please enter a subject");
define ('VALIDATE_EMPTY_STREET', "Please enter a street");
define ('VALIDATE_EMPTY_USERNAME', "Please enter a username");
define ('VALIDATE_EMPTY_ZIP', "Please enter your Zip or Post code");
define ('VALIDATE_INVALID_ADMIN_STATUS', "Admin status must be 'Admin', 'Mod' or 'User'");
define ('VALIDATE_INVALID_ALPHA', "Please enter only letters");
define ('VALIDATE_INVALID_ALPHANUMERIC', "Please enter only letters and numbers");
define ('VALIDATE_INVALID_DATE', "Invalid date of birth");
define ('VALIDATE_INVALID_EMAIL', "Invalid Email address");
define ('VALIDATE_INVALID_NAME', "Must contain only letters and hyphen (-)");
define ('VALIDATE_INVALID_PASSWORD', "Invalid password: 6-16 Chars no spaces, [a-z][0-9] only.");
define ('VALIDATE_INVALID_PHONE', "Invalid phone number.");
define ('VALIDATE_INVALID_SUFFIX', "Suffix must either be empty or 'Jnr'");
define ('VALIDATE_INVALID_STREET', "Must contain only letters and numbers");
define ('VALIDATE_INVALID_URL', "Invalid URL");
define ('VALIDATE_INVALID_USERNAME', "Invalid username: 6-16 Chars no spaces, [a-z][0-9] only.");
define ('VALIDATE_INVALID_ZIP', "Invalid zip code");
define ('VALIDATE_EMPTY_NAME', "Please enter a name");
?>