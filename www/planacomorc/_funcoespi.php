<?php

/**
 * Fun��es de apoio ao gerenciamento de PIs.
 * $Id: _funcoespi.php 99771 2015-07-06 20:42:51Z werteralmeida $
 */
function listaPisManterAt($dados) {
    global $db;
    $obrigatorias = UNIDADES_OBRIGATORIAS;
    /* Filtros */
    $where = '';
    $where .= $dados['pi'] ? " AND pli.plicod ilike '%" . $dados['pi'] . "%' " : "";
    $where .= $dados['unidade'] ? " AND (pli.unicod = '{$dados['unidade']}' OR pli.ungcod IN (select ungcod from public.unidadegestora where unicod = '{$dados['unidade']}') )" : "";
    $where .= $dados['plisituacao'] && $dados['plisituacao'] != 'undefined' ? " AND pli.plisituacao = '" . $dados['plisituacao'] . "' " : "";
    $where .= $dados['apenas_obrigatorias'] ? " AND pli.unicod IN (" . UNIDADES_OBRIGATORIAS . ")" : "";
    //$where .= $dados["ptres"] != 'null' && $dados["ptres"] != '' ? " AND ptres.ptrid in ('" . str_replace(",", "','", $dados['ptres']) . "') " : "";
    if (is_array($_REQUEST["ptres"])) {
        $where .= $_REQUEST["ptres"] != 'null' && !$_REQUEST["ptres"] ? " AND ptres.ptrid in ('" . implode("','", $_REQUEST["ptres"]) . "') " : "";
    } else {
        $where .= $_REQUEST["ptres"] != 'null' && $_REQUEST["ptres"] != '' ? " AND ptres.ptrid in ('" . str_replace(",", "','", $dados['ptres']) . "') " : "";
    }
    $titulodescricaoTmp = removeAcentos(str_replace("-", "", $_REQUEST['titulodescricao']));
    $where .= $dados['titulodescricao'] ? " AND (UPPER(public.removeacento(pli.plititulo)) ilike '%" . $titulodescricaoTmp . "%' OR UPPER(public.removeacento(pli.plidsc)) ilike '%" . $titulodescricaoTmp . "%')" : "";


    if ($dados['apenasEmpenhados'] == 'true') {
        $strColumns = "
            pli.plicod AS codigo,
            uni.unicod || ' - ' || uni.unidsc AS unidsc,
            COALESCE(pemp.total,0) AS empenhado_total
        ";
    } else {
        $strColumns = "
            pli.pliid  AS acoes,
            pli.plicod AS codigo,
            CASE
                WHEN trim(pli.plititulo) IS NOT NULL
                THEN pli.plititulo
                ELSE 'N�o Preenchido '
            END                               AS titulo,
            uni.unicod || ' - ' || uni.unidsc AS unidsc,
            CASE
                WHEN pli.plisituacao = 'P'
                THEN ' Pendente '
                WHEN pli.plisituacao = 'A'
                THEN ' Aprovado '
                WHEN pli.plisituacao = 'R'
                THEN ' Revisado '
                WHEN pli.plisituacao = 'C'
                THEN ' Cadastrado no SIAFI '
                WHEN pli.plisituacao = 'E'
                THEN ' Enviado para Revis�o '
                WHEN pli.plisituacao = 'S'
                THEN ' Confimado no SIAFI '
                WHEN pli.plisituacao = 'T'
                THEN ' Cadastrado no SIAFI '
                WHEN pli.plisituacao = 'H'
                THEN ' Homologa��o '
            END                    AS situacao,
            COALESCE(pemp.total,0) AS empenhado_total
        ";
    }


    $sql = "SELECT
        DISTINCT
        {$strColumns}
FROM
    monitora.pi_planointerno pli
LEFT JOIN
    monitora.pi_planointernoptres pip
ON
    pip.pliid = pli.pliid
LEFT JOIN
    siafi.pliempenho pemp
ON
    pli.plicod = pemp.plicod
AND pli.pliano = pemp.exercicio
LEFT JOIN
    monitora.ptres
ON
    pip.ptrid = ptres.ptrid
LEFT JOIN
    public.unidade uni
ON
    uni.unicod = pli.unicod
LEFT JOIN
    public.unidadegestora ung
ON
    ung.ungcod = pli.ungcod
WHERE
    pli.plistatus = 'A'
AND pli.pliano = '{$_SESSION['exercicio']}'
$where
    ORDER BY
    2";

    return $sql;
}

/**
 * Lista de PIs para MANTER
 */
function listaPisManter($dados) {
    global $db;

    $obrigatorias = UNIDADES_OBRIGATORIAS;

    /* Filtros */
    $where = '';
    $where .= $dados['pi'] ? " AND pli.plicod ilike '%" . $dados['pi'] . "%' " : "";
    $where .= $dados['unidade'] ? " AND (pli.unicod = '{$dados['unidade']}' OR pli.ungcod IN (select ungcod from public.unidadegestora where unicod = '{$dados['unidade']}') )" : "";
    $where .= $dados['plisituacao'] && $dados['plisituacao'] != 'undefined' ? " AND pli.plisituacao = '" . $dados['plisituacao'] . "' " : "";
    $where .= $dados['apenas_obrigatorias'] ? " AND (pli.unicod IN ($obrigatorias) OR pli.ungcod IN (select ungcod from public.unidadegestora where unicod IN ($obrigatorias)) )" : "";
    $where .= $dados["ptres"] != 'null' && $dados["ptres"] != '' ? " AND ptres.ptrid in ('" . str_replace(",", "','", $dados['ptres']) . "') " : "";
    $titulodescricaoTmp = removeAcentos(str_replace("-", "", $_REQUEST['titulodescricao']));
    $where .= $dados['titulodescricao'] ? " AND (UPPER(public.removeacento(pli.plititulo)) ilike '%" . $titulodescricaoTmp . "%' OR UPPER(public.removeacento(pli.plidsc)) ilike '%" . $titulodescricaoTmp . "%')" : "";


    /* Caso seja selecionado apenas 1 PTRES, mostrar as colunas dos dados apenas para aquele PTRES */
    $sqlAdicional = "";
    if ($dados["ptres"] != 'null' && $dados["ptres"]) {
        $arPtres = explode(",", $dados['ptres']);
    }

    if ($_REQUEST["ptres"] && count($arPtres) == 1 && $dados['ptres'][0] != "" && $_REQUEST["ptres"] != 'null') {
        $dados = $db->carregar("SELECT ptres FROM monitora.ptres WHERE ptrid = {$_REQUEST["ptres"][0]}");
        $ptres = $dados[0]['ptres'];
        #ver($_REQUEST["ptres"][0],d);
        $sqlAdicional.=", COALESCE((SELECT
                                        pip.pipvalor
                                    FROM
                                        monitora.pi_planointernoptres pip
                                    LEFT JOIN
                                        monitora.ptres
                                    ON
                                        pip.ptrid = ptres.ptrid
                                    WHERE
                                        ptres.ptrano = '{$_SESSION['exercicio']}'
                                    AND pip.pliid= gmb.pliid
                                    AND ptres.ptrid={$_REQUEST["ptres"][0]}),0.00) as dotacao_pip_ptres
                        ,        COALESCE((SELECT
                                    total
                                FROM
                                    siafi.pliptrempenho ppe
                                WHERE
                                    plicod = gmb.codigo
                                    AND ppe.exercicio = '{$_SESSION['exercicio']}'
                                AND ptres = '{$ptres}'),0.00) as empenhado_pi_ptres";
    }
    /* Cabe�alho da Consulta */

    $acoes = " '-' ";
    /* Remove as a��es para o perfil Gabinete */
    $acoes = <<<SQL
        gmb.pliid
SQL;

    $params['SELECT'] = <<<SQL
SELECT
    {$acoes}
    AS acoes,

        gmb.codigo as codigo,
        CASE WHEN trim(gmb.titulo) is not null THEN
            gmb.titulo  || '<input type=\"hidden\" id=\"plititulo[' || gmb.pliid || ']\" value=\"' || gmb.codigo || ' - ' || gmb.titulo || '\">'
        ELSE
            'N�o Preenchido <input type=\"hidden\" id=\"plititulo[' || gmb.pliid || ']\" value=\" ' || gmb.codigo ||' - N�o Preenchido\"/>'
        END as titulo,
        uni.unicod || ' - ' || uni.unidsc as unidsc,
        '<center>' || CASE
        WHEN pli.plisituacao = 'P' THEN ' Pendente '
        WHEN pli.plisituacao = 'A' THEN ' Aprovado '
        WHEN pli.plisituacao = 'R' THEN ' Revisado '
        WHEN pli.plisituacao = 'C' THEN ' Cadastrado no SIAFI '
        WHEN pli.plisituacao = 'E' THEN ' Enviado para Revis�o '
        WHEN pli.plisituacao = 'S' THEN ' Confimado no SIAFI '
        WHEN pli.plisituacao = 'T' THEN ' <span style="color:red">Cadastrado no SIAFI</span> '
        WHEN pli.plisituacao = 'H' THEN ' Homologa��o '
        END || '</center>' AS situacao,
        empenhado AS empenhado_total
        {$sqlAdicional}
SQL;
    $params['where'] = " $where ";
    $sql = retornaConsultaPI($params);

    /* Busca alternativa para recuperar os PIs que possuem Empenho mas n�o est�o cadastrados no SIMEC */
    if ($dados['apenasEmpenhados'] == 'true') {
        $sql = <<<SQL
               SELECT
                plicod,
                'N�o informado.' AS unidade,
                total            AS empenhado
            FROM
                siafi.pliempenho pip
            WHERE
                pip.exercicio = '{$_SESSION['exercicio']}'
            AND pip.plicod NOT IN
                (
                    SELECT
                        plicod
                    FROM
                        monitora.pi_planointerno pli
                    WHERE
                        pli.pliano = '{$_SESSION['exercicio']}' )
SQL;
    }

    $cabecalho = array("A��o",
        "C�digo",
        "Unidade",
        "Empenhado Total do PI (R$)"
    );
    /* Caso seja selecionado apenas 1 PTRES, mostrar as colunas dos dados apenas para aquele PTRES */
    if ($_REQUEST["ptres"][0] && count($_REQUEST["ptres"]) == 1) {
        array_push($cabecalho, "Dota��o para este PTRES (R$)", "Empenhado neste PTRES (R$)", "N�o Empenhado neste PTRES");
    }

    return $sql;
}

/**
 * Monta a combo de UGs filtrando por UO
 */
function carregarComboUG($unicod) {
    global $db;

    if (in_array(PFL_GABINETE, pegaPerfilGeral($_SESSION['usucpf']))) {
        $filtroPerfilUG = <<<DML
AND EXISTS (SELECT 1
               FROM planacomorc.usuarioresponsabilidade urp
               WHERE urp.ungcod = ung.ungcod
                 AND urp.pflcod = %d
                 AND urp.usucpf = '%s'
                 AND urp.rpustatus = 'A')
DML;
        $filtroPerfilUG = sprintf($filtroPerfilUG, PFL_GABINETE, $_SESSION['usucpf']);
    }

    $sql = <<<DML
SELECT ung.ungcod AS codigo,
       ung.ungcod || ' - ' || ungabrev AS descricao
  FROM public.unidadegestora ung
  WHERE ung.ungstatus= 'A'
    AND ung.unicod = '%s' {$filtroPerfilUG}
  ORDER BY ung.unicod
DML;

    $stmt = sprintf($sql, $unicod);
    $dados = $db->carregar($stmt);
    if (count($dados) && $dados[0]) {
        $infoCombo = 'Selecione';
    } else {
        $dados = array();
        $infoCombo = 'Nenhuma UG encontrada';
    }
    //$db->monta_combo('unicod', $dados, 'S', $infoCombo, 'carregarSubacao', null, null, 240, 'S', 'unicod', false);
    $db->monta_combo('ungcod', $dados, 'S', $infoCombo, 'carregarSubacao', null, null, 240, 'N', 'ungcod', null, (isset($ungcod) ? $ungcod : null), null, 'class="form-control chosen-select" style="width=100%;""', null, null);
}

/**
 *  Carrega as suba��es de uma UO
 */
function carregarComboSubacao($unicod, $ungcod = null, $retornaSQL = false) {
    global $db;
    $sql = <<<DML
SELECT DISTINCT sba.sbaid AS codigo,
                sba.sbacod || ' - ' || sba.sbatitulo AS descricao
  FROM monitora.pi_subacao sba
    INNER JOIN monitora.pi_subacaounidade sbu USING(sbaid)
    LEFT JOIN public.unidadegestora udg USING(ungcod)
  WHERE COALESCE(sbu.unicod, udg.unicod) = '%s' %s
    AND sba.sbaano = '{$_SESSION['exercicio']}'
    AND sba.sbastatus = 'A'    
  ORDER BY descricao
DML;
    $whereAdicional = '';
    if ($ungcod) {
        $whereAdicional = sprintf(" AND sbu.ungcod = '%s'", $ungcod);
    }
    $stmt = sprintf($sql, $unicod, $whereAdicional);
    if ($retornaSQL) {
        return $stmt;
    }

    $dados = $db->carregar($stmt);
    if (count($dados) && $dados[0]) {
        $infoCombo = 'Selecione';
    } else {
        $dados = array();
        $infoCombo = 'Nenhuma Suba��o encontrada';
    }
    //ver($stmt);
    //$db->monta_combo('sbaid', $dados, 'S', $infoCombo, 'carregarInfoPI', '', '', '240', 'S', 'sbaid', false);
    $db->monta_combo('sbaid', $dados, 'S', $infoCombo, 'selecionarsubacao', null, null, '240', 'N', 'sbaid', null, '', null, 'class="form-control chosen-select" '
            . 'style="width=100%;""', null, (isset($sbaid) ? $sbaid : null));
}

/**
 *  Carrega as suba��es de uma UO
 */
function carregarComboSubacaoUO($unicod, $retornaSQL = false) {
    global $db;
    $sql = <<<DML
SELECT DISTINCT sba.sbaid AS codigo,
                sba.sbacod || ' - ' || sba.sbatitulo AS descricao
  FROM monitora.pi_subacao sba
    INNER JOIN monitora.pi_subacaounidade sbu USING(sbaid)
    WHERE sbu.unicod = '%s' %s
    AND sba.sbaano = '{$_SESSION['exercicio']}'
    AND sba.sbastatus = 'A'
  ORDER BY descricao
DML;

    $whereAdicional = '';
    $stmt = sprintf($sql, $unicod, $whereAdicional);
    if ($retornaSQL) {
        return $stmt;
    }

    $dados = $db->carregar($stmt);
    if (count($dados) && $dados[0]) {
        $infoCombo = 'Selecione';
    } else {
        $dados = array();
        $infoCombo = 'Nenhuma Suba��o encontrada';
    }
    //$db->monta_combo('sbaid', $dados, 'S', $infoCombo, '', '', '', '240', 'S', 'sbaid', false);
    $db->monta_combo('sbaid', $dados, 'S', $infoCombo, 'selecionarsubacao', null, null, '240', 'N', 'sbaid', null, '', null, 'class="form-control chosen-select" style="width=100%;""', null, '');
}

/* Valida o PI */

function validaCodPi($pi, $pliid = false) {
    global $db;
    $sql = "SELECT plicod FROM monitora.pi_planointerno WHERE plistatus='A' AND date_part('Y', plidata) = '{$_SESSION['exercicio']}' AND plicod = '{$pi}'" . (($pliid) ? " AND pliid != '" . $pliid . "'" : "");
    $plicod = $db->PegaUm($sql);

    if (!$plicod) {
        $retorno = "";
        echo $retorno;
        exit;
    } else {
        $retorno = "pijaexiste";
        echo $retorno;
        $sql = "SELECT p.plicod as plicod, coalesce(p.plititulo,'N�o preenchido') as titulo,
                    coalesce(SUM(pp.pipvalor),0) as total,
                    CASE WHEN p.plisituacao = 'P' THEN ' Pendente ' WHEN p.plisituacao = 'C' THEN ' Aprovado '
                         WHEN p.plisituacao = 'H' THEN ' Homologado ' WHEN p.plisituacao = 'V' THEN ' Revisado '
                         WHEN p.plisituacao = 'S' THEN ' Cadastrado no SIAFI ' WHEN p.plisituacao = 'R' THEN ' Enviado para Revis�o ' END as situacao,
                    u.usunome ||' por '||to_char(p.plidata, 'dd/mm/YYYY hh24:mi'),
                    COALESCE(a._atinumero||' - '||a.atidescricao, 'N�o atribuido')as atividade
                    FROM monitora.pi_planointerno p
                    LEFT JOIN monitora.pi_planointernoptres pp ON  pp.pliid=p.pliid
                    LEFT JOIN seguranca.usuario u ON u.usucpf = p.usucpf
                    LEFT JOIN monitora.pi_planointernoatividade pa on pa.pliid = p.pliid
                    LEFT JOIN pde.atividade a on a.atiid = pa.atiid
                    WHERE p.plicod='" . $plicod . "' AND p.plistatus = 'A'
                    GROUP BY p.plicod,p.plititulo,u.usunome,p.plidata,p.plisituacao,atividade
                    ORDER BY p.plidata DESC";
        $cabecalho = array("C�digo PI", "T�tulo", "Total PI", "Situa��o", "Dados inser��o", "Atividade");
        $db->monta_lista($sql, $cabecalho, 500, 10, 'N', '', '');
        exit;
    }
}

/* Retorna os dados da Suba��o */

function buscaDadosSubacao($sbaid, $capid = "", $retornarArray = false) {
    global $db;

    $sql = <<<DML
        SELECT
            psa.sbacod,
            psa.sbasigla,
            psa.sbadsc,
            COALESCE(SUM(psd.sadvalor), 0.00) AS dotacao,
            COALESCE(SUM(dpp2.valorpi), 0.00) AS detalhado_pi,
            COALESCE(SUM(sbe.total), 0.00) AS empenhado
        FROM monitora.pi_subacao psa
        LEFT JOIN monitora.pi_subacaodotacao psd USING(sbaid)
        --LEFT JOIN monitora.ptres ptr USING(ptrid)
        LEFT JOIN (
            SELECT
                dpp.sbaid,
                dpp.ptrid,
                SUM(dpp.valorpi) AS valorpi
            FROM monitora.v_pi_detalhepiptres dpp
            GROUP BY dpp.sbaid,dpp.ptrid)
            dpp2 USING(ptrid, sbaid)
        LEFT JOIN siafi.sbaempenho sbe ON(sbe.sbacod = psa.sbacod AND sbe.exercicio = '{$_SESSION['exercicio']}')
        WHERE psa.sbaid = %d
            AND psa.sbaano = '%s'
            --AND ptr.ptrano='{$_SESSION['exercicio']}'
        GROUP BY psa.sbacod,
            psa.sbasigla,
            psa.sbadsc
DML;
    $stmt = sprintf($sql, $sbaid, $_SESSION['exercicio']);
    $subacao = $db->pegaLinha($stmt);

    $categoria = "";
    if ($capid) {
        $sql = "SELECT capdsc FROM monitora.pi_categoriaapropriacao WHERE capid='" . $capid . "'";
        $categoria = $db->pegaUm($sql);
    }
    $arrRetorno = array('sbacod' => $subacao['sbacod']);
    $arrRetorno['sbasigla'] = $subacao['sbasigla'];
    $arrRetorno['categoria'] = $categoria;
    $arrRetorno['sbadsc'] = $subacao['sbadsc'];
    $arrRetorno['dotacao'] = number_format($subacao['dotacao'], 2, ',', '.');
    $arrRetorno['detalhado_pi'] = number_format($subacao['detalhado_pi'], 2, ',', '.');
    $arrRetorno['empenhado'] = number_format($subacao['empenhado'], 2, ',', '.');

    if ($retornarArray) {
        return $arrRetorno;
    }
    echo implode('!@#', $arrRetorno);
}

/* Carregar os enquadramentos para a Suba��o */

function carregarComboEnquadramentoPorSubacao($sbaid) {
    global $db;

    if ($sbaid) {
        $sql = "SELECT ed.eqdid as codigo, ed.eqdcod ||' - '|| ed.eqddsc as descricao
              FROM monitora.pi_enquadramentodespesa ed
                   INNER JOIN monitora.pi_subacaoenquadramento se on ed.eqdid = se.eqdid
              WHERE ed.eqdano='" . $_SESSION['exercicio'] . "' and ed.eqdstatus='A' and se.sbaid=$sbaid
              ORDER BY ed.eqdcod";
        $arDados = $db->carregar($sql);
        if (!$arDados) {
            $arDados = array();
        }
        //die($db->monta_combo('eqdid', $arDados, 'S', 'Selecione', 'atualizarPrevisaoPI', '', '', '240', 'S', 'eqdid', false));
        die($db->monta_combo('eqdid', $arDados, 'S', 'Selecione', 'atualizarPrevisaoPI', null, null, 240, 'N', 'eqdid', null, (isset($eqdid) ? $eqdid : null), null, 'class="form-control chosen-select" style="width=100%;""', null, null));
    }
}

/**
 * Salva os PI's que foram do FNDE na base do ELABREV
 * para ser usado no Termo de Coopera��o Descentralizada
 * @param $unicod
 * @param $plicod
 * @return bool
 */
function _salvarPI_ElabrevTED($unicod, $plicod) {
    global $db;

    if ($unicod != '26298') {
        return false;
    }

    $strSQL = "
        insert into ted.dadosprogramasfnde
            (prgcodfnde, plicod, gescod, tpddoccod, obscod, eventocontabil)
        values('3', '{$plicod}', '61500000000', 'NC', '2', '300300')
    ";

    $db->executar($strSQL);
    $db->commit();
    return true;
}

/**
 * Faz a inser��o do PI no banco de dados do monitora.
 *
 * @global cls_banco $db Conex�o com a base de dados.
 * @param array $dados Array com os dados da requisi��o
 * @param bool $comCommit Se deve ou n�o ser feito um commit ap�s a inser��o.
 * @param bool $criarComoAprovado
 *              Indica que, ao executar uma transa��o do tipo 'E', o status do PI deve ser APROVADO.
 * @return type bool|integer
 */
function salvarPI($dados, $comCommit = true, $criarComoAprovado = false) {
    global $db, $obrigatorias_array;


    /*
     * Ajusta o t�tulo do PI 
     */
    $sql = "SELECT
                UPPER (sbasigla) AS sigla
            FROM
                monitora.pi_subacao
            WHERE
                sbacod = '{$dados['sbacod']}'
            AND sbaano = '{$_SESSION['exercicio']}' LIMIT 1";
    $prefixoTitulo = $db->pegaUm($sql);
    $titulo = explode('-', $dados['plititulo']);
    if (trim($titulo[0]) != $prefixoTitulo) {
        $dados['plititulo'] = $prefixoTitulo .' - '.$dados['plititulo'];
    }

    $unicod = $dados['unicod'] ? $dados['unicod'] : $dados['unicod_disable'];

    /* Apenas para PI de Unidades */
    $cadastroSIAF = 'false';
    if ($dados['plicadsiafi'] == 'T') {
        $cadastroSIAF = 'true';
    }
    $plicodC = strtoupper($dados['plicod']);

    $sql = <<<DML
SELECT *
  FROM monitora.pi_planointerno
  WHERE plicod = '{$plicodC}'
    AND pliano = '{$_SESSION['exercicio']}'
    AND unicod = '{$unicod}'
    AND plistatus = 'A'
DML;
    $plicod = $db->PegaUm($sql);
    if (empty($dados['pliid'])) {
        if ($dados['plicodsubacao']) {
            $subacao = strtoupper($dados['plicodsubacao']);
        } else {
            $subacao = strtoupper(substr($plicod, 1, 4));
        }
        if (!$plicod) {
            $plicod = strtoupper($dados['plicod']);
            $plicod = str_replace(' ', '', $plicod);
            $sql = <<<DML
INSERT INTO monitora.pi_planointerno(
    mdeid,
    eqdid,
    neeid,
    capid,
    sbaid,
    plititulo,
    plicodsubacao,
    plicod,
    plilivre,
    plidsc,
    usucpf,
    unicod,
    ungcod,
    pliano,
    plisituacao,
    plicadsiafi
) VALUES (%d, %d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s')
  RETURNING pliid;
DML;
            $stmt = sprintf(
                    $sql, $dados['mdeid'], $dados['eqdid'], $dados['neeid'], $dados['capid'], $dados['sbaid'], str_replace(array("'"), ' ', $dados['plititulo']), $subacao, $plicod, $dados['plilivre'], str_replace(array("'"), ' ', $dados['plidsc']), $_SESSION['usucpf'], $unicod, $dados['ungcod'], $_SESSION['exercicio'], ($criarComoAprovado ? 'A' : 'H'), $cadastroSIAF);
            $pliid = $db->pegaUm($stmt);

            // -- Associando o pi aos ptres
            associarPIePTRES($pliid, $dados['plivalor'], $dados['plivalored']);
            // -- Inserindo as novas associa��es PI/Enquadramento
            associarPIeEnquadramento($pliid, $dados['m_eqdid']);
        } else {
            echo '<script>alert(\'PI: ' . $plicodC . ' j� cadastrado para esta unidade.\');location.href= \'planacomorc.php?modulo=principal/unidade/cadastro_pi&acao=A\'</script>';
            die();
        }
    } else {
        $sql = <<<DML
UPDATE monitora.pi_planointerno
  SET plititulo = '%s',
      plidsc = '%s',
     plicadsiafi = {$cadastroSIAF}
  WHERE pliid = %d
DML;
        $stmt = sprintf($sql, trim($dados['plititulo']), trim($dados['plidsc']), $dados['pliid']);
        $db->executar($stmt);

        // -- Apagando os enquadramentos j� associados, para uma posterior re-inser��o
        desassociarPIeEnquadramento($dados['pliid']);

        // -- Apagando os ptres j� associados, para uma posterior re-inser��o
        if ($dados['obrigatoria'] != '0') {
            desassociarPIePTRES($dados['pliid']);
        }
        //carregando array com ptres cadastrados para comparar com o array que foi desmarcado.
        $arrPtrids[] = $db->carregarColuna("SELECT ptrid FROM monitora.pi_planointernoptres WHERE pliid = " . $dados['pliid']);
        if (count($dados['ids_apagar']) > 0) {
            foreach ($arrPtrids as $arrPtrid) {
                $arrPtrid = $arrPtrid;
            }
            foreach ($dados['ids_apagar'] as $ptrid) {
                if (in_array($ptrid, $arrPtrid)) {
                    removerPTRESdoPI($dados['pliid'], $ptrid);
                }
            }
        }
        if ($dados['pliid']) {
            $pliidFinal = $dados['pliid'];
        } else {
            $pliidFinal = $pliid;
        }
        //ver($dados);
        // -- Inserindo as novas associa��es PI/PTRES
        associarPIePTRES($pliidFinal, $dados['plivalor'], $dados['plivalored']);
        // -- Inserindo as novas associa��es PI/Enquadramento
        associarPIeEnquadramento($pliidFinal, $dados['m_eqdid']);
    }

    //Salva PI na base do Elabrev para Termos de Coopera��o pertencentes ao FNDE
    _salvarPI_ElabrevTED($unicod, $dados['plicod']);

    if ($comCommit) {
        return $db->commit();
    }

    return $pliid;
}

function associarPIePTRES($pliid, $pliNovosPTRES, $pliPTRESAssociados) {
    //ver($pliid,$pliNovosPTRES, $pliPTRESAssociados,d);
    global $db;

    $sql = <<<DML
INSERT INTO monitora.pi_planointernoptres(pliid, ptrid, pipvalor)
  VALUES(%d, %d, %f)
DML;
    // -- Inserindo dota��es selecionadas agora
    if ($pliNovosPTRES) {
        foreach ($pliNovosPTRES as $valor) {
            $stmt = sprintf(
                    $sql, $pliid, key($valor), str_replace(array('.', ','), array('', '.'), current($valor))
            );
            //ver($stmt,d);
            $db->executar($stmt);
        }
    }

    // -- Inserindo dota��es selecionadas previamente
    if ($pliPTRESAssociados) {
        foreach ($pliPTRESAssociados as $PTRES => $valor) {
            $stmt = sprintf(
                    $sql, $pliid, $PTRES, str_replace(array('.', ','), array('', '.'), $valor)
            );
            $db->executar($stmt);
        }
    }
}

function associarPIeEnquadramento($pliid, $enquadramento) {
    //ver($pliid,$pliNovosPTRES, $pliPTRESAssociados,d);
    global $db;
    if (sizeof($enquadramento) == 1 && $enquadramento[0] == '')
        return;
    $sql = <<<DML
INSERT INTO spo.planointernometapne(pliid, mpneid)
  VALUES
DML;
    // -- Inserindo dota��es selecionadas agora
    if ($enquadramento && count($enquadramento) > 0) {
        foreach ($enquadramento as $valor) {
            if ($valor == '') {
                continue;
            }
            $sql .="($pliid,$valor),";
        }
        $db->carregar(substr($sql, 0, -1));
    }
}

function removerPTRESdoPI($pliid, $ptrid) {
    global $db;
    $sql = <<<DML
DELETE
  FROM monitora.pi_planointernoptres
  WHERE pliid = %d AND ptrid =%d
DML;
    $stmt = sprintf($sql, $pliid, $ptrid);
    //exit($stmt);
    return $db->executar($stmt);
}

function desassociarPIePTRES($pliid) {
    global $db;
    $sql = <<<DML
DELETE
  FROM monitora.pi_planointernoptres
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $pliid);
    $db->executar($stmt);
}

function desassociarPIeEnquadramento($pliid) {
    global $db;
    $sql = <<<DML
DELETE
  FROM spo.planointernometapne
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $pliid);
    $db->executar($stmt);
}

/**
 * Busca os PTRES associados a um PI
 * Query utilizada tamb�m em: simec/monitora/modulos/principal/planotrabalhoUG/listarProgramaUG.inc
 * @global cls_banco $db Conex�o com a base de dados.
 * @param integer $pliid
 * @param integer $sbaid
 * @return bol|array
 *
 * @global cls_banco $db
 * @param integer $pliid
 * @param integer $sbaid
 * @param type $obrigatorio
 * @return array|bol
 */
function buscarPTRESdoPI($pliid, $sbaid, $obrigatorio = '') {
    global $db;

    $params['SELECT'] = <<<SQL
SELECT dtl.ptrid,
       dtl.ptres,
       trim(aca.prgcod || '.' || aca.acacod || '.' || aca.unicod || '.' || aca.loccod || ' - ' || aca.acatitulo) AS descricao,
       uni.unidsc,
       COALESCE(SUM(dtl.ptrdotacao), 0.00) AS dotacaoatual,
       COALESCE(SUM(dt.valor), 0.00) AS det_subacao,
       -- dotacaoinicial - det_subacao
       (COALESCE(SUM(dtl.ptrdotacao), 0.00) - COALESCE(SUM(dt.valor), 0.00)) AS nao_det_subacao,
       COALESCE(SUM(dt2.valorpi), 0.00) AS det_pi,
       -- det_subacao - det_pi
       (COALESCE(SUM(dt.valor), 0.00) - COALESCE(SUM(dt2.valorpi), 0.00)) AS nao_det_pi,
       COALESCE((pemp.total), 0.00) AS empenhado,
       COALESCE(SUM(dtl.ptrdotacao), 0.00) - COALESCE(pemp.total, 0.00) AS nao_empenhado,
       (SELECT pipvalor FROM monitora.pi_planointernoptres WHERE ptrid = dtl.ptrid AND pliid = {$pliid}) as pipvalor
SQL;
    /* Filtros */
    if ($obrigatorio == 'N') {
        $params['obrigatorio'] = 'n';
    }
    $where .= $sbaid ? " AND dt.ptrid IN (SELECT ptrid FROM monitora.pi_subacaodotacao WHERE sbaid = '" . $sbaid . "')" : '';
    $where .= $pliid ? " AND pli.pliid = $pliid " : "";

    /* Parametros para montar a consulta */
    $params['where'] = $where;
    $sql = retornaConsultaPTRES($params);
    $result = is_array($result) ? $result : Array();

    $result = $db->carregar($sql);
    if (is_array($result)) {
        foreach ($result as $key => $_) {
            $result[$key]['dotacaoatual'] = mascaraMoeda($result[$key]['dotacaoatual']);
            $result[$key]['det_subacao'] = mascaraMoeda($result[$key]['det_subacao']);
            $result[$key]['nao_det_subacao'] = mascaraMoeda($result[$key]['nao_det_subacao']);
            $result[$key]['det_pi'] = mascaraMoeda($result[$key]['det_pi']);
            $result[$key]['nao_det_pi'] = mascaraMoeda($result[$key]['nao_det_pi']);
            $result[$key]['empenhado'] = mascaraMoeda($result[$key]['empenhado']);
            $result[$key]['nao_empenhado'] = mascaraMoeda($result[$key]['nao_empenhado']);
            $result[$key]['pipvalor_'] = $result[$key]['pipvalor']; // -- N�o formatado - para soma na interface
            $result[$key]['pipvalor'] = number_format($result[$key]['pipvalor'], 2, ',', '.');
        }
    }
    return $result;
}

/**
 *
 * @global cls_banco $db
 * @param type $dados
 * @return type
 * @todo Ao executar uma transa��o direta, o PI deve ter seu status atualizado para aprovado.
 */
function salvarSolicitacaoPI($dados) {
    global $db;
//ver($dados,d);
    // -- Solicita��es com esta configura��o, implicam em uma transa��o
    // -- direta (uma transa��o de execu��o, sem uma transa��o de solicita��o).
    // -- Este tipo de transa��o deve atender �s seguintes condi��es:
    // -- 1) O usu�rio deve ser superusu�rio, OU
    // -- 2) Os PTRESs selecionados devem ser da mesma UO do usu�rio.
    // -- Se nem 1) ou 2) forem atendidas, a transa��o dever� ser modificada
    // -- para transa��o de solicita��o
    $podeCriarTransacaoDireta = false;
    if (empty($dados['scpid']) && ('E' == $dados['tipotransacao'])) {
//ver($dados,$dados['plivalor'],d);
        // if (!($podeCriarTransacaoDireta = podeCriarTransacaoDireta($dados['plivalor']))) {
//            $dados['tipotransacao'] = 'S';
//        }
    }

    $sql = <<<DML
INSERT INTO planacomorc.solicitacaocriacaopi(
    scpano,
    scptitulo,
    scpdsc,
    scpcod,
    unicod,
    ungcod,
    sbaid,
    scpdotacaosubacao,
    scpdetalhadopisubacao,
    scpempenhadosubacao,
    eqdid,
    neeid,
    capid,
    scplivre,
    mdeid,
    usucpf,
    scpidorigem,
    tipotransacao
) VALUES(%s, %s, '%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s', %s, '%s') RETURNING scpid
DML;

    $dados['plititulo'] = str_replace("'", "''", trim($dados['plititulo']));
    $dados['scpdotacaosubacao'] = formataFloat($dados['scpdotacaosubacao']);
    $dados['scpdetalhadopisubacao'] = formataFloat($dados['scpdetalhadopisubacao']);
    $dados['scpempenhadosubacao'] = formataFloat($dados['scpempenhadosubacao']);

    $stmt = sprintf(
            $sql, $_SESSION['exercicio'], empty($dados['plititulo']) ? 'null' : "'{$dados['plititulo']}'", trim(str_replace("'", "''", $dados['plidsc'])), empty($dados['plicod']) ? 'null' : "'{$dados['plicod']}'", empty($dados['unicod']) ? 'null' : "'{$dados['unicod']}'", empty($dados['ungcod']) ? 'null' : "'{$dados['ungcod']}'", empty($dados['sbaid']) ? 'null' : $dados['sbaid'], empty($dados['scpdotacaosubacao']) ? 'null' : "'{$dados['scpdotacaosubacao']}'", empty($dados['scpdetalhadopisubacao']) ? 'null' : "'{$dados['scpdetalhadopisubacao']}'", empty($dados['scpempenhadosubacao']) ? 'null' : "'{$dados['scpempenhadosubacao']}'", empty($dados['eqdid']) ? 'null' : $dados['eqdid'], empty($dados['neeid']) ? 'null' : $dados['neeid'], empty($dados['capid']) ? 'null' : $dados['capid'], empty($dados['plilivre']) ? 'null' : "'{$dados['plilivre']}'", empty($dados['mdeid']) ? 'null' : $dados['mdeid'], $_SESSION['usucpf'], empty($dados['scpid']) ? 'null' : $dados['scpid'], empty($dados['tipotransacao']) ? 'S' : $dados['tipotransacao']
    );

    if ($scpid = $db->pegaUm($stmt)) {
        // -- Insere as dota��es selecionadas e solicitadas
        solicitacaoPIePTRES($scpid, $dados['plivalor'], $dados['plivalored']);
    }

    if ($dados['scpid']) {
        concluirSolicitacaoOrigem($dados['scpid']);
    }

    // -- Criar o PI
    if ('E' == $dados['tipotransacao']) {
        $pliid = salvarPI($dados, $comCommit = false, $podeCriarTransacaoDireta);
    }

    $success = $db->commit();
#ver($success,d);
    if ($success) {
        if ('S' == $dados['tipotransacao']) { // -- Manda e-mail de solicita��o de PI
            enviaEmailPI(
                    array(
                        'tipoEvento' => 'solCadPI',
                        'scpid' => $scpid
                    )
            );
        } elseif ('E' == $dados['tipotransacao']) { // -- Manda e-mail de homologa��o de PI
            enviaEmailPI(
                    array(
                        'tipoEvento' => 'homCadPI',
                        'scpid' => ($dados['scpid'] ? $dados['scpid'] : $scpid),
                        'pliid' => $pliid
                    )
            );

            // -- Quer dizer que o PI tamb�m foi criado automaticamente, ent�o tem que notificar
            // -- Sobre a aprova��o deste PI
            if ($podeCriarTransacaoDireta) {
                enviaEmailPI(
                        array(
                            'tipoEvento' => 'aprCadPi',
                            'pliid' => $pliid
                        )
                );
            }
        }
    }

    return $success;
}

/**
 * Verifica se o usu�rio que fez a solicita��o pode fazer uma transa��o direta.
 *
 * @global cls_banco $db Conex�o com a base de dados.
 * @param array $PTRESParaInsercao Lista de PTRESs que ser�o inclu�dos no PI.
 * @return boolean
 */
function podeCriarTransacaoDireta(array $PTRESParaInsercao) {
    //ver($PTRESParaInsercao,d);
    global $db;

    // -- Se for super usu�rio pode criar transa��o direta
    if ($_SESSION['superuser']) {
        return true;
    }

    // -- Se n�o for do perfil GO, n�o pode criar transa��o direta
    if (!in_array(PFL_GESTAO_ORCAMENTARIA, pegaPerfilGeral())) {
        return false;
    }

    // -- Contando que o usu�rio fa�a parte do grupo PFL_GESTAO_ORCAMENTARIA,
    // -- devemos verificar se todos os PTRES s�o pertencentes � mesma UG dele.
    $listaPTRES = array();
    foreach ($PTRESParaInsercao as $ptres => $_) {
        $listaPTRES[] = $ptres;
    }
    $query = <<<DML
SELECT usr.unicod
  FROM planacomorc.usuarioresponsabilidade usr
  WHERE usr.usucpf = '%s'
    AND usr.pflcod = %d
    AND usr.rpustatus = 'A'
    AND EXISTS(SELECT 1
                 FROM monitora.ptres ptr
                 WHERE ptr.ptres IN (%s)
                   AND ptr.unicod = usr.unicod)
DML;
    // -- Existe alguma UO do PTRES que n�o esteja na lista de UOs do usu�rio?
    $query = <<<DML
SELECT ptr.unicod
  FROM monitora.ptres ptr
  WHERE ptr.ptres IN (%s)
EXCEPT
SELECT usr.unicod
  FROM planacomorc.usuarioresponsabilidade usr
  WHERE usr.usucpf = '%s'
    AND usr.pflcod = %d
    AND usr.rpustatus = 'A'
DML;
    $stmt = sprintf(
            $query, "'" . implode("', '", $listaPTRES) . "'", $_SESSION['usucpf'], PFL_GESTAO_ORCAMENTARIA
    );

    $uos = (bool) $db->carregar($stmt);
    // -- Verificando a lista de UOs dos PTRESs EXCLU�NDO as UOs do usu�rio
    if ($uos) { // -- Se restou alguma UO, ela n�o est� associada ao Gestor, ent�o n�o deve ser uma transa��o direta
        return false;
    } else { // -- Se n�o restou nenhuma UO, ent�o pode ser criada uma transa��o direta
        return true;
    }
}

function solicitacaoPIePTRES($pliid, $pliNovosPTRES, $pliPTRESAssociados) {
    global $db;
    $sql = <<<DML
INSERT INTO planacomorc.solicitacaopidotacao(scpid, ptrid, spdvalorsolicitado)
  VALUES(%d, %d, %f)
DML;
    // -- Inserindo dota��es selecionadas agora
    if ($pliNovosPTRES) {
        foreach ($pliNovosPTRES as $valor) {
            $stmt = sprintf($sql, $pliid, key($valor), formataFloat(current($valor)));
            $db->executar($stmt);
        }
    }

    // -- Inserindo dota��es selecionadas previamente
    if ($pliPTRESAssociados) {
        foreach ($pliPTRESAssociados as $PTRES => $valor) {
            $stmt = sprintf(
                    $sql, $pliid, $PTRES, str_replace(array('.', ','), array('', '.'), $valor)
            );
            $db->executar($stmt);
        }
    }
}

function concluirSolicitacaoOrigem($scpid) {
    global $db;

    $sql = <<<DML
UPDATE planacomorc.solicitacaocriacaopi
  SET scpprocessado = TRUE
  WHERE scpid = %d
DML;
    $stmt = sprintf($sql, $scpid);
    $db->executar($stmt);
}

function buscarPTRESdaSolicitacao($scpid, $sbaid) {
    global $db;

    $sql = <<<DML
SELECT dtl.ptrid,
       dtl.ptres,
       trim(aca.prgcod || '.' || aca.acacod || '.' || aca.unicod || '.' || aca.loccod || ' - ' ||aca.acadsc) AS descricao,
       COALESCE(SUM(dtl.ptrdotacao), 0.00) AS dotacaoatual,
       COALESCE(SUM(dt.valor), 0.00) AS det_subacao,
       -- dotacaoinicial - det_subacao
       (COALESCE(SUM(dtl.ptrdotacao), 0.00) - COALESCE(SUM(dt.valor), 0.00)) AS nao_det_subacao,
       COALESCE(SUM(dt2.valorpi), 0.00) AS det_pi,
       -- det_subacao - det_pi
       (COALESCE(SUM(dt.valor), 0.00) - COALESCE(SUM(dt2.valorpi), 0.00)) AS nao_det_pi,
       COALESCE((pemp.total), 0.00) AS empenhado,
       COALESCE(SUM(dtl.ptrdotacao), 0.00) - COALESCE(pemp.total, 0.00) AS nao_empenhado,
       sdp.spdvalorsolicitado AS pipvalor
  FROM planacomorc.solicitacaocriacaopi scp
    LEFT JOIN planacomorc.solicitacaopidotacao sdp USING(scpid)
    LEFT JOIN monitora.ptres dtl USING(ptrid)
    left join monitora.acao aca USING(acaid)
    LEFT JOIN monitora.pi_subacaodotacao sd USING(ptrid)
    LEFT JOIN monitora.pi_subacao a ON a.sbaid = sd.sbaid
    LEFT JOIN (SELECT sbaid,
                      ptrid,
                      SUM(sadvalor) AS valor
                 FROM monitora.pi_subacaodotacao
                 GROUP BY sbaid,
                          ptrid) dt ON dtl.ptrid = dt.ptrid AND dt.sbaid = a.sbaid
    LEFT JOIN (SELECT sbaid,
                      ptrid,
                      SUM(dtl.valorpi) AS valorpi
                 FROM monitora.v_pi_detalhepiptres dtl
                 GROUP BY sbaid,
                          dtl.ptrid) dt2 ON dtl.ptrid = dt2.ptrid AND dt2.sbaid = sd.sbaid
    LEFT JOIN siafi.ptrempenho pemp
      ON (pemp.ptres = dtl.ptres AND pemp.exercicio = '{$_SESSION['exercicio']}')
  WHERE scp.scpid = %d
    AND aca.prgano = '%s'
    AND aca.acasnrap = FALSE
    AND scp.sbaid = a.sbaid
  GROUP BY dtl.ptrid,
           dtl.ptres,
           aca.prgcod,
           aca.acacod,
           aca.unicod,
           aca.loccod,
           aca.acadsc,
           sdp.spdvalorsolicitado,
           pemp.total
DML;
    $stmt = sprintf($sql, $scpid, $_SESSION['exercicio']);
    $result = $db->carregar($stmt);
    // -- formatando valores monetarios
    $result = is_array($result) ? $result : Array();
    foreach ($result as $key => $_) {
        $result[$key]['dotacaoatual'] = mascaraMoeda($result[$key]['dotacaoatual']);
        $result[$key]['det_subacao'] = mascaraMoeda($result[$key]['det_subacao']);
        $result[$key]['nao_det_subacao'] = mascaraMoeda($result[$key]['nao_det_subacao']);
        $result[$key]['det_pi'] = mascaraMoeda($result[$key]['det_pi']);
        $result[$key]['nao_det_pi'] = mascaraMoeda($result[$key]['nao_det_pi']);
        $result[$key]['empenhado'] = mascaraMoeda($result[$key]['empenhado']);
        $result[$key]['nao_empenhado'] = mascaraMoeda($result[$key]['nao_empenhado']);
        $result[$key]['pipvalor_'] = $result[$key]['pipvalor']; // -- N�o formatado - para soma na interface
        $result[$key]['pipvalor'] = mascaraMoeda($result[$key]['pipvalor']);
    }

    return $result;
}

function formataFloat($num) {
    return str_replace(array('.', ','), array('', '.'), $num);
}

function carregarTransacao($scpid) {
    global $db;

    $sql = <<<DML
SELECT scp.scpid,
       scp.mdeid,
       mde.mdecod,
       scp.eqdid,
       eqd.eqdcod,
       scp.neeid,
       nee.neecod,
       scp.capid,
       cap.capcod,
       scp.sbaid,
       TRIM(scp.scptitulo) AS plititulo,
       scp.scpdsc AS plidsc,
       TRIM(scp.scplivre) AS plilivre,
       scp.unicod,
       scp.ungcod,
       sba.sbasigla || ' - ' AS sbasigla,
       sba.sbacod,
       scp.scpdotacaosubacao AS dotacao,
       scp.scpdetalhadopisubacao AS detalhado_pi,
       scp.scpempenhadosubacao AS empenhado,
       scp.tipotransacao
  FROM planacomorc.solicitacaocriacaopi scp
    LEFT JOIN monitora.pi_subacao sba
      ON (scp.sbaid = sba.sbaid AND scp.scpano = sba.sbaano)
    LEFT JOIN monitora.pi_enquadramentodespesa eqd
      ON (scp.eqdid = eqd.eqdid AND scp.scpano = eqd.eqdano)
    LEFT JOIN monitora.pi_niveletapaensino nee
      ON (scp.neeid = nee.neeid AND scp.scpano = nee.neeano)
    LEFT JOIN monitora.pi_categoriaapropriacao cap
      ON (scp.capid = cap.capid AND scp.scpano = cap.capano)
    LEFT JOIN monitora.pi_modalidadeensino mde
      ON (scp.mdeid = mde.mdeid AND scp.scpano = mde.mdeano)
  WHERE scpid = %d
DML;
    $stmt = sprintf($sql, $scpid);
    $return = $db->pegaLinha($stmt);

    // -- Formatando dados monetarios
    $return['dotacao'] = number_format($return['dotacao'], 2, ',', '.');
    $return['detalhado_pi'] = number_format($return['detalhado_pi'], 2, ',', '.');
    $return['empenhado'] = number_format($return['empenhado'], 2, ',', '.');

    return $return;
}

function carregarPI($pliid) {
    global $db;

    $sql = <<<DML
SELECT pli.pliid,
       pli.mdeid,
       mde.mdecod,
       pli.eqdid,
       eqd.eqdcod,
       pli.neeid,
       nee.neecod,
       pli.capid,
       cap.capcod,
       pli.sbaid,
       pli.plititulo,
       pli.plicodsubacao,
       pli.plicod,
       pli.plilivre,
       pli.plidsc,
       pli.unicod,
       pli.ungcod,
       pli.pliano,
       pli.plicadsiafi,
       to_char(pli.plidata, 'dd/mm/YYYY') as plidata,
       CASE plisituacao
           WHEN 'A' THEN 'Aprovado'
           WHEN 'E' THEN 'Enviado para revisao'
           WHEN 'P' THEN 'Pendente'
           WHEN 'C' THEN 'Cadastrado no SIAFI'
           WHEN 'R' THEN 'Revisado'
           WHEN 'H' THEN 'Homologado'
           WHEN 'T' THEN '<span style="color:red">Cadastrado no SIAFI</span>'
             ELSE 'Tendencioso'
         END AS plisituacao,
       sba.sbaid,
       sba.sbasigla || ' - ' AS sbasigla,
       sba.sbacod
  FROM monitora.pi_planointerno pli
    LEFT JOIN monitora.pi_subacao sba
      ON (pli.sbaid = sba.sbaid AND pli.pliano = sba.sbaano)
    LEFT JOIN monitora.pi_enquadramentodespesa eqd
      ON (pli.eqdid = eqd.eqdid AND pli.pliano = eqd.eqdano)
    LEFT JOIN monitora.pi_niveletapaensino nee
      ON (pli.neeid = nee.neeid AND pli.pliano = nee.neeano)
    LEFT JOIN monitora.pi_categoriaapropriacao cap
      ON (pli.capid = cap.capid AND pli.pliano = cap.capano)
    LEFT JOIN monitora.pi_modalidadeensino mde
      ON (pli.mdeid = mde.mdeid)
      --ON (pli.mdeid = mde.mdeid AND pli.pliano = mde.mdeano) -- Resolver 2014
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $pliid);
    //ver($stmt);
    return $db->pegaLinha($stmt);
}

function carregarEnquadramentoPI($pliid) {
    global $db;

    $sql = <<<DML
    SELECT
        mpneid as codigo
    FROM spo.planointernometapne
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $pliid);
    $dados = $db->carregar($stmt);
    $new = array();
    if ($dados) {
        foreach ($dados as $key => $v) {
            $new[] = $v['codigo'];
        }
        $dados = $new;
    }

    return $dados;
}

function inativarPI($dados) {
    global $db;
    $sql = <<<DML
UPDATE monitora.pi_planointerno
  SET plistatus = 'I'
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $dados['pliid']);
    $db->executar($stmt);
    return $db->commit();
}

/**
 * Verifica se um campo da requisi��o foi definido e se tem valor.
 * @param string $campo Nome do campo para verifica��o no $_REQUEST.
 * @return bool
 */
function validaRequisicao($campo) {
    return (isset($_REQUEST[$campo]) && !empty($_REQUEST[$campo]));
}

/**
 * Constroi a query que preenche o filtro de UO de acordo com a sess�o do usu�rio.
 * Funciona para os dois perfis (GESTAO_ORC e GABIN), retornando a instru��o SQL.
 * @param bool $semFiltroPerfil Indica se o filtro de perfil deve ser aplicado.
 * @return string
 */
function consultaUOs($semFiltroPerfil = false) {
    if (!$semFiltroPerfil) {
        if (in_array(PFL_GABINETE, pegaPerfilGeral($_SESSION['usucpf']))) {
            $filtroPerfilUO = <<<SQL
 AND EXISTS (SELECT 1
               FROM planacomorc.usuarioresponsabilidade urp
                 INNER JOIN public.unidadegestora ung USING(ungcod)
               WHERE ung.unicod = uni.unicod
                 AND urp.pflcod = %d
                 AND urp.usucpf = '%s'
                 AND urp.rpustatus = 'A')
SQL;
            $filtroPerfilUO = sprintf($filtroPerfilUO, PFL_GABINETE, $_SESSION['usucpf']);
        } elseif (in_array(PFL_GESTAO_ORCAMENTARIA, pegaPerfilGeral())) {
            $filtroPerfilUO = <<<SQL
  AND EXISTS (SELECT 1
                FROM planacomorc.usuarioresponsabilidade urp
                WHERE uni.unicod = urp.unicod
                  AND urp.pflcod = %d
                  AND urp.usucpf = '%s'
                  AND urp.rpustatus = 'A')
SQL;
            $filtroPerfilUO = sprintf($filtroPerfilUO, PFL_GESTAO_ORCAMENTARIA, $_SESSION['usucpf']);
        } elseif (in_array(PFL_NAO_OBRIGATORIAS, pegaPerfilGeral())) {
            $filtroPerfilUO .= <<<SQL
  AND uni.unicod NOT IN(%s)
SQL;
            $filtroPerfilUO = sprintf($filtroPerfilUO, UNIDADES_OBRIGATORIAS);
        }
    }


    $sql = <<<DML
SELECT uni.unicod AS codigo,
       uni.unicod || ' - ' || uni.unidsc AS descricao
  FROM public.unidade uni
  WHERE uni.unistatus = 'A'
    AND orgcod = '26000'
     {$filtroPerfilUO}
  ORDER BY uni.unicod
DML;
    return sprintf($sql, UNIDADES_OBRIGATORIAS);
}

function vincular($dados) {
    global $db;

    $pliid = $dados['pliid'];
    $plisituacao = $dados['situacao'];

    // -- Atualizando a situa��o do PIs
    $sql = <<<DML
UPDATE monitora.pi_planointerno
  SET plisituacao = '%s'
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, $plisituacao, $pliid);
    $db->executar($stmt);

    // -- Gravando o hist�rico da atualiza��o
    $sql = <<<DML
INSERT INTO monitora.pi_planointernohistorico(
    pliid,
    plicod,
    pihobs,
    pihdata,
    usucpf,
    pihsituacao,
    plicodorigem)
SELECT pli.pliid, pli.plicod, NULL, NOW(), '%s', '%s', pli.plicod
  FROM monitora.pi_planointerno pli
  WHERE pli.pliid = %d
DML;
    $stmt = sprintf($sql, $_SESSION['usucpf'], $plisituacao, $pliid);

    $db->executar($stmt);

    if (!$db->commit()) {
        $db->rollback();
        return;
    }

    // -- Notificando os gestores
    if ('A' == $plisituacao) {
        enviaEmailPI(
                array(
                    'tipoEvento' => 'aprCadPi',
                    'pliid' => $pliid
                )
        );
    }

    echo 'Ok!';
}

function piAtualizadoSiafi($dados) {
    global $db;
    $sql = <<<DML
UPDATE monitora.pi_planointerno
  SET plisituacao = 'C'
  WHERE pliid = %d
DML;
    $stmt = sprintf($sql, (int) $dados['pliid']);
    $db->executar($stmt);
    return $db->commit();
}

/**
 * Lista de PIs das UOS para MANTER
 */
function listaPisUoManter($dados) {
    global $db;
    $perfis = pegaPerfilGeral();

    $obrigatorias = UNIDADES_OBRIGATORIAS;
    /* Filtros */
    $where = '';
    $where .= $dados['pi'] ? " AND pli.plicod ilike '%" . $_REQUEST['pi'] . "%' " : "";
    $where .= $dados['unicod'] ? " AND (pli.unicod = '{$dados['unicod']}' OR pli.ungcod IN (select ungcod from public.unidadegestora where unicod = '{$dados['unicod']}') )" : "";
    $where .= $_REQUEST["ptres"][0] ? " AND ptres.ptrid in ('" . implode("','", $_REQUEST["ptres"]) . "') " : "";
    $titulodescricaoTmp = removeAcentos(str_replace("-", "", $_REQUEST['titulodescricao']));
    $where .= $dados['titulodescricao'] ? " AND (UPPER(public.removeacento(pli.plititulo)) ilike '%" . $titulodescricaoTmp . "%' OR UPPER(public.removeacento(pli.plidsc)) ilike '%" . $titulodescricaoTmp . "%')" : "";

    /* Filtrando apenas os PIs das UOs */
    if (in_array(PFL_GESTAO_ORCAMENTARIA_IFS, $perfis)) {
        $sqlUO = <<<DML
EXISTS (SELECT 1
         FROM planacomorc.usuarioresponsabilidade rpu
         WHERE rpu.usucpf = '%s'
           AND rpu.pflcod = %d
           AND rpu.rpustatus = 'A'
           AND rpu.unicod  = uni.unicod)
DML;
        $whereUO = sprintf($sqlUO, $_SESSION['usucpf'], PFL_GESTAO_ORCAMENTARIA_IFS);
        $whereUO = " AND {$whereUO}";
        $where .= $whereUO;
    }

    /* Apenas as UOs n�o Obrigat�rias */
    $where .= " AND uni.unicod NOT IN ($obrigatorias) ";

    /* Caso seja selecionado apenas 1 PTRES, mostrar as colunas dos dados apenas para aquele PTRES */
    $sqlAdicional = "";
    if ($_REQUEST["ptres"][0] && count($_REQUEST["ptres"]) == 1) {
        $dados = $db->carregar("SELECT ptres FROM monitora.ptres WHERE ptrid = {$_REQUEST["ptres"][0]}");
        $ptres = $dados[0]['ptres'];
        #ver($_REQUEST["ptres"][0],d);
        $sqlAdicional.=", COALESCE((SELECT
                                        pip.pipvalor
                                    FROM
                                        monitora.pi_planointernoptres pip
                                    LEFT JOIN
                                        monitora.ptres
                                    ON
                                        pip.ptrid = ptres.ptrid
                                    WHERE
                                        ptres.ptrano = '{$_SESSION['exercicio']}'
                                    AND pip.pliid= gmb.pliid
                                    AND ptres.ptrid={$_REQUEST["ptres"][0]}),0.00) as dotacao_pip_ptres
                        ,        COALESCE((SELECT
                                    total
                                FROM
                                    siafi.pliptrempenho ppe
                                WHERE
                                    plicod = gmb.codigo
                                    AND ppe.exercicio = '{$_SESSION['exercicio']}'
                                AND ptres = '{$ptres}'),0.00) as empenhado_pi_ptres
                        ,COALESCE((SELECT
                                        pip.pipvalor
                                    FROM
                                        monitora.pi_planointernoptres pip
                                    LEFT JOIN
                                        monitora.ptres
                                    ON
                                        pip.ptrid = ptres.ptrid
                                    WHERE
                                        ptres.ptrano = '{$_SESSION['exercicio']}'
                                    AND pip.pliid= gmb.pliid
                                    AND ptres.ptrid={$_REQUEST["ptres"][0]}),0.00) -
                                COALESCE((SELECT
                                    total
                                FROM
                                    siafi.pliptrempenho ppe
                                WHERE
                                    plicod = gmb.codigo
                                    AND ppe.exercicio = '{$_SESSION['exercicio']}'
                                AND ptres = '{$ptres}'),0.00) AS nao_empenhado_tpi_ptres";
    }
    /* Cabe�alho da Consulta */

    $acoes = " '-' ";
    $acoes = <<<SQL
        gmb.pliid
SQL;

    $params['SELECT'] = <<<SQL
SELECT
    {$acoes}
    AS acoes,

        gmb.codigo ||' ',
        CASE WHEN trim(gmb.titulo) is not null THEN
            gmb.titulo  || '<input type=\"hidden\" id=\"plititulo[' || gmb.pliid || ']\" value=\"' || gmb.codigo || ' - ' || gmb.titulo || '\">'
        ELSE
            'N�o Preenchido <input type=\"hidden\" id=\"plititulo[' || gmb.pliid || ']\" value=\" ' || gmb.codigo ||' - N�o Preenchido\"/>'
        END as titulo,
        COALESCE(uni.unidsc, ung.ungabrev) as unidsc,
        CASE WHEN obrid IS NULL THEN '-' ELSE 'SIM' END as obras,
        CASE WHEN pli.plicadsiafi = 't' THEN 'Sim' ELSE 'N�o' END as cadastroSIAF,
        COALESCE((SELECT
                                        SUM(pip.pipvalor)
                                    FROM
                                        monitora.pi_planointernoptres pip
                                    LEFT JOIN
                                        monitora.ptres
                                    ON
                                        pip.ptrid = ptres.ptrid
                                    WHERE
                                        ptres.ptrano = '{$_SESSION['exercicio']}'
                                    AND pip.pliid= gmb.pliid
                                    ),0.00) as dotacao_total,
        empenhado AS empenhado_total,
        COALESCE((SELECT
                                        SUM(pip.pipvalor)
                                    FROM
                                        monitora.pi_planointernoptres pip
                                    LEFT JOIN
                                        monitora.ptres
                                    ON
                                        pip.ptrid = ptres.ptrid
                                    WHERE
                                        ptres.ptrano = '{$_SESSION['exercicio']}'
                                    AND pip.pliid= gmb.pliid
                                    ),0.00) - empenhado AS nao_empenhado_total
        {$sqlAdicional}
SQL;
    $params['where'] = " $where ";
    $sql = retornaConsultaPI($params);

    /* Busca alternativa para recuperar os PIs que possuem Empenho mas n�o est�o cadastrados no SIMEC
     * POR UO */
    if ($dados['apenasEmpenhadosUo'] == 'true') {
        $where = $dados['unicod'] ? " AND sld.unicod = '{$dados['unicod']}' " : "";
        #ver($where,d);
        $sql = <<<SQL
SELECT
    '-' as acao,
    plicod ,
    'N�o Informado.' as titulo,
    unicod || ' - ' || uni.unidsc as unidsc,
    'N�o Informado.' as obras,
    0 as orcamento,
    total as empenhado,
    0 as nao_empenhado
FROM
    dblink
    (
    'dbname=dbsimecfinanceiro hostaddr=192.168.222.21 user=seguranca password=phpsegurancasimec port=5432'
    ,
    '
    SELECT
    sld.plicod,
    sld.unicod,
    SUM(
    CASE
    WHEN sld.sldcontacontabil IN (''292130100'',
    ''292130201'',
    ''292130202'',
    ''292130203'',
    ''292130301'')
    THEN
    CASE
    WHEN sld.ungcod=''154004''
    THEN (sld.sldvalor)*2.2088
    ELSE (sld.sldvalor)
    END
    ELSE 0
    END ) AS total
    FROM
    dw.saldo{$_SESSION['exercicio']} sld
    WHERE
    sld.sldcontacontabil IN (''292130100'',
    ''292130201'',
    ''292130202'',
    ''292130203'',
    ''292130301'')
    AND plicod IS NOT NULL
    AND plicod <> ''''
    AND LENGTH(sld.plicod) = 11
    AND sld.unicod NOT IN (''26101'', ''26298'', ''26291'', ''26443'', ''26290'', ''74902'')
    AND  SUBSTR(sld.unicod , 1, 2)  = ''26''
    $where
    GROUP BY
    sld.plicod, sld.unicod
    ORDER BY   sld.unicod  ;
    '
        ) AS pisiafi ( plicod VARCHAR(15), unicod VARCHAR(5), total NUMERIC(15,2) )
   left join public.unidade uni USING(unicod)
   WHERE
        plicod NOT IN
        (
            SELECT
                plicod
            FROM
                monitora.pi_planointerno pli
            WHERE
                pli.pliano = '{$_SESSION['exercicio']}')

SQL;
    }

    $cabecalho = array("A��o",
        "C�digo",
        "T�tulo",
        "Unidade",
        "Obras",
        "Cadastrado SIAF",
        "Or�amento Total do PI (R$)",
        "Empenhado Total do PI (R$)",
        "N�o Empenhado Total do PI (R$)");
    /* Caso seja selecionado apenas 1 PTRES, mostrar as colunas dos dados apenas para aquele PTRES */
    if ($_REQUEST["ptres"][0] && count($_REQUEST["ptres"]) == 1) {
        array_push($cabecalho, "Dota��o para este PTRES (R$)", "Empenhado neste PTRES (R$)", "N�o Empenhado neste PTRES");
    }
    $db->monta_lista_ordenaGROUPBY($sql, $cabecalho, 200, 5, 'S', 'center');
}

function botaoEnviarRevisao() {
    ?>
    <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="modalRevisao()">&nbsp;&nbsp;Revis�o&nbsp;</button>
    <br style="clear:both" />
    <br style="clear:both" />
    <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="trocarSituacao('C')">Atualizado<br />no SIAFI</button>
    <?php
}

function botaoTornarPendente() {
    ?>
    <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="trocarSituacao('P')">Tornar<br />Pendente</button>
    <?php
}

/**
 * Exibe o bot?o de aprovar de acordo com as permiss?es do usu?rio.
 *
 * @param array $unicodPI UO de quem solicitou o PI
 * @param array $perfis
 * @param array $unicodsResponsabilidade
 * @param type $pflCode
 * @param type $arUOdoPTRES
 */
function botaoAprovar(array $unicodPI, array $perfis, array $unicodsResponsabilidade, $pflCode, $arUOdoPTRES) {
    $podeAprovar = false;
    if (1 == $_SESSION ['superuser']) {
        $podeAprovar = true;
    } elseif (in_array($pflCode, $perfis)) { // -- Verifica se ? o perfil que pode aprovar PIs
        if (1 == count($unicodPI) && in_array(array_pop($unicodPI), $unicodsResponsabilidade)) {
            // -- Se a ?NICA UO do PI estiver dentro do conjunto UO-RESPONSABILIDADE, deixa aprovar o PI
            $podeAprovar = true;
        }
    }

    if ($podeAprovar) {
        ?>
        <br style="clear:both" />
        <br style="clear:both" />
        <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="trocarSituacao('A')">&nbsp;&nbsp;Aprovar&nbsp;&nbsp;</button>
        <?php
    }
}

function botaoCadastrarSIAFI($pliid) {
    ?>
    <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="trocarSituacao('C')">Cadastrar<br />no SIAFI</button>
    <?php
}

function botaoAtualizarSIAFI($pliid) {
    ?>
    <button type="button" class="btn btn-primary" style="color:#FFBF00;font-weight:bold" onclick="trocarSituacao('T')">Cadastrar<br />no SIAFI</button>
    <br style="clear:both" />
    <br style="clear:both" />
    <button type="button" class="btn btn-primary" style="font-weight:bold" onclick="trocarSituacao('C')">Atualizado<br />no SIAFI</button>
    <?php
}
