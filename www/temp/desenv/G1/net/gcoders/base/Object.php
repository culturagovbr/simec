<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
//
// +------------------------------------------------------------------------+
// | G1 - MVC Framework for PHP5                                            |
// | Copyright (c) 2005 The GCoders Group                                   |
// | All Right Reserved                                                     |
// +------------------------------------------------------------------------+
// | The contents of this file are subject to the Mozilla Public License    |
// | Version 1.1 (the "MPL"); you may not use this file except in           |
// | compliance with the License. You may obtain a copy of the License at   |
// | http://www.mozilla.org/MPL/                                            |
// |                                                                        |
// | Software distributed under the License is distributed on an "AS IS"    |
// | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See    |
// | the License for the specific language governing rights and limitations |
// | under the License.                                                     |
// |                                                                        |
// | The Original Code is The GCoders Group.                                |
// | The Initial Developer of the Original Code is:                         |
// |     Douglas Gontijo <douglas@gcoders.net>                              |
// |                                                                        |
// | Alternatively, the contents of this file may be used under the terms   |
// | of the BSD License (BSD License), in which case the provisions of BSD  |
// | License are applicable instead of those above.                         |
// |                                                                        |
// | If you wish to allow use of your version of this file only under the   |
// | terms of the BSD License and not to allow others to use your version   |
// | of this file under the MPL, indicate your decision by deleting the     |
// | provisions above and replace them with the notice and other provisions |
// | required by the BSD License                                            |
// |                                                                        |
// | If you do not delete the provisions above, a recipient may use your    |
// | version of this file under either the MPL or the BSD License.          |
// +------------------------------------------------------------------------+
//
// $Id$
//


/**
 * @package net.gcoders.base
 * @class Object
 */


/**
 * @class Object
 *
 *
 * @version $Revision$
 * @author  Douglas Gontijo <douglas@gcoders.net>
 */
abstract class Object {
    //-------------------------------------------------------------- constants


    //------------------------------------------------------------- properties


    //----------------------------------------------------------------- public


    //-------------------------------------------------------------- protected


    //---------------------------------------------------------------- private


}





