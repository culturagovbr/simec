<?php

function getmicrotime()
{list($usec, $sec) = explode(" ", microtime());
 return ((float)$usec + (float)$sec);} 

date_default_timezone_set ('America/Sao_Paulo');

$_REQUEST['baselogin'] = "simec_espelho_producao";

/* configura��es */
ini_set("memory_limit", "2048M");
set_time_limit(0);
/* FIM configura��es */

// carrega as fun��es gerais
//include_once "/var/www/simec/global/config.inc";
include_once "config.inc";
include_once APPRAIZ . "includes/classes_simec.inc";
include_once APPRAIZ . "includes/funcoes.inc";

include "_constantes.php";
include "_funcoes_escola.php";

// CPF do administrador de sistemas
if(!$_SESSION['usucpf'])
$_SESSION['usucpforigem'] = '00000000191';


// abre conex�o com o servidor de banco de dados
$db = new cls_banco();

$sql = "select * from sismedio.listaescolasensinomedio l 
		left join seguranca.usuario u on u.usucpf = l.lemcpfgestor 
		left join workflow.documento d on d.docid = l.docid 
		where lemuf NOT IN('SP') and (esdid='".ESD_FLUXOESCOLA_EMELABORACAO."' OR esdid IS NULL) and lemcpfgestor is not null";

$listaescolasensinomedio = $db->carregar($sql);

if($listaescolasensinomedio[0]) {
	foreach($listaescolasensinomedio as $lee) {
		
		$arr = array('usucpf'   => $lee['lemcpfgestor'],
					 'usunome'  => $lee['lemnomegestor'],
					 'usuemail' => $lee['lememailgestor'],
					 'lemcodigoinep' => $lee['lemcodigoinep'],
					 'suscod'   => 'A',
					 'naoredirecionar' => true);
		
		inserirGestorEscolaGerenciamento($arr);
		
		echo $lee['lemcpfgestor']."<br>";
		
		/*
		 * ENVIANDO EMAIL CONFIRMANDO O PROCESSAMENTO
		*/
		require_once APPRAIZ . 'includes/phpmailer/class.phpmailer.php';
		require_once APPRAIZ . 'includes/phpmailer/class.smtp.php';
		$mensagem = new PHPMailer();
		$mensagem->persistencia = $db;
		$mensagem->Host         = "localhost";
		$mensagem->Mailer       = "smtp";
		$mensagem->FromName		= "SIMEC - SISM�dio";
		$mensagem->From 		= "pactoensinomedio@mec.gov.br";
		$mensagem->AddAddress($lee['lememailgestor'], $lee['lemnomegestor']);
		$mensagem->Subject = "SISM�dio - Ativa��o do acesso ao SIMEC - SISM�dio";
		
		$mensagem->Body = "<p>Prezado(a) ".$lee['lemnomegestor'].",</p>
						   <p>Voc� foi cadastrado no SisM�dio como diretor(a) da ".$lee['lemnomeescola'].". Para acessar o sistema e informar os professores que participar�o do Pacto Nacional pelo Fortalecimento do Ensino M�dio, digite simec.mec.gov.br, preencha o campo indicado com o seu CPF e a senha ".(($lee['ususenha'])?md5_decrypt_senha( $lee['ususenha'], '' ):"simecdti").". Para conhecer o passo a passo, leia em cada tela as 'Orienta��es' ou acesse o manual do SisM�dio na p�gina inicial do SIMEC.</p>
							<p>
							- Caso exceda o n�mero de tentativas e o seu acesso seja bloqueado, por favor, aguarde 24 horas e acesse novamente o SIMEC utilizando a senha informada na mensagem. Observe se a tecla CAPS LOCK ou FIXA do seu teclado est� desativada.<br>
							- Caso voc� n�o seja mais o diretor dessa escola, por favor, pe�a que a Secretaria de Educa��o atualize as informa��es do gestor atual.<br>
							</p>
							
							<p>Em caso de d�vida, por favor, envie um e-mail para pactoensinomedio@mec.gov.br.</p> 
							
							<p>Secretaria de Educa��o B�sica<br>
							Minist�rio da Educa��o</p>";
		
		$mensagem->IsHTML( true );
		$mensagem->Send();
		/*
		 * FIM
		* ENVIANDO EMAIL CONFIRMANDO O PROCESSAMENTO
		*/
		
		
	}
	
}

?>
