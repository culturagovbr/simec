<?php
define('PFL_SUPERUSUARIO', 1340); //Novo
define("PFL_COORDENADORACAO", 1362); //Novo
define("PFL_VALIDADORACAO", 1363); //Novo
define("PFL_CGP_GESTAO", 1364); //Novo
define("PFL_CGP_OPERACIONAL", 1393); //Novo
define("PFL_COORDENADORSUBACAO", 1351); //Novo
define("PFL_CONSULTA", 1395); //Novo
define("PFL_GABINETE", 1396); //Novo
define("PFL_NAO_OBRIGATORIAS", 1207);
define("PFL_VALIDADOR_SUBSTITUTO", 1398);//Novo
define("PFL_GESTAO_ORCAMENTARIA", 1397);//Novo
define("PFL_GESTAO_ORCAMENTARIA_IFS", 1399);//Novo
define('PFL_RELATORIO_TCU', 1365); //Novo
define('PFL_MONITOR_INTERNO', 1405); //Novo

define('TPDID_RELATORIO_TCU', 203);
define('TPDID_ACOMPANHAMENTO_SUBACAO', 151);
define('TPDID_ACOMPANHAMENTO_ACAO', 119);
define('ESDID_TCU_EM_PREENCHIMENTO', 1292);
define('ESDID_TCU_ANALISE_SPO', 1293);
define('ESDID_TCU_ACERTOS_UO', 1294);
define('ESDID_TCU_CONCLUIDO', 1295);

define('ESDID_ACP_ACAO_EM_PREENCHIMENTO', 1547);
define('ESDID_ACP_ACAO_EM_VALIDACAO', 1544);
define('ESDID_ACP_ACAO_EM_APROVACAO', 1545);
define('ESDID_ACP_ACAO_FINALIZADO', 1543);
define('ESDID_ACP_ACAO_ENVIADO_SIOP', 1546);

/**
 * Identifica o nome do sistema. Utilizado para armazenar dados na sess�o.
 */
define('MODULO', $_SESSION['sisdiretorio']);

define("UNIDADES_OBRIGATORIAS", "'26101','26291', '26290', '26298', '26443', '74902', '73107'");
