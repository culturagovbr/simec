<?php 

// constantes dos perfis do sistema
define( "IES_SUPERUSUARIO", 	  312 );
define( "IES_ADMINISTRADOR", 	  328 );
define( "IES_VALIDACAOSESU", 	  329 );
define( "IES_COMISSAOAVALIADORA", 330 );
define( "IES_CADASTRADORIES", 	  331 );
define( "IES_CONSULTAGERAL", 	  332 );
define( "IES_CONSULTAIES", 		  333 );

// constantes do Workflow
define( "NAO_INICIADO", 					144 );
define( "AGUARDANDO_VALIDACAO_CRITERIOS", 	145 );
define( "EM_PREENCHIMENTO_PROJETO", 		146 );
define( "EM_ANALISE", 						147 );
define( "AGUARDANDO_APROVACAO", 			148 );
define( "EM_PREENCHIMENTO_RECONSIDERACAO",  149 );
define( "EM_ANALISE_RECONSIDERACAO", 		150 );

define( "IES_TIPO_DOCUMENTO", 15 );

define( "IES_FUNID_SUPERIOR", 	 12 );
define( "IES_FUNID_RESPONSAVEL", 59 );

?>