<?php

	// �rg�os do m�dulo de obras
	define('ORGAO_SESU', 1);
	define('ORGAO_SETEC', 2);
	define('ORGAO_FNDE', 3);

	// Constatens dos ID's dos m�dulos que usam o m�dulo de obras
	define('ID_OBRAS', 15);
	define('ID_PARINDIGENA', 32);
	
	// Constantes dos tipos de forma de repasse de recursos
	define('TFR_CONVENIO', 2);
	define('TFR_DESCENTRALIZACAO', 3);
	define('TFR_REC_PROPRIO', 4);
	
	// Constantes das fun��es das entidades
	define('ID_UNIVERSIDADE',12);
	define('ID_ESCOLAS_TECNICAS',14);
	
?>