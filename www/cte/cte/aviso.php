<b>Por que elaborar o Plano de A��es Articuladas (PAR)?</b> 
<p>
	A partir do lan�amento do Plano de Desenvolvimento da Educa��o (PDE), em
	2007, todas as transfer�ncias volunt�rias e assist�ncia t�cnica do MEC aos
	munic�pios, estados e Distrito Federal est�o vinculadas � ades�o ao
	Compromisso Todos pela Educa��o e � elabora��o do PAR - instrumentos que s�o
	fundamentais para a melhoria do Ideb.
</p>
<ul>
	<li>
		Assinar o
		<a
			title="Termo de Ades�o ao Compromisso Todos pela Educa��o"
			href="http://portal.mec.gov.br/index.php?option=content&task=view&id=613&Itemid=933&sistemas=1"
		>Termo de Ades�o ao Compromisso Todos pela Educa��o</a>
	</li>
	<li>
		Realizar o diagn�stico da situa��o da educa��o no munic�pio e em seguida o (PAR)
	</li>
	<li>
		Inserir os dados do PAR no SIMEC - Sistema de Monitoramento do MEC (
		<a
			title=link
			href="http://portal.mec.gov.br/arquivos/pdf/par_simec.pdf"
		>Siga as instru��es sobre como inserir os dados no SIMEC</a>)
	</li>
</ul>
<p>
	- &nbsp;Os 1.242 munic�pios relacionados na
	<a
		title="Resolu��o CD/FNDE n. 29, de 20 de junho de 2007"
		href="http://portal.mec.gov.br/arquivos/pdf/r29_20062007.pdf"
	>Resolu��o CD/FNDE n\ 29, de 20 de junho de 2007</a>, ser�o acompanhados
	pelos consultores do MEC na realiza��o do diagn�stico e na elabora��o do PAR.
</p>
<p>
	- Os demais munic�pios s�o convidados a elaborar o
	<a
		title="Diagn�stico PAR"
		href="http://portal.mec.gov.br/arquivos/pdf/diagnostico_par.pdf"
	>diagn�stico</a> e o
	<a
		title="PAR "
		href="http://portal.mec.gov.br/arquivos/pdf/par.pdf"
	>Plano de A��es Articuladas</a> PAR de forma aut�noma, contando para isso com o
	<a
		title="Manual passo a passo "
		href="http://portal.mec.gov.br/arquivos/pdf/manual_passoapasso.pdf"
	>manual passo a passo</a> e com o
	<a
		title="Guia Pr�tico de A��es"
		href="http://portal.mec.gov.br/arquivos/pdf/guia_pratico_9.pdf"
	>Guia Pr�tico de A��es</a>.
	<br/><br/>
	-&nbsp;Os Estados tamb�m s�o convidados a realizar o
	<a
		title="Diagn�stico dos estados"
		href="http://portal.mec.gov.br/arquivos/pdf/acs_ctpe.pdf"
	>diagn�stico</a> e elaborar o
	<a
		title="PAR Padr�o"
		href="http://portal.mec.gov.br/arquivos/pdf/par_padrao.pdf"
	>Plano de A��es Articuladas</a> de forma aut�noma, dispondo para isso de um
	Instrumento diagn�stico espec�fico, que se refere tanto ao Ensino Fundamental
	quanto ao Ensino M�dio.&nbsp;
	<br/>
</p>
<ul>
	<li>Imprimir e encaminhar o PAR assinado para o seguinte endere�o:</li>
</ul>
<p>
	Fundo Nacional de Desenvolvimento da Educa��o
	<br/>
	Diretoria de Programas e Projetos Educacionais
	<br/>
	SBS - Quadra 2 - Bloco F - Edif�cio �urea - Sobreloja - Sala 7
	<br/>
	Bras�lia - DF, CEP 70070-929
	<br/>
	&nbsp;
	<br/>
</p>
<ul>
	<li>
		Leia a �ntegra do
		<a
			title="Decreto 6.094/2007"
			href="http://www.planalto.gov.br/ccivil_03/_Ato2007-2010/2007/Decreto/D6094.htm"
		>Decreto 6.094 de 24 de abril de 2007</a>, que disp�e sobre a implementa��o
		do Plano de Metas Compromisso Todos pela Educa��o
	</li>
	<li>
		Leia a �ntegra da
		<a
			title="Resolu��o n. 47, 20/9/2007"
			href="http://portal.mec.gov.br/arquivos/pdf/resolucao_047.pdf"
		>Resolu��o n. 47, 20/9/2007</a>, que altera a Resolu��o CD/FNDE n. 29,
		de 20 de julho de 2007 e estabelece os crit�rios, os par�metros e os
		procedimentos para a operacionaliza��o da assist�ncia financeira
		suplementar e volunt�ria a projetos educacionais, no �mbito do
		Compromisso Todos pela Educa��o, no exerc�cio de 2007.
		<br/>
	</li>
</ul>
