<?php

//Sistema
define( "CTE_SISTEMA", 13 );

//Produ��o

// Perfil
define( "CTE_PERFIL_EQUIPE_MUNICIPAL", 130 );
define( "CTE_PERFIL_EQUIPE_LOCAL", 124 );
define( "CTE_PERFIL_EQUIPE_LOCAL_APROVACAO", 128 );
define( "CTE_PERFIL_EQUIPE_TECNICA", 125 );
define( "CTE_PERFIL_CONSULTORES", 126 );
define( "CTE_PERFIL_ALTA_GESTAO", 129 );
define( "CTE_PERFIL_CONSULTA_GERAL", 127 );
define( "CTE_PERFIL_EQUIPE_TECNICA_FNDE", 167 );

define( "CTE_PERFIL_SUPER_USUARIO", 121 );
define( "CTE_PERFIL_ADMINISTRADOR", 147 );

// Estado Documento
define( "CTE_ESTADO_DIAGNOSTICO", 1 );
define( "CTE_ESTADO_PAR", 2 );
define( "CTE_ESTADO_ANALISE", 10 ); // assist�ncia t�cnica
define( "CTE_ESTADO_ANALISE_FIN", 13 ); // assist�ncia financeira
define( "CTE_ESTADO_FINALIZADO", 11 );
define( "CTE_ESTADO_PARECER", 14 );
define( "CTE_ESTADO_FNDE", 15 );

// Instrumentos
define( "INSTRUMENTO_DIAGNOSTICO_ESTADUAL", 1 );
define( "INSTRUMENTO_DIAGNOSTICO_MUNICIPAL", 2 );

define( "CTE_OBRA_CONSTRUCAO", 1 );
define( "CTE_OBRA_REFORMA", 2 );
define( "CTE_OBRA_AMPLIACAO", 3 );

// Formas de execucao

define( "FORMA_EXECUCAO_ASS_TEC", 4 );

?>