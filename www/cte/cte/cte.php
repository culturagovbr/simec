<?php
/**
 * Rotina que controla o acesso �s p�ginas do m�dulo. Carrega as bibliotecas
 * padr�es do sistema e executa tarefas de inicializa��o. 
 *
 * @author Ren� de Lima Barbosa <renebarbosa@mec.gov.br> 
 * @since 22/03/2207
 */

/**
 * Obt�m o tempo comprecis�o de microsegundos. Essa informa��o � utilizada para
 * calcular o tempo de execu��o da p�gina.  
 * 
 * @return float
 * @see /includes/rodape.inc
 */

function getmicrotime(){
	list( $usec, $sec ) = explode( ' ', microtime() );
	return (float) $usec + (float) $sec; 
}

// obt�m o tempo inicial da execu��o
$Tinicio = getmicrotime();

// controle o cache do navegador
header( "Cache-Control: no-store, no-cache, must-revalidate" );
header( "Cache-Control: post-check=0, pre-check=0", false );
header( "Cache-control: private, no-cache" );
header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
header( "Pragma: no-cache" );

// carrega as fun��es gerais
include_once "config.inc";
include "verificasistema.inc";
include_once APPRAIZ . "includes/funcoes.inc";
include_once APPRAIZ . "includes/classes_simec.inc";
include_once APPRAIZ . 'includes/workflow.php';

/*
if ( $_SESSION['usucpf'] != '70183040163' && $_SESSION['usucpf'] != '39330346120' )
{
header( "Location: ../manutencao.htm" );
die();
}
*/

// carrega as fun��es do m�dulo
include_once '_constantes.php';
include_once '_funcoes.php';
include_once '_componentes.php';

// abre conex�o com o servidor de banco de dados
$db = new cls_banco();

// carrega os dados do m�dulo
$modulo = $_REQUEST['modulo'];
$sql= "select ittemail, orgcod, ittabrev from instituicao where ittstatus = 'A'";
foreach( (array) $db->pegaLinha( $sql ) as $campo => $valor ) {
	$_SESSION[$campo]= trim( $valor );
}

// carrega a p�gina solicitada pelo usu�rio
$sql = sprintf( "select u.usuchaveativacao from seguranca.usuario u where u.usucpf = '%s'", $_SESSION['usucpf'] );
$chave = $db->pegaUm( $sql );
if ( $chave == 'f' ) {
	// leva o usu�rio para o formul�rio de troca de senha
	include APPRAIZ . $_SESSION['sisdiretorio'] . "/modulos/sistema/usuario/altsenha.inc";
	include APPRAIZ . "includes/rodape.inc";
} else if ( $_REQUEST['modulo'] ) {
	// leva o usu�rio para a p�gina solicitada
	include APPRAIZ . 'includes/testa_acesso.inc';
	include APPRAIZ . $_SESSION['sisdiretorio'] . "/modulos/" . $_REQUEST['modulo'] . ".inc";
	include APPRAIZ . "includes/rodape.inc";
} else {
	// leva o usu�rio para o formul�rio de login
	header( "Location: login.php" );
}

?>