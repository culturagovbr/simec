<html>
<? php?>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 11 (filtered)">
<title>APRESENTA��O</title>
<style>

<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-autospace:none;
	font-size:12.0pt;
	font-family:Arial;}
p.MsoBodyTextIndent, li.MsoBodyTextIndent, div.MsoBodyTextIndent
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:35.4pt;
	text-autospace:none;
	font-size:11.0pt;
	font-family:Arial;}
p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-autospace:none;
	font-size:11.5pt;
	font-family:Arial;}
p.MsoBodyText3, li.MsoBodyText3, div.MsoBodyText3
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-autospace:none;
	font-size:11.0pt;
	font-family:Arial;}
p.MsoBodyTextIndent2, li.MsoBodyTextIndent2, div.MsoBodyTextIndent2
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:35.4pt;
	text-autospace:none;
	font-size:11.5pt;
	font-family:Arial;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
@page Section1
	{size:595.45pt 841.7pt;
	margin:72.0pt 84.95pt 72.0pt 84.95pt;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
</head>
<body lang=PT-BR link=blue vlink=purple>
<table class="tabela" align="center" bgcolor="" cellspacing="1" cellpadding="3" style="font-family:Arial, Verdana; font-size:10px;">
	<tr>
	<td class="tituloPrincipalAbas"><b>APRESENTA��O</b></td>
	</tr>
	<tr>
		<td>
<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:Arial'>Minist�rio da Educa��o</span></b></p>
<p class=MsoBodyTextIndent align=center style='text-align:center;text-indent:
0cm'><b><span style='font-size:10.0pt'>PAR - PLANO DE A��ES ARTICULADAS</span></b></p>
<p class=MsoBodyTextIndent align=center style='text-align:center;text-indent:
0cm'><b><span style='font-size:10.0pt'>RELAT�RIO P�BLICO</span></b></p>
<p class=MsoBodyTextIndent align=center style='text-align:center;text-indent:
0cm'><b><span style='font-size:10.0pt'>APRESENTA��O</span></b></p>
<p class=MsoBodyTextIndent style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm;text-indent:0cm'><span style='font-size:10.0pt'>����������� O
Plano de Desenvolvimento da Educa��o (PDE), apresentado pelo Minist�rio da Educa��o
em abril de 2007, colocou � disposi��o dos estados, do Distrito Federal e dos
munic�pios, instrumentos eficazes de avalia��o e de implementa��o de pol�ticas
de melhoria da qualidade da educa��o, sobretudo da educa��o b�sica p�blica.</span></p>

<p class=MsoBodyTextIndent style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm'><span style='font-size:10.0pt'>O Plano de Metas
Compromisso Todos pela Educa��o, institu�do pelo Decreto 6.094 de 24 de abril
de 2007, � um programa estrat�gico do PDE, e inaugura um novo regime de
colabora��o, que busca concertar a atua��o dos entes federados sem ferir-lhes a
autonomia, envolvendo primordialmente a decis�o pol�tica, a a��o t�cnica e
atendimento da demanda educacional, visando � melhoria dos indicadores
educacionais. Trata-se de um compromisso fundado em 28 diretrizes e consubstanciado
em um plano de metas concretas, efetivas, que compartilha compet�ncias
pol�ticas, t�cnicas e financeiras para a execu��o de programas de manuten��o e
desenvolvimento da educa��o b�sica.</span></p>

<p class=MsoBodyTextIndent style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm;text-indent:0cm'><span style='font-size:10.0pt'>����������� A
partir da ades�o ao Plano de Metas Compromisso Todos pela Educa��o, os estados
e munic�pios elaboram seus respectivos Planos de A��es Articuladas.</span></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:2cm;margin-bottom:6.0pt;
margin-left:3cm;text-align:justify;text-indent:35.4pt;text-autospace:none'><span
style='font-size:10.0pt;font-family:Arial'>Para auxiliar na elabora��o do PAR,
o Minist�rio da Educa��o criou um novo sistema, o SIMEC � M�dulo PAR Plano de
Metas -, integrado aos sistemas que j� possu�a, e que pode ser acessado de
qualquer computador conectado � internet, representando uma importante evolu��o
tecnol�gica, com agilidade e transpar�ncia nos processos de elabora��o, an�lise
e apresenta��o de resultados dos PAR.</span></p>

<p class=MsoBodyTextIndent style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm'><span style='font-size:10.0pt'>Com metas claras,
pass�veis de acompanhamento p�blico e controle social, o MEC pode assim
disponibilizar, para consulta p�blica, os relat�rios dos Planos de A��es
Articuladas elaborados pelos estados e munic�pios que aderiram ao Plano de
Metas Compromisso Todos pela Educa��o.</span></p>

<p class=MsoBodyTextIndent style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm;text-indent:0cm'><span style='font-size:10.0pt'>����������� Apresentamos,
a seguir, uma breve descri��o dos elementos constitutivos do PAR.</span></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:2cm;margin-bottom:6.0pt;
margin-left:3cm;text-align:justify;text-indent:35.4pt;text-autospace:none'><span
style='font-size:10.0pt;font-family:Arial'>Inicialmente, os estados e
munic�pios devem realizar um diagn�stico minucioso da realidade educacional
local. A partir desse diagn�stico, desenvolver�o um conjunto coerente de a��es
que resulta no PAR.</span></p>

<p class=MsoBodyTextIndent2 style='margin-top:6.0pt;margin-right:2cm;
margin-bottom:6.0pt;margin-left:3cm'><span style='font-size:10.0pt'>O
instrumento para o diagn�stico da situa��o educacional local est� estruturado
em quatro grandes dimens�es:</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>1. Gest�o Educacional.</span></b></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>2. Forma��o de Professores e dos Profissionais de Servi�o e Apoio Escolar.</span></b></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:150%'>
<b><span style='font-size:10.0pt;line-height:150%'>3. Pr�ticas Pedag�gicas e Avalia��o.</span></b></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>4. Infra-estrutura F�sica e Recursos Pedag�gicos.</span></b></p>

<p class=MsoBodyText2 style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm;text-indent:35.4pt'><span style='font-size:10.0pt'>Cada
dimens�o � composta por �reas de atua��o, e cada �rea apresenta indicadores espec�ficos.
Esses indicadores s�o pontuados segundo a descri��o de crit�rios
correspondentes a quatro n�veis.</span></p>

<p class=MsoBodyText2 style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:3cm;text-indent:35.4pt'><span style='font-size:10.0pt'>A
pontua��o gerada para cada indicador � fator determinante para a elabora��o do
PAR, ou seja, na metodologia adotada, apenas crit�rios de pontua��o 1 e 2, que
representam situa��es insatisfat�rias ou inexistentes, podem gerar a��es.</span></p>

<p class=MsoBodyText2 style='margin-top:6.0pt;margin-right:2cm;margin-bottom:
6.0pt;margin-left:2cm;text-indent:35.4pt'><span style='font-size:10.0pt'>Assim,
o relat�rio disponibilizado apresenta as seguintes informa��es:</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>1. S�ntese por indicador:</span></b><span
style='font-size:10.0pt;line-height:150%'> resultado detalhado da realiza��o do
diagn�stico.</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>2. S�ntese da dimens�o:</span></b><span
style='font-size:10.0pt;line-height:150%'> resultado quantitativo da realiza��o
do diagn�stico.</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>3. S�ntese do PAR:</span></b><span
style='font-size:10.0pt;line-height:150%'> apresenta o detalhamento das a��es e
suba��es selecionadas por cada estado ou munic�pio.</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>4. Termo de Coopera��o:</span></b><span
style='font-size:10.0pt;line-height:150%'> apresenta a rela��o de a��es e
suba��es que contar�o com o apoio t�cnico do Minist�rio da Educa��o.</span></p>

<p class=MsoBodyText2 style='margin-left:6cm;text-indent:-18.0pt;line-height:
150%'><b><span style='font-size:10.0pt;line-height:150%'>5. Libera��o dos recursos:</span></b><span
style='font-size:10.0pt;line-height:150%'> apresenta a rela��o de a��es que
geraram conv�nio, ou seja, a libera��o de recursos financeiros.</span></p>

<p class=MsoBodyText3><span style='font-size:10.0pt'><span style='text-decoration:
 none'>&nbsp;</span></span></p>

<p class=MsoBodyText3 style='margin-left:2cm;text-indent:35.4pt'><span style='font-size:10.0pt'>
Cabe destacar que no presente momento apenas as informa��es sobre as redes municipais est�o dispon�veis.
</span></p>
<p class=MsoBodyText3 style='text-indent:35.4pt'><span style='font-size:10.0pt'>&nbsp;</span></p>
<p class=MsoBodyText3 style='margin-left:2cm;text-indent:35.4pt'><span style='font-size:10.0pt'>Para
mais informa��es, consulte o portal do MEC, <a href="http://www.mec.gov.br/">www.mec.gov.br</a>,
veja �IDEB - Saiba como melhorar�.</span></p>
		</td>
	</tr>
</table>

</body>
</html>
