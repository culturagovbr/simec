<?

define('SISID_ASSINT', 62);

define('PERFIL_SUPER_USUARIO', 310);
define('PERFIL_PEC_G', 679);
define('PERFIL_UNIVERSIDADES', 314);
define('PERFIL_CONSULTA', 449);
define('PERFIL_ADMINISTRADOR', 313);

define('PAIS_BRASIL', 1);

define('FUN_ORG_INTERNACIONAL', 43);

define('PROG_TIPO_BI', 'B');
define('PROG_TIPO_MU', 'M');

define('BEN_TIPO_BRA_EXT', 'B');
define('BEN_TIPO_EST_INS', 'E');
define('BEN_TIPO_COO_TEC', 'C');

define('BEN_TITU_UMP', 	 1);
define('BEN_TITU_DOISP', 2);

define('BEN_VINC_PER', 'P');
define('BEN_VINC_DOC', 'D');

define('BEN_NIV_SIM', 'S');
define('BEN_NIV_NAO', 'N');

define('FUNC_BENINTERNACIONAL', 57);

define('BEN_IND_BENEF_DOC', 'D');
define('BEN_IND_BENEF_DPE', 'C');
define('BEN_IND_BENEF_PES', 'P');
?>