<?php
//
// Perfil
//

define("PERFIL_SUPERUSUARIO", 614);
define("PERFIL_GESTOR", 651);
define("PERFIL_COORDENADOR", 652);
define("PERFIL_ADMINISTRADOR", 665);
define("PERFIL_CONSULTA", 667);

//
//Modalidadedo Curso
//

define("MODALIDADE_PRESENCIAL", 	1);
define("MODALIDADE_SEMIPRESENCIAL", 2);
define("MODALIDADE_DISTANCIA", 		3);

//
// Estados Workflow
//

define("WF_EM_ELABORACAO", 401);
define("WF_EM_ANALISE_GESTOR_CURSO", 402);
define("WF_EM_VALIDADO_GESTOR", 403);

//
// Tipo de Organiza��o
//

define("TO_CRITERIO_IES", 1);

//
//Categoria Membro Equipe
//

define("CME_EQUIPE_UAB", 8);

//
//Fun��o Exercida - Publico Alvo
//

define("FE_DOCENTE", 1);

//
//Ano CENSO
//

define("ANO_CENSO", 2010);
?>