<?PHP
    #VARIAVEIS DE MENUS/ABAS.
    define('ABA_QUESTIONARIO_MEDICINA', 57811);#ABA QUESTION�RIO AVALIA��O MEDICINA.
    define('ABA_GERAR_QUESTIONARIO_PERGUNTA', 57817);#ABA QUESTION�RIO AVALIA��O MEDICINA.
    define('ABA_CADASTRO_MANTENEDORA',57827);#ABA CADASTRO MANTENEDORA.

    #COSNTANTES PERFIL SISTEMA MAIS M�DICO - MEC.
    define('PERFIL_MM_MEC_SUPER_USUARIO', 1145);
    define('PERFIL_MM_MEC_ADMINISTRADOR', 1204);
    define('PERFIL_MM_MEC_REVISOR', 1403);
    
    #COSNTANTES PERFIL MODULO INSTRUMENTO DE AVALIA��O.
    define('PERFIL_INST_AVAL_AVALIADOR_MEC', 1146);
     define('PERFIL_HOSPITAL', 1275);     
      define('PERFIL_CONSULTA', 1236); 
    define('PERFIL_IES', 1279);
    define('PERFIL_INST_AVAL_ANALISTA_MEC', 1147);
    define('PERFIL_INST_AVAL_CONSULTA', 1236);
    define('PERFIL_SUPER_USUARIO', 1145);

    #ID DOS QUESTIONARIO DE AVALIA��O.
    define('QUEST_INSTRUMENTO_AVAL_CURSO_MEDICINA', 1);
    
    #ID DOS TIPO DE UNIDADE DE SA�DE.
    define('UNIDADE_SAUDE_HOSPITAL', 1);
    define('UNIDADE_SAUDE_UBS', 2);
    define('UNIDADE_SAUDE_UPA', 3);
    define('UNIDADE_SAUDE_CAPS', 4);
    define('UNIDADE_SAUDE_AMBULATORIO', 5);
    
    #WORKFLOW - FLUXO
    define('WF_FLUXO_AVALIACAO_IN_LOCO_MM', 183);
    define('WF_FLUXO_MANTENEDORA', 199);
    
    #WORKFLOW - A��O
    define('WF_EM_PREENCHIMENTO_AVALIADOR', 1128);
    define('WF_EM_ANALISE_MEC', 1129);
    define('WF_EM_REANALISE_MEC', 1130);
    define('WF_EM_AJUSTE_AVALIADOR', 1131);
    define('WF_PROCESSO_FINALIZADO', 1132);
    
    define('WF_EM_PREENCHIMENTO_IES', 1261);
    define('WF_EM_AJUSTE_IES', 1264);
    
    
?>