<!DOCTYPE html>
<html lang="pt-BR">

<?php require "head.php"; ?>

<header>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <img src="imagens/logo-simec.png" class="res" width="150">
            <a class="brasil pull-right" href="http://www.brasil.gov.br/"><img alt="Brasil - Governo Federal" src="http://portal.mec.gov.br/templates/mec2014/images/brasil.png" style="margin-right: 10px;"></a>
        </div>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <h1 style="text-align: center;">POL�TICA NACIONAL DE FORMA��O DOS PROFISSIONAIS DA EDUCA��O B�SICA</h1>
        </div>
		<div class="col-lg-12 col-sm-12 col-xs-12">
        	<div class="alert alert-success" role="alert">
				<p>Sua avalia��o foi encaminhada com sucesso.</p>
				<p>Agradecemos sua participa��o!</p>
			</div>
        	<div class="form-group">
				<a class="btn btn-danger" href="index.php"><span class="glyphicon glyphicon-hand-left"></span> Voltar</a>
			</div>
		</div>
   	</div>
   	

   <?php require "footer.php"; ?>
</div> <!-- /container -->
</body>
</html>