<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="alert alert-success" role="alert">
            <p>Envio de receitas encerrado em <?php echo $dataFormatada; ?>.</p>
            <p>Obrigado pela colaboração!</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12 text-left">
            <button title="Finalizar" class="btn btn-danger center-block">
                <span class="fa"></span> Finalizar
            </button>
        </h2>
    </div>
</div>