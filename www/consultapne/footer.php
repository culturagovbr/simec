<footer class="well well-sm">
	<p style="text-align: center;">&copy; 2015 Minist�rio da Educa��o. Todos os direitos reservados.</p>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootbox.js"></script>
<script src="js/lodash.min.js"></script>
<script src="js/jquery.chosen.js" type="text/javascript"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="js/jquery.countdown.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.extend.js" type="text/javascript"></script>
<script src="js/jquery.raty.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
