<?


function enviadoComite() {
	global $db;
	$msg.="O Plano de Desenvolvimento da Escola foi enviado para an�lise do MEC fora do prazo, que expirou no dia 09/12/2011. Acesse o site do PDE Escola no endere�o http://pdeescola.mec.gov.br e conhe�a as regras de an�lise para os planos enviados fora do prazo.".'\n\n\n';	
	$msg.="O plano da (nome da escola) foi enviado para an�lise do Comit�. Caso o mesmo seja aprovado pela Secretaria e validado pelo MEC, a escola receber� os recursos indicados no plano mas, para que isso aconte�a, confira se:".'\n\n';
	$msg.="1)	A escola possui Unidade Executora (UEx) e a mesma j� se recadastrou no PDDE Web este ano;".'\n';
	$msg.="2)	A presta��o de contas dos anos anteriores foi encaminhada, recebida e aprovada pelo FNDE;".'\n';
	$msg.="3)	O saldo de recursos de 2009, se houver, foi reprogramado para 2010;".'\n';
	$msg.="4)	A conta corrente da Unidade Executora (UEx) continua ativa.".'\n';
	$msg.="Se tiver d�vidas, acesse <www.fnde.gov.br>, procure o link <Consulta Presta��o de Contas> e preencha os campos indicando o nome do seu munic�pio e da sua escola. Ou envie um e-mail para pdeescola@mec.gov.br.";
	echo "<script>alert('".$msg."');</script>";
	return true;
	
}

function listasGrandesDesafios($dados) {
	global $db;
	
	$sql = "SELECT * FROM pdeinterativo2013.respostaideb WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadosideb = $db->pegaLinha($sql);
	
	$filtro = $dados['filtro'];
	
	if($dadosideb['ridinicialum'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar o IDEB dos Anos Iniciais em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaideb[ridinicialum] value='".$dadosideb['ridinicialumdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos.";
	}
	if($dadosideb['ridfinalum'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar o IDEB dos Anos Finais em <input ".(($dados['desabilitar'])?"disabled":"")." type=text name=respostaideb[ridfinalum] value='".$dadosideb['ridfinalumdesafio']."' class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos.";
	}
	if($dadosideb['ridmedioum'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar o IDEB do Ensino M�dio em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaideb[ridmedioum] value='".$dadosideb['ridmedioumdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos.";
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostataxarendimento WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadostx = $db->pegaLinha($sql);
	
	if($dadostx['rtrfunaprova'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar a taxa de aprova��o da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrfunaprova] value='".$dadostx['rtrfunaprovadesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrfunreprova'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Reduzir a taxa de reprova��o da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrfunreprova] value='".$dadostx['rtrfunreprovadesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrfunabandono'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Reduzir a taxa de abandono da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrfunabandono] value='".$dadostx['rtrfunabandonodesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrmedaprova'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar a taxa de aprova��o da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrmedaprova] value='".$dadostx['rtrmedaprovadesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino M�dio.";
	}
	if($dadostx['rtrmedreprova'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Reduzir a taxa de reprova��o da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrmedreprova] value='".$dadostx['rtrmedreprovadesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino M�dio.";
	}
	if($dadostx['rtrmedabandono'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Reduzir a taxa de abandono da escola em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostataxarendimento[rtrmedabandono] value='".$dadostx['rtrmedabandonodesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos no Ensino M�dio.";
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostaprovabrasil WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadospb = $db->pegaLinha($sql);
	
	if($dadospb['rpbinicialport'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar os resultados de L�ngua Portuguesa na Prova Brasil em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaprovabrasil[rpbinicialport] value='".$dadospb['rpbinicialportdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos nos Anos Iniciais.";
	}
	if($dadospb['rpbinicialmat'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar os resultados de Matem�tica na Prova Brasil em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaprovabrasil[rpbinicialmat] value='".$dadospb['rpbinicialmatdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos.";
	}
	if($dadospb['rpbfinalport'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar os resultados de L�ngua Portuguesa na Prova Brasil em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaprovabrasil[rpbfinalport] value='".$dadospb['rpbfinalportdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em dois anos nos Anos Finais.";
	}
	if($dadospb['rpbfinalmat'.$filtro]=="t") {
		$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> Elevar os resultados de Matem�tica na Prova Brasil em <input ".(($dados['desabilitar'])?"disabled":"")." name=respostaprovabrasil[rpbfinalmat] value='".$dadospb['rpbfinalmatdesafio']."' type=text class=normal size=4 maxlength=3 onkeyup=this.value=mascaraglobal('###',this.value);> % em Anos Finais.";
	}
	
	if($dados['return_arr']) {
		
		return $desafiosindicadorestaxas;
		
	} else {
	
		echo "<table class=listagem cellSpacing=1 cellPadding=3	align=center width=100%>";
		if($desafiosindicadorestaxas) {
			//pdeinterativo2013.outrosdesafios
			$sql = "select otdtexto from pdeinterativo2013.outrosdesafios where pdeid = ".$_SESSION['pdeinterativo2013_vars']['pdeid']." and otdstatus = 'A' ";
			$otdtexto = $db->pegaUm($sql);
			if($otdtexto){
				$desafiosindicadorestaxas[] = "<img src=../imagens/seta_filho.gif> $otdtexto.";
			}
			foreach($desafiosindicadorestaxas as $desafio) {
				echo "<tr>
						<td> $desafio</td>
					  </tr>";
			}
		} else {
			
			echo "<tr>
					<td align=\"center\" style=\"color: rgb(204, 0, 0);\">N�o foram encontrados Registros.</td>
				  </tr>";
			
		}
		echo "</table>";
	
	}
	
}


function gerenciarOutrosDesafios($dados) {
	global $db;
	
	$sql = "SELECT * FROM pdeinterativo2013.respostaideb WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadosideb = $db->pegaLinha($sql);
	
	if($dadosideb['ridinicialumcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadosideb['ridinicialumdesafiooutros']=="t")?"checked":"")." name=ridinicialumdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaideb',this);> Elevar o IDEB dos Anos Iniciais em ### % em dois anos.";
	}
	if($dadosideb['ridfinalumcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadosideb['ridfinalumdesafiooutros']=="t")?"checked":"")." name=ridfinalumdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaideb',this);> Elevar o IDEB dos Anos Finais em ### % em dois anos.";
	}
	if($dadosideb['ridmedioumcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadosideb['ridmedioumdesafiooutros']=="t")?"checked":"")." name=ridmedioumdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaideb',this);> Elevar o IDEB do Ensino M�dio em ### % em dois anos.";
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostataxarendimento WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadostx = $db->pegaLinha($sql);
	
	if($dadostx['rtrfunaprovacritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrfunaprovadesafiooutros']=="t")?"checked":"")." name=rtrfunaprovadesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Elevar a taxa de aprova��o da escola em ### % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrfunreprovacritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrfunreprovadesafiooutros']=="t")?"checked":"")." name=rtrfunreprovadesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Reduzir a taxa de reprova��o da escola em ### % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrfunabandonocritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrfunabandonodesafiooutros']=="t")?"checked":"")." name=rtrfunabandonodesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Reduzir a taxa de abandono da escola em ### % em dois anos no Ensino Fundamental.";
	}
	if($dadostx['rtrmedaprovacritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrmedaprovadesafiooutros']=="t")?"checked":"")." name=rtrmedaprovadesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Elevar a taxa de aprova��o da escola em ### % em dois anos no Ensino M�dio.";
	}
	if($dadostx['rtrmedreprovacritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrmedreprovadesafiooutros']=="t")?"checked":"")." name=rtrmedreprovadesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Reduzir a taxa de reprova��o da escola em ### % em dois anos no Ensino M�dio.";
	}
	if($dadostx['rtrmedabandonocritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadostx['rtrmedabandonodesafiooutros']=="t")?"checked":"")." name=rtrmedabandonodesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostataxarendimento',this);> Reduzir a taxa de abandono da escola em ### % em dois anos no Ensino M�dio.";
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostaprovabrasil WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$dadospb = $db->pegaLinha($sql);
	
	if($dadospb['rpbinicialportcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadospb['rpbinicialportdesafiooutros']=="t")?"checked":"")." name=rpbinicialportdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaprovabrasil',this);> Elevar os resultados de L�ngua Portuguesa na Prova Brasil em ### % em dois anos nos Anos Iniciais.";
	}
	if($dadospb['rpbinicialmatcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadospb['rpbinicialmatdesafiooutros']=="t")?"checked":"")." name=rpbinicialmatdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaprovabrasil',this);> Elevar os resultados de Matem�tica na Prova Brasil em ### % em dois anos.";
	}
	if($dadospb['rpbfinalportcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadospb['rpbfinalportdesafiooutros']=="t")?"checked":"")." name=rpbfinalportdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaprovabrasil',this);> Elevar os resultados de L�ngua Portuguesa na Prova Brasil em ### % em dois anos nos Anos Finais.";
	}
	if($dadospb['rpbfinalmatcritico']!="t") {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($dadospb['rpbfinalmatdesafiooutros']=="t")?"checked":"")." name=rpbfinalmatdesafiooutros value=TRUE onclick=marcarOutrosDesafios('respostaprovabrasil',this);> Elevar os resultados de Matem�tica na Prova Brasil em ### % em Anos Finais.";
	}
	
	//pdeinterativo2013.outrosdesafios
	$sql = "select otdtexto from pdeinterativo2013.outrosdesafios where pdeid = ".$_SESSION['pdeinterativo2013_vars']['pdeid']." and otdstatus = 'A' ";
	$otdtexto = $db->pegaUm($sql);
		
	echo "<script language=\"JavaScript\" src=\"../includes/funcoes.js\"></script>";
	echo '<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>';
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../includes/Estilo.css\"/>";
	echo "<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>";
	echo "<script>
			function marcarOutrosDesafios(tabela,obj) {
				jQuery.ajax({
			   		type: \"POST\",
			   		url: \"pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A\",
			   		data: \"requisicao=marcarOutrosDesafios&tabela=\"+tabela+\"&campo=\"+obj.name+\"&marcado=\"+obj.checked,
			   		async: false,
			   		success: function(msg){}
			 		});
			 		
			 	window.opener.listaOutrosDesafios();
			}
			function marcarOutrosDesafiosOutros(obj) {
				if(jQuery(obj).attr('checked') == true){
					jQuery('#texto_outros').show();
				}else{
					jQuery('#texto_outros').val('').hide();
				}
			}
			function salvarOutrosDesafios()
			{
				if(jQuery('[name=outrosdesafiosoutros]').attr('checked') == true){
					jQuery.ajax({
			   		type: \"POST\",
			   		url: \"pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A\",
			   		data: \"requisicao=salvarOutrosDesafios&otdtexto=\"+jQuery('[name=otdtexto]').val(),
			   		async: false,
			   		success: function(msg){
			   				window.opener.listaOutrosDesafios();
			   				alert('Opera��o realizada com sucesso!');
			   				window.close();
						}
			 		});
				}else{
					jQuery.ajax({
			   		type: \"POST\",
			   		url: \"pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A\",
			   		data: \"requisicao=excluirOutrosDesafios\",
			   		async: false,
			   		success: function(msg){
			   				window.opener.listaOutrosDesafios();
			   				alert('Opera��o realizada com sucesso!');
			   				window.close();
						}
			 		});
				}
			}
		  </script>";
	
	echo "<table class=\"tabela\" bgcolor=\"#f5f5f5\" cellSpacing=\"1\" cellPadding=\"3\"	align=\"center\">";
	echo "<tr><td class=SubTituloCentro>Outros Desafios</td></tr>";
	
	if($desafiosindicadorestaxas) {
		$desafiosindicadorestaxas[] = "<input type=checkbox ".(($otdtexto)?"checked":"")." name='outrosdesafiosoutros' value=TRUE onclick='marcarOutrosDesafiosOutros(this)' /> Outros: <span id='texto_outros' style='display:".($otdtexto ? "" : "none")."'  >".campo_texto("otdtexto","N","S","",60,255,"","","","","","","",$otdtexto)."</span>";
		foreach($desafiosindicadorestaxas as $desafio) {
			echo "<tr>
					<td>$desafio</td>
				  </tr>";
		}
	} else {
		
		echo "<tr>
				<td align=\"center\" style=\"color: rgb(204, 0, 0);\">N�o foram encontrados Registros.</td>
			  </tr>";
		
	}
	echo "<tr><td class=SubTituloCentro><input type=button value=Salvar onclick=salvarOutrosDesafios();> <input type=button value=Cancelar onclick=window.close();></td></tr>";
	echo "</table>";
	
}

function marcarOutrosDesafios($dados) {
	global $db;
	
	$sql = "UPDATE pdeinterativo2013.".$dados['tabela']." SET ".$dados['campo']."=".$dados['marcado']." WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$db->executar($sql);
	$db->commit();
	apagarCachePdeInterativo();	
	
}

function planoestrategico_0_2_planoacao($dados) {
	global $db;
	
	if(!$dados['salvarparcial']) {
		$papids = $db->carregarColuna("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papstatus='A' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		$prb_sem_estrategia = false;
		$est_sem_acao = false;
		if($papids) {
			foreach($papids as $papid) {
				$paeids = $db->carregarColuna("SELECT paeid FROM pdeinterativo2013.planoacaoestrategia WHERE paestatus='A' AND papid='".$papid."'");
				if($paeids) {
					foreach($paeids as $paeid) {
						$num = $db->pegaUm("SELECT COUNT(paaid) as num FROM pdeinterativo2013.planoacaoacao WHERE paastatus='A' AND paeid='".$paeid."'");
						if($num==0) {
							$est_sem_acao = true;
						}
					}
				} else {
					$prb_sem_estrategia = true;				
				}
			}
		}
	
		if($prb_sem_estrategia) $alert[] = "O usu�rio s� poder� inserir no m�nimo 1 (uma) e no m�ximo 2 (duas) estrat�gias para cada problema.";
		if($est_sem_acao) $alert[] = "� obrigat�rio inserir pelo menos uma a��o para cada estrat�gia e, no m�nimo, uma estrat�gia para cada problema.";
		
		if($alert) {
			die("<script>
					alert('N�o foi possivel gravar o plano de a��o. Os seguintes problemas foram encontrados:".'\n\n'.implode('\n',$alert)."');
					window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_2_planoacao';
				 </script>");
		}
	}
	
	if($dados['metas']) {
		foreach($dados['metas'] as $metid => $valor) {
			$sql = "SELECT rmeid FROM pdeinterativo2013.respostameta WHERE metid='".$metid."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
			$rmeid = $db->pegaUm($sql);
			if($rmeid) {
				$sql = "UPDATE pdeinterativo2013.respostameta
   						SET rmetaxa=".(($valor)?"'".$valor."'":"NULL")."
 						WHERE rmeid='".$rmeid."'";
			} else {
				$sql = "INSERT INTO pdeinterativo2013.respostameta(
		            	metid, pdeid, rmetaxa)
		    			VALUES ('".$metid."', '".$_SESSION['pdeinterativo2013_vars']['pdeid']."', ".(($valor)?"'".$valor."'":"NULL").");";
			}
			$db->executar($sql);
			$db->commit();
		}
		
	}
	
	if($dados['metas2']) {
		foreach($dados['metas2'] as $metid => $valor) {
			$sql = "SELECT rmeid FROM pdeinterativo2013.respostameta WHERE metid='".$metid."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
			$rmeid = $db->pegaUm($sql);
			if($rmeid) {
				$sql = "UPDATE pdeinterativo2013.respostameta
   						SET rmecheckbox=".(($valor)?$valor:"NULL")."
 						WHERE rmeid='".$rmeid."'";
			} else {
				$sql = "INSERT INTO pdeinterativo2013.respostameta(
		            	metid, pdeid, rmecheckbox)
		    			VALUES ('".$metid."', '".$_SESSION['pdeinterativo2013_vars']['pdeid']."', ".(($valor)?$valor:"NULL").");";
			}
			$db->executar($sql);
			$db->commit();
		}
		
	}

	if($dados['pesid_estrategias']) {
		foreach($dados['pesid_estrategias'] as $paeid => $respid) {
			$sql = "UPDATE pdeinterativo2013.planoacaoestrategia SET respid='".$respid."' WHERE paeid='".$paeid."'";			
			$db->executar($sql);
			$db->commit();
		}
	}
	
	if($dados['aoaid']) {
		foreach($dados['aoaid'] as $abaid => $aoaid) {
			$opaid = $db->pegaUm("SELECT opaid FROM pdeinterativo2013.objetivoplanoacao WHERE abaid='".$abaid."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if($opaid) {
				
				$sql = "UPDATE pdeinterativo2013.objetivoplanoacao SET aoaid=".(($aoaid)?"'".$aoaid."'":"NULL")."
 						WHERE opaid='".$opaid."'";
				
				$db->executar($sql);
				$db->commit();
				
			} else {
				
				if($aoaid) {
					$sql = "INSERT INTO pdeinterativo2013.objetivoplanoacao(
	            			pdeid, abaid, aoaid, opastatus)
	    					VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$abaid."', '".$aoaid."', 'A');";
					$db->executar($sql);
					$db->commit();
				}
			}
		}
	}
	
	apagarCachePdeInterativo();
	
	if(!$dados['salvarparcial']) {
		
		salvarAbaResposta("planoestrategico_0_2_planoacao");
		
		echo "<script>
				alert('Dados gravados com sucesso');
				window.location='".$dados['togo']."';
			  </script>";
	}
	
	
	
}

function planoestrategico_0_1_grandesdesafios($dados) {
	global $db;
	
	if((count($dados['respostaprovabrasil'])+count($dados['respostaideb'])+count($dados['respostataxarendimento'])) < 2) {
		echo "<script>
				alert('� obrigat�rio 2(dois) desafios. Clique em Inserir outros desafios.');
				window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_1_grandesdesafios';
			  </script>";
		exit;
		
	}
	
	if($dados['respostaideb']) {
		$ridid = $db->pegaUm("SELECT ridid FROM pdeinterativo2013.respostaideb WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		if(!$ridid) die("<script>alert('� necess�rio salvar o Diagnostico: 1.1. IDEB');window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_1_grandesdesafios';</script>");
		foreach($dados['respostaideb'] as $campo => $valor) {
			$sql = "UPDATE pdeinterativo2013.respostaideb SET ".$campo."desafio=".(($valor)?"'".$valor."'":"NULL")." WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
			$db->executar($sql);
			$db->commit();
		}
	}
	
	if($dados['respostataxarendimento']) {
		$rtrid = $db->pegaUm("SELECT rtrid FROM pdeinterativo2013.respostataxarendimento WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		if(!$rtrid) die("<script>alert('� necess�rio salvar o Diagnostico: 1.2. Taxas de rendimento');window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_1_grandesdesafios';</script>");
		foreach($dados['respostataxarendimento'] as $campo => $valor) {
			$sql = "UPDATE pdeinterativo2013.respostataxarendimento SET ".$campo."desafio=".(($valor)?"'".$valor."'":"NULL")." WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
			$db->executar($sql);
			$db->commit();
		}
	}
	if($dados['respostaprovabrasil']) {
		$rpbid = $db->pegaUm("SELECT rpbid FROM pdeinterativo2013.respostaprovabrasil WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		if(!$rpbid) die("<script>alert('� necess�rio salvar o Diagnostico: 1.3. Prova Brasil');window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&aba=planoestrategico_0_pdeescola&aba1=planoestrategico_0_1_grandesdesafios';</script>");
		foreach($dados['respostaprovabrasil'] as $campo => $valor) {
			$sql = "UPDATE pdeinterativo2013.respostaprovabrasil SET ".$campo."desafio=".(($valor)?"'".$valor."'":"NULL")." WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
			$db->executar($sql);
			$db->commit();
		}
	}
		
	salvarAbaResposta("planoestrategico_0_1_grandesdesafios");
	
	apagarCachePdeInterativo();	
	
	echo "<script>
			alert('Dados gravados com sucesso');
			window.location='".$dados['togo']."';
		  </script>";
	
}

function atualizarProblemasDimensao($dados) {
	global $db;
	$sql = "SELECT UPPER(abadescricao) as abadescricao, abacod, abaid FROM pdeinterativo2013.aba WHERE abaidpai = '".ABA_DIAGNOSTICO."' AND abaid != ".ABA_DIAGNOSTICO_TAXASINDICADORES." AND abatipo IS NULL ORDER BY abaid";
	if(CACHE_MEM) {
		$abas = $db->carregar($sql,null,3600);
	} else {
		$abas = $db->carregar($sql);
	}
	
	$sql = "UPDATE pdeinterativo2013.planoacaoproblema SET papstatus='I' WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$db->executar($sql);
	$db->commit();
	
	if($abas[0]) :
		foreach($abas as $aba) :
			//$inicioP = getmicrotime();
			$funcao = "listaproblemas_".$aba['abacod'];
			$problemas = $funcao($dados);
			if($problemas) :
				foreach($problemas as $problema) :
					if($problema['codigo']) $prb_codigo[] = $problema['codigo'];
				endforeach;
			endif;
			//echo "Texec - ".$aba['abacod']." : ".number_format( ( getmicrotime() - $inicioP ), 4, ',', '.' )."<br>";
		endforeach;
		
		if($prb_codigo) {
			foreach($prb_codigo as $pp) {
				$db->executar("UPDATE pdeinterativo2013.planoacaoproblema SET papstatus='A' WHERE papid='".$pp."'");
				$db->commit();
			}
		}
	endif;
	
}
	
function carregarPlanoEstrategicoDimensao($dados) {
	global $db;
	if(CACHE_FILE) {
		/* In�cio - Cache em arquivo*/
		include_once APPRAIZ.'includes/classes/cacheSimec.class.inc';
		$cache = new cache("planoestrategico_".$_SESSION['pdeinterativo2013_vars']['pdeid']);
		/* Fim - Cache em arquivo*/
	}
	
	if(verificaFlagPDEInterativo('atualizaplano')) {
		atualizarProblemasDimensao($dados);
		$db->executar("UPDATE pdeinterativo2013.flag SET atualizaplano=FALSE WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		$db->commit();
	}
	echo "<table class=tabela bgcolor=#f5f5f5 cellSpacing=1 cellPadding=3	align=center>";

	$sql = "SELECT UPPER(abadescricao) as abadescricao, abacod, abaid FROM pdeinterativo2013.aba WHERE abaidpai = '".ABA_DIAGNOSTICO."' AND abaid != ".ABA_DIAGNOSTICO_TAXASINDICADORES." AND abatipo IS NULL ORDER BY abaid";
	if(CACHE_MEM) {
		$abas = $db->carregar($sql,null,3600);
	} else {
		$abas = $db->carregar($sql);
	}
	if($abas[0]) :
		foreach($abas as $aba) :
			planoestrategicoDimensao($dados = array('abaid' => $aba['abaid'], 'abadescricao' => $aba['abadescricao'],'abacod' => $aba['abacod']));
		endforeach;
	endif;
	
	$db->commit();
	
	echo "</table>";
}

function excluirAcao($dados) {
	global $db;

	apagarCachePdeInterativo();
		
	$sql = "UPDATE pdeinterativo2013.planoacaoacao SET paastatus='I' WHERE paaid='".$dados['paaid']."'";
	$db->executar($sql);
	$db->commit();
	echo "A��o removida com sucesso";
}

function gerenciarEstrategias($dados) {
	global $db;

	echo "<script language=\"JavaScript\" src=\"../includes/funcoes.js\"></script>";
	echo '<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>';
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../includes/Estilo.css\"/>";
	echo "<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>";
	
	echo "<script>
		  function marcarEstrategia(epaid,obj) {
		  	var numMarcadas = jQuery(\"[name^='estrategiaplanoacaoapoio[']:checked\").length;
			if(numMarcadas > 2) {
				alert('O usu�rio s� poder� inserir no m�nimo 1 (uma) e no m�ximo 2 (duas) estrat�gias para cada problema.');
				obj.checked=false;
				return false;
			} else {
			  	divCarregando();
				jQuery.ajax({
			   		type: \"POST\",
			   		url: \"pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A\",
			   		data: \"requisicao=gravarEstrategiaProblema&epaid=\"+epaid+\"&papid=".$dados['papid']."&chk=\"+obj.checked,
			   		async: false,
			   		success: function(msg){}
			 		});
			 	window.opener.carregarPlanoEstrategicoDimensao();
			 	divCarregado();
		 	}
		  }
		  </script>";
	echo "<body>";
	echo "<table class=tabela bgcolor=#f5f5f5 cellSpacing=1 cellPadding=3 align=center>";
	echo "<tr><td class=SubTituloCentro>".$db->pegaUm("SELECT abadescricao FROM pdeinterativo2013.aba WHERE abaid='".$dados['abaid']."'")."</td></tr>";
	echo "</table>";
	
	$sql = "SELECT '<input '||CASE WHEN (SELECT paeid FROM pdeinterativo2013.planoacaoestrategia WHERE papid='".$dados['papid']."' AND epaid=e.epaid AND paestatus='A' ORDER BY paeid DESC LIMIT 1 ) IS NULL THEN '' ELSE 'checked' END ||' type=checkbox name=estrategiaplanoacaoapoio[] value=TRUE onclick=\"marcarEstrategia(\''||epaid||'\', this);\">' as chk, epadesc FROM pdeinterativo2013.estrategiaplanoacaoapoio e WHERE abaid='".$dados['abaid']."' AND epastatus='A'";
	$db->monta_lista_simples($sql,$cabecalho=array("&nbsp;","Estrat�gia"),50,5,'N','95%',$par2);
	echo "</body>";

	
}

function gravarEstrategiaProblema($dados) {
	global $db;

	$sql = "SELECT paeid FROM pdeinterativo2013.planoacaoestrategia WHERE papid='".$dados['papid']."' AND epaid='".$dados['epaid']."'";
	$paeid = $db->pegaUm($sql);
	
	if($dados['chk']=='true' && !$paeid) {
		$sql = "INSERT INTO pdeinterativo2013.planoacaoestrategia(
			            papid, epaid, paestatus)
			    VALUES ('".$dados['papid']."', '".$dados['epaid']."', 'A');";
		$db->executar($sql);
	} elseif($dados['chk']=='false' && $paeid) {
		$sql = "UPDATE pdeinterativo2013.planoacaoestrategia SET paestatus='I' WHERE paeid='".$paeid."'";
		$db->executar($sql);
	} elseif($dados['chk']=='true' && $paeid) {
		$sql = "UPDATE pdeinterativo2013.planoacaoestrategia SET paestatus='A' WHERE paeid='".$paeid."'";
		$db->executar($sql);
	}
	$db->commit();
	apagarCachePdeInterativo();
	
	
}

function deletarBensServicos($dados) {
	global $db;
	if(is_numeric($dados['pabid'])) {
		$sql = "UPDATE pdeinterativo2013.planoacaobemservico SET pabstatus='I' WHERE pabid='".$dados['pabid']."'";
		$db->executar($sql);
		$db->commit();
		$sql = "UPDATE pdeinterativo2013.planoacaoacao SET paacustototal=(SELECT SUM(COALESCE(pabvalorcapital,0)+COALESCE(pabvalorcusteiro,0)) FROM pdeinterativo2013.planoacaobemservico WHERE pabstatus='A' AND paaid IN(SELECT paaid FROM pdeinterativo2013.planoacaobemservico WHERE pabid='".$dados['pabid']."' AND pabstatus='A')) 
				WHERE paaid IN(SELECT paaid FROM pdeinterativo2013.planoacaobemservico WHERE pabid='".$dados['pabid']."' AND pabstatus='A')";
			
		$db->executar($sql);
		$db->commit();
		echo "Deletado com sucesso";
		apagarCachePdeInterativo();
	} else {
		echo "N�o foi poss�vel remover 'Bens e Servi�os'";
	}
	
}

function gerenciarAcao($dados) {
	global $db;
	$sql_acao = "SELECT aapid as codigo, aapdesc as descricao FROM pdeinterativo2013.acaoapoio WHERE aapstatus='A'";
	$sql_objeto = "SELECT oapid as codigo, oapdesc as descricao FROM pdeinterativo2013.objetoapoio WHERE oapstatus='A'";
	?>
	<html>
	<head>
	<script language="JavaScript" src="../includes/funcoes.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
	<!-- (IN�CIO) BIBLIOTECAS - PARA USO DOS COMPONENTES (CALEND�RIO E SLIDER) -->
	<script	language="javascript" type="text/javascript" src="../includes/blendtrans.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JsLibrary/_start.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JsLibrary/slider/slider.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>
	<!-- (IN�CIO) BIBLIOTECAS - PARA USO DOS COMPONENTES (BOX) -->
	<script type="text/javascript" src="../includes/ModalDialogBox/modal-message.js"></script>
	<script type="text/javascript" src="../includes/ModalDialogBox/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="../includes/ModalDialogBox/ajax.js"></script>
	<link rel="stylesheet" href="../includes/ModalDialogBox/modal-message.css" type="text/css" media="screen" />

	<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
	<link rel="stylesheet" type="text/css" href="../pdeinterativo2013/css/pdeinterativo2013.css"/>
	<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>
	</head>
	<body>
	<? 
	if($dados['paaid']) {
		
		if(!is_numeric($dados['paaid'])) die("<script>alert('Problemas para abrir a��o. Tente novamente.');window.close();</script>");
		
		$sql = "SELECT * FROM pdeinterativo2013.planoacaoacao WHERE paaid='".$dados['paaid']."'";
		$planoacaoacao = $db->pegaLinha($sql);
		
		extract($planoacaoacao);
		
		$requisicao = "atualizarAcaoPlanoEstrategico";
			
	} else {
		
		$requisicao = "inserirAcaoPlanoEstrategico";
		
	}
	?>
	<script>
	function submeterAcao() {
		if(document.getElementById('aapid').value=='') {
			alert('Selecione uma a��o');
			return false;
		}
		document.getElementById('paaacaoqtd').value = mascaraglobal('#######',document.getElementById('paaacaoqtd').value);
		if(document.getElementById('paaacaoqtd').value=='') {
			alert('Preencha a quantidade');
			return false;
		}
		if(document.getElementById('oapid').value=='') {
			alert('Selecione um objeto');
			return false;
		}
		if(document.getElementById('paaperiodoinicio').value=='') {
			alert('Preencha a data inicial');
			return false;
		}
		if(!validaData(document.getElementById('paaperiodoinicio'))) {
			alert('Data inicial inv�lida');
			return false;
		}
		if(document.getElementById('paaperiodofim').value=='') {
			alert('Preencha a data final');
			return false;
		}
		if(!validaData(document.getElementById('paaperiodofim'))) {
			alert('Data final inv�lida');
			return false;
		}
		if(document.getElementById('paadetalhamento').value=='') {
			alert('Preencha o detalhamento');
			return false;
		}
		if(jQuery("[name^='paarecurso']:checked").length == 0) {
			alert('Selecione se precisa de recurso');
			return false;
		}
		
		divCarregando();
		document.getElementById('formulario').submit();

		
	}
	
	function deletarBensServicos(pabid) {
		var conf = confirm('Deseja realmente excluir?');
		if(conf) {
			divCarregando();
			jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=deletarBensServicos&pabid="+pabid,
		   		async: false,
		   		success: function(msg){
				alert(msg);
				carregarBensServicos();
				carregarSaldoPdeEscolaAcao();
				}
		 		});
		 	divCarregado();
		}
	}
	
	function carregarBensServicos() {
		var paaid = jQuery("[name='paaid']").val();
		jQuery.ajax({
	   		type: "POST",
	   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
	   		data: "requisicao=carregarBensServicos&paaid=" + paaid,
	   		async: false,
	   		success: function(msg){document.getElementById('div_bensservicos').innerHTML = msg;}
	 		});
	}
	
	function carregarSaldoPdeEscolaAcao() {
		jQuery.ajax({
	   		type: "POST",
	   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
	   		data: "requisicao=carregarSaldoPdeEscolaAcao&paaid="+jQuery("[name='paaid']").val(),
	   		async: false,
	   		success: function(msg){document.getElementById('td_saldoPdeEscolaAcao').innerHTML = msg;}
	 		});
	}
	
	function exibePrecisaRecursos(resp)
	{
		jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","disabled");
		var paaid = jQuery("[name='paaid']").val();
		
		if(!paaid){
			var msg='';
			var erro_formulario=false;
			
			if(document.getElementById('paaperiodoinicio').value!='') {
				if(!validaData(document.getElementById('paaperiodoinicio'))) {
					msg+='- Data inicial inv�lida\n';			
					erro_formulario=true;
				}
			}
			if(document.getElementById('paaperiodofim').value!='') {
				if(!validaData(document.getElementById('paaperiodofim'))) {
					msg+='- Data final inv�lida\n';
					erro_formulario=true;
				}
			}
			
			if(erro_formulario) {
				alert(msg);
				document.getElementsByName('paarecurso')[0].checked=false;
				document.getElementsByName('paarecurso')[1].checked=false;
				jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
				return false;
			}
			
			paaid = salvaAcaoAjax();
			jQuery("[name='requisicao']").val('atualizarAcaoPlanoEstrategico');
			jQuery("[name='paaid']").val(paaid);
		}
		
		if(resp == true){
			var erro_formulario=false;
			var msg='Foram encontrados erros:\n\n';
			if(document.getElementById('aapid').value=='') {
				msg+='- Selecione uma a��o\n'; 
				erro_formulario=true;
			}
			if(document.getElementById('paaacaoqtd').value=='') {
				msg+='- Preencha a quantidade\n';
				erro_formulario=true;
			}
			if(document.getElementById('oapid').value=='') {
				msg+='- Selecione um objeto\n';
				erro_formulario=true;
			}
			if(document.getElementById('paaperiodoinicio').value=='') {
				msg+='- Preencha a data inicial\n';
				erro_formulario=true;
			}
			if(!validaData(document.getElementById('paaperiodoinicio'))) {
				msg+='- Data inicial inv�lida\n';			
				erro_formulario=true;
			}
			if(document.getElementById('paaperiodofim').value=='') {
				msg+='- Preencha a data final\n';
				erro_formulario=true;
			}
			if(!validaData(document.getElementById('paaperiodofim'))) {
				msg+='- Data final inv�lida\n';
				erro_formulario=true;
			}
			if(document.getElementById('paadetalhamento').value=='') {
				msg+='- Preencha o detalhamento\n';
				erro_formulario=true;
			}
			if(erro_formulario) {
				alert(msg);
				document.getElementsByName('paarecurso')[0].checked=false;
				document.getElementsByName('paarecurso')[1].checked=false;
				jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
				return false;
			}
		
			jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=carregarBensServicos&paaid=" + paaid,
		   		async: false,
		   		success: function(msg){
		   			jQuery("#tr_bens_servicos").show();
		   			jQuery("#div_bensservicos").html(msg);
		   			jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
		   		}
		 		});
		}else{
			if(verificaBensServicos(paaid)){
				if(confirm("Existem Aquisi��es e Contrata��es vinculadas a esta A��o, deseja exclu�-las?")){
					excluirBensServicosPorAcao(paaid);
					jQuery("#tr_bens_servicos").hide();
					jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
				}else{
					jQuery("[name='paarecurso'][value='TRUE']").attr("checked","checked");
					jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
				}
			}else{
				jQuery("#div_bensservicos").html("");
				jQuery("#tr_bens_servicos").hide();
				jQuery("[name='btn_salvar'],[name='btn_cancelar']").attr("disabled","");
			}
		}
	}
	
	function salvaAcaoAjax()
	{
		var paaid = '';
		
		document.getElementById('paaacaoqtd').value=mascaraglobal('#######',document.getElementById('paaacaoqtd').value);
		
		var arrDados = jQuery("#formulario").serialize();
		jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: arrDados + "&requisicao=salvaAcaoAjax",
		   		async: false,
		   		success: function(msg){
		   			paaid = msg;
		   			}
		 		});
		 		return paaid;
	}
	
	function verificaBensServicos(paaid)
	{
		var count = false;
		jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=verificaBensServicos&paaid=" + paaid,
		   		async: false,
		   		success: function(msg){
		   			count = msg;
		   			}
		 		});
		 		
		 		if(count){
		 			return true;
		 		}else{
		 			return false;
		 		}
	}
	
	function excluirBensServicosPorAcao(paaid)
	{
		jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=excluirBensServicosPorAcao&paaid=" + paaid,
		   		async: false,
		   		success: function(msg){
		   			jQuery("#div_bensservicos").html("");
		   			}
		 		});
	}
	
	jQuery(document).ready(function() {
		carregarSaldoPdeEscolaAcao();
	<? if($paaid): ?>
		carregarBensServicos();
	<? endif; ?>
	});
	
	messageObj = new DHTML_modalMessage();	// We only create one object of this class
	messageObj.setShadowOffset(5);	// Large shadow
	
	function displayMessage(url) {
		var today = new Date();
		messageObj.setSource(url+'&hash='+today);
		messageObj.setCssClassMessageBox(false);
		messageObj.setSize(690,400);
		messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
		messageObj.display();
	}
	
	function closeMessage() {
		messageObj.close();	
	}
	
	function inserirBens()
	{
		var today = new Date();
		var paaid = jQuery("[name='paaid']").val();
		displayMessage('pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarBensServicos&paaid='+paaid+'&hash='+today);
	}
	</script>
	<form method="post" id="formulario">
	<input type="hidden" name="paeid" value="<?=$dados['paeid'] ?>">
	<input type="hidden" name="paaid" value="<?=$dados['paaid'] ?>">
	<input type="hidden" name="requisicao" value="<?=$requisicao ?>">
	<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
		<tr>
			<td class="SubTituloDireita"><font class="blue">Orienta��es:</font></td>
			<td class="blue">
		<p>Para facilitar a defini��o das a��es, o MEC selecionou previamente alguns verbos de a��o e os principais objetos para cada verbo. Neste caso, para construir a senten�a, a escola deve escolher a A��o, indicar a Quantidade e definir o Objeto da a��o. Depois, � necess�rio descrever o per�odo em que a escola pretende realizar aquela a��o e, no campo �Detalhamento a��o�, descrever algumas caracter�sticas da atividade que ser� realizada.</p> 
		<p>Depois de definir a a��o e seus objetivos, � necess�rio responder se s�o necess�rios recursos financeiros para realiza-la. Em caso afirmativo, selecione �Sim�. O sistema exibir� o bot�o �Inserir�. Neste caso, o GT deve indicar a �Categoria da despesa�, escolher um �Item� daquela categoria, definir a �Unidade de refer�ncia�, escolher �Quantidade� daquele item, descrever o �Valor unit�rio�, informar a �Fonte� e escolher se o item ser� adquirido com recursos da 1� parcela ou da 2� parcela. Observe que o saldo de recursos de cada parcela e a natureza da despesa v�o diminuindo � medida em que forem inseridos bens ou servi�os.</p>
		<p>O sistema calcular� o valor total e exibir� na rubrica capital ou custeio, de acordo com a classifica��o indicada na Portaria 448/2002. Caso exista diverg�ncia de classifica��o da natureza da despesa em rela��o aos crit�rios da sua secretaria, n�o inclua aquele item, a fim de que n�o haja problemas durante a execu��o do plano.</p>
		<p>E lembre-se! Antes de inserir as a��es, observe o saldo de recursos do Ano 1 e Ano 2 e os respectivos valores de capital e custeio.</p>
			</td>
		</tr>
		<tr>
			<td class="SubTituloCentro" colspan="2">A��o</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Saldo do PDE Escola</td>
			<td id="td_saldoPdeEscolaAcao"></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">A��o</td>
			<td><? $db->monta_combo('aapid', $sql_acao, 'S', 'Selecione', '', '', '', '', 'S', 'aapid','', $aapid); ?> Quantidade: <? echo campo_texto('paaacaoqtd', "S", "S", "Quantidade", 7, 6, "######", "", '', '', 0, 'id="paaacaoqtd"', '', $paaacaoqtd ); ?> Objeto <? $db->monta_combo('oapid', $sql_objeto, 'S', 'Selecione', '', '', '', '', 'S', 'oapid', '', $oapid); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Per�odo</td>
			<td><? echo campo_data2('paaperiodoinicio','S', 'S', 'Per�odo inicial', 'S', '', '', $paaperiodoinicio ); ?> a <? echo campo_data2('paaperiodofim','S', 'S', 'Per�odo final', 'S', '', '', $paaperiodofim); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Detalhamento da a��o</td>
			<td><? echo campo_textarea( 'paadetalhamento', 'S', 'S', '', '70', '4', '200', '', '', '','','',$paadetalhamento); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Precisa de recursos financeiros?</td>
			<td><input type="radio" onclick="exibePrecisaRecursos(true)" name="paarecurso" value="TRUE" <?=(($paarecurso=="t")?"checked":"") ?> > Sim <input type="radio" name="paarecurso" onclick="exibePrecisaRecursos(false)" value="FALSE" <?=(($paarecurso=="f")?"checked":"") ?> > N�o</td>
		</tr>
		<tr id="tr_bens_servicos" style="display:<?php echo $paarecurso && $paarecurso == "t" ? "" : "none" ?>" >
			<td class="SubTituloDireita">Bens e servi�os</td>
			<td>
				Clique em "Inserir" para incluir as aquisi��es e contrata��es desejadas.
				<p><input type="button" name="inserirbensservicos" value="Inserir" onclick="inserirBens()"></p>
				<div id="div_bensservicos"></div>
			</td>
		</tr>
		<tr>
			<td class="SubTituloCentro" colspan="2"><input type="button" value="Salvar" name="btn_salvar" onclick="submeterAcao();"> <input type="button"  name="btn_cancelar" value="Fechar" onclick="window.close();"></td>
		</tr>
		
	</table>
	</form>
	<?php verificaPermissao(PDEESC_PERFIL_DIRETOR); ?>
	</body>
	</html>
	<?
}

function carregarSaldoPdeEscolaAcao($dados) {
	global $db;
	
	$sql = "SELECT * FROM pdeinterativo2013.cargacapitalcusteio WHERE codinep='".$_SESSION['pdeinterativo2013_vars']['pdicodinep']."' AND cccstatus='A'";
	$cargacapitalcusteio = $db->pegaLinha($sql);
	

	$sql = "SELECT SUM(pabvalorcapital) as pabvalorcapital, SUM(pabvalorcusteiro) as pabvalorcusteiro, pabparcela 
			FROM pdeinterativo2013.planoacaobemservico pab 
			INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paaid = pab.paaid
			INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.paeid = paa.paeid 
			INNER JOIN pdeinterativo2013.planoacaoproblema pap ON pap.papid = pae.papid 
			WHERE pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND pabstatus='A' AND papstatus='A' AND paestatus='A' AND paastatus='A' 
			GROUP BY pabparcela";
	$planoacaobemservicos = $db->carregar($sql);
	if($planoacaobemservicos[0]) {
		foreach($planoacaobemservicos as $pbs) {
			$gastopbs[$pbs['pabparcela']] = array('pabvalorcapital'=>$pbs['pabvalorcapital'],'pabvalorcusteiro'=>$pbs['pabvalorcusteiro']);
		}
	}
			
	if($cargacapitalcusteio):
	?>
	<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3">
	<tr>
		<td class="SubTituloDireita">1� Parcela:</td>
		<td>R$ <?=number_format(($cargacapitalcusteio['ccccapitalprimeira']+$cargacapitalcusteio['ccccusteioprimeira']-$gastopbs['P']['pabvalorcapital']-$gastopbs['P']['pabvalorcusteiro']),2,",",".")." ( Capital: R$ ".number_format($cargacapitalcusteio['ccccapitalprimeira']-$gastopbs['P']['pabvalorcapital'],2,",",".").", Custeio: R$ ".number_format($cargacapitalcusteio['ccccusteioprimeira']-$gastopbs['P']['pabvalorcusteiro'],2,",",".").")" ?></td>
	</tr>
	<tr>
		<td class="SubTituloDireita">2� Parcela:</td>
		<td>R$ <?=number_format(($cargacapitalcusteio['ccccapitalsegunda']+$cargacapitalcusteio['ccccusteiosegunda']-$gastopbs['S']['pabvalorcapital']-$gastopbs['S']['pabvalorcusteiro']),2,",",".")." ( Capital: R$ ".number_format($cargacapitalcusteio['ccccapitalsegunda']-$gastopbs['S']['pabvalorcapital'],2,",",".").", Custeio: R$ ".number_format($cargacapitalcusteio['ccccusteiosegunda']-$gastopbs['S']['pabvalorcusteiro'],2,",",".").")" ?></td>
	</tr>
	</table>
	<? else : ?>
	<p>N�o foram encontrados dados sobre o saldo</p>
	<? endif;
}

function salvaAcaoAjax($dados)
{
	global $db;
	extract($dados);
	$paeid = !$paeid ? "null" : $paeid;
	$aapid = !$aapid ? "null" : $aapid;
	$oapid = !$oapid ? "null" : $oapid;
	$paaacaoqtd = !$paaacaoqtd ? "null" : $paaacaoqtd;
	$paaperiodoinicio = !$paaperiodoinicio || strlen($paaperiodoinicio)!=10 ? "null" : "'".formata_data_sql($paaperiodoinicio)."'";
	$paaperiodofim = !$paaperiodofim || strlen($paaperiodofim)!=10 ? "null" : "'".formata_data_sql($paaperiodofim)."'";
	$paadetalhamento = !$paadetalhamento ? "null" : "'".$paadetalhamento."'";
	$paarecurso = !$paarecurso ? "null" : "'".$paarecurso."'";
	$paacustototal = !$paacustototal ? "null" : "'".$paacustototal."'";
	$paastatus = "'A'";
	
	$sql = "INSERT INTO 
				pdeinterativo2013.planoacaoacao
			(paeid,aapid,oapid,paaacaoqtd,paaperiodoinicio,paaperiodofim,paadetalhamento,paarecurso,paacustototal,paastatus)
				VALUES
			($paeid,$aapid,$oapid,$paaacaoqtd,$paaperiodoinicio,$paaperiodofim,$paadetalhamento,$paarecurso,$paacustototal,$paastatus)
				RETURNING paaid";
	$paaid = $db->pegaUm($sql);
	$db->commit();
	apagarCachePdeInterativo();
	ob_clean();
	echo $paaid;
	
}

function atualizarAcaoPlanoEstrategico($dados) {
	global $db;
	if(is_numeric($dados['paaid'])) {
		
		if(formata_data_sql($dados['paaperiodoinicio'])>formata_data_sql($dados['paaperiodofim'])) die("<script>alert('Data Inicial maior do Data Final');window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarAcao&paeid=".$dados['paeid']."&paaid=".$dados['paaid']."';</script>");
		
		$sql = "UPDATE pdeinterativo2013.planoacaoacao
	   			SET aapid='".$dados['aapid']."', oapid='".$dados['oapid']."', 
	   				paaacaoqtd='".$dados['paaacaoqtd']."', paaperiodoinicio='".formata_data_sql($dados['paaperiodoinicio'])."', 
	    			paaperiodofim='".formata_data_sql($dados['paaperiodofim'])."', 
	       			paadetalhamento='".$dados['paadetalhamento']."', paarecurso=".(($dados['paarecurso'])?$dados['paarecurso']:"TRUE")."  
	 			WHERE paaid='".$dados['paaid']."';";
		$db->executar($sql);
		$db->commit();
		
		apagarCachePdeInterativo();
				
		echo "<script>
				window.opener.carregarPlanoEstrategicoDimensao();
				alert('A��o atualizada com sucesso');
				window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarAcao&paeid=".$dados['paeid']."&paaid=".$dados['paaid']."';
			  </script>";
	} else {
		echo "<script>
				alert('N�o foi poss�vel atualizar a��o. Feche a tela e tente novamente');
				window.close();
			  </script>";
	}
	
}

function comboItensCategorias($dados) {
	global $db;
	$sql = "SELECT ciaid as codigo, ciadesc as descricao FROM pdeinterativo2013.categoriaitemacao WHERE ciastatus='A' AND cacid='".$dados['cacid']."'";
	$db->monta_combo('ciaid', $sql, 'S', 'Selecione', 'selecionarItem', '', '', '', 'S', 'ciaid','', $dados['ciaid']);
}

function pegaUnidadeReferenciaItem($dados) {
	global $db;
	$sql = "SELECT ure.uredesc FROM pdeinterativo2013.unidadereferencia ure 
			INNER JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ureid = ure.ureid 
			WHERE ciaid='".$dados['ciaid']."'";
	
	$unrefe = $db->pegaUm($sql);
	
	echo (($unrefe)?$unrefe:"N�o cadastrado");
	
}



function atualizarBensServicos($dados) {
	global $db;
	// verificando se existe saldo 
	$sql = "SELECT * FROM pdeinterativo2013.cargacapitalcusteio WHERE codinep='".$_SESSION['pdeinterativo2013_vars']['pdicodinep']."' AND cccstatus='A'";
	$cargacapitalcusteio = $db->pegaLinha($sql);
	
	$problema = false;
	if($cargacapitalcusteio) {
		if($dados['pabparcela']=="P") $alt="primeira";
		if($dados['pabparcela']=="S") $alt="segunda";
		
		$outros_pabs = $db->pegaLinha("SELECT COALESCE(SUM(pabvalorcapital),0) as pabvalorcapital, COALESCE(SUM(pabvalorcusteiro),0) as pabvalorcusteiro 
								  	   FROM pdeinterativo2013.planoacaobemservico p 
								  	   INNER JOIN pdeinterativo2013.planoacaoacao po ON po.paaid = p.paaid 
								  	   INNER JOIN pdeinterativo2013.planoacaoestrategia pe ON pe.paeid = po.paeid 
								  	   INNER JOIN pdeinterativo2013.planoacaoproblema pp ON pp.papid = pe.papid  
								  	   WHERE pabparcela='".$dados['pabparcela']."' AND 
	   										 pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
								  	   		 pabstatus='A' AND papstatus='A' AND paestatus='A' AND paastatus='A' AND pabid!='".$dados['pabid']."'");
		
		if((str_replace(array(".",","),array("","."),$dados['pabvalorcapital'])+$outros_pabs['pabvalorcapital']) > $cargacapitalcusteio['ccccapital'.$alt]) $problema=true;
		if((str_replace(array(".",","),array("","."),$dados['pabvalorcusteiro'])+$outros_pabs['pabvalorcusteiro']) > $cargacapitalcusteio['ccccusteio'.$alt]) $problema=true;
		
		if($problema) die("N�o h� saldo dispon�vel nesta categoria de despesa ou nesta parcela.");
		
	}
	// FIM - verificando se existe saldo
	
	$sql = "UPDATE pdeinterativo2013.planoacaobemservico
   			SET ciaid='".$dados['ciaid']."', 
   				pabqtd='".$dados['pabqtd']."', pabvalor='".str_replace(array(".",","),array("","."),$dados['pabvalor'])."', 
   				pabvalorcapital=".(($dados['pabvalorcapital'])?"'".str_replace(array(".",","),array("","."),$dados['pabvalorcapital'])."'":"NULL").", 
       			pabvalorcusteiro=".(($dados['pabvalorcusteiro'])?"'".str_replace(array(".",","),array("","."),$dados['pabvalorcusteiro'])."'":"NULL").", 
       			pabfonte='".$dados['pabfonte']."', pabparcela=".(($dados['pabparcela'])?"'".$dados['pabparcela']."'":"NULL")."  
 			WHERE pabid='".$dados['pabid']."';";
	
	$db->executar($sql);
	$db->commit();
	$sql = "UPDATE pdeinterativo2013.planoacaoacao SET paacustototal=(SELECT SUM(COALESCE(pabvalorcapital,0)+COALESCE(pabvalorcusteiro,0)) 
			FROM pdeinterativo2013.planoacaobemservico WHERE pabstatus='A' AND paaid IN(SELECT paaid FROM pdeinterativo2013.planoacaobemservico WHERE pabid='".$dados['pabid']."' AND pabstatus='A')) 
			WHERE paaid IN(SELECT paaid FROM pdeinterativo2013.planoacaobemservico WHERE pabid='".$dados['pabid']."' AND pabstatus='A')";
	
	$db->executar($sql);
	$db->commit();
	
	apagarCachePdeInterativo();
	
}

function gerenciarBensServicos($dados) {
	global $db;

	$sql_categoria = "SELECT cacid as codigo, cacdesc as descricao, cacnatureza FROM pdeinterativo2013.categoriaacao WHERE cacstatus='A'";
	
	if($_SESSION['pdeinterativo2013_vars']['pditempdeescola']=="t") {
		$sql_fonte = array(0 => array("codigo" => "P", "descricao" => "PDDE/PDE Escola"),
						   1 => array("codigo" => "O", "descricao" => "Outras"));
	} else {
		$sql_fonte = array(0 => array("codigo" => "P", "descricao" => "Fonte da Secretaria"),
						   1 => array("codigo" => "O", "descricao" => "Outras"));
	}
	
	if($dados['pabid']) {
		$sql = "SELECT pab.pabid, cia.cacid, pab.ciaid as ciaid, pab.ureid as ureid, pabqtd, trim(to_char(pabvalor,'999g999g999d99')) as pabvalor,
  					   trim(to_char(pabvalorcapital,'999g999g999d99')) as pabvalorcapital,
  					   trim(to_char(pabvalorcusteiro,'999g999g999d99')) as pabvalorcusteiro,
					   pabfonte,
  					   pabparcela  
  				FROM pdeinterativo2013.planoacaobemservico pab 
				LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
				WHERE pab.pabid='".$dados['pabid']."' AND pab.pabstatus='A'";
		$planoacaobemservico = $db->pegaLinha($sql);
		
		if($planoacaobemservico) {
			extract($planoacaobemservico);
			$requisicao = "atualizarBensServicos";
		} else {
			$requisicao = "inserirBensServicos";
		}

	} else {
		$requisicao = "inserirBensServicos";
	}
	?>
	<script language="JavaScript" src="../includes/funcoes.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
	<script>
	function selecionarCategoriaItem(cacid) {
		if(cacid!='') {
			document.getElementById('pabvalorcapital').value = '';
			document.getElementById('pabvalorcusteiro').value = '';
			document.getElementById('td_itemcategoria').innerHTML = 'Carregando...';
			somarBensServicos('');
			jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=comboItensCategorias<?=(($ciaid)?"&ciaid=".$ciaid:"") ?>&cacid="+cacid,
		   		async: false,
		   		success: function(msg){document.getElementById('td_itemcategoria').innerHTML = msg;}
		 		});
		} else {
			document.getElementById('td_itemcategoria').innerHTML = 'Selecione categoria';
		}
	}
	
	function selecionarItem(ciaid) {
		if(ciaid) {
			document.getElementById('td_unreferencia').innerHTML = 'Carregando...';
			somarBensServicos('');
			jQuery.ajax({
		   		type: "POST",
		   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
		   		data: "requisicao=pegaUnidadeReferenciaItem&ciaid="+ciaid,
		   		async: false,
		   		success: function(msg){document.getElementById('td_unreferencia').innerHTML = msg;}
		 		});
	 	} else {
	 		document.getElementById('td_unreferencia').innerHTML = 'Selecione item';
	 	}
	}

	
	function submeterBensServicos() {
		if(document.getElementById('cacid').value=='') {
			alert('Selecione uma categoria');
			return false;
		}
		if(document.getElementById('ciaid').value=='') {
			alert('Selecione um item');
			return false;
		}
		document.getElementById('pabqtd').value = mascaraglobal('#####',document.getElementById('pabqtd').value);
		if(document.getElementById('pabqtd').value=='') {
			alert('Preencha a quantidade');
			return false;
		}
		document.getElementById('pabvalor').value = mascaraglobal('###.###.###,##',document.getElementById('pabvalor').value);
		if(document.getElementById('pabvalor').value=='') {
			alert('Preencha o valor unit�rio');
			return false;
		}
		if(document.getElementById('pabfonte').value=='') {
			alert('Selecione a fonte');
			return false;
		}
		if(document.getElementById('pabfonte').value!='O') {
			if(jQuery("[name^='pabparcela']:checked").length == 0) {
				alert('Selecione se precisa de recurso');
				return false;
			}
		}
		
		document.getElementById('btnsalvar').disabled=true;
		document.getElementById('btncancelar').disabled=true;
		
		jQuery.ajax({
	   		type: "POST",
	   		url: "pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A",
	   		data: "requisicao=<?=$requisicao ?>&"+jQuery('#formulario2').serialize(),
	   		async: false,
	   		success: function(msg){
	   		if(msg) {
	   			alert(msg);
				document.getElementById('btnsalvar').disabled=false;
				document.getElementById('btncancelar').disabled=false;
	   		} else {
			 	carregarBensServicos();
			 	carregarSaldoPdeEscolaAcao();
			 	closeMessage();
		 	}
	   		}
	 		});
	 		

		
	}
	
	/* Fun��o para subustituir todos */
	function replaceAll(str, de, para){
	    var pos = str.indexOf(de);
	    while (pos > -1){
			str = str.replace(de, para);
			pos = str.indexOf(de);
		}
	    return (str);
	}
	
	function exibeParcela(fonte) {
		if(fonte=="O") {
			document.getElementById('tr_parcela').style.display='none';
		} else {
			document.getElementById('tr_parcela').style.display='';
		}
	}

	
	function somarBensServicos(obj) {
	
		var bensservicos = new Array();
		
		if(document.getElementById('cacid').value=='') {
			alert('Selecione categoria');
			obj.value='';
			return false;
		}
		<? 
		$bensServicos = $db->carregar($sql_categoria);
		if($bensServicos[0]) {
			foreach($bensServicos as $bs) {
				echo "bensservicos[".$bs['codigo']."]='".$bs['cacnatureza']."';";
			}
		}
		?>
		var valorunit = replaceAll(replaceAll(document.getElementById('pabvalor').value, '.', ''),',','.');
		if(document.getElementById('pabqtd').value && valorunit) {
			var valortotal = parseFloat(document.getElementById('pabqtd').value)*parseFloat(valorunit);
			if(bensservicos[document.getElementById('cacid').value]=='A') {
				document.getElementById('pabvalorcapital').value = mascaraglobal('###.###.###,##',valortotal.toFixed(2));
			}
			if(bensservicos[document.getElementById('cacid').value]=='U') {
				document.getElementById('pabvalorcusteiro').value = mascaraglobal('###.###.###,##',valortotal.toFixed(2));
			}
		}
	}
	
	jQuery(document).ready(function() {
	<? if($cacid): ?>
	selecionarCategoriaItem('<?=$cacid ?>');
	<? endif; ?>
	});
	
	</script>
	<form method="post" id="formulario2">
	<input type="hidden" name="paaid" value="<?=$dados['paaid'] ?>">
	<input type="hidden" name="pabid" value="<?=$pabid ?>">
	<table class="listagem" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
		<tr>
			<td class="SubTituloCentro" colspan="2">Bens e Servi�os</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Categoria</td>
			<td><? $db->monta_combo('cacid', $sql_categoria, 'S', 'Selecione', 'selecionarCategoriaItem', '', '', '', 'S', 'cacid','', $cacid); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Item</td>
			<td id="td_itemcategoria">Selecione categoria</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Unidade de refer�ncia</td>
			<td id="td_unreferencia">Selecione item</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Quantidade</td>
			<td><? echo campo_texto('pabqtd', 'S', 'S', 'Quantidade', 6, 5, "#####", "", '', '', 0, 'id="pabqtd"', 'somarBensServicos(this);', $pabqtd ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor Unit�rio(R$)</td>
			<td><? echo campo_texto('pabvalor', 'S', 'S', 'Valor Unit�rio(R$)', 16, 16, "###.###.###,##", "", '', '', 0, 'id="pabvalor"', 'somarBensServicos(this);', $pabvalor ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Capital(R$)</td>
			<td><? echo campo_texto('pabvalorcapital', 'S', 'N', 'Capital(R$)', 16, 16, "###.###.###,##", "", '', '', 0, 'id="pabvalorcapital"', '', $pabvalorcapital ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Custeio(R$)</td>
			<td><? echo campo_texto('pabvalorcusteiro', 'S', 'N', 'Custeio(R$)', 16, 16, "###.###.###,##", "", '', '', 0, 'id="pabvalorcusteiro"', '', $pabvalorcusteiro ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Fonte</td>
			<td><? $db->monta_combo('pabfonte', $sql_fonte, 'S', 'Selecione', 'exibeParcela', '', '', '', 'S', 'pabfonte','', $pabfonte); ?></td>
		</tr>
		<tr id="tr_parcela" style="display:none;">
			<td class="SubTituloDireita">Parcela / Ano</td>
			<td>
			<input type="radio" name="pabparcela" value="P" <?=(($pabparcela=="P")?"checked":"") ?>>1� Parcela (2011)<br>
			<input type="radio" name="pabparcela" value="S" <?=(($pabparcela=="S")?"checked":"") ?>>2� Parcela (2012)
			</td>
		</tr>
		<tr>
			<td class="SubTituloCentro" colspan="2"><input type="button" id="btnsalvar" value="Salvar" onclick="submeterBensServicos();"> <input id="btncancelar" type="button" value="Cancelar" onclick="closeMessage();"></td>
		</tr>
	</table>
	</form>
	<?

}

function inserirBensServicos($dados) {
	global $db;
	
	// verificando se existe saldo 
	$sql = "SELECT * FROM pdeinterativo2013.cargacapitalcusteio WHERE codinep='".$_SESSION['pdeinterativo2013_vars']['pdicodinep']."' AND cccstatus='A'";
	$cargacapitalcusteio = $db->pegaLinha($sql);
	
	$problema = false;
	
	if(!is_numeric($dados['paaid']) || !$dados['paaid']) die("Houve problemas na inser��o do registro. Feche esta tela e reinicie o procedimento.");

	if($cargacapitalcusteio && $dados['pabfonte']!="O") {
		if($dados['pabparcela']=="P") $alt="primeira";
		if($dados['pabparcela']=="S") $alt="segunda";
		
		$outros_pabs = $db->pegaLinha("SELECT COALESCE(SUM(pabvalorcapital),0) as pabvalorcapital, COALESCE(SUM(pabvalorcusteiro),0) as pabvalorcusteiro 
								  	   FROM pdeinterativo2013.planoacaobemservico p 
								  	   INNER JOIN pdeinterativo2013.planoacaoacao po ON po.paaid = p.paaid 
								  	   INNER JOIN pdeinterativo2013.planoacaoestrategia pe ON pe.paeid = po.paeid 
								  	   INNER JOIN pdeinterativo2013.planoacaoproblema pp ON pp.papid = pe.papid  
								  	   WHERE pabparcela='".$dados['pabparcela']."' AND 
	   										 pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
								  	   		 pabstatus='A' AND papstatus='A' AND paestatus='A' AND paastatus='A'");
		
		if((str_replace(array(".",","),array("","."),$dados['pabvalorcapital'])+$outros_pabs['pabvalorcapital']) > $cargacapitalcusteio['ccccapital'.$alt]) $problema=true;
		if((str_replace(array(".",","),array("","."),$dados['pabvalorcusteiro'])+$outros_pabs['pabvalorcusteiro']) > $cargacapitalcusteio['ccccusteio'.$alt]) $problema=true;
		
		if($problema) die("N�o h� saldo dispon�vel nesta categoria de despesa ou nesta parcela.");
		
	}
	// FIM - verificando se existe saldo

	
	$sql = "INSERT INTO pdeinterativo2013.planoacaobemservico(
            paaid, ciaid, pabqtd, pabvalor, pabvalorcapital, 
            pabvalorcusteiro, pabfonte, pabparcela, pabstatus)
    		VALUES ('".$dados['paaid']."', '".$dados['ciaid']."',  
    				'".$dados['pabqtd']."', '".str_replace(array(".",","),array("","."),$dados['pabvalor'])."', 
    				".(($dados['pabvalorcapital'])?"'".str_replace(array(".",","),array("","."),$dados['pabvalorcapital'])."'":"NULL").", 
    				".(($dados['pabvalorcusteiro'])?"'".str_replace(array(".",","),array("","."),$dados['pabvalorcusteiro'])."'":"NULL").",
    				'".$dados['pabfonte']."', ".(($dados['pabparcela'])?"'".$dados['pabparcela']."'":"NULL").", 'A');";
	
	$db->executar($sql);
	$db->commit();
		
	$sql = "UPDATE pdeinterativo2013.planoacaoacao SET paacustototal=(SELECT SUM(COALESCE(pabvalorcapital,0)+COALESCE(pabvalorcusteiro,0)) FROM pdeinterativo2013.planoacaobemservico WHERE pabstatus='A' AND paaid='".$dados['paaid']."') 
			WHERE paaid='".$dados['paaid']."'";
	
	$db->executar($sql);
	$db->commit();
	
	apagarCachePdeInterativo();
	
	
}

function carregarBensServicos($dados) {
	global $db;
	if($dados['paaid']) {
		
		if(!is_numeric($dados['paaid'])) die("Ocorreu erro durante o carregamento. Feche a tela e tente novamente.");
		
		$sql = "SELECT '<center><span style=\"white-space:nowrap;\" ><img src=../imagens/alterar.gif style=cursor:pointer; onclick=\"displayMessage(\'pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarBensServicos&pabid='||pab.pabid||'\');\"> <img src=../imagens/excluir.gif style=cursor:pointer; onclick=\"deletarBensServicos('||pab.pabid||')\"></span></center>' as acao, cac.cacdesc, cit.ciadesc, unr.uredesc, pab.pabqtd,
					   pabvalor,
  					   pabvalorcapital,
  					   pabvalorcusteiro,
  					   CASE WHEN pabfonte='P' THEN 'Fonte da secretaria' ELSE 'Outras' END,
  					   CASE WHEN pabparcela='P' THEN '1� Parcela (2011)' 
  					   		WHEN pabparcela='S' THEN '2� Parcela (2012)' 
  					   		ELSE '&nbsp;' END as pabfonte
  					     
				FROM pdeinterativo2013.planoacaobemservico pab 
				LEFT JOIN pdeinterativo2013.categoriaitemacao cit ON cit.ciaid = pab.ciaid 
				LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cit.cacid
				LEFT JOIN pdeinterativo2013.unidadereferencia unr ON unr.ureid = cit.ureid
				WHERE paaid='".$dados['paaid']."' AND pabstatus='A'";
	} else {
		$sql = array();
	}
	$cabecalho = array("A��o","Categoria","Item","Unidade de referencia","Qtd","Valor(R$)","Capital(R$)","Custeio(R$)","Fonte","Parcela/Ano");
	$db->monta_lista_simples($sql,$cabecalho,50,5,'N','100%',$par2);
	
}


function verificaBensServicos($dados)
{
	global $db;
	
	if(!is_numeric($dados['paaid'])) {
		return false;
	}
	
	if($dados['paaid']) {
		$sql = "SELECT count(*)
				FROM pdeinterativo2013.planoacaobemservico pab
				WHERE paaid='".$dados['paaid']."' AND pabstatus='A'";
		$count = $db->pegaUm($sql);
		if($count){
			echo $count;
		}else{
			return false;
		}
	} else {
		return false;
	}
}

function excluirBensServicosPorAcao($dados)
{
	global $db;
	if(is_numeric($dados['paaid'])) {
		$sql = "UPDATE pdeinterativo2013.planoacaobemservico set pabstatus = 'I'
				WHERE paaid='".$dados['paaid']."' AND pabstatus='A'";
		$db->executar($sql);
		$db->commit();
		apagarCachePdeInterativo();
		
		return true;
	} else {
		return false;
	}
}

function inserirAcaoPlanoEstrategico($dados) {
	global $db;
	
	if(formata_data_sql($dados['paaperiodoinicio'])>formata_data_sql($dados['paaperiodofim'])) die("<script>alert('Data Inicial maior do Data Final');window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarAcao&paeid=".$dados['paeid']."&paaid=';</script>");
	if(!$dados['paarecurso']) $dados['paarecurso']="TRUE";
	
	$sql = "INSERT INTO pdeinterativo2013.planoacaoacao(
            paeid, aapid, oapid, paaacaoqtd, paaperiodoinicio, paaperiodofim, 
            paadetalhamento, paarecurso, paacustototal, paastatus)
    		VALUES ('".$dados['paeid']."', 
    				'".$dados['aapid']."', 
    				'".$dados['oapid']."', 
    				'".$dados['paaacaoqtd']."', 
    				".(($dados['paaperiodoinicio'])?"'".formata_data_sql($dados['paaperiodoinicio'])."'":"NULL").", 
    				".(($dados['paaperiodofim'])?"'".formata_data_sql($dados['paaperiodofim'])."'":"NULL").", 
    				'".$dados['paadetalhamento']."', 
    				".$dados['paarecurso'].",
            		0, 
            		'A') RETURNING paaid;";
	$paaid = $db->pegaUm($sql);
	$db->commit();
	
	apagarCachePdeInterativo();
	
	echo "<script>
			window.opener.carregarPlanoEstrategicoDimensao();
			alert('A��o inserida com sucesso');
			window.location='pdeinterativo2013.php?modulo=principal/planoestrategico&acao=A&requisicao=gerenciarAcao&paeid=".$dados['paeid']."&paaid=".$paaid."';
		  </script>";
}

function planoestrategicoDimensao($dados) {
	global $db;

	$sql_objetivo = "SELECT aoaid as codigo, aoadesc as descricao FROM pdeinterativo2013.apoioobjetivoplanoacao WHERE aoastatus='A' and abaid = {$dados['abaid']}";
	
	$aoaid = $db->pegaUm("SELECT aoaid 
						  FROM pdeinterativo2013.objetivoplanoacao 
						  WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND abaid='".$dados['abaid']."' AND opastatus='A'");

	?>
	<tr>
		<td style="cursor:pointer" onclick="exibeDimensao('<?php echo $dados['abaid'] ?>')" class="SubTituloTabela center bold" colspan="2"><img style="display:none" id="img_mais_<?php echo $dados['abaid'] ?>" src="../imagens/mais.gif" /> <img style="display:none" id="img_menos_<?php echo $dados['abaid'] ?>" src="../imagens/menos.gif" /> <?=$dados['abadescricao'] ?></td>
	</tr>

	<?php

		$funcao = "listametas_".$dados['abacod'];
		$metas = $funcao($dados);
	?>
	<tr class="dimensao_<?php echo $dados['abaid'] ?>" >
		<td colspan="2" valign="top">
		<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
		<?
		$sql = "SELECT prb.papid as codigo, prb.papdescricao as descricao 
				FROM pdeinterativo2013.planoacaoproblema prb 
				WHERE prb.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
					  prb.papstatus='A' AND 
					  prb.abacod='".$dados['abacod']."'   
				ORDER BY prb.papid";
		$problemas = $db->carregar($sql);
		?>
			<?php if($problemas[0]): ?>
			
				<?php if($metas): ?>
					<tr class="dimensao_<?php echo $dados['abaid'] ?>" >
						<td class="SubTituloDireita">Objetivo</td>
						<td colspan="2"><? $db->monta_combo('aoaid['.$dados['abaid'].']', $sql_objetivo, 'S', 'Selecione', 'salvarFormularioParcialmente', '', '', '', 'N', 'aoaid['.$dados['abaid'].']', '',$aoaid); ?></td>
					</tr>
					<tr class="dimensao_<?php echo $dados['abaid'] ?>" >
					
						<td class="SubTituloDireita">Metas</td>
						<td colspan="2"><?php echo implode("<br>",$metas); ?></td>
					</tr>
				<?php else: ?>
					<tr class="dimensao_<?php echo $dados['abaid'] ?>" >
						<td colspan="3" class="center">N�o foram identificados metas</td>
					</tr>
				<?php endif; ?>

				<tr>
					<td class="SubTituloCentro" style="border: 1px solid #000;" width="33%">Problemas</td>
					<td class="SubTituloCentro" style="border: 1px solid #000;" width="25%">Estrat�gia</td>
					<td class="SubTituloCentro" style="border: 1px solid #000;" width="42%">A��o</td>
				</tr>
			<?php endif; ?>
			<?
			$sql_responsaveis = "SELECT DISTINCT * FROM (
								 SELECT 'P_'||p.pesid as codigo, p.pesnome as descricao FROM pdeinterativo2013.pessoa p 
								 INNER JOIN pdeinterativo2013.pessoagruptrab pa ON pa.pesid = p.pesid 
								 INNER JOIN pdeinterativo2013.grupotrabalho gt ON gt.grtid = pa.grtid 
								 WHERE gt.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' 
								 UNION ALL 
								 SELECT 'P_'||p.pesid as codigo, p.pesnome as descricao FROM pdeinterativo2013.pessoa p 
								 INNER JOIN pdeinterativo2013.membroconselho mc ON mc.pesid = p.pesid 
								 WHERE mc.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' 
								 UNION ALL 
								 SELECT 'P_'||p.pesid as codigo, p.pesnome as descricao FROM pdeinterativo2013.pessoa p 
								 INNER JOIN pdeinterativo2013.demaisprofissionais dp ON dp.pesid = p.pesid 
								 WHERE dp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'
								 UNION ALL 
								 SELECT 'D_'||d.pk_cod_docente as codigo, d.no_docente as descricao FROM educacenso_2010.tab_docente_disc_turma t 
								 INNER JOIN educacenso_2010.tab_docente d ON d.pk_cod_docente = t.fk_cod_docente 
								 INNER JOIN pdeinterativo2013.respostadocente r ON r.pk_cod_docente = d.pk_cod_docente
								 WHERE fk_cod_entidade='".$_SESSION['pdeinterativo2013_vars']['pdicodinep']."' AND r.rdovinculo IN('T','E')
								 ) foo
								 ";
			
			if($problemas):
			$i=1;
			foreach($problemas as $key => $problema) :
			
			if($problema['codigo']) {
				$sql = "SELECT * FROM pdeinterativo2013.planoacaoestrategia pe 
						INNER JOIN pdeinterativo2013.estrategiaplanoacaoapoio ea ON ea.epaid = pe.epaid 
						WHERE pe.papid='".$problema['codigo']."' AND paestatus='A'";
				$estrategias = $db->carregar($sql);
			}
			
			?>
			<tr>
				<td rowspan="<?=(count($estrategias)+1) ?>" valign="top" class="bordapreto center"><?=$problema['descricao'] ?>&nbsp;</td>
				<td style="background-color:#DCDCDC" colspan="2" class="bordapreto">
					<input style="margin-left:90px" type="button" value="Inserir estrat�gia" name="estrategia" onclick="gerenciarEstrategias('<?=$dados['abaid'] ?>','<?=$problema['codigo'] ?>');">
					<?php $arrPerfil = pegaPerfilGeral(); ?>
					<?php if(!in_array(PDEESC_PERFIL_DIRETOR,$arrPerfil) && (in_array(PDEINT_PERFIL_COMITE_PAR_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_PAR_MUNICIPAL, $arrPerfil)||in_array(PDEINT_PERFIL_CONSULTA_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_CONSULTA_MUNICIPAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_MUNICIPAL, $arrPerfil))): ?>
						<a href="#" onclick="gerenciarEstrategias('<?=$dados['abaid'] ?>','<?=$problema['codigo'] ?>');"><img src="../imagens/consultar.gif" > Visualizar estrat�gias</a>
					<?php endif; ?>
				</td>
			</tr>
			<?
			if($estrategias[0]) :
				foreach($estrategias as $estrategia):
				
			?>
			<tr>
				<td style="background-color:#DCDCDC"  class="bordapreto center"><?=$estrategia['epadesc'] ?><br>
				<br>
				<b>Responsavel:</b><br>
				<? $db->monta_combo('pesid_estrategias['.$estrategia['paeid'].']', $sql_responsaveis, 'S', 'Selecione', 'salvarFormularioParcialmente', '', '', '200', 'N', 'pesid_estrategias', '', $estrategia['respid']); ?>
				</td>
				<td style="background-color:#DCDCDC" valign="top" class="bordapreto">
				<p><input type="button" value="Inserir a��o" onclick="gerenciarAcao('<?=$estrategia['paeid'] ?>','');"></p>
				<? 
				$arrAcao = false;
				
				if(!in_array(PDEESC_PERFIL_DIRETOR,$arrPerfil) && (in_array(PDEINT_PERFIL_COMITE_PAR_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_PAR_MUNICIPAL, $arrPerfil)||in_array(PDEINT_PERFIL_CONSULTA_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_CONSULTA_MUNICIPAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_ESTADUAL, $arrPerfil)||in_array(PDEINT_PERFIL_COMITE_MUNICIPAL, $arrPerfil))){
					$sql = "SELECT 
						paaid,
						'<span style=\"white-space:nowrap;\" >
						<img src=\"../imagens/consultar.gif\" style=\"cursor:pointer;\" onclick=\"gerenciarAcao('||paa.paeid||','||paa.paaid||');\"> 
						</span>' as acao, 
						aap.aapdesc, 
						paa.paaacaoqtd, 
						oap.oapdesc, 
						case when char_length(paa.paadetalhamento) > 30
							then '<span class=\"tooltip\" title=\"' || paa.paadetalhamento || '\" >' || substring(paa.paadetalhamento,0,30) || '...</span>'
							else paa.paadetalhamento
						end as paadetalhamento, 
						to_char(paaperiodoinicio,'dd/mm/YYYY')||' a '||to_char(paaperiodofim,'dd/mm/YYYY') as periodo, 
						CASE WHEN paarecurso=TRUE THEN 'Sim' ELSE 'N�o' END as recurso, paa.paacustototal 
						FROM pdeinterativo2013.planoacaoacao paa 
						LEFT JOIN pdeinterativo2013.acaoapoio aap ON aap.aapid = paa.aapid 
						LEFT JOIN pdeinterativo2013.objetoapoio oap ON oap.oapid = paa.oapid
						WHERE paeid='".$estrategia['paeid']."' AND paa.paastatus='A'";
				}else{
					$sql = "SELECT 
						paaid,
						'<span style=\"white-space:nowrap;\" >
						<img src=\"../imagens/alterar.gif\" style=\"cursor:pointer;\" onclick=\"gerenciarAcao('||paa.paeid||','||paa.paaid||');\"> 
						<img src=../imagens/excluir.gif style=cursor:pointer; onclick=\"excluirAcao('||paa.paaid||');\">
						</span>' as acao, 
						aap.aapdesc, 
						paa.paaacaoqtd, 
						oap.oapdesc, 
						case when char_length(paa.paadetalhamento) > 30
							then '<span class=\"tooltip\" title=\"' || paa.paadetalhamento || '\" >' || substring(paa.paadetalhamento,0,30) || '...</span>'
							else paa.paadetalhamento
						end as paadetalhamento, 
						to_char(paaperiodoinicio,'dd/mm/YYYY')||' a '||to_char(paaperiodofim,'dd/mm/YYYY') as periodo, 
						CASE WHEN paarecurso=TRUE THEN 'Sim' ELSE 'N�o' END as recurso, paa.paacustototal 
						FROM pdeinterativo2013.planoacaoacao paa 
						LEFT JOIN pdeinterativo2013.acaoapoio aap ON aap.aapid = paa.aapid 
						LEFT JOIN pdeinterativo2013.objetoapoio oap ON oap.oapid = paa.oapid
						WHERE paeid='".$estrategia['paeid']."' AND paa.paastatus='A'";	
				}
				
				$arrDados = $db->carregar($sql);
				if($arrDados){
					$n = 0;
					foreach($arrDados as $dadosCia){
						$arrAcao[$n] = $dadosCia;
						$sql = "select 
									cia.ciadesc
								from 
									pdeinterativo2013.planoacaobemservico ben
								inner join
									pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = ben.ciaid 
								where 
									paaid = {$dadosCia['paaid']}";
						$arrServicos = $db->carregarColuna($sql);
						$arrServicos = !$arrServicos ? array("N/A") : $arrServicos;
						$arrAcao[$n]['servicos'] = implode(", ",$arrServicos);
						$num_let = strlen($arrAcao[$n]['servicos']);
						if($num_let > 30){
							$arrAcao[$n]['servicos'] = "<span class=\"tooltip\" title=\"".$arrAcao[$n]['servicos']."\" />".substr($arrAcao[$n]['servicos'],0,30)."...</span>";
						}
						unset($arrAcao[$n]['paaid']);
						$n++;
					}
				}
				$arrAcao = !$arrAcao ? array() : $arrAcao;
				$cabecalho = array("&nbsp","A��o","Qtd","Objeto","Detalhamento","Per�odo","Recurso?","Total a��o","Bens e servi�os");
				echo "<div style='background-color:#ffffff' >";
				$db->monta_lista_simples($arrAcao,$cabecalho,50,5,'N','100%',$par2);
				echo "</div>"; 
				?>
				</td>
			</tr>
			<? endforeach; ?>
			<? else: ?>
			<tr>
				<td class="SubTituloCentro" style="border: 1px solid #000;" colspan="2">N�o existem estrat�gias cadastradas</td>
			</tr>
			<? endif; ?>

			<? endforeach; ?>
			<? endif; ?>
		</table>		
		</td>
	</tr>
	<?
}


function listaproblemas_diagnostico_2_distorcaoeaproveitamento($dados) {
	global $db;
	
	
	//In�cio - Distor��o Idade-S�rie (D,D)
	?>
	<?php $arrDistorcao = carregaDistorcaoDiagnosticoMatricula(false,"M",null); ?>
	<?php $arrRespEscola = array() ?>
	<?php if( $arrDistorcao && verificaTurmasCNE(array_unique($arrDistorcao)) ): ?>
			<?php $arrProblemas['descricao'][] = "Em 2010, a escola possu�a ".count($arrDistorcao)." turma(s) com n� de matr�culas superior ao par�metro do CNE." ?>
			<?php $arrProblemas['codigo'][] = "matricula_".implode("_",$arrDistorcao) ?>
	<?php endif; ?>
	
	<?php $arrDistorcao = carregaDistorcaoDiagnosticoMatricula(null,"D","D"); ?>
	<?php $arrRespEscola = array() ?>
	<?php $arrRespEscola = recuperaRespostasEscola(null,"D","D",null,array("(op.oppdesc ilike 'Nunca' or op.oppid = ".OPP_RARAMENTE.")")); ?>
	<?php if( $arrDistorcao && verificaCheckBoxTaxa("distorcao",array_unique($arrDistorcao)) ): ?>
			<?php $arrProblemas['descricao'][] = "Em 2010, a escola possu�a ".count($arrDistorcao)." turma(s) com taxa de distor��o superior � m�dia do Brasil." ?>
			<?php $arrProblemas['codigo'][] = "distorcao_".implode("_",$arrDistorcao) ?>
	<?php endif; ?>
	<?php if($arrRespEscola): ?>
		<?php foreach($arrRespEscola as $resp): ?>
			<?php if( verificaCheckBoxPergunta($resp['repid']) ): ?>
				<?php $arrProblemas['descricao'][] = str_replace( array("(*)","?") , array(strtolower($resp['oppdesc']),".") ,$resp['prgdesc']) ?>
				<?php $arrProblemas['codigo'][] = $resp['repid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	
	<?php $arrDistorcaoReprovacao = carregaDistorcaoDiagnosticoMatricula(null,"A","R"); ?>
	<?php $arrDistorcaoAbandono = carregaDistorcaoDiagnosticoMatricula(null,"A","A"); ?>
	<?php $arrRespEscola = array() ?>
	<?php $arrRespEscola = recuperaRespostasEscola(null,"D","A",null,array("(op.oppdesc ilike 'Nunca' or op.oppid = ".OPP_RARAMENTE.")")); ?>
	<?php if( $arrDistorcaoReprovacao && verificaCheckBoxTaxa("reprovacao",array_unique($arrDistorcaoReprovacao)) ): ?>
			<?php $arrProblemas['descricao'][] = "Em 2010, a escola possu�a ".count($arrDistorcaoReprovacao)." turma(s) com taxa de reprova��o superior � m�dia do Brasil." ?>
			<?php $arrProblemas['codigo'][] = "reprovacao_".implode("_",$arrDistorcaoReprovacao) ?>
	<?php endif; ?>
	<?php if( $arrDistorcaoAbandono && verificaCheckBoxTaxa("abandono",array_unique($arrDistorcaoAbandono)) ): ?>
			<?php $arrProblemas['descricao'][] = "Em 2010, a escola possu�a ".count($arrDistorcaoAbandono)." turma(s) com taxa de abandono superior � m�dia do Brasil." ?>
			<?php $arrProblemas['codigo'][] = "abandono_".implode("_",$arrDistorcaoAbandono) ?>
	<?php endif; ?>
	<?php if($arrRespEscola): ?>
		<?php foreach($arrRespEscola as $resp): ?>
			<?php if( verificaCheckBoxPergunta($resp['repid']) ): ?>
				<?php $arrProblemas['descricao'][] = str_replace( array("(*)","?") , array(strtolower($resp['oppdesc']),".") ,$resp['prgdesc']) ?>
				<?php $arrProblemas['codigo'][] = $resp['repid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	
	<?php $arrTurmas = recuperaTurmasCriticasPorEscola();?>
	<?php $arrTaxaReprovacaoBrasil = carregaTaxa(); ?>
	<?php $arrDisciplinas = retornaDisciplinasTurma(); ?>
	<?php $arrDistorcaoTaxaReprovacaoDisciplina = carregaDistorcaoTaxaReprovacaoDisciplina(); ?>
	<?php $arrDistorcaoTaxa = carregaDistorcaoDiagnosticoTaxaEscolar(); ?>
	<?php $arrRespEscola = array() ?>
	<?php $arrRespEscola = recuperaRespostasEscola(null,"D","C",null,array("(op.oppdesc ilike 'Nunca' or op.oppid = ".OPP_RARAMENTE.")")); ?>
	<?php if($arrTurmas): ?>
		<?php if($arrTurmas['Ensino Fundamental']): ?>
			<?php foreach($arrTurmas['Ensino Fundamental'] as $em): ?>
				<?php if($arrDisciplinas[$em['pk_cod_turma']]): ?>
					<?php foreach($arrDisciplinas[$em['pk_cod_turma']] as $ds): ?>
						<?php if( $arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']] != "" && $em['nummatricula'] ): ?>
							<?php if( round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0) > $arrTaxaReprovacaoBrasil['U']): ?>
								<?php $arrTurmasCriticasReprovacao["U"][$ds['pk_cod_disciplina']][] = $em['pk_cod_turma'] ?>
								<?php $arrNomeDisciplina[$ds['pk_cod_disciplina']] = $ds['no_disciplina'] ?>
								<?php $arrTurmasC["U"][$ds['pk_cod_disciplina']][$em['pk_cod_turma']] = array( 
																				"disciplina" => $ds['no_disciplina'],
																				"serie" => $em['serie'],
																				"turma" => $em['turma'],
																				"horario" => $em['hrinicio']." - ".$em['hrfim'],
																				"taxaBrasil" => $arrTaxaReprovacaoBrasil['U']." %",
																				"taxa" => round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0)." %"
																			   ) ?>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if($arrTurmas['Ensino M�dio']): ?>
			<?php foreach($arrTurmas['Ensino M�dio'] as $em): ?>
				<?php if($arrDisciplinas[$em['pk_cod_turma']]): ?>
					<?php foreach($arrDisciplinas[$em['pk_cod_turma']] as $ds): ?>
						<?php if( $arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']] != "" && $em['nummatricula'] ): ?>
							<?php if( round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0) > $arrTaxaReprovacaoBrasil['M']): ?>
								<?php $arrTurmasCriticasReprovacao["M"][$ds['pk_cod_disciplina']][] = $em['pk_cod_turma'] ?>
								<?php $arrNomeDisciplina[$ds['pk_cod_disciplina']] = $ds['no_disciplina'] ?>
								<?php $arrTurmasC["M"][$ds['pk_cod_disciplina']][$em['pk_cod_turma']] = array( 
																				"disciplina" => $ds['no_disciplina'],
																				"serie" => $em['serie'],
																				"turma" => $em['turma'],
																				"horario" => $em['hrinicio']." - ".$em['hrfim'],
																				"taxaBrasil" => $arrTaxaReprovacaoBrasil['M']." %",
																				"taxa" => round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0)." %"
																			   ) ?>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>
	<?php if($arrTurmasCriticasReprovacao["U"]): ?>
		<?php foreach($arrTurmasCriticasReprovacao["U"] as $disciplina => $arrTurmas): ?>
			<?php if( verificaCheckBoxDisciplina($disciplina,$arrTurmas) ): ?>
				<?php $arrProblemas['descricao'][] = count($arrTurmas)." turma(s) do ensino fundamental aprensentou(aram) taxa de reprova��o em ".$arrNomeDisciplina[$disciplina]." superior(es) � m�dia do Brasil." ?>
				<?php $arrProblemas['codigo'][] = "disciplina_{$disciplina}_".implode("_",$arrTurmas) ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if($arrTurmasCriticasReprovacao["M"]): ?>
		<?php foreach($arrTurmasCriticasReprovacao["M"] as $disciplina => $arrTurmas): ?>
			<?php if( verificaCheckBoxDisciplina($disciplina,$arrTurmas) ): ?>
				<?php $arrProblemas['descricao'][] = count($arrTurmas)." turma(s) do ensino m�dio aprensentou(aram) taxa de reprova��o em ".$arrNomeDisciplina[$disciplina]." superior(es) � m�dia do Brasil. <a href=\"javascript:exibeTurma(\'m_$disciplina\')\" >Clique aqui para exibir a(s) turma(s).</a>" ?>
				<?php $arrProblemas['codigo'][] = "disciplina_{$disciplina}_".implode("_",$arrTurmas) ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if($arrRespEscola): ?>
		<?php foreach($arrRespEscola as $resp): ?>
			<?php if( verificaCheckBoxPergunta($resp['repid']) ): ?>
				<?php $arrProblemas['descricao'][] = str_replace( array("(*)","?") , array(strtolower($resp['oppdesc']),".") ,$resp['prgdesc']) ?>
				<?php $arrProblemas['codigo'][] = $resp['repid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif;
	if($arrProblemas['descricao']){
		foreach($arrProblemas['descricao'] as $chave => $prob){
			$arrProblemasDistorcaoAproveitamento[$chave]['descricao'] = $prob;
			$arrProblemasDistorcaoAproveitamento[$chave]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$arrProblemas['codigo'][$chave]."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$arrProblemasDistorcaoAproveitamento[$chave]['codigo']){
				$arrProblemasDistorcaoAproveitamento[$chave]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$arrProblemas['codigo'][$chave]."', 'A', '".$prob."', 'diagnostico_2_distorcaoeaproveitamento') RETURNING papid;");
				$db->commit();
			}
		}	
	}
	//Fim - Distor��o Idade-S�rie (D,D)

	return $arrProblemasDistorcaoAproveitamento;
}

function listametas_diagnostico_2_distorcaoeaproveitamento() {
	 global $db;
	 
	 $arrDistorcao = carregaDistorcaoDiagnosticoMatricula(null,"D","D");
	 
	 $sql = "select distinct
				pk_cod_turma,
				no_etapa_ensino,
				no_turma,
				hr_inicial || ':' || hr_inicial_minuto as hrinicio,
				hr_final || ':' || hr_final_minuto as hrfim,
				CASE 
					WHEN dianumdistorcao > 0
						THEN (case when (select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma and t.id_status = 1)>0 then round(( (dianumdistorcao::numeric/(select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma and t.id_status = 1)::numeric)*100)) else round((dianumdistorcao::numeric/1*100)) end)
					when dianumdistorcao = 0
						THEN 0
					ELSE null
				END || ' %' as taxa
			from 
				educacenso_2010.tab_turma turma
			inner join
				educacenso_2010.tab_etapa_ensino etapa ON etapa.pk_cod_etapa_ensino = turma.fk_cod_etapa_ensino
			left join
				pdeinterativo2013.distorcaoaproveitamento dia ON dia.fk_cod_turma = turma.pk_cod_turma
			where 
				dia.pdeid = {$_SESSION['pdeinterativo2013_vars']['pdeid']}
			and
				dia.diasubmodulo = 'D'
			and
				dianumdistorcao is not null
			and
				dia.diastatus = 'A'
			order by
				no_etapa_ensino";
	
	 
	 $arrDados = $db->carregar($sql);
	
	if( $arrDados && $arrDistorcao && verificaCheckBoxTaxa("distorcao",array_unique($arrDistorcao)) ){
		

			
			foreach($arrDados as $dado){
			if(strstr($dado['no_etapa_ensino'],"Ensino Fundamental")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'D' and mt.metdetalhe='Ensino Fundamental' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid
				ORDER BY mt.metid";
		
				
				
				$metas_idade_serie_ef = $db->carregar($sql);
				continue;
			}
			if(strstr($dado['no_etapa_ensino'],"Ensino M�dio")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'D' and mt.metdetalhe='Ensino M�dio' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid
				ORDER BY mt.metid";
		
				
				$metas_idade_serie_em = $db->carregar($sql);
				continue;
			}
		}
	}
	 
	if($metas_idade_serie_ef[0]) {
		foreach($metas_idade_serie_ef as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	if($metas_idade_serie_em[0]) {
		foreach($metas_idade_serie_em as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	
	$arrDistorcaoReprovacao = carregaDistorcaoDiagnosticoMatricula(null,"A","R");
	$arrDistorcaoAbandono = carregaDistorcaoDiagnosticoMatricula(null,"A","A");
	
	$sql = "select distinct
				pk_cod_turma,
				no_etapa_ensino,
				no_turma,
				hr_inicial || ':' || hr_inicial_minuto as hrinicio,
				hr_final || ':' || hr_final_minuto as hrfim,
				CASE 
					WHEN dianumreprovado > 0
						THEN (case when (select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma)>0 then round(( (dianumreprovado::numeric/(select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma)::numeric)*100)) else round((dianumreprovado::numeric/1)::numeric)*100 end)
					when dianumreprovado = 0
						THEN 0
					ELSE null
				END || ' %' as taxa
			from 
				educacenso_2010.tab_turma turma
			inner join
				educacenso_2010.tab_etapa_ensino etapa ON etapa.pk_cod_etapa_ensino = turma.fk_cod_etapa_ensino
			left join
				pdeinterativo2013.distorcaoaproveitamento dia ON dia.fk_cod_turma = turma.pk_cod_turma
			where 
				dia.pdeid = {$_SESSION['pdeinterativo2013_vars']['pdeid']}
			and
				dia.diasubmodulo = 'A'
			and
				dia.diastatus = 'A'
			and
				dia.dianumreprovado is not null
			order by
				no_etapa_ensino";
	
	$arrDadosReprovado = $db->carregar($sql);
	
	$sql = "select distinct
				pk_cod_turma,
				no_etapa_ensino,
				no_turma,
				hr_inicial || ':' || hr_inicial_minuto as hrinicio,
				hr_final || ':' || hr_final_minuto as hrfim,
				CASE 
					WHEN dianumabandono > 0
						THEN (case when (select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma and t.id_status = 1)>0 then round(( (dianumabandono::numeric/(select distinct count(pk_cod_matricula) from educacenso_2010.tab_matricula t where t.fk_cod_turma = turma.pk_cod_turma and t.id_status = 1)::numeric)*100)) else round((dianumabandono::numeric/1)::numeric)*100 end)
					when dianumabandono = 0
						THEN 0
					ELSE null
				END || ' %' as taxa
			from 
				educacenso_2010.tab_turma turma
			inner join
				educacenso_2010.tab_etapa_ensino etapa ON etapa.pk_cod_etapa_ensino = turma.fk_cod_etapa_ensino
			left join
				pdeinterativo2013.distorcaoaproveitamento dia ON dia.fk_cod_turma = turma.pk_cod_turma
			where 
				dia.pdeid = {$_SESSION['pdeinterativo2013_vars']['pdeid']}
			and
				dia.diasubmodulo = 'A'
			and
				dia.diastatus = 'A'
			and
				dia.dianumabandono is not null
			order by
				no_etapa_ensino";
	
	$arrDadosAbandono = $db->carregar($sql);
	
	if( $arrDadosAbandono && $arrDistorcaoAbandono && verificaCheckBoxTaxa("abandono",array_unique($arrDistorcaoAbandono)) ){
		foreach($arrDadosAbandono as $dado){
			if(strstr($dado['no_etapa_ensino'],"Ensino Fundamental")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'A' and mt.metdetalhe='Ensino Fundamental' and metdesc ilike ('%abandono%') and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid
				ORDER BY mt.metid";
		
				$metas_aproveitamento_escolar_abandono_ef = $db->carregar($sql);
				continue;
			}
			if(strstr($dado['no_etapa_ensino'],"Ensino M�dio")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'A' and mt.metdetalhe='Ensino M�dio' and metdesc ilike ('%abandono%') and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
				$metas_aproveitamento_escolar_abandono_em = $db->carregar($sql);
				continue;
			}
		}
	}
	
	if( $arrDadosReprovado && $arrDistorcaoReprovacao && verificaCheckBoxTaxa("reprovacao",array_unique($arrDistorcaoReprovacao)) ){
		foreach($arrDadosReprovado as $dado){
			if(strstr($dado['no_etapa_ensino'],"Ensino Fundamental")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'A' and mt.metdetalhe='Ensino Fundamental' and metdesc ilike ('%reprova��o%') and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
				$metas_aproveitamento_escolar_reprovacao_ef = $db->carregar($sql);
				continue;
			}
			if(strstr($dado['no_etapa_ensino'],"Ensino M�dio")){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'A' and mt.metdetalhe='Ensino M�dio' and metdesc ilike ('%reprova��o%') and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
				$metas_aproveitamento_escolar_reprovacao_em = $db->carregar($sql);
				continue;
			}
		}
	}
	
	if($metas_aproveitamento_escolar_abandono_ef[0]) {
		foreach($metas_aproveitamento_escolar_abandono_ef as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	if($metas_aproveitamento_escolar_abandono_em[0]) {
		foreach($metas_aproveitamento_escolar_abandono_em as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	
	if($metas_aproveitamento_escolar_reprovacao_ef[0]) {
		foreach($metas_aproveitamento_escolar_reprovacao_ef as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	if($metas_aproveitamento_escolar_reprovacao_em[0]) {
		foreach($metas_aproveitamento_escolar_reprovacao_em as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	
	$arrTurmas = recuperaTurmasCriticasPorEscola();
	$arrTaxaReprovacaoBrasil = carregaTaxa();
	$arrDisciplinas = retornaDisciplinasTurma();
	$arrDistorcaoTaxaReprovacaoDisciplina = carregaDistorcaoTaxaReprovacaoDisciplina();
	$arrDistorcaoTaxa = carregaDistorcaoDiagnosticoTaxaEscolar();
	if($arrTurmas){
		if($arrTurmas['Ensino Fundamental']){
			foreach($arrTurmas['Ensino Fundamental'] as $em){
				if($arrDisciplinas[$em['pk_cod_turma']]){
					foreach($arrDisciplinas[$em['pk_cod_turma']] as $ds){
						if( $arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']] != "" && $em['nummatricula'] ){
							if( round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0) > $arrTaxaReprovacaoBrasil['U']){
								$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
								".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
								WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'C' and mt.metdetalhe='Ensino Fundamental' and mt.metstatus='A' 
								GROUP BY mt.mettipo, mt.metdesc, mt.metid 
								ORDER BY mt.metid";
								$metas_areas_conhecimento_ef = $db->carregar($sql);
								break;
							}
						}
					}
				}
			}
		}
		if($arrTurmas['Ensino M�dio']){
			foreach($arrTurmas['Ensino M�dio'] as $em){
				if($arrDisciplinas[$em['pk_cod_turma']]){
					foreach($arrDisciplinas[$em['pk_cod_turma']] as $ds){
						if( $arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']] != "" && $em['nummatricula'] ){
							if( round((($arrDistorcaoTaxaReprovacaoDisciplina[$em['pk_cod_turma']][$ds['pk_cod_disciplina']]/$em['nummatricula'])*100),0) > $arrTaxaReprovacaoBrasil['M']){
								$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
								".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
								WHERE mt.metmodulo = 'D' and mt.metsubmodulo = 'C' and mt.metdetalhe='Ensino M�dio' and mt.metstatus='A' 
								GROUP BY mt.mettipo, mt.metdesc, mt.metid 
								ORDER BY mt.metid";
						
								$metas_areas_conhecimento_em = $db->carregar($sql);
								break;
							}
						}
					}
				}
			}
		}
	}
	
	if($metas_areas_conhecimento_ef[0]) {
		foreach($metas_areas_conhecimento_ef as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	if($metas_areas_conhecimento_em[0]) {
		foreach($metas_areas_conhecimento_em as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	 
	 return $metas ? $metas : false;
}


function listametas_diagnostico_3_ensinoeaprendizagem($dados) {
	global $db;
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' and (prgdetalhe='Projeto Pedag�gico' or prgdetalhe='Projeto Pedag�gico - Educa��o Infantil')) AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";

	$existe_projetospedagogicos = $db->carregar($sql);
	if($existe_projetospedagogicos[0]) {
		
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
															        WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'E' and mt.metsubmodulo = 'P' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
		$metas_projetospedagogicos = $db->carregar($sql);
		if($metas_projetospedagogicos[0]) {
			foreach($metas_projetospedagogicos as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
		
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' and (prgdetalhe='Curr�culo - Educa��o Infantil' or prgdetalhe='Curr�culo') ) AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_curriculo = $db->carregar($sql);
	if($existe_curriculo[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT COALESCE(rmetaxa::varchar,'') FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
															        WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'E' and mt.metsubmodulo = 'P' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid  
				ORDER BY mt.metid";
		
		$metas_curriculo = $db->carregar($sql);
		if($metas_curriculo[0]) {
			foreach($metas_curriculo as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A' ) AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_avaliacoes = $db->carregar($sql);
	if($existe_avaliacoes[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT COALESCE(rmetaxa::varchar,'') FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
																	WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'E' and mt.metsubmodulo = 'P' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		$metas_avaliacoes = $db->carregar($sql);
		if($metas_avaliacoes[0]) {
			foreach($metas_avaliacoes as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'T' and prgstatus = 'A' and prgdetalhe='Tempo de Aprendizagem') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_tempoaprendizagem = $db->carregar($sql);
	
	$sql = "SELECT * FROM pdeinterativo2013.respostatempoaprendizagem WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND rtaporquecritico=TRUE";
	$respostatempoaprendizagem = $db->pegaLinha($sql);
	
	if($existe_tempoaprendizagem[0] || $respostatempoaprendizagem['rtacaso']=="N") {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT COALESCE(rmetaxa::varchar,'') FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
																	WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'E' and mt.metsubmodulo = 'T' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
		$metas_tempoaprendizagem = $db->carregar($sql);
		if($metas_tempoaprendizagem[0]) {
			foreach($metas_tempoaprendizagem as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	return $metas;
	
	
}


function listaproblemas_diagnostico_3_ensinoeaprendizagem($dados) {
	global $db;
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_projetospedagogicos = $db->carregar($sql);
	if($pergs_projetospedagogicos[0]) {
		foreach($pergs_projetospedagogicos as $pergs) {
			$problemasensinoaprendizagem[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasensinoaprendizagem[$pergs['repid']]['codigo']) {
				$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasensinoaprendizagem[$pergs['repid']]['descricao']."', 'diagnostico_3_ensinoeaprendizagem') RETURNING papid;");
				$db->commit();
			}
			
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_curriculo = $db->carregar($sql);
	if($pergs_curriculo[0]) {
		foreach($pergs_curriculo as $pergs) {
			$problemasensinoaprendizagem[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasensinoaprendizagem[$pergs['repid']]['codigo']) {
				$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasensinoaprendizagem[$pergs['repid']]['descricao']."', 'diagnostico_3_ensinoeaprendizagem') RETURNING papid;");
				$db->commit();				
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'P' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_avaliacoes = $db->carregar($sql);
	if($pergs_avaliacoes[0]) {
		foreach($pergs_avaliacoes as $pergs) {
			$problemasensinoaprendizagem[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasensinoaprendizagem[$pergs['repid']]['codigo']) {
				$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasensinoaprendizagem[$pergs['repid']]['descricao']."', 'diagnostico_3_ensinoeaprendizagem') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE rp.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'E' and prgsubmodulo = 'T' and prgstatus = 'A' and prgdetalhe='Tempo de Aprendizagem') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_tempoaprendizagem = $db->carregar($sql);
	if($pergs_tempoaprendizagem[0]) {
		foreach($pergs_tempoaprendizagem as $pergs) {
			$problemasensinoaprendizagem[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasensinoaprendizagem[$pergs['repid']]['codigo']) {
				$problemasensinoaprendizagem[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasensinoaprendizagem[$pergs['repid']]['descricao']."', 'diagnostico_3_ensinoeaprendizagem') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostatempoaprendizagem WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND rtaporquecritico=TRUE";
	$respostatempoaprendizagem = $db->pegaLinha($sql);
	
	$_PORQUE = array("espacofisico" => "N�o possui espa�o f�sico",
					 "profissionais" => "N�o dispoe de profissionais para coordenar as atividades",
					 "recursosmateriais" => "N�o dispoe de recursos materiais",
					 "outro" => "Outro");
	
	if($respostatempoaprendizagem['rtacaso']=="N") {
		$rtaporque = explode(";",$respostatempoaprendizagem['rtaporque']);
		if($rtaporque) {
			
			foreach($rtaporque as $pq) {
				$probl[] = $_PORQUE[$pq];	
			}
			$problemasensinoaprendizagem['rtaporque']['descricao'] = "A escola n�o desenvolve a��es de educa��o integral porque: ".implode(", ",$probl);
			$problemasensinoaprendizagem['rtaporque']['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='rtaporque' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasensinoaprendizagem['rtaporque']['codigo']) {
				$problemasensinoaprendizagem['rtaporque']['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', 'rtaporque', 'A', '".$problemasensinoaprendizagem['rtaporque']['descricao']."', 'diagnostico_3_ensinoeaprendizagem') RETURNING papid;");
				$db->commit();				
			}
		}
	}
	
	return $problemasensinoaprendizagem;
	
}


function listaproblemas_diagnostico_4_gestao($dados) {
	global $db;
	$sql = "SELECT tp.tpeid, tp.tpedesc, count(p.pesid) as qtd FROM pdeinterativo2013.pessoa p 
			INNER JOIN pdeinterativo2013.pessoatipoperfil ptp ON ptp.pesid = p.pesid 
			LEFT JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = ptp.pdeid 
			LEFT JOIN pdeinterativo2013.detalhepessoa dp ON dp.pesid = p.pesid 
			LEFT JOIN pdeinterativo2013.tipoperfil tp ON tp.tpeid = ptp.tpeid 
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND dp.tenid IN(2,3) AND ptp.tpeid IN(SELECT tpeid FROM pdeinterativo2013.perfilarea WHERE apeid IN('".APE_EQUIPEPEDAGOGICA."','".APE_DIRETOR."','".APE_VICEDIRETOR."','".APE_SECRETARIA."')) 
			AND p.critico=true
			GROUP BY tp.tpeid, tp.tpedesc";
	
	$pessoas = $db->carregar($sql);
	
	if($pessoas[0]) {
		foreach($pessoas as $pes) {
			$sql = "SELECT p.pesid FROM pdeinterativo2013.pessoa p 
					INNER JOIN pdeinterativo2013.pessoatipoperfil ptp ON ptp.pesid = p.pesid
					LEFT JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = ptp.pdeid 
					LEFT JOIN pdeinterativo2013.detalhepessoa dp ON dp.pesid = p.pesid 
					WHERE p.critico=true AND pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND dp.tenid IN(2,3) AND ptp.tpeid='".$pes['tpeid']."'";
			
			$p = $db->carregar($sql);
			unset($pesids);
			if($p[0]) {
				foreach($p as $po) {
					$pesids[] = $po['pesid'];
				}
			}
			$problemasgestao['gestao_'.implode(",",$pesids)]['descricao'] = "H� ".$pes['qtd']." ".$pes['tpedesc']." que n�o possui(em) gradua��o.";
			$problemasgestao['gestao_'.implode(",",$pesids)]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='gestao_".implode(",",$pesids)."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao['gestao_'.implode(",",$pesids)]['codigo']) {
				$problemasgestao['gestao_'.implode(",",$pesids)]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', 'gestao_".implode(",",$pesids)."', 'A', '".$problemasgestao['gestao_'.implode(",",$pesids)]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'D' and prgstatus = 'A' and prgdetalhe='Lideran�a') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_lideranca = $db->carregar($sql);
	if($pergs_lideranca[0]) {
		foreach($pergs_lideranca as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) {
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'D' and prgstatus = 'A' and prgdetalhe='Acompanhamento') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_acompanhamento = $db->carregar($sql);
	if($pergs_acompanhamento[0]) {
		foreach($pergs_acompanhamento as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) {
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Planejamento') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_planejamento = $db->carregar($sql);
	
	if($pergs_planejamento[0]) {
		foreach($pergs_planejamento as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) {
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Rotinas') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_rotinas = $db->carregar($sql);
	
	if($pergs_rotinas[0]) {
		foreach($pergs_rotinas as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) {
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Normas e Regulamentos') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_normas = $db->carregar($sql);
	
	if($pergs_normas[0]) {
		foreach($pergs_normas as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) { 
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'F' and prgstatus = 'A' and prgdetalhe='Gest�o Financeira') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_financeiras = $db->carregar($sql);
	
	if($pergs_financeiras[0]) {
		foreach($pergs_financeiras as $pergs) {
			$problemasgestao[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemasgestao[$pergs['repid']]['codigo']) {
				$problemasgestao[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemasgestao[$pergs['repid']]['descricao']."', 'diagnostico_4_gestao') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	return $problemasgestao;
}

function listametas_diagnostico_4_gestao($dados) {
	global $db;
	
	$sql = "SELECT rp.repid, pe.prgdesc, LOWER(op.oppdesc) as oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'D' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_direcao = $db->carregar($sql);
	if($existe_direcao[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '|| CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">')
																	 WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")."
				WHERE mt.metmodulo = 'G' and mt.metsubmodulo = 'D' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid  
				ORDER BY mt.metid";
		
		$metas_direcao = $db->carregar($sql);
		if($metas_direcao[0]) {
			foreach($metas_direcao as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'P' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_processos = $db->carregar($sql);
	if($existe_processos[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '|| CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
																	 WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")."		 
				WHERE mt.metmodulo = 'G' and mt.metsubmodulo = 'P' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
		$metas_processos = $db->carregar($sql);
		if($metas_processos[0]) {
			foreach($metas_processos as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'G' and prgsubmodulo = 'F' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_financas = $db->carregar($sql);
	
	if($existe_financas[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '|| CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
																	 WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")."
				WHERE mt.metmodulo = 'G' and mt.metsubmodulo = 'F' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
		$metas_financas = $db->carregar($sql);
		if($metas_financas[0]) {
			foreach($metas_financas as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	return $metas;
}


function listaproblemas_diagnostico_5_comunidadeescolar($dados) {
	global $db;
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'E' and prgstatus = 'A' and prgdetalhe='Compromisso') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_compromisso = $db->carregar($sql);
	if($pergs_compromisso[0]) {
		foreach($pergs_compromisso as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'E' and prgstatus = 'A' and prgdetalhe='Protagonismo e Participa��o') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_protagonismo = $db->carregar($sql);
	if($pergs_protagonismo[0]) {
		foreach($pergs_protagonismo as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace("(*)", $pergs['oppdesc'], $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'E' and prgstatus = 'A' and prgdetalhe='Sa�de e Bem-estar') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_saude = $db->carregar($sql);
	if($pergs_saude[0]) {
		foreach($pergs_saude as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'D' and prgstatus = 'A' and prgdetalhe='Pr�ticas') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_praticas = $db->carregar($sql);
	
	if($pergs_praticas[0]) {
		foreach($pergs_praticas as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'D' and prgstatus = 'A' and prgdetalhe='Experiencia e Auto-Confian�a') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_experiencia = $db->carregar($sql);
	
	if($pergs_experiencia[0]) {
		foreach($pergs_experiencia as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Coopera��o e Respeito') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_cooperacao = $db->carregar($sql);
	if($pergs_cooperacao[0]) {
		foreach($pergs_cooperacao as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'P' and prgstatus = 'A' and prgdetalhe='Motiva��o') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_motivacao = $db->carregar($sql);
	
	if($pergs_motivacao[0]) {
		foreach($pergs_motivacao as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'C' and prgstatus = 'A' and prgdetalhe='Comunica��o') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_comunicacao = $db->carregar($sql);
	
	if($pergs_comunicacao[0]) {
		foreach($pergs_comunicacao as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'C' and prgstatus = 'A' and prgdetalhe='Participa��o') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$pergs_participacao = $db->carregar($sql);
	
	if($pergs_participacao[0]) {
		foreach($pergs_participacao as $pergs) {
			$problemascomunidadeescolar[$pergs['repid']]['descricao'] = str_replace(array("(*)","?"), array($pergs['oppdesc'],"."), $pergs['prgdesc']);
			$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$pergs['repid']."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar[$pergs['repid']]['codigo']) {
				$problemascomunidadeescolar[$pergs['repid']]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$pergs['repid']."', 'A', '".$problemascomunidadeescolar[$pergs['repid']]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	$sql = "SELECT rd.rdoid FROM pdeinterativo2013.respostadocente rd 
			INNER JOIN pdeinterativo2013.respostadocenteformacao rf ON rf.rdoid = rd.rdoid 
			INNER JOIN educacenso_2010.tab_docente td ON td.pk_cod_docente = rd.pk_cod_docente 
			WHERE rd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND rd.rdocritico=TRUE AND rd.rdostatus='A'
			GROUP BY rd.rdoid";
	
	$docentesformacao = $db->carregarColuna($sql);
	
	if($docentesformacao) {
		$problemascomunidadeescolar['docentesformacao_'.implode(",",$docentesformacao)]['descricao'] = count($docentesformacao)." docentes consideram que sua forma��o n�o � apropriada para ministrar a �rea de conhecimento/ disciplina que atuam.";
		$problemascomunidadeescolar['docentesformacao_'.implode(",",$docentesformacao)]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='docentesformacao_".implode(",",$docentesformacao)."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		if(!$problemascomunidadeescolar['docentesformacao_'.implode(",",$docentesformacao)]['codigo']) {
			$problemascomunidadeescolar['docentesformacao_'.implode(",",$docentesformacao)]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', 'docentesformacao_".implode(",",$docentesformacao)."', 'A', '".$problemascomunidadeescolar['docentesformacao_'.implode(",",$docentesformacao)]['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
			$db->commit();
		}
		
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.respostapaiscomunidade WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$respostapaiscomunidade = $db->pegaLinha($sql);
	
	if($respostapaiscomunidade) {
		if($respostapaiscomunidade['rpcpossuiconcelhocritico']=="t" && $respostapaiscomunidade['rpcpossuiconcelho']=="f") {
			$problemascomunidadeescolar['rpcpossuiconcelho']['descricao'] = "A escola n�o possui Conselho Escolar";
			$problemascomunidadeescolar['rpcpossuiconcelho']['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='rpcpossuiconcelho' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$problemascomunidadeescolar['rpcpossuiconcelho']['codigo']) {
				$problemascomunidadeescolar['rpcpossuiconcelho']['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', 'rpcpossuiconcelho', 'A', '".$problemascomunidadeescolar['rpcpossuiconcelho']['descricao']."', 'diagnostico_5_comunidadeescolar') RETURNING papid;");
				$db->commit();
			}
		}
	}
	
	return $problemascomunidadeescolar;
}

function listametas_diagnostico_5_comunidadeescolar($dados) {
	global $db;
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'E' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_estudantes = $db->carregar($sql);
	if($existe_estudantes[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '|| CASE WHEN mettipo='I' THEN REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') 
																	 WHEN mettipo='C' THEN REPLACE(metdesc,'K','<input type=checkbox  onclick=salvarFormularioParcialmente(); value=\"TRUE\" name=metas2['||mt.metid||'] '||COALESCE((SELECT CASE WHEN rmecheckbox=TRUE THEN 'checked' ELSE '' END FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'')||' >') END as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'C' and mt.metsubmodulo = 'E' and mt.metstatus='A' 
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
		
		$metas_estudantes = $db->carregar($sql);
		if($metas_estudantes[0]) {
			foreach($metas_estudantes as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}

	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'D' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_docentes = $db->carregar($sql);
	
	if($existe_docentes[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'C' and mt.metsubmodulo = 'D' and mt.metstatus='A'  
				GROUP BY mt.mettipo, mt.metdesc, mt.metid
				ORDER BY mt.metid";
		
		$metas_docentes = $db->carregar($sql);
		if($metas_docentes[0]) {
			foreach($metas_docentes as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	$sql = "SELECT rp.repid, pe.prgdesc, op.oppdesc FROM pdeinterativo2013.respostapergunta rp 
			INNER JOIN pdeinterativo2013.pdinterativo pd ON pd.pdeid = rp.pdeid 
			INNER JOIN pdeinterativo2013.pergunta pe ON pe.prgid = rp.prgid 
			INNER JOIN pdeinterativo2013.opcaopergunta op ON op.oppid = rp.oppid
			WHERE pd.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
				  rp.prgid IN(select prgid from pdeinterativo2013.pergunta where prgmodulo = 'C' and prgsubmodulo = 'P' and prgstatus = 'A') AND
				  rp.oppid IN(5,6) AND rp.critico=true ORDER BY rp.repid";
	
	$existe_paiscomunidade = $db->carregar($sql);
	
	if($existe_paiscomunidade[0]) {
		$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'C' and mt.metsubmodulo = 'P' and mt.metstatus='A'  
				GROUP BY mt.mettipo, mt.metdesc, mt.metid
				ORDER BY mt.metid";
		
		$metas_paiscomunidade = $db->carregar($sql);
		if($metas_paiscomunidade[0]) {
			foreach($metas_paiscomunidade as $meta) {
				$metas[] = $meta['metdesc'];
			}
		}
	}
	
	return $metas;
}



function listaproblemas_diagnostico_6_infraestrutura($dados) {
	global $db;
	?>
	 <?php $arrInstalacoesNecessarias = recuperaInstalacoesNecessarias(); ?>
	 <?php $arrInstalacoesInadequadas = recuperaInstalacoesInadequadas(); ?>
	 <?php $arrEquipamentosRuins = recuperaEquipamentosRuins() ?>
	 <?php $arrRespEscola = false; ?>
	 <?php $arrRespEscola = recuperaRespostasEscola(null,"I","I",null,array("(op.oppdesc ilike 'Nunca' or op.oppid = ".OPP_RARAMENTE.")")); ?>
	 <?php if($arrInstalacoesNecessarias): ?>
		<?php foreach($arrInstalacoesNecessarias as $infra): ?>
			<?php if( $infra['rifcritico'] == "t" ): ?>
				<?php $arrProblemas['descricao'][] = "A escola n�o possui ".$infra['ifidesc']."." ?>
				<?php $arrProblemas['codigo'][] = "naopossui_".$infra['ifiid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if($arrInstalacoesInadequadas): ?>
		<?php foreach($arrInstalacoesInadequadas as $infra): ?>
			<?php if( $infra['rifcritico'] == "t" ): ?>
				<?php $arrProblemas['descricao'][] = "A escola possui ". $infra['rifqtdinadequado'] ." ". $infra['ifidesc'] ." inadequados(as)." ?>
				<?php $arrProblemas['codigo'][] = "naopossui_".$infra['ifiid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if($arrEquipamentosRuins): ?>
		<?php foreach($arrEquipamentosRuins as $equip): ?>
			<?php if( $equip['remcritico'] == "t" ): ?>
				<?php $arrProblemas['descricao'][] = "A escola avalia que o estado de conserva��o de ".number_format($equip['rmeqtdruin'],"",2,".")." ".$equip['tmedesc']." � ruim." ?>
				<?php $arrProblemas['codigo'][] = "equipamento_".$equip['tmeid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	 <?php if($arrRespEscola): ?>
		<?php foreach($arrRespEscola as $resp): ?>
			<?php if( verificaCheckBoxPergunta($resp['repid']) ): ?>
				<?php $arrProblemas['descricao'][] = str_replace( array("(*)","?") , array(strtolower($resp['oppdesc']),".") ,$resp['prgdesc']) ?>
				<?php $arrProblemas['codigo'][] = $resp['repid'] ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif;
	
	if($arrProblemas['descricao']){
		foreach($arrProblemas['descricao'] as $chave => $prob){
			$arrProblemasInfra[$chave]['descricao'] = $prob;
			$arrProblemasInfra[$chave]['codigo'] = $db->pegaUm("SELECT papid FROM pdeinterativo2013.planoacaoproblema WHERE papidentificador='".$arrProblemas['codigo'][$chave]."' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
			if(!$arrProblemasInfra[$chave]['codigo']){
				$arrProblemasInfra[$chave]['codigo'] = $db->pegaUm("INSERT INTO pdeinterativo2013.planoacaoproblema(pdeid, papidentificador, papstatus, papdescricao, abacod) VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$arrProblemas['codigo'][$chave]."', 'A', '".$prob."', 'diagnostico_6_infraestrutura') RETURNING papid;");
				$db->commit();
			}
		}	
	}
	return $arrProblemasInfra;
	
}

function listametas_diagnostico_6_infraestrutura($dados) {
	global $db;
	
	$arrInstalacoesNecessarias = recuperaInstalacoesNecessarias();
	if($arrInstalacoesNecessarias){
		foreach($arrInstalacoesNecessarias as $infra){
			if($infra['rifcritico'] == "t"){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'I' and mt.metsubmodulo = 'I' and mt.metstatus='A'  
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
				$metas_instalacoes = $db->carregar($sql);
				break;
			}
		}
	}else{
		$arrInstalacoesInadequadas = recuperaInstalacoesInadequadas();
		if($arrInstalacoesInadequadas){
			foreach($arrInstalacoesInadequadas as $infra){
				if($infra['rifcritico'] == "t"){
					$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
					".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
					WHERE mt.metmodulo = 'I' and mt.metsubmodulo = 'I' and mt.metstatus='A'  
					GROUP BY mt.mettipo, mt.metdesc, mt.metid 
					ORDER BY mt.metid";
					$metas_instalacoes = $db->carregar($sql);
					break;
				}
			}
		}
	}
	
	$arrEquipamentosRuins = recuperaEquipamentosRuins();
	if($arrEquipamentosRuins){
		foreach($arrEquipamentosRuins as $equip){
			if($equip['remcritico'] == "t"){
				$sql = "SELECT '<img src=../imagens/seta_filho.gif> '||REPLACE(metdesc,'X','<input type=text onBlur=salvarFormularioParcialmente(); class=normal size=8 maxlength=8 value=\"'|| COALESCE((SELECT rmetaxa::varchar FROM pdeinterativo2013.respostameta rm WHERE rm.metid=mt.metid AND rm.pdeid=".$_SESSION['pdeinterativo2013_vars']['pdeid']." LIMIT 1),'') ||'\" name=metas['||mt.metid||'] onKeyUp=\"this.value=mascaraglobal(\'########\',this.value);\">') as metdesc FROM pdeinterativo2013.meta mt 
				".(($dados['somente_resposta'])?"INNER JOIN pdeinterativo2013.respostameta rm ON rm.metid=mt.metid AND (rm.rmetaxa IS NOT NULL OR rm.rmecheckbox=TRUE)":"")." 
				WHERE mt.metmodulo = 'I' and mt.metsubmodulo = 'E' and mt.metstatus='A'  
				GROUP BY mt.mettipo, mt.metdesc, mt.metid 
				ORDER BY mt.metid";
				$metas_equipamentos = $db->carregar($sql);
				break;
			}
		}
	}
	
	if($metas_instalacoes[0]) {
		foreach($metas_instalacoes as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	
	if($metas_equipamentos[0]) {
		foreach($metas_equipamentos as $meta) {
			$metas[] = $meta['metdesc'];
		}
	}
	
	return $metas? $metas : false;
}


function visualizarAnalises($dados) {
	global $db;
	echo '<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>';
	echo '<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>';
	
	$sql = "SELECT cac.cacdesc, (SUM(COALESCE(pab.pabvalorcapital,0))+SUM(COALESCE(pab.pabvalorcusteiro,0))) as to, SUM(pab.pabvalorcapital) as ca, SUM(pab.pabvalorcusteiro) as cu 
				FROM pdeinterativo2013.planoacaoproblema pap 
				INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
				INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
				INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
				LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
				LEFT JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
				LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
				WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND ((pabfonte='P' AND pabparcela IN('S','P'))) AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' 
				GROUP BY cac.cacdesc";
	
	$cabecalho = array("Categoria","Valor Total","Capital","Custeio");
	$db->monta_lista_simples($sql,$cabecalho,50,5,'S','100%',$par2);
	
	
	
}


function gerenciarAnalises($dados) {
	global $db;
	?>
	<html>
	<head>
	<script language="JavaScript" src="../includes/funcoes.js"></script>
	<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
	<script>
	function controlarIcone(obj, id) {
		if(obj.title=='menos') {
			obj.title='mais';
			obj.src='../imagens/mais.gif';
			document.getElementById(id).style.display='none';
		} else {
			obj.title='menos';
			obj.src='../imagens/menos.gif';
			document.getElementById(id).style.display='';
		}
	}
	</script>
	</head>
	<body>
	<form method="post" id="formulario2">
	<input type="hidden" name="requisicao" value="planoestrategico_0_3_visualizarplanoacao">
	<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
	<tr>
		<td class="SubTituloCentro">Analise de Pre�os e Categorias</td>
	</tr>
	<tr>
		<td>
		<p><b><img src="../imagens/menos.gif" style="cursor:pointer;" onclick="controlarIcone(this,'div_u_sup');" title="menos"> Itens com valores unit�rios superiores ao par�metro do MEC</b></p>
		<div id="div_u_sup">
		<?
		
		$sql = "SELECT cac.cacdesc, cia.ciadesc, ure.uredesc, pab.pabqtd, pab.pabvalor, pab.pabvalorcapital, pab.pabvalorcusteiro,
					   CASE WHEN pabfonte='P' THEN 'PDDE/PDE Escola' ELSE 'Outras' END,
  					   CASE WHEN pabparcela='P' THEN '1� Parcela (2011)' ELSE '2� Parcela (2012)' END as pabfonte,
		 			   ROUND(cia.ciaprecomaximo,2) as ciaprecomaximo, ROUND((((pab.pabvalor/cia.ciaprecomaximo)*100)-100),0) as porcent, '<input type=hidden name=planoacaobemservico[mec]['||pab.pabid||'] value=\"FALSE\"><input type=checkbox name=planoacaobemservico[mec]['||pab.pabid||'] value=\"TRUE\" '||CASE WHEN pabmecanalise=TRUE THEN 'checked' ELSE '' END||'>' as f 
				FROM pdeinterativo2013.planoacaoproblema pap 
				INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
				INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
				INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
				LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
				LEFT JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
				LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
				WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND pab.pabvalor > cia.ciaprecomaximo";
		
		$cabecalho = array("Categoria","Itens/Servi�os","Unidade de refer�ncia",
						   "Quantidade","Valor Unit�rio(R$)","Capital(R$)","Custeio(R$)",
						   "Fonte","Parcela/Ano","Valor MEC","%","");
		$db->monta_lista_simples($sql,$cabecalho,50,5,'N','100%',$par2);
		
		?>
		</div>
		<p><b><img src="../imagens/menos.gif" style="cursor:pointer;" onclick="controlarIcone(this,'div_u_inf');"> Itens com valores unit�rios inferiores ao par�metro do MEC</b></p>
		<div id="div_u_inf">
		<?
		
		$sql = "SELECT cac.cacdesc, cia.ciadesc, ure.uredesc, pab.pabqtd, pab.pabvalor, pab.pabvalorcapital, pab.pabvalorcusteiro,
					   CASE WHEN pabfonte='P' THEN 'PDDE/PDE Escola' ELSE 'Outras' END,
  					   CASE WHEN pabparcela='P' THEN '1� Parcela (2011)' ELSE '2� Parcela (2012)' END as pabfonte,
		 			   ROUND(cia.ciaprecominimo,2) as ciaprecominimo, CASE WHEN pab.pabvalor > 0 THEN ROUND((((cia.ciaprecominimo/pab.pabvalor)*100)-100),0)::text ELSE '0' END as porcent, '<input type=hidden name=planoacaobemservico[mec]['||pab.pabid||'] value=\"FALSE\"> <input type=checkbox name=planoacaobemservico[mec]['||pab.pabid||'] value=\"TRUE\" '||CASE WHEN pabmecanalise=TRUE THEN 'checked' ELSE '' END||'>' as f 
				FROM pdeinterativo2013.planoacaoproblema pap 
				INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
				INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
				INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
				LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
				LEFT JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
				LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
				WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND pab.pabvalor < cia.ciaprecominimo";
		
		$cabecalho = array("Categoria","Itens/Servi�os","Unidade de refer�ncia",
						   "Quantidade","Valor Unit�rio(R$)","Capital(R$)","Custeio(R$)",
						   "Fonte","Parcela/Ano","Valor MEC","%","");
		$db->monta_lista_simples($sql,$cabecalho,50,5,'N','100%',$par2);
		
		?>
		</div>
		<p><b><img src="../imagens/menos.gif" style="cursor:pointer;" onclick="controlarIcone(this,'div_u_inf');"> Categoria com valores superiores ao par�metro do MEC</b></p>
		<div id="div_c_sup">
		<?
		
		$total = $db->pegaUm("SELECT SUM(pab.pabvalor) FROM pdeinterativo2013.planoacaobemservico pab 
					 		  INNER JOIN pdeinterativo2013.planoacaoacao paa ON pab.paaid = paa.paaid 
					 		  INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON paa.paeid = pae.paeid 
					 		  INNER JOIN pdeinterativo2013.planoacaoproblema pap ON pae.papid = pap.papid 
					 		  INNER JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid
							  INNER JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
							  INNER JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
					 		  WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
		$sql = array();
		if($total) {
			$sql = "SELECT cac.cacdesc, SUM(pab.pabvalor) as pabvalor, ROUND((SUM(pab.pabvalor)/".$total.")*100,2), cac.cacpercentual, '<input type=checkbox name=categoriaacaoanalise[] value='||cac.cacid||' '|| CASE WHEN caa.caaid IS NOT NULL THEN 'checked' ELSE '' END ||'>' as f 
					FROM pdeinterativo2013.planoacaoproblema pap 
					INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
					INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
					INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
					INNER JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
					INNER JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
					INNER JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid 
					LEFT JOIN pdeinterativo2013.categoriaanalise caa ON caa.cacid = cac.cacid AND caa.pdeid = pap.pdeid 
					WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'
					GROUP BY cac.cacid, cac.cacdesc, cac.cacpercentual, caa.caaid 
					HAVING ROUND((SUM(pab.pabvalor)/".$total.")*100,2) > cac.cacpercentual";
		}
		
		$cabecalho = array("Categoria","Total(R$)","Percentual Atingido", "% MEC","");
		$db->monta_lista_simples($sql,$cabecalho,50,5,'N','100%',$par2);
		
		?>
		</div>


		</td>
	</tr>
	<tr>
	<td class="SubTituloCentro"><input type="submit" value="Salvar"> <input type="button" value="Fechar" onclick="window.close();"></td>
	</tr>
	</table>
	</form>
	</body>
	</html>
	<?
}


function planoestrategico_0_3_visualizarplanoacao($dados) {
	global $db;
	
	apagarCachePdeInterativo();
	
	if($dados['planoacaobemservico']) {
		foreach($dados['planoacaobemservico'] as $tipo => $arr) {
			foreach($arr as $pabid => $bollean) {
				
				$sql = "UPDATE pdeinterativo2013.planoacaobemservico
					    SET pab".$tipo."analise=".$bollean."
						WHERE pabid='".$pabid."'";
				
				$db->executar($sql);
				$db->commit();
				
			}
		}
	}
	
	
	$db->executar("DELETE FROM pdeinterativo2013.categoriaanalise WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
	
	if($dados['categoriaacaoanalise']) {
		foreach($dados['categoriaacaoanalise'] as $cacid) {
			$db->executar("INSERT INTO pdeinterativo2013.categoriaanalise(pdeid, cacid)
    					   VALUES ('".$_SESSION['pdeinterativo2013_vars']['pdeid']."', '".$cacid."');");
			$db->commit();
		}
	}
	

		
	echo "<script>
			alert('Dados gravados com sucesso');
			".(($dados['togo'])?"window.location='".$dados['togo']."';":"window.close();")."
		  </script>";
}

function condicaoComite() {
	global $db;

	if($_SESSION['pdeinterativo2013_vars']['pditempdeescola']=="f") {
		return "Escolas Sem PDE Interativo n�o podem tramitar o plano";
	}
	
	if(!$_SESSION['pdeinterativo2013_vars']['pdeid']) {
		return "Ocorreu um erro: N�o foi poss�vel identificar a escola, clique na Tela Inicial e recom�e a navega��o.";
	}
	
	$num = $db->pegaUm("SELECT COUNT(abrid) as num FROM pdeinterativo2013.abaresposta WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'");
	
	if($num < 32) $erro .= "Progresso de Preenchimento do PDE n�o esta 100%;".'\n';
	
	$sql = "select count(*) as num from pdeinterativo2013.aba a 
			inner join pdeinterativo2013.abaresposta b on b.abaid = a.abaid 
			where b.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' and a.abacod='planoestrategico_0_2_planoacao'";
	
	$num = $db->pegaUm($sql);
	
	if($num==0) {
		$erro .= "- Possivelmente o plano foi atualizado, clique na aba 1.2 Planos de a��o e clique novamente no bot�o Salvar;".'\n';
	}
	
	$sql = "SELECT * FROM pdeinterativo2013.cargacapitalcusteio WHERE codinep='".$_SESSION['pdeinterativo2013_vars']['pdicodinep']."' AND cccstatus='A'";
	$cargacapitalcusteio = $db->pegaLinha($sql);
	$problema = false;
	
	$_parcelas = array('P' => 'primeira',
					   'S' => 'segunda');

	foreach($_parcelas as $parcela => $descricao) {
		
		$outros_pabs = $db->pegaLinha("SELECT COALESCE(SUM(pabvalorcapital),0) as pabvalorcapital, COALESCE(SUM(pabvalorcusteiro),0) as pabvalorcusteiro 
								  	   FROM pdeinterativo2013.planoacaobemservico p 
								  	   INNER JOIN pdeinterativo2013.planoacaoacao po ON po.paaid = p.paaid 
								  	   INNER JOIN pdeinterativo2013.planoacaoestrategia pe ON pe.paeid = po.paeid 
								  	   INNER JOIN pdeinterativo2013.planoacaoproblema pp ON pp.papid = pe.papid  
								  	   WHERE pabparcela='".$parcela."' AND 
	   										 pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND 
								  	   		 pabstatus='A' AND papstatus='A' AND paestatus='A' AND paastatus='A'");

		
		if($outros_pabs['pabvalorcapital'] != $cargacapitalcusteio['ccccapital'.$descricao])
			$financas[]="- ".ucfirst($descricao)." parcela de capital ainda possui recursos;".'\n';
		if($outros_pabs['pabvalorcusteiro'] != $cargacapitalcusteio['ccccusteio'.$descricao])
			$financas[]="- ".ucfirst($descricao)." parcela de custeio ainda possui recursos;".'\n';
			
		
	}
	
	if($financas) {
		$erro .= "Ainda h� recursos dispon�veis a serem utilizados:".'\n'.implode("",$financas);
	}
	
	if($erro) {
		return $erro;
	} else {
		return true;
	}
	
}

function condicaoMEC() {
	global $db;
	return true;
	/*$sql = "SELECT COUNT(prcid) FROM pdeinterativo2013.parecer WHERE pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' ANd prcstatus='A' AND prcaprovado=TRUE";
	$pareceres = $db->pegaUm($sql);
	$sql = "SELECT COUNT(pabid) FROM pdeinterativo2013.planoacaoproblema pr 
			LEFT JOIN pdeinterativo2013.planoacaoestrategia pe ON pe.papid = pr.papid 
			LEFT JOIN pdeinterativo2013.planoacaoacao pa ON pa.paeid = pe.paeid 
			LEFT JOIN pdeinterativo2013.planoacaobemservico ps ON ps.paaid = pa.paaid 
			WHERE pabcomiteanalise=FALSE AND papstatus='A' AND pabstatus='A' AND paestatus='A' AND paastatus='A' AND pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."'";
	$acoesnaomarcadas = $db->pegaUm($sql);
	if($pareceres==6 && $acoesnaomarcadas==0) {
		return true;
	} else {
		return false;
	}*/
}

function condicaoValidado() {
	global $db;
	
	if($_SESSION['pdeinterativo2013_vars']['pditempdeescola']=="f") {
		return "Escolas Sem PDE Interativo n�o podem tramitar o plano";
	}
	
	$sql = "SELECT pabmecanalise 
			FROM pdeinterativo2013.planoacaoproblema pap 
			INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
			INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
			INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
			LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
			LEFT JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
			LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
			WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND pab.pabvalor > cia.ciaprecomaximo";
	
	$mecanalisemaximo = $db->carregarColuna($sql);
			
	$sql = "SELECT pabmecanalise 
			FROM pdeinterativo2013.planoacaoproblema pap 
			INNER JOIN pdeinterativo2013.planoacaoestrategia pae ON pae.papid = pap.papid 
			INNER JOIN pdeinterativo2013.planoacaoacao paa ON paa.paeid = pae.paeid 
			INNER JOIN pdeinterativo2013.planoacaobemservico pab ON pab.paaid = paa.paaid 
			LEFT JOIN pdeinterativo2013.categoriaitemacao cia ON cia.ciaid = pab.ciaid 
			LEFT JOIN pdeinterativo2013.unidadereferencia ure ON ure.ureid = cia.ureid 
			LEFT JOIN pdeinterativo2013.categoriaacao cac ON cac.cacid = cia.cacid
			WHERE pab.pabstatus='A' AND paa.paastatus='A' AND pae.paestatus='A' AND pap.papstatus='A' AND pap.pdeid='".$_SESSION['pdeinterativo2013_vars']['pdeid']."' AND pab.pabvalor < cia.ciaprecominimo";
	
	$mecanaliseminimo = $db->carregarColuna($sql);

	$ima = false;
	if($mecanalisemaximo) {
		foreach($mecanalisemaximo as $ia) {
			if($ia != "t") $ima=true;
		}
	}
	
	if($ima) $erro.="Itens com valores unit�rios superiores ao par�metro do MEC n�o foram validados;".'\n';
	
	$imo = false;
	if($mecanaliseminimo) {
		foreach($mecanaliseminimo as $io) {
			if($io != "t") $imo=true;
		}
	}
	
	if($imo) $erro.="Itens com valores unit�rios inferiores ao par�metro do MEC n�o foram validados;".'\n';
	
	if($erro) return $erro;
	else return true;
	

}

function validarPdeInterativo()
{
	global $db;
	/* configura��es do relatorio - Memoria limite de 1024 Mbytes */
	ini_set("memory_limit", "1024M");
	set_time_limit(0);
	/* FIM configura��es - Memoria limite de 1024 Mbytes */
	
	//include_once("pdeWs.php");
	//$ws = new pdeWs();
	
	//$entcodent = $db->pegaUm("select pdicodinep from pdeinterativo2013.pdinterativo where entid = ".$_SESSION['entid']);
	if($_SESSION["pdeinterativo2013_vars"]["pdicodinep"]) {
		$entcodent = $_SESSION["pdeinterativo2013_vars"]["pdicodinep"];
		
		$coProgramaFNDE = 96;
		$anoAtual = date('Y');
	
		//$teste = $ws->pdeEscolaWs('atualizaAnaliseEscola', $anoAtual, $entcodent, $coProgramaFNDE);
		$teste = false; //Remover para funcionar o Web Service
		
		if( $teste && $teste != "errowebservice" )
		{
			$db->executar("UPDATE pdeinterativo2013.pdinterativo SET pdiretornofnde='t' WHERE pdistatus = 'A' and pdicodinep = '{$entcodent}'");
			$db->commit();
			return true;
		}
		else
		{
			$db->executar("UPDATE pdeinterativo2013.pdinterativo SET pdiretornofnde='f' WHERE pdistatus = 'A' and pdicodinep = '{$entcodent}'");
			$db->commit();
			return false;
		}
		return true;
	} else {
		return false;
	}
}

?>