<?php

define("LARGURA_ETIQUETA_SCA", 190);
define("ALTURA_ETIQUETA_SCA", 130);
define("SCA_PERIODO_RELMOVVISITANTE", 31);

define("SCA_EXPEDIENTE_NORMAL", 0);
define("SCA_EXPEDIENTE_FORA_HORARIO", 1);
define("SCA_EXPEDIENTE_FINAL_SEMANA", 2);
define("SCA_EXPEDIENTE_FERIADO", 3);

// constantes de entrada (local de trabalho da recepcionista)
define('ENTRADA_EDIFICIO_SEDE_PRINCIPAL', 1);
define('ENTRADA_EDIFICIO_SEDE_PRIVATIVA', 2);
define('ENTRADA_ANEXO_1', 3);

?>