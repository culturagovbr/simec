<?php 

define( "EMI_PERFIL_SUPERUSER", 	342 );
define( "EMI_PERFIL_CONSULTA", 		345 );
define( "EMI_PERFIL_ADMINISTRADOR", 346 );
define( "EMI_PERFIL_ANALISTACOEM",  379 );
define( "EMI_PERFIL_ANALISTASEDUC",  668 );
define( "EMI_PERFIL_APROVADOR",     380 );
define( "EMI_PERFIL_CADASTRADOR",   381 );

define( "EMI_ACAO_ESTADO_DOC",   249 );

define( "EMI_FUNID_COORDENADOR", 60 );
define( "EMI_FUNID_ESCOLA", 	 3  );

define( "EMI_TIPO_ENTIDADE_ESCOLA", 1 );
define( "EMI_TIPO_ENTIDADE_SEC", 	2 );

define( "EMI_TIPO_DOCUMENTO", 10 );

// Estados dos documentos (workflow)
define( "EMI_NAO_INICIADO", 		62 );
define( "EMI_EM_PRENCHIMENTO", 		63 );
define( "EMI_EM_ANALISE",			64 );
define( "EMI_EM_APROVACAO", 		65 );
define( "EMI_APROVADO", 			66 );
define( "EMI_CORRECAO_APROVADOS", 	168 );
define( "EMI_GERACAO_PTA", 			169 );

?>