<?php
//Carrega parametros iniciais do simec
include_once "controleInicio.inc";

// carrega as fun��es gerais
include_once APPRAIZ . "includes/classes/Modelo.class.inc";
include_once APPRAIZ . 'includes/workflow.php';

// carrega as fun��es do m�dulo
include_once '_constantes.php';
include_once '_funcoes.php';
include_once '_componentes.php';
include_once 'autoload.php';

//Carrega as fun��es de controle de acesso
include_once "controleAcesso.inc";
?>