

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title></title>
		<meta name="generator content="StarOffice/OpenOffice.org XSLT (http://xml.openoffice.org/sx2ml)" />
		<meta name="author" content="Neuza" />
		<meta name="created" content="2008-01-31T17:54:00" />
		<meta name="changedby" content="mec" />
		<meta name="changed" content="2008-02-07T18:09:00" />
		<style type="text/css">
			@page { size: 8.5inch 11inch; margin-top: 0.5inch; margin-bottom: 0.7874inch; margin-left: 1.1811inch; margin-right: 0.3291inch }
			table { border-collapse: collapse; border-spacing: 0; empty-cells: show }
			td,th { vertical-align: top; }
			h1,h2,h3,h4,h5,h6 { clear: both }
			li { margin: 10px 0 0 0; }
			* { margin: 0; font-family: Arial; font-size: 11pt; }
			* .fr1 { font-size: 11pt; vertical-align: top; margin-left: 0.1256in; margin-right: 0.1256in; padding: 0.0102in; border-style: none; }
			* .fr2 { font-size: 11pt; vertical-align: top; margin-left: 0.1256in; margin-right: 0.1256in; padding: 0.0102in; border-style: none; }
			* .Frame { font-size: 11pt; vertical-align: top; text-align: center; }
			* .Graphics { font-size: 11pt; vertical-align: top; text-align: center; }
			* .OLE { font-size: 11pt; vertical-align: top; text-align: center; }
			* .Caption { font-family: Arial; font-size: 11pt; margin-top: 0.0835in; margin-bottom: 0.0835in; font-style: italic; }
			* .Corpodetexto2 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.039in; text-align: justify ! important; text-indent: 0inch; }
			* .Corpodetexto3 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.1327in; text-align: justify ! important; text-indent: 0inch; }
			* .Footer { font-family: Arial; font-size: 11pt; }
			* .Header { font-family: Arial; font-size: 11pt; }
			* .Heading { font-family: Arial; font-size: 14pt; margin-top: 0.1665in; margin-bottom: 0.0835in; }
			* .Heading1 { font-family: Arial; font-size: 11pt; margin-left: 0.0984in; margin-right: 0.1327in; text-align: justify ! important; text-indent: 0inch; }
			* .Heading2 { font-family: Arial; font-size: 11pt; margin-left: 0.0984in; margin-right: 0.1327in; text-align: center ! important; text-indent: 0inch; }
			* .Heading3 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .Heading4 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.1327in; text-align: center ! important; text-indent: 0inch; }
			* .Heading5 { font-family: Arial; font-size: 11pt; margin-left: 0.0984in; margin-right: -0.3598in; text-align: justify ! important; text-indent: 0inch; color: #0000ff; }
			* .Heading6 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.1327in; text-align: justify ! important; text-indent: 0inch; color: #0000ff; }
			* .Heading7 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .Heading8 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.1327in; text-align: justify ! important; text-indent: 0inch; }
			* .Heading9 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0.1327in; text-align: justify ! important; text-indent: 2.0673inch; color: #0000ff; }
			* .Index { font-family: Arial; font-size: 11pt; }
			* .List { font-family: Arial; font-size: 11pt; line-height: 150%; text-align: justify ! important; }
			* .Lista2 { font-family: Arial; font-size: 11pt; margin-left: 0.3929in; margin-right: 0in; text-indent: -0.1965inch; }
			* .P1 { font-family: Arial; font-size: 11pt; margin-left: 1.4772in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; font-weight: bold; }
			* .P10 { font-family: Arial; font-size: 11pt; text-align: justify ! important; font-weight: bold; }
			* .P11 { font-family: Arial; font-size: 11pt; margin-left: 2.5598in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P12 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 2.0673inch; }
			* .P13 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 2.0673inch; }
			* .P14 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: -0.0193in; text-align: justify ! important; text-indent: 0inch; font-weight: bold; }
			* .P15 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0.9846inch; }
			* .P16 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0.9846inch; }
			* .P17 { font-family: Arial; font-size: 11pt; margin-left: 0.9846in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P18 { font-family: Arial; font-size: 11pt; margin-left: 0.9846in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P19 { font-family: Arial; font-size: 11pt; margin-left: 0.9846in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P2 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			
			* .P20 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P21 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P22 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P23 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P24 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P25 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P26 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P27 { font-family: Arial; font-size: 11pt; text-align: justify ! important; font-weight: bold; }
			* .P28 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P29 { font-family: Arial; font-size: 11pt; text-align: justify ! important; font-weight: bold; }
			
			* .P3 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .P30 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .P31 { font-family: Arial; font-size: 11pt; }
			* .P32 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .P33 { font-family: Arial; font-size: 11pt; margin-left: 1.4772in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; font-weight: bold; }
			* .P34 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P35 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P36 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P37 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; font-weight: bold; }
			* .P38 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; font-weight: bold; }
			* .P39 { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; text-align: justify ! important; text-indent: 0inch; }
			* .P4 { font-family: Arial; font-size: 11pt; }
			* .P40 { font-family: Arial; font-size: 11pt; text-align: justify ! important; font-weight: bold; }
			* .P5 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P6 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P7 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P8 { font-family: Arial; font-size: 11pt; text-align: justify ! important; }
			* .P9 { font-family: Arial; font-size: 11pt; text-align: center ! important; }
			* .Recuodecorpodetexto2 { font-family: Arial; font-size: 11pt; margin-left: 2.0673in; margin-right: 0in; line-height: 150%; text-align: justify ! important; text-indent: -2.0673inch; }
			* .Recuodecorpodetexto3 { font-family: Arial; font-size: 11pt; margin-left: 1.9689in; margin-right: 0in; line-height: 150%; text-align: justify ! important; text-indent: -1.9689inch; }
			* .Standard { font-family: Arial; font-size: 11pt; }
			* .TableContents { font-family: Arial; font-size: 11pt; }
			* .TableHeading { font-family: Arial; font-size: 11pt; text-align: center ! important; font-weight: bold; }
			* .Textbody { font-family: Arial; font-size: 11pt; line-height: 150%; text-align: justify ! important; }
			* .Textbodyindent { font-family: Arial; font-size: 11pt; margin-left: 0in; margin-right: 0in; line-height: 150%; text-align: justify ! important; text-indent: 1.9689inch; }
			* .Textodebal�o { font-family: Tahoma; font-size: 11pt; }
			* .Textodecoment�rio { font-family: Arial; font-size: 11pt; }
			* .Textoembloco { font-family: Arial; font-size: 11pt; margin-left: 2.1661in; margin-right: -0.3598in; text-align: justify ! important; text-indent: -2.1661inch; }
			* .Sect1 { margin-left: 0in; margin-right: 0.2957in; }
			* .Table1 { width: 6.791in; margin-left: -0.0486in; }
			* .Table1A1 { vertical-align: top; padding-left: 0.0486in; padding-right: 0.0486in; padding-top: 0in; padding-bottom: 0in; border-style: none; }
			* .Table1A { width: 3.3181in; }
			* .Table1B { width: 3.4729in; }
			* .Table11 {  }
			* .Emphasis { font-style: italic; }
			* .Fontepar�gpadr�o {  }
			* .Internetlink { color: #0000ff; text-decoration: underline; }
			* .PageNumber {  }
			* .T1 { font-family: Arial; font-size: 11pt; }
			* .T10 { font-family: Arial; font-size: 11pt; }
			* .T11 { font-family: Arial; font-size: 11pt; }
			* .T12 { font-size: 11pt; }
			* .T13 { font-size: 11pt; font-weight: bold; }
			* .T14 {  }
			* .T15 {  }
			* .T2 { font-family: Arial; font-size: 11pt; }
			* .T3 { font-family: Arial; }
			* .T4 { font-family: Arial; }
			* .T5 { font-family: Arial; font-weight: bold; }
			* .T6 { font-family: Arial; }
			* .T7 { font-family: Arial; font-size: 11pt; }
			* .T8 { font-family: Arial; font-size: 11pt; font-weight: bold; }
			* .T9 { font-family: Arial; font-size: 11pt; }
			* .VisitedInternetLink { color: #800080; text-decoration: underline; }
		</style>
		<link rel="stylesheet" type="text/css href="chrome://firebug/content/highlighter.css" />
	</head>


<body dir="ltr">
	<div style="text-align: center;">
		<img width="80" height="80" src="/imagens/brasao.gif"/>
		<p>Minist�rio da Educa��o�</p>
		<p>&nbsp;</p>
	</div>
	<p class="P11">
		<span class="_T3"><b>TERMO DE COOPERA��O T�CNICA</b> N� </span>#TERMO#
		<span class="_T3">QUE ENTRE SI CELEBRAM O MINIST�RIO DA EDUCA��O - MEC E O MUNIC�PIO DE </span><span style="text-transform:uppercase;"><?= $prefeitura["mundescricao"]."/".$prefeitura["estuf"] ?></span>
		<span class="_T3"> REPRESENTADO PELA </span><?= $prefeitura["entnome"] ?>.
	</p>
	<p>&nbsp;</p>
<p class="P25">
A Uni�o, por meio do Minist�rio da Educa��o - MEC, inscrito no CNPJ sob o n� 00.394.445/0124-52, com sede na Esplanada dos Minist�rios, Bloco L, Bras�lia/DF, neste ato representado pelo ministro de Estado Fernando Haddad</span>
, e o MUNIC�PIO DE <span style="text-transform:uppercase;"> <?= $prefeitura["mundescricao"]."/".$prefeitura["estuf"] ?></span>
, representado pela �<?= $prefeitura["entnome"] ?>
, inscrita no CNPJ/MF sob o n� <?= $prefeitura["entnumcpfcnpj"] ?>
, neste ato representada pelo(a) prefeito(a) <?// $prefeito["fundsc"] ?>
 </span><?= $prefeito["entnome"] ?>
, residente e domiciliado(a) em <?= $prefeito["mundescricao"]."/".$prefeitura["estuf"] ?>
<?// , Carteira de Identidade n� <?= $prefeito["entnumrg"] ?>
<? //, expedida pelo(a) <?= $prefeito["entorgaoexpedidor"]// ?>
, CPF n� <?= $prefeito["entnumcpfcnpj"] ?>
, resolvem celebrar o presente <b>Termo de Coopera��o T�cnica</b>, em conformidade com as pe�as constantes no Processo n� <?= $prefeitura["munprocesso"] ?>
, nos termos do Decreto n� 6094 de 24 de abril de 2007, mediante as cl�usulas e condi��es a seguir estabelecidas:

</p>

<p class="P27">�</p>
<p class="P27">DO OBJETO�</p>
<p class="P13">�</p>
<p class="P5">

CL�USULA PRIMEIRA - O presente
<b>Termo de Coopera��o T�cnica</b>
tem por objeto a conjuga��o de esfor�os entre as partes para a promo��o de a��es e atividades que contribuam para o processo de desenvolvimento educacional do munic�pio, visando a melhoria do �ndice de Desenvolvimento da Educa��o B�sica - IDEB.

</p>
<p class="P7">�</p>
<p class="P5"><span class="_T3">Par�grafo �nico - �O MEC designa
como unidades executoras do presente </span><span class="_T5"><b>Termo
de Coopera��o T�cnica</b></span><span class="_T3"> o Fundo Nacional
de Desenvolvimento da Educa��o - FNDE, a Secretaria de Educa��o Especial
- SEESP, a Secretaria de Educa��o a Dist�ncia - SEED, a Secretaria
de Educa��o Continuada, Alfabetiza��o e Diversidade - SECAD, a Secretaria de
Educa��o Profissional e Tecnol�gica - SETEC e a Secretaria de Educa��o B�sica -
SEB.</span></p>
<div class="Sect1">
<p class="P14">�</p>
<p class="P10">DAS A��ES�</p>
<p class="P10">�</p>
<p class="P5"><span class="_T3">CL�USULA SEGUNDA - A implementa��o do</span><span
	class="_T5"> <b>Termo de Coopera��o T�cnica</b> </span><span class="_T3">se
dar� por interm�dio da execu��o de a��es e atividades descritas no
Anexo I deste Termo.</span></p>
<p class="P7">�</p>
<p class="P5"><span class="_T3">
Par�grafo �nico - A execu��o das a��es constantes do Anexo I ser� de acordo com os quantitativos, estrat�gias de implementa��o e cronogramas constantes do Plano de A��es Articuladas, parte integrante deste processo.
</span></p>
<p class="P7">�</p>
<p class="P10">DAS ATRIBUI��ES DOS PART�CIPES</p>
<p class="P7">�</p>
<p class="P6"><span class="_T3">CL�USULA TERCEIRA - Compete
conjuntamente aos part�cipes:</span></p>
<p class="P16">�</p>
<ol style="margin-left: 0.5cm; list-style-type: lower-alpha;">
	<li class="P17" style="margin-left: 2.5cm;">
	<p class="P17" style="margin-left: 0.25cm;"><span class="_T3">desenvolver,
	elaborar e prover apoio t�cnico aos programas e projetos a serem
	definidos para a implementa��o do presente Termo;</span></p>
	</li>

	<li class="P17" style="margin-left: 2.5cm;">
	<p class="P17" style="margin-left: 0.25cm;"><span class="T6">d</span><span
		class="_T3">isponibilizar materiais e informa��es t�cnicas necess�rias
	� implementa��o dos programas e projetos;</span></p>
	</li>

	<li class="P17" style="margin-left: 2.5cm;">
	<p class="P17" style="margin-left: 0.25cm;"><span class="_T3">acompanhar,
	monitorar e avaliar os resultados alcan�ados nas a��es e atividades
	programadas, visando a otimiza��o e/ou adequa��o quando necess�rios;</span></p>
	</li>

	<li class="P20" style="margin-left: 2.5cm;">
	<p class="P20" style="margin-left: 0.25cm;">conduzir todas as
	atividades com efici�ncia e dentro de pr�ticas administrativas e
	t�cnicas adequadas.�</p>
	</li>
</ol>
<p class="P8">�</p>
<p class="P29">DA VIG�NCIA�</p>
<p class="P29">�</p>
<p class="P34"><span class="T12">CL�USULA QUARTA - O presente </span><span
	class="T13"><b>Termo de Coopera��o T�cnica</b> </span><span class="T12">vigorar� pelo
prazo de 04 (quatro) anos, a partir da data de sua assinatura, com a
possibilidade de prorroga��o por igual ou inferior per�odo, podendo ser
rescindido por iniciativa de qualquer das partes, mediante aviso pr�vio
de no m�nimo 30 (trinta) dias.</span></p>
<p class="P35">�</p>


<p class="P29">DAS ALTERA��ES (AJUSTES)</p>
<p class="P29">�</p>
<p class="P34"><span class="T12">
CL�USULA QUINTA - O presente <b>Termo de Coopera��o T�cnica</b> poder� ser ajustado (aditivado), sempre que houver acordo para altera��o das a��es previstas no Plano de A��es Articuladas.
</span></p>
<p class="P35">�</p>


<p class="P40">DOS RECURSOS�</p>
<p class="P40">�</p>
<p class="P5"><span class="_T3">CL�USULA SEXTA - N�o haver� transfer�ncia
volunt�ria de recursos financeiros entre os part�cipes para a execu��o
deste </span><span class="_T5"><b>Termo de Coopera��o T�cnica</b>.</span><span
	class="_T3"> As despesas necess�rias � plena consecu��o do objeto
acordado, tais como servi�os de terceiros, pessoal, deslocamentos,
comunica��o entre os �rg�os e outras que se fizerem necess�rias, correr�o
por conta de dota��es espec�ficas constantes nos or�amentos dos
part�cipes e/ou outros parceiros</span><span class="_T4">.</span></p>
<p class="P7">�</p>
<p class="P10">DA RESCIS�O�</p>
<p class="P10">�</p>
<p class="P21"><span class="_T3">CL�USULA S�TIMA - A rescis�o deste
Termo ocorrer� em decorr�ncia do inadimplemento das cl�usulas
pactuadas, quando a execu��o das a��es e atividades estiver em desacordo
com o objeto, e ainda por raz�es de interesse p�blico.</span></p>
<p class="P7">�</p>
<h1 class="P37"><a name="DA_PUBLICA_C3_87_C3_83O" />DA PUBLICA��O</h1>
<p class="P10">�</p>
<p class="P21"><span class="_T3">CL�USULA OITAVA - A publica��o deste <b>Termo de Coopera��o 
T�cnica</b> ser� efetivada, por extrato, no Di�rio Oficial da Uni�o, que correr� � conta do 
MEC as despesas correspondentes.</span></p>
<p class="P7">�</p>
<p class="P7">�</p>
<p class="P10">DAS DISPOSI��ES GERAIS�</p>
<p class="P7">�</p>
<p class="P5"><span class="_T3">CL�USULA NONA - A efetividade das a��es assumidas pelo MEC/FNDE 
fica condicionada � disponibilidade or�ament�ria-financeira, 
bem como �s demais circunst�ncias impeditivas ao cumprimento do estabelecido no 
presente Termo.</span></p>
<p class="P7">�</p>
<p class="P10">DO FORO�</p>
<p class="P13">�</p>
CL�USULA D�CIMA - Fica eleito o Foro da Se��o Judici�ria do Distrito Federal, para dirimir d�vidas ou lit�gios decorrentes da interpreta��o, aplica��o ou execu��o deste Termo, com ren�ncia expressa de qualquer outro.
</span></p>
<p class="P7">�</p>
<p class="P15">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E, por estarem de pleno acordo, firmam o
presente instrumento em 04 (quatro) vias de igual teor e forma, perante
as duas testemunhas abaixo qualificadas.�</p>
<p class="P13">�</p>
<?php
$mes = array(
	"Janeiro",
	"Fevereiro",
	"Mar�o",
	"Abril",
	"Maio",
	"Junho",
	"Julho",
	"Agosto",
	"Setembro",
	"Outubro",
	"Novembro",
	"Dezembro"
);
?>
<p class="P12"><span class="_T3">Bras�lia-DF, <?= date( "d" ) ?> de <?= $mes[date( "n" )-1] ?> de 2008.</span></p>

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td style="width: 50%; text-align: center;">
			<div style="margin: 10px; border: 1px solid #000000; padding: 40px 5px 5px 5px; height: 150px;">
				<p>________________________________</p>
				<p style="font-weight: bold;">FERNANDO HADDAD</p>
				<p style="font-size:6px;">&nbsp;</p>
				<p>MINIST�RIO DA EDUCA��O</p>
			</div>
		</td>
		<td style="width: 50%; text-align: center;">
			<div style="margin: 10px; border: 1px solid #000000; padding: 40px 5px 5px 5px; height: 150px;">
				<p>________________________________</p>
				<p style="font-weight: bold;"><?= $prefeito["entnome"] ?></p>
				<p style="font-size:6px;">&nbsp;</p>
				<?= $prefeitura["entnome"] ?>
			</div>
		</td>
	</tr>
</table>

<p class="P24">�</p>
<p class="P7">Testemunhas:�</p>
<p class="P7">�</p>
<div style="text-align: left;">
<table cellspacing="0" cellpadding="0" border="0" class="Table1">
	<colgroup>
		<col width="368" />
		<col width="385" />
	</colgroup>
	<tr class="Table11">
		<td style="text-align: left; width: 3.3181in;" class="Table1A1">
		<p class="P22">Nome:...................................................�</p>
		</td>
		<td style="text-align: left; width: 3.4729in;" class="Table1A1">
		<p class="P22">Nome:......................................................�</p>
		</td>
	</tr>
	<tr class="Table11">
		<td style="text-align: left; width: 3.3181in;" class="Table1A1">
		<p class="P22">CPF:.....................................................�</p>
		</td>
		<td style="text-align: left; width: 3.4729in;" class="Table1A1">
		<p class="P22">CPF:........................................................�</p>
		</td>
	</tr>
	<tr class="Table11">
		<td style="text-align: left; width: 3.3181in;" class="Table1A1">
		<p class="P22">R.G:......................................................�</p>
		</td>
		<td style="text-align: left; width: 3.4729in;" class="Table1A1">
		<p class="P22">R.G:.........................................................�</p>
		</td>
	</tr>
	<tr class="Table11">
		<td style="text-align: left; width: 3.3181in;" class="Table1A1">
		<p class="P22">Assinatura:............................................�</p>
		</td>
		<td style="text-align: left; width: 3.4729in;" class="Table1A1">
		<p class="P22">Assinatura:...............................................�</p>
		</td>
	</tr>
</table>
</div>
<p class="P5">�</p>
<p class="P23">�</p>
</div>
<div style="page-break-before: always;">
	<h1>Anexo 1</h1>
	<p>
		Assist�ncia T�cnica do MEC ao Munic�pio <?= $prefeitura["mundescricao"] ?>.
	</p>
	<p>&nbsp;</p>
	<?php
	$dimdsc = "";
	?>
	<?php foreach( $subacoes as $subacao ): ?>
		<?php
		$quantidade = $subacao["sba1ano"] + $subacao["sba2ano"] + $subacao["sba3ano"] + $subacao["sba4ano"];
		if ( $quantidade == 0 ) {
			continue;
		}
		$texto = $subacao["sbatexto"];	
		$texto = str_replace( "#OBJETIVO#", $subacao["sbaobjetivo"], $texto );
		$texto = str_replace( "#UNIDADE_MEDIDA#", $subacao["unddsc"], $texto );
		$texto = str_replace( "#QUANTIDADE#", (integer) $quantidade, $texto );
		$texto = str_replace( "#PROGRAMA#", $subacao["prgdsc"], $texto );
		?>
		<?php if( $subacao["dimdsc"] != $dimdsc ): ?>
			<?php $dimdsc = $subacao["dimdsc"]; ?>
			</ol>
			<h3 style="margin:14px 0 14px 0;"><?= $subacao["dimcod"] ?>. <?= $subacao["dimdsc"] ?></h3>
			<ol style="list-style: decimal;">
		<?php endif; ?>
		<li style="list-style: decimal;">
			<?php
			echo $texto;
			?>
		</li>
	<?php endforeach; ?>
	</ol>
</div>
</body>
</html>
