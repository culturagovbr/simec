<?php
/**
 * Tela inicial do m�dulo planacomorc com o menu de op��es dispon�veis no sistema.
 * $Id: inicio.inc 103493 2015-10-08 19:31:03Z werteralmeida $
 */
/*
 * Componetnes para a cria��o da p�gina In�cio
 */
include APPRAIZ . "includes/dashboardbootstrap.inc";

$perfis = pegaPerfilGeral();
// -- Setando o novo exercicio escolhido via seletor no cabe�alho da p�gina
if (isset($_REQUEST['exercicio'])) {
    $_SESSION['exercicio'] = $_REQUEST['exercicio'];
}

include APPRAIZ . "includes/cabecalho.inc";
?>

<link href="/includes/spo.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="/includes/funcoesspo.js"></script>
<script type="text/javascript" lang="javascript">
    $(document).ready(function () {
        inicio();
    });
</script>
<div style="width: 450px; float:right; ">
    <p style="color:tomato; font-weight: bold; margin-bottom: 4px;">
        <span class="glyphicon glyphicon-asterisk"></span> Aten��o, nosso m�dulo mudou!
    </p>
    <p>
        Agora tudo referente a <b>Acompanhamentos</b> est� em um m�dulo <a href="/acomporc/acomporc.php?modulo=inicio&acao=C">Novo</a>.
    </p>


    <p style="font-weight: bold;font-size: 11px; margin-top: 4px;">
        Mas n�o se preocupe, todos os dados e funcionalidades permanecem como antes. Em caso de d�vidas entrar em contato (spo.planejamento@mec.gov.br)
    </p>

</div>
<div style="clear:both;"></div>
<table width="95%" class="tabela" bgcolor="#f5f5f5" border="0" cellSpacing="1" cellPadding="3" align="center">
    <tr align="center">
        <td align="center" >
            <?php if (in_array(1207, $perfis) || in_array(PFL_SUPERUSUARIO, $perfis) || in_array(PFL_CPMO, $perfis)) { ?>
                <div style=" background-color: #9370DB;" class="divGraf">
                    <center>
                        <span class="tituloCaixa" >Institui��es Federais</span> <br><br><br>
                    </center>

                    <?php
                    $params = array();
                    $params['texto'] = 'Lista de Suba��es';
                    $params['tipo'] = 'listar';
                    $params['url'] = 'planacomorc.php?modulo=principal/unidade/listasubacoes&acao=A';
                    montaBotaoInicio($params);

                    $params = array();
                    $params['texto'] = 'Cadastrar Suba��o';
                    $params['tipo'] = 'cadastrar';
                    $params['url'] = 'planacomorc.php?modulo=principal/unidade/cadastro_subacoes&acao=A';
                    montaBotaoInicio($params);

                    $params = array();
                    $params['texto'] = 'Lista de PI';
                    $params['tipo'] = 'listar';
                    $params['url'] = 'planacomorc.php?modulo=principal/unidade/listapimanter&acao=A';
                    montaBotaoInicio($params);

                    $params = array();
                    $params['texto'] = 'Cadastrar PI';
                    $params['tipo'] = 'cadastrar';
                    $params['url'] = 'planacomorc.php?modulo=principal/unidade/cadastro_pi&acao=A';
                    montaBotaoInicio($params);

                    $params = array();
                    $params['texto'] = 'Lista de PTRES';
                    $params['tipo'] = 'listar';
                    $params['url'] = 'planacomorc.php?modulo=principal/unidade/listaptres&acao=A';
                    montaBotaoInicio($params);

                    $params = array();
                    $params['texto'] = 'Manual de Cadastro de PI e Suba��es (Institui��es Federais)';
                    $params['tipo'] = 'pdf';
                    $params['url'] = '../planacomorc/manual_simec_subacoes_e_pis_unidades.pdf';
                    montaBotaoInicio($params);
                    ?>
                </div>
                <?php
            }
            if (in_array(PFL_SUPERUSUARIO, $perfis) || in_array(PFL_CPMO, $perfis) || in_array(PFL_RELATORIO_TCU, $perfis)) {
                ?>
                <!--                <div style="background-color: #FF8C00;" class="divGraf">
                                    <span class="tituloCaixa" >Relat�rio de Gest�o (TCU/CGU)</span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Preencher Relat�rio';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=principal/relatoriogestao/cadastro_gestao&acao=A';
                montaBotaoInicio($params);
                ?>
                                    <span class="subTituloCaixa">Question�rio por Exerc�cio</span>
                <?php
                $params = array();
                $params['texto'] = 'Question�rio de Relat�rio';
                $params['tipo'] = 'questionario';
                $params['url'] = '/planacomorc/planacomorc.php?modulo=principal/questionariorelatoriogestao/listaquestionario&acao=A';
                montaBotaoInicio($params);
                ?>
                                </div>-->
            <?php } ?>
            <div id="divAcoes" class="divGraf bg-ds">
                <span class="tituloCaixa">A��es <?= $_SESSION['exercicio']; ?></span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Extrato completo das A��es ' . $_SESSION['exercicio'];
                $params['tipo'] = 'snapshot';
                $params['url'] = 'planacomorc.php?modulo=principal/dadosacoes/listadadosacoes&acao=A';
                montaBotaoInicio($params);

//                    $params = array();
//                    $params['texto'] = 'Central de Acompanhamento';
//                    $params['tipo'] = 'central';
//                    $params['url'] = 'planacomorc.php?modulo=centralacompanhamento&acao=A';
//                    montaBotaoInicio($params);
//
//                    $params = array();
//                    $params['texto'] = 'Acompanhamento de A��es';
//                    $params['tipo'] = 'acompanhamento';
//                    $params['url'] = 'planacomorc.php?modulo=principal/acoes/listaunidades&acao=A';
//                    montaBotaoInicio($params);
//
//                    $params = array();
//                    $params['texto'] = 'Question�rio de Acompanhamento';
//                    $params['tipo'] = 'questionario';
//                    $params['url'] = 'planacomorc.php?modulo=principal/questionario/listaquestionario&acao=A';
//                    montaBotaoInicio($params);
//
//                    $params = array();
//                    $params['texto'] = 'Per�odos de Refer�ncia do Acompanhamento';
//                    $params['tipo'] = 'calendario';
//                    $params['url'] = 'planacomorc.php?modulo=principal/periodoreferencia/listaperiodoreferencia&acao=A';
//                    montaBotaoInicio($params);
//
//                    $params = array();
//                    $params['texto'] = 'Metodologia do Acompanhamento';
//                    $params['tipo'] = 'calendario';
//                    $params['url'] = 'planacomorc.php?modulo=principal/produto/listametodologia&acao=A';
//                    montaBotaoInicio($params);
//
//                    $params = array();
//                    $params['texto'] = 'Enviar Acompanhamento para o SIOP em LOTE';
//                    $params['tipo'] = 'enviar';
//                    $params['url'] = '/planacomorc/planacomorc.php?modulo=sistema/comunica/enviaQuantitativos&acao=A';
//                    montaBotaoInicio($params);
                ?>
                <span class="subTituloCaixa">Relat�rios</span>
                <?php
//                    $params = array();
//                    $params['texto'] = 'Respons�veis';
//                    $params['tipo'] = 'user';
//                    $params['url'] = '/planacomorc/planacomorc.php?modulo=relatorio/responsaveis/acoes&acao=A';
//                    montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Espelho das A��es';
                $params['tipo'] = 'espelho';
                $params['url'] = 'planacomorc.php?modulo=principal/dadosacoes/espelho&acao=A';
                montaBotaoInicio($params);
                ?>
            </div>
            <div class="divGraf bg-ds" id="divSubacoes">
                <span class="tituloCaixa">Suba��es <?= $_SESSION['exercicio']; ?></span><br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Lista de Suba��es';
                $params['tipo'] = 'listar';
                $params['url'] = 'planacomorc.php?modulo=principal/subacoes/listasubacoesmanter&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Cadastrar Suba��o';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=principal/subacoes/cadastro&acao=A';
                montaBotaoInicio($params);

//                    $params = array();
//                    $params['texto'] = 'Acompanhamento';
//                    $params['tipo'] = 'acompanhamento';
//                    $params['url'] = 'planacomorc.php?modulo=principal/subacoes/listasubacoes&acao=A';
//                    montaBotaoInicio($params);
//                    
//                    $params = array();
//                    $params['texto'] = 'Central de Acompanhamento';
//                    $params['tipo'] = 'central';
//                    $params['url'] = 'planacomorc.php?modulo=principal/subacoes/centralacompanhamento&acao=A';
//                    montaBotaoInicio($params);
//                    
//                    $params = array();
//                    $params['texto'] = 'Produtos das Suba��es';
//                    $params['tipo'] = 'listar';
//                    $params['url'] = 'planacomorc.php?modulo=principal/produto/listaproduto&acao=A';
//                    montaBotaoInicio($params);
//                                        $params = array();
//                    $params['texto'] = 'Question�rio de Acompanhamento de Suba��es';
//                    $params['tipo'] = 'questionario';
//                    $params['url'] = 'planacomorc.php?modulo=principal/questionariosubacoes/listaquestionario&acao=A';
//                    montaBotaoInicio($params);
//                    
                $params = array();
                $params['texto'] = 'Solicita��o de Remanejamento de Suba��o';
                $params['tipo'] = 'processo';
                $params['url'] = 'planacomorc.php?modulo=principal/remanejavalorsubacao/listaptres&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Remanejamento de Suba��o';
                $params['tipo'] = 'processo';
                $params['url'] = 'planacomorc.php?modulo=principal/remanejavalorsubacao/listaptres&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Transa��es de Remanejamento de Suba��o';
                $params['tipo'] = 'listar';
                $params['url'] = 'planacomorc.php?modulo=principal/remanejavalorsubacao/listartransacoes&acao=A';
                montaBotaoInicio($params);
                ?>
                <!--<span class="subTituloCaixa">Relat�rios</span>-->
                <?php
//                    $params = array();
//                    $params['texto'] = 'Produtos das Suba��es';
//                    $params['tipo'] = 'user';
//                    $params['url'] = '/planacomorc/planacomorc.php?modulo=relatorio/responsaveis/subacoes&acao=A';
//                    montaBotaoInicio($params);
                ?>
            </div>

            <div id="divPI" class="divGraf">
                <span class="tituloCaixa" >Plano Interno</span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Lista de PI';
                $params['tipo'] = 'listar';
                $params['url'] = 'planacomorc.php?modulo=principal/planointerno/listapimanter&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Cadastrar PI';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=principal/planointerno/cadastro&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Solicita��o de Cadastro de PI';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=principal/planointerno/cadastro&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Listar Transa��es de Solicita��o de Cadastro de PI';
                $params['tipo'] = 'listar';
                $params['url'] = 'planacomorc.php?modulo=principal/planointerno/listatransacoes&acao=A';
                montaBotaoInicio($params);
                ?>
            </div>
            <div style="background-color: darksalmon;" class="divGraf">
                <span class="tituloCaixa" >PTRES <?= $_SESSION['exercicio']; ?></span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Lista de PTRES';
                $params['tipo'] = 'listar';
                $params['url'] = 'planacomorc.php?modulo=principal/ptres/listaptres&acao=A';
                montaBotaoInicio($params);
                ?>
            </div>
            <div id="divTabelaApoio" class="divGraf">
                <span class="tituloCaixa" >Tabelas de Apoio</span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Enquadramentos de Despesa';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadEnquadramentoDespesas&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Modalidades de Ensino/Tema/P�blico';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadModalidadesEnsino&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'N�vel/Etapa de Ensino';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadNivelEtapaEnsino&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Executor Or�ament�rio e Financeiro';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadExecutorOrcamentarioFinanceiro&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Gestor da Suba��o';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadGestorSubacao&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Vincula��o de Enquadramentos de Despesa a Suba��o';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/AddEnquadramentoSubacao&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Vincula��o Or�ament�ria da A��o Estrat�gica.';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/vinculacaoOrcamentaria&acao=A&listar=true';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = ' Categoria de Apropria��o.';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'planacomorc.php?modulo=sistema/tabelasapoio/cadCategoriaApropriacao&acao=A&listar=true';
                montaBotaoInicio($params);
                ?>
            </div>
            <div id="divManuais" class="divGraf">
                <span class="tituloCaixa" >Manuais</span> <br><br><br>
                <?php
//                $params = array();
//                $params['texto'] = 'Planejamento e Acompanhamento Or�ament�rio 2013';
//                $params['tipo'] = 'pdf';
//                $params['url'] = '../planacomorc/Manual_SIMEC_Planejamento_e_Acompanhamento_Orcamentario_2013.pdf';
//                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Manual de Cadastro de PI e Suba��es (Institui��es Federais)';
                $params['tipo'] = 'pdf';
                $params['url'] = '../planacomorc/manual_simec_subacoes_e_pis_unidades.pdf';
                montaBotaoInicio($params);
                ?>
                <?php
                if (in_array(PFL_SUPERUSUARIO, $perfis) || in_array(PFL_CPMO, $perfis)) {
                    $params = array();
                    $params['texto'] = 'Calend�rio da SPO';
                    $params['tipo'] = 'calendario';
                    $params['url'] = '../planacomorc/calendario_spo.pdf';
                    montaBotaoInicio($params);
                }
                ?>
            </div>
            <div id="divComunicados" class="divCap">
                <span class="tituloCaixa">Comunicados</span>
                <?php
                montaComunicados();
                ?>
            </div>
        </td>
    </tr>
</table>
