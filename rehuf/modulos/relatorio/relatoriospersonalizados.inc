<?php
if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

include APPRAIZ . 'includes/Agrupador.php';
include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - REHUF", "" );

?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadosrehuf';
		var janela = window.open( '', 'relatoriopersonlizadosrehuf', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$relatorios = array(0=>array('codigo'=>'relatpersonal_sustentsus.inc','descricao'=>'Relat�rio de Sustentabilidade SUS (Ideal) (2004-2008)'),
								1=>array('codigo'=>'relatpersonal_leitos.inc','descricao'=>'Relat�rio de Leitos (2004-2008)'),
								2=>array('codigo'=>'relatpersonal_leitos2009.inc','descricao'=>'Relat�rio de Leitos (2009-2010)'),
								3=>array('codigo'=>'relatpersonal_recursosus.inc','descricao'=>'Relat�rio de Recursos do SUS (2004-2008)'),
								4=>array('codigo'=>'relatpersonal_tipocontratacao.inc','descricao'=>'Relat�rio de quantidade de pessoal (2004-2008)'),
								5=>array('codigo'=>'relatpersonal_sustentsusreal.inc','descricao'=>'Relat�rio de Sustentabilidade SUS (Real) (2004-2008)'),
								6=>array('codigo'=>'relatpersonal_ftestdes2008.inc','descricao'=>'Relat�rio de Fatores Estrat�gicos de Desempenho (2008)'),
								7=>array('codigo'=>'relatpersonal_altacomplexidade.inc','descricao'=>'Relat�rio de Procedimentos de Alta Complexidade (2008-2009)'),
								8=>array('codigo'=>'relatpersonal_reccorr.inc','descricao'=>'Relat�rio de Saldo Corrente (2004-2008)'),
								9=>array('codigo'=>'relatpersonal_plantoeshosp.inc','descricao'=>'Relat�rio de plant�es hospitalares (2009)'),
								10=>array('codigo'=>'relatpersonal_plantoeshosp2010.inc','descricao'=>'Relat�rio de plant�es hospitalares (2010)'),
								11=>array('codigo'=>'relatpersonal_plantoeshosp2011.inc','descricao'=>'Relat�rio de plant�es hospitalares (2011)'),
								12=>array('codigo'=>'relatpersonal_plantoeshosp2012.inc','descricao'=>'Relat�rio de plant�es hospitalares (2012)'),
								13=>array('codigo'=>'relatpersonal_plantoeshosp2013.inc','descricao'=>'Relat�rio de plant�es hospitalares (2013)'),
								14=>array('codigo'=>'relatpersonal_matrizdistribuicao.inc','descricao'=>'Matriz de distribui��o (2009)'),
								15=>array('codigo'=>'relatpersonal_matrizdistribuicao2010.inc','descricao'=>'Matriz de distribui��o (2010)'),
								16=>array('codigo'=>'relatpersonal_matrizdistribuicao2011.inc','descricao'=>'Matriz de distribui��o (2011)'),
								17=>array('codigo'=>'relatpersonal_matrizdistribuicao2012.inc','descricao'=>'Matriz de distribui��o (2012) - 1�'),
								18=>array('codigo'=>'relatpersonal_matrizdistribuicao2013.inc','descricao'=>'Matriz de distribui��o (2013)'),
								19=>array('codigo'=>'relatpersonal_matrizdistribuicao2014.inc','descricao'=>'Matriz de distribui��o (2014)'),
								20=>array('codigo'=>'relatpersonal_faec.inc','descricao'=>'Relat�rio de Procedimentos de FAEC (2008-2009)'),
								21=>array('codigo'=>'relatpersonal_receitacorrente.inc','descricao'=>'Relat�rio de Receita Corrente (2009)'),
								22=>array('codigo'=>'relatpersonal_hospitalendereco.inc','descricao'=>'Relat�rio de dados dos Hospitais'),
								23=>array('codigo'=>'relatpersonal_taxaspartocesario.inc','descricao'=>'Taxas de Partos Ces�reos'));
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '400', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>