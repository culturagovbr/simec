<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
<script language="JavaScript" src="../includes/prototype.js"></script>
<script language="JavaScript" src="./js/rehuf.js"></script>
<style type="text/css">
.stylecolunas {
	background-color: #dcdcdc;
	text-align: center; 
}
.stylelinhas {
	background-color: #dcdcdc;
	text-align: right; 
}
.styleconteudo {
	text-align: left;
}
.titulounidade {
	background-color:#CDCDCD;
	text-align:center; 
	font-weight: bold;
}
</style>
<?
/* configura��es */
ini_set("memory_limit", "3000M");
set_time_limit(0);
/* FIM configura��es */

$sql = array(// Total de leitos
			 0 => "SELECT ROUND(SUM(ctivalor)/4) FROM rehuf.conteudoitem WHERE esuid='{esuid}' AND colid IN('441','442','443','444') AND ctiexercicio='{ano}' and perid='893'",
			 // Total de leitos UTI
			 1 => "SELECT ROUND(SUM(ctivalor)/4) FROM rehuf.conteudoitem WHERE esuid='{esuid}' AND linid IN('15050','15051','15052') AND colid IN('441','442','443','444') AND ctiexercicio='{ano}' and perid='893'",
			 // Salas Cir�rgicas Ativas
			 2 => "SELECT ROUND(SUM(ctivalor)/4) FROM rehuf.conteudoitem WHERE esuid='{esuid}' AND linid IN('15083','15088') AND ctiexercicio='{ano}' AND perid='896'",
			 // Total Alunos (Gradua��o)
			 3 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid='{esuid}' AND colid IN('660') AND ctiexercicio='{ano}' AND perid IN('846')",
			 // Total Alunos de Medicina (Gradua��o)
			 4 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND colid IN('660') AND linid IN('14829','15018') AND ctiexercicio='{ano}' AND perid IN('846')",
			 // Total Alunos (Resid�ncia M�dica)
			 5 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND colid IN('397','398','399','400')  AND ctiexercicio='{ano}' AND perid IN('847')",
			 // Total Docentes
			 6 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND colid IN('680') AND ctiexercicio='{ano}' AND perid IN('934')",
			 // Total Interna��es
			 7 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND colid IN('612') AND ctiexercicio='{ano}' AND perid='949'",
			 // N�mero de Projetos Aprovados no CEP
			 8 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND linid='287' AND colid IN('662') AND ctiexercicio='{ano}' AND perid='910'",
			 // Partos de Alto Risco
			 9 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND linid IN('1992','1993') AND ctiexercicio='{ano}' AND perid='777'",
			 // Total Pessoal
			 10 => "SELECT ROUND(SUM(ctivalor)) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND colid IN('492','494','496','515','500','502','504','506') AND ctiexercicio='{ano}' AND perid in('828', '884', '944')",
			// Total de leitos-Dia
			 11 => "SELECT ROUND(SUM(ctivalor)/4) FROM rehuf.conteudoitem WHERE esuid='{esuid}' AND linid IN('15055','15056','15057','15058') AND colid IN('441','442','443','444') AND ctiexercicio='{ano}' AND perid='893'",
			 // Total Interna��es-Dia
			 12 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND linid='1986' AND ctiexercicio='{ano}' AND perid='777'",
			 // Total Interna��es
			 13 => "SELECT SUM(ctivalor) FROM rehuf.conteudoitem WHERE esuid IN('{esuid}') AND linid IN('8142','8141','8140','15110','1978','1989','1987','1986','1984','1983','1981','1980','1979','15111','1988','1993','1992','1990','1991') AND ctiexercicio='{ano}' AND perid='777'"
			 
			 );
			  
			 

$filtro = (($_REQUEST['entid'][0])?"WHERE ent.entid IN ('".implode("','",$_REQUEST['entid'])."')":"");
$esuids = $db->carregar("SELECT esu.esuid, ent.entnome, ent.entid, ena.entsig, ende.estuf, mundescricao, esu.esutipo FROM rehuf.estruturaunidade esu
						 LEFT JOIN entidade.entidade ent ON ent.entid=esu.entid 
						 LEFT JOIN entidade.funcaoentidade fen ON fen.entid = ent.entid 
						 LEFT JOIN entidade.funentassoc fea ON fea.fueid = fen.fueid 
			 			 LEFT JOIN entidade.entidade ena ON ena.entid = fea.entid 
						 LEFT JOIN entidade.endereco ende ON ende.entid = ent.entid 
						 LEFT JOIN territorios.municipio mun ON mun.muncod = ende.muncod AND mun.estuf = ende.estuf
			 			 WHERE fen.funid = '". HOSPITALUNIV ."' AND (esu.esuindexibicao IS NULL OR esu.esuindexibicao = true) ORDER BY ena.entsig");

$_ano = array('2014');

echo "<h1>Matriz de distribui��o</h1>";
foreach($_ano as $ano) {
	echo "<table width='100%'>";
	echo "<tr>";
	echo "<td class='SubTituloCentro' colspan='20'>ANO : ".$ano."</td>";
	echo "</tr>";
	
	
	echo "<tr>";
	echo "<td class='SubTituloCentro'>Hospital</td>";
	echo "<td class='SubTituloCentro'>IFES</td>";
	echo "<td class='SubTituloCentro'>Tipo do HUF</td>";
	echo "<td class='SubTituloCentro'>Total de Leitos</td>";
	echo "<td class='SubTituloCentro'>Total de Leitos-Dia</td>";
	echo "<td class='SubTituloCentro'>Total de Leitos UTI</td>";
	
	echo "<td class='SubTituloCentro'>Partos de Alto Risco</td>";
	echo "<td class='SubTituloCentro'>Salas Cir�rgicas Ativas</td>";
	echo "<td class='SubTituloCentro'>Total Alunos (Gradua��o)</td>";
	echo "<td class='SubTituloCentro'>Total Alunos de Medicina (Gradua��o)</td>";
	echo "<td class='SubTituloCentro'>Total Alunos (Resid�ncia M�dica)</td>";
	

	echo "<td class='SubTituloCentro'>Total Docentes</td>";
	echo "<td class='SubTituloCentro'>Total Pessoal</td>";
	echo "<td class='SubTituloCentro'>Total de Interna��es</td>";
	echo "<td class='SubTituloCentro'>Total de Interna��es Leitos Dia</td>";
	echo "<td class='SubTituloCentro'>Total Interna��es Dia</td>";
	echo "<td class='SubTituloCentro'>Projetos Aprovados pelo CEP</td>";
	echo "</tr>";
	
	foreach($esuids as $esuid) {
		$totalLeitos = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[0]));
		$totalLeitosDia = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[11]));
		$totalLeitosUTI = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[1]));
		$totalSalasCirurgicasAtivas = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[2]));
		
		$totalAlunosGraduacao = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[3]));
		
		$totalAlunosGraduacaoMedicina = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[4]));

		$totalAlunosMedicinaResidencia = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[5]));
		$totalDocentes = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[6]));
		$totalInternacoesLeitosDia = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[7]));
		$totalInternacoesDia = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[12]));
		$totalInternacoes = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[13]));
		
		$totalProjetosCEP = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[8]));
		$totalPartosAltoRisco = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[9]));
		
		$totalPessoal = $db->pegaUm(str_replace(array("{esuid}","{ano}"),array($esuid['esuid'],$ano),$sql[10]));
		
		switch($esuid['esutipo']) {
			case 'E':
				$esutipo = "Especialidade";
				break;
			case 'G':
				$esutipo = "Geral";
				break;
			case 'M':
				$esutipo = "Maternidade";
				break;
				
		}
		
		
		echo "<tr>";
		echo "<td nowrap>".$esuid['entnome']."</td>";
		echo "<td>".$esuid['entsig']."</td>";
		echo "<td>".$esutipo."</td>";
		echo "<td align=right>".(($totalLeitos)?$totalLeitos:"0")."</td>";
		echo "<td align=right>".(($totalLeitosDia)?$totalLeitosDia:"0")."</td>";
		echo "<td align=right>".(($totalLeitosUTI)?$totalLeitosUTI:"0")."</td>";
		echo "<td align=right>".(($totalPartosAltoRisco)?$totalPartosAltoRisco:"0")."</td>";
		echo "<td align=right>".(($totalSalasCirurgicasAtivas)?$totalSalasCirurgicasAtivas:"0")."</td>";
		echo "<td align=right>".(($totalAlunosGraduacao)?$totalAlunosGraduacao:"0")."</td>";
		echo "<td align=right>".(($totalAlunosGraduacaoMedicina)?$totalAlunosGraduacaoMedicina:"0")."</td>";
		echo "<td align=right>".(($totalAlunosMedicinaResidencia)?$totalAlunosMedicinaResidencia:"0")."</td>";
		echo "<td align=right>".(($totalDocentes)?$totalDocentes:"0")."</td>";
		echo "<td align=right>".(($totalPessoal)?$totalPessoal:"0")."</td>";
		echo "<td align=right>".(($totalInternacoes)?$totalInternacoes:"0")."</td>";
		echo "<td align=right>".(($totalInternacoesLeitosDia)?$totalInternacoesLeitosDia:"0")."</td>";
		echo "<td align=right>".(($totalInternacoesDia)?$totalInternacoesDia:"0")."</td>";
		echo "<td align=right>".(($totalProjetosCEP)?$totalProjetosCEP:"0")."</td>";
		echo "</tr>";
	}


echo "</table>";

}

?>