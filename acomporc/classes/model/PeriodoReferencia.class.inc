<?php
/**
 * Implementa��o da classe de abstra��o de per�odos de refer�ncia.
 *
 * $Id$
 */

/**
 * @see Spo_Model_Periodoreferencia
 */
class Acomporc_Model_PeriodoReferencia extends Spo_Model_Periodoreferencia_Abstract
{
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = "acomporc.periodoreferencia";

    /**
     * @inheritdoc
     */
    protected function init()
    {
        // -- A - A��o, S - Suba��o, T - Tcu
        $this->arAtributos['prftipo'] = null;
    }

    /**
     * @param array $whereAdicional, desconsiderar.
     * @inheritdoc
     */
    public function carregarUltimo(array $whereAdicional = array())
    {
        $whereAdicional = array('prftipo' => $this->arAtributos['prftipo']);
        return parent::carregarUltimo($whereAdicional);
    }

    /**
     * @param array $whereAdicional, desconsiderar.
     * @inheritdoc
     */
    public function carregarAtual(array $whereAdicional = array())
    {
        $whereAdicional = array('prftipo' => $this->arAtributos['prftipo']);
        return parent::carregarAtual($whereAdicional);
    }
}
