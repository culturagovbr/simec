<?php

/**
 * Modelo de Respons�vel
 * @author Alysson Rafael
 */
class Responsavel extends Modelo{
	
	
	/**
	 * Nome da tabela especificada
	 * @var string
	 * @access protected
	 */
	protected $stNomeTabela = "siape.vwservidorativo";

	/**
     * M�todo respons�vel por carregar um respons�vel pela matr�cula
     * @name carregaResponsavel
     * @author Alysson Rafael
     * @access public
     * @param int $nu_matricula_siape - Matr�cula a ser pesquisada
     * @return array
     */
	function carregaResponsavel($nu_matricula_siape){
		
		$nu_matricula_siape = addslashes($nu_matricula_siape);
		
		$sql = "SELECT no_servidor,co_uorg_lotacao_servidor FROM $this->stNomeTabela WHERE nu_matricula_siape='".$nu_matricula_siape."' ";
		$resultado = $this->pegaLinha($sql);
		
		return $resultado;
		
	}
	
}