<?php
// topo
include_once APPSAP . 'modulos/principal/bensmovimentacao/partialsControl/topo.inc';

//caso a requisi��o seja para gravar a movimenta��o
if($_POST['requisicao'] == 'salvar' && !empty($_POST['uendidatual'])){

	if(!empty($_POST['mvbid'])){
		//exclui os filhos da movimenta��o atualizando o estado na tabela rgp
		$oMovimentacao->cancelar($_POST['mvbid']);
		
		//exclui a movimenta��o
		$oMovimentacao->excluirMovimentacao($_POST['mvbid']);
	}
	
		
	//recupera os rgps do endere�o selecionado
	$rgps = $oRgp->rgpsPorEnderecoUnidade($_POST['uendidatual'],4);
		
	//verifica se retornou rgp
	if(is_array($rgps) && count($rgps) > 0){
			
		//grava a movimenta��o
		$resultado = $oMovimentacao->salvarMovimentacao($_POST);
			
		//grava os filhos da movimenta��o
		$oBensMovimentacaoRgp->salvar($rgps,$resultado[1]);

		//coloca o novo id de movimenta��o no array de post
		$_POST['mvbid'] = $resultado[1];
		
		//pega a qtd de rgps da movimenta��o em quest�o
		$_POST['qtd_rgps'] = $oBensMovimentacaoRgp->verificaExistenciaBens($_POST['mvbid']);
	}
	else{
		alerta('N�o existem bens no endere�o selecionado.');
	}
       	
    extract($_POST);
	
}
//caso a requisi��o seja para concluir a movimenta��o
else if($_POST['requisicao'] == 'concluir'){
	
	//pega a qtd de rgps da movimenta��o em quest�o
	$qtd = $oBensMovimentacaoRgp->verificaExistenciaBens($_POST['mvbid']);
	
	if($qtd > 0){
		
		//pega os dados da movimenta��o
		$dadosMovimentacao = $oMovimentacao->pegarMovimentacaoLocalidade(array('mvbid'=>$_POST['mvbid']));
				
		
		//grava o termo
		$_POST['ternum'] = $oTermo->gerarTermo('MV',$dadosMovimentacao['ternum']);
		$_POST['ternumanterior'] = $dadosMovimentacao['ternum'];
		
	
		//atualiza o registro da movimenta��o
		$resultado = $oMovimentacao->concluirMovimentacao($_POST);
		
		if($resultado[0]){
			direcionar('?modulo=principal/bensmovimentacao/pesquisar&acao=A&mvbid='.$resultado[1].'&mvbtipo=L','Opera��o realizada com sucesso!');
		}
		else if(!$resultado[0]){
			direcionar('?modulo=principal/bensmovimentacao/pesquisar&acao=A','Opera��o falhou!');
		}
	}
	else{
		alerta('A movimenta��o n�o p�de ser conclu�da pois n�o existem bens relacionados ao processo');
		extract($_POST);
	}
}
else if($_POST['requisicao'] == 'cancelar'){
	
	if(!empty($_POST['mvbid'])){
		//exclui os filhos da movimenta��o atualizando o estado na tabela rgp
		$oMovimentacao->cancelar($_POST['mvbid']);
	
		//exclui a movimenta��o
		$oMovimentacao->excluirMovimentacao($_POST['mvbid']);
	}
	
	direcionar('?modulo=principal/bensmovimentacao/pesquisar&acao=A','Opera��o realizada com sucesso!');
	
}
//caso a requisi��o seja via url
else if(!empty($_GET['mvbid'])){
	
	//recupera os dados da movimenta��o
	$resultado = $oMovimentacao->pegarMovimentacaoLocalidade($_GET);
	if(is_array($resultado)){
		extract($resultado);
	}
	
	//pega a qtd de rgps da movimenta��o em quest�o
	$qtd_rgps = $oBensMovimentacaoRgp->verificaExistenciaBens($_GET['mvbid']);
}

$pagina = 'adicionarMovimentacaoLocalidade';

$tituloTela = 'Movimenta��o de Bens por Localidade';
$subTituloTela = 'Dados do Processo de Movimenta��o';

// monta cabe�alho do sistema
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$arMnuid = array(ABA_EDITAR_MOVIMENTACAO_GERAL,ABA_EDITAR_MOVIMENTACAO_LOCALIDADE,ABA_EDITAR_MOVIMENTACAO_SIAPE);
$db->cria_aba( $abacod_tela, $url, '', $arMnuid );

// monta o t�tulo da tela
monta_titulo($tituloTela, $subTituloTela);

// view
include_once APPSAP . 'modulos/principal/bensmovimentacao/views/formMovimentacaoLocalidade.inc';