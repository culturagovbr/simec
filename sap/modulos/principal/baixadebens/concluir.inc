<?php

if($_POST['requisicao'] == 'concluir'){
	
	$oBensBaixa->atualizarSemHistorico($_POST);
	
	extract($_GET);
	
	if(!empty($bbrid)){
	
		$baixaRgp = $oBensBaixaRgp->pegarRegistro($bbrid);
		$bebid = $baixaRgp['bebid'];
	
	}	
	
	// verifica se h� itens na baixa
	if(!empty($bebid) && $oBensBaixaRgp->validaTotalBaixas($bebid)){
		
		$tesid = $_POST['tesid'];
		
		//verifica se a Situa��o de todos os RGPs relacionados ao processo � Em Processo de Baixa
		$erroSituacao = false;
		$rgps = $oBensBaixaRgp->pegarRpgsDaBaixa($bebid);
		
		foreach($rgps as $key => $value){
			$sbmid = $oRgp->pegarSituacao($value);
			if($sbmid != SBM_BAIXA && $sbmid != SBM_BAIXADO){
				$erroSituacao = true;
				break;
			}
		}
		//caso todos estejam em processo de baixa
		if(!$erroSituacao){
			
			//se o tipo de sa�da for diferente de doa��o confirma e salva
			if($tesid != TES_SAIDA_DOACAO){
				
				$_SESSION['just'] = $_POST['bhsjustificativa'];
				//apresenta confirma��o para o usu�rio
				echo '<script>window.location = "?modulo=principal/baixadebens/confirm&acao=A&urlok=editar&urlcancel=pesquisar&bebid='.$bebid.'"</script>';

			//se o tipo de sa�da for doa��o
			}else if($tesid == TES_SAIDA_DOACAO){
				
				//verifica se a Situa��o de todos os RGPs relacionados ao processo � Inserv�vel
				reset($rgps);
				$erroEstadoConservacao = false;
				foreach($rgps as $key => $value){
					$ecoid = $oRgp->pegaEstadoConservacao($value);
					if($ecoid != ECO_INSERVIVEL){
						$erroEstadoConservacao = true;
						break;
					}
				}
				//caso todos sejam inserv�veis confirma e salva
				if(!$erroEstadoConservacao){

					$_SESSION['just'] = $_POST['bhsjustificativa'];
					//apresenta confirma��o para o usu�rio					
					echo '<script>window.location = "?modulo=principal/baixadebens/confirm&acao=A&urlok=editar&urlcancel=pesquisar&bebid='.$bebid.'"</script>';

				}
				else{
					alert('O desfazimento n�o p�de ser conclu�do pois existem RGPs associados ao processo com o estado de conserva��o diferente de Inserv�vel.');
				}
			}
		}
		else{
			alert('O desfazimento n�o p�de ser conclu�do pois existem RGPs associados ao processo com a situa��o diferente de Em Processo de Baixa.');
		}
	}else{
		
		alert('O desfazimento n�o p�de ser conclu�do pois n�o existem RGPs associados ao processo.');
		
	}
	
}
else if($_GET['confirmado'] == 'S'){
	$oBensBaixa->ativar($_GET['bebid']);
	direcionar('?modulo=principal/baixadebens/pesquisar&acao=A&bebid=' . $_GET['bebid']);
}

