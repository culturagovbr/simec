<?php
// topo
include_once APPSAP . 'modulos/principal/fornecedor/partialsControl/topo.inc';

// monta o t�tulo da tela
monta_titulo('Fornecedor','');

//caso tenha postado o formul�rio
if($_SERVER['REQUEST_METHOD']=="POST" && $_POST['requisicao'] == 'salvar'){
	
	//verifica se j� existe registro com o documento informado
	$repetido = $oFornecedor->verificaUnico($_POST['forcpfcnpj']);
		
	//caso n�o encontre repetido, salva
	if(!$repetido){
		$resultado = $oFornecedor->salvarFornecedor($_POST);
	}
	else{
		alerta('J� existe um Fornecedor cadastrado com o documento informado.');
		extract($_POST);
	}
}


// view
include_once APPSAP . 'modulos/principal/fornecedor/views/formDefault.inc';

//fazendo este teste aqui pq o javascript � inclu�do no form
if($_SERVER['REQUEST_METHOD']=="POST" && $_POST['requisicao'] == 'salvar'){
	if($resultado[0]){
		executarScriptPai("retornaDadosFornecedor(\"".$_POST['forcpfcnpj']."\",\"".$_POST['forrazaosocial']."\")");
		alerta('Opera��o realizada com sucesso!');
		fecharPopup();
		die;
	}
	else if(!$resultado[0] && !$repetido){
		alerta('Opera��o Falhou!');
		die;
	}
}