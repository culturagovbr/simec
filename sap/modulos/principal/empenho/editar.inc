<?php
// topo
include_once APPSAP . 'modulos/principal/bens/partialsControl/topo.inc';

//caso tenha postado o formul�rio
if($_SERVER['REQUEST_METHOD']=="POST" && $_POST['requisicao'] == 'salvar'){
	if($oEmpenho->verificaEmpenhoRepetido($_POST)){
		alerta('J� existe registro do Empenho informado.');
		extract($_POST);
	}
	else{
		
		$arrValidacao = $oEmpenho->validaEdicao($_POST);
		if($arrValidacao[0]){
			alerta('A data deve ser a mesma ou anterior � dos registros de entrada.');
			echo "<script>window.location='?modulo=principal/empenho/editar&acao=A&empid=".$_POST['empid']."'</script>";
			exit;
			//extract($_POST);
		}
		else if($arrValidacao[1]){
			alerta('O valor para Material Permanente deve ser igual ou superior � soma do Valor do Documento de todos os processos de Entrada relacionados.');
			echo "<script>window.location='?modulo=principal/empenho/editar&acao=A&empid=".$_POST['empid']."'</script>";
			exit;
			//extract($_POST);
		}
		else{
			$resultado = $oEmpenho->salvarEmpenho($_POST);
			if($resultado[0]){
				direcionar('?modulo=principal/empenho/'.$resultado[2].'&acao=A&empid='.$resultado[1],'Opera��o realizada com sucesso!');
			}
			else if(!$resultado[0]){
				direcionar('?modulo=principal/empenho/'.$resultado[2].'&acao=A&empid='.$resultado[1],'Opera��o falhou!');
			}
		}
		
		
	}
	
	
}
//caso seja via url
else if($_SERVER['REQUEST_METHOD']=="GET" && !empty($_GET['empid'])){
	$Empenho = $oEmpenho->carregaEmpenhoPorId($_GET['empid']);
	extract($Empenho);
	
	$empdata = formata_data($empdata);
	
	//verifica se o empenho est� sendo usado por alguma
	//entrada de bens. caso esteja, bloqueia os campos de valor
	$emUso = $oEmpenho->verificaEmpenhoEmUso($_GET['empid']);
}

if($_GET['popup'] != 'S'){
	// monta cabe�alho do sistema
	include_once APPRAIZ . 'includes/cabecalho.inc';
	print '<br/>';
}

$arMnuid = array();
$db->cria_aba( $abacod_tela, $url, '', $arMnuid );

// monta o t�tulo da tela
monta_titulo('Nota de Empenho','');

// view
include_once APPSAP . 'modulos/principal/empenho/views/formDefault.inc';
