<?php
// topo
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/tipoDocumento/partialsControl/topo.inc';

// se requisicao for para salvar
if($_POST['requisicao'] == 'salvar'){

	// verifica se registro ja existe
	if($oTipoDocumento->existe($_POST)){
		
		alerta('Registro j� existe.');
		extract($_POST);
		
	}else{
		
		// atualiza
		if($oTipoDocumento->atualizar($_POST))
			direcionar('?modulo=sistema/tabelasdeapoio/tipoDocumento/editar&acao=A&tdcid='. $_POST['tdcid'], 'Opera��o realizada com sucesso.');
		else
			alerta('Erro.');
			
	}
	
}else if(!empty($_GET['tdcid'])){

	$dados = $oTipoDocumento->pegarRegistro($_GET['tdcid']);
	extract($dados);
	
}

// monta cabe�alho do sistema
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$db->cria_aba( $abacod_tela, $url, '', array());

// monta o t�tulo da tela
monta_titulo('Tipo de Documento', '');

// view
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/tipoDocumento/views/formDefault.inc';
