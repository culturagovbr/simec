<?php
// topo
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/situacaoBem/partialsControl/topo.inc';

// se requisicao for para salvar
if($_POST['requisicao'] == 'salvar'){
	
	// verifica se registro ja exite
	if($oSituacaoBem->existe($_POST)){
		
		alerta('Registro j� existe.');
		extract($_POST);
		
	}else{
		
		// salva
		if($oSituacaoBem->salvar($_POST))
			direcionar('?modulo=sistema/tabelasdeapoio/situacaoBem/adicionar&acao=A', 'Opera��o realizada com sucesso.');
		else
			alerta('Erro.');
			
	}
	
}

// monta cabe�alho do sistema
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

// monta o t�tulo da tela
monta_titulo('Situa&ccedil;&atilde;o do Bem', '');

// view
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/situacaoBem/views/formDefault.inc';
