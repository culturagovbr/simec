<?php
// topo
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/catalogo/partialsControl/topo.inc';

//caso tenha postado o formul�rio
if($_SERVER['REQUEST_METHOD']=="POST" && $_POST['requisicao'] == 'salvar'){
			
	//veririca se existe no banco um registro com a combina��o
	//catcodclasse+ccbid+icbid, pois n�o poder� repetir
	$uniqueCatalogo = $oCatalogo->verificaUniqueCatalogo($_POST);
		
	//caso n�o encontre registro com a descri��o informada
	if(!$uniqueCatalogo){
		$resultado = $oCatalogo->salvarCatalogo($_POST);
        if($resultado[0]){
        	direcionar('?modulo=sistema/tabelasdeapoio/catalogo/'.$resultado[2].'&acao=A','Opera��o realizada com sucesso!');
        }
        else if(!$resultado[0]){
        	direcionar('?modulo=sistema/tabelasdeapoio/catalogo/'.$resultado[2].'&acao=A','Opera��o falhou!');
        }
	}
	else if($uniqueCatalogo){
		alerta('J� existe registro do Cat�logo informado.');
		extract($_POST);
	}
}

// monta cabe�alho do sistema
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$arMnuid = array( ABA_EDITAR_CATALOGO );
$db->cria_aba( $abacod_tela, $url, '', $arMnuid);

// monta o t�tulo da tela
monta_titulo('Cat&aacute;logo','');

// view
include_once APPSAP . 'modulos/sistema/tabelasdeapoio/catalogo/views/formDefault.inc';
