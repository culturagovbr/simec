<?php
// incluindo as classes necessárias
$oTipoPeca = new TipoPeca();

//caso tenha postado o formulário
if($_POST['requisicao'] == 'salvar'){
	
	//verifica se já existe o registro
	$repetido = $oTipoPeca->verifica_unico($_POST);
	
	//caso não encontre repetido, salva
	if(!$repetido){
		$resultado = $oTipoPeca->salvar($_POST);
		
		if($resultado[0]){
			$msg = 'Opera��o realizada com sucesso!';
			direcionar('?modulo=principal/tipopecapublicitaria/'.$resultado[2].'&acao=A&id='.$resultado[1],$msg);
		}
		else if(!$resultado[0]){
			$msg = 'Opera��o falhou!';
			direcionar('?modulo=principal/tipopecapublicitaria/'.$resultado[2].'&acao=A&id='.$resultado[1],$msg);
		}
	}
	else{
		$msg = 'Tipo de Pe�a Publicit�ria j� Cadastrado!';
		alerta($msg);
		extract($_POST);
	}
}
//caso seja via url
else if(!empty($_GET['id'])){
	$tipopeca = $oTipoPeca->carrega_registro_por_id($_GET['id']);
	extract($tipopeca);
}

// monta cabeçalho do sistema
include_once APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

$arMnuid = array();
$db->cria_aba($abacod_tela, $url, '', $arMnuid);

// monta o título da tela
monta_titulo('Tipo de Pe�a Publicit�ria','');
?>
<link type="text/css" rel="stylesheet" href="./css/default.css" />

<script language='javascript' type='text/javascript' src='../includes/prototype.js'></script>
<script language='javascript' type='text/javascript' src='./js/default.js'></script>
<script language='javascript' type='text/javascript' src='./js/ajax.js'></script>
<script language='javascript' type='text/javascript'>
	/**
	 * Redireciona para a pesquisa
	 * @name voltar
	 * @return void
	 */
	function voltar(){
		window.location = '?modulo=principal/tipopecapublicitaria/listaTipoPecaPublicitaria&acao=A';
	}
	
	/**
	 * Submete o formulário
	 * @name salvar
	 * @return void
	 */	
	function salvar(){
		$('btnSalvar').disable();
		if(validaFormularioCadastro()){
			$('formularioCadastro').submit();
		}
	}
	
	/**
	 * Valida os campos obrigat�rios
	 * @name validaFormularioCadastro
	 * @return bool
	 */	
	function validaFormularioCadastro(){
	
		if($('tppdsc').getValue() == '') {
			alert('Preencher campo obrigat�rio!');
			$('tppdsc').focus();
			$('btnSalvar').enable();
	        return false;
		}
	
		return true;
	}
</script>

<form name="formularioCadastro" id="formularioCadastro" method="post">
	<table class="tabela" align="center" border="0" cellpadding="5" cellspacing="1">
		<tr>
			<td class="SubtituloDireita"> Tipo de Pe�a Publicit�ria: </td>
			<td class="campo">
				<?=campo_texto('tppdsc','S','S','Informe o Tipo de Pe�a Publicit�ria',100,255,'','','left','',0,'id="tppdsc"');?>
            </td>
		</tr>
		<tr>
			<td class="SubtituloDireita"> Possui Per�odo de Veicula��o: </td>
			<td class="campo">
				<input type='radio' name='tppperiodo' id='tppperiodo' value='S' <?php if($tppperiodo == 'S'){ echo 'checked'; } ?>>Sim
				<input type='radio' name='tppperiodo' id='tppperiodo' value='N' <?php if($tppperiodo == 'N' || empty($tppperiodo)){ echo 'checked'; } ?>>N�o
            </td>
		</tr>
		<tr class="buttons">
        	<td colspan='2' align='center'>
            	<input type='hidden' name='requisicao' id='requisicao' value='salvar'>
            	<input type='hidden' name='tppid' id='tppid' value='<?php echo $tppid; ?>'>
            	<input type='button' class="botao" name='btnSalvar' id='btnSalvar' value='Salvar' onclick="javascript:salvar();">
				<input type='button' class="botao" name='btnVoltar' id='btnVoltar' value='Voltar' onclick="javascript:voltar();">
            </td>
        </tr>
	</table>
</form>