<?php
    
    $_SESSION['gestao']['Aba'] = 'PRORROGACAO';
    
    if($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
    }
    
    $get_orcid = $_REQUEST['orcid'];
    $get_tpsid = $_REQUEST['tpsid'];
    
    if($_GET['cprid'] != ''){
        $cprid = $_GET['cprid'];
        $dadosServ = dadosServidor( $cprid );

        #Est�gio Probat�rio:
        if($dadosServ['cprsitestagio'] == 't'){
            $checkSim_est = 'checked = "checked"';
        }else{
            $checkNao_est = 'checked = "checked"';
        }
        #Sindic�ncia e PAD:
        if($dadosServ['cprsindpad'] == 't'){
            $checkSim_sind = 'checked = "checked"';
        }else{
            $checkNao_sind = 'checked = "checked"'; 
        }
        #Dedica��o Exclusiva:
        if($dadosServ['cprsitdedicacao'] == 't'){
            $checkSim_ded = 'checked = "checked"';
        }else{
            $checkNao_ded = 'checked = "checked"';
        }
        #Mudan�a de Prazo: 	
        if($dadosServ['cprsitmudanca'] == 't'){
            $checkSim_mud = 'checked = "checked"';
        }else{
            $checkNao_mud = 'checked = "checked"';
        }
        #Perda de Prazo: 	
        if($dadosServ['cprsitperdaprazo'] == 't'){
            $checkSim_pra = 'checked = "checked"';
        }else{
            $checkNao_pra = 'checked = "checked"';
        }
        
        if($dadosServ['columncprprazoind'] == 't'){
            $checkSim_ind = 'checked = "checked"';
        }else{
            $checkSim_ind = '';
        }
        
        if($dadosServ['columncprprazoano'] == 't'){
            $checkSim_ano = 'checked = "checked"';
        }else{
            $checkSim_ano = '';
        }
        
        if($dadosServ['columncprprazodata'] != ''){
            $checkSim_dat = 'checked = "checked"';
        }else{
            $checkSim_dat = '';
        }
    }
    
    if($_REQUEST['cadastro'] == 'S' && $_REQUEST['nu_matricula_siape'] != ''){
        $dados['nu_matricula_siape']= $_REQUEST['nu_matricula_siape'];
        $dados['tipo']  = 'C';
        $dadosServ      = buscarDadosServidor( $dados );
    }
    
    include APPRAIZ . "includes/cabecalho.inc";
    echo "<br>";
    monta_titulo('Gest�o de Pessoas', '<b>PRORROGA��O - Cadastro de Prorroga��o</b>');
    echo "<br>";
    
    $abacod_tela    = ABA_CAD_PRORROGACAO_SERVIDOR;
    $url            = 'gestaopessoa.php?modulo=principal/cessao_prorrogacao/cad_dados_prorrogacao&acao=A';
    $parametros     = '';
    
    $db->cria_aba($abacod_tela, $url, $parametros);
    
?>

<link href="../includes/JsLibrary/date/displaycalendar/displayCalendar.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript" src="../includes/funcoes.js"></script>
<script type="text/javascript" src="../includes/JQuery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/dateFunctions.js"></script>
<script type="text/javascript" src="../includes/JsLibrary/date/displaycalendar/displayCalendar.js"></script>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#salvarProcesso').click(function(){
            var orcid                   = $('#orcid');
            var cprnumprocesso          = $('#cprnumprocesso');
            var tpsid                   = $('#tpsid');
            var ogcid                   = $('#ogcid');
            var cprfuncaodesempenhada   = $('#cprfuncaodesempenhada');
            var cprcodigosimbolo        = $('#cprcodigosimbolo');
            var tonid                   = $('#tonid');
            var cprsitestagio           = $('input:radio[name=cprsitestagio]:checked');
            var cprsindpad              = $('input:radio[name=cprsindpad]:checked');
            var cprsitdedicacao         = $('input:radio[name=cprsitdedicacao]:checked');
            var cprdtinlusao            = $('#cprdtinlusao');
            var aimid                   = $('#aimid');
            
            var columncprprazo          = $('input:radio[name=columncprprazo]:checked');
            var columncprprazodata      = $('#columncprprazodata');
            
            var erro;

            if(!orcid.val()){
                alert('O campo "Unidade de Cadastro" � um campo obrigat�rio!');
                orcid.focus();
                erro = 1;
                return false;
            }
            if(!aimid.val()){
                alert('O campo "Unidade de MEC" � um campo obrigat�rio!');
                aimid.focus();
                erro = 1;
                return false;
            }
            if(!cprnumprocesso.val()){
                alert('O campo "O N� do Processo" � um campo obrigat�rio!');
                cprnumprocesso.focus();
                erro = 1;
                return false;
            }
            if(!tpsid.val()){
                alert('O campo "Tipo de Solicita��o" � um campo obrigat�rio!');
                tpsid.focus();
                erro = 1;
                return false;
            }
            if(!ogcid.val()){
                alert('O campo "Org�o de Cession�rio" � um campo obrigat�rio!');
                ogcid.focus();
                erro = 1;
                return false;
            }
            if(!cprfuncaodesempenhada.val()){
                alert('O campo "Fun��o a ser Desempenhada" � um campo obrigat�rio!');
                cprfuncaodesempenhada.focus();
                erro = 1;
                return false;
            }
            if( !columncprprazo.val() ){
                alert('O campo "Prazo" � um campo obrigat�rio!');
                columncprprazo.focus();
                erro = 1;
                return false;
            }
            if( columncprprazo.val() == 'D' && !columncprprazodata.val() ){
                alert('O campo "Prazo - Data" � um campo obrigat�rio!');
                columncprprazodata.focus();
                erro = 1;
                return false;
            }
            if(!cprcodigosimbolo.val()){
                alert('O campo "C�digo/Simbolo" � um campo obrigat�rio!');
                cprcodigosimbolo.focus();
                erro = 1;
                return false;
            } 
            if(!tonid.val()){
                alert('O campo "�nus" � um campo obrigat�rio!');
                tonid.focus();
                erro = 1;
                return false;
            } 
            if(!cprsitestagio.val()){
                alert('O campo "Est�gio Probat�rio" � um campo obrigat�rio!');
                cprsitestagio.focus();
                erro = 1;
                return false;
            } 
            if(!cprsindpad.val()){
                alert('O campo "Sindic�ncia e PAD" � um campo obrigat�rio!');
                cprsindpad.focus();
                erro = 1;
                return false;
            } 
            if(!cprsitdedicacao.val()){
                alert('O campo "Dedica��o Exclusiva" � um campo obrigat�rio!');
                cprsitdedicacao.focus();
                erro = 1;
                return false;
            } 
            if(!cprdtinlusao.val()){
                alert('O campo "Data do Lan�amento" � um campo obrigat�rio!');
                cprdtinlusao.focus();
                erro = 1;
                return false;
            } 
            
            if(!erro){
                $('#requisicao').val('salvarDadosProrrogacao');
                $('#formulario').submit();
            }
        });
    });
    
    function abrirPopUpCadServidorSiape( nu_cpf ){
        var cprid = $('#cprid').val();
        var busca;
        if(nu_cpf != ''){
            busca = '&busca=S&nu_cpf='+nu_cpf;
        }else{
            busca = '';
        }
        window.open('gestaopessoa.php?modulo=principal/cessao_prorrogacao/cad_dados_servidor_siape&acao=A&cprid='+cprid+busca,'','toolbar=no, location=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=850, height=500');
    }   
    
    function abrirPopUpServidor(){
        var nu_cpf = replaceAll( replaceAll( $('#nu_cpf').val(), ".", "" ), "-", "" );
        if( nu_cpf == ''){
            alert('Para ver detalhes do Servidor � necess�rio digitar o CPF.');
            $('#nu_cpf').focus();
            return false;
        }
        window.open('gestaopessoa.php?modulo=principal/cadDadosPessoais&acao=A&pop=sim&fdpcpf='+nu_cpf,'','toolbar=no, location=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=900, height=600');
    }
    
    function atualizaComboUndCad( tpsid ){ 
        $.ajax({
            type    : "POST",
            url     : window.location,
            data    : "requisicao=atualizaComboUndCad&tpsid="+tpsid,
            async: false,
            success: function(resp){
                $('#unidadeCadastro').html(resp);
            }
        });
    }    
    
    function buscarDadosServidor( nu_matricula_siape, nu_cpf ){
    
        var param;
        if(nu_matricula_siape != ''){
            param = 'nu_matricula_siape='+nu_matricula_siape;
        }else{
            param = 'nu_cpf='+nu_cpf;
        }
        
        $.ajax({
            type    : "POST",
            url     : window.location,
            data    : "requisicao=buscarDadosServidor&"+param,
            //asynchronous: false,
            success: function(resp){
                var dados = $.parseJSON(resp);             
                
                if(dados.nu_matricula_siape != '0'){
                    $('#nu_cpf').val(dados.nu_cpf);
                    $('#no_servidor').val(dados.no_servidor);
                    $('#co_orgao').val(dados.co_orgao);
                    $('#co_situacao_servidor').val(dados.co_situacao_servidor);
                    $('#co_funcao').val(dados.co_funcao);
                    $('#co_cargo_emprego').val(dados.co_cargo_emprego);
                }else{
                    $('#nu_cpf').val('');
                    $('#no_servidor').val('');
                    $('#co_orgao').val('');
                    $('#co_situacao_servidor').val('');
                    $('#co_funcao').val('');
                    $('#co_cargo_emprego').val('');
                    $('#addServidor').attr('disabled', false);
                    alert('O SIAPE n�o foi encontrado em nossa base de dados, se n�o foi digitado de forma incorreta � necessario cadastr�-lo!');
                }
            }
        });
    }
    
    function mascaraPersonalizada( numContrato ){
        var numContrato = replaceAll( replaceAll( replaceAll( numContrato, ".", ""), "-", ""), "/","") ;
        var tamanho = numContrato.length;
        var contrato;
        
        if(numContrato != ''){
            if( tamanho == 12 ){
                contrato = mascaraglobal('######.####-##', numContrato);
            }else if( tamanho == 17 ){
                contrato = mascaraglobal('#####.######/####-##', numContrato);
            }else{
                alert('N�mero de Contrato Ivalido, verifique o n�mero digitado!');
            }
        }
        $('#cprnumprocesso').val(contrato);
    }
    
    function voltarListaProrrogacao(){
        var cprid = $('#cprid').val();
        var orcid = $('#orcid').val();
        
        window.location.href = 'gestaopessoa.php?modulo=principal/cessao_prorrogacao/lista_busca_prorrogacao&acao=A&cprid='+cprid;//+'&orcid='+orcid;
    }
    
    function verificarDuplicidadeCPF( nu_cpf ){
        if(nu_cpf != ''){
            var valido = validar_cpf( nu_cpf );
        
            if(!valido){
                alert( "CPF inv�lido! Favor informar um CPF v�lido!" );
                $('#nu_cpf').focus();
                return false;
            }else{
               $.ajax({
                    type    : "POST",
                    url     : window.location,
                    data    : "requisicao=verificarDuplicidadeCPF&nu_cpf="+nu_cpf,
                    //asynchronous: false,
                    success: function(resp){
                        var dados = $.parseJSON(resp);
                        var qtd  = dados.qtd_cpf;

                        if( qtd == 0 ){
                            $('#nu_matricula_siape').val('');
                            $('#nu_cpf').val('');
                            $('#no_servidor').val('');
                            $('#co_orgao').val('');
                            $('#co_situacao_servidor').val('');
                            $('#co_funcao').val('');
                            $('#co_cargo_emprego').val('');
                            $('#addServidor').attr('disabled', false);
                            alert('O CPF digitado n�o foi encontrado em nossa base de dados � necess�rio cadastra-lo!');
                        }

                        if(qtd == 1){
                            buscarDadosServidor( '', nu_cpf );
                        }

                        if(qtd > 1){
                            abrirPopUpCadServidorSiape(nu_cpf);
                        }
                    }
                });
            }
        }
    }
    
</script>

<form action="" method="POST" id="formulario" name="formulario">
<input type="hidden" id="requisicao" name="requisicao" value=""/>
<input type="hidden" id="cprid" name="cprid" value="<?=$dadosServ['cprid'];?>"/>
 
    <table align="center" bgcolor="#f5f5f5" border="0" class="tabela" cellpadding="3" cellspacing="1">
        <tr>
            <td class ="SubTituloCentro" style="text-align: left;" colspan="3" >Dados do Servidor</td>
            <!-- COLUNA DO WORKFLOW -->
            <td width="25%" rowspan="20" align="center" class ="SubTituloCentro">
                <?php
                    require_once APPRAIZ . "includes/workflow.php";

                    if($cprid != ''){
                        $docid = buscarDocidCessaoProrrogacao( $cprid );

                        if($docid != ''){
                            $dados_wf = array("cprid" => $cprid);
                            wf_desenhaBarraNavegacao($docid, $dados_wf);
                        }
                    }
                ?>
            </td>
            <!-- FIM COLUNA DO WORKFLOW -->
        </tr>
        <!--INICIO INFORMA��O SERVIDOR-->
        <tr>
            <td class ="SubTituloDireita" width="30%">C�digo SIAPE:</td>
            <td width="15%">
                <?php
                    if($_GET['siape']){
                        $nu_matricula_siape = $_GET['siape'];
                        echo '<script>buscarDadosServidor('.$_GET['siape'].', \'\')</script>';
                    }else{
                        $nu_matricula_siape = $dadosServ['nu_matricula_siape'];
                    }
                    echo campo_texto('nu_matricula_siape', 'S', $habilitado['hab'], '', 24, 7, '#######', '', '', '', 0, 'id="nu_matricula_siape"', '', $nu_matricula_siape, 'buscarDadosServidor(this.value, \'\')', null); 
                ?>
            </td>
            <td width="65%">
                <input type="button" name="addServidor" id="addServidor" onclick="abrirPopUpCadServidorSiape('');" value="Adicionar Servidor" title="Adicionar novo Servidor" disabled="disabled">
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">CPF:</td>
            <td>
                <?php
                    $nu_cpf = $dadosServ['nu_cpf'];
                    echo campo_texto('nu_cpf', 'S', $habilitado['hab'], '', 24, 14, '###.###.###-##', '', '', '', 0, 'id="nu_cpf"', '', $nu_cpf, 'verificarDuplicidadeCPF(this.value)', null); 
                ?>
            </td>
            <td width="65%">
                <!--input type="button" name="detServidor" id="detServidor" onclick="abrirPopUpServidor();" value="Detalhes Servidor" title="Consultar Detalhes do Servidor" disabled="disabled"-->
                <input type="button" name="detServidor" id="detServidor" value="Detalhes Servidor" title="Consultar Detalhes do Servidor" disabled="disabled">
                </span>
            </td>
        </tr>        
        <tr>
            <td class ="SubTituloDireita">Nome:</td>
            <td colspan="2">
                <?php
                    $no_servidor = trim($dadosServ['no_servidor']);
                    echo campo_texto('no_servidor', 'N', 'N', '', 45, 16, '', '', '', '', 0, 'id="no_servidor"', '', $no_servidor, null, null); 
                ?>
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Sigla/Org�o:</td>
            <td colspan="2">
                <?php
                    $co_orgao = $dadosServ['co_orgao'];
                    $sql = "
                        SELECT  co_orgao AS codigo, 
                                --trim(sg_orgao) ||' - '||trim(ds_orgao) AS descricao
                                trim(ds_orgao) AS descricao
                        FROM siape.tb_simec_orgao
                    ";
                    $db->monta_combo('co_orgao', $sql, 'N', 'Selecione...', '', '', '', '336', 'S', 'co_orgao', false, $co_orgao, null);
                ?>	
            </td>
        </tr>
        <tr> 
            <td class ="SubTituloDireita">Situa��o:</td>
            <td colspan="2">
                <?php
                    $co_situacao_servidor = $dadosServ['co_situacao_servidor'];
                    $sql = "
                        SELECT  co_situacao_servidor AS codigo, 
                                ds_situacao_servidor AS descricao
                        FROM siape.tb_simec_situacao_servidor
                    ";
                    $db->monta_combo("co_situacao_servidor", $sql, 'N', 'Selecione...', '', '', '', '336', 'S', 'co_situacao_servidor', false, $co_situacao_servidor, null);
                ?>	
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Fun��o:</td>
            <td colspan="2">
                <?php
                    $co_funcao = $dadosServ['co_funcao'];
                    $sql = "
                        SELECT  co_funcao AS codigo, 
                                ds_funcao AS descricao
                        FROM siape.tb_siape_funcao
                    ";
                    $db->monta_combo("co_funcao", $sql, 'N', 'Selecione...', '', '', '', '336', 'S', 'co_funcao', false, $co_funcao, null);
                ?>
            </td>
        </tr>
        <tr> 
            <td class ="SubTituloDireita">Cargo:</td>
            <td colspan="2">
                <?php
                    $co_cargo_emprego = $dadosServ['co_cargo_emprego'];
                    $sql = "
                        SELECT  co_cargo_emprego AS codigo, 
                                ds_cargo_emprego AS descricao
                        FROM siape.tb_simec_cargo_emprego
                    ";
                    $db->monta_combo("co_cargo_emprego", $sql, 'N', 'Selecione...', '', '', '', '336', 'S', 'co_cargo_emprego', false, $co_cargo_emprego, null);
                ?>	
            </td>
        </tr>
        <!--FIM IMFORMA��O SERVIDOR-->
        <tr><td class ="SubTituloCentro" style="text-align: left;" colspan="3">Informa��o da Cess�o/Prorroga��o</td></tr>
        <tr>
            <td class ="SubTituloDireita">Tipo de Solicita��o:</td>
            <td colspan="2">
                <?php
                    if($get_tpsid == ''){
                        $tpsid = $dadosServ['tpsid'];
                    }else{
                        $tpsid = $get_tpsid;
                    }
                    
                    $sql = "
                        SELECT  tpsid AS codigo, 
                                tpsdsc AS descricao
                        FROM gestaopessoa.tiposolicitacao
                        WHERE tpsid IN (1, 2)
                    ";
                    $db->monta_combo("tpsid", $sql, 'S', 'Selecione...', '', '', '', '336', 'S', 'tpsid', false, $tpsid, null);
                ?>	
            </td>
        </tr>
        <tr> 
            <td class ="SubTituloDireita">Unidade de Cadastro:</td>
            <td colspan="2"  id="unidadeCadastro">
                <?php
                    if($get_orcid == ''){
                        $orcid = $dadosServ['orcid'] ;
                    }else{
                        $orcid = $get_orcid;
                    }

                    $sql = "
                        SELECT  orcid AS codigo, 
                                orcdsc AS descricao
                        FROM gestaopessoa.origemcessao
                        WHERE orcid in (1, 3)
                        order by orcdsc
                    ";                    
                    $db->monta_combo("orcid", $sql, 'S', 'Selecione...', '', '', '', '336', 'S', 'orcid', false, $orcid, null);
                ?>	
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Unidade MEC:</td>
            <td colspan="2">
                <?PHP
                    $aimid = $dadosServ['aimid'];   
                    $sql = "
                        SELECT  aimid AS codigo, 
                                aimsigla||' - '||aimdsc AS descricao
                        FROM gestaopessoa.areamec
                        order by aimdsc
                    ";
                    $db->monta_combo("aimid", $sql, 'S', 'Selecione...', '', '', '', '336', 'S', 'aimid', false, $aimid, null);                                                                                
                ?>
            </td>
        </tr>        
        <tr>
            <td class ="SubTituloDireita">N� do processo:</td>
            <td colspan="2">
                <?php
                    $cprnumprocesso = $dadosServ['cprnumprocesso'];
                    echo campo_texto('cprnumprocesso', 'S', 'S', '', 45, 20, '', '', '', '', 0, 'id="cprnumprocesso"', '', $cprnumprocesso, 'mascaraPersonalizada(this.value)', null); 
                ?>
            </td>
        </tr>      
        <tr> 
            <td class ="SubTituloDireita">�rg�o Cession�rio:</td>
            <td colspan="2">
                <?PHP
                    $sql = "
                        SELECT  ogcid AS codigo, 
                                ogcsigla||' - '||ogcdsc AS descricao
                        FROM gestaopessoa.orgaocessionario
                        WHERE 1=1
                    ";
                    $arCampos = array(
                        array(
                            "codigo" => "ogcid",
                            "descricao" => "<b>C�digo:</b>",
                            "tipo" => "1"),
                        array(
                            "codigo" => "ogcdsc",
                            "descricao" => "<b>Descri��o:</b>",
                            "tipo" => "0")
                     );
                    
                    $ogcid = array(
                            "value" => $dadosServ['ogcid'],
                            "descricao" => $dadosServ['ogcdsc']
                    );
                    campo_popup('ogcid', $sql, 'Selecione �rg�o Cession�rio', null, '400x800', '44', $arCampos, 1, false, false, false, 'N');
                                                                                
                ?>                
                
            </td>
        </tr>       
        <tr> 
            <td class ="SubTituloDireita">Fun��o a ser Desempenhada:</td>
            <td colspan="2">
                <?php
                    $cprfuncaodesempenhada = $dadosServ['cprfuncaodesempenhada'];
                    echo campo_textarea('cprfuncaodesempenhada', 'S', 'S', '', 68, 6, '100', '', 0, '', false, null, $cprfuncaodesempenhada); 
                ?>	
            </td>
        </tr>        
        <tr>
            <td class ="SubTituloDireita">Prazo:</td>
            <td colspan="2">
                <input type="radio" name="columncprprazo" value="I" <?=$checkSim_ind?>> <b>Indeterminado</b> <br>
                <input type="radio" name="columncprprazo" value="U" <?=$checkSim_ano?>> <b>(1) Ano</b> <br>
                <input type="radio" name="columncprprazo" value="D" <?=$checkSim_dat?>>
                <?php
                    $columncprprazodata = $dadosServ['columncprprazodata'];
                    echo campo_data2('columncprprazodata','N','S','','DD/MM/YYYY','','', $columncprprazodata, '', '', 'columncprprazodata' );
                ?>
            </td>
        </tr>        
        <tr> 
            <td class ="SubTituloDireita">C�digo/Simbolo:</td>
            <td colspan="2">
                <?php
                    $cprcodigosimbolo = $dadosServ['cprcodigosimbolo'];
                    echo campo_texto('cprcodigosimbolo', 'S', 'S', '', 45, 20, '', '', '', '', 0, 'id="cprcodigosimbolo"', '', $cprcodigosimbolo, null, null);
                ?>	
            </td>
        </tr>
        <tr> 
            <td class ="SubTituloDireita">�nus:</td>
            <td colspan="2">
                <?php
                    $tonid = $dadosServ['tonid'];
                    $sql = "
                        SELECT  tonid AS codigo, 
                                tondsc AS descricao
                        FROM gestaopessoa.tipoonus
                    ";
                    $db->monta_combo("tonid", $sql, 'S', 'Selecione...', '', '', '', '336', 'S', 'tonid', false, $tonid, null);
                ?>	
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Est�gio Probat�rio:</td>
            <td colspan="2">
                <input type="radio" name="cprsitestagio" id="cprsitestagio_S" value="S" <?=$checkSim_est;?>> Sim
                <input type="radio" name="cprsitestagio" id="cprsitestagio_N" value="N" <?=$checkNao_est;?>> N�o
            </td>
        </tr>       
        <tr>
            <td class ="SubTituloDireita">Sindic�ncia e PAD:</td>
            <td colspan="2">
                <input type="radio" name="cprsindpad" id="cprsindpad_S" value="S" <?=$checkSim_sind;?>> Sim
                <input type="radio" name="cprsindpad" id="cprsindpad_N" value="N" <?=$checkNao_sind;?>> N�o
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Dedica��o Exclusiva:</td>
            <td colspan="2">
                <input type="radio" name="cprsitdedicacao" id="cprsitdedicacao_S" value="S" <?=$checkSim_ded;?>> Sim
                <input type="radio" name="cprsitdedicacao" id="cprsitdedicacao_N" value="N" <?=$checkNao_ded;?>> N�o
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Mudan�a de Cargo:</td>
            <td colspan="2">
                <input type="radio" name="cprsitmudanca" id="cprsitmudanca_S" value="S" <?=$checkSim_mud;?>> Sim
                <input type="radio" name="cprsitmudanca" id="cprsitmudanca_N" value="N" <?=$checkNao_mud;?>> N�o
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Perda de Prazo:</td>
            <td colspan="2">
                <input type="radio" name="cprsitperdaprazo" id="cprsitperdaprazo_S" value="S" <?=$checkSim_pra;?>> Sim
                <input type="radio" name="cprsitperdaprazo" id="cprsitperdaprazo_N" value="N" <?=$checkNao_pra;?>> N�o
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Data do vencimento da Prorroga��o:</td>
            <td colspan="2">
                <?php
                    $cprdtvencprorrogacao = $dadosServ['cprdtvencprorrogacao'];
                    echo campo_data2('cprdtvencprorrogacao','S','S','','DD/MM/YYYY','','', $cprdtvencprorrogacao, '', '', 'cprdtvencprorrogacao' );
                ?>
            </td>
        </tr>
        <tr>
            <td class ="SubTituloDireita">Data do Lan�amento:</td>
            <td colspan="2">
                <?php
                    $cprdtinlusao = $dadosServ['cprdtinlusao'] != '' ? $dadosServ['cprdtinlusao'] : date("d/m/Y");
                    echo campo_data2('cprdtinlusao','N','N','','DD/MM/YYYY','','', $cprdtinlusao, '', '', 'cprdtinlusao' );
                ?>
            </td>
        </tr> 
        <tr>
            <td class="SubTituloCentro" colspan="4">
                <input type="button" id="salvarProcesso" name="salvar" value="Salvar"/>
                <input type="button" id="voltar" name="voltar" value="Voltar" onclick="voltarListaProrrogacao();"/>
            </td>
        </tr>
    </table>
</form>
