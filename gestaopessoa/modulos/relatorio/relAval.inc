
<!-- Relat�rio M�dia final -->

<?PHP
    $sql = "
        --AVALIA��O COMPLETA e COM EQUIPE
        SELECT  sersiape, 
                sernome, 
                ROUND(SUM(nota)) AS nota,
                CASE
                    WHEN ROUND(SUM(nota)) <= 30 THEN '6'
                    WHEN ROUND(SUM(nota)) >= 31 AND ROUND(SUM(nota)) <= 40 THEN '8'
                    WHEN ROUND(SUM(nota)) >= 41 AND ROUND(SUM(nota)) <= 50 THEN '10'
                    WHEN ROUND(SUM(nota)) >= 51 AND ROUND(SUM(nota)) <= 60 THEN '12'
                    WHEN ROUND(SUM(nota)) >= 61 AND ROUND(SUM(nota)) <= 70 THEN '14'
                    WHEN ROUND(SUM(nota)) >= 71 AND ROUND(SUM(nota)) <= 80 THEN '16'
                    WHEN ROUND(SUM(nota)) >= 81 AND ROUND(SUM(nota)) <= 90 THEN '18'
                    WHEN ROUND(SUM(nota)) >= 91 AND ROUND(SUM(nota)) <= 100 THEN '20'
                END AS pontos,
                'Avalia��o Completa' AS avaliacao
        FROM (		
            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.15) AS nota 
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']} AND r.resano = {$_SESSION['exercicio']}
            ---Autoavalia��o
            AND r.tavid = 1  
            ---Tem Avalia��o por Equipe
            AND s.sercomequipe = 't'
            ---Avalia��o Completa
            AND r.resavaliacaopendente = 'f'
            --- Retira todos os CPF's COM avalia��o pendente
            AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 't' AND resano = {$_SESSION['exercicio']})
            GROUP BY sersiape, sernome

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.60) AS nota
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']}  AND r.resano = {$_SESSION['exercicio']} 
            ---Avalia��o Chefe
            AND r.tavid = 2 
            ---Tem Avalia��o por Equipe
            AND s.sercomequipe = 't'
            ---Avalia��o Completa
            AND r.resavaliacaopendente = 'f'
            --- Retira todos os CPF's COM avalia��o pendente
            AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 't' AND resano = {$_SESSION['exercicio']} ) 
            GROUP BY sersiape, sernome

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(nota)*0.25 as nota 
            FROM (
                SELECT  s.sersiape, 
                        s.sernome, 
                        d.defid, 
                        ROUND(AVG(d.defpeso*r.resnota)) AS nota
                FROM gestaopessoa.servidor s
                INNER JOIN gestaopessoa.respostaavaliacao r ON r.sercpf = s.sercpf
                INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
                WHERE seranoreferencia = {$_SESSION['exercicio']}  AND r.resano = {$_SESSION['exercicio']} 
                ---Avalia��o Equipe
                AND tavid = 3 
                ---Tem Avalia��o por Equipe
                AND s.sercomequipe = 't'
                ---Avalia��o Completa
                AND r.resavaliacaopendente = 'f'
                --- Retira todos os CPF's COM avalia��o pendente
                AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 't' AND resano = {$_SESSION['exercicio']} ) 
                GROUP BY s.sersiape, s.sernome, d.defid  ) AS f   
            GROUP BY sersiape, sernome
        ) as foo
        GROUP BY sersiape, sernome

        UNION ALL

        --AVALIA��O INCOMPLETA e COM EQUIPE
        SELECT  sersiape, 
                sernome, 
                ROUND(SUM(nota)) AS nota,
                CASE
                    WHEN ROUND(SUM(nota)) <= 30 THEN '6'
                    WHEN ROUND(SUM(nota)) >= 31 AND ROUND(SUM(nota)) <= 40 THEN '8'
                    WHEN ROUND(SUM(nota)) >= 41 AND ROUND(SUM(nota)) <= 50 THEN '10'
                    WHEN ROUND(SUM(nota)) >= 51 AND ROUND(SUM(nota)) <= 60 THEN '12'
                    WHEN ROUND(SUM(nota)) >= 61 AND ROUND(SUM(nota)) <= 70 THEN '14'
                    WHEN ROUND(SUM(nota)) >= 71 AND ROUND(SUM(nota)) <= 80 THEN '16'
                    WHEN ROUND(SUM(nota)) >= 81 AND ROUND(SUM(nota)) <= 90 THEN '18'
                    WHEN ROUND(SUM(nota)) >= 91 AND ROUND(SUM(nota)) <= 100 THEN '20'
                END AS pontos,
                'Avalia��o Incompleta' AS avaliacao
        FROM (		
            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.15) AS nota 
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']}  AND r.resano = {$_SESSION['exercicio']} 
            ---Autoavalia��o
            AND r.tavid = 1  
            ---Avalia��o por Equipe
            AND s.sercomequipe = 't'
            ---Avalia��o Incompleta
            AND r.resavaliacaopendente = 't'
            --- Retira todos os CPF's SEM avalia��o pendente
            AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 'f' AND resano = 2012)  
            GROUP BY sersiape,sernome

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.60) AS nota
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']} AND r.resano = {$_SESSION['exercicio']} 
            ---Avalia��o Chefe
            AND r.tavid = 2 
            ---Avalia��o por Equipe
            AND s.sercomequipe = 't'
            ---Avalia��o Incompleta
            AND r.resavaliacaopendente = 't'
            --- Retira todos os CPF's SEM avalia��o pendente
            AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 'f' AND resano = 2012) 
            GROUP BY sersiape,sernome

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(nota)*0.25 as nota 
            FROM(
                SELECT  s.sersiape, 
                        s.sernome, 
                        d.defid, 
                        ROUND(AVG(d.defpeso*r.resnota)) AS nota
                FROM gestaopessoa.servidor s
                INNER JOIN gestaopessoa.respostaavaliacao r ON r.sercpf = s.sercpf
                INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
                WHERE seranoreferencia = {$_SESSION['exercicio']} AND r.resano = {$_SESSION['exercicio']} 
                ---Equipe
                AND tavid = 3
                ---Avalia��o por Equipe
                AND s.sercomequipe = 't'
                ---Avalia��o Incompleta
                AND r.resavaliacaopendente = 't'
                --- Retira todos os CPF's SEM avalia��o pendente
                AND s.sercpf NOT IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resavaliacaopendente = 'f' AND resano = 2012) 
                ---AND s.sercpf = '24845094134'
                GROUP BY s.sersiape,s.sernome,d.defid
            ) AS f   
            GROUP BY sersiape,sernome

        ) as foo
        GROUP BY sersiape,sernome

        UNION ALL

        --AVALIA��O COMPLETA e SEM EQUIPE
        SELECT  sersiape,
                sernome, 
                ROUND(SUM(nota)) as nota,CASE
                    WHEN ROUND(SUM(nota)) <= 30 THEN '6'
                    WHEN ROUND(SUM(nota)) >= 31 AND ROUND(SUM(nota)) <= 40 THEN '8'
                    WHEN ROUND(SUM(nota)) >= 41 AND ROUND(SUM(nota)) <= 50 THEN '10'
                    WHEN ROUND(SUM(nota)) >= 51 AND ROUND(SUM(nota)) <= 60 THEN '12'
                    WHEN ROUND(SUM(nota)) >= 61 AND ROUND(SUM(nota)) <= 70 THEN '14'
                    WHEN ROUND(SUM(nota)) >= 71 AND ROUND(SUM(nota)) <= 80 THEN '16'
                    WHEN ROUND(SUM(nota)) >= 81 AND ROUND(SUM(nota)) <= 90 THEN '18'
                    WHEN ROUND(SUM(nota)) >= 91 AND ROUND(SUM(nota)) <= 100 THEN '20'
                END AS pontos,                
                'Avalia��o Completa' AS avaliacao
        FROM (
            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.275) AS nota 
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']}  AND r.resano = {$_SESSION['exercicio']} 
            ---Autoavalia��o
            AND r.tavid = 1  
            ---N�o tem Avalia��o por Equipe
            AND s.sercomequipe = 'f'
            ---SEM avalia��o pendente
            AND r.resavaliacaopendente = 'f'
            AND s.sercpf IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resano = 2012 AND tavid IN (1,2) AND resavaliacaopendente = 'f') 
            GROUP BY sersiape,sernome, r.resavaliacaopendente

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.725) AS nota
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']} AND r.resano = {$_SESSION['exercicio']} 
            ---Avalia��o Chefe
            AND r.tavid = 2 
            ---N�o tem Avalia��o por Equipe
            AND s.sercomequipe = 'f'
            ---SEM avalia��o pendente
            AND r.resavaliacaopendente = 'f'
            AND s.sercpf IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resano = 2012 AND tavid IN (1,2) AND resavaliacaopendente = 'f') 
            GROUP BY sersiape,sernome
        ) as foo
        GROUP BY sersiape,sernome

        UNION ALL 

        --AVALIA��O INCOMPLETA e SEM EQUIPE
        SELECT  sersiape,
                sernome, 
                ROUND(SUM(nota)), 
                CASE
                    WHEN ROUND(SUM(nota)) <= 30 THEN '6'
                    WHEN ROUND(SUM(nota)) >= 31 AND ROUND(SUM(nota)) <= 40 THEN '8'
                    WHEN ROUND(SUM(nota)) >= 41 AND ROUND(SUM(nota)) <= 50 THEN '10'
                    WHEN ROUND(SUM(nota)) >= 51 AND ROUND(SUM(nota)) <= 60 THEN '12'
                    WHEN ROUND(SUM(nota)) >= 61 AND ROUND(SUM(nota)) <= 70 THEN '14'
                    WHEN ROUND(SUM(nota)) >= 71 AND ROUND(SUM(nota)) <= 80 THEN '16'
                    WHEN ROUND(SUM(nota)) >= 81 AND ROUND(SUM(nota)) <= 90 THEN '18'
                    WHEN ROUND(SUM(nota)) >= 91 AND ROUND(SUM(nota)) <= 100 THEN '20'
                END AS pontos,
                'Avalia��o Incompleta' AS avaliacao
        FROM (
            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.275) AS nota 
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            WHERE seranoreferencia = {$_SESSION['exercicio']} AND r.resano = {$_SESSION['exercicio']}
            ---Autoavalia��o
            AND r.tavid = 1
            ---N�o tem Avalia��o por Equipe
            AND s.sercomequipe = 'f'
            ---COM avalia��o pendente
            AND resavaliacaopendente = 't' 
            AND s.sercpf IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resano = 2012 AND tavid IN (1,2) AND resavaliacaopendente = 't') 
            GROUP BY sersiape,sernome

            UNION ALL 

            SELECT  sersiape,
                    sernome, 
                    SUM(ROUND(d.defpeso*r.resnota)*0.725) AS nota
            FROM gestaopessoa.servidor s
            INNER JOIN gestaopessoa.respostaavaliacao r on r.sercpf = s.sercpf
            INNER JOIN gestaopessoa.definicao AS d ON d.defid = r.defid
            ---Avalia��o Chefe
            WHERE seranoreferencia = 2012
            AND r.resano = 2012
            ---Avalia��o Chefe
            AND tavid = 2 
            ---N�o tem Avalia��o por Equipe
            AND s.sercomequipe = 'f'
            --COM avalia��o pendente
            AND resavaliacaopendente = 't' 
            AND s.sercpf IN (SELECT DISTINCT sercpf FROM gestaopessoa.respostaavaliacao WHERE resano = 2012 AND tavid IN (1,2) AND resavaliacaopendente = 't')                 
            GROUP BY sersiape,sernome 
        ) as foo
        GROUP BY sersiape, sernome
        ORDER BY sernome
    ";

$cabecalho = array("SIAPE", "Nome", "Avalia��o Final", "Pontos GDACE", "Status Avalia��o");
$dadosExcel = $db->carregar($sql);

if(empty($dadosExcel)){
	echo '
		<script>
			alert("Nenhum registro encontrado!");
			window.close(); 
		</script>';
	exit;
    
} else{
	ob_clean();
	header('content-type: text/html; charset=ISO-8859-1');
	$db->sql_to_excel($dadosExcel, 'Relat�rio_Avaliacao_Final', $cabecalho);
}?>	