<?php

    if ($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
        exit;
    }

    if ($_REQUEST['estuf']){
        $_SESSION['projovemcampo']['estuf'] = $_REQUEST['estuf'];
    } else if ($_REQUEST['muncod']){
        $_SESSION['projovemcampo']['muncod'] = $_REQUEST['muncod'];
    }

	carregarProjovemCampo();

    if (!$_SESSION['projovemcampo']['apcid']) {
        die("<script>
                                alert('Problema encontrado no carregamento. Inicie novamente a navega��o.');
                                window.location='projovemcampo.php?modulo=inicio&acao=C';
                         </script>");
    }

    include_once APPRAIZ . 'includes/cabecalho.inc';
    echo '<br>';

    echo montarAbasArray(montaMenuProJovemCampo(), $_SERVER['REQUEST_URI']);

    monta_titulo('Projovem Campo', montaTituloEstMun());

    
    echo intrucao();


function intrucao(){
	
    if ($_REQUEST['estuf'])
        $strH2 = "Senhor Secret�rio Estadual de Educa��o";
    else if ($_REQUEST['muncod'])
        $strH2 = "Senhor Secret�rio Munic�pal de Educa��o";
?>
<form id="form" name="form" method="POST">
	<center>
	<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
		<tr>
			<td width="90%" align="center">
				<div style="overflow: auto; height:380px; width: 70%; background-color: rgb(250,250,250); border-width:1px; border-style:solid; text-align:left; padding: 10 10 10 10;" >
					
					 <p>Senhor(a) Secret�rio(a) de Educa��o,<p>
<br />
                                <p>Caso seja do interesse dessa localidade fazer a ades�o ao Programa Projovem Campo � necess�rio:</p>
<br />
								<p>-	Aceitar este Termo de Ades�o no qual est� indicada a meta de atendimento proposta para essa localidade.
                                <p>-	Preencher com sua sugest�o de meta o campo dispon�vel nesse Sistema que pode ser maior ou menor daquela j� indicada. A proposta de meta do termo de ades�o ser� analisada pela DPEJUV/SECADI e a meta poder� ser ajustada ou n�o.</p>
                                <p>-    Visualizar o Termo de Ades�o com os ajustes neste sistema e ACEITAR novamente o Termo de Ades�o com a meta ajustada, caso ela tenha sido aprovada pela DPEJUV/SECADI.</p>
								<p>-    ACEITAR, tamb�m, caso a sugest�o de meta n�o tenha sido aprovada
								<p>-    Imprimir e assinar o Termo de Ades�o, ap�s o ACEITE final por essa Secretaria, e enviar para o seguinte endere�o:
                               <p> 
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Minist�rio da Educa��o � MEC <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secretaria de Educa��o Continuada, Alfabetiza��o e Inclus�o - SECADI.<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diretoria de Pol�ticas de Educa��o para a Juventude - DPEJUV<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Esplanada dos Minist�rios, Bloco L � 2� andar � Sala 220<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CEP: 70.047-900  Bras�lia-DF<br/>
                                <p>
                                
                                <p>-  Indicar o Coordenador Geral do Programa dessa localidade, escolhido entre os profissionais do quadro efetivo da Secret�ria de Educa��o ou selecionado/contratado (Resolu��o CD/FNDE/MEC N� 11, de 16 de abril de 2014).
					            <p>-  Solicitar o cadastro do Coordenador Geral no m�dulo Projovem Urbano do SIMEC para o preenchimento do Plano de Implementa��o, conforme as instru��es do SIMEC e as determina��es da Resolu��o CD/FNDE/MEC N� 11, de 16 de abril de 2014
					            <p>-  Analisar e enviar o Plano de Implementa��o que dever� estar devidamente preenchido e finalizado para a an�lise da DPEJUV/SECADI.
					            <p>-  Imprimir e assinar o Plano de Implementa��o, ap�s a valida��o desta Diretoria, e envi�-lo para o seguinte endere�o:
					           <p> 
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Minist�rio da Educa��o � MEC <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secretaria de Educa��o Continuada, Alfabetiza��o e Inclus�o - SECADI.<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diretoria de Pol�ticas de Educa��o para a Juventude - DPEJUV<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Esplanada dos Minist�rios, Bloco L � 2� andar � Sala 220<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CEP: 70.047-900  Bras�lia-DF<br/>
                                <p>
					</div>
				</td>
			</tr>
			<tr>
				<td class="SubTituloCentro"><input type="button" name="proximo" value="Pr�ximo" onclick="window.location='projovemcampo.php?modulo=principal/identificacao&acao=A';"></td>
			</tr>
		</table>
	</center>
</form>
<?
}
registarUltimoAcesso();
?>