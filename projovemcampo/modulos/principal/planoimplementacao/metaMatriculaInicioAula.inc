<?php
if($_SESSION['projovemcampo']['estuf']) {
	
	$esfera = "Federal";
	
	$sql = "SELECT DISTINCT
				COALESCE(sec.secacnpj,'-') as entnumcpfcnpj, est.estuf, est.estcod, apc.*
			FROM 
				projovemcampo.secretaria sec
			INNER JOIN projovemcampo.adesaoprojovemcampo apc ON apc.secaid = sec.secaid
			INNER JOIN territorios.estado est ON est.estcod = apc.apccodibge::character(2)
			WHERE 
				est.estuf='".$_SESSION['projovemcampo']['estuf']."' AND apc.apcesfera = 'E'";
	
	$secretaria = $db->pegaLinha($sql);
}
if($_SESSION['projovemcampo']['muncod']) {
	
	$esfera = "Municipal";
	
	$sql = "SELECT DISTINCT
				COALESCE(sec.secacnpj,'-') as entnumcpfcnpj, mun.estuf, mun.mundescricao
			FROM 
				projovemcampo.secretaria sec
			INNER JOIN projovemcampo.adesaoprojovemcampo apc ON apc.secaid = sec.secaid
			INNER JOIN territorios.municipio mun ON mun.muncod = apc.apccodibge::character(7)
			WHERE 
				mun.muncod='".$_SESSION['projovemcampo']['muncod']."' AND apc.apcesfera = 'M'";
	
	$secretaria = $db->pegaLinha($sql);
}

$meta = pegameta();
$perfil = pegaPerfilGeral();

if($esdid != ESD_EMELABORACAO && !$db->testa_superuser()) {
	$habilitado = 'N';
} elseif(in_array(PFL_CONSULTA, $perfil)){
	$habilitado = 'N';
}else{
	$habilitado = 'N';
}

?>
<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
	<tr>
		<td class="SubTituloDireita">Orienta��es:</td>
		<td>
		<font color=blue>
			<p>O Plano de Implementa��o � um instrumento de apoio � gest�o local que baliza a utiliza��o de recursos pelo EEx,conforme crit�rios estabelecidos na Resolu��o CD?FNDE n� 11 de 16 de abril de 2014, n�o condicionado o in�cio das atividades ou utiliza��o dos recursos a aproval��o da SECADI/MEC.</p>
			<p>Somente o Secret�rio de Educa��o ou o Coordenador Geral indicado poder� desenvolver o Plano de Implementa��o que dever� ser validado apenas pelo Secret�rio de Educa��o.</p>
			<p>A assinatura do Secret�rio de educa��o ser� a �nica aceita na vers�o final do Plano de Implementa��o e dever� ser impressa para envio � DPEJUV/SECADI/MEC</p>
			<p>A seguir apresenta-se a meta final do termo de ades�o para o exerc�cio de 2014, bem como o per�odo de matr�cula e a data do in�cio de aula.</p>
		</font>
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">Secretaria de Educa��o</td>
		<td>
		
		<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" width="100%">
		<tr>
			<td class="SubTituloDireita" width="20%"><b>CNPJ</b></td>
			<td><?=mascaraglobal($secretaria['entnumcpfcnpj'],'##.###.###/####-##')
// 				echo campo_texto('entnumcpfcnpj', 'N', $habilitado, 'CNPJ', 22, 19, "##.###.###/####-##", "", '', '', 0, 'id="entnumcpfcnpj"', '', (($secretaria['entnumcpfcnpj'])?mascaraglobal($secretaria['entnumcpfcnpj'],'##.###.###/####-##'):''), '' ); 
				?>
			</td>
		<tr>
		<tr>
			<td class="SubTituloDireita" width="20%"><b>Esfera</b></td>
			<td><?=$esfera ?></td>
		<tr>
		<tr>
			<td class="SubTituloDireita" width="20%"><b>Estado</b></td>
			<td><?=$secretaria['estuf'] ?></td>
		<tr>
		<tr>
			<td class="SubTituloDireita" width="20%"><b>Munic�pio</b></td>
			<td><?=(($secretaria['mundescricao'])?$secretaria['mundescricao']:'-') ?></td>
		<tr>
		</table>
		
		</td>
	</tr>
	<tr>
		<td class="SubTituloDireita">&nbsp;</td>
		<td>
		
		<table class="listagem" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" width="100%">
		<tr>
			<td>Meta definida no termo de Ades�o para o exerc�cio de 2014</td>
			<td><?=$meta ?></td>
		<tr>
		<tr>
			<td>Data de abertura da matr�cula</td>
			<td>--</td>
		<tr>
		<tr>
			<td>Data de encerramento da matr�cula</td>
			<td>--</td>
		<tr>
		<tr>
			<td>Data de in�cio das aulas</td>
			<td>--</td>
		<tr>
		</table>
		
		</td>
	</tr>
	<tr>
		<td class="SubTituloCentro" colspan="2">
			<input type="button" name="proximo" value="Pr�ximo" 
				   onclick="window.location='projovemcampo.php?modulo=principal/planoImplementacao&acao=A&aba=profissionais';">
		</td>
	</tr>
</table>
<? 
 registarUltimoAcesso(); ?>
