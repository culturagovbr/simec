<?php 
    if (!$_SESSION['projovemcampo']['apcid']) {
        die("
            <script>
                alert('Problema encontrado no carregamento. Inicie novamente a navega��o.');
                window.location='projovemcampo.php?modulo=inicio&acao=C';
           </script>
        ");
    }

    if ($_REQUEST['requisicao']) {
        $_REQUEST['requisicao']($_REQUEST);
        exit;
    }
//     $dataAtual      = mktime( date('H'), date('i'), date('s'), date('m'), date('d'), date('Y') );
//     $dataBloqueio   = mktime( 23, 59, 00, 08,6, 2014 );
//     $bloqueioHorario = (bool) ( $dataAtual > $dataBloqueio );
    $habilita = 'N';
    $perfil = pegaPerfilGeral();
    if( ( in_array(PFL_SECRETARIO_MUNICIPAL, $perfil) || in_array(PFL_SECRETARIO_ESTADUAL, $perfil)|| in_array(PFL_COORDENADOR_ESTADUAL, $perfil) || in_array(PFL_COORDENADOR_MUNICIPAL, $perfil)) /* && 
    	$_SESSION['projovemcampo']['ppuano'] >= date('Y') && !$bloqueioHorario*/) {
        $habilita = 'S';
    }
    if( ( in_array(PFL_EQUIPE_MEC, $perfil) || in_array(PFL_ADMINISTRADOR, $perfil) || $db->testa_superuser()  /*&& !$bloqueioHorario*/) ){
    	$habilita = 'S';
    }

    include_once APPRAIZ . 'includes/cabecalho.inc';
    require_once APPRAIZ . "www/includes/webservice/cpf.php";
    echo '<br>';


    $usuarioprojovem = pegarUsuarioMaterial();
// ver($usuarioprojovem);
    $rsSecretaria = recuperaSecretariaPorUfMuncod();
    if($usuarioprojovem['muncodmaterialdidatico']){
	    $sql = "SELECT
					estuf
				FROM
					territorios.municipio
				WHERE
					muncod = '{$usuarioprojovem['muncodmaterialdidatico']}'";
	    $uf = $db->pegaUm ( $sql );
    }
?>
    <script type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="../includes/funcoes.js"></script>
    <script>

        function salvarenderecoresp() {
            if (jQuery('#novo_cpf').val() == "") {
                alert('CPF � obrigat�rio');
                return false;
            }
            if (jQuery('#iseorgaoexpedidor').val() == "") {
                alert('�rg�o Emissor/Expedidor � obrigat�rio');
                return false;
            }
            if (jQuery('#cepmaterialdiddatico').val() == "") {
                alert('CEP � obrigat�rio');
                return false;
            }
            if (jQuery('#enderecomaterialdidatico').val() == "") {
                alert('Endere�o � obrigat�rio');
                return false;
            }
            if (jQuery('#bairromaterialdidatico').val() == "") {
                alert('Bairro � obrigat�rio');
                return false;
            }
            if (jQuery('#numeromaterialdidatico').val() == "") {
                alert('N�mero � obrigat�rio');
                return false;
            }
            if (jQuery('#eemuf').val() == "") {
                alert('UF � obrigat�rio');
                return false;
            }
            if (jQuery('#muncodmaterialdidatico').val() == "") {
                alert('Munic�pio � obrigat�rio');
                return false;
            }

            jQuery('#form').submit();
        }

        function getEnderecoPeloCEP(cep) {
            jQuery.ajax({
                type: "POST",
                url: "/geral/consultadadosentidade.php",
                data: "requisicao=pegarenderecoPorCEP&endcep=" + cep,
                async: false,
                success: function(dados) {
                    var enderecos = dados.split("||");
                    jQuery('#enderecomaterialdidatico').val(enderecos[0]);
                    jQuery('#bairromaterialdidatico').val(enderecos[1]);
                    jQuery('#mundescricao').val(enderecos[2]);
                    jQuery('#eemuf').val(enderecos[3]);
                    jQuery('#muncodmaterialdidatico').val(enderecos[4]);
                }
            });
        }
   
        function alterarCPF( cpf ){
        
            var cpf = jQuery('#novo_cpf').val();
            //VALIDAR CPF.
            if( !validar_cpf( cpf ) ){
                alert('CPF inv�lido!');
                $('#novo_cpf').focus();
                $('#salvar').attr('disabled', 'true');
                return false;		
            }else if( validar_cpf( cpf ) ){
                $('#salvar').removeAttr('disabled');
            }
            
            //VALIDAR CPF RECEITA
            var comp = new dCPF();
            comp.buscarDados(cpf);
            //console.log(comp.dados);
            if( comp.dados.no_pessoa_rf != '' ){
                jQuery('#td_nome_secretario').html(comp.dados.no_pessoa_rf);
                jQuery('[name=cpfmaterialdidatico]').val(comp.dados.nu_cpf_rf);
            }else{
                alert('CPF n�o encontrado na base de dados da receita.' );
                return false;
            }
        }
    </script>
    <form id="form" name="form" method="POST">
        <input type="hidden" name="requisicao" value="gravarresponsavelmaterial">
        <table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3"	align="center">
            <? //if (in_array(PFL_EQUIPE_MEC, $perfil) || $db->testa_superuser()) : ?>
            <td rowspan="3" class="SubTituloDireita">Respons�vel pelo recebimento do material:</td>
            <tr>
                <td class="SubTituloDireita">
                    <?php echo $usuarioprojovem['usunome'] ? '<b>Novo CPF:</b>' : 'CPF:'; ?>
                </td>

                <td>
                    <?php
                        if($usuarioprojovem['usucpf']){
                            $cpf = formatar_cpf( $usuarioprojovem['usucpf'] );
                        }else{
                            $cpf = formatar_cpf( $_SESSION['usucpf'] );
                        }
                        echo campo_texto('novo_cpf', 'S', 'S', 'Novo CPF', 15, 14, "###.###.###-##", '', '', '', 0, 'id="novo_cpf"', '', $cpf, 'alterarCPF(\'novo_cpf\');');
                    ?>
                </td>
            </tr>
            <? //endif; ?>
            <tr>
                <td class="SubTituloDireita">Nome Completo:</td>
                <td>
                    <?php
                        $usunome = $db->pegaUm("select usunome from seguranca.usuario where usucpf = '{$_SESSION['usucpf']}'");
                    ?>
                    <input type="hidden" name="cpfmaterialdidatico" value="<? echo (($usuarioprojovem['cpfmaterialdidatico']) ? $usuarioprojovem['cpfmaterialdidatico'] : $_SESSION['usucpf']); ?>">
                    <input type="hidden" name="nomematerialdidatico" value="<? echo (($usuarioprojovem['usunome']) ? $usuarioprojovem['usunome'] : $usunome); ?>">
                    <span id="td_nome_secretario"><? echo (($usuarioprojovem['usunome']) ? $usuarioprojovem['usunome'] : $usunome); ?></span>
                </td>
            </tr>
            <td rowspan="8" class="SubTituloDireita">Endere�o de recebimento do material:</td>
            <tr>
                <td class="SubTituloDireita">CEP:</td>
                <td><? echo campo_texto('cepmaterialdiddatico', "S", $habilita, "CEP", 10, 9, "#####-###", "", '', '', 0, 'id="cepmaterialdiddatico"', '', mascaraglobal($usuarioprojovem['cepmaterialdiddatico'], "#####-###"), 'getEnderecoPeloCEP(this.value);'); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">Endere�o</td>
                <td><? echo campo_texto('enderecomaterialdidatico', "S", 'N', "CEP", 50, 200, "", "", '', '', 0, 'id="enderecomaterialdidatico"', '', $usuarioprojovem['enderecomaterialdidatico']); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">Bairro:</td>
                <td><? echo campo_texto('bairromaterialdidatico', "S", 'N', "CEP", 50, 200, "", "", '', '', 0, 'id="bairromaterialdidatico"', '', $usuarioprojovem['bairromaterialdidatico']); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">Complemento:</td>
                <td><? echo campo_texto('complementomaterialdidatico', "N", $habilita, "CEP", 50, 200, "", "", '', '', 0, 'id="complementomaterialdidatico"', '', $usuarioprojovem['complementomaterialdidatico']); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">N�mero:</td>
                <td><? echo campo_texto('numeromaterialdidatico', "S", $habilita, "CEP", 9, 10, "#########", "", '', '', 0, 'id="numeromaterialdidatico"', '', $usuarioprojovem['numeromaterialdidatico']); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">UF:</td>
                <td><? echo campo_texto('eemuf', "N", 'N', "UF", 2, 4, "", "", '', '', 0, 'id="eemuf"', '', $uf); ?></td>
            </tr>
            <tr>
                <td class="SubTituloDireita">Municipio:</td>
                <td>
                    <input type="hidden" name="muncodmaterialdidatico" id="muncodmaterialdidatico" value="<?= $usuarioprojovem['muncodmaterialdidatico'] ?>">
    <? echo campo_texto('mundescricao', "N", "N", "Munic�pio", 50, 200, "", "", '', '', 0, 'id="mundescricao"', '', (($usuarioprojovem['muncodmaterialdidatico']) ? $db->pegaUm("SELECT mundescricao FROM territorios.municipio WHERE muncod='" . $usuarioprojovem['muncodmaterialdidatico'] . "'") : "")); ?>
                </td>
            </tr>
    <?php if ($habilita == 'S') { ?>
                <tr>
                    <td class="SubTituloCentro" colspan="3">
                    	<input type="button" name="anterior" value="Anterior" onclick="window.location='projovemcampo.php?modulo=principal/planoImplementacao&acao=A&aba=demaisAcoes';"> 
                        <input type="button" id="salvar" name="salvar" value="Salvar" onclick="salvarenderecoresp();">
                        <input type="button" name="proximo" value="Pr�ximo" onclick="window.location='projovemcampo.php?modulo=principal/planoImplementacao&acao=A&aba=repasseRecurso';"></td>
                    </td>
                </tr>
    <?php } ?>
        </table>
    </form>
    
    <? registarUltimoAcesso(); ?>
