<?php
	
class Dimensao extends Modelo
{
	
    /**
     * Nome da tabela especificada
     * @var string
     * @access protected
     */
    protected $stNomeTabela = "par.dimensao";	

    /**
     * Chave primaria.
     * @var array
     * @access protected
     */
    protected $arChavePrimaria = array( "dimid" );

    /**
     * Atributos
     * @var array
     * @access protected
     */    
    protected $arAtributos     = array(
									  	'dimid' => null, 
									  	'dimcod' => null, 
									  	'dimdsc' => null, 
									  	'dimstatus' => null,
    									'itrid' => null,
									  );
	protected $stOrdem = null;

	public function lista($retorno, $itrid, $whereExterno = NULL, $whereInterno= NULL, $innerInterno = NULL)
	{  
		if(!$itrid || !$_SESSION['par']['inuid']){
			return false; 
		}
		
		if($_SESSION['par']['inuid'] != 1){ // verifica se o inuid � DF
			$where = " and i.indid not in (213, 214, 220, 267, 268, 269, 270) ";
		}else{
			if($_SESSION['par']['estuf'] != "DF"){
				$where = " and i.indid not in (213,214,220,267,268,269,270) ";
			}
		}
		
		$sql = "SELECT 	 d.dimid,
						 d.dimcod,
						 d.dimdsc,
						 a.areid,
						 a.arecod,
						 a.aredsc,
						 i.indid,
						 i.indcod,
						 i.inddsc,
						 ds.ptoid,
						 ds.aciid,
						 ds.acidsc,
						 ds.sbaid,
						 ds.sbadsc,
						 ds.sbaordem,
						 ds.frmid,
						 ds.crtpontuacao 
					FROM par.dimensao d
					INNER JOIN par.area a ON a.dimid = d.dimid
					INNER JOIN par.indicador i ON i.areid = a.areid
					LEFT JOIN 
						(SELECT  p.ptoid,
							 ac.aciid,
							 ac.acidsc,
							 s.sbaid,
							 s.sbadsc,
							 s.sbaordem,
							 s.frmid,
							 c.indid,
							 c.crtpontuacao  
						 FROM par.criterio 	c 
						 INNER JOIN par.pontuacao 	p  ON p.crtid = c.crtid  AND p.ptostatus = 'A'
						 LEFT JOIN par.acao 		ac ON ac.ptoid = p.ptoid AND ac.acistatus = 'A'
						 LEFT JOIN par.subacao 		s  ON s.aciid = ac.aciid AND s.sbastatus = 'A'
						 ".$innerInterno."
						 WHERE p.inuid = {$_SESSION['par']['inuid']}
						  ".$whereInterno."
						 ) AS ds ON i.indid = ds.indid  
					WHERE 	d.itrid = $itrid
						AND dimstatus = 'A'
						AND arestatus = 'A'
						AND indstatus = 'A'
						".$where.$whereExterno."
					ORDER BY d.dimcod,a.arecod,i.indcod, ds.sbaordem
							";
		//dbg($sql,1);
		/*
			$sql = "SELECT DISTINCT d.dimid,
	                         d.dimcod,
	                         d.dimdsc,
	                         a.areid,
	                         a.arecod,
	                         a.aredsc,
	                         i.indid,
	                         i.indcod,
	                         i.inddsc,
	                         p.ptoid,
	                         ac.aciid,
	                         ac.acidsc,
	                         s.sbaid,
	                         s.sbadsc,
	                         s.sbaordem,
	                         s.frmid,
	                         count(sd.sbdid) as sbdid
	             FROM par.dimensao d
	             INNER JOIN par.area a ON a.dimid = d.dimid
	             INNER JOIN par.indicador i ON i.areid = a.areid
	             LEFT JOIN par.criterio c ON c.indid = i.indid
	             LEFT JOIN par.pontuacao p ON p.crtid = c.crtid AND ptostatus = 'A' AND p.inuid = {$_SESSION['par']['inuid']}
                 LEFT JOIN par.acao ac ON ac.ptoid = p.ptoid
                 LEFT JOIN par.subacao s ON s.aciid = ac.aciid
                 LEFT JOIN par.subacaodetalhe sd ON sd.sbaid = s.sbaid
                 INNER JOIN par.instrumentounidade iu ON p.inuid = iu.inuid -- AND iu.inuid = {$_SESSION['par']['inuid']}
	             WHERE d.itrid = $itrid
	                        AND dimstatus = 'A'
	                        AND arestatus = 'A'
	                        AND indstatus = 'A'
	                        ".$where.$whereExterno."
	             GROUP BY d.dimid,
							 d.dimcod,
							 d.dimdsc,
							 a.areid,
							 a.arecod,
							 a.aredsc,
							 i.indid,
							 i.indcod,
							 i.inddsc,
							 p.ptoid,
							 ac.aciid,
							 ac.acidsc,
							 s.sbaid,
							 s.sbadsc,
							 s.frmid,
							 s.sbaordem
	             ORDER BY dimcod, arecod, indcod, s.sbaordem";
			*/
	                     // dbg($sql,1);
		if($retorno == 'array'){
			return $this->carregar($sql);
		} else {
			return $sql;
		}
	}
	
	public function recuperarDimensoesGuia($itrid)
	{
		$sql = "SELECT DISTINCT 
					d.dimid, 
					d.dimcod, 
					d.dimdsc,
					d.itrid					
				FROM {$this->stNomeTabela} d
				INNER JOIN par.instrumento i ON i.itrid = d.itrid 
				WHERE d.dimstatus = 'A'
				AND d.itrid = {$itrid}
				ORDER BY d.dimcod
				--limit 1
				";

		return $this->carregar($sql);
	}
	
	public function recuperarOrdemDimensaoPorItrid($itrid)
	{
		$sql = "SELECT 
					max(dimcod) as ordem 
				FROM {$this->stNomeTabela}
				WHERE itrid = {$itrid}";
				
		$this->stOrdem =  $this->pegaUm($sql)+1;
	}
	
	public function verificaSubitensGuia($dimid)
	{
		
		$sql = "SELECT 
					count(areid) 
				FROM par.area 
				WHERE dimid = {$dimid}";
		
		if($this->pegaUm($sql) > 0){
			return false;
		} else {
			return true;
		}
		
	}
	
	public function deletarDimensaoGuia($dimid)
	{
		
		if(!$this->verificaSubitensGuia($dimid)){
			
			alert("N�o foi poss�vel excluir a dimens�o, existem subitens cadastrados.");
			echo "<script>window.location.href = 'par.php?modulo=principal/configuracao/guia&acao=A';</script>";		
			return false;
		}		
		
		$this->dimid = $dimid;
		$this->excluir();
		$this->commit();		
	}
	
}