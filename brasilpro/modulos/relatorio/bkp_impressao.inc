
		<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
		<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>
		
<?php



print '<br/>';

//monta_titulo( $titulo_modulo, '&nbsp;' );

$sql = "
		select
			d.dimid,
			d.dimcod || ' - ' || d.dimdsc as dimensao,
			area.ardcod || ' - ' || area.arddsc as area,
			area.ardid,			
			i.indcod || ' - ' || i.inddsc as indicador,
			i.indid,
			i.indcod,
			c.ctrpontuacao as pontuacao,
			Case acao.acilocalizador
			when 'E' then 'Estadual'
			when 'M' then 'Municipal'
			end as Tipo,
			acao.aciid,
			acao.ptoid,
			acao.acidsc,
			acao.acirpns,
			acao.acicrg,
			to_char(acao.acidtinicial,'dd/mm/yyyy') as acidtinicial,
			to_char(acao.acidtfinal,'dd/mm/yyyy') as acidtfinal,
			acao.acirstd,
			acao.acilocalizador,
			subacao.sbaid,
			subacao.undid,
			subacao.frmid,
			subacao.sbadsc,
			subacao.sbastgmpl,
			subacao.sbaprm,
			subacao.sbapcr,
			coalesce(subacao.sba1ano, 0) as sba1ano,
			coalesce(subacao.sba2ano, 0) as sba2ano,
			coalesce(subacao.sba3ano, 0) as sba3ano,
			coalesce(subacao.sba4ano, 0) as sba4ano,
			coalesce(subacao.sbaunt,0) as sbaunt,
			subacao.sbauntdsc,
			subacao.sba1ini,
			subacao.sba1fim,
			subacao.sba2ini,
			subacao.sba2fim,
			subacao.sba3ini,
			subacao.sba3fim,
			subacao.sba4ini,
			subacao.sba4fim,
			coalesce(u.unddsc,'') as unddsc,
			coalesce(f.frmdsc,'') as frmdsc,
			c.crtdsc,
			c.ctrpontuacao,
			f.frmid
		from
			cte.dimensao d
			inner join cte.areadimensao area ON area.dimid = d.dimid
			inner join cte.indicador i ON i.ardid = area.ardid
			inner join cte.pontuacao p ON p.indid = i.indid and p.ptostatus = 'A'
			inner join cte.criterio c ON c.crtid = p.crtid
			inner join cte.instrumentounidade iu ON iu.inuid = p.inuid
			inner join cte.acaoindicador acao ON acao.ptoid = p.ptoid
			inner join cte.subacaoindicador subacao ON subacao.aciid = acao.aciid
			inner join cte.unidademedida u on u.undid = subacao.undid
			inner join cte.formaexecucao f on f.frmid = subacao.frmid
			
		where iu.inuid = '".$_SESSION['inuid']."' and
		p.ptostatus = 'A'
		order by
			d.dimcod,  
			area.ardcod,
			i.indcod, p.ptoid, acao.aciid, subacao.sbaid;
";

//dbg($sql,1 );
$dado = $db->carregar($sql);
$i=0;

$totalreg = count($dado);

$novaDimensao = $dado[$i]['dimcod'];
$novaArea = $dado[$i]['ardcod'];
$novoIndicador = $dado[$i]['indcod'];
$novaAcao = $dado[$i]['aciid'];
$novasubAcao = $dado[$i]['sbaid'];	

$totalGeralAno1 = 0;
$totalGeralAno2 = 0;
$totalGeralAno3 = 0;
$totalGeralAno4 = 0;	


?>


<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<thead>
	<th colspan="2" align="left">
	<?
		$sql = "select estdescricao from territorios.estado where estuf = '" . $_SESSION['estuf'] . "'";
		$estado = $db->pegaUm($sql);
	?>
		<h1>Par do Estado:  <?=$estado?></h1> <br>
		<?='Impresso em: ' . date('d/m/Y - h:i' ); ?>
	</th>
</thead>
<? 
	while ( $i < $totalreg ) {

		$dimensao = $dado[$i]['dimcod'];
		while ($dimensao == $novaDimensao) 
		{
			if($i >= $totalreg ) break;
				?>
					<tr>
						<td class="SubTituloDireita">
							<b>Dimens�o</b>
						</td>
						<td class="" style="text-align:left">	
							<?=$dado[$i]['dimensao']; ?>
						</td>
					</tr>
					<?
				$area = $dado[$i]['ardcod'];	
				while ($area == $novaArea) 
				{		
					if($i >= $totalreg ) break;
							?>
								<tr>
									<td class="SubTituloDireita">
										<b>�rea</b>
									</td>
									<td class="" style="text-align:left">	
										<?=$dado[$i]['area']; ?>
									</td>
								</tr>
								
						
								<?
									$indicador = $dado[$i]['indcod'];	
									while ($indicador == $novoIndicador) 
									{		
										if($i >= $totalreg ) break;
											//print 'Indicador'.$indicador.'<br>';
											//print 'IndicadorNOVO'.$novoIndicador.'<br>';
											
												?>
													<tr>
														<td class="SubTituloDireita">
															<b>Indicador</b>
														</td>
														<td class="" style="text-align:left">	
															<?=$dado[$i]['indicador']; ?>
														</td>
													</tr>
													<tr>
														<td class="SubTituloDireita">
															<b>Crit�rio / Pontua��o</b>
														</td>
														<td class="" style="text-align:left">	
															<?echo $dado[$i]['ctrpontuacao'] . ' - '.$dado[$i]['crtdsc']; ?>
														</td>
													</tr>
										
													<?

													$acao = $dado[$i]['aciid'];
													while ($acao == $novaAcao) 
													{	
														if($i >= $totalreg ) break;
														//print 'A��oFORA'.$acao.'<br>';
														//print 'NovaA��oFORA'.$novaAcao.'<br><p>';
											?>
																<tr>
																	<td class="SubTituloDireita">
																		<b>A��o</b>
																	</td>
																	<td class="" style="text-align:left">
																		<table class="listagem" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="left" >
																			<tr>
																				<td  class="SubTituloDireita">
																					Demanda:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['tipo'];?>
																				</td>
																			</tr>	
																			<tr>
																				<td  class="SubTituloDireita">
																					Descri��o da A��o:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acidsc']; ?>
																				</td>
																			</tr>
																			<tr>
																				<td  class="SubTituloDireita">
																					Nome do Respons�vel:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acirpns']; ?>
																				</td>
																			</tr>	
																			<tr>
																				<td  class="SubTituloDireita">
																					Cargo do Respons�vel:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acicrg']; ?>
																				</td>
																			</tr>			 
																			<tr>
																				<td  class="SubTituloDireita">
																					Per�odo Inicial:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acidtinicial']; ?>
																				</td>
																			</tr>
																			<tr>
																				<td  class="SubTituloDireita">
																					Per�odo Final:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acidtfinal']; ?>
																				</td>
																			</tr>
																			<tr>
																				<td  class="SubTituloDireita">
																					Resultado Experado:
																				</td>
																				
																				<td>
																					<?=$dado[$i]['acirstd']; ?>
																				</td>
																			</tr>
																		</table>	
																	</td>
																</tr>			
																<?
																	$subacao = $dado[$i]['sbaid'];
																	while ($acao == $novaAcao ) 
																	{
																				mostra_subacao();
																				if($i >= $totalreg ) break;
																	}
														}		
														
														?>	
												<tr>
													<td class="SubTituloDireita" >
													<b>Total Geral por Indicador</b>
													</td>
													<td>			
														<table class="listagem" width="100%">
															<thead>
																<th align="center">
																	<b>1� Ano</b>
																</th>
																<th align="center">
																	<b>2� Ano</b>
																</th>
																<th align="center">
																	<b>3� Ano</b>
																</th>
																<th align="center">
																	<b>4� Ano</b>
																</th>
																<th align="center">
																	<b>Total</b>
																</th>
															</thead>
															<tr>
																<td align="right">
																	<?=number_format($totalGeralIndicadorAno1,2,',','.');?>
																</td>
																<td align="right">
																	<?=number_format($totalGeralIndicadorAno2,2,',','.');?>
																</td>
																<td align="right">
																	<?=number_format($totalGeralIndicadorAno3,2,',','.');?>
																</td>
																<td align="right">
																	<?=number_format($totalGeralIndicadorAno4,2,',','.');?>
																</td>
																<td align="right">
																	<?=number_format($totalGeralIndicadorAno1 + $totalGeralIndicadorAno2 + $totalGeralIndicadorAno3 + $totalGeralIndicadorAno4 ,2,',','.');?>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<? 
													$totalGeralIndicadorAno1 = 0;
													$totalGeralIndicadorAno2 = 0;
													$totalGeralIndicadorAno3 = 0;
													$totalGeralIndicadorAno4 = 0;
												
						}		
							?>
						<tr>
							<td class="SubTituloDireita" >
							<b>Total Geral por �rea</b>
							</td>
							<td>			
								<table class="listagem" width="100%">
									<thead>
										<th align="center">
											<b>1� Ano</b>
										</th>
										<th align="center">
											<b>2� Ano</b>
										</th>
										<th align="center">
											<b>3� Ano</b>
										</th>
										<th align="center">
											<b>4� Ano</b>
										</th>
										<th align="center">
											<b>Total</b>
										</th>
									</thead>
									<tr>
										<td align="right">
											<?=number_format($totalGeralAreaAno1,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralAreaAno2,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralAreaAno3,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralAreaAno4,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralAreaAno1 + $totalGeralAreaAno2 + $totalGeralAreaAno3+ $totalGeralAreaAno4,2,',','.');?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<? 
							$totalGeralAreaAno1 = 0;
							$totalGeralAreaAno2 = 0;
							$totalGeralAreaAno3 = 0;
							$totalGeralAreaAno4 = 0;
						
		}
?>
		<tr>
							<td class="SubTituloDireita" >
							<b>Total Geral por Dimens�o</b>
							</td>
							<td>			
								<table class="listagem" width="100%">
									<thead>
										<th align="center">
											<b>1� Ano</b>
										</th>
										<th align="center">
											<b>2� Ano</b>
										</th>
										<th align="center">
											<b>3� Ano</b>
										</th>
										<th align="center">
											<b>4� Ano</b>
										</th>
										<th align="center">
											<b>Total</b>
										</th>
									</thead>
									<tr>
										<td align="right">
											<?=number_format($totalGeralDimensaAno1,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralDimensaAno2,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralDimensaAno3,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralDimensaAno4,2,',','.');?>
										</td>
										<td align="right">
											<?=number_format($totalGeralDimensaAno1 + $totalGeralDimensaAno2 + $totalGeralDimensaAno3+ $totalGeralDimensaAno4,2,',','.');?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<? 
							$totalGeralDimensaAno1 = 0;
							$totalGeralDimensaAno2 = 0;
							$totalGeralDimensaAno3 = 0;
							$totalGeralDimensaAno4 = 0;	
	}
	
	}?>	
<tr>
	<td class="SubTituloDireita"  colspan="2" style="text-align: center;">
	<b>Total Geral</b>
	</td>
</tr>
<tr>
	<td colspan="2" >
		<table class="listagem" width="100%">
			<thead>
				<th align="center">
					<b>1� Ano</b>
				</th>
				<th align="center">
					<b>2� Ano</b>
				</th>
				<th align="center">
					<b>3� Ano</b>
				</th>
				<th align="center">
					<b>4� Ano</b>
				</th>
				<th align="center">
					<b>Total</b>
				</th>
			</thead>
			<tr>
				<td align="right">
					<?=number_format($totalGeralAno1,2,',','.');?>
				</td>
				<td align="right">
					<?=number_format($totalGeralAno2,2,',','.');?>
				</td>
				<td align="right">
					<?=number_format($totalGeralAno3,2,',','.');?>
				</td>
				<td align="right">
					<?=number_format($totalGeralAno4,2,',','.');?>
				</td>
				<td align="right">
					<?=number_format($totalGeralAno1 + $totalGeralAno2 + $totalGeralAno3 + $totalGeralAno4 ,2,',','.');?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
	
	
<?





function mostra_subacao(){
	
	global $dado, $i, $totalreg;
	global $total, $totalGeralAno1, $totalGeralAno2, $totalGeralAno3, $totalGeralAno4;
	global $totalGeralIndicadorAno1, $totalGeralIndicadorAno2, $totalGeralIndicadorAno3, $totalGeralIndicadorAno4;
	global $totalGeralAreaAno1, $totalGeralAreaAno2, $totalGeralAreaAno3, $totalGeralAreaAno4;
	global $totalGeralDimensaAno1, $totalGeralDimensaAno2, $totalGeralDimensaAno3, $totalGeralDimensaAno4;
	global $novaDimensao, $novaArea, $novoIndicador, $novaAcao, $novasubAcao;
		?>
					<tr>
						<td class="SubTituloDireita">
							<b>Sub-A��o</b>
						</td>
						<td class="" style="text-align:left">	
						
							<table  class="listagem" width="100%" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="left" >
								<tr>
									<td  class="SubTituloDireita">
										Descri��o da Suba��o:
									</td>
									
									<td>
										<?=$dado[$i]['sbadsc']; ?>
									</td>
								</tr>									
								<tr>
									<td  class="SubTituloDireita">
										Estrat�gia de Implementa��o:
									</td>
									
									<td>
										<?=$dado[$i]['sbastgmpl']; ?>
									</td>
								</tr>
								<tr>
									<td  class="SubTituloDireita">
										Programa:
									</td>
									
									<td>
										<?=$dado[$i]['sbaprm']; ?>
									</td>
								</tr>
								<tr>
									<td  class="SubTituloDireita">
										Unidade de Medida:
									</td>
									
									<td>
										<?=$dado[$i]['unddsc']; ?>
									</td>
								</tr>
								<tr>
									<td  class="SubTituloDireita">
										Forma de Execu��o
									</td>
									
									<td>
										<?=$dado[$i]['frmdsc']; ?>
									</td>
								</tr>
								<tr>
									<td  class="SubTituloDireita">
										Institui��o Parceira (se houver):
									</td>
									
									<td>
										<?=$dado[$i]['sbapcr']; ?>
									</td>
								</tr>
								<tr>
									<td  class="SubTituloDireita">
										Quantidades e Cronograma F�sico
									</td>									
									<td>
										<table class="listagem" width="100%">
											<thead> 
												<th>
													&nbsp;	
												</th>											
												<th align="center">
														<b>1� Ano</b>
												</th>																								
												<th align="center">
														<b>2� Ano</b>
												</th>
												<th align="center">
														<b>3� Ano</b>
												</th>
												<th align="center">
														<b>4� Ano</b>
												</th>
												<th align="center">
													<b>Total</b>
												</th>
											</thead>											
											<tr>
												<td align="right">
													<b>Quantidades:</b>
												</td>
												<td align="right">
													<?=number_format($dado[$i]['sba1ano'],0); ?>
												</td>
												<td align="right">
													<?=number_format($dado[$i]['sba2ano'],0); ?>
												</td>
												<td align="right">
													<?=number_format($dado[$i]['sba3ano'],0); ?>
												</td>
												<td align="right">
													<?=number_format($dado[$i]['sba4ano'],0); ?>
												</td>
												<td align="right">
													<?
													 	$total = 	$dado[$i]['sba1ano'] + 
													 				$dado[$i]['sba2ano'] + 
													 				$dado[$i]['sba3ano'] + 
													 				$dado[$i]['sba4ano'];
													 	echo number_format($total,0);
													 ?>
												</td>
											</tr>											
											<tr>
												<td align="right">
													<b>Cronograma F�sico:</b>
												</td>
												<td align="right">
													<?if(!Empty($dado[$i]['sba1ini'])) echo $dado[$i]['sba1ini'] . " at� " ; ?>  <?=$dado[$i]['sba1fim']; ?>
												</td>
												
												<td align="right">
													<?if(!Empty($dado[$i]['sba2ini'])) echo $dado[$i]['sba2ini'] . " at� " ; ?>  <?=$dado[$i]['sba2fim']; ?>		
												</td>
												<td align="right">
													<?if(!Empty($dado[$i]['sba3ini'])) echo $dado[$i]['sba3ini'] . " at� " ; ?> <?=$dado[$i]['sba3fim']; ?>
												</td>
												<td align="right">
													<?if(!Empty($dado[$i]['sba4ini'])) echo $dado[$i]['sba4ini'] . " at� " ; ?> <?=$dado[$i]['sba4fim']; ?>
												</td>
												<td align="right">
													&nbsp;
												</td>
											</tr>											
										</table>

										<?php if ( $dado[$i]['sbaunt'] > 0 ) { ?>

										<table 	class="listagem" width="100%"  cellspacing="1" cellpadding="3" >
											<tr>
												<td align="right">
													<b>Valor Unit�rio:</b>
												</td>
												<td align="right">
													<?=number_format($dado[$i]['sbaunt'],2,',','.');?>
												</td>
											</tr>
											<tr>
												<td align="right">
													<b>Detalhamento da Composi��o</br> do Valor Unit�rio:</b>  	
												</td>
												<td>
													<?=$dado[$i]['sbauntdsc']?>
												</td>
											</tr>
										</table>
										
										<table class="listagem" width="100%"  cellspacing="1" cellpadding="3" >
											<thead>
												<th>
													&nbsp;
												</th>
												<th align="center">
													<b>1� Ano</b>
												</th>
												<th align="center">
													<b>2� Ano</b>
												</th>
												<th align="center">
													<b>3� Ano</b>
												</th>
												<th align="center">
													<b>4� Ano</b>
												</th>
												<th align="center">
													<b>Total</b>
												</th>
											</thead>
											<tr>
												<?
													$ano1 = $dado[$i]['sbaunt'] * $dado[$i]['sba1ano'];
													$ano2 = $dado[$i]['sbaunt'] * $dado[$i]['sba2ano'];
													$ano3 = $dado[$i]['sbaunt'] * $dado[$i]['sba3ano'];
													$ano4 = $dado[$i]['sbaunt'] * $dado[$i]['sba4ano'];
												
													$total = $ano1 + $ano2 + $ano3 + $ano4;
													
													$totalGeralAno1 += $ano1;
													$totalGeralAno2 += $ano2;
													$totalGeralAno3 += $ano3;
													$totalGeralAno4 += $ano4;	
													
													$totalGeralIndicadorAno1 += $ano1;
													$totalGeralIndicadorAno2 += $ano2;
													$totalGeralIndicadorAno3 += $ano3;
													$totalGeralIndicadorAno4 += $ano4;
																	
													$totalGeralAreaAno1 += $ano1;
													$totalGeralAreaAno2 += $ano2;
													$totalGeralAreaAno3 += $ano3;
													$totalGeralAreaAno4 += $ano4;
													
													$totalGeralDimensaAno1 += $ano1;
													$totalGeralDimensaAno2 += $ano2;
													$totalGeralDimensaAno3 += $ano3;
													$totalGeralDimensaAno4 += $ano4;
									
													
												 ?>
												<td align="right">
													<b>Valores Anuais:</b>
												</td>												
												<td align="right">
													<?=number_format($ano1,2,',','.');?>
												</td>
												<td align="right">
													<?=number_format($ano2,2,',','.');?>
												</td>
												<td align="right">
													<?=number_format($ano3,2,',','.');?>
												</td>
												<td align="right">
													<?=number_format($ano4,2,',','.');?>
												</td>
												<td align="right">
													<?=number_format($total,2,',','.') ?>
												</td>
											</tr>		
										</table>
										<?php }  ?>
																																												
									</td>
								</tr>
							</table>	
						</td>
					</tr>
					
				<?		$i++;			

						$novaDimensao = $dado[$i]['dimcod'];
						$novaArea = $dado[$i]['ardcod'];
						$novoIndicador = $dado[$i]['indcod'];
						$novaAcao = $dado[$i]['aciid'];
						$novasubAcao = $dado[$i]['sbaid'];
						
}

?>
