<?php
/************************* INCLUDES *************************************************/
include_once(APPRAIZ.'www/brasilpro/monitoraFinanceiro/includes/constantes.php');
include_once(APPRAIZ.'brasilpro/modulos/principal/monitoraFinanceiro/funcoes.inc');
include_once(APPRAIZ.'includes/classes/dateTime.inc');
include_once(APPRAIZ.'brasilpro/classes/HistoricoConvItemComposicao.class.inc');

/************************* DECLARA��O DE VARIAVEIS *********************************/
$scsid				 	= $_REQUEST['scsid'];
$sbaid 					= $_REQUEST['sbaid'];
$hmsid					= $_REQUEST['hmsid'];
$cosid					= $_REQUEST['cosid'];
$quantidade				= $_REQUEST['quant'];
$valor					= $_REQUEST['valor'];
$quantEmpenhada 		= $_REQUEST['hmsquantidadeempenhada'];
$quantliquidada 		= $_REQUEST['hmsquantidadeliquidada'];
$quantPaga				= $_REQUEST['hmsquantidadepaga'];
$valorUnitario 			= $_REQUEST['hmsvalorunitario'];
$micid					= $_REQUEST['micid'];
$ano		 			= $_SESSION['ano'];//$_REQUEST['ano'];

$quantidade				= number_format($quantidade,0,'.','');

if($scsid == NAOINICIADAITEMCOMP){
	$hmsobs	= $_REQUEST['hmsobsi'];
}else{
	$hmsobs	= $_REQUEST['hmsobs'];
}

/************************* INSTANCIA CLASSES E OBJETOS ******************************/
$obdata 	= new Data();
$obHistItem	= new HistoricoConvItemComposicao();

/************************* CARREGA DADOS PARA MOSTRAR NA TELA E CARREGA COMBOS *****/
if( $_REQUEST['hciid'] ){// carrega dados do item de composi��o caso exista um monitoramento
	$obHistItem->carregarPorId( $_REQUEST['hciid'] );
}

$acaind 					 	= recuperaDadosPopupItensComp($sbaid, $cosid); 	  // recupera dados de refer�ncia
$listaHistorico 			 	= recuperaHistoricoMonitoramentoPagItens($cosid); // carrega lista de historico de monitoramento do item
$selectStatusMotivoItensComp 	= statusMotivoItensComp($obHistItem->micid);

/************************* SALVA DADOS ******************************/
if( $_REQUEST['salvar'] ){
	$sucesso = 0;
	if($hmsid){
		// Regra de n�gocios - c�lculos dados e retira v�rgulas para salvar certo no BD //
		$quantEmpenhada = str_replace(".", "", $quantEmpenhada);
		$quantEmpenhada = str_replace(",", "", $quantEmpenhada);
		$quantliquidada = str_replace(".", "", $quantliquidada);
		$quantliquidada = str_replace(",", "", $quantliquidada);
		$quantPaga 		= str_replace(".", "", $quantPaga);
		$quantPaga 		= str_replace(",", "", $quantPaga);
		$valorUnitario 	= str_replace(".", "", $valorUnitario);
		$valorUnitario 	= str_replace(",", ".", $valorUnitario);
		
		$valorTotalEmpenhado 	= $quantEmpenhada * $valorUnitario;
		$ValorTotalLiquidado 	= $quantliquidada * $valorUnitario;
		$ValorTotalPago 		= $quantPaga * $valorUnitario;
		// Fim Regra de n�gocios - c�lculos //
		// salva dados //
		$arCampos = array(  "hciid","hmsid","cosid");
		$obHistItem->popularObjeto( $arCampos );
		$obHistItem->scsid = $scsid;
		
		$obHistItem->hmsquantidadeempenhada = null;
		$obHistItem->hmsquantidadeliquidada = null;
		$obHistItem->hmsquantidadepaga 		= null;
		$obHistItem->hmsvalorunitario 		= null;
		$obHistItem->hmsvalortotalempenhado = null;
		$obHistItem->hmsvalortotalliquidado = null;
		$obHistItem->hmsvalortotalpago 		= null;
		$obHistItem->hmsano 				= $ano;
		$obHistItem->hmsobs 				= $hmsobs;
		
		if($scsid == EXECUTADOITEMCOMP || $scsid == EMEXECU��OITEMCOMP){ //Se form 1
			$obHistItem->hmsquantidadeempenhada = $quantEmpenhada;
			$obHistItem->hmsquantidadeliquidada = $quantliquidada;
			$obHistItem->hmsquantidadepaga 		= $quantPaga;
			$obHistItem->hmsvalorunitario 		= $valorUnitario;
			$obHistItem->hmsvalortotalempenhado = $valorTotalEmpenhado;
			$obHistItem->hmsvalortotalliquidado = $ValorTotalLiquidado;
			$obHistItem->hmsvalortotalpago 		= $ValorTotalPago;
			$obHistItem->hmsobs 				= null;
			$obHistItem->micid	 				= null;
			$arCamposNulo = array('hmsobs', 'micid');
		}else if($scsid == NAOINICIADAITEMCOMP){ 						//Se form 2
			$obHistItem->hmsobs 				= $hmsobs;
			$obHistItem->micid 					= $micid;
			$arCamposNulo = array(	'hmsquantidadeempenhada', 
									'hmsquantidadeliquidada',
									'hmsquantidadepaga',
									'hmsvalorunitario',
									'hmsvalortotalempenhado',
									'hmsvalortotalliquidado',
									'hmsvalortotalpago'
								);
			$sucesso = 1;
		}else{ 															//Se form 3
			$obHistItem->hmsobs 				= $hmsobs;
			$sucesso = 1;
			$arCamposNulo = array(	'hmsquantidadeempenhada', 
									'hmsquantidadeliquidada',
									'hmsquantidadepaga',
									'hmsvalorunitario',
									'hmsvalortotalempenhado',
									'hmsvalortotalliquidado',
									'hmsvalortotalpago'
								);
		}
		
		if($scsid == EXECUTADOITEMCOMP || $scsid == EMEXECU��OITEMCOMP){
			if($quantEmpenhada == "" || $quantliquidada == "" || $quantPaga == "" || $valorUnitario == "" ){
				echo "<script> 
				alert( 'Todos os campos s�o obrigat�rios. ' ); 
		  	  </script>";
				$sucesso = 0;
				
			}else{
				$sucesso = 1;
			}
		}
		
		if($sucesso == 1){
			$obHistItem->salvar(true, true, $arCamposNulo);
			$obHistItem->commit();
			$cosid = $obHistItem->cosid;
			$hciid = $obHistItem->hciid;
			if($quantPaga == NULL){
				$quantPaga = 0;
			}
			if($valor == NULL){
					$valor = 0;
			}
			if($valorUnitario == NULL){
					$valorUnitario = 0;
			}
			if($ValorTotalPago == NULL){
					$ValorTotalPago = 0;
			}
			if($quantidade == NULL){
					$quantidade = 0;
			}
			
		   echo '<script> 
					alert( "Opera��o realizada com sucesso. " );
					parent.closeLytebox('.$cosid.','.$sbaid.','.$hmsid.','.$hciid.','.$quantidade.','.$valor.', '.$quantPaga.', '.$valorUnitario.', '.$ValorTotalPago.');
			  	  </script>';
		}
    }else{
    	echo "<script> 
				alert( 'N�o existe monitoramento aberto. Inicie um monitoramento para salvar os dados. ' ); 
		  	  </script>";
    }

}

/************************* CARREGA VARIAVEIS E FORMATA DADOS PARA MOSTRAR NA TELA ******************************/
$hmsobs 	= $obHistItem->hmsobs;
$hmsobsi	= $obHistItem->hmsobs;
$scsid		= $obHistItem->scsid;
$comboStatusItensComposicao	= carregaComboStatusItensComposicao($scsid, $cosid);
$valor 		= number_format($valor, 2, ',', '.');

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../../brasilpro/monitoraFinanceiro/includes/monitora_financeiro.css"/>
	<link rel="stylesheet" type="text/css" href="../../includes/Estilo.css"/>
	<link rel="stylesheet" type="text/css" href="../../includes/listagem.css"/>
	<script type="text/javascript" src="../../includes/funcoes.js"></script>
</head>
<body>
<form name="formulario" id="formulario" action="" method="post">
	<input type=hidden name="salvar" value="false" />
	<input type=hidden name="hciid" value="<?php echo $obHistItem->hciid; ?>" />
	<input type=hidden name="cosid" value="<?php echo $cosid; ?>" />
	<input type=hidden name="ano" value="<?php echo $ano; ?>" />
	<table align="center" border="0" class="tabela" cellpadding="3" cellspacing="1" width="100%"; style="border-bottom: 0 !important;">
		<colgroup>
			<col/>
		</colgroup>
		<tbody>
			<tr>
				<td class="tdDegradde02" style="padding: 0 15px 0 15px;">
					<div style="float: left; position: relative;">
						<h3 title="Monitora itens composi��o">
							Monitora item composi��o - <? echo $acaind['cosdsc']; ?>
						</h3>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="tabela" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" align="center">
		<tr><th colspan="2">Dados de refer�ncia</th></tr>
		<tr><td class="SubTituloDireita">Dimens�o :</td><td><? echo $acaind['dimcod'].". ".$acaind['dimdsc']; ?></td></tr>
		<tr><td class="SubTituloDireita">�rea :</td><td><? echo $acaind['ardcod'].". ".$acaind['arddsc']; ?></td></tr>
		<tr><td class="SubTituloDireita">Indicador :</td><td><? echo $acaind['indcod'].". ".$acaind['inddsc']; ?></td></tr>
		<tr><td class="SubTituloDireita">Descri��o Suba��o :</td><td><? echo $acaind['sbadsc']; ?></td></tr>
		<tr><td class="SubTituloDireita">Quantidade do Item :</td><td><? echo $quantidade; ?></td></tr>
		<tr><td class="SubTituloDireita">Valor do Item :</td><td><? echo $valor;?></td></tr>
	</table>
		<br />
	<table class="tabela" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3" align="center">
		<tr>
			<td class="SubTituloDireita">Selecione o status do item:</td>
			<td><?=$comboStatusItensComposicao; ?></td>
		</tr>
	</table>

	<table id="form1" align="center" border="0" class="tabela" cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<th>Execu��o</th>
			<th>Quantidades</th>
		</tr>
		<tr>
			<td class="SubTituloDireita" >Quantidade empenhada</td>
			
			
			<td><?= campo_texto('hmsquantidadeempenhada', "N", "S", "", 30, 50, "", "", '', '', '', 'id="hmsquantidadeempenhada" onkeyup="calculatotais(this, \'quantidadeempenhada\');"', '', $obHistItem->hmsquantidadeempenhada ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Quantidade liquidada</td>
			<td><?= campo_texto('hmsquantidadeliquidada', "N", "S", "", 30, 50, "", "", '', '', 0, 'id="hmsquantidadeliquidada" onkeyup="calculatotais(this, \'quantidadeliquidada\');"','',$obHistItem->hmsquantidadeliquidada ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Quantidade paga</td>
			<td><?= campo_texto('hmsquantidadepaga', "N", "S", "", 30, 50, "", "", '', '', 0, 'id="hmsquantidadepaga" onkeyup="calculatotais(this, \'quantidadepaga\'); "','',$obHistItem->hmsquantidadepaga ); ?></td>
		</tr>
		<tr>
			<th></th>
			<th>Valores:</th>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor unit�rio contratado/executado</td>
			<td><?= campo_texto('hmsvalorunitario', "N", "S", "", 30, 50, "", "", '', '', 0, 'id="hmsvalorunitario" onkeyup="mascaraValor(this); calculatotais(this, \'valorunitario\');"','',$obHistItem->hmsvalorunitario );  ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor total empenhado</td>
			<td><?= campo_texto('hmsvalortotalempenhado', "N", "N", "", 30, 50, "", "", '', '', 0, 'id="hmsvalortotalempenhado"','',$obHistItem->hmsvalortotalempenhado );?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor total liquidado</td>
			<td><?= campo_texto('hmsvalortotalliquidado', "N", "N", "", 30, 50, "", "", '', '', 0, 'id="hmsvalortotalliquidado"','',$obHistItem->hmsvalortotalliquidado ); ?></td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Valor total pago</td>
			<td><?= campo_texto('hmsvalortotalpago', "N", "N", "", 30, 50, "", "", '', '', 0, 'id="hmsvalortotalpago"','',$obHistItem->hmsvalortotalpago ); ?></td>
		</tr>	
		<tr>
			<td></td>
			<td><input type="button" name="salvar" value="salvar"  onclick="salvarDados();" /></td>
		</tr>
	</table>

	<table id="form2" align="center" border="0" class="tabela" cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<th colspan="2" >Porque n�o foi iniciado?</th>
		</tr>
		<tr>
			<td class="SubTituloDireita">Motivo:</td>
			<td><?=$selectStatusMotivoItensComp; ?>
			</td>
		</tr>
		<tr>
			<td class="SubTituloDireita">Observa��o:</td>
			<td><?=campo_textarea('hmsobsi', 'N', 'S', '', 70, 3, 2000 ); ?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="button" name="salvar" value="salvar"  onclick="salvarDados();" /></td>
		</tr>
	</table>

	<table id="form3" align="center" border="0" class="tabela" cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<th id="texto" colspan="2" ></th>
		</tr>
		<tr>
			<td class="SubTituloDireita">Motivo:</td>
			<td><?=campo_textarea('hmsobs', "S", 'S', '', 70, 3, 2000 ); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="button" name="salvar" value="salvar"  onclick="salvarDados();" /></td>
		</tr>
	</table>

	<br>
	<table align="center" border="0" class="tabela" cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<th class="tdDegradde02" colspan="4" >Hist�rico de Monitoramento do Item</th>
		</tr>
		<tr>
			<td colspan="3">
			<?php 
				if(count($listaHistorico) >= 1 ){//Formata data
					$valorEmpenhado = 0;
					$valorLiquidado = 0;
					$valorPago		= 0;
					for ($cont = 0; $cont < count($listaHistorico); $cont++){
						$mes = $obdata->formataData($listaHistorico[$cont]['hmcdatamonitoramento'],"mesTextual de yyyy","yyyy-mm-dd");
						$listaHistorico[$cont]['hmcdatamonitoramento'] = $mes;
						
						// calculos de valores
						$calculoEmpenhado = (integer)$listaHistorico[$cont]['hmsvalortotalempenhado'] * (integer)$listaHistorico[$cont]['hmsquantidadeempenhada'];
						$calculoLiquidado = (integer)$listaHistorico[$cont]['hmsvalortotalliquidado'] * (integer)$listaHistorico[$cont]['hmsquantidadeliquidada'];
						$calculoPago	  = (integer)$listaHistorico[$cont]['hmsvalortotalpago'] * (integer)$listaHistorico[$cont]['hmsquantidadepaga'];
						$valorEmpenhado   = $calculoEmpenhado + $valorEmpenhado;
						$valorLiquidado   = $calculoLiquidado + $valorLiquidado;
						$valorPago 		  = $calculoPago + $valorPago;
						
						$listaHistorico[$cont]['hmsvalortotalempenhado'] = $valorEmpenhado;
						$listaHistorico[$cont]['hmsvalortotalliquidado'] = $valorEmpenhado;
						$listaHistorico[$cont]['hmsvalortotalpago'] 	 = $valorEmpenhado;
					}
				}

				$cabecalho = array("M�s de Monitoramento", "status", "Qtd.empenhada", "Qtd. liquidada", 
								   "Qtd. paga", "Vlr. unit�rio", "Vlr. empenhado", "Vlr. liquidado", "Vlr.pago");
				$obHistItem->monta_lista_simples($listaHistorico,$cabecalho,20, 10, 'N', '100%', '');
			?>
			</td>
		</tr>
	</table>
	
</form>
<script type="text/javascript">


	//mascaraValor( document.getElementById( 'hmsquantidadeempenhada' ) );
	//mascaraValor( document.getElementById( 'hmsquantidadeliquidada' ) );
	//mascaraValor( document.getElementById( 'hmsquantidadepaga' ) );
	mascaraValor( document.getElementById( 'hmsvalorunitario' ));
	mascaraValor( document.getElementById( 'hmsvalortotalempenhado' ));
	mascaraValor( document.getElementById( 'hmsvalortotalliquidado' ));
	mascaraValor( document.getElementById( 'hmsvalortotalpago' ));
	
	// Esconde todas os form da tela.
	document.getElementById('form1').style.display = 'none';
	document.getElementById('form2').style.display = 'none';
	document.getElementById('form3').style.display = 'none';
	var statusItensComposicao = document.getElementById('scsid').value;
	
	function calculatotais(campo, calculoCampo){
		var quantEmpenhada 		= document.getElementById('hmsquantidadeempenhada').value;
		var quantidadeliquidada = document.getElementById('hmsquantidadeliquidada').value;
		var quantidadepaga 		= document.getElementById('hmsquantidadepaga').value;
		var valorunitario 		= document.getElementById('hmsvalorunitario').value;
		var valorunitarioMasc 	= document.getElementById('hmsvalorunitario').value;
		if(valorunitario == ""){
			valorunitario = '1,0';
		}
		
		if(calculoCampo == 'quantidadeempenhada'){ 
			if(calculoCampo == 'valorunitario'){
				var qtdempe 		= campo.value;
				qtdempe 			=  parseFloat(replaceAll(replaceAll(qtdempe, ".", ""), ",", "."));
				valorunitario   =  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var qtde 		= qtdempe * valorunitario;
				document.getElementById('hmsvalortotalempenhado').value = mascaraValorResultado(qtde);
			}
		}
		if(calculoCampo == 'quantidadeliquidada'){
			if(calculoCampo == 'valorunitario'){
				var qtdl 		= campo.value;
				qtdl 			=  parseFloat(replaceAll(replaceAll(qtdl, ".", ""), ",", "."));
				valorunitarioa   =  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var qtdli 		= qtdl * valorunitarioa;
				document.getElementById('hmsvalortotalliquidado').value = mascaraValorResultado(qtdli);
				//var qtde = parseInt( mascaraglobal( "[#]", campo.value ) ) * parseInt( mascaraglobal( "[#]", valorunitario ) );
				//document.getElementById('hmsvalortotalliquidado').value = mascaraValorInterno(qtde, quantidadeliquidada,valorunitarioMasc );
			}
		}
		if(calculoCampo == 'quantidadepaga'){
			if(calculoCampo == 'valorunitario'){
				var qtdpaga 	= campo.value;
				qtdpaga 		=  parseFloat(replaceAll(replaceAll(qtdpaga, ".", ""), ",", "."));
				valorunitariob   =  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var qtdpagaa 	= qtdpaga * valorunitariob;
				document.getElementById('hmsvalortotalpago').value = mascaraValorResultado(qtdpagaa);
				//var qtde = parseInt( mascaraglobal( "[#]", campo.value ) ) * parseInt( mascaraglobal( "[#]", valorunitario ) );
				//document.getElementById('hmsvalortotalpago').value 		= mascaraValorInterno(qtde, quantidadepaga,valorunitarioMasc );
			}
		}
		if(calculoCampo == 'valorunitario'){
		
			if(quantEmpenhada){
				var quantEmpenhada 		= quantEmpenhada;
				quantEmpenhada 			=  parseFloat(replaceAll(replaceAll(quantEmpenhada, ".", ""), ",", "."));
				valorunitarioc   		=  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var qtdEmp 		= quantEmpenhada * valorunitarioc;
				document.getElementById('hmsvalortotalempenhado').value = mascaraValorResultado(qtdEmp);
				//var qtde =  parseInt( mascaraglobal( "[#]", quantEmpenhada ) ) * parseInt( mascaraglobal( "[#]", valorunitario ) );
				//document.getElementById('hmsvalortotalempenhado').value = mascaraValorInterno(qtde, quantEmpenhada,valorunitarioMasc  );
			}
			
			if(quantidadeliquidada){
				var quantidadeliquidada 	= quantidadeliquidada;
				quantidadeliquidada 		=  parseFloat(replaceAll(replaceAll(quantidadeliquidada, ".", ""), ",", "."));
				valorunitariod   			=  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var quantidadeliqu 			= quantidadeliquidada * valorunitariod;
				document.getElementById('hmsvalortotalliquidado').value = mascaraValorResultado(quantidadeliqu);
				//var qtde =  parseInt( mascaraglobal( "[#]", quantidadeliquidada ) ) * parseInt( mascaraglobal( "[#]", valorunitario ) );
				//document.getElementById('hmsvalortotalliquidado').value = mascaraValorInterno(qtde, quantidadeliquidada, valorunitarioMasc );
			}
			
			if(quantidadepaga){
			
				var quantidadepaga 		= quantidadepaga;
				quantidadepaga 			=  parseFloat(replaceAll(replaceAll(quantidadepaga, ".", ""), ",", "."));
				valorunitarioe   =  parseFloat(replaceAll(replaceAll(valorunitario, ".", ""), ",", "."));
				var qtdpaga 		= quantidadepaga * valorunitarioe;
				document.getElementById('hmsvalortotalpago').value = mascaraValorResultado(qtdpaga);
				//var qtde =  parseInt( mascaraglobal( "[#]", quantidadepaga ) ) * parseInt( mascaraglobal( "[#]", valorunitario ) );
				//document.getElementById('hmsvalortotalpago').value 	= mascaraValorInterno(qtde, quantidadepaga, valorunitarioMasc );
			}
		}
		
	}
	
	function mascaraValorResultado( obj ){
		var valor = new String();
		valor = obj.toString();
		var complemento = '';
		if( valor.charAt(0) == '-' ){
			complemento = '-';
		}
		obj = complemento+MascaraMonetario( valor );
		return obj;

	}
	
	

	function mascaraValorInterno( vlr ,obj, obj2 ){
		valor = vlr;
		var complemento = '';
		if(obj2 != ""){
			if( obj2.substr(0, 1) == '-' ){
				complemento = '-';
			}
		}
		if( obj.substr(0, 1) == '-' ){
			complemento = '-';
		}
		return complemento+mascaraglobal( '[###.]###,##', valor );
		
	}
	
	
	/**
	 * function mascaraComNegativo();
	 * descscricao 	: Mascara valores negativos ou n�o
	 * author 		: Thiago Tasca Barbosa
	 */
	function mascaraValor( obj ){
		valor = obj.value;
		var complemento = '';
		
		if( valor.substr(0, 1) == '-' ){
			complemento = '-';
		}
		obj.value = complemento+mascaraglobal( '[###.]###,##', valor );
		
	}
		
	/**
	 * function salvarDados();
	 * descscricao 	: Salva dados do form no bd.
	 * author 		: Thiago Tasca Barbosa
	 */
	function salvarDados(){
		var statusItensComposicao = document.getElementById('scsid').value;
		var quantidade = <?=$quantidade;?>;
		if(statusItensComposicao == <?php echo CANCELADAITEMCOMP; ?> || statusItensComposicao == <?php echo EMPLANEJAMENTOITEMCOMP; ?>  ){
			var observacao = document.getElementById("hmsobs").value;
			if(!observacao){
				alert("Obrigat�rio informar o motivo");
				return false;
			}
		}
		if(statusItensComposicao == <?php echo NAOINICIADAITEMCOMP; ?>){
			var motivo = document.getElementById("micid").value;
			if(!motivo){
				alert("Obrigat�rio informar o status");
				return false;
			}
		}

		if(statusItensComposicao == <?php echo EXECUTADOITEMCOMP; ?> || statusItensComposicao == <?php echo EMEXECU��OITEMCOMP; ?>){
			var quantEmpenhada 	= document.getElementById('hmsquantidadeempenhada').value;
			var quantliquidada 	= document.getElementById('hmsquantidadeliquidada').value;
			var quantPaga		= document.getElementById('hmsquantidadepaga').value;
			var valorUnitario 	= document.getElementById('hmsvalorunitario').value;
			if(quantEmpenhada == '' || quantliquidada == '' || quantPaga == '' || valorUnitario == '' ){
				alert("Obrigat�rio informar todos os campos.");
				return false;
			}
			if(quantEmpenhada > quantidade || quantliquidada > quantidade || quantPaga > quantidade ){
				alert("A quantidade informada n�o pode ser maior que a quantidade do item de composi��o.");
				return false;
			}
		
		}
		document.formulario.salvar.value = true;
		document.formulario.submit();
	}
	
	/**
	 * function carregaEditarItensComposicao(idBtnEditar, valorSetado, sbaid, hmsid, hciid, cnvid);
	 * descscricao 	: Carrega o a tabela do form de acordo com o status.
	 * author 		: Thiago Tasca Barbosa
	 * parametros 	: scsid (status do item de composi��o)
	 */
	function carregaEditarItensComposicao(scsid){
		if(scsid == <?=EXECUTADOITEMCOMP;?> || scsid == <?=EMEXECU��OITEMCOMP;?> ){
			document.getElementById('form1').style.display = '';
			document.getElementById('form2').style.display = 'none';
			document.getElementById('form3').style.display = 'none';
		}else if(scsid == <?=NAOINICIADAITEMCOMP;?> ){
			document.getElementById('form1').style.display = 'none';
			document.getElementById('form2').style.display = '';
			document.getElementById('form3').style.display = 'none';
		}else if(scsid == <?=CANCELADAITEMCOMP;?> || scsid == <?=EMPLANEJAMENTOITEMCOMP;?>){
			document.getElementById('form1').style.display = 'none';
			document.getElementById('form2').style.display = 'none';
			document.getElementById('form3').style.display = '';
			var texto = "Porque foi cancelada?";
			if(scsid == <?=EMPLANEJAMENTOITEMCOMP;?>){
				texto = "Porque est� em planejamento?";
			}
			document.getElementById('texto').innerHTML = texto;
		}
	}
	
	/* Fun��o para subustituir todos 
	* descscricao 	: 
	 * author 		: Alexandre Dourado
	 * parametros 	: 
	 */
	function replaceAll(str, de, para){
	    var pos = str.indexOf(de);
	    while (pos > -1){
			str = str.replace(de, para);
			pos = str.indexOf(de);
		}
	    return (str);
	}
	
	
	carregaEditarItensComposicao(statusItensComposicao);
</script>
</body>
</html>