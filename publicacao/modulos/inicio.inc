<?php
/**
 * Sistema de Publica��o.
 * werteralmeida
 */

/**
 * Cabecalho do SIMEC.
 * @see cabecalho.inc
 */
include APPRAIZ . "includes/cabecalho.inc";

/*
 * Componetnes para a cria��o da p�gina In�cio
 */
include APPRAIZ . "includes/dashboardbootstrap.inc";

?>

<link href="/includes/spo.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="/includes/funcoesspo.js"></script>
<script type="text/javascript" lang="javascript">$(document).ready(function () {
        inicio();
    });
</script>
<table width="95%" class="tabela" bgcolor="#f5f5f5" border="0" cellSpacing="1" cellPadding="3" align="center">
    <tr align="center">
        <td align="center">
             <div class="divGraf bg-gn">
                <span class="tituloCaixa">Pedidos de Publica��o</span>
                <?php
                $params = array();
                $params['texto'] = 'Cadastrar Solicita��o';
                $params['tipo'] = 'cadastrar';
                $params['url'] = 'publicacao.php?modulo=principal/solicitacaopublicacao/formSolicitacaopublicacao&acao=A';
                montaBotaoInicio($params);

                $params = array();
                $params['texto'] = 'Listar Solicita��es';
                $params['tipo'] = 'listar';
                $params['url'] = 'publicacao.php?modulo=principal/solicitacaopublicacao/listarSolicitacaopublicacao&acao=A';
                montaBotaoInicio($params);

                ?>
                <span class="subTituloCaixa">Relat�rios</span>
                <?php
                    $params = array();
                    $params['texto'] = 'Geral';
                    $params['tipo'] = 'relatorio';
                    $params['url'] = '';
                    montaBotaoInicio($params);
                    
                    $params['texto'] = 'Relat�rio Resumido';
                    $params['tipo'] = 'relatorio';
                    $params['url'] = '';
                    montaBotaoInicio($params);
                ?>
            </div>
            <div id="divManuais" class="divGraf">
                <span class="tituloCaixa" >Manuais</span> <br><br><br>
                <?php
                $params = array();
                $params['texto'] = 'Manual de Solicita��o';
                $params['tipo'] = 'pdf';
                $params['url'] = '';
                montaBotaoInicio($params);
                ?>
            </div>
        </td>
    </tr>
</table>