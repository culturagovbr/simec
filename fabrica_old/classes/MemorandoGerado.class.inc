<?php
/**
 * Memorando
 * @author Ronda
 *
 */
class MemorandoGerado
{
    private $html;
    
    public function setHtml( $html )
    {
        $this->html = $html;
    }
    
    public function getHtml()
    {
        return $this->html;
    }
}
