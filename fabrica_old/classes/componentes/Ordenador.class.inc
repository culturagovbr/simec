<?php
/**
 * 
 * Componente que monta o Ordenador da Grid
 * @author R�yner Lima <rayner.lima@squadra.com.br>
 *
 */
class Ordenador {
	
	private $strNomeOrdenador;
	private $arrCamposOrdenacao;
	private $strUrlAjax;
	
	const ORDEM_PADRAO = 'asc';

	/**
	 * 
	 * Recebe um nome para o ordenador com o prop�sito de ser impresso para nome de classes css e ids para o html
	 * @param str $strNomeOrdenador
	 */
	public function setNomeOrdenador( $strNomeOrdenador ){
		$this->strNomeOrdenador = $strNomeOrdenador;
	}
	
	/**
	 *
	 * Informa � classe um array associativo com campos que servir�o de atributos para ordena��o,
	 * onde a chave do array � o que ser� apresentado na p�gina e o valor � o nome do campo no banco de dados.
	 * Caso o o valor de uma chave seja <code>null</code>, o ordenador apresentar� na tela apenas um texto com a chave do array, 
	 * caso contr�rio apresentar� um link com o valor do array.
	 * <code>$arrCamposOrdenacao = array('Nome do usu�rio'=>'no_usuario', 'Data de Nascimento'=>'dt_nascimento');</code>
	 * @param array $arrCamposOrdenacao
	 */
	public function setCamposOrdenacao( $arrCamposOrdenacao ){
		$this->arrCamposOrdenacao = $arrCamposOrdenacao;
	}
	
	/**
	 * 
	 * Informa � classe uma url que ser� chamada ao clicar num link impresso pelo ordenador
	 * @param str $strUrlAjax
	 */
	public function setUrlAjax( $strUrlAjax ) {
		$this->strUrlAjax = $strUrlAjax;
	}
	
	/**
	 * 
	 * Imprime o cabe�alho na p�gina gerando uma sa�da html
	 */
	public function montaOrdenador() {
		$elementos = "<thead>";
		$elementos .= 	"<tr id=\"ordenador\">";
		$x = 1;
		$urlAjax = $this->strUrlAjax;
		$nomeOrdenador = $this->strNomeOrdenador;
		foreach ($this->arrCamposOrdenacao as $label => $campo) {
			if ($x==1){
				$ativo = "ativo";
			} else {
				$ativo = "";
			}
			if ($campo == null){
				$elementos .= "<th>$label</th>";
			} else {
				$elementos .= "<th><a id=\"$campo-asc\" href=\"$urlAjax\" class=\"ordenador $nomeOrdenador $ativo\">$label</th>";
				++$x;
			}
		}
		$elementos .= 	"</tr>";
		$elementos .= "</thead>";
		return $elementos;
	}
	
}