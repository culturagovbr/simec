<?php

include_once "_funcoes.php";

$perfis = pegaPerfilGeral();

if ( isset( $_REQUEST['buscar'] ) ) {
	include $_REQUEST['relatorio'];
	exit;
}

include APPRAIZ . 'includes/cabecalho.inc';
print '<br/>';

monta_titulo( "Relat�rio Personalizados - SISPACTO", "" );

if(in_array(PFL_COORDENADORIES,$perfis)) {
	
	$relatorios = array(0=>array('codigo'=>'relatpersonal_relatorioabrangencia.inc','descricao'=>'Relat�rio de Abrang�ncia Munic�pios/Universidades'),
						1=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO'),
						2=>array('codigo'=>'relatpersonal_turmasgerais.inc','descricao'=>'Informa��es sobre dados gerais das turmas'),
						3=>array('codigo'=>'relatpersonal_aprendizagem.inc','descricao'=>'Informa��es sobre aprendizagem'),
						4=>array('codigo'=>'relatpersonal_evasao.inc','descricao'=>'Relat�rio de evas�o do perfis'),
						5=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
						6=>array('codigo'=>'relatpersonal_acompanhamentogeral.inc','descricao'=>'Relat�rio de Acompanhamento Geral')
						
						);
	
} else {
	
	$relatorios = array(0=>array('codigo'=>'relatpersonal_erroscadastrossgb.inc','descricao'=>'Relat�rio de erros no cadastro de bolsistas no SGB'),
						1=>array('codigo'=>'relatpersonal_aceitacaotermocompromisso.inc','descricao'=>'Relat�rio de ades�o ao termo de compromisso'),
						2=>array('codigo'=>'relatpersonal_errospagamentossgb.inc','descricao'=>'Relat�rio de erros no envio de pagamentos no SGB'),
						3=>array('codigo'=>'relatpersonal_bolsistasparados.inc','descricao'=>'Relat�rio de Bolsistas sem tramita��o do pagamento'),
						4=>array('codigo'=>'relatpersonal_relatorioabrangencia.inc','descricao'=>'Relat�rio de Abrang�ncia Munic�pios/Universidades'),
						5=>array('codigo'=>'relatpersonal_perfilparticipantes.inc','descricao'=>'Perfil dos Participantes do PACTO'),
						6=>array('codigo'=>'relatpersonal_numerobolsistas.inc','descricao'=>'N�mero de bolsistas do PACTO'),
						7=>array('codigo'=>'relatpersonal_turmasgerais.inc','descricao'=>'Informa��es sobre dados gerais das turmas'),
						8=>array('codigo'=>'relatpersonal_professorespacto.inc','descricao'=>'Professores participando do Pacto'),
						9=>array('codigo'=>'relatpersonal_aprendizagem.inc','descricao'=>'Informa��es sobre aprendizagem'),
						10=>array('codigo'=>'relatpersonal_turmasformadores.inc','descricao'=>'Turmas de formadores'),
						11=>array('codigo'=>'relatpersonal_quantitativos.inc','descricao'=>'Quantitativos do SISPACTO'),
						12=>array('codigo'=>'relatpersonal_dataplanoatv.inc','descricao'=>'Relat�rio das datas dos planos de atividades'),
						13=>array('codigo'=>'relatpersonal_gestaomobilizacao.inc','descricao'=>'Relat�rio personalizados Gest�o e Mobiliza��o'),
						14=>array('codigo'=>'relatpersonal_evasao.inc','descricao'=>'Relat�rio de evas�o do perfis'),
						15=>array('codigo'=>'relatpersonal_notacomplementar.inc','descricao'=>'Relat�rio de avalia��o complementar'),
						16=>array('codigo'=>'relatpersonal_acompanhamentogeral.inc','descricao'=>'Relat�rio de Acompanhamento Geral'),
						17=>array('codigo'=>'relatpersonal_relatoriofinal.inc','descricao'=>'Situa��o dos Relat�rios Finais'),
						18=>array('codigo'=>'relatpersonal_desempenho.inc','descricao'=>'Desempenho das redes estaduais e municipais (f�sico e financeiro)'),
						19=>array('codigo'=>'relatpersonal_analiserelatoriofinal.inc','descricao'=>'An�lise do relat�rio final'),
						20=>array('codigo'=>'relatpersonal_orcamento.inc','descricao'=>'An�lise de or�amento'),
						21=>array('codigo'=>'relatpersonal_inep.inc','descricao'=>'Relat�rio de INEP'),
			
						
						);
	
}


?>
<script type="text/javascript">
function exibirRelatorio() {
	if(document.getElementById('relatorio').value!='') {
		var formulario = document.formulario;
		// submete formulario
		formulario.target = 'relatoriopersonlizadossispacto';
		var janela = window.open( '', 'relatoriopersonlizadossispacto', 'width=900,height=645,status=1,menubar=1,toolbar=0,resizable=0,scrollbars=1' );
		formulario.submit();
		janela.focus();
	} else {
		alert("Selecione um relat�rio");
		return false;
	}
}
</script>

<form action="" method="post" name="formulario">
	<input type="hidden" name="buscar" value="1"/>
	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td>Filtros</td></tr>		
		<tr>
			<td class="SubTituloDireita" valign="top">Relat�rios :</td>
			<td>
			<?
			$db->monta_combo('relatorio', $relatorios, 'S', 'Selecione', '', '', '', '400', 'S', 'relatorio');
			?>
			</td>
		</tr>

	    <tr>
		<td class="SubTituloDireita" valign="top">&nbsp;</td>
		<td class="SubTituloDireita" style="text-align:left;"><input type="button" name="filtrar" value="Visualizar" onclick="exibirRelatorio();"/></td>
		</tr>
</table>