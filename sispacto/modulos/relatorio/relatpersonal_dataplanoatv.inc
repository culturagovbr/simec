<?
/* configurações */
ini_set("memory_limit", "2048M");
set_time_limit(600);
/* FIM configurações */
?>
<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel='stylesheet' type='text/css' href='../includes/listagem.css'/>

	<table class="tabela" align="center" bgcolor="#f5f5f5" cellspacing="1" cellpadding="3">		
        <tr><td class="SubTituloEsquerda">Datas por universidade</td></tr>		
		<tr>
			<td>
			<? 
			$sql = "
			select 
			
			universidade,
			di1||' - '||df1 as per1, 
			di2||' - '||df2 as per2,
			di3||' - '||df3 as per3,
			di4||' - '||df4 as per4,
			di5||' - '||df5 as per5,
			di6||' - '||df6 as per6,
			di7||' - '||df7 as per7,
			di8||' - '||df8 as per8
			
			from (
select 
foo.universidade,
case when ini_1 is not null then to_char(ini_1,'dd/mm/YYYY') else to_char(iniprev_1,'dd/mm/YYYY') end as di1,
case when ini_2 is not null then to_char(ini_2,'dd/mm/YYYY') else to_char(iniprev_2,'dd/mm/YYYY') end as di2,
case when ini_3 is not null then to_char(ini_3,'dd/mm/YYYY') else to_char(iniprev_3,'dd/mm/YYYY') end as di3,
case when ini_4 is not null then to_char(ini_4,'dd/mm/YYYY') else to_char(iniprev_4,'dd/mm/YYYY') end as di4,
case when ini_5 is not null then to_char(ini_5,'dd/mm/YYYY') else to_char(iniprev_5,'dd/mm/YYYY') end as di5,
case when ini_6 is not null then to_char(ini_6,'dd/mm/YYYY') else to_char(iniprev_6,'dd/mm/YYYY') end as di6,
case when ini_7 is not null then to_char(ini_7,'dd/mm/YYYY') else to_char(iniprev_7,'dd/mm/YYYY') end as di7,
case when ini_8 is not null then to_char(ini_8,'dd/mm/YYYY') else to_char(iniprev_8,'dd/mm/YYYY') end as di8,

case when fim_1 is not null then to_char(fim_1,'dd/mm/YYYY') else to_char(fimprev_1,'dd/mm/YYYY') end as df1,
case when fim_2 is not null then to_char(fim_2,'dd/mm/YYYY') else to_char(fimprev_2,'dd/mm/YYYY') end as df2,
case when fim_3 is not null then to_char(fim_3,'dd/mm/YYYY') else to_char(fimprev_3,'dd/mm/YYYY') end as df3,
case when fim_4 is not null then to_char(fim_4,'dd/mm/YYYY') else to_char(fimprev_4,'dd/mm/YYYY') end as df4,
case when fim_5 is not null then to_char(fim_5,'dd/mm/YYYY') else to_char(fimprev_5,'dd/mm/YYYY') end as df5,
case when fim_6 is not null then to_char(fim_6,'dd/mm/YYYY') else to_char(fimprev_6,'dd/mm/YYYY') end as df6,
case when fim_7 is not null then to_char(fim_7,'dd/mm/YYYY') else to_char(fimprev_7,'dd/mm/YYYY') end as df7,
case when fim_8 is not null then to_char(fim_8,'dd/mm/YYYY') else to_char(fimprev_8,'dd/mm/YYYY') end as df8

from (

select 
uu.unisigla || ' / ' || uu.uninome as universidade,

(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 11 AND es.uncid=u.uncid) as iniprev_1, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 12 AND es.uncid=u.uncid) as iniprev_2, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 13 AND es.uncid=u.uncid) as iniprev_3, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 14 AND es.uncid=u.uncid) as iniprev_4, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 15 AND es.uncid=u.uncid) as iniprev_5, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 16 AND es.uncid=u.uncid) as iniprev_6, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 17 AND es.uncid=u.uncid) as iniprev_7, 
(SELECT au.aundatainicioprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 18 AND es.uncid=u.uncid) as iniprev_8,

 
 
 (SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 11 AND es.uncid=u.uncid) as fimprev_1, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 12 AND es.uncid=u.uncid) as fimprev_2, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 13 AND es.uncid=u.uncid) as fimprev_3, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 14 AND es.uncid=u.uncid) as fimprev_4, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 15 AND es.uncid=u.uncid) as fimprev_5, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 16 AND es.uncid=u.uncid) as fimprev_6, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 17 AND es.uncid=u.uncid) as fimprev_7, 
(SELECT au.aundatafimprev FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 18 AND es.uncid=u.uncid) as fimprev_8, 
 


 (SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 11 AND es.uncid=u.uncid) as ini_1, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 12 AND es.uncid=u.uncid) as ini_2, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 13 AND es.uncid=u.uncid) as ini_3, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 14 AND es.uncid=u.uncid) as ini_4, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 15 AND es.uncid=u.uncid) as ini_5, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 16 AND es.uncid=u.uncid) as ini_6, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 17 AND es.uncid=u.uncid) as ini_7, 
(SELECT au.aundatainicio FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 18 AND es.uncid=u.uncid) as ini_8,


 

 (SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 11 AND es.uncid=u.uncid) as fim_1, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 12 AND es.uncid=u.uncid) as fim_2, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 13 AND es.uncid=u.uncid) as fim_3, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 14 AND es.uncid=u.uncid) as fim_4, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 15 AND es.uncid=u.uncid) as fim_5, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 16 AND es.uncid=u.uncid) as fim_6, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 17 AND es.uncid=u.uncid) as fim_7, 
(SELECT au.aundatafim FROM sispacto.atividadeuniversidade au 
 INNER JOIN sispacto.estruturacurso es ON es.ecuid = au.ecuid 
 WHERE au.suaid = 18 AND es.uncid=u.uncid) as fim_8

from sispacto.universidadecadastro u 
inner join sispacto.universidade uu on uu.uniid = u.uniid
) foo ) foo2
			";
			
			
			$cabecalho = array("Universidade","3.1. Abertura do Curso - Módulo inicial (40h)",
"3.2. 1° encontro com Orientadores",
"3.3. 2º encontro com Orientadores",
"3.4. Relatório parcial do curso",
"3.5. 3º encontro com Orientadores",
"3.6. 4º encontro com Orientadores",
"3.7. Solenidade de encerramento",
"3.8. Relatório final do curso");
			
			$db->monta_lista_simples($sql,$cabecalho,100000,5,'N','100%','N');
			 
			?>
			</td>
		</tr>
		

</table>