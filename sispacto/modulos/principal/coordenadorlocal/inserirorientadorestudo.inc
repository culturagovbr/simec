<?
include "_funcoes_coordenadorlocal.php";

if($_REQUEST['requisicao']) {
	$_REQUEST['requisicao']($_REQUEST);
	exit;
}

if(!$_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['picid']) {
	$al = array("alert"=>"Problemas no acesso a p�gina. Por favor, utilize a navega��o do sistema para acessar as p�ginas.","location"=>"sispacto.php?modulo=inicio&acao=C");
	alertlocation($al);
}

?>
<script language="JavaScript" src="../includes/funcoes.js"></script>
<link rel="stylesheet" type="text/css" href="../includes/Estilo.css"/>
<link rel="stylesheet" type="text/css" href="../includes/listagem.css"/>
<script language="javascript" type="text/javascript" src="../includes/JQuery/jquery-ui-1.8.4.custom/js/jquery-1.4.2.min.js"></script>
<script language="javascript" type="text/javascript" src="./js/sispacto.js"></script>
<script type=text/javascript src=/includes/prototype.js></script>
<script language="javascript" type="text/javascript" src="../includes/webservice/cpf.js" /></script>
<script>
var tipoOrientador = new Array(); 

<? foreach($_TIPO_ORIENTADORES as $key => $value) : ?>
tipoOrientador['<?=$key ?>'] = "<?=$value ?>";
<? endforeach; ?>

function inserirOrientadorEstudoLabel(cpf,nome,email,tipo,muncod,mundescricao) {

	var existe_cpf = window.opener.document.getElementById('img_'+cpf);
	
	if(existe_cpf) {
		alert('CPF ja esta cadastrado');
		return false;
	}
	
	var existe_em_outros_municipios = false;
	jQuery.ajax({
   		type: "POST",
   		url: window.location.href,
   		data: 'requisicao=verificarPerfilOutrosMunicipios&cpf='+cpf+'&picid=<?=$_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['picid'] ?>',
   		async: false,
   		success: function(texto){
   			if(texto!="") {
   				alert(texto);
   				existe_em_outros_municipios = true;	
   			}
   		}
	});
	if(existe_em_outros_municipios) {
		return false;
	}


	var tabela_or = window.opener.document.getElementById('tabelaorientadoresestudo');
	
	var total_a_serem_cadastrados = parseInt(window.opener.document.getElementById('total_a_serem_cadastrados').value);
	if(total_a_serem_cadastrados <= (tabela_or.rows.length-1)) {
		alert('N�o h� mais vagas de Orientadores de Estudo. Caso queira substituir, exclua um dos Orientadores selecionados na tela principal e selecione novamente');
		return false;
	}

	var linha = tabela_or.insertRow(tabela_or.rows.length);
	var col_acoes = linha.insertCell(0);
	col_acoes.innerHTML = "<center><img src=\"../imagens/excluir.gif\" border=\"0\" style=\"cursor:pointer;\" id=\"img_"+cpf+"_"+muncod+"\" onclick=\"removerOrientadorEstudo('', this);\"></center>";
	var col_cpf = linha.insertCell(1);
	col_cpf.innerHTML = cpf+"<input type=\"hidden\" name=\"cpf[]\" value=\""+cpf+"\">";
	var col_nome = linha.insertCell(2);
	col_nome.innerHTML = nome+"<input type=\"hidden\" name=\"nome["+cpf+"]\" value=\""+nome+"\">";
	var col_email = linha.insertCell(3);
	col_email.id  = "td_img_"+cpf+'_'+muncod;
	col_email.innerHTML = email+"<input type=\"hidden\" name=\"email["+cpf+"]\" value=\""+email+"\">";
	var col_mundescricao = linha.insertCell(4);
	col_mundescricao.innerHTML = mundescricao+"<input type=\"hidden\" name=\"muncodatuacao["+cpf+"]\" value=\""+muncod+"\">";
	var col_tipo = linha.insertCell(5);
	col_tipo.innerHTML = tipoOrientador[tipo]+"<input type=\"hidden\" name=\"tipo["+cpf+"]\" value=\""+tipo+"\">";
	var col_anexo = linha.insertCell(6);
	if(tipo=="profissionaismagisterio") {
		col_anexo.innerHTML = "<input type=\"file\" name=\"anexoportaria["+cpf+"]\">";	
	} else {
		col_anexo.innerHTML = "&nbsp;";
	}

	
	window.opener.document.getElementById('alteracaodados').value="1";
	
	return true;

}

function atualizaBotaoOrientadorEstudo(tipo) {
	var deparaMostrarBotaoOrientadorEstudo = new Array();
	deparaMostrarBotaoOrientadorEstudo['tutoresproletramento'] 		  = 'tutoresredesemproletramento';
	deparaMostrarBotaoOrientadorEstudo['tutoresredesemproletramento'] = 'profissionaismagisterio';

	if(deparaMostrarBotaoOrientadorEstudo[tipo]) {
		ajaxatualizar('requisicao=atualizarBotoesOrientadoresEstudo&picid=<?=$_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['picid'] ?>&tipo='+deparaMostrarBotaoOrientadorEstudo[tipo],'');
	}

}

function carregaUsuario() {
	divCarregando();
	var usucpf=document.getElementById('iuscpf').value;
	usucpf = usucpf.replace('-','');
	usucpf = usucpf.replace('.','');
	usucpf = usucpf.replace('.','');
				
	var comp = new dCPF();
	comp.buscarDados(usucpf);
	var arrDados = new Object();
	
	if(!comp.dados.no_pessoa_rf){
		alert('CPF Inv�lido');
		divCarregado();
		return false;
	}
	
	document.getElementById('iusnome').value=comp.dados.no_pessoa_rf;
	divCarregado();
}

function inserirProfissionalMagisterio() {

	if(!document.getElementById('muncod_endereco')) {
		alert('UF em branco');
		return false;
	}

	jQuery('#iuscpf').val(mascaraglobal('###.###.###-##',jQuery('#iuscpf').val()));
	
	if(jQuery('#iuscpf').val()=='') {
		alert('CPF em branco');
		return false;
	}
	
	if(!validar_cpf(jQuery('#iuscpf').val())) {
		alert('CPF inv�lido');
		return false;
	}
	
	if(jQuery('#iusnome').val()=='') {
		alert('Nome em branco');
		return false;
	}
	
	if(jQuery('#iusemailprincipal').val()=='') {
		alert('Email Principal em branco');
		return false;
	}
	
    if(!validaEmail(jQuery('#iusemailprincipal').val())) {
    	alert('Email Principal inv�lido');
    	return false;
    }
    
    if(jQuery('#muncod_endereco').val()=='') {
    	alert('Munic�pio em branco');
    	return false;
    }
    
    var conf = confirm('Termo de Veracidade das Informa��es\n\nVoc� assegura que o(a) profissional cadastrado preenche os requisitos abaixo?\n\n-� servidor(a) efetivo(a) rede de ensino\n-Possui forma��o em Licenciatura ou Pedagogia\n-Possui experi�ncia na forma��o de professores alfabetizadores\n-N�o recebe bolsa de estudo de outro programa federal de forma��o\n-Possui disponibilidade para atuar como Orientador(a) de estudo');
    
    if(!conf) {
    	return false;
    }

	
	var resultado = inserirOrientadorEstudoLabel(jQuery('#iuscpf').val(),jQuery('#iusnome').val(),jQuery('#iusemailprincipal').val(),"profissionaismagisterio",jQuery('#muncod_endereco').val(),jQuery('#uf').val()+' / '+jQuery("#mundescricao").val());
	
	divCarregando();	
	
	if(resultado) {
		window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A';
	} else {
		divCarregado();
	}

}

function inserirTutoresProletramento(obj, mun) {

	var tabela = obj.parentNode.parentNode.parentNode;
	var linha  = obj.parentNode.parentNode;
	
	var cpf    = tabela.rows[linha.rowIndex-1].cells[1].innerHTML.substr(0,11);
	var nome   = tabela.rows[linha.rowIndex-1].cells[2].innerHTML;
	var email  = tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].value;
	var muncod = jQuery('#muncod_pl_'+cpf+'_'+mun).val();
	var mundescricao = jQuery('#uf_pl_'+cpf+'_'+mun).val()+' / '+jQuery('#mundescricao_pl_'+cpf+'_'+mun).val();
	
	cpf = mascaraglobal('###.###.###-##',cpf);
	
	if(cpf=='') {
		alert('CPF em branco');
		return false;
	}
	
	if(!validar_cpf(cpf)) {
		alert('CPF inv�lido');
		return false;
	}
	
	if(nome=='') {
		alert('Nome em branco');
		return false;
	}
	
	if(email=='') {
		alert('Email em branco');
		return false;
	}
	
    if(!validaEmail(email)) {
    	alert('Email inv�lido');
    	return false;
    }
    
    if(muncod=='') {
    	alert('Munic�pio em branco');
    	return false;
    }
    
    var conf = confirm('Voc� assegura que este(a) profissional foi tutor do Pr�-Letramento e est� vinculado � sua rede de ensino?');
    
    if(!conf) {
    	return false;
    }
	
	var resultado = inserirOrientadorEstudoLabel(cpf,nome,email,"tutoresproletramento",muncod,mundescricao);
	
	divCarregando();
	
	if(resultado) {
		atualizaBotaoOrientadorEstudo('tutoresproletramento');
		atualizaBotaoOrientadorEstudo('tutoresredesemproletramento');
		tabela.rows[linha.rowIndex-1].cells[0].innerHTML = "<img src=../imagens/gif_inclui_d.gif>";
		tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].className = "disabled";
		tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].readOnly=true;
	}
	
	divCarregado();

}

function inserirTutoresSemProletramento(obj, mun) {

	var tabela = obj.parentNode.parentNode.parentNode;
	var linha  = obj.parentNode.parentNode;
	
	var cpf   = tabela.rows[linha.rowIndex-1].cells[1].innerHTML.substr(0,11);
	var nome  = tabela.rows[linha.rowIndex-1].cells[2].innerHTML;
	var email = tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].value;
	var muncod = jQuery('#muncod_pl_'+cpf+'_'+mun).val();
	var mundescricao = jQuery('#uf_pl_'+cpf+'_'+mun).val()+' / '+jQuery('#mundescricao_pl_'+cpf+'_'+mun).val();
	
	cpf = mascaraglobal('###.###.###-##',cpf);
	
	if(cpf=='') {
		alert('CPF em branco');
		return false;
	}
	
	if(!validar_cpf(cpf)) {
		alert('CPF inv�lido');
		return false;
	}
	
	if(nome=='') {
		alert('Nome em branco');
		return false;
	}
	
	if(email=='') {
		alert('Email em branco');
		return false;
	}
	
    if(!validaEmail(email)) {
    	alert('Email inv�lido');
    	return false;
    }
    
    if(muncod=='') {
    	alert('Munic�pio em branco');
    	return false;
    }
    
    var conf = confirm('Termo de Veracidade das Informa��es\n\nVoc� assegura que este(a) professor(a) preenche os requisitos abaixo?\n\n-� servidor(a) efetivo(a) rede de ensino\n-Possui forma��o em Licenciatura ou Pedagogia\n-Possui experi�ncia na forma��o de professores alfabetizados\n-N�o recebe bolsa de estudo de outro programa federal de forma��o\n-Possui disponibilidade para atuar como Orientador(a) de estudo');
    
    if(!conf) {
    	return false;
    }
	
	var resultado = inserirOrientadorEstudoLabel(cpf,nome,email,"tutoresredesemproletramento",muncod,mundescricao);
	
	divCarregando();
	
	if(resultado) {
		tabela.rows[linha.rowIndex-1].cells[0].innerHTML = "<img src=../imagens/gif_inclui_d.gif>";
		tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].className = "disabled";
		tabela.rows[linha.rowIndex-1].cells[3].childNodes[0].readOnly=true;
	}
	
	divCarregado();

}

function carregarMundescricao(vlr) {
	jQuery('#mundescricao').val(jQuery("#muncod_endereco option:selected").text());
}

</script>
<?
switch($_REQUEST['navegacao']) :
case 'tutoresproletramento':
?>
<script>
jQuery(document).ready(function() {
	window.opener.jQuery("[id^='img_']").each(function() {
	
		var id_img = replaceAll(replaceAll(jQuery(this).attr('id'),".",""),"-","");
		
		if(document.getElementById('a_'+id_img)) {
		
			jQuery('#a_'+id_img).attr('src','../imagens/gif_inclui_d.gif');
			jQuery('#a_'+id_img).attr('onclick','').unbind('click');
			jQuery('#a_'+id_img).css('cursor','');
			
			var emailp = window.opener.document.getElementById('td_'+jQuery(this).attr('id')).innerHTML;
			emailp = emailp.split("<");
			
			jQuery('#email_'+id_img).val(emailp[0]);
			jQuery('#email_'+id_img).attr('class','disabled');
			jQuery('#email_'+id_img).attr('readonly', true);
			
		}
	});
});
</script>
<form method="post" name="formulario" id="formulario">
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2"><?=$_TIPO_ORIENTADORES[$_REQUEST['navegacao']] ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">CPF</td>
	<td><?=campo_texto('cpf', "N", "S", "CPF", 15, 14, "###.###.###-##", "", '', '', 0, 'id="cpf"', '', $_REQUEST['cpf']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">Nome</td>
	<td><?=campo_texto('nome', "N", "S", "Nome", 30, 60, "", "", '', '', 0, 'id="nome"', '', $_REQUEST['nome']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">UF</td>
	<td><?
	if(!isset($_REQUEST['uf'])) $_REQUEST['uf'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['estuf'];
	$sql = "SELECT estuf as codigo, estuf as descricao FROM territorios.estado ORDER BY estuf";
	$db->monta_combo('uf', $sql, 'S', 'Selecione', 'carregarMunicipiosPorUF3', '', '', '200', 'N', 'uf', '', $_REQUEST['uf']);
	?></td>
</tr>
<tr>
	<td class="SubTituloDireita">Munic�pio</td>
	<td id="td_municipio3">
	<? 
	if($_REQUEST['uf']) :
		if(!isset($_REQUEST['muncod_endereco'])) $_REQUEST['muncod_endereco'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['muncod'];
		$sql = "SELECT muncod as codigo, mundescricao as descricao FROM territorios.municipio WHERE estuf='".$_REQUEST['uf']."' ORDER BY mundescricao"; 
		$db->monta_combo('muncod_endereco', $sql, 'S', 'Selecione', '', '', '', '', 'S', 'muncod_endereco', '', $_REQUEST['muncod_endereco']);
	else: 
		echo "Selecione uma UF";
	endif; ?>
	</td>
</tr>
<tr>
	<td class="SubTituloCentro" colspan="2"><input type="submit" value="Filtrar"> <input type="button" value="Voltar" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A';"></td>
</tr>
</table>
</form>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td colspan="2">
	<?
	if($_REQUEST['cpf']) {
		$f[] = "cpf='".str_replace(array(".","-"),array(""),$_REQUEST['cpf'])."'";
	}
	if($_REQUEST['nome']) {
		$f[] = "removeacento(nome)=removeacento('".$_REQUEST['nome']."')";
	}
	if($_REQUEST['uf']) {
		$f[] = "uf='".$_REQUEST['uf']."'";
	}
	if($_REQUEST['muncod_endereco']) {
		$f[] = "p.muncod='".$_REQUEST['muncod_endereco']."'";
	}
	
	$sql = "SELECT '<img id=\"a_img_'||cpf||'_'||m.muncod||'\" src=\"../imagens/gif_inclui.gif\" style=\"cursor:pointer;\" onclick=\"inserirTutoresProletramento(this,\''||m.muncod||'\');\" ><input type=\"hidden\" name=\"uf['||cpf||']\" id=\"uf_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.estuf||'\"><input type=\"hidden\" name=\"muncod['||cpf||']\" id=\"muncod_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.muncod||'\"><input type=\"hidden\" name=\"mundescricao['||cpf||']\" id=\"mundescricao_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.mundescricao||'\">' as acao, cpf, nome, '<input class=normal id=\"email_img_'||cpf||'_'||m.muncod||'\" type=text name=email['||cpf||'] size=30>' as email, m.estuf||' / '||m.mundescricao as municipio 
			FROM sispacto.tutoresproletramento p
			LEFT JOIN territorios.municipio m ON m.muncod = p.muncod   
			WHERE tupstatus='A'".(($f)?" AND ".implode(" AND ",$f):"")." 
			GROUP BY cpf, nome, m.estuf, m.muncod, m.mundescricao
			ORDER BY nome";
	
	$cabecalho = array("&nbsp;","CPF","Nome","Email","Munic�pio");
	$db->monta_lista($sql,$cabecalho,50,10,'N','center',$par2);
	?>	
	</td>
</tr>
</table>
<?
break;
case 'tutoresredesemproletramento':
?>
<script>
jQuery(document).ready(function() {
	window.opener.jQuery("[id^='img_']").each(function() {
	
		var id_img = replaceAll(replaceAll(jQuery(this).attr('id'),".",""),"-","");
		
		if(document.getElementById('a_'+id_img)) {
		
			jQuery('#a_'+id_img).attr('src','../imagens/gif_inclui_d.gif');
			jQuery('#a_'+id_img).attr('onclick','').unbind('click');
			jQuery('#a_'+id_img).css('cursor','');
			
			var emailp = window.opener.document.getElementById('td_'+jQuery(this).attr('id')).innerHTML;
			emailp = emailp.split("<");
			
			jQuery('#email_'+id_img).val(emailp[0]);
			jQuery('#email_'+id_img).attr('class','disabled');
			jQuery('#email_'+id_img).attr('readonly', true);
			
		}
	});
});
</script>
<form method="post" name="formulario" id="formulario">
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2"><?=$_TIPO_ORIENTADORES[$_REQUEST['navegacao']] ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">CPF</td>
	<td><?=campo_texto('cpf', "N", "S", "CPF", 15, 14, "###.###.###-##", "", '', '', 0, 'id="cpf"', '', $_REQUEST['cpf']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">Nome</td>
	<td><?=campo_texto('nome', "N", "S", "Nome", 30, 60, "", "", '', '', 0, 'id="nome"', '', $_REQUEST['nome']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">UF</td>
	<td><?
	$_REQUEST['uf'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['estuf'];
	echo "<input type=\"hidden\" name=\"uf\" id=\"uf\" value=\"".$_REQUEST['uf']."\">".$_REQUEST['uf'];
	?></td>
</tr>
<tr>
	<td class="SubTituloDireita">Munic�pio</td>
	<td id="td_municipio3">
	<? 
	$_REQUEST['muncod_endereco'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['muncod'];
	$mundescricao = $db->pegaUm("SELECT mundescricao FROM territorios.municipio WHERE muncod='".$_REQUEST['muncod_endereco']."'");
	echo "<input type=\"hidden\" name=\"mundescricao\" id=\"mundescricao\" value=\"".$mundescricao."\"><input type=\"hidden\" name=\"muncod_endereco\" id=\"muncod_endereco\" value=\"".$_REQUEST['muncod_endereco']."\">".$mundescricao; 
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloCentro" colspan="2"><input type="submit" value="Filtrar"> <input type="button" value="Voltar" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A';"></td>
</tr>
</table>
</form>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td colspan="2">
	<?
	if($_REQUEST['cpf']) {
		$f[] = "cpf='".str_replace(array(".","-"),array(""),$_REQUEST['cpf'])."'";
	}
	if($_REQUEST['nome']) {
		$f[] = "removeacento(docente)=removeacento('".$_REQUEST['nome']."')";
	}
	if($_REQUEST['uf']) {
		$f[] = "uf='".$_REQUEST['uf']."'";
	}
	if($_REQUEST['muncod_endereco']) {
		$f[] = "t.muncod='".$_REQUEST['muncod_endereco']."'";
	}
	
	$sql = "SELECT '<img id=\"a_img_'||cpf||'_'||m.muncod||'\" src=\"../imagens/gif_inclui.gif\" style=\"cursor:pointer;\" onclick=\"inserirTutoresSemProletramento(this,\''||m.muncod||'\');\" ><input type=\"hidden\" name=\"uf['||cpf||']\" id=\"uf_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.estuf||'\"><input type=\"hidden\" name=\"muncod['||cpf||']\" id=\"muncod_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.muncod||'\"><input type=\"hidden\" name=\"mundescricao['||cpf||']\" id=\"mundescricao_pl_'||cpf||'_'||m.muncod||'\" value=\"'||m.mundescricao||'\">' as acao, cpf, docente, '<input class=normal id=\"email_img_'||cpf||'_'||m.muncod||'\" type=text name=email['||cpf||'] size=30 value=\"'||email||'\">' as email, m.estuf||' / '||m.mundescricao as municipio 
			FROM sispacto.tutoressemproletramento t 
			LEFT JOIN territorios.municipio m ON m.muncod = t.muncod
			WHERE esfera ilike '".$_SESSION['sispacto']['esfera']."' and tspstatus='A'".(($f)?" AND ".implode(" AND ",$f):"")."
			GROUP BY cpf, docente, email, m.estuf, m.muncod, m.mundescricao
			ORDER BY docente";
	
	$cabecalho = array("&nbsp;","CPF","Nome","Email","Munic�pio");
	$db->monta_lista($sql,$cabecalho,50,10,'N','center',$par2);
	?>	
	</td>
</tr>
</table>
<?
break;
case 'profissionaismagisterio':
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro" colspan="2"><?=$_TIPO_ORIENTADORES[$_REQUEST['navegacao']] ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Orienta��es</td>
	<td><? echo carregarOrientacao($_SERVER['REQUEST_URI']); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">CPF</td>
	<td><?=campo_texto('iuscpf', "S", "S", "CPF", 15, 14, "###.###.###-##", "", '', '', 0, 'id="iuscpf"', '', mascaraglobal($iuscpf,"###.###.###-##"), 'if(this.value!=\'\'){carregaUsuario();}'); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Nome</td>
	<td><?=campo_texto('iusnome', "S", "N", "Nome", 67, 150, "", "", '', '', 0, 'id="iusnome"', ''); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita" width="20%">Email</td>
	<td><?=campo_texto('iusemailprincipal', "S", "S", "Principal", 67, 60, "", "", '', '', 0, 'id="iusemailprincipal"'); ?></td>
</tr>
<tr>
	<td class="SubTituloDireita">UF</td>
	<td><?
	$_REQUEST['uf'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['estuf'];
	echo "<input type=\"hidden\" name=\"uf\" id=\"uf\" value=\"".$_REQUEST['uf']."\">".$_REQUEST['uf'];
	?></td>
</tr>
<tr>
	<td class="SubTituloDireita">Munic�pio</td>
	<td id="td_municipio3">
	<?
	if($_SESSION['sispacto']['esfera']=='municipal') { 
		$_REQUEST['muncod_endereco'] = $_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['muncod'];
		$mundescricao = $db->pegaUm("SELECT mundescricao FROM territorios.municipio WHERE muncod='".$_REQUEST['muncod_endereco']."'");
		echo "<input type=\"hidden\" name=\"mundescricao\" id=\"mundescricao\" value=\"".$mundescricao."\"><input type=\"hidden\" name=\"muncod_endereco\" id=\"muncod_endereco\" value=\"".$_REQUEST['muncod_endereco']."\">".$mundescricao;
	} else {
		$sql = "SELECT muncod as codigo, mundescricao as descricao FROM territorios.municipio WHERE estuf='".$_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['estuf']."' ORDER BY mundescricao";
		$db->monta_combo('muncod_endereco', $sql, 'S', 'Selecione', 'carregarMundescricao', '', '', '', 'S', 'muncod_endereco', '');
		echo "<input type=\"hidden\" name=\"mundescricao\" id=\"mundescricao\" value=\"\">";
	}
	?>
	</td>
</tr>
<tr>
	<td class="SubTituloCentro" colspan="2"><input type="button" name="salvar" value="Salvar" onclick="inserirProfissionalMagisterio();"> <input type="button" value="Voltar" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A';"></td>
</tr>
</table>
<?
break;
default:
	$sql = "SELECT picmostrarbotaotutoresredesemproletramento, picmostrarbotaoprofissionaismagisterio FROM sispacto.pactoidadecerta WHERE picid='".$_SESSION['sispacto']['coordenadorlocal'][$_SESSION['sispacto']['esfera']]['picid']."' AND picstatus='A'";
	$pactoidadecerta = $db->pegaLinha($sql);
?>
<table class="tabela" bgcolor="#f5f5f5" cellSpacing="1" cellPadding="3" align="center">
<tr>
	<td class="SubTituloCentro">Inserir Orientadores de Estudo</td>
</tr>
<tr>
	<td>
	<p><b>- Tutores do Pr�-Letramento:</b></p>
	<p>Priorize o preenchimento das vagas dispon�veis com profissionais da sua rede que foram tutores do Pr�-Letramento. Ao clicar no bot�o "Tutores do Pr�-Letramento", ser�o exibidos os nomes dos tutores cujo endere�o informado no Sistema Geral de Bolsas indicava este munic�pio.</p>
	<p align="center"><input type="button" name="tutoresproletramento" value="Tutores do Pr�-Letramento" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A&navegacao=tutoresproletramento';"><br><input type="button" value="N�o desejo cadastrar tutores do Pr�-Letramento" onclick="atualizaBotaoOrientadorEstudo('tutoresproletramento');atualizaBotaoOrientadorEstudo('tutoresredesemproletramento');window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A';"></p>
	<p>Em caso de d�vida, consulte o Manual de Orienta��es do SisPacto.</p>
	</td>
</tr>
<? if($pactoidadecerta['picmostrarbotaotutoresredesemproletramento']=="t") : ?>
<tr>
	<td>
	<p><b>-Professores que n�o foram Tutores do Pr�-Letramento:</b></p>
	<p>Caso a sua rede de ensino n�o possua profissionais que foram tutores do Pr�-Letramento ou estes n�o possam atuar como Orientadores de Estudo do Pacto, clique no bot�o abaixo e preencha ou complete as vagas dispon�veis com professores da sua rede (informados no Censo 2012) com experi�ncia na forma��o de alfabetizadores e que atendam aos requisitos exigidos.</p>
	<p align="center"><input type="button" name="tutoresredesemproletramento" value="Professores da rede que n�o foram Tutores do Pr�-Letramento" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A&navegacao=tutoresredesemproletramento';"></p>
	<p>Em caso de d�vida, consulte o Manual de Orienta��es do SisPacto.</p>
	</td>
</tr>
<? endif; ?>
<? if($pactoidadecerta['picmostrarbotaoprofissionaismagisterio']=="t") : ?>
<tr>
	<td>
	<p><b>- Profissionais do Magist�rio com experi�ncia em forma��o:</b></p>
	<p>Se a sua rede de ensino n�o possui profissionais que foram tutores do Pr�-Letramento nem professores com experi�ncia na forma��o de alfabetizadores, preencha ou complete as vagas dispon�veis com profissionais do magist�rio, vinculados � sua rede, que preencham os requisitos exigidos.</p>
	<p align="center"><input type="button" name="profissionaismagisterio" value="Profissionais do Magist�rio com experi�ncia em forma��o de professores" onclick="divCarregando();window.location='sispacto.php?modulo=principal/coordenadorlocal/inserirorientadorestudo&acao=A&navegacao=profissionaismagisterio';"></p>	
	</td>
</tr>
<? endif; ?>
<tr>
	<td class="SubTituloCentro"><input type="button" value="Fechar" onclick="window.close();"></td>
</tr>
</table>
<?
endswitch;
?>